package canon.utils;

import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.AutoDetectParser;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.Parser;
import org.apache.tika.sax.BodyContentHandler;
import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by rifai on 19/4/16.
 */
public class FileUtils {

    public static String contentType(InputStream fis) {
        ContentHandler contenthandler = new BodyContentHandler();
        Metadata metadata = new Metadata();

        Parser parser = new AutoDetectParser();
        ParseContext parseContext = new ParseContext();
        String contentType = "";

        try {
            parser.parse(fis, contenthandler, metadata, parseContext);
            contentType = metadata.get(Metadata.CONTENT_TYPE);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (TikaException e) {
            e.printStackTrace();
        }

        return contentType;
    }
}
