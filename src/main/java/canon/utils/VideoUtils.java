package canon.utils;

/**
 * Created by rifai on 10/4/16.
 */

import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.AutoDetectParser;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.Parser;
import org.apache.tika.sax.BodyContentHandler;
import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;

import javax.media.*;
import javax.media.control.FrameGrabbingControl;
import javax.media.control.FramePositioningControl;
import javax.media.format.VideoFormat;
import javax.media.util.BufferToImage;
import java.awt.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/** @author Krishna Vangapandu **/
public class VideoUtils {

    private static List<String> AMBIGUOUS_VIDEO_FILE_TYPES = new ArrayList<String>();

    static {
        AMBIGUOUS_VIDEO_FILE_TYPES.add("audio/3gpp");
        AMBIGUOUS_VIDEO_FILE_TYPES.add("audio/3gpp2");
        AMBIGUOUS_VIDEO_FILE_TYPES.add("audio/mp4");
        AMBIGUOUS_VIDEO_FILE_TYPES.add("application/mp4");
        AMBIGUOUS_VIDEO_FILE_TYPES.add("application/mp21");
    }

    final static String reg = "(?:youtube(?:-nocookie)?\\.com\\/(?:[^\\/\\n\\s]+\\/\\S+\\/|(?:v|e(?:mbed)?)\\/|\\S*?[?&]v=)|youtu\\.be\\/)([a-zA-Z0-9_-]{11})";
    final static String regV = "(/https?:\\/\\/(?:www\\.|player\\.)?vimeo.com\\/(?:channels\\/(?:\\w+\\/)?|groups\\/([^\\/]*)\\/videos\\/|album\\/(\\d+)\\/video\\/|video\\/|)(\\d+)(?:$|\\/|\\?)/)";


    @SuppressWarnings("deprecation")
    /** * videoFile - path to the video File. */
    public static Player getPlayer(String videoFile)
            throws NoPlayerException, IOException
    {
        File f = new File(videoFile);
        if (!f.exists()) throw new FileNotFoundException("File doesnt exist");
        MediaLocator ml = new MediaLocator(f.toURL());
        Player player = Manager.createPlayer(ml);
        player.realize();
        while (player.getState() != Player.Realized);
        return player;
    }
    public static float getFrameRate(Player player)
    {
        return (float)noOfFrames(player)/(float)player.getDuration().getSeconds();
    }
    public static int noOfFrames(Player player)
    {
        FramePositioningControl fpc = (FramePositioningControl)player.getControl("javax.media.control.FramePositioningControl");
        Time duration = player.getDuration();
        int i = fpc.mapTimeToFrame(duration);
        if (i != FramePositioningControl.FRAME_UNKNOWN) return i;
        else return -1;
    }
    /** * * @param player - the player from which you want to get the image
     * @param frameNumber - the framenumber you want to extract
     * @return Image at the current frame position */
    public static Image getImageOfCurrentFrame(Player player, int frameNumber)
    {
        FramePositioningControl fpc = (FramePositioningControl) player .getControl("javax.media.control.FramePositioningControl");
        FrameGrabbingControl fgc = (FrameGrabbingControl) player .getControl("javax.media.control.FrameGrabbingControl");
        return getImageOfCurrentFrame(fpc, fgc, frameNumber);
    }

    public static Image getImageOfCurrentFrame(FramePositioningControl fpc, FrameGrabbingControl fgc, int frameNumber)
    {
        fpc.seek(frameNumber);
        Buffer frameBuffer = fgc.grabFrame();
        BufferToImage bti = new BufferToImage((VideoFormat) frameBuffer .getFormat());
        return bti.createImage(frameBuffer);
    }

    public static FramePositioningControl getFPC(Player player)
    {
        FramePositioningControl fpc = (FramePositioningControl) player .getControl("javax.media.control.FramePositioningControl");
        return fpc;
    }

    public static FrameGrabbingControl getFGC(Player player)
    {
        FrameGrabbingControl fgc = (FrameGrabbingControl) player .getControl("javax.media.control.FrameGrabbingControl");
        return fgc;
    }

    public static ArrayList<Image> getAllImages(Player player)
    {
        ArrayList<Image> imageSeq = new ArrayList<Image>();
        int numberOfFrames = noOfFrames(player);
        FramePositioningControl fpc = getFPC(player);
        FrameGrabbingControl fgc = getFGC(player);
        for (int i = 0;i <= numberOfFrames;i++)
        {
            Image img = getImageOfCurrentFrame(fpc, fgc, i);
            if(img!=null) imageSeq.add(img);
        }
        return imageSeq;
    }

    public static ArrayList<Image> getAllImages(String fileName) throws NoPlayerException,IOException
    {
        Player player = getPlayer(fileName);
        ArrayList<Image> img = getAllImages(player);
        player.close();
        return img;
    }

    public static boolean isVideo(InputStream fis) {
        ContentHandler contenthandler = new BodyContentHandler();
        Metadata metadata = new Metadata();

        Parser parser = new AutoDetectParser();
        ParseContext parseContext = new ParseContext();
        String contentType = "";

        try {
            parser.parse(fis, contenthandler, metadata, parseContext);
            contentType = metadata.get(Metadata.CONTENT_TYPE);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (TikaException e) {
            e.printStackTrace();
        }

        return contentType.startsWith("video") || AMBIGUOUS_VIDEO_FILE_TYPES.contains(contentType);
    }

    public static String isWhatsVideosType( String videoUrl ){
        String result =  "o";
        if( VideoUtils.getVideoYoutubeId( videoUrl ).toString().length() > 0 ) {
            result = "y";
        }else if( VideoUtils.getVideoVimeoId( videoUrl ).toString().length() > 0 ) {
            result = "v";
        }
        return result;
    }

    public static String getVideoYoutubeId(String videoUrl) {
        Pattern pattern = Pattern.compile(reg, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(videoUrl);

        if (matcher.find())
            return matcher.group(1);
        return "";
    }

    public static String getVideoVimeoId(String url) {
        // standard web url. format: "https://vimeo.com/49243107" or
        // "http://vimeo.com/channels/staffpicks/49243107"
        {
            Pattern u = Pattern.compile("vimeo.com.*/(\\d+)");
            Matcher um = u.matcher(url.toString());

            if (um.find())
                return um.group(1);
        }
        // rss feed url. format:
        // "http://vimeo.com/moogaloop.swf?clip_id=49243107"
        {
            Pattern u = Pattern.compile("vimeo.com.*=(\\d+)");
            Matcher um = u.matcher(url.toString());

            if (um.find())
                return um.group(1);
        }
        return "";
    }
}
