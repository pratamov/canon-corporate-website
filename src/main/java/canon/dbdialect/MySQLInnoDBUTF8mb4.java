package canon.dbdialect;

import org.hibernate.dialect.MySQLInnoDBDialect;

/**
 * Sets the default charset to UTF-8mb4.
 */
public class MySQLInnoDBUTF8mb4 extends MySQLInnoDBDialect {

    @Override
    public String getTableTypeString() {
        return " ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci";
    }
}
