
package org.tempuri;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the org.tempuri package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _CreateCanonOnSiteServiceRequestsSerialnumber_QNAME = new QName("http://tempuri.org/", "serialnumber");
    private final static QName _CreateCanonOnSiteServiceRequestsFaultydescription_QNAME = new QName("http://tempuri.org/", "faultydescription");
    private final static QName _CreateCanonOnSiteServiceRequestsAvoidlunchtime_QNAME = new QName("http://tempuri.org/", "avoidlunchtime");
    private final static QName _CreateCanonOnSiteServiceRequestsSalutation_QNAME = new QName("http://tempuri.org/", "salutation");
    private final static QName _CreateCanonOnSiteServiceRequestsContactperson_QNAME = new QName("http://tempuri.org/", "contactperson");
    private final static QName _CreateCanonOnSiteServiceRequestsContactno_QNAME = new QName("http://tempuri.org/", "contactno");
    private final static QName _CreateCanonOnSiteServiceRequestsEmailaddress_QNAME = new QName("http://tempuri.org/", "emailaddress");
    private final static QName _CreateCanonOnSiteServiceRequestsConfirmemailaddress_QNAME = new QName("http://tempuri.org/", "confirmemailaddress");
    private final static QName _CreateCanonOnSiteServiceRequestsCompanyname_QNAME = new QName("http://tempuri.org/", "companyname");
    private final static QName _CreateCanonOrderConsumablesTypeofconsumables_QNAME = new QName("http://tempuri.org/", "typeofconsumables");
    private final static QName _CreateCanonOrderConsumablesModel_QNAME = new QName("http://tempuri.org/", "model");
    private final static QName _CreateCanonOrderConsumablesQuantity_QNAME = new QName("http://tempuri.org/", "quantity");
    private final static QName _CreateCanonOrderConsumablesOtherinstructions_QNAME = new QName("http://tempuri.org/", "otherinstructions");
    private final static QName _CreateCanonOrderConsumablesFileName_QNAME = new QName("http://tempuri.org/", "fileName");
    private final static QName _CreateCanonOrderConsumablesFileType_QNAME = new QName("http://tempuri.org/", "fileType");
    private final static QName _CreateCanonOrderConsumablesFileData_QNAME = new QName("http://tempuri.org/", "fileData");
    private final static QName _CreateCanonEmailEnquiriesCountry_QNAME = new QName("http://tempuri.org/", "country");
    private final static QName _CreateCanonEmailEnquiriesTopic_QNAME = new QName("http://tempuri.org/", "topic");
    private final static QName _CreateCanonEmailEnquiriesSubtopic_QNAME = new QName("http://tempuri.org/", "subtopic");
    private final static QName _CreateCanonEmailEnquiriesApplyforposition_QNAME = new QName("http://tempuri.org/", "applyforposition");
    private final static QName _CreateCanonEmailEnquiriesEnquiry_QNAME = new QName("http://tempuri.org/", "enquiry");
    private final static QName _CreateCanonVoiceOfCustomerServicearea_QNAME = new QName("http://tempuri.org/", "servicearea");
    private final static QName _CreateCanonVoiceOfCustomerSubservicearea_QNAME = new QName("http://tempuri.org/", "subservicearea");
    private final static QName _CreateCanonVoiceOfCustomerStaff_QNAME = new QName("http://tempuri.org/", "staff");
    private final static QName _CreateCanonVoiceOfCustomerKeeppostedonarrivaltime_QNAME = new QName("http://tempuri.org/", "keeppostedonarrivaltime");
    private final static QName _CreateCanonVoiceOfCustomerPresenceknownuponarrival_QNAME = new QName("http://tempuri.org/", "presenceknownuponarrival");
    private final static QName _CreateCanonVoiceOfCustomerExplainaftertheservice_QNAME = new QName("http://tempuri.org/", "explainaftertheservice");
    private final static QName _CreateCanonVoiceOfCustomerSpeakwellofservice_QNAME = new QName("http://tempuri.org/", "speakwellofservice");
    private final static QName _CreateCanonVoiceOfCustomerWhyyousayso_QNAME = new QName("http://tempuri.org/", "whyyousayso");
    private final static QName _CreateeBPCaseShortdescription_QNAME = new QName("http://tempuri.org/", "shortdescription");
    private final static QName _CreateeBPCaseDetaileddescription_QNAME = new QName("http://tempuri.org/", "detaileddescription");
    private final static QName _CreateeBPCaseCasetype_QNAME = new QName("http://tempuri.org/", "casetype");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.tempuri
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CreateCanonOnSiteServiceRequests }
     * 
     */
    public CreateCanonOnSiteServiceRequests createCreateCanonOnSiteServiceRequests() {
        return new CreateCanonOnSiteServiceRequests();
    }

    /**
     * Create an instance of {@link CreateCanonOnSiteServiceRequestsResponse }
     * 
     */
    public CreateCanonOnSiteServiceRequestsResponse createCreateCanonOnSiteServiceRequestsResponse() {
        return new CreateCanonOnSiteServiceRequestsResponse();
    }

    /**
     * Create an instance of {@link CreateCanonOrderConsumables }
     * 
     */
    public CreateCanonOrderConsumables createCreateCanonOrderConsumables() {
        return new CreateCanonOrderConsumables();
    }

    /**
     * Create an instance of {@link CreateCanonOrderConsumablesResponse }
     * 
     */
    public CreateCanonOrderConsumablesResponse createCreateCanonOrderConsumablesResponse() {
        return new CreateCanonOrderConsumablesResponse();
    }

    /**
     * Create an instance of {@link CreateCanonEmailEnquiries }
     * 
     */
    public CreateCanonEmailEnquiries createCreateCanonEmailEnquiries() {
        return new CreateCanonEmailEnquiries();
    }

    /**
     * Create an instance of {@link CreateCanonEmailEnquiriesResponse }
     * 
     */
    public CreateCanonEmailEnquiriesResponse createCreateCanonEmailEnquiriesResponse() {
        return new CreateCanonEmailEnquiriesResponse();
    }

    /**
     * Create an instance of {@link CreateCanonVoiceOfCustomer }
     * 
     */
    public CreateCanonVoiceOfCustomer createCreateCanonVoiceOfCustomer() {
        return new CreateCanonVoiceOfCustomer();
    }

    /**
     * Create an instance of {@link CreateCanonVoiceOfCustomerResponse }
     * 
     */
    public CreateCanonVoiceOfCustomerResponse createCreateCanonVoiceOfCustomerResponse() {
        return new CreateCanonVoiceOfCustomerResponse();
    }

    /**
     * Create an instance of {@link CreateeBPCase }
     * 
     */
    public CreateeBPCase createCreateeBPCase() {
        return new CreateeBPCase();
    }

    /**
     * Create an instance of {@link CreateeBPCaseResponse }
     * 
     */
    public CreateeBPCaseResponse createCreateeBPCaseResponse() {
        return new CreateeBPCaseResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "serialnumber", scope = CreateCanonOnSiteServiceRequests.class)
    public JAXBElement<String> createCreateCanonOnSiteServiceRequestsSerialnumber(String value) {
        return new JAXBElement<String>(_CreateCanonOnSiteServiceRequestsSerialnumber_QNAME, String.class, CreateCanonOnSiteServiceRequests.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "faultydescription", scope = CreateCanonOnSiteServiceRequests.class)
    public JAXBElement<String> createCreateCanonOnSiteServiceRequestsFaultydescription(String value) {
        return new JAXBElement<String>(_CreateCanonOnSiteServiceRequestsFaultydescription_QNAME, String.class, CreateCanonOnSiteServiceRequests.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "avoidlunchtime", scope = CreateCanonOnSiteServiceRequests.class)
    public JAXBElement<String> createCreateCanonOnSiteServiceRequestsAvoidlunchtime(String value) {
        return new JAXBElement<String>(_CreateCanonOnSiteServiceRequestsAvoidlunchtime_QNAME, String.class, CreateCanonOnSiteServiceRequests.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "salutation", scope = CreateCanonOnSiteServiceRequests.class)
    public JAXBElement<String> createCreateCanonOnSiteServiceRequestsSalutation(String value) {
        return new JAXBElement<String>(_CreateCanonOnSiteServiceRequestsSalutation_QNAME, String.class, CreateCanonOnSiteServiceRequests.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "contactperson", scope = CreateCanonOnSiteServiceRequests.class)
    public JAXBElement<String> createCreateCanonOnSiteServiceRequestsContactperson(String value) {
        return new JAXBElement<String>(_CreateCanonOnSiteServiceRequestsContactperson_QNAME, String.class, CreateCanonOnSiteServiceRequests.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "contactno", scope = CreateCanonOnSiteServiceRequests.class)
    public JAXBElement<String> createCreateCanonOnSiteServiceRequestsContactno(String value) {
        return new JAXBElement<String>(_CreateCanonOnSiteServiceRequestsContactno_QNAME, String.class, CreateCanonOnSiteServiceRequests.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "emailaddress", scope = CreateCanonOnSiteServiceRequests.class)
    public JAXBElement<String> createCreateCanonOnSiteServiceRequestsEmailaddress(String value) {
        return new JAXBElement<String>(_CreateCanonOnSiteServiceRequestsEmailaddress_QNAME, String.class, CreateCanonOnSiteServiceRequests.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "confirmemailaddress", scope = CreateCanonOnSiteServiceRequests.class)
    public JAXBElement<String> createCreateCanonOnSiteServiceRequestsConfirmemailaddress(String value) {
        return new JAXBElement<String>(_CreateCanonOnSiteServiceRequestsConfirmemailaddress_QNAME, String.class, CreateCanonOnSiteServiceRequests.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "companyname", scope = CreateCanonOnSiteServiceRequests.class)
    public JAXBElement<String> createCreateCanonOnSiteServiceRequestsCompanyname(String value) {
        return new JAXBElement<String>(_CreateCanonOnSiteServiceRequestsCompanyname_QNAME, String.class, CreateCanonOnSiteServiceRequests.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "typeofconsumables", scope = CreateCanonOrderConsumables.class)
    public JAXBElement<String> createCreateCanonOrderConsumablesTypeofconsumables(String value) {
        return new JAXBElement<String>(_CreateCanonOrderConsumablesTypeofconsumables_QNAME, String.class, CreateCanonOrderConsumables.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "model", scope = CreateCanonOrderConsumables.class)
    public JAXBElement<String> createCreateCanonOrderConsumablesModel(String value) {
        return new JAXBElement<String>(_CreateCanonOrderConsumablesModel_QNAME, String.class, CreateCanonOrderConsumables.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "serialnumber", scope = CreateCanonOrderConsumables.class)
    public JAXBElement<String> createCreateCanonOrderConsumablesSerialnumber(String value) {
        return new JAXBElement<String>(_CreateCanonOnSiteServiceRequestsSerialnumber_QNAME, String.class, CreateCanonOrderConsumables.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "quantity", scope = CreateCanonOrderConsumables.class)
    public JAXBElement<String> createCreateCanonOrderConsumablesQuantity(String value) {
        return new JAXBElement<String>(_CreateCanonOrderConsumablesQuantity_QNAME, String.class, CreateCanonOrderConsumables.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "otherinstructions", scope = CreateCanonOrderConsumables.class)
    public JAXBElement<String> createCreateCanonOrderConsumablesOtherinstructions(String value) {
        return new JAXBElement<String>(_CreateCanonOrderConsumablesOtherinstructions_QNAME, String.class, CreateCanonOrderConsumables.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "avoidlunchtime", scope = CreateCanonOrderConsumables.class)
    public JAXBElement<String> createCreateCanonOrderConsumablesAvoidlunchtime(String value) {
        return new JAXBElement<String>(_CreateCanonOnSiteServiceRequestsAvoidlunchtime_QNAME, String.class, CreateCanonOrderConsumables.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "salutation", scope = CreateCanonOrderConsumables.class)
    public JAXBElement<String> createCreateCanonOrderConsumablesSalutation(String value) {
        return new JAXBElement<String>(_CreateCanonOnSiteServiceRequestsSalutation_QNAME, String.class, CreateCanonOrderConsumables.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "contactperson", scope = CreateCanonOrderConsumables.class)
    public JAXBElement<String> createCreateCanonOrderConsumablesContactperson(String value) {
        return new JAXBElement<String>(_CreateCanonOnSiteServiceRequestsContactperson_QNAME, String.class, CreateCanonOrderConsumables.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "contactno", scope = CreateCanonOrderConsumables.class)
    public JAXBElement<String> createCreateCanonOrderConsumablesContactno(String value) {
        return new JAXBElement<String>(_CreateCanonOnSiteServiceRequestsContactno_QNAME, String.class, CreateCanonOrderConsumables.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "emailaddress", scope = CreateCanonOrderConsumables.class)
    public JAXBElement<String> createCreateCanonOrderConsumablesEmailaddress(String value) {
        return new JAXBElement<String>(_CreateCanonOnSiteServiceRequestsEmailaddress_QNAME, String.class, CreateCanonOrderConsumables.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "confirmemailaddress", scope = CreateCanonOrderConsumables.class)
    public JAXBElement<String> createCreateCanonOrderConsumablesConfirmemailaddress(String value) {
        return new JAXBElement<String>(_CreateCanonOnSiteServiceRequestsConfirmemailaddress_QNAME, String.class, CreateCanonOrderConsumables.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "companyname", scope = CreateCanonOrderConsumables.class)
    public JAXBElement<String> createCreateCanonOrderConsumablesCompanyname(String value) {
        return new JAXBElement<String>(_CreateCanonOnSiteServiceRequestsCompanyname_QNAME, String.class, CreateCanonOrderConsumables.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "fileName", scope = CreateCanonOrderConsumables.class)
    public JAXBElement<String> createCreateCanonOrderConsumablesFileName(String value) {
        return new JAXBElement<String>(_CreateCanonOrderConsumablesFileName_QNAME, String.class, CreateCanonOrderConsumables.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "fileType", scope = CreateCanonOrderConsumables.class)
    public JAXBElement<String> createCreateCanonOrderConsumablesFileType(String value) {
        return new JAXBElement<String>(_CreateCanonOrderConsumablesFileType_QNAME, String.class, CreateCanonOrderConsumables.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link byte[]}{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "fileData", scope = CreateCanonOrderConsumables.class)
    public JAXBElement<byte[]> createCreateCanonOrderConsumablesFileData(byte[] value) {
        return new JAXBElement<byte[]>(_CreateCanonOrderConsumablesFileData_QNAME, byte[].class, CreateCanonOrderConsumables.class, ((byte[]) value));
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "country", scope = CreateCanonEmailEnquiries.class)
    public JAXBElement<String> createCreateCanonEmailEnquiriesCountry(String value) {
        return new JAXBElement<String>(_CreateCanonEmailEnquiriesCountry_QNAME, String.class, CreateCanonEmailEnquiries.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "topic", scope = CreateCanonEmailEnquiries.class)
    public JAXBElement<String> createCreateCanonEmailEnquiriesTopic(String value) {
        return new JAXBElement<String>(_CreateCanonEmailEnquiriesTopic_QNAME, String.class, CreateCanonEmailEnquiries.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "subtopic", scope = CreateCanonEmailEnquiries.class)
    public JAXBElement<String> createCreateCanonEmailEnquiriesSubtopic(String value) {
        return new JAXBElement<String>(_CreateCanonEmailEnquiriesSubtopic_QNAME, String.class, CreateCanonEmailEnquiries.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "model", scope = CreateCanonEmailEnquiries.class)
    public JAXBElement<String> createCreateCanonEmailEnquiriesModel(String value) {
        return new JAXBElement<String>(_CreateCanonOrderConsumablesModel_QNAME, String.class, CreateCanonEmailEnquiries.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "serialnumber", scope = CreateCanonEmailEnquiries.class)
    public JAXBElement<String> createCreateCanonEmailEnquiriesSerialnumber(String value) {
        return new JAXBElement<String>(_CreateCanonOnSiteServiceRequestsSerialnumber_QNAME, String.class, CreateCanonEmailEnquiries.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "applyforposition", scope = CreateCanonEmailEnquiries.class)
    public JAXBElement<String> createCreateCanonEmailEnquiriesApplyforposition(String value) {
        return new JAXBElement<String>(_CreateCanonEmailEnquiriesApplyforposition_QNAME, String.class, CreateCanonEmailEnquiries.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "enquiry", scope = CreateCanonEmailEnquiries.class)
    public JAXBElement<String> createCreateCanonEmailEnquiriesEnquiry(String value) {
        return new JAXBElement<String>(_CreateCanonEmailEnquiriesEnquiry_QNAME, String.class, CreateCanonEmailEnquiries.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "salutation", scope = CreateCanonEmailEnquiries.class)
    public JAXBElement<String> createCreateCanonEmailEnquiriesSalutation(String value) {
        return new JAXBElement<String>(_CreateCanonOnSiteServiceRequestsSalutation_QNAME, String.class, CreateCanonEmailEnquiries.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "contactperson", scope = CreateCanonEmailEnquiries.class)
    public JAXBElement<String> createCreateCanonEmailEnquiriesContactperson(String value) {
        return new JAXBElement<String>(_CreateCanonOnSiteServiceRequestsContactperson_QNAME, String.class, CreateCanonEmailEnquiries.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "contactno", scope = CreateCanonEmailEnquiries.class)
    public JAXBElement<String> createCreateCanonEmailEnquiriesContactno(String value) {
        return new JAXBElement<String>(_CreateCanonOnSiteServiceRequestsContactno_QNAME, String.class, CreateCanonEmailEnquiries.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "emailaddress", scope = CreateCanonEmailEnquiries.class)
    public JAXBElement<String> createCreateCanonEmailEnquiriesEmailaddress(String value) {
        return new JAXBElement<String>(_CreateCanonOnSiteServiceRequestsEmailaddress_QNAME, String.class, CreateCanonEmailEnquiries.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "confirmemailaddress", scope = CreateCanonEmailEnquiries.class)
    public JAXBElement<String> createCreateCanonEmailEnquiriesConfirmemailaddress(String value) {
        return new JAXBElement<String>(_CreateCanonOnSiteServiceRequestsConfirmemailaddress_QNAME, String.class, CreateCanonEmailEnquiries.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "companyname", scope = CreateCanonEmailEnquiries.class)
    public JAXBElement<String> createCreateCanonEmailEnquiriesCompanyname(String value) {
        return new JAXBElement<String>(_CreateCanonOnSiteServiceRequestsCompanyname_QNAME, String.class, CreateCanonEmailEnquiries.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "fileName", scope = CreateCanonEmailEnquiries.class)
    public JAXBElement<String> createCreateCanonEmailEnquiriesFileName(String value) {
        return new JAXBElement<String>(_CreateCanonOrderConsumablesFileName_QNAME, String.class, CreateCanonEmailEnquiries.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "fileType", scope = CreateCanonEmailEnquiries.class)
    public JAXBElement<String> createCreateCanonEmailEnquiriesFileType(String value) {
        return new JAXBElement<String>(_CreateCanonOrderConsumablesFileType_QNAME, String.class, CreateCanonEmailEnquiries.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link byte[]}{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "fileData", scope = CreateCanonEmailEnquiries.class)
    public JAXBElement<byte[]> createCreateCanonEmailEnquiriesFileData(byte[] value) {
        return new JAXBElement<byte[]>(_CreateCanonOrderConsumablesFileData_QNAME, byte[].class, CreateCanonEmailEnquiries.class, ((byte[]) value));
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "servicearea", scope = CreateCanonVoiceOfCustomer.class)
    public JAXBElement<String> createCreateCanonVoiceOfCustomerServicearea(String value) {
        return new JAXBElement<String>(_CreateCanonVoiceOfCustomerServicearea_QNAME, String.class, CreateCanonVoiceOfCustomer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "subservicearea", scope = CreateCanonVoiceOfCustomer.class)
    public JAXBElement<String> createCreateCanonVoiceOfCustomerSubservicearea(String value) {
        return new JAXBElement<String>(_CreateCanonVoiceOfCustomerSubservicearea_QNAME, String.class, CreateCanonVoiceOfCustomer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "staff", scope = CreateCanonVoiceOfCustomer.class)
    public JAXBElement<String> createCreateCanonVoiceOfCustomerStaff(String value) {
        return new JAXBElement<String>(_CreateCanonVoiceOfCustomerStaff_QNAME, String.class, CreateCanonVoiceOfCustomer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "keeppostedonarrivaltime", scope = CreateCanonVoiceOfCustomer.class)
    public JAXBElement<String> createCreateCanonVoiceOfCustomerKeeppostedonarrivaltime(String value) {
        return new JAXBElement<String>(_CreateCanonVoiceOfCustomerKeeppostedonarrivaltime_QNAME, String.class, CreateCanonVoiceOfCustomer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "presenceknownuponarrival", scope = CreateCanonVoiceOfCustomer.class)
    public JAXBElement<String> createCreateCanonVoiceOfCustomerPresenceknownuponarrival(String value) {
        return new JAXBElement<String>(_CreateCanonVoiceOfCustomerPresenceknownuponarrival_QNAME, String.class, CreateCanonVoiceOfCustomer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "explainaftertheservice", scope = CreateCanonVoiceOfCustomer.class)
    public JAXBElement<String> createCreateCanonVoiceOfCustomerExplainaftertheservice(String value) {
        return new JAXBElement<String>(_CreateCanonVoiceOfCustomerExplainaftertheservice_QNAME, String.class, CreateCanonVoiceOfCustomer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "speakwellofservice", scope = CreateCanonVoiceOfCustomer.class)
    public JAXBElement<String> createCreateCanonVoiceOfCustomerSpeakwellofservice(String value) {
        return new JAXBElement<String>(_CreateCanonVoiceOfCustomerSpeakwellofservice_QNAME, String.class, CreateCanonVoiceOfCustomer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "whyyousayso", scope = CreateCanonVoiceOfCustomer.class)
    public JAXBElement<String> createCreateCanonVoiceOfCustomerWhyyousayso(String value) {
        return new JAXBElement<String>(_CreateCanonVoiceOfCustomerWhyyousayso_QNAME, String.class, CreateCanonVoiceOfCustomer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "contactperson", scope = CreateCanonVoiceOfCustomer.class)
    public JAXBElement<String> createCreateCanonVoiceOfCustomerContactperson(String value) {
        return new JAXBElement<String>(_CreateCanonOnSiteServiceRequestsContactperson_QNAME, String.class, CreateCanonVoiceOfCustomer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "contactno", scope = CreateCanonVoiceOfCustomer.class)
    public JAXBElement<String> createCreateCanonVoiceOfCustomerContactno(String value) {
        return new JAXBElement<String>(_CreateCanonOnSiteServiceRequestsContactno_QNAME, String.class, CreateCanonVoiceOfCustomer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "emailaddress", scope = CreateCanonVoiceOfCustomer.class)
    public JAXBElement<String> createCreateCanonVoiceOfCustomerEmailaddress(String value) {
        return new JAXBElement<String>(_CreateCanonOnSiteServiceRequestsEmailaddress_QNAME, String.class, CreateCanonVoiceOfCustomer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "confirmemailaddress", scope = CreateCanonVoiceOfCustomer.class)
    public JAXBElement<String> createCreateCanonVoiceOfCustomerConfirmemailaddress(String value) {
        return new JAXBElement<String>(_CreateCanonOnSiteServiceRequestsConfirmemailaddress_QNAME, String.class, CreateCanonVoiceOfCustomer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "companyname", scope = CreateCanonVoiceOfCustomer.class)
    public JAXBElement<String> createCreateCanonVoiceOfCustomerCompanyname(String value) {
        return new JAXBElement<String>(_CreateCanonOnSiteServiceRequestsCompanyname_QNAME, String.class, CreateCanonVoiceOfCustomer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "contactperson", scope = CreateeBPCase.class)
    public JAXBElement<String> createCreateeBPCaseContactperson(String value) {
        return new JAXBElement<String>(_CreateCanonOnSiteServiceRequestsContactperson_QNAME, String.class, CreateeBPCase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "contactno", scope = CreateeBPCase.class)
    public JAXBElement<String> createCreateeBPCaseContactno(String value) {
        return new JAXBElement<String>(_CreateCanonOnSiteServiceRequestsContactno_QNAME, String.class, CreateeBPCase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "emailaddress", scope = CreateeBPCase.class)
    public JAXBElement<String> createCreateeBPCaseEmailaddress(String value) {
        return new JAXBElement<String>(_CreateCanonOnSiteServiceRequestsEmailaddress_QNAME, String.class, CreateeBPCase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "companyname", scope = CreateeBPCase.class)
    public JAXBElement<String> createCreateeBPCaseCompanyname(String value) {
        return new JAXBElement<String>(_CreateCanonOnSiteServiceRequestsCompanyname_QNAME, String.class, CreateeBPCase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "country", scope = CreateeBPCase.class)
    public JAXBElement<String> createCreateeBPCaseCountry(String value) {
        return new JAXBElement<String>(_CreateCanonEmailEnquiriesCountry_QNAME, String.class, CreateeBPCase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "shortdescription", scope = CreateeBPCase.class)
    public JAXBElement<String> createCreateeBPCaseShortdescription(String value) {
        return new JAXBElement<String>(_CreateeBPCaseShortdescription_QNAME, String.class, CreateeBPCase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "detaileddescription", scope = CreateeBPCase.class)
    public JAXBElement<String> createCreateeBPCaseDetaileddescription(String value) {
        return new JAXBElement<String>(_CreateeBPCaseDetaileddescription_QNAME, String.class, CreateeBPCase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "casetype", scope = CreateeBPCase.class)
    public JAXBElement<String> createCreateeBPCaseCasetype(String value) {
        return new JAXBElement<String>(_CreateeBPCaseCasetype_QNAME, String.class, CreateeBPCase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "model", scope = CreateeBPCase.class)
    public JAXBElement<String> createCreateeBPCaseModel(String value) {
        return new JAXBElement<String>(_CreateCanonOrderConsumablesModel_QNAME, String.class, CreateeBPCase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "serialnumber", scope = CreateeBPCase.class)
    public JAXBElement<String> createCreateeBPCaseSerialnumber(String value) {
        return new JAXBElement<String>(_CreateCanonOnSiteServiceRequestsSerialnumber_QNAME, String.class, CreateeBPCase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "fileName", scope = CreateeBPCase.class)
    public JAXBElement<String> createCreateeBPCaseFileName(String value) {
        return new JAXBElement<String>(_CreateCanonOrderConsumablesFileName_QNAME, String.class, CreateeBPCase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "fileType", scope = CreateeBPCase.class)
    public JAXBElement<String> createCreateeBPCaseFileType(String value) {
        return new JAXBElement<String>(_CreateCanonOrderConsumablesFileType_QNAME, String.class, CreateeBPCase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link byte[]}{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "fileData", scope = CreateeBPCase.class)
    public JAXBElement<byte[]> createCreateeBPCaseFileData(byte[] value) {
        return new JAXBElement<byte[]>(_CreateCanonOrderConsumablesFileData_QNAME, byte[].class, CreateeBPCase.class, ((byte[]) value));
    }

}
