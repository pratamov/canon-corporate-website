
package org.tempuri;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="servicearea" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="subservicearea" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="staff" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="keeppostedonarrivaltime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="presenceknownuponarrival" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="explainaftertheservice" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="speakwellofservice" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="whyyousayso" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="contactperson" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="contactno" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="emailaddress" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="confirmemailaddress" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="companyname" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "servicearea",
    "subservicearea",
    "staff",
    "keeppostedonarrivaltime",
    "presenceknownuponarrival",
    "explainaftertheservice",
    "speakwellofservice",
    "whyyousayso",
    "contactperson",
    "contactno",
    "emailaddress",
    "confirmemailaddress",
    "companyname"
})
@XmlRootElement(name = "CreateCanonVoiceOfCustomer")
public class CreateCanonVoiceOfCustomer {

    @XmlElementRef(name = "servicearea", namespace = "http://tempuri.org/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> servicearea;
    @XmlElementRef(name = "subservicearea", namespace = "http://tempuri.org/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> subservicearea;
    @XmlElementRef(name = "staff", namespace = "http://tempuri.org/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> staff;
    @XmlElementRef(name = "keeppostedonarrivaltime", namespace = "http://tempuri.org/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> keeppostedonarrivaltime;
    @XmlElementRef(name = "presenceknownuponarrival", namespace = "http://tempuri.org/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> presenceknownuponarrival;
    @XmlElementRef(name = "explainaftertheservice", namespace = "http://tempuri.org/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> explainaftertheservice;
    @XmlElementRef(name = "speakwellofservice", namespace = "http://tempuri.org/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> speakwellofservice;
    @XmlElementRef(name = "whyyousayso", namespace = "http://tempuri.org/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> whyyousayso;
    @XmlElementRef(name = "contactperson", namespace = "http://tempuri.org/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> contactperson;
    @XmlElementRef(name = "contactno", namespace = "http://tempuri.org/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> contactno;
    @XmlElementRef(name = "emailaddress", namespace = "http://tempuri.org/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> emailaddress;
    @XmlElementRef(name = "confirmemailaddress", namespace = "http://tempuri.org/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> confirmemailaddress;
    @XmlElementRef(name = "companyname", namespace = "http://tempuri.org/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> companyname;

    /**
     * Gets the value of the servicearea property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getServicearea() {
        return servicearea;
    }

    /**
     * Sets the value of the servicearea property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setServicearea(JAXBElement<String> value) {
        this.servicearea = value;
    }

    /**
     * Gets the value of the subservicearea property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSubservicearea() {
        return subservicearea;
    }

    /**
     * Sets the value of the subservicearea property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSubservicearea(JAXBElement<String> value) {
        this.subservicearea = value;
    }

    /**
     * Gets the value of the staff property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getStaff() {
        return staff;
    }

    /**
     * Sets the value of the staff property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setStaff(JAXBElement<String> value) {
        this.staff = value;
    }

    /**
     * Gets the value of the keeppostedonarrivaltime property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getKeeppostedonarrivaltime() {
        return keeppostedonarrivaltime;
    }

    /**
     * Sets the value of the keeppostedonarrivaltime property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setKeeppostedonarrivaltime(JAXBElement<String> value) {
        this.keeppostedonarrivaltime = value;
    }

    /**
     * Gets the value of the presenceknownuponarrival property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPresenceknownuponarrival() {
        return presenceknownuponarrival;
    }

    /**
     * Sets the value of the presenceknownuponarrival property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPresenceknownuponarrival(JAXBElement<String> value) {
        this.presenceknownuponarrival = value;
    }

    /**
     * Gets the value of the explainaftertheservice property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getExplainaftertheservice() {
        return explainaftertheservice;
    }

    /**
     * Sets the value of the explainaftertheservice property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setExplainaftertheservice(JAXBElement<String> value) {
        this.explainaftertheservice = value;
    }

    /**
     * Gets the value of the speakwellofservice property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSpeakwellofservice() {
        return speakwellofservice;
    }

    /**
     * Sets the value of the speakwellofservice property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSpeakwellofservice(JAXBElement<String> value) {
        this.speakwellofservice = value;
    }

    /**
     * Gets the value of the whyyousayso property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getWhyyousayso() {
        return whyyousayso;
    }

    /**
     * Sets the value of the whyyousayso property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setWhyyousayso(JAXBElement<String> value) {
        this.whyyousayso = value;
    }

    /**
     * Gets the value of the contactperson property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getContactperson() {
        return contactperson;
    }

    /**
     * Sets the value of the contactperson property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setContactperson(JAXBElement<String> value) {
        this.contactperson = value;
    }

    /**
     * Gets the value of the contactno property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getContactno() {
        return contactno;
    }

    /**
     * Sets the value of the contactno property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setContactno(JAXBElement<String> value) {
        this.contactno = value;
    }

    /**
     * Gets the value of the emailaddress property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getEmailaddress() {
        return emailaddress;
    }

    /**
     * Sets the value of the emailaddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setEmailaddress(JAXBElement<String> value) {
        this.emailaddress = value;
    }

    /**
     * Gets the value of the confirmemailaddress property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getConfirmemailaddress() {
        return confirmemailaddress;
    }

    /**
     * Sets the value of the confirmemailaddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setConfirmemailaddress(JAXBElement<String> value) {
        this.confirmemailaddress = value;
    }

    /**
     * Gets the value of the companyname property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCompanyname() {
        return companyname;
    }

    /**
     * Sets the value of the companyname property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCompanyname(JAXBElement<String> value) {
        this.companyname = value;
    }

}
