
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CreateCanonEmailEnquiriesResult" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "createCanonEmailEnquiriesResult"
})
@XmlRootElement(name = "CreateCanonEmailEnquiriesResponse")
public class CreateCanonEmailEnquiriesResponse {

    @XmlElement(name = "CreateCanonEmailEnquiriesResult")
    protected Integer createCanonEmailEnquiriesResult;

    /**
     * Gets the value of the createCanonEmailEnquiriesResult property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCreateCanonEmailEnquiriesResult() {
        return createCanonEmailEnquiriesResult;
    }

    /**
     * Sets the value of the createCanonEmailEnquiriesResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCreateCanonEmailEnquiriesResult(Integer value) {
        this.createCanonEmailEnquiriesResult = value;
    }

}
