
package org.tempuri;

/**
 * Please modify this class to meet your needs
 * This class is not complete
 */

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.FaultAction;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

/**
 * This class was generated by Apache CXF 3.2.1
 * 2017-12-31T14:37:43.176+07:00
 * Generated source version: 3.2.1
 * 
 */
public final class ICanonDataService_BasicHttpBindingICanonDataService_Client {

    private static final QName SERVICE_NAME = new QName("http://tempuri.org/", "CanonDataService");

    private ICanonDataService_BasicHttpBindingICanonDataService_Client() {
    }

    public static void main(String args[]) throws Exception {
        URL wsdlURL = CanonDataService.WSDL_LOCATION;
        if (args.length > 0 && args[0] != null && !"".equals(args[0])) { 
            File wsdlFile = new File(args[0]);
            try {
                if (wsdlFile.exists()) {
                    wsdlURL = wsdlFile.toURI().toURL();
                } else {
                    wsdlURL = new URL(args[0]);
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }
      
        CanonDataService ss = new CanonDataService(wsdlURL, SERVICE_NAME);
        ICanonDataService port = ss.getBasicHttpBindingICanonDataService();  
        
        {
        System.out.println("Invoking createCanonOrderConsumables...");
        String _createCanonOrderConsumables_typeofconsumables = "";
        String _createCanonOrderConsumables_model = "";
        String _createCanonOrderConsumables_serialnumber = "";
        String _createCanonOrderConsumables_quantity = "";
        String _createCanonOrderConsumables_otherinstructions = "";
        String _createCanonOrderConsumables_avoidlunchtime = "";
        String _createCanonOrderConsumables_salutation = "";
        String _createCanonOrderConsumables_contactperson = "";
        String _createCanonOrderConsumables_contactno = "";
        String _createCanonOrderConsumables_emailaddress = "";
        String _createCanonOrderConsumables_confirmemailaddress = "";
        String _createCanonOrderConsumables_companyname = "";
        String _createCanonOrderConsumables_fileName = "";
        String _createCanonOrderConsumables_fileType = "";
        byte[] _createCanonOrderConsumables_fileData = new byte[0];
        Integer _createCanonOrderConsumables__return = port.createCanonOrderConsumables(_createCanonOrderConsumables_typeofconsumables, _createCanonOrderConsumables_model, _createCanonOrderConsumables_serialnumber, _createCanonOrderConsumables_quantity, _createCanonOrderConsumables_otherinstructions, _createCanonOrderConsumables_avoidlunchtime, _createCanonOrderConsumables_salutation, _createCanonOrderConsumables_contactperson, _createCanonOrderConsumables_contactno, _createCanonOrderConsumables_emailaddress, _createCanonOrderConsumables_confirmemailaddress, _createCanonOrderConsumables_companyname, _createCanonOrderConsumables_fileName, _createCanonOrderConsumables_fileType, _createCanonOrderConsumables_fileData);
        System.out.println("createCanonOrderConsumables.result=" + _createCanonOrderConsumables__return);


        }
        {
        System.out.println("Invoking createCanonEmailEnquiries...");
        String _createCanonEmailEnquiries_country = "";
        String _createCanonEmailEnquiries_topic = "";
        String _createCanonEmailEnquiries_subtopic = "";
        String _createCanonEmailEnquiries_model = "";
        String _createCanonEmailEnquiries_serialnumber = "";
        String _createCanonEmailEnquiries_applyforposition = "";
        String _createCanonEmailEnquiries_enquiry = "";
        String _createCanonEmailEnquiries_salutation = "";
        String _createCanonEmailEnquiries_contactperson = "";
        String _createCanonEmailEnquiries_contactno = "";
        String _createCanonEmailEnquiries_emailaddress = "";
        String _createCanonEmailEnquiries_confirmemailaddress = "";
        String _createCanonEmailEnquiries_companyname = "";
        String _createCanonEmailEnquiries_fileName = "";
        String _createCanonEmailEnquiries_fileType = "";
        byte[] _createCanonEmailEnquiries_fileData = new byte[0];
        Integer _createCanonEmailEnquiries__return = port.createCanonEmailEnquiries(_createCanonEmailEnquiries_country, _createCanonEmailEnquiries_topic, _createCanonEmailEnquiries_subtopic, _createCanonEmailEnquiries_model, _createCanonEmailEnquiries_serialnumber, _createCanonEmailEnquiries_applyforposition, _createCanonEmailEnquiries_enquiry, _createCanonEmailEnquiries_salutation, _createCanonEmailEnquiries_contactperson, _createCanonEmailEnquiries_contactno, _createCanonEmailEnquiries_emailaddress, _createCanonEmailEnquiries_confirmemailaddress, _createCanonEmailEnquiries_companyname, _createCanonEmailEnquiries_fileName, _createCanonEmailEnquiries_fileType, _createCanonEmailEnquiries_fileData);
        System.out.println("createCanonEmailEnquiries.result=" + _createCanonEmailEnquiries__return);


        }
        {
        System.out.println("Invoking createCanonOnSiteServiceRequests...");
        String _createCanonOnSiteServiceRequests_serialnumber = "";
        String _createCanonOnSiteServiceRequests_faultydescription = "";
        String _createCanonOnSiteServiceRequests_avoidlunchtime = "";
        String _createCanonOnSiteServiceRequests_salutation = "";
        String _createCanonOnSiteServiceRequests_contactperson = "";
        String _createCanonOnSiteServiceRequests_contactno = "";
        String _createCanonOnSiteServiceRequests_emailaddress = "";
        String _createCanonOnSiteServiceRequests_confirmemailaddress = "";
        String _createCanonOnSiteServiceRequests_companyname = "";
        Integer _createCanonOnSiteServiceRequests__return = port.createCanonOnSiteServiceRequests(_createCanonOnSiteServiceRequests_serialnumber, _createCanonOnSiteServiceRequests_faultydescription, _createCanonOnSiteServiceRequests_avoidlunchtime, _createCanonOnSiteServiceRequests_salutation, _createCanonOnSiteServiceRequests_contactperson, _createCanonOnSiteServiceRequests_contactno, _createCanonOnSiteServiceRequests_emailaddress, _createCanonOnSiteServiceRequests_confirmemailaddress, _createCanonOnSiteServiceRequests_companyname);
        System.out.println("createCanonOnSiteServiceRequests.result=" + _createCanonOnSiteServiceRequests__return);


        }
        {
        System.out.println("Invoking createeBPCase...");
        String _createeBPCase_contactperson = "";
        String _createeBPCase_contactno = "";
        String _createeBPCase_emailaddress = "";
        String _createeBPCase_companyname = "";
        String _createeBPCase_country = "";
        String _createeBPCase_shortdescription = "";
        String _createeBPCase_detaileddescription = "";
        String _createeBPCase_casetype = "";
        String _createeBPCase_model = "";
        String _createeBPCase_serialnumber = "";
        String _createeBPCase_fileName = "";
        String _createeBPCase_fileType = "";
        byte[] _createeBPCase_fileData = new byte[0];
        try {
            Integer _createeBPCase__return = port.createeBPCase(_createeBPCase_contactperson, _createeBPCase_contactno, _createeBPCase_emailaddress, _createeBPCase_companyname, _createeBPCase_country, _createeBPCase_shortdescription, _createeBPCase_detaileddescription, _createeBPCase_casetype, _createeBPCase_model, _createeBPCase_serialnumber, _createeBPCase_fileName, _createeBPCase_fileType, _createeBPCase_fileData);
            System.out.println("createeBPCase.result=" + _createeBPCase__return);

        } catch (ICanonDataServiceCreateeBPCaseStringFaultFaultMessage e) { 
            System.out.println("Expected exception: ICanonDataService_CreateeBPCase_StringFault_FaultMessage has occurred.");
            System.out.println(e.toString());
        }
            }
        {
        System.out.println("Invoking createCanonVoiceOfCustomer...");
        String _createCanonVoiceOfCustomer_servicearea = "";
        String _createCanonVoiceOfCustomer_subservicearea = "";
        String _createCanonVoiceOfCustomer_staff = "";
        String _createCanonVoiceOfCustomer_keeppostedonarrivaltime = "";
        String _createCanonVoiceOfCustomer_presenceknownuponarrival = "";
        String _createCanonVoiceOfCustomer_explainaftertheservice = "";
        String _createCanonVoiceOfCustomer_speakwellofservice = "";
        String _createCanonVoiceOfCustomer_whyyousayso = "";
        String _createCanonVoiceOfCustomer_contactperson = "";
        String _createCanonVoiceOfCustomer_contactno = "";
        String _createCanonVoiceOfCustomer_emailaddress = "";
        String _createCanonVoiceOfCustomer_confirmemailaddress = "";
        String _createCanonVoiceOfCustomer_companyname = "";
        Integer _createCanonVoiceOfCustomer__return = port.createCanonVoiceOfCustomer(_createCanonVoiceOfCustomer_servicearea, _createCanonVoiceOfCustomer_subservicearea, _createCanonVoiceOfCustomer_staff, _createCanonVoiceOfCustomer_keeppostedonarrivaltime, _createCanonVoiceOfCustomer_presenceknownuponarrival, _createCanonVoiceOfCustomer_explainaftertheservice, _createCanonVoiceOfCustomer_speakwellofservice, _createCanonVoiceOfCustomer_whyyousayso, _createCanonVoiceOfCustomer_contactperson, _createCanonVoiceOfCustomer_contactno, _createCanonVoiceOfCustomer_emailaddress, _createCanonVoiceOfCustomer_confirmemailaddress, _createCanonVoiceOfCustomer_companyname);
        System.out.println("createCanonVoiceOfCustomer.result=" + _createCanonVoiceOfCustomer__return);


        }

        System.exit(0);
    }

}
