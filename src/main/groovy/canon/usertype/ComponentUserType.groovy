package canon.usertype

import canon.enums.ComponentType
import canon.model.Component
import groovy.transform.CompileStatic
import org.hibernate.HibernateException
import org.hibernate.engine.spi.SessionImplementor
import org.hibernate.type.IntegerType
import org.hibernate.type.StringType
import org.hibernate.type.Type
import org.hibernate.usertype.CompositeUserType

import java.sql.PreparedStatement
import java.sql.ResultSet
import java.sql.SQLException
import java.sql.Types

/**
 * Created by marjan on 16/12/17.
 */
@CompileStatic
class ComponentUserType implements CompositeUserType {

    private static final String[] PROPERTY_NAMES = ['compType', 'compContent', 'compOrder']
    private static final Type[] PROPERTY_TYPES = [StringType.INSTANCE, StringType.INSTANCE, IntegerType.INSTANCE] as Type[]

    String[] getPropertyNames() {
        PROPERTY_NAMES
    }

    Type[] getPropertyTypes() {
        PROPERTY_TYPES
    }

    Object getPropertyValue(Object component, int propertyIndex) {
        if (component == null) {
            return null
        }

        final Component c = (Component) component
        switch (propertyIndex) {
            case 0:
                return c.compType
            case 1:
                return c.compContent
            case 2:
                return c.compOrder
            default:
                throw new HibernateException("Invalid property index [${propertyIndex}]")
        }
    }

    void setPropertyValue(Object component, int propertyIndex, Object value) throws HibernateException {
        // Component is an inmutable type and it's properties cannot be modified
        throw new UnsupportedOperationException()
    }

    Object nullSafeGet(ResultSet rs, String[] names, SessionImplementor session, Object owner) throws SQLException {
        assert names.length == 3
        String cType = rs.getString(names[0])
        String content = rs.getString(names[1])
        Integer order = rs.getInt(names[2])
        ComponentType c = (ComponentType) (cType ? ComponentType.valueOf(cType) : null)

        if (cType == null) {
            return null
        }

        new Component(c, content, order)
    }

    void nullSafeSet(PreparedStatement st, Object value, int index, SessionImplementor session) throws SQLException {
        if (value) {
            final Component c = (Component) value
            st.setString(index, c.compType.name())
            st.setString(index + 1, c.compContent)
            st.setInt(index + 2, c.compOrder)
        } else {
            st.setString(index, null)
            st.setString(index + 1, null)
            st.setNull(index + 2, Types.INTEGER)
        }
    }

    boolean equals(Object x, Object y) {
        x == y
    }

    Class returnedClass() {
        Component.class
    }

    Object assemble(Serializable cached, SessionImplementor session, Object owner) {
        cached
    }

    Object deepCopy(Object o) {
        //((Component) o)?.clone()
        if(o != null) {
            Component newComponent = (Component) o
            return new Component(newComponent.getCompType(), newComponent.getCompContent(), newComponent.getCompOrder())
        }
        return null
    }

    Serializable disassemble(Object value, SessionImplementor session) {
        (Serializable) value
    }

    int hashCode(Object o) {
        o.hashCode()
    }

    boolean isMutable() {
        false
    }

    Object replace(Object original, Object target, SessionImplementor session, Object owner) {
        original
    }
}
