package canon.model.comp

import groovy.json.JsonOutput

class SliderComp {

    String title
    List<Map<String, Object>> images

    SliderComp(String title, List<Map> images) {
        this.title = title
        this.images = images
    }

    SliderComp(Map<String, Object> object) {
        this.title = object.get("title")

        this.images = []
        List<Map> medias = object.get("images") as List<Map>
        medias?.each {Map map ->
            String mediaIdStr = map.get("mediaId")
            Long mediaId = mediaIdStr && mediaIdStr.isLong() ? Long.valueOf(mediaIdStr) : null
            this.images.add([mediaId: mediaId, mediaUrl: map.get("mediaUrl")])
        }
    }

    String toJsonString() {
        return JsonOutput.toJson([
                title: title,
                images: images
        ])
    }
}
