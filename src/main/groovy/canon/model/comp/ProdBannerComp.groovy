package canon.model.comp

import groovy.json.JsonOutput

/**
 * Created by marjan on 16/12/17.
 */
class ProdBannerComp {

    ProdBannerComp(Long id, String bgUrl, Long bgId, String desc) {
        this.id = id
        this.desc = desc
        this.bgUrl = bgUrl
        this.bgId = bgId
    }

    ProdBannerComp(Map<String, Object> object) {
        String prodId = object.get("id")
        this.id = prodId && prodId.isLong() ? Long.valueOf(prodId) : null
        this.desc = object.get("desc") as String
        this.bgUrl = object.get("bgUrl") as String
        String bId = object.get("bgId")
        this.bgId = bId && bId.isLong() ? Long.valueOf(bId) : null
    }

    Long id
    String desc
    String bgUrl
    Long bgId

    String toJsonString() {
        return JsonOutput.toJson([
                id: id,
                desc: desc,
                bgUrl: bgUrl,
                bgId: bgId
        ])
    }
}
