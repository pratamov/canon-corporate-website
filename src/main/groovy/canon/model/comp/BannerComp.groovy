package canon.model.comp

import canon.Banner
import groovy.json.JsonOutput

/**
 * Created by marjan on 02/01/18.
 */
class BannerComp {

    BannerComp(Long id) {
        this.id = id
    }

    BannerComp(Map<String, Object> object) {
        String id = object.get("id")
        this.id = id && id.isLong() ? Long.valueOf(id) : null
    }

    Long id
    Banner banner

    String toJsonString() {
        return JsonOutput.toJson([id: id])
    }
}
