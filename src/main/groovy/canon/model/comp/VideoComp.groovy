package canon.model.comp

import groovy.json.JsonOutput

/**
 * Created by marjan on 16/12/17.
 */
class VideoComp {

    VideoComp(String caption, String textPosition, String thumbnail, String videoUrl, Long mediaId) {
        this.caption = caption
        this.thumbnail = thumbnail
        this.videoUrl = videoUrl
        this.mediaId = mediaId
    }

    VideoComp(Map<String, Object> object) {
        this.caption = object.get("caption") as String
        this.thumbnail = object.get("thumbnail") as String
        this.videoUrl = object.get("videoUrl") as String

        String medId = object.get("mediaId")
        this.mediaId = medId && medId.isLong() ? Long.valueOf(medId) : null
    }

    String caption
    String thumbnail
    String videoUrl
    Long mediaId

    String toJsonString() {
        return JsonOutput.toJson([caption: caption, thumbnail: thumbnail, videoUrl: videoUrl, mediaId: mediaId])
    }
}
