package canon.model.comp

import canon.Product
import groovy.json.JsonOutput

class FeaturedProductsComp {

    String title
    List<Product> featuredList

    FeaturedProductsComp(String title) {
        this.title = title
        this.featuredList = new ArrayList<>()
    }

    FeaturedProductsComp(Map<String, Object> object) {
        this.title = object.get("title") as String
        this.featuredList = new ArrayList<>()
    }

    String toJsonString() {
        return JsonOutput.toJson([title: title])
    }
}
