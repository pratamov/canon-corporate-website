package canon.model.comp

import groovy.json.JsonOutput

/**
 * Created by huda on 27/12/17.
 */
class ProdImageMediumComp {

    String subtitle
    String description
    List<Map<String, Object>> images

    ProdImageMediumComp(String subtitle, String description, List<Map> images) {
        this.subtitle = subtitle
        this.description = description
        this.images = images
    }

    ProdImageMediumComp(Map<String, Object> object) {
        this.subtitle = object.get("subtitle") as String
        this.description = object.get("description") as String
        
        this.images = []
        List<Map> medias = object.get("images") as List<Map>
        medias?.each {Map map ->
            String mediaIdStr = map.get("mediaId")
            Long mediaId = mediaIdStr && mediaIdStr.isLong() ? Long.valueOf(mediaIdStr) : null
            this.images.add([mediaId: mediaId, mediaUrl: map.get("mediaUrl")])
        }
    }

    String toJsonString() {
        return JsonOutput.toJson([subtitle: subtitle, description: description, images: images])
    }
}
