package canon.model.comp

import canon.enums.MediaSource
import groovy.json.JsonOutput
import org.apache.commons.lang.StringUtils

/**
 * Created by marjan on 16/12/17.
 */
class ImageComp {

    ImageComp(String alt, String caption, String copyright, String imageUrl, Long mediaId, MediaSource source, String title) {
        this.alt = alt
        this.caption = caption
        this.copyright = copyright
        this.imageUrl = imageUrl
        this.mediaId = mediaId
        this.source = source
        this.title = title
    }

    ImageComp(Map<String, Object> object) {
        this.alt = object.get("alt") as String
        this.caption = object.get("caption") as String
        this.copyright = object.get("copyright") as String
        this.imageUrl = object.get("imageUrl") as String

        String medId = object.get("mediaId")
        this.mediaId = medId && medId.isLong() ? Long.valueOf(medId) : null

        try {
            String source = object.get("source")
            if (StringUtils.isNotBlank(source)) {
                this.source = MediaSource.valueOf(source)
            }
        } catch (Exception e) {}

        this.title = object.get("title") as String
    }

    String alt
    String caption
    String copyright
    String imageUrl
    Long mediaId
    MediaSource source
    String title

    String toJsonString() {
        return JsonOutput.toJson([
                alt: alt,
                caption: caption,
                copyright: copyright,
                imageUrl: imageUrl,
                mediaId: mediaId,
                source: source?.name(),
                title: title
        ])
    }
}
