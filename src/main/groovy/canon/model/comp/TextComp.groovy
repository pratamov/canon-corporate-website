package canon.model.comp

import groovy.json.JsonOutput

/**
 * Created by marjan on 16/12/17.
 */
class TextComp {

    TextComp(String content) {
        this.content = content
    }

    TextComp(Map<String, Object> object) {
        this.content = object.get("content") ?: ""
    }

    String content

    String toJsonString() {
        return JsonOutput.toJson([content: content])
    }
}
