package canon.model.comp

import canon.Category
import canon.Page
import canon.enums.ContentType
import groovy.json.JsonOutput
import org.apache.commons.lang.StringUtils

class FeaturedArticlesComp {

    String title
    Boolean hideTitle = Boolean.FALSE
    ContentType contentType
    Boolean featuredOnly = Boolean.TRUE
    Boolean hideViewAll = Boolean.FALSE
    Long categoryId

    Category category
    List<Page> featuredList

    FeaturedArticlesComp(String title, Boolean hideTitle, ContentType contentType, Boolean featuredOnly, Boolean hideViewAll, Long categoryId) {
        this.title = title
        this.hideTitle = hideTitle
        this.contentType = contentType
        this.featuredOnly = featuredOnly
        this.hideViewAll = hideViewAll
        this.categoryId = categoryId
        this.featuredList = new ArrayList<>()
    }

    FeaturedArticlesComp(Map<String, Object> object) {
        this.title = object.get("title") as String
        this.hideTitle = object.get("hideTitle") as Boolean
        try {
            String cType = object.get("contentType")
            if (StringUtils.isNotBlank(cType)) this.contentType = ContentType.valueOf(cType?.toUpperCase())
        } catch (Exception e) {}
        this.featuredOnly = object.get("featuredOnly") as Boolean
        this.hideViewAll = object.get("hideViewAll") as Boolean

        String catId = object.get("categoryId")
        this.categoryId = catId && catId.isLong() ? Long.valueOf(catId) : null
        this.featuredList = new ArrayList<>()
    }

    String toJsonString() {
        return JsonOutput.toJson([
                title: title,
                hideTitle: hideTitle,
                contentType: contentType.name(),
                featuredOnly: featuredOnly,
                hideViewAll: hideViewAll,
                categoryId: categoryId
        ])
    }
}
