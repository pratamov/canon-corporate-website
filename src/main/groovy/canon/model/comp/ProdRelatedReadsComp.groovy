package canon.model.comp

import groovy.json.JsonOutput

class ProdRelatedReadsComp {

    Long id
    List<Map> articles

    ProdRelatedReadsComp(Long id, List<Map> articles) {
        this.id = id
        this.articles = articles
    }

    ProdRelatedReadsComp(Map<String, Object> objectMap) {
        String idStr = objectMap.get("id")
        this.id = idStr && idStr.isLong() ? Long.valueOf(idStr) : null

        this.articles = []
        List<Map> articles = objectMap.get("articles")
        articles?.each {Map map ->
            String articleIdStr = map.get("id")
            Long articleId = articleIdStr && articleIdStr.isLong() ? Long.valueOf(articleIdStr) : null
            String slug = map.get("slug")
            String tagIdStr = map.get("tagId")
            Long tagId = tagIdStr && tagIdStr.isLong() ? Long.valueOf(tagIdStr) : null
            String tagName = map.get("tagName")
            String title = map.get("title")
            String bgIdStr = map.get("bgId")
            Long bgId = bgIdStr && bgIdStr.isLong() ? Long.valueOf(bgIdStr) : null
            String bgUrl = map.get("bgUrl")

            this.articles.add([
                    articleId: articleId,
                    articleSlug: slug,
                    articleTagId: tagId,
                    articleTagName: tagName,
                    articleTitle: title,
                    articleBgId: bgId,
                    articleBgUrl: bgUrl
            ])
        }
    }

    String toJsonString() {
        return JsonOutput.toJson([
                id: this.id
        ])
    }
}
