package canon.model.comp

import canon.Feature
import groovy.json.JsonOutput

class ProdFeaturesComp {

    Long id
    List<Long> featuresId
    List<Feature> featureList

    ProdFeaturesComp(Long id) {
        this.id = id
        this.featuresId = new ArrayList<>()
        this.featureList = new ArrayList<>()
    }

    ProdFeaturesComp(Map<String, Object> object){
        String prodId = object.get("id")
        this.id = prodId && prodId.isLong() ? Long.valueOf(prodId) : null

        List<Long> features = new ArrayList<>()
        List<Map> featureMap = object.get("features") as List<Map>
        featureMap?.each { Map map ->
            String featureIdStr = map.get("id")
            Long featureId = featureIdStr && featureIdStr.isLong() ? Long.valueOf(featureIdStr) : null
            features.add(featureId)
        }
        this.featuresId = features
        this.featureList = new ArrayList<>()
    }

    String toJsonString() {
        List<Map> features = []
        featuresId.each {
            features.add([id: it])
        }
        return JsonOutput.toJson([
                id: id,
                features: features
        ])
    }
}
