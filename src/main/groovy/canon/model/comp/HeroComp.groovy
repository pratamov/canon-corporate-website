package canon.model.comp

import canon.enums.MediaSource
import groovy.json.JsonOutput
import org.apache.commons.lang.StringUtils

/**
 * Created by marjan on 16/12/17.
 */
class HeroComp {

    HeroComp(String copyright, String imageUrl, Long mediaId, MediaSource source) {
        this.copyright = copyright
        this.imageUrl = imageUrl
        this.mediaId = mediaId
        this.source = source
    }

    HeroComp(String copyright, String imageUrl, Long mediaId, Float rectangleXPosition, Float squareXPosition, MediaSource source) {
        this.copyright = copyright
        this.imageUrl = imageUrl
        this.mediaId = mediaId
        this.rectangleXPosition = rectangleXPosition
        this.squareXPosition = squareXPosition
        this.source = source
    }

    HeroComp(Map<String, Object> object) {
        this.copyright = object.get("copyright") as String
        this.imageUrl = object.get("imageUrl") as String

        String medId = object.get("mediaId")
        this.mediaId = medId && medId.isLong() ? Long.parseLong(medId) : null

        try {
            String rectangleXPosition = object.get("rectangleXPosition")
            this.rectangleXPosition = rectangleXPosition && rectangleXPosition.isFloat() ? Float.parseFloat(rectangleXPosition) : null
        } catch (Exception e) {}

        try {
            String squareXPosition = object.get("squareXPosition")
            this.squareXPosition = squareXPosition && squareXPosition.isFloat() ? Float.parseFloat(squareXPosition) : null
        } catch (Exception e) {}

        try {
            String source = object.get("source")
            if (StringUtils.isNotBlank(source)) {
                this.source = MediaSource.valueOf(source)
            }
        } catch (Exception e) {}
    }

    String copyright
    String imageUrl
    Long mediaId
    Float rectangleXPosition
    Float squareXPosition
    MediaSource source

    String toJsonString() {
        return JsonOutput.toJson([
                copyright: copyright,
                imageUrl: imageUrl,
                mediaId: mediaId,
                rectangleXPosition: rectangleXPosition,
                squareXPosition: squareXPosition,
                source: source?.name()
        ])
    }
}
