package canon.model.comp

import canon.enums.RelationType
import groovy.json.JsonOutput

class ProdRelationComp {

    Long id
    RelationType relationType
    List<Map> relatedProducts
    Integer countRelatedProducts

    ProdRelationComp(Long id, RelationType relationType) {
        this.id = id
        this.relationType = relationType
    }

    ProdRelationComp(Map<String, Object> objectMap) {
        String idStr = objectMap.get("id")
        this.id = idStr && idStr.isLong() ? Long.valueOf(idStr) : null
        try {
            this.relationType = RelationType.valueOf(objectMap.get("relationType") as String) } catch (Exception e) {}
        try {
            String c = objectMap.get("countRelatedProduct")
            this.countRelatedProducts = c && c.isInteger() ? Integer.valueOf(c) : 0
        } catch (Exception e) {}
        this.relatedProducts = []
        objectMap.get("relatedProducts")?.each {Map map ->
            String relatedProductIdStr = map.get("id")
            Long relatedProductId = relatedProductIdStr && relatedProductIdStr.isLong() ? Long.valueOf(relatedProductIdStr) : null
            String relatedProductName = map.get("name")
            String relatedProductSlug = map.get("slug")
            String relatedProductMediaId = map.get("mediaId")
            String relatedProductMediaUrl = map.get("mediaUrl")

            this.relatedProducts.add([
                    id: relatedProductId,
                    name: relatedProductName,
                    slug: relatedProductSlug,
                    mediaId: relatedProductMediaId,
                    mediaUrl: relatedProductMediaUrl
            ])
        }
    }

    String toJsonString() {
        return JsonOutput.toJson([
                id: this.id,
                relationType: this.relationType
        ])
    }
}
