package canon.model.comp

import groovy.json.JsonOutput

/**
 * Created by marjan on 16/12/17.
 */
class ProdDescComp {

    ProdDescComp(Long id, String newSubText, String newDesc) {
        this.id = id
        this.newSubText = newSubText
        this.newDesc = newDesc
    }

    ProdDescComp(Map<String, Object> object) {
        String prodId = object.get("id")
        this.id = prodId && prodId.isLong() ? Long.valueOf(prodId) : null

        this.newSubText = object.get("newSubText") as String
        this.newDesc = object.get("newDesc") as String
    }

    Long id
    String newSubText
    String newDesc

    String toJsonString() {
        return JsonOutput.toJson([id: id, newSubText: newSubText, newDesc: newDesc])
    }
}
