package canon.model.comp

import groovy.json.JsonOutput

class ProdImageComp {

    Long id

    ProdImageComp(Long id) {
        this.id = id
    }

    ProdImageComp(Map<String, Object> object) {
        String prodId = object.get("id")
        this.id = prodId && prodId.isLong() ? Long.valueOf(prodId) : null
    }

    String toJsonString() {
        return JsonOutput.toJson([
                id: id
        ])
    }
}
