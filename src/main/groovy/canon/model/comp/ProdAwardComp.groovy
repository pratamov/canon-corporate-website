package canon.model.comp

import groovy.json.JsonOutput

class ProdAwardComp {

    Long id

    ProdAwardComp(Long id, List<Map<String, Object>> awards) {
        this.id = id
    }

    ProdAwardComp(Map<String, Object> object) {
        String prodId = object.get("id")
        this.id = prodId && prodId.isLong() ? Long.valueOf(prodId) : null
    }

    String toJsonString() {
        return JsonOutput.toJson([
                id: id
        ])
    }
}
