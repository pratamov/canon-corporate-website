package canon.model.comp

import groovy.json.JsonOutput

class InformationCardComp {

    String sectionTitle
    String cardTitle
    Long mediaId
    String mediaUrl
    String desc
    String url
    String buttonLabel

    InformationCardComp(String sectionTitle,String cardTitle, Long mediaId, String mediaUrl, String desc, String url, String buttonLabel) {
        this.sectionTitle = sectionTitle
        this.cardTitle = cardTitle
        this.mediaId = mediaId
        this.mediaUrl = mediaUrl
        this.desc = desc
        this.url = url
        this.buttonLabel = buttonLabel
    }

    InformationCardComp(Map<String, Object> object) {
        this.sectionTitle = object.get("sectionTitle")
        this.cardTitle = object.get("cardTitle")
        this.mediaId = object.get("mediaId")
        this.mediaUrl = object.get("mediaUrl")
        this.desc = object.get("desc")
        this.url = object.get("url")
        this.buttonLabel = object.get("buttonLabel")

    }

    String toJsonString() {
        return JsonOutput.toJson([
                sectionTitle: this.sectionTitle,
                cardTitle: this.cardTitle,
                mediaId: this.mediaId,
                mediaUrl: this.mediaUrl,
                desc: this.desc,
                url: this.url,
                buttonLabel: this.buttonLabel
        ])
    }
}
