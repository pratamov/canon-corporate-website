package canon.model.comp

import groovy.json.JsonOutput


class ProdPromotionComp {

    Long id
    String desc

    ProdPromotionComp(Long id, String desc) {
        this.id = id
        this.desc = desc
    }

    ProdPromotionComp(Map<String, Object> object){
        String prodId = object.get("id")
        this.id = prodId && prodId.isLong() ? Long.valueOf(prodId) : null
        this.desc = object.get("desc") as String
    }

    String toJsonString() {
        return JsonOutput.toJson([
                id: id,
                desc: desc
        ])
    }
}
