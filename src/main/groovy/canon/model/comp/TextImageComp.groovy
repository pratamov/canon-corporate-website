package canon.model.comp

import canon.enums.MediaSource
import groovy.json.JsonOutput
import org.apache.commons.lang.StringUtils

/**
 * Created by marjan on 16/12/17.
 */
class TextImageComp {

    TextImageComp(String alt, String content, String copyright, String imagePosition, String imageUrl, Long mediaId, MediaSource source) {
        this.alt = alt
        this.content = content
        this.copyright = copyright
        this.imagePosition = imagePosition
        this.imageUrl = imageUrl
        this.mediaId = mediaId
        this.source = source
    }

    TextImageComp(Map<String, Object> object) {
        this.alt = object.get("alt") as String
        this.content = object.get("content") as String
        this.copyright = object.get("copyright") as String
        this.imagePosition = object.get("imagePosition") as String
        this.imageUrl = object.get("imageUrl") as String

        String medId = object.get("mediaId")
        this.mediaId = medId && medId.isLong() ? Long.valueOf(medId) : null

        try {
            String source = object.get("source")
            if (StringUtils.isNotBlank(source)) {
                this.source = MediaSource.valueOf(source)
            }
        } catch (Exception e) {}
    }

    String alt
    String content
    String copyright
    String imagePosition
    String imageUrl
    Long mediaId
    MediaSource source

    String toJsonString() {
        return JsonOutput.toJson([
                alt: alt,
                content: content,
                copyright: copyright,
                imagePosition: imagePosition,
                imageUrl: imageUrl,
                mediaId: mediaId,
                source: source?.name()
        ])
    }
}
