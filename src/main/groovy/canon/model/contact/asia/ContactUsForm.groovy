package canon.model.contact.asia

import grails.validation.Validateable

/**
 * Created by rifai on 14/1/18.
 */
class ContactUsForm implements Serializable, Validateable {


    String typeOfEnquiry;
    String specificEnquiry;
    String salutation;
    String name;
    String emailAddress;
    String retypeEmailAddress;
    String contactNo;
    String countryOfResidence;
    String message;

    static constraints = {
        typeOfEnquiry(blank:false, maxSize: 255)
        specificEnquiry(blank:false, maxSize: 255)
        salutation(blank:false, maxSize: 5)
        name(blank:false, maxSize: 255)
        emailAddress(email:true, blank:false)
        retypeEmailAddress(email:true, blank:false)
        contactNo(blank:false, minSize: 8, maxSize: 20)
        countryOfResidence(blank: false)
        message(blank: false, minSize: 10, maxSize: 4096)
    }

}
