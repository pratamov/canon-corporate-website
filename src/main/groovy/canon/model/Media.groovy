package canon.model

import canon.enums.MediaSource
import canon.enums.MediaType
import org.apache.commons.lang.StringUtils

/**
 * Created by marjan on 11/01/18.
 */
class Media {

    Media() {
        this.alt = null
        this.copyright = null
        this.url = null
        this.mediaId = null
        this.type = null
        this.source = null
    }

    Media(String copyright, String url, Long mediaId, MediaType type, MediaSource source) {
        this.copyright = copyright
        this.url = url
        this.mediaId = mediaId
        this.type = type
        this.source = source
    }

    Media(Map<String, Object> object) {
        this.alt = object.get("alt")
        this.copyright = object.get("copyright")
        this.url = object.get("url")

        String mediaId = object.get("mediaId")
        this.mediaId = mediaId && mediaId.isLong() ? Long.parseLong(mediaId) : null
        try {
            String type = object.get("type")
            if (StringUtils.isNotBlank(type)) this.type = MediaType.valueOf(type?.toUpperCase())
        } catch (Exception e) {}
        try {
            String source = object.get("source")
            if (StringUtils.isNotBlank(source)) this.source = MediaSource.valueOf(source?.toUpperCase())
        } catch (Exception e) {}
    }

    String alt
    String copyright
    String url
    Long mediaId
    MediaType type
    MediaSource source

    static constraints = {
        alt nullable: true, blank: true
        copyright nullable: true, blank: true
        url nullable: true, blank: true, maxSize: 2048
        mediaId nullable: true
        type nullable: true
        source nullable: true
    }
}
