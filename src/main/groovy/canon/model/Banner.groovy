package canon.model

import canon.enums.MediaSource
import grails.validation.Validateable
import org.apache.commons.lang.StringUtils

/**
 * Created by marjan on 16/12/17.
 */
class Banner implements Serializable, Validateable {

    Banner() {
        this.copyright = null
        this.url = null
        this.mediaId = null
        this.source = null
        this.featuredX = null
        this.featuredLargeX = null
        this.featuredMediumX = null
        this.featuredSmallX = null
        this.searchX = null
    }

    Banner(String copyright, String url, Long mediaId, MediaSource source,
           Float featuredX, Float featuredLargeX, Float featuredMediumX, Float featuredSmallX, Float searchX) {
        this.copyright = copyright
        this.url = url
        this.mediaId = mediaId
        this.source = source
        this.featuredX = featuredX
        this.featuredLargeX = featuredLargeX
        this.featuredMediumX = featuredMediumX
        this.featuredSmallX = featuredSmallX
        this.searchX = searchX
    }

    Banner(Map<String, Object> object) {
        this.copyright = object.get("copyright")
        this.url = object.get("url")

        String mediaId = object.get("mediaId")
        this.mediaId = mediaId && mediaId.isLong() ? Long.parseLong(mediaId) : null
        try {
            String source = object.get("source")
            if (StringUtils.isNotBlank(source)) this.source = MediaSource.valueOf(source?.toUpperCase())
        } catch (Exception e) {}

        this.featuredX = getFloatValue(object.get("featuredX"))
        this.featuredLargeX = getFloatValue(object.get("featuredLargeX"))
        this.featuredMediumX = getFloatValue(object.get("featuredMediumX"))
        this.featuredSmallX = getFloatValue(object.get("featuredSmallX"))
        this.searchX = getFloatValue(object.get("searchX"))
    }

    String copyright
    String url
    Long mediaId
    MediaSource source
    Float featuredX
    Float featuredLargeX
    Float featuredMediumX
    Float featuredSmallX
    Float searchX

    static constraints = {
        copyright nullable: true, blank: true
        url nullable: true, blank: true
        mediaId nullable: true
        source nullable: true
        featuredX nullable: true
        featuredLargeX nullable: true
        featuredMediumX nullable: true
        featuredSmallX nullable: true
        searchX nullable: true
    }

    private static Float getFloatValue(Object input) {
        try {
            String s = (String) input
            if (StringUtils.isBlank(s)) return null
            s = s.trim()
            return s.isFloat() ? Float.parseFloat(s) : null
        } catch (Exception e) {}
        return null
    }
}
