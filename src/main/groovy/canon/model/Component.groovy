package canon.model

import canon.enums.ComponentType
import groovy.json.JsonSlurper
import org.apache.commons.lang.WordUtils

/**
 * Created by marjan on 16/12/17.
 */
class Component implements Serializable {

    Component(ComponentType compType, String compContent, Integer compOrder) {
        this.compType = compType
        this.compContent = compContent ?: "{}"
        this.compOrder = compOrder
    }

    Component(ComponentType compType, Map<String, Object> object, Integer compOrder) {
        this.compType = compType
        this.compOrder = compOrder

        String content = "{}"
        try {
            String cName = getInstanceName()
            Object o = this.getClass().getClassLoader().loadClass(cName)?.newInstance(object)
            content = o?.toJsonString() ?: "{}"
        } catch (Exception e) {
            e.printStackTrace()
        }
        this.compContent = content
    }

    Object comp
    ComponentType compType
    String compContent
    Integer compOrder

    def getAsComponent() {
        if (comp != null) {
            return comp
        }

        def object = getAsJsonObject()
        if (object instanceof Map) {
            try {
                String cName = getInstanceName()
                comp = this.getClass().getClassLoader().loadClass(cName)?.newInstance(object)
            } catch (Exception e) {
                e.printStackTrace()
            }
        } else if (object instanceof List) {
            // do noting for now
        }
        return comp
    }

    def getAsJsonObject() {
        def jsonSlurper = new JsonSlurper()
        try {
            return jsonSlurper.parseText(compContent ?: "{}")
        } catch (Exception e) {
            e.printStackTrace()
        }
        return null
    }

    private String getInstanceName() {
        String cName = compType.name()
        switch (compType) {
            case ComponentType.QUOTE:
                cName = ComponentType.TEXT.name()
                break
            case ComponentType.TEXT_LEFT: case ComponentType.TEXT_RIGHT:
                cName = "Text Image"
                break
            case ComponentType.FEATURED_ARTICLES_SOLUTION:
                cName = ComponentType.FEATURED_ARTICLES.name()
                break
        }
        cName = cName.replace("_", " ")
        cName = WordUtils.capitalizeFully(cName)
        cName = "canon.model.comp." + cName.replace(" ", "") + "Comp"
        return cName
    }
}
