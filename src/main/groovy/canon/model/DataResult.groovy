package canon.model

class DataResult {

    Boolean success
    String status
    String message
    List<?> errors
    Object data

    DataResult() {}

    DataResult(Boolean success, String message) {
        this.success = success
        this.message = message
    }

    DataResult(Boolean success, Object data) {
        this.success = success
        this.data = data
    }

    DataResult(Boolean success, String status, String message) {
        this.success = success
        this.status = status
        this.message = message
    }

    DataResult(Boolean success, String status, String message, List<?> errors, Object data) {
        this.success = success
        this.status = status
        this.message = message
        this.errors = errors
        this.data = data
    }

    HashMap<?, ?> toMap() {
        return [
                success: this.success,
                timestamp: new Date(),
                status: this.status,
                message: this.message,
                errors: this.errors,
                data: this.data
        ]
    }
}
