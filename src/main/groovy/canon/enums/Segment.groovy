package canon.enums

enum Segment {

    CONSUMER("Consumer", "consumer"),
    BUSINESS("Business", "business")

    private static Map<String, Segment> slugsMap = new HashMap<String, Segment>()
    static {
        for (Segment myEnum : values()) {
            slugsMap.put(myEnum.slug, myEnum)
        }
    }

    private final String value
    private final String slug

    Segment(String value, String slug) {
        this.value = value
        this.slug = slug
    }

    String value() { value }
    String slug() { slug }

    static Segment getBySlug( String slug ) {
        slugsMap.get(slug)
    }
}