package canon.enums

/**
 * Created by marjan on 27-Mar-16.
 */
enum ContentPrefix {

    NONE("None"),
    VIDEO("Video")

    private final String value

    ContentPrefix(String value) {
        this.value = value
    }

    String value() { value }
}