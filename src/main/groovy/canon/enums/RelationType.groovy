package canon.enums

/**
 * Created by marjan on 12/12/17.
 */
enum RelationType {
    PRODUCT, ACCESSORIES, CONSUMABLE
}