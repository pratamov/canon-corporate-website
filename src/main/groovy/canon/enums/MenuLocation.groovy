package canon.enums

enum MenuLocation {
    HEADER_RIGHT, HEADER_LEFT, MOBILE
}