package canon.enums

enum BannerType {
    PRODUCT_DEALS,
    PRODUCT_PRESENTATION
}