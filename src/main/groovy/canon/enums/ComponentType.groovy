package canon.enums

/**
 * Created by marjan on 07-Mar-16.
 */
enum ComponentType {

    BANNER("Banner/Slider"),
    CHECKLIST("Checklist"),
    FEATURED_ARTICLES("Featured Articles"),
    FEATURED_PRODUCTS("Featured Products"),
    HERO("Hero Image"),
    IMAGE("Image with Caption"),
    INFORMATION_CARD("Information Card"),
    LINE("Horizontal Line"),
    PROD_AWARD("Product Award"),
    PROD_BANNER("Product Banner"),
    PROD_DESC("Product Description"),
    PROD_FEATURES("Product Features"),
    PROD_IMAGE("Product Image"),
    PROD_IMAGE_MEDIUM("Product Image Medium"),
    PROD_PROMOTION("Product Promotion"),
    PROD_RELATED_READS("Product Related Reads"),
    PROD_RELATION("Product Relation"),
    QUOTE("Quote"),
    SLIDER('Slider'),
    TABLE("Table"),
    TEXT("Text Block"),
    TEXT_LEFT("Text Left - Image Right"),
    TEXT_RIGHT("Text Right - Image Left"),
    VIDEO("Video"),
    FEATURED_ARTICLES_SOLUTION("Featured Articles Solution")

    private final String value

    ComponentType(String value) {
        this.value = value
    }

    String value() { value }
}