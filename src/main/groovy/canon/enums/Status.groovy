package canon.enums

/**
 * Created by marjan on 06-Mar-16.
 */
enum Status {
    ACTIVE, APPROVED, ARCHIVED, DELETED, DRAFT,
    INACTIVE, PENDING, PUBLISHED, RELEASE, SHADOW
}