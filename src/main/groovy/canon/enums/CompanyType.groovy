package canon.enums

/**
 * Created by marjan on 11/01/18.
 */
enum CompanyType {
    DEALER, DISTRIBUTOR, GENERAL
}