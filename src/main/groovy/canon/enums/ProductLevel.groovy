package canon.enums

/**
 * Created by marjan on 14/12/17.
 */
enum ProductLevel {
    BEGINNER, SEMI_PROFESSIONAL, PROFESSIONAL
}