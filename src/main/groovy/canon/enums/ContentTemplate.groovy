package canon.enums

/**
 * Created by marjan on 18-Mar-16.
 */
enum ContentTemplate {

    PAGE("Page"),
    ARTICLE("Article"),
    INFOGRAPHICS("Infographics"),
    EDITORIAL("Editorial"),
    EVENT("Event"),
    GENERIC("Generic"),
    SHOPPING("Shopping")

    private final String value

    ContentTemplate(String value) {
        this.value = value
    }

    String value() { value }

}