package canon.enums

/**
 * Created by marjan on 11/01/18.
 */
enum DocumentType {
    BROCHURE, DOCUMENT
}