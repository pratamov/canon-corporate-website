package canon.enums

/**
 * Created by marjan on 05-Mar-16.
 */
enum ContentType {
    ARTICLE, COURSE, EVENT, PAGE, SOLUTION
}