package canon.enums

/**
 * Created by marjan on 06-Mar-16.
 */
enum CategoryType {
    ARTICLE, EVENT, COURSE, PRODUCT, SOLUTION
}