package canon.enums

/**
 * Created by marjan on 05-Mar-16.
 */
enum ContentSetting {

    NONE("None"),
    SPONSORED("Sponsored Content")

    private final String value

    ContentSetting(String value) {
        this.value = value
    }

    String value() { value }
}