package canon.enums

/**
 * Created by marjan on 17/01/18.
 */
enum LookupType {
    DA_FOLDER, TAG, TITLE
}