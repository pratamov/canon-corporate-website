package canon.enums

/**
 * Created by marjan on 05-Mar-16.
 */
enum ParamType {

    COUNTRY("country"), LANGUAGE("lang"), SITE("site"),
    CATEGORY("category"), SLUG("id"),
    UNKNOWN("unknown")

    private final String value

    ParamType(String value) {
        this.value = value
    }

    String getValue() {
        value
    }
}