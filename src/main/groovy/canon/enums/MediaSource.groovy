package canon.enums

/**
 * Created by marjan on 06-Mar-16.
 */
enum MediaSource {
    INTERNAL, EXTERNAL
}