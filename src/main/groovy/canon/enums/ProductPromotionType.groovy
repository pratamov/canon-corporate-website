package canon.enums

/**
 * Created by marjan on 06-Mar-16.
 */
enum ProductPromotionType {

    PERCENTAGE("Percentage"),
    AMOUNT("Amount")

    private final String value

    ProductPromotionType(String value) {
        this.value = value
    }

    String value() { value }
}