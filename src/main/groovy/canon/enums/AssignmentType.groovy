package canon.enums

/**
 * Created by marjan on 13/12/17.
 */
enum AssignmentType {
    USER, GROUP
}