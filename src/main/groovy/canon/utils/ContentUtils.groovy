package canon.utils

import org.apache.commons.lang.StringUtils
import org.jsoup.Jsoup
import org.jsoup.nodes.Document

import java.text.BreakIterator

/**
 * Created by marjan on 10/4/17.
 */
class ContentUtils {

    static truncate = { String str, Integer max = 150 ->
        if (StringUtils.isBlank(str)) {
            return ""
        }

        str = str?.trim()
        if (str.size() <= max) {
            return str
        }
        return cut(cleanUpTag(str), max)
    }

    static cleanUp = { String str ->
        if (StringUtils.isBlank(str)) {
            return ""
        }
        return cleanUpTag(str)
    }

    static daySuffix = { Integer n ->
        if (n >= 11 && n <= 13) {
            return "th"
        }
        switch (n % 10) {
            case 1:  return "st"
            case 2:  return "nd"
            case 3:  return "rd"
            default: return "th"
        }
    }

    private static String cleanUpTag(String str) {
        Document doc = Jsoup.parse(prepare(str))
        return doc?.text()
    }

    private static String prepare(String str) {
        SanitizingUtils sanitizingUtils = new SanitizingUtils()
        return Jsoup.clean(str, sanitizingUtils.addProtocols("img","src","data"))
    }

    private static cut(String str, Integer max) {
        BreakIterator bi = BreakIterator.getWordInstance()
        bi.setText(str)
        try {
            int firstAfter = bi.following(max);
            if(firstAfter == BreakIterator.DONE) {
                return str;
            }
            return str.substring(0, firstAfter) + "...";
        } catch (IllegalArgumentException illEx) {

        }
        return str;
    }
}
