package canon.utils

import canon.enums.MediaType
import org.apache.commons.lang.StringUtils

/**
 * Created by marjan on 10/4/17.
 */
class MediaUtils {

    static forType = { String name ->
        if (StringUtils.isBlank(name)) {
            return null
        }
        return checkType(name)
    }

    private static MediaType checkType(String name) {
        String mimeType = URLConnection.guessContentTypeFromName(name)
        if (mimeType?.startsWith("image/")) {
            return MediaType.IMAGE
        } else if (mimeType?.startsWith("video/")) {
            return MediaType.VIDEO
        }
        return MediaType.DOCUMENT
    }
}
