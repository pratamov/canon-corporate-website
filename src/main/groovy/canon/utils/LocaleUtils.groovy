package canon.utils
/**
 * Created by marjan on 29/12/17.
 */
class LocaleUtils {

    static getLanguage = { Locale locale ->
        if (!locale) return null
        return toLang(locale)
    }

    private static String toLang(Locale locale) {
        if (locale.baseLocale) {
            return locale.baseLocale.language + (locale.baseLocale.region ? "_" + locale.baseLocale.region : "")
        } else {
            return locale.language
        }
    }
}
