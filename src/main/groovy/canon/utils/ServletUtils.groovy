package canon.utils

import org.apache.commons.validator.routines.InetAddressValidator


/**
 * Created by rifai on 16/1/18.
 */
class ServletUtils {

    public static String getIpAddress(javax.servlet.http.HttpServletRequest request) {

        String ipAddress = request.getRemoteAddr();
        println("request.getRemoteAddr() : " + ipAddress);

        if (ipAddress && InetAddressValidator.VALIDATOR.isValid(ipAddress)){
            println("ipAddress : " + ipAddress);
            return ipAddress
        }

        ipAddress = request.getHeader("X-Forwarded-For")

        if(ipAddress && InetAddressValidator.VALIDATOR.isValid(ipAddress)) {
            println("request.getHeader(\"X-Forwarded-For\") : " + ipAddress);
            return ipAddress
        }

        ipAddress = request.getHeader("Client-IP")

        if(ipAddress && InetAddressValidator.VALIDATOR.isValid(ipAddress)) {
            println("request.getHeader(\"Client-IP\") : " + ipAddress);
            return ipAddress
        }

        return ipAddress

    }

}
