<html>
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title><g:message code="public.login.title" default="Login - Canon"/></title>

    <asset:stylesheet src="font-awesome/css/font-awesome.css" />
    <asset:stylesheet src="css/main.css" />
    <asset:stylesheet src="css/custom.css" />
</head>

<body class="gray-bg">

<div class="middle-box text-center loginscreen animated fadeInDown">
    <div>
        <h3><g:message code="admin.login.welcomeNote.label" default="Welcome to Canon"/></h3>
        <g:if test='${flash.message}'>
            <div class="login_message">${flash.message}</div>
        </g:if>
        <form class="m-t" role="form" action="${postUrl ?: createLink(url:'/login/authenticate')}" autocomplete="off" method="POST">
            <div class="form-group">
                <input type="text" class="text_ form-control" name="${usernameParameter ?: 'username'}" placeholder="${message(code: 'springSecurity.login.username.label')}" id="username" required=""/>
            </div>
            <div class="form-group">
                <input type="password" autocomplete="off" class="text_ form-control" name="${passwordParameter ?: 'password'}" placeholder="${message(code: 'springSecurity.login.password.label')}" id="password" required=""/>
            </div>
            %{--<div class="form-group">
                <input type="checkbox" class="text_ chk" name="${rememberMeParameter ?: 'remember-me'}" id="remember_me" <g:if test='${hasCookie}'>checked="checked"</g:if>/>
                <label for="remember_me"><g:message code='default.login.remember.me.label'/></label>
            </div>--}%
            <button type="submit" class="btn btn-primary block full-width m-b">${message(code: 'springSecurity.login.button')}</button>
        </form>
    </div>
</div>
    <asset:javascript src="js/jquery-3.1.1.min.js"/>
    <asset:javascript src="js/main.js"/>
    <asset:javascript src="js/custom.js"/>
</body>
</html>