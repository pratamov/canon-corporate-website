<%--
  Created by IntelliJ IDEA.
  User: muyalif
  Date: 26/12/17
  Time: 15.04
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title></title>
    <meta name="layout" content="canon"/>
</head>

<body>
<!-- Start Wrap Component -->
<div class="wrap-component">
    <!-- Start Content -->
    <div class="wrap-banner">
        <img src="images/promo.png" alt="">
    </div>
    <!-- End Content -->

    <!-- Start Content -->
    <div class="section">
        <div class="container">
            <div class="row">
                <div class="col-md-3 sidebar">
                    <div class="content">
                        <!-- Start Widget -->
                        <div class="widget" id="sort" data-toggle=".toggle-sort">
                            <a class="close" href="#"><span class="fa fa-times"></span></a>
                            <h6 class="title">Sort By</h6>
                            <select class="form-control select input-sm margin-bottom20 margin-top20">
                                <option value="relevance">Relevance</option>
                                <option value="most viewed">Most Viewed</option>
                                <option value="az" selected>A-Z</option>
                                <option value="za" selected>Z-A</option>
                                <option value="hight to low">Price hight to low</option>
                                <option value="low to hight">Price low to hight</option>
                            </select>
                        </div>
                        <!-- End Widget -->
                        <!-- Start Widget -->
                        <div class="widget" id="filter" data-toggle=".toggle-filter">
                            <a class="close" href="#"><span class="fa fa-times"></span></a>
                            <h6 class="title">Filter by</h6>
                            <p class="sub-title">Catalogue</p>
                            <ul class="clear-list margin-bottom30" id="catalogue">
                                <li>
                                    <div class="checkbox">
                                        <input id="checkbox1" type="checkbox">
                                        <label for="checkbox1">
                                            2018 All-Year-Round
                                        </label>
                                    </div>
                                </li>
                                <li>
                                    <div class="checkbox">
                                        <input id="checkbox2" type="checkbox">
                                        <label for="checkbox2">
                                            Chinese New Year 2018
                                        </label>
                                    </div>
                                </li>
                                <li>
                                    <div class="checkbox">
                                        <input id="checkbox3" type="checkbox">
                                        <label for="checkbox3">
                                            Merry Christmas 2017
                                        </label>
                                    </div>
                                </li>
                                <li>
                                    <div class="checkbox">
                                        <input id="checkbox4" type="checkbox">
                                        <label for="checkbox4">
                                            Black Friday 2017
                                        </label>
                                    </div>
                                </li>
                            </ul>

                            <p class="sub-title">Category</p>
                            <ul class="clear-list margin-bottom30">
                                <!-- Start Dropdown -->
                                <li class="dropdown-list">
                                    <div class="checkbox main">
                                        <input id="checkbox6" type="checkbox">
                                        <label for="checkbox6">
                                            Photography
                                        </label>
                                    </div>
                                    <!-- Start Dropdown Content -->
                                    <ul class="clear-list dropdown-content">
                                        <li>
                                            <div class="checkbox">
                                                <input id="checkboxd1" type="checkbox">
                                                <label for="checkboxd1">
                                                    Lorem Ipsum
                                                </label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="checkbox">
                                                <input id="checkboxd2" type="checkbox">
                                                <label for="checkboxd2">
                                                    Sit amet consect
                                                </label>
                                            </div>
                                        </li>
                                    </ul>
                                    <!-- End Dropdown Content -->
                                </li>
                                <!-- End Dropdown -->

                                <!-- Start Dropdown -->
                                <li class="dropdown-list">
                                    <div class="checkbox main">
                                        <input id="checkbox7" type="checkbox">
                                        <label for="checkbox7">
                                            Printing
                                        </label>
                                    </div>
                                    <!-- Start Dropdown Content -->
                                    <ul class="clear-list dropdown-content">
                                        <li>
                                            <div class="checkbox">
                                                <input id="checkboxd3" type="checkbox">
                                                <label for="checkboxd3">
                                                    Single Function
                                                </label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="checkbox">
                                                <input id="checkboxd4" type="checkbox">
                                                <label for="checkboxd4">
                                                    Multi-Function
                                                </label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="checkbox">
                                                <input id="checkboxd5" type="checkbox">
                                                <label for="checkboxd5">
                                                    SELPHY
                                                </label>
                                            </div>
                                        </li>
                                    </ul>
                                    <!-- End Dropdown Content -->
                                </li>
                                <!-- End Dropdown -->

                                <!-- Start Dropdown -->
                                <li class="dropdown-list">
                                    <div class="checkbox main">
                                        <input id="checkbox8" type="checkbox">
                                        <label for="checkbox8">
                                            Scanning
                                        </label>
                                    </div>
                                    <!-- Start Dropdown Content -->
                                    <ul class="clear-list dropdown-content">
                                        <li>
                                            <div class="checkbox">
                                                <input id="checkboxd6" type="checkbox">
                                                <label for="checkboxd6">
                                                    Test One
                                                </label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="checkbox">
                                                <input id="checkboxd7" type="checkbox">
                                                <label for="checkboxd7">
                                                    Test Two
                                                </label>
                                            </div>
                                        </li>
                                    </ul>
                                    <!-- End Dropdown Content -->
                                </li>
                                <!-- End Dropdown -->

                                <!-- Start Dropdown -->
                                <li class="dropdown-list">
                                    <div class="checkbox main">
                                        <input id="checkbox9" type="checkbox">
                                        <label for="checkbox9">
                                            Videography
                                        </label>
                                    </div>
                                    <!-- Start Dropdown Content -->
                                    <ul class="clear-list dropdown-content">
                                        <li>
                                            <div class="checkbox">
                                                <input id="checkboxd8" type="checkbox">
                                                <label for="checkboxd8">
                                                    Test One
                                                </label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="checkbox">
                                                <input id="checkboxd9" type="checkbox">
                                                <label for="checkboxd9">
                                                    Test Two
                                                </label>
                                            </div>
                                        </li>
                                    </ul>
                                    <!-- End Dropdown Content -->
                                </li>
                                <!-- End Dropdown -->

                                <!-- Start Dropdown -->
                                <li class="dropdown-list">
                                    <div class="checkbox main">
                                        <input id="checkbox10" type="checkbox">
                                        <label for="checkbox10">
                                            Presentation
                                        </label>
                                    </div>
                                    <!-- Start Dropdown Content -->
                                    <ul class="clear-list dropdown-content">
                                        <li>
                                            <div class="checkbox">
                                                <input id="checkboxd10" type="checkbox">
                                                <label for="checkboxd10">
                                                    Test One
                                                </label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="checkbox">
                                                <input id="checkboxd11" type="checkbox">
                                                <label for="checkboxd11">
                                                    Test Two
                                                </label>
                                            </div>
                                        </li>
                                    </ul>
                                    <!-- End Dropdown Content -->
                                </li>
                                <!-- End Dropdown -->

                                <!-- Start Dropdown -->
                                <li class="dropdown-list">
                                    <div class="checkbox main">
                                        <input id="checkbox110" type="checkbox">
                                        <label for="checkbox110">
                                            Others
                                        </label>
                                    </div>
                                    <!-- Start Dropdown Content -->
                                    <ul class="clear-list dropdown-content">
                                        <li>
                                            <div class="checkbox">
                                                <input id="checkboxd12" type="checkbox">
                                                <label for="checkboxd12">
                                                    Test One
                                                </label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="checkbox">
                                                <input id="checkboxd13" type="checkbox">
                                                <label for="checkboxd13">
                                                    Test Two
                                                </label>
                                            </div>
                                        </li>
                                    </ul>
                                    <!-- End Dropdown Content -->
                                </li>
                                <!-- End Dropdown -->
                            </ul>

                            <p class="sub-title">Price</p>
                            <ul class="clear-list toggle-more" id="price">
                                <li>
                                    <div class="checkbox">
                                        <input id="checkbox61" type="checkbox">
                                        <label for="checkbox61">
                                            $0 — $1000
                                        </label>
                                    </div>
                                </li>
                                <li>
                                    <div class="checkbox">
                                        <input id="checkbox71" type="checkbox">
                                        <label for="checkbox71">
                                            $1001 — $2000
                                        </label>
                                    </div>
                                </li>
                                <li>
                                    <div class="checkbox">
                                        <input id="checkbox81" type="checkbox">
                                        <label for="checkbox81">
                                            $2001 — $3000
                                        </label>
                                    </div>
                                </li>
                                <li>
                                    <div class="checkbox">
                                        <input id="checkbox91" type="checkbox">
                                        <label for="checkbox91">
                                            $3001 — $4000
                                        </label>
                                    </div>
                                </li>
                                <li>
                                    <div class="checkbox">
                                        <input id="checkbox101" type="checkbox">
                                        <label for="checkbox101">
                                            $4001 — $5000
                                        </label>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <!-- End Widget -->
                    </div>
                </div>
                <div class="col-md-9 main">
                    <div class="content">
                        <!-- Start Btn Toggle -->
                        <div class="btn-filter">
                            <a href="#" class="btn btn-default btn-sm toggle-filter">
                                Filter By
                            </a>
                            <a href="#" class="btn btn-default btn-sm toggle-sort">
                                Sort By
                            </a>
                        </div>
                        <!-- End Btn Toggle -->

                        <!-- Start Row -->
                        <div class="row">
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <!-- Start Post Item -->
                                <div class="post-item match-height border">
                                    <div class="wrap-image">
                                        <a href="#"><img src="https://canon-corpweb-dev.s3.amazonaws.com/images/category-sub/interchangeable-lens-cameras.png" alt="" /></a>
                                    </div>
                                    <div class="content">
                                        <div class="equal-height">
                                            <h5 class="title">
                                                <a href="#">PIXMA MG3670</a>
                                            </h5>
                                            <span class="badge primary small relative">Free Gift</span>
                                            <p class="margin-bottom0">
                                                PG-740 Black Ink worth $25.50
                                            </p>
                                        </div>
                                        <hr />
                                        <h4 class="price">$139</h4>
                                    </div>
                                </div>
                                <!-- End Post Item -->
                            </div>
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <!-- Start Post Item -->
                                <div class="post-item match-height border">
                                    <div class="wrap-image">
                                        <a href="#"><img src="https://canon-corpweb-dev.s3.amazonaws.com/images/category-sub/interchangeable-lens-cameras.png" alt="" /></a>
                                    </div>
                                    <div class="content">
                                        <div class="equal-height">
                                            <h5 class="title">
                                                <a href="#">PIXMA TS5170</a>
                                            </h5>
                                            <span class="badge primary small relative">Free Gift</span>
                                            <p class="margin-bottom0">
                                                Photo paper kit worth $22.90<br />
                                                Targus Bag
                                            </p>
                                        </div>
                                        <hr />
                                        <h4 class="price">$189</h4>
                                    </div>
                                </div>
                                <!-- End Post Item -->
                            </div>
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <!-- Start Post Item -->
                                <div class="post-item match-height border">
                                    <div class="wrap-image">
                                        <a href="#"><img src="https://canon-corpweb-dev.s3.amazonaws.com/images/category-sub/interchangeable-lens-cameras.png" alt="" /></a>
                                    </div>
                                    <div class="content">
                                        <div class="equal-height">
                                            <h5 class="title">
                                                <a href="#">PIXMA MG5770</a>
                                            </h5>
                                            <span class="badge primary small relative">Free Gift</span>
                                            <p class="margin-bottom0">
                                                Targus Bag
                                            </p>
                                        </div>
                                        <hr />
                                        <h4 class="price">$169</h4>
                                    </div>
                                </div>
                                <!-- End Post Item -->
                            </div>
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <!-- Start Post Item -->
                                <div class="post-item match-height border">
                                    <div class="wrap-image">
                                        <a href="#"><img src="https://canon-corpweb-dev.s3.amazonaws.com/images/category-sub/interchangeable-lens-cameras.png" alt="" /></a>
                                    </div>
                                    <div class="content">
                                        <div class="equal-height">
                                            <h5 class="title">
                                                <a href="#">PIXMA TS5070</a>
                                            </h5>
                                            <span class="badge primary small relative">Free Gift</span>
                                            <p class="margin-bottom0">
                                                Photo paper kit worth $22.90<br />
                                                Targus Bag
                                            </p>
                                        </div>
                                        <hr />
                                        <div class="row text-center">
                                            <div class="col-xs-6"><h4 class="price"><span>$199</span></h4></div>
                                            <div class="col-xs-6"><h4 class="price on">$169</h4></div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Post Item -->
                            </div>
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <!-- Start Post Item -->
                                <div class="post-item match-height border">
                                    <div class="wrap-image">
                                        <a href="#"><img src="https://canon-corpweb-dev.s3.amazonaws.com/images/category-sub/interchangeable-lens-cameras.png" alt="" /></a>
                                    </div>
                                    <div class="content">
                                        <div class="equal-height">
                                            <h5 class="title">
                                                <a href="#">PIXMA TS8070</a>
                                            </h5>
                                            <span class="badge primary small relative">Free Gift</span>
                                            <p class="margin-bottom0">
                                                Photo paper kit worth $22.90<br />
                                                Targus Bag
                                            </p>
                                        </div>
                                        <hr />
                                        <div class="row text-center">
                                            <div class="col-xs-6"><h4 class="price"><span>$299</span></h4></div>
                                            <div class="col-xs-6"><h4 class="price on">$249</h4></div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Post Item -->
                            </div>
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <!-- Start Post Item -->
                                <div class="post-item match-height border">
                                    <div class="wrap-image">
                                        <a href="#"><img src="https://canon-corpweb-dev.s3.amazonaws.com/images/category-sub/interchangeable-lens-cameras.png" alt="" /></a>
                                    </div>
                                    <div class="content">
                                        <div class="equal-height">
                                            <h5 class="title">
                                                <a href="#">PIXMA TS8170</a>
                                            </h5>
                                            <span class="badge primary small relative">Free Gift</span>
                                            <p class="margin-bottom0">
                                                Photo paper kit worth $22.90<br />
                                                Targus Bag
                                            </p>
                                        </div>
                                        <hr />
                                        <h4 class="price">$299</h4>
                                    </div>
                                </div>
                                <!-- End Post Item -->
                            </div>
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <!-- Start Post Item -->
                                <div class="post-item match-height border">
                                    <img src="https://canon-corpweb-dev.s3.amazonaws.com/images/category-sub/interchangeable-lens-cameras.png" alt="" />
                                    <div class="content">
                                        <div class="equal-height">
                                            <h5 class="title">
                                                <a href="#">PIXMA G2000</a>
                                            </h5>
                                            <span class="badge primary small relative">Free Gift</span>
                                            <p class="margin-bottom0">
                                                Targus Bag
                                            </p>
                                        </div>
                                        <hr />
                                        <h4 class="price">$239</h4>
                                    </div>
                                </div>
                                <!-- End Post Item -->
                            </div>
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <!-- Start Post Item -->
                                <div class="post-item match-height border">
                                    <img src="https://canon-corpweb-dev.s3.amazonaws.com/images/category-sub/interchangeable-lens-cameras.png" alt="" />
                                    <div class="content">
                                        <div class="equal-height">
                                            <h5 class="title">
                                                <a href="#">PIXMA G3000</a>
                                            </h5>
                                            <span class="badge primary small relative">Free Gift</span>
                                            <p class="margin-bottom0">
                                                Targus Bag
                                            </p>
                                        </div>
                                        <hr />
                                        <h4 class="price">$339</h4>
                                    </div>
                                </div>
                                <!-- End Post Item -->
                            </div>
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <!-- Start Post Item -->
                                <div class="post-item match-height border">
                                    <img src="https://canon-corpweb-dev.s3.amazonaws.com/images/category-sub/interchangeable-lens-cameras.png" alt="" />
                                    <div class="content">
                                        <div class="equal-height">
                                            <h5 class="title">
                                                <a href="#">PIXMA G4000</a>
                                            </h5>
                                            <span class="badge primary small relative">Free Gift</span>
                                            <p class="margin-bottom0">
                                                Targus Bag
                                            </p>
                                        </div>
                                        <hr />
                                        <h4 class="price">$389</h4>
                                    </div>
                                </div>
                                <!-- End Post Item -->
                            </div>
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <!-- Start Post Item -->
                                <div class="post-item match-height border">
                                    <img src="https://canon-corpweb-dev.s3.amazonaws.com/images/category-sub/interchangeable-lens-cameras.png" alt="" />
                                    <div class="content">
                                        <div class="equal-height">
                                            <h5 class="title">
                                                <a href="#">PIXMA MG2570S</a>
                                            </h5>
                                            <span class="badge primary small relative">Free Gift</span>
                                            <p class="margin-bottom0">
                                                PG-745s Black Ink worth $15.15
                                            </p>
                                        </div>
                                        <hr />
                                        <h4 class="price">$59</h4>
                                    </div>
                                </div>
                                <!-- End Post Item -->
                            </div>
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <!-- Start Post Item -->
                                <div class="post-item match-height border">
                                    <img src="https://canon-corpweb-dev.s3.amazonaws.com/images/category-sub/interchangeable-lens-cameras.png" alt="" />
                                    <div class="content">
                                        <div class="equal-height">
                                            <h5 class="title">
                                                <a href="#">PIXMA MG3070S</a>
                                            </h5>
                                            <span class="badge primary small relative">Free Gift</span>
                                            <p class="margin-bottom0">
                                                PG-745s Black Ink worth $15.15
                                            </p>
                                        </div>
                                        <hr />
                                        <h4 class="price">$79</h4>
                                    </div>
                                </div>
                                <!-- End Post Item -->
                            </div>
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <!-- Start Post Item -->
                                <div class="post-item match-height border">
                                    <img src="https://canon-corpweb-dev.s3.amazonaws.com/images/category-sub/interchangeable-lens-cameras.png" alt="" />
                                    <div class="content">
                                        <div class="equal-height">
                                            <h5 class="title">
                                                <a href="#">PIXMA E410</a>
                                            </h5>
                                            <span class="badge primary small relative">Free Gift</span>
                                            <p class="margin-bottom0">
                                                PG-47 Black Ink worth $14.35
                                            </p>
                                        </div>
                                        <hr />
                                        <h4 class="price">$89</h4>
                                    </div>
                                </div>
                                <!-- End Post Item -->
                            </div>
                        </div>
                        <!-- End Row -->
                    </div>
                    <!-- Start Pagination -->
                    <div class="post-pagination">
                        <ul>
                            <li class="nav"><a href="#"><span class="fa fa-angle-left"></span></a></li>
                            <li class="page"><input type="number" value="1"></li>
                            <li>of</li>
                            <li class="count">24</li>
                            <li class="nav"><a href="#"><span class="fa fa-angle-right"></span></a></li>
                        </ul>
                    </div>
                    <!-- End Pagination -->
                    <!-- Start Info -->
                    <div class="post-info left-small">
                        <span class="fa fa-circle"></span>All prices above are recommended retail price in SGD, unless otherwise stated.
                    </div>
                    <!-- End Info -->
                </div>
            </div>
        </div>
    </div>
    <!-- End Content -->
</div>
<!-- End Wrap Component -->
</body>
</html>