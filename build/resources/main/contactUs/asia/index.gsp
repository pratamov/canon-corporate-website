<%--
  Created by IntelliJ IDEA.
  User: rifai
  Date: 31/12/17
  Time: 2:43 AM
--%>

<%@ page import="grails.converters.JSON" contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title><g:message code="public.contactus.header.label" default="Contact Us"/></title>
    <meta name="layout" content="canon"/>
</head>

<body>
<!-- Start Content -->
<div class="section">
    <div class="wrap-contact">
        <div class="container">
            <!-- Start Box Panel -->
            <div class="box-panel bg-map">
                <h1 class="title-contact text-center left-small"><g:message code="public.contactus.header.label" default="Contact Us"/></h1>
                <div class="content-contact">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2 text-center left-small">
                            <p class="margin-bottom20">
                                Have you tried visited "<a href="#" class="single-link">Frequently Asked Questions</a>" you may find the answer to your query there. If not we look forward to help you out here.
                            </p>
                        </div>
                    </div>
                    <hr class="line">
                    <div class="row margin-top30">
                        <div class="col-sm-5 col-sm-push-7 m-margin-bottom30">
                            <form action="<g:createLink controller="contactUs" action="submit" params="[site: 'asia']"/>" id="form-contact-us-asia">
                                <div class="form-group">
                                    <select class="form-control select" id="type_of_enquiry" name="typeOfEnquiry" data-field-name="typeOfEnquiry">
                                        <option value="" selected="selected" disabled="disabled"><g:message code="public.contactus.typeOfEnquiry.select" default="Select a type of enquiry"/> </option>
                                        <g:each in="${enquiryList}" var="enquiry">
                                            <option value="${enquiry.key}">${enquiry.name}</option>
                                        </g:each>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <select class="form-control select" id="specific_enquiry" name="specificEnquiry" data-field-name="specificEnquiry">
                                        <option value="" selected="selected" disabled="disabled"><g:message code="public.contactus.specificEnquiry.select" default="Select a specific enquiry"/> </option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <select class="form-control select" id="salutation" name="salutation" data-field-name="salutation">
                                        <option value="" selected="selected" disabled="disabled"><g:message code="public.contactus.salutation.select" default="Select a salutation"/> </option>
                                        <g:each in="${salutationList}" var="salutation">
                                            <option value="${salutation}">${salutation}</option>
                                        </g:each>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <input type="name" class="form-control" placeholder="<g:message code="public.contactus.name.label" default="Name"/>" id="name" name="name" data-field-name="name">
                                </div>
                                <div class="form-group">
                                    <input type="emailAddress" class="form-control" placeholder="<g:message code="public.contactus.emailAddress.label" default="Email Address"/>" id="emailAddress" name="emailAddress" data-field-name="emailAddress">
                                </div>
                                <div class="form-group">
                                    <input type="retypeEmailAddress" class="form-control" placeholder="<g:message code="public.contactus.retypeEmailAddress.label" default="Retype Email Address"/>" id="retypeEmailAddress" name="retypeEmailAddress" data-field-name="retypeEmailAddress">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="<g:message code="public.contactus.contactNo.label" default="Contact No"/>" id="contactNo" name="contactNo" data-field-name="contactNo">
                                </div>
                                <div class="form-group">
                                    <select class="form-control select" id="country_of_residence" id="countryOfResidence" name="countryOfResidence" data-field-name="countryOfResidence">
                                        <option value="" selected="selected" disabled="disabled"><g:message code="public.contactus.countryOfResidence.label" default="Country of Residence"/> </option>
                                        <g:each in="${countryList}" var="country">
                                            <option value="${country.key}" data-url="${country.url}">${country.name}</option>
                                        </g:each>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <textarea class="form-control" placeholder="Message" rows="5" id="message" name="message" data-field-name="message"></textarea>
                                </div>
                                <button class="btn btn-primary btn-block" type="submit"><g:message code="public.contactus.send.label" default="Send"/></button>
                            </form>
                        </div>
                        <div class="col-sm-7 col-sm-pull-5 m-padding-bottom20">
                            ${raw(leftSideContent)}
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Box Panel -->
        </div>
    </div>
</div>
<!-- End Content -->



</div>
<!-- End Wrap Component -->
<script type="text/javascript">
    var ENQUIRY_LIST = ${raw(enquiryJson)};
</script>
<div class="clearfix"></div>
</body>
</html>