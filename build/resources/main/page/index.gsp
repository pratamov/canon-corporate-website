<%--
  Created by IntelliJ IDEA.
  User: muyalif
  Date: 29/11/17
  Time: 11.17
--%>

<%@ page import="canon.model.Component; canon.Category; canon.PageComponent" contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title><g:message code="public.article.main.${categoryType?.name()}.title" default="" /></title>
    <meta name="layout" content="canon"/>
</head>

<body>

<!-- Start Wrap Component -->
<div class="wrap-component">

    <!-- Start Inner Page -->
    <div class="inner-page absolute">
        <div class="blurheader"></div>
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-7 col-sm-4 col-xs-6 hide-small">
                    <g:message code="canon.enums.CategoryType.${categoryType?.name()}.label" default="" />
                </div>
                <div class="col-md-5 col-sm-8 col-xs-6 text-right">
                    <!-- Menus -->
                    <div class="wrap-menus open">
                        <a href="#" class="toggle-menus"><g:message code="public.article.main.selectMenu.label" default="Select menu" /></a>
                        <ul class="list-inline menus">
                            <g:each in="${categories as List<canon.Category>}" var="cat">
                                <li>
                                    <g:link controller="page" action="list" params="[site: params?.site, segment: params?.segment, category: cat.slug, categoryType: cat.categoryType?.name()?.toLowerCase()]">
                                        ${cat?.translation?.name ?: cat?.name}
                                    </g:link>
                                </li>
                            </g:each>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Inner Page -->
    <div class="clearfix"></div>

    <!-- Start Content -->
    <div class="section-demo demo-text3 bg" data-image="${grailsApplication.config.amazonaws.s3.baseUrl?:''}images/bg/img13.jpg">
        <div class="overlay"></div>
        <div class="container">
            <div class="content">
                <h1 class="title"><g:message code="public.article.main.searchDiscover.label" default="Discover great reads" /></h1>
                <div class="row">
                    <div class="col-sm-10 col-sm-offset-1">
                        <div class="input-serach">
                            <g:form controller="page" action="list" method="GET">
                                <g:hiddenField name="site" value="${params?.site}" />
                                <g:hiddenField name="lang" value="${params?.lang}" />
                                <g:hiddenField name="segment" value="${params?.segment}" />
                                <g:hiddenField name="category" value="${params?.category}" />
                                <g:hiddenField name="categoryType" value="${params?.categoryType}" />
                                <input type="text" placeholder="${message(code: 'public.article.main.search.label', default: 'Search Articles')}" name="q" value="${params?.q ?: ''}" />
                            </g:form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Content -->
    <div class="section-fix"></div>

    <g:each in="${components as List<canon.model.Component>}" var="comp">
        <g:renderComp component="${comp}" site="${params?.site}" segment="${params?.segment}" />
    </g:each>

</div>
<!-- End Wrap Component -->

<div class="clearfix"></div>
</body>
</html>