<%@ page import="canon.model.Component; canon.Category; canon.PageComponent" contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title><g:message code="public.article.main.${categoryType?.name()}.title" default="" /></title>
    <meta name="layout" content="canon"/>
</head>

<body>     
    <!-- Start Wrap Component -->
    <div class="wrap-component">
        <!-- Start Inner Page -->
        <div class="inner-page absolute">
            <div class="blurheader"></div>
            <div class="overlay"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-7 col-sm-4 col-xs-6 hide-small">
                        Business Solutions
                    </div>
                    <div class="col-md-5 col-sm-8 col-xs-6 text-right">
                        <!-- Menus -->
                        <div class="wrap-menus open">
                            <a href="#" class="toggle-menus">Select menu</a>
                            <ul class="list-inline menus">
                                <li class="active"><a href="#">Business Solutions</a></li>
                                <li><a href="#">Case Studies</a></li>
                            </ul>
                        </div>
                        <a href="#" class="btn btn-primary btn-single">Contact Us</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Inner Page -->

        <div class="clearfix"></div>
    
        <!-- Start Content -->
        <div class="section-demo demo-text bg" data-image="${grailsApplication.config.amazonaws.s3.baseUrl?:''}images/bg/img12.jpg">
            <div class="overlay"></div>
            <div class="container">
                <div class="content">
                    <h1 class="title"><g:message code="public.business.solutions.header.label" default="Solutions tailored to your business needs" /></h1>
                </div>
            </div>
        </div>
        <!-- End Content -->
    
        <div class="clearfix"></div>
        <g:each in="${components as List<canon.model.Component>}" var="comp">
            <g:renderComp component="${comp}" site="${params?.site}" segment="${params?.segment}" />
        </g:each>
        <!-- End Content -->
    </div>
    <!-- End Wrap Component -->

    <div class="clearfix"></div>
</body>
</html>
