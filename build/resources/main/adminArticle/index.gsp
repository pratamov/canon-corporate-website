<%--
  Created by IntelliJ IDEA.
  User: muyalif
  Date: 05/01/18
  Time: 18.04
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title><g:message code="admin.dashboard.title" default="Admin Dashboard"/></title>
    <meta name="layout" content="admin"/>
</head>

<body>
<!-- Start Page Heading -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-6">
        <h2><g:message code="admin.article.list.title" default="Article List" /></h2>
        <ol class="breadcrumb">
            <li>
                <g:link controller="adminArticle" action="index"><g:message code="admin.article.title" default="Article" /></g:link></a>
            </li>
            <li class="active">
                <strong><g:message code="admin.article.list.title" default="Article List" /></strong>
            </li>
        </ol>

    </div>
    <div class="col-lg-6 text-right">
        <g:link class="btn btn-primary" controller="adminArticle" action="create"><g:message code="admin.article.list.addNewArticle.label" default="Add New Article"/></g:link>
    </div>
</div>
<!-- End Page Heading -->

<!-- Start Content -->
<div class="wrapper wrapper-content">
    <!-- Start Ibox -->
    <div class="ibox-content margin-bottom60">
        <form method="get" action="${createLink(controller: 'adminArticle', action: 'index')}" >
            <div class="row">
                <div class="col-sm-8">
                    <div class="form-inline">
                        <div class="form-group margin-bottom20">
                            <label>Filter by</label>
                            <g:select class="select form-control w100" name="site" from="${locationList}" value="${params?.site}"
                                optionValue="name" optionKey="id" noSelection="${['': message(code: 'admin.article.list.allLocations.label', default: 'All Locations')]}"/>
                        </div>
                        <div class="form-group margin-bottom20">
                            <label><g:message code="admin.article.list.dateRange.label" default="Date range"/></label>
                            <div class="input-group date">
                                <input name="dateFrom" type="text" class="form-control" value="${params?.dateFrom}">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </span>
                            </div>
                        </div>
                        <div class="form-group margin-bottom20">
                            <label><g:message code="admin.article.list.to.label" default="to"/></label>
                            <div class="input-group date">
                                <input name="dateTo" type="text" class="form-control" value="${params?.dateTo}">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="input-group"><input type="text" name="q" value="${params?.q ?: ''}" placeholder="${message(code: 'admin.article.list.searchKeyword.label', default: 'Search keyword')}" class="input-sm form-control"> <span class="input-group-btn">
                        <button type="submit" class="btn btn-sm btn-primary"><g:message code="admin.article.list.search.label" default="Search"/></button></span></div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 margin-bottom20">
                    <div data-toggle="buttons" class="btn-group">
                        <g:each in="${countByType}" var="type">
                            <label class="btn btn-sm btn-white ${type.key == params.type ? 'active' : ''}">
                                <input type="radio" id="option7" name="type" value="${type.key}"
                                       class="js-auto-submit" ${type.key == params.type ? 'checked' : ''}>
                                <g:message code="canon.enums.CategoryType.${type.key}.label" default="" /> (${countByType.get(type.key)})
                            </label>
                        </g:each>
                    </div>
                </div>
                <div class="col-sm-7 margin-bottom20">
                    <div data-toggle="buttons" class="btn-group">
                        <g:each in="${statusList}" var="status">
                            <label class="btn btn-sm btn-white ${status.name() == params.status ? 'active' : ''}">
                                <input type="radio" id="option8" name="status" value="${status.name()}"
                                       class="js-auto-submit" ${status.name() == params.status ? 'checked' : ''}>
                                <g:message code="canon.enums.Status.${status.name()}.label" default="" /> (${countByStatus.get(status.name())})
                            </label>
                        </g:each>
                    </div>
                </div>
                <div class="col-sm-5 text-right">
                    <div data-toggle="buttons" class="btn-group">
                        <label class="btn btn-sm btn-white"> <input type="radio" name="options"> Duplicate</label>
                        <label class="btn btn-sm btn-white"> <input type="radio" name="options"> Delete</label>
                        <label class="btn btn-sm btn-white"> <input type="radio" name="options"> Move to Archive</label>
                    </div>
                </div>
            </div>
        </form>
        <div class="table-responsive margin-top50">
            <table class="table table-striped sortable">
                <thead>
                <tr>
                    <th class="input">
                        <div class="checkbox single">
                            <input id="checkboxd1" type="checkbox" class="checkall">
                            <label for="checkboxd1"></label>
                        </div>
                    </th>
                    <th data-sort="string" class="sort"><g:message code="admin.default.articleTitle.label" default="Article Title"/></th>
                    <th data-sort="string" class="sort"><g:message code="admin.default.author.label" default="Author"/></th>
                    <th data-sort="string" class="sort"><g:message code="admin.default.location.label" default="Location"/></th>
                    <th><g:message code="admin.article.list.category.label" default="Category"/></th>
                    <th data-sort="date" class="sort"><g:message code="admin.default.date.label" default="Date"/></th>
                </tr>
                </thead>
                <tbody>
                    <g:set var="datePrefix" value="${message(code: 'admin.article.list.publishedOn.label', default: 'Published on')}"/>
                    <g:each in="${pageList}" var="page" status="i">
                        <tr>
                            <td class="input">
                                <div class="checkbox single">
                                    <input id="checkboxd${i}" type="checkbox">
                                    <label for="checkboxd${i}"></label>
                                </div>
                            </td>
                            <td><a href="#" class="single-link">${page.title}</a></td>
                            <td>${page.author}</td>
                            <td>${page.locations}</td>
                            <td>${page.categories}</td>
                            <td>${datePrefix} <g:formatDate date="${page.publishedDate}" format="dd MMM yyyy" /></td>
                        </tr>
                    </g:each>
                </tbody>
            </table>
        </div>

        <!-- Start Pagination -->
        <g:set var="max" value="${Math.min(params?.int('max', 20), 30)}"/>
        <g:set var="page" value="${params?.int('page') ?: 1}"/>
        <g:set var="totalCount" value="${countByStatus.get(params?.status)}"/>
        <g:set var="totalPage" value="${Math.ceil(totalCount/max) as java.lang.Integer}"/>
        <g:if test="${totalCount > max}">
            <div class="post-pagination">
                <ul>
                    <li class="nav">
                        <g:if test="${page > 1}">
                            <g:link controller="adminArticle" action="index" params="${params - [page: params?.page] + [page: page - 1]}">
                                <span class="fa fa-angle-left"></span>
                            </g:link>
                        </g:if>
                        <g:else>
                            <span class="fa fa-angle-left"></span>
                        </g:else>
                    </li>
                    <li class="page">
                        <input name="page" type="number" value="${page}" class="js-input-page"
                               data-redirect-url="${createLink(controller: 'adminArticle', action: 'index', params: params - [page: params?.page] + [page: '---input-page---'])}" data-total-page="${totalPage}" data-page-placeholder="---input-page---">
                    </li>
                    <li><g:message code="admin.article.list.of.label" default="of"/> </li>
                    <li class="count">${totalPage}</li>
                    <li class="nav">
                        <g:if test="${page < totalPage}">
                            <g:link controller="adminArticle" action="index" params="${params - [page: params?.page] + [page: page + 1]}">
                                <span class="fa fa-angle-right"></span>
                            </g:link>
                        </g:if>
                        <g:else>
                            <span class="fa fa-angle-right"></span>
                        </g:else>
                    </li>
                </ul>
            </div>
        </g:if>
        <!-- End Pagination -->

    </div>
    <!-- End Ibox -->
</div>
<!-- End Content -->
</body>
</html>