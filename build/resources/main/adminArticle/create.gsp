<%--
  Created by IntelliJ IDEA.
  User: muyalif
  Date: 05/01/18
  Time: 19.06
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
    <head>
        <title><g:message code="admin.article.create.title" default="Add New Article"/></title>
        <meta name="layout" content="admin"/>
        <g:render template="/adminArticle/mustache"/>
    </head>

    <body>
        <!-- Start Page Heading -->
        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-10">
                <h2><g:message code="admin.article.create.title" default="Add New Article"/></h2>
                <ol class="breadcrumb">
                    <li>
                        <g:link controller="adminArticles" action="index"><g:message code="admin.article.create.editorial.label" default="Editorial"/></g:link>
                    </li>
                    <li>
                        <g:link controller="adminArticles" action="index"><g:message code="admin.article.title" default="Article"/></g:link>
                    </li>
                    <li>
                        <g:link controller="adminArticles" action="index"><g:message code="admin.article.list.title" default="Article List"/></g:link>
                    </li>
                    <li class="active">
                        <strong><g:message code="admin.article.create.title" default="Add New Article"/></strong>
                    </li>
                </ol>
            </div>
            <div class="col-lg-2"></div>
        </div>
        <!-- End Page Heading -->
        <g:hasErrors bean="${this.page}">
            <ul class="errors" role="alert">
                <g:eachError bean="${this.page}" var="error">
                    <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
                </g:eachError>
            </ul>
        </g:hasErrors>

        <g:form controller="adminArticle" action="save" method="POST" name="page-form">
            <g:hiddenField name="flow" value=""/>

            <!-- Start Content -->
            <div class="wrapper wrapper-content">
                <!-- Start box -->
                <div class="ibox-title">
                    <h3 class="title"><g:message code="admin.article.create.overview.label" default="Article Overview"/></h3>
                </div>
                <div class="ibox-content">
                    <div class="form-group margin-bottom20">
                        <label><g:message code="admin.article.create.selectWorkflow.label" default="Select Workflow"/></label>
                        <select class="select form-control" data-placeholder="${message(code:'admin.article.create.selectWorkflow.label', default:'Select Workflow')}">
                            <option></option>
                            <option value="Bahamas">Bahamas</option>
                            <option value="Bahrain">Bahrain</option>
                            <option value="Bangladesh">Bangladesh</option>
                            <option value="Barbados">Barbados</option>
                            <option value="Belarus">Belarus</option>
                            <option value="Belgium">Belgium</option>
                            <option value="Belize">Belize</option>
                            <option value="Benin">Benin</option>
                        </select>
                    </div>
                    <div class="row">
                        <g:each in="${workFlowList}" var="workFlow">
                            <g:if test="${workFlow.isStart}">
                                <div class="col-md-3">
                                    <div class="label-arrow ${currentWorkFlow?.id == workFlow?.id ? 'active' : ''}">${workFlow.name}</div>
                                    <div class="form-group margin-bottom20">
                                        <label><g:message code="admin.article.create.assignTo.label" default="Assign to"/></label>
                                        <input type="text" class="form-control" name="assignment.${workFlow?.id}.id" disabled  value="${user?.username}" />
                                    </div>
                                </div>
                            </g:if>
                            <g:else>
                                <div class="col-md-3">
                                    <div class="label-arrow">${workFlow.name}</div>
                                    <div class="form-group margin-bottom20">
                                        <label><g:message code="admin.article.create.assignTo.label" default="Assign to"/></label>
                                        <select class="select form-control" data-placeholder="${message(code:'admin.articles.title', default:'Article Preparer')} ${workFlow.name}">
                                            <option></option>
                                            <g:each in="${authorList}" var="author">
                                                <option value="${author.id}">${author.username}</option>
                                            </g:each>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label><g:message code="admin.article.create.deadline.label" default="Deadline"/></label>
                                        <div class="input-group date">
                                            <input type="text" class="form-control" value="02-10-2018" ${!currentWorkFlow.isStart ? 'disabled' : ''}>
                                            <span class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </g:else>
                        </g:each>
                    </div>
                </div>
                <!-- End Ibox -->
                <!-- Start Ibox -->
                <div class="ibox-content margin-bottom30">
                    <div class="form-group margin-bottom20">
                        <label><g:message code="admin.article.create.articleTitle.label" default="Article Title"/></label>
                        <input type="text" name="headline" maxlength="255" class="form-control" placeholder="${message(code:'admin.article.create.typeArticleTitle.label', default:'Type Article Title')}" value="${page?.headline}" />
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group margin-bottom20">
                                <label><g:message code="admin.article.create.articleLanguage.label" default="Article Language"/></label>
                                <select class="select form-control" name="language" data-placeholder="${message(code:'admin.article.create.selectLanguage.label', default:'Select Language')}">
                                    <option></option>
                                    <g:each in="${languageList}" var="language">
                                        <option value="${language.id}" ${language.id == page?.language?.id ? 'selected' : ''}>${language.name}</option>
                                    </g:each>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group margin-bottom20">
                                <label><g:message code="admin.article.create.articleCategory.label" default="Article Category"/></label>
                                <select class="select form-control" data-placeholder="Select Category">
                                    <option value="">All</option>
                                    <option value="Tips & tutorial">Tips & tutorial</option>
                                    <option value="option1">option1</option>
                                    <option value="option2">option2</option>
                                    <option value="option3">option3</option>
                                    <option value="option4">option4</option>
                                    <option value="option5">option5</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group margin-bottom20">
                                <label><g:message code="admin.article.create.articleAvailabilityDate.label" default="Article Availability Date"/></label>
                                <div class="input-group date">
                                    <input type="text" class="form-control" value="16-10-2014">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group margin-bottom20">
                                <label><g:message code="admin.article.create.articleDiscontinuedDate.label" default="Article Discontinued Date"/></label>
                                <div class="input-group date">
                                    <input type="text" class="form-control" value="16-10-2014">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Ibox -->
                <!-- Start Ibox -->
                <div class="ibox-content margin-bottom30">
                    <!-- Start Panel -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <strong><g:message code="admin.article.create.locationSettings.label" default="Location Settings"/></strong>
                        </div>
                        <div class="panel-body">
                            <!-- Start Table -->
                            <table class="table table-striped js-table-page-location">
                                <thead>
                                <tr>
                                    <th><g:message code="admin.article.create.contentLocation.label" default="Content Location"/></th>
                                    <th><g:message code="admin.article.create.articleCategory.label" default="Article Category"/></th>
                                    <th><g:message code="admin.article.create.publishDate.label" default="Publish Date"/></th>
                                    <th><g:message code="admin.article.create.archiveDate.label" default="Archive Date"/></th>
                                    <th class="text-right"><g:message code="admin.article.create.action.label" default="Action"/></th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                            <!-- End Table -->
                            <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#modalLocation"><g:message code="admin.article.create.addNewLocation.label" default="Add New Location"/></a>

                            <!-- Start Modal -->
                            <div class="modal inmodal" id="modalLocation" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content animated fadeIn">
                                        <div class="modal-header">
                                            <h3 class="modal-title"><g:message code="admin.article.create.addNewLocation.label" default="Add New Location"/></h3>
                                        </div>
                                        <div class="modal-body padding-bottom10 js-page-location-detail">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group margin-bottom20">
                                                        <label><g:message code="admin.article.create.selectContentDisplayLocations.label" default="Select Content Display Locations"/></label>
                                                        <select class="select form-control js-select-location-site" data-placeholder="${message(code:'admin.article.create.selectCountry.label', default:'Select Country')}">

                                                            <g:each in="${siteList}" var="site">
                                                                <option value="${site?.id}">${site?.name}</option>
                                                            </g:each>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group margin-bottom20">
                                                        <label><g:message code="admin.article.create.articleCategory.label" default="Article Category"/></label>
                                                        <select class="select form-control js-select-location-category" data-placeholder="${message(code:'admin.article.create.selectCategory.label', default:'Select Category')}">
                                                            <optgroup label="Consumer">
                                                                <g:each in="${consumerList}" var="category">
                                                                    <option value="${category[0]}" %{--${category[0] == params.long('parentCategory') ? 'selected' : ''--}%}>${category[1]}</option>
                                                                </g:each>
                                                            </optgroup>
                                                            <optgroup label="Business">
                                                                <g:each in="${businessList}" var="category">
                                                                    <option value="${category[0]}" %{--${category[0] == params.long('parentCategory') ? 'selected' : ''--}%}>${category[1]}</option>
                                                                </g:each>
                                                            </optgroup>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group margin-bottom20">
                                                        <label><g:message code="admin.article.create.publishDate.label" default="Publish Date"/></label><br/>
                                                        <small><em>(<g:message code="admin.article.create.publishDate.default.label" default="if empty, Product will be published when approved"/>)</em></small>
                                                        <div class="input-group date">
                                                            <input type="text" class="form-control" name="location-published-date">
                                                            <span class="input-group-addon">
                                                                <i class="fa fa-calendar"></i>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group margin-bottom20">
                                                        <label><g:message code="admin.article.create.archiveDate.label" default="Archive Date"/></label><br/><br/>
                                                        <div class="input-group date">
                                                            <input type="text" class="form-control" name="location-archived-date" placeholder="default to 12 months">
                                                            <span class="input-group-addon">
                                                                <i class="fa fa-calendar"></i>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-white" data-dismiss="modal"><g:message code="admin.article.create.cancel.label" default="Cancel"/></button>
                                            <button type="button" class="btn btn-primary js-page-save-location" data-close-modal-after-save="true"><g:message code="admin.article.create.save.label" default="Save"/></button>
                                            <button type="button" class="btn btn-primary js-page-save-location" data-close-modal-after-save="false"><g:message code="admin.article.create.saveAddAnother.label" default="Save & Add Another"/></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End Modal -->
                        </div>
                    </div>
                    <!-- End Panel -->

                    <!-- Start Panel -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <strong><g:message code="admin.article.create.tag.label" default="Tag"/></strong>
                        </div>
                        <div class="panel-body">
                            <div class="form-group">
                                <p><g:message code="admin.article.create.tag.desc.label" default="Select up to 3 tags to associate with the page"/></p>
                                <select id="page-tags" name="tags" class="form-control" data-placeholder="${message(code:'admin.article.create.tag.selectTags.label', default:'Select Tags')}">
                                    <option></option>
                                    <g:each in="${tags}" var="tag">
                                        <option value="${tag.id}" ${page?.getPageTags()?.id?.contains(tag.id) ? 'selected':''}>${tag.name}</option>
                                    </g:each>
                                </select>
                            </div>
                        </div>
                    </div>
                    <!-- End Panel -->

                    <!-- Start Panel -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <strong><g:message code="admin.article.create.topic.label" default="Topic"/></strong>
                        </div>
                        <div class="panel-body">
                            <div class="form-group">
                                <p><g:message code="admin.article.create.topic.desc.label" default="Topic"/></p>
                                <select id="page-topics" name="topics" class="form-control" data-placeholder="${message(code:"admin.article.create.topic.selectTopics.label", default:"Select Topics")}">
                                    <option></option>
                                    <g:each in="${topics}" var="topic">
                                        <option value="${topic.id}" ${page?.getPageSubject()?.id?.contains(topic.id) ? 'selected':''}>${topic.name}</option>
                                    </g:each>
                                </select>
                            </div>
                        </div>
                    </div>
                    <!-- End Panel -->

                    <!-- Start Panel -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <strong><g:message code="admin.article.create.socialShare.label" default="Social Share"/></strong> (<g:message code="admin.article.create.optional.label" default="optional"/>)
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" action="/action_page.php">
                                <div class="form-group">
                                    <label class="control-label col-sm-2 text-left"><g:message code="admin.article.create.socialShare.title.label" default="Title"/></label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control">
                                        <small><em><g:message code="admin.article.create.socialShare.titleDesc.label" default="If the field is blank, the value will be pulled from product title."/></em></small>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2 text-left"><g:message code="admin.article.create.socialShare.description.label" default="Description"/></label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control">
                                        <small><em><g:message code="admin.article.create.socialShare.descriptionDesc.label" default="If the field is blank, the value will be pulled from text below hero image (synopsis)"/></em></small>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2 text-left"><g:message code="admin.article.create.socialShare.image.label" default="Image"/></label>
                                    <div class="col-sm-4">
                                        <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                            <div class="form-control" data-trigger="fileinput">
                                                <i class="glyphicon glyphicon-file fileinput-exists"></i>
                                                <span class="fileinput-filename"></span>
                                            </div>
                                            <span class="input-group-addon btn btn-default btn-file">
                                                <span class="fileinput-new"><g:message code="admin.article.create.socialShare.selectFile.label" default="Select file"/></span>
                                                <span class="fileinput-exists"><g:message code="admin.article.create.socialShare.change.label" default="Change"/></span>
                                                <input type="file" name="..."/>
                                            </span>
                                            <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                        </div>
                                        <small><em><g:message code="admin.article.create.socialShare.imageDesc.label" default="If the field is blank, the hero image will be used."/></em></small>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Panel -->
                </div>
                <!-- End Ibox -->
                <!-- Start Ibox -->
                <div class="ibox-title">
                    <button class="btn btn-primary pull-right"><g:message code="admin.article.create.inlineEditor.label" default="Inline Editor"/></button>
                    <h3 class="title">Preview</h3>
                </div>
                <div class="preview-page">
                    <div class="content"><g:message code="admin.article.create.previewPage.label" default="our preview will appear here. Click on the Edit Content button below to start creating your product."/></div>
                </div>
                <div class="ibox-content margin-bottom80"></div>
            </div>
            <!-- End Ibox -->
            <content tag="footer">
                <div class="pull-right">
                    <button class="btn btn-default margin-left10"><g:message code="admin.article.create.cancel.label" default="Cancel"/></button>
                    <button class="btn btn-primary margin-left10 js-page-submit-form" data-flow="NO_CHANGES"><g:message code="admin.article.create.saveChanges.label" default="Save Changes"/></button>
                    <button class="btn btn-primary margin-left10 js-page-submit-form" data-flow="NEXT"><g:message code="admin.article.create.submit.label" default="Submit"/></button>
                </div>
            </content>
        <!-- End Content -->
        </g:form>
    </body>
</html>