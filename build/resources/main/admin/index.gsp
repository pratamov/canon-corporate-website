<%--
  Created by IntelliJ IDEA.
  User: marjan
  Date: 11/12/17
  Time: 18.29
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title><g:message code="admin.dashboard.title" default="Admin Dashboard"/></title>
    <meta name="layout" content="admin"/>
</head>

<body>
<!-- Start Page Heading -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Dashboard</h2>
        <ol class="breadcrumb">
            <li class="active">
                <strong>Dashboard</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2"></div>
</div>
<!-- End Page Heading -->

<!-- Start Content -->
<div class="wrapper wrapper-content">
    <!-- Start Ibox -->
    <div class="ibox-content margin-bottom60">
        <div class="row">
            <div class="col-sm-8">
                <form class="form-inline">
                    <div class="form-group margin-bottom20">
                        <label>Date range</label>
                        <select class="form-control">
                            <option>14 Sept 2017</option>
                            <option>13 Sept 2017</option>
                            <option>12 Sept 2017</option>
                            <option>11 Sept 2017</option>
                            <option>10 Sept 2017</option>
                            <option>9 Sept 2017</option>
                        </select>
                    </div>
                    <div class="form-group margin-bottom20">
                        <label>to</label>
                        <select class="form-control">
                            <option>14 Sept 2017</option>
                            <option>13 Sept 2017</option>
                            <option>12 Sept 2017</option>
                            <option>11 Sept 2017</option>
                            <option>10 Sept 2017</option>
                            <option>9 Sept 2017</option>
                        </select>
                    </div>
                </form>
            </div>
            <div class="col-sm-4">
                <div class="input-group"><input type="text" placeholder="Search keyword" class="input-sm form-control"> <span class="input-group-btn">
                    <button type="button" class="btn btn-sm btn-primary">Search</button> </span></div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 margin-bottom20">
                <div data-toggle="buttons" class="btn-group">
                    <label class="btn btn-sm btn-white"> <input type="radio" id="option1" name="options">All (256)</label>
                    <label class="btn btn-sm btn-white active"> <input type="radio" id="option2" name="options">Pages (10)</label>
                    <label class="btn btn-sm btn-white"> <input type="radio" id="option3" name="options">Products (10)</label>
                    <label class="btn btn-sm btn-white"> <input type="radio" id="option4" name="options">Articles (10)</label>
                    <label class="btn btn-sm btn-white"> <input type="radio" id="option5" name="options">Events (10)</label>
                    <label class="btn btn-sm btn-white"> <input type="radio" id="option6" name="options">Courses (10)</label>
                    <label class="btn btn-sm btn-white"> <input type="radio" id="option7" name="options">DAM (10)</label>
                    <label class="btn btn-sm btn-white"> <input type="radio" id="option8" name="options">Dealers (10)</label>
                    <label class="btn btn-sm btn-white"> <input type="radio" id="option9" name="options">FAQ (10)</label>
                    <label class="btn btn-sm btn-white"> <input type="radio" id="option10" name="options">Emails (10)</label>
                    <label class="btn btn-sm btn-white"> <input type="radio" id="option11" name="options">Forms (10)</label>
                    <label class="btn btn-sm btn-white"> <input type="radio" id="option12" name="options">Lists (10)</label>
                    <label class="btn btn-sm btn-white"> <input type="radio" id="option13" name="options">Errors (10)</label>
                    <label class="btn btn-sm btn-white"> <input type="radio" id="option14" name="options">Reports (10)</label>
                    <label class="btn btn-sm btn-white"> <input type="radio" id="option15" name="options">Settings (10)</label>
                    <label class="btn btn-sm btn-white"> <input type="radio" id="option16" name="options">Users (10)</label>
                    <label class="btn btn-sm btn-white"> <input type="radio" id="option17" name="options">Roles (10)</label>
                    <label class="btn btn-sm btn-white"> <input type="radio" id="option18" name="options">Workflows (10)</label>
                </div>
            </div>
            <div class="col-sm-7 margin-bottom20">
                <div data-toggle="buttons" class="btn-group">
                    <label class="btn btn-sm btn-white"> <input type="radio" id="option19" name="options">All (120)</label>
                    <label class="btn btn-sm btn-white"> <input type="radio" id="option20" name="options">Archive (0)</label>
                    <label class="btn btn-sm btn-white"> <input type="radio" id="option21" name="options">Trash (0)</label>
                </div>
            </div>
            <div class="col-sm-5 text-right">
                <div data-toggle="buttons" class="btn-group">
                    <label class="btn btn-sm btn-white"> <input type="radio" id="option23" name="options"> Delete</label>
                    <label class="btn btn-sm btn-white"> <input type="radio" id="option24" name="options"> Move to Archive</label>
                </div>
            </div>
        </div>
        <div class="table-responsive margin-top50">
            <table class="table table-striped sortable">
                <thead>
                <tr>

                    <th class="input"><input type="checkbox" id="checkall" class="i-checks" name="input[]"></th>
                    <th data-sort="string" class="sort">Date</th>
                    <th data-sort="string" class="sort">Resource </th>
                    <th data-sort="string" class="sort">Subject</th>
                    <th data-sort="string" class="sort">Message </th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td class="input"><input type="checkbox" class="i-checks" name="input[]"></td>
                    <td>14 Sep 2017</td>
                    <td>Articles</td>
                    <td><a href="#" class="single-link">Getting the most out of your lens</a></td>
                    <td>The workflow has been updated from Author (Elson Loh) to Proof Reader (Jason Huang)</td>
                </tr>
                <tr>
                    <td class="input"><input type="checkbox" class="i-checks" name="input[]"></td>
                    <td>10 Sep 2017</td>
                    <td>Products</td>
                    <td><a href="#" class="single-link">EOS 6D Mark II</a></td>
                    <td>ALERT! The product will expire in 1 week’s time on 17 Sep 2017.</td>
                </tr>
                <tr>
                    <td class="input"><input type="checkbox" class="i-checks" name="input[]"></td>
                    <td>30 Aug 2017</td>
                    <td>Products</td>
                    <td><a href="#" class="single-link">imageCLASS MF631Cn</a></td>
                    <td>ALERT! The product will expire in 1 week’s time on 06 Sep 2017.</td>
                </tr>
                <tr>
                    <td class="input"><input type="checkbox" class="i-checks" name="input[]"></td>
                    <td>28 Aug 2017</td>
                    <td>User</td>
                    <td>ser Creation</td>
                    <td>Lorem ipsum dolor sit amet, eu nam partem persequeris, altera concludaturque vis ne.</td>
                </tr>
                <tr>
                    <td class="input"><input type="checkbox" class="i-checks" name="input[]"></td>
                    <td>26 Aug 2017</td>
                    <td>Articles</td>
                    <td><a href="#" class="single-link">Your window on the world</a></td>
                    <td>Lorem ipsum dolor sit amet, eu nam partem persequeris, altera concludaturque vis ne.</td>
                </tr>
                <tr>
                    <td class="input"><input type="checkbox" class="i-checks" name="input[]"></td>
                    <td>18 Aug 2017</td>
                    <td>Articles</td>
                    <td><a href="#" class="single-link">Canon Signature Zoo Photo-walk with Julian W.</a></td>
                    <td>Lorem ipsum dolor sit amet, eu nam partem persequeris, altera concludaturque vis ne.</td>
                </tr>
                <tr>
                    <td class="input"><input type="checkbox" class="i-checks" name="input[]"></td>
                    <td>05 Aug 2017</td>
                    <td>Articles</td>
                    <td><a href="#" class="single-link">Mongolia Eagle Hunting Photo-Tour (7D6N)</a></td>
                    <td>Lorem ipsum dolor sit amet, eu nam partem persequeris, altera concludaturque vis ne.</td>
                </tr>
                </tbody>
            </table>
        </div>

        <!-- Start Pagination -->
        <div class="post-pagination">
            <ul>
                <li class="nav"><a href="#"><span class="fa fa-angle-left"></span></a></li>
                <li class="page">1</li>
                <li>of</li>
                <li class="count">24</li>
                <li class="nav"><a href="#"><span class="fa fa-angle-right"></span></a></li>
            </ul>
        </div>
        <!-- End Pagination -->

    </div>
    <!-- End Ibox -->
</div>
<!-- End Content -->
</body>
</html>