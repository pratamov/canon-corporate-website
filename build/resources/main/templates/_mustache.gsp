<script id="specs-compare-template" type="x-tmpl-mustache">
    {{#groups}}
    <div class="wrap-table-info table-compare">
       <h3 class="table-title">{{groupName}}</h3>
       <table class="table table-info">
           {{#specs}}
           <tr class="{{event}}">
               <td class="title">{{name}}</td>
               <td>{{valuea}}</td>
               <td>{{valueb}}</td>
               <td>{{valuec}}</td>
           </tr>
           {{/specs}}
       </table>
    </div>
    {{/groups}}
</script>