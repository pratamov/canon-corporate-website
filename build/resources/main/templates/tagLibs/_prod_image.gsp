<%@ page import="canon.ProductMedia" %>
<!-- Start Content -->
<g:set var="compProdPromotion" value="${component?.getAsComponent() as canon.model.comp.ProdImageComp}" />
<g:set var="productMedias" value="${product?.productMedias as java.util.List<canon.ProductMedia>}"/>
<g:if test="${productMedias}">
    <!-- Start Section -->
    <div class="section padding-top0 padding-bottom0">
        <div class="container">
            <!-- Start Wrap Views -->
            <div class="wrap-view">
                <ul class="control">
                    <li>
                        <a href="#zoomin"><span class="fa fa-search"></span></a>
                        <g:message code="product.overview.zoom.label" default="Zoom"/>
                    </li>
                    <li class="active">
                        <a href="#rotate" class="custom_next"><span class="fa fa-rotate-left"></span></a>
                        <g:message code="product.overview.360view.label" default="360º View"/>
                    </li>
                </ul>

                <!-- Start Load 360 -->
                <div id="rotate" class="content-view rotate-img threesixty hide">
                    <div class="spinner">
                        <span><g:message code="product.overview.zeroPercent.label" default="0%"/></span>
                    </div>
                    <ol class="threesixty_images"></ol>
                </div>
                <!-- End Load 360 -->

                <!-- Start Zoom In -->
                <div id="zoomin" class="content-view wrap-zoomin hide">
                    <div class="panzoom">
                        <img src="" class="img-zoom">
                    </div>
                    <div class="buttons">
                        <button class="zoom-in btn btn-primary btn-sm"><span class="fa fa-plus"></span></button>
                        <button class="zoom-out btn btn-primary btn-sm"><span class="fa fa-minus"></span></button>
                        <button class="reset btn btn-primary btn-sm"><span class="fa fa-refresh"></span></button>
                    </div>
                </div>
                <!-- End Zoom In -->
            </div>
            <!-- End Wrap Views -->

            <!-- Start Item Views -->
            <div class="owl-carousel owl-view">
                <g:each in="${productMedias}" var="productMedia">
                    <div class="item ${productMedia?.isDefault ? 'active': '' } list-view" data-type="${productMedia?.media?.mediaType?.name()?.toLowerCase()}" data-path="${productMedia?.media?.url}">
                        <div class="content">
                            <a href="#">
                                <img src="${productMedia?.media?.url}" alt="" />
                            </a>
                        </div>
                    </div>
                </g:each>
            </div>
            <!-- End Item Views -->
        </div>
    </div>
    <!-- End Section -->
    <div class="clearfix"></div>
</g:if>
