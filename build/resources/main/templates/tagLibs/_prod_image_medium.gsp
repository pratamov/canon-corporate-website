<!-- Start Content -->
<g:set var="prodImageMedium" value="${component?.getAsComponent() as canon.model.comp.ProdImageMediumComp}" />
<g:set var="prod" value="${product as canon.Product}"/>
<g:set var="prodSellingPoint" value="${prod.productSellingPoints.sort{it.pointOrder} as List<canon.ProductSellingPoint>}"/>
<g:if test="${prodImageMedium}">
    <!-- Start Content Overview -->
    <div class="section">
        <div class="content-detail m-padding-top30 padding-bottom0">
            <div class="container">
                <!-- Start Overview -->
                <div class="overview">
                    <div class="row">
                        <div class="col-md-7 col-md-push-5">
                            <!-- Start Slider -->
                            <div class="wrap-slide-image nav-noborder">
                                <div class="owl-carousel owl-theme owl-image">
                                    <g:each in="${prodImageMedium.images}" var="img">
                                        <div class="item">
                                            <img src="${img?.mediaUrl}" alt=""/>
                                        </div>
                                    </g:each>
                                </div>
                            </div>
                            <!-- End Slider -->
                        </div>
                        <div class="col-md-5 col-md-pull-7">
                            <h1 class="title">${prod?.name}</h1>
                            <h2 class="sub-title">${prodImageMedium.subtitle ?: prod?.subText}</h2>
                            <p>${prodImageMedium.description ?: prod?.description}</p>
                            <g:if test="${prodSellingPoint}">
                                <ul class="list-normal">
                                    <g:each in="${prodSellingPoint}" var="psp">
                                        <li>${psp.content}</li>
                                    </g:each>
                                </ul>
                            </g:if>
                        </div>
                    </div>
                </div>
                <!-- End Overview -->
            </div>
        </div>
    </div>
    <!-- End Content Overview -->

    
    <div class="clearfix"></div>
</g:if>
