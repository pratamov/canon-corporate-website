<g:set var="compProdDesc" value="${component?.getAsComponent() as canon.model.comp.ProdDescComp}" />
<!-- Start Section Detail -->
<div class="content-detail m-padding-bottom30">
    <div class="container">

        <!-- Start Content -->
        <div class="row main-content">
            <div class="col-md-8 col-md-offset-2">
                <h1 class="title">
                    ${compProdDesc?.getNewSubText() ?: product?.subText}
                </h1>
                <div class="desc">
                    ${compProdDesc?.getNewDesc() ?: product?.description}
                </div>
            </div>
        </div>
        <!-- End Content -->
    </div>
</div>
<!-- End Section Detail -->