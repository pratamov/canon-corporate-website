<g:set var="compText" value="${component?.getAsComponent() as canon.model.comp.TextComp}" />
<g:if test="${compText && org.apache.commons.lang.StringUtils.isNotBlank(compText?.getContent())}">
    <!-- Start Section -->
    <div class="section">
        <div class="container">
            ${raw(compText?.getContent())}
        </div>
    </div>
    <!-- End Section -->
</g:if>