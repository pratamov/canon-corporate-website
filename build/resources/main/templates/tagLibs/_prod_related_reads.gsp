<g:set var="compProdRelatedReads" value="${component?.getAsComponent() as canon.model.comp.ProdRelatedReadsComp}" />
<g:if test="${compProdRelatedReads?.articles}">
    <!-- Start Section -->
    <div class="section">
        <div class="container">
            <!-- Start Features -->
            <h1 class="title-section text-center"><g:message code="product.overview.relatedReads.label" default="Related Reads"/></h1>
            <div class="wrap-carousel-in-small">
                <div class="row carousel-in-small">
                    <g:each in="${compProdRelatedReads?.articles}" var="article">
                        <div class="col-sm-4 col-xs-12">
                            <!-- Start Post Item -->
                            <div class="post-item bg-mode">
                                <span class="badge gray left hide-small">${article?.articleTagName}</span>
                                <img src="${article?.articleBgUrl}" alt="" />
                                <div class="content">
                                    <h3 class="title">${article?.articleTitle}</h3>
                                </div>
                                <g:link class="link" controller="page" action="show" id="${article?.articleSlug}" params="params"/>
                            </div>
                            <!-- End Post Item -->
                        </div>
                    </g:each>
                </div>
            </div>
            <!-- End Content -->
        </div>
    </div>
    <!-- End Section -->

    <div class="clearfix"></div>
</g:if>