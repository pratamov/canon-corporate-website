<g:set var="compInfoCard" value="${component?.getAsComponent() as canon.model.comp.InformationCardComp}" />
<!-- Start Section Detail -->
<g:if test="${compInfoCard?.desc}">
    <div class="section">
        <div class="container">
            <!-- Start Features -->
            <g:if test="${compInfoCard?.sectionTitle}">
                <h1 class="title-section text-center">${compInfoCard?.sectionTitle}</h1>
            </g:if>
            <!-- Start Post Item -->
            <div class="post-item match-height">
                <div class="row wrap-vertical-content">
                    <div class="col-sm-6 getheight">
                        <img src="${compInfoCard?.mediaUrl}" alt="" />
                    </div>
                    <div class="col-sm-6 desc">
                        <div class="vertical-middle">
                            <div class="content left-small">
                                <g:if test="${compInfoCard?.cardTitle}">
                                    <h3 class="title"><a href="#">${compInfoCard?.cardTitle}</a></h3>
                                </g:if>
                                <p>
                                    ${raw(compInfoCard?.desc)}
                                </p>
                                <g:if test="${compInfoCard?.url}">
                                    <a href="${compInfoCard?.url}" class="btn btn-primary ">${compInfoCard?.buttonLabel}</a>
                                </g:if>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Post Item -->
        </div>
    </div>
    <!-- End Section -->
    <div class="clearfix"></div>
</g:if>