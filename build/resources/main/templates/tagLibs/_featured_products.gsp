<%@ page import="canon.model.comp.FeaturedProductsComp" %>
<g:set var="compFeaturedProduct" value="${component?.getAsComponent() as FeaturedProductsComp}" />
<g:if test="${compFeaturedProduct && compFeaturedProduct.getFeaturedList()?.size() > 0}">
    <!-- Start Content -->
    <div class="section products" id="product">
        <div class="container">
            <div class="row">
                <g:each var="featuredProduct" in="${compFeaturedProduct.getFeaturedList()}" status="i">
                    <g:if test="${compFeaturedProduct.getFeaturedList()?.size() - 1 != i}">
                        <div class="col-sm-4 col-xs-6">
                            <!-- Start Post Item -->
                            <div class="post-item match-height">
                                <span class="badge left"><g:message code="home.tag.new.label" default="New"/></span>
                                <img src="${featuredProduct?.defaultMedia?.media?.url}" alt="">
                                <div class="content">
                                    <h3 class="title">
                                        <g:link controller="product" action="show" id="${featuredProduct?.slug}" params="[segment: params?.segment, site: params?.site, lang: params?.lang]">
                                            ${featuredProduct?.name}
                                        </g:link>
                                    </h3>
                                </div>
                            </div>
                            <!-- End Post Item -->
                        </div>
                    </g:if>
                    <g:else>
                        <div class="col-sm-12 col-xs-6">
                            <!-- Start Post Item -->
                            <div class="post-item match-height">
                                <span class="badge left"><g:message code="home.tag.promotion.label" default="Promotion"/></span>
                                <div class="row wrap-vertical-content">
                                    <div class="col-sm-6 getheight">
                                        <img src="${featuredProduct?.defaultMedia?.media?.url}" alt="">
                                    </div>
                                    <div class="col-sm-6 desc">
                                        <div class="vertical-middle">
                                            <div class="content">
                                                <h3 class="title">
                                                    <g:link controller="product" action="show" params="[id: featuredProduct?.slug]">
                                                        ${featuredProduct?.name}
                                                    </g:link>
                                                </h3>
                                                <p class="hide-small">
                                                    ${featuredProduct?.description}
                                                </p>
                                                <g:link class="btn btn-primary hide-small" controller="product" action="show" params="[id: featuredProduct?.slug]">
                                                    <g:message code="home.button.viewProduct.label" default="View Product"/>
                                                </g:link>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End Post Item -->
                        </div>
                    </g:else>
                </g:each>
            </div>

            <div class="text-right">
                <g:link class="btn btn-default btn-sm more" controller="product" action="category" params="[segment: params?.segment, site: params?.site, lang: params?.lang]">
                    <g:message code="component.FEATURED_PRODUCTS.viewAll.label" default="View all products"/> <span class="fa fa-angle-right"></span>
                </g:link>
            </div>
        </div>
    </div>
    <!-- End Content -->
    <div class="clearfix"></div>
</g:if>