<g:set var="compBanner" value="${component?.getAsComponent() as canon.model.comp.BannerComp}" />
<g:if test="${compBanner && compBanner.getBanner().getBannerItems()}">
    <div class="home" id="home">
        <div class="wrap-carousel">
            <div class="owl-carousel owl-home">
                <g:each in="${compBanner.getBanner().getBannerItems()}" var="bannerItem">
                    <div class="item">
                        <div class="bg" data-image="${bannerItem?.background?.getUrl()}" data-copyright="${bannerItem?.background?.getCopyright() ?: ''}"></div>
                        <div class="content">
                            <h1 class="font-light">
                                ${raw(bannerItem?.title)}
                            </h1>
                            <g:link url="${bannerItem?.buttonUrl}" class="btn btn-primary">
                                ${bannerItem?.buttonLabel ?: message(code: "home.button.findADeal.label", default: "Find a deal")}
                            </g:link>
                        </div>
                    </div>
                </g:each>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</g:if>