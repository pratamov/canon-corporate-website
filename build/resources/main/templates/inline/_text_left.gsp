<g:set var="compTextImage" value="${component?.getAsComponent() as canon.model.comp.TextImageComp}" />
<!-- Start Section -->
<section class="section component-text" data-type="component-text-left" data-title="Text (left) - Image (right)">
    <div class="row">
        <div class="col-md-6 component-text-image-content">
            ${raw(compTextImage?.getContent() ?: '')}
        </div>
        <div class="col-md-6">
            <img src="${compTextImage?.getImageUrl() ?: ''}" class="img-responsive component-text-image-image" alt=""
                 data-copyright="${compTextImage?.getCopyright() ?: ''}" data-id="${compTextImage?.getMediaId() ?: ''}" data-source="${compTextImage?.getSource()?.name() ?: ''}" />
        </div>
    </div>
</section>
<!-- End Section -->