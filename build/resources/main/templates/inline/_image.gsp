<g:set var="compImage" value="${component?.getAsComponent() as canon.model.comp.ImageComp}" />
<!-- Start Section -->
<section class="section component-text" data-type="component-image" data-title="Image with caption">
    <h3 class="font-light margin-bottom30 component-image-title">${compImage?.getTitle() ?: ""}</h3>
    <div class="img-article">
        <img src="${compImage?.getImageUrl() ?: ''}" class="img-responsive component-image-image" alt=""
             data-copyright="${compImage?.getCopyright() ?: ''}" data-id="${compImage?.getMediaId() ?: ''}" data-source="${compImage?.getSource()?.name() ?: ''}" />
        <p class="margin-top10 component-image-caption">
            ${raw(compImage?.getCaption() ?: '')}
        </p>
    </div>
</section>
<!-- End Section -->