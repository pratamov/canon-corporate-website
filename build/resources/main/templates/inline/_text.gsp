<g:set var="compText" value="${component?.getAsComponent() as canon.model.comp.TextComp}" />
<!-- Start Section -->
<section class="section component-text" data-type="component-text" data-title="Text block">
    ${raw(compText?.getContent() ?: '')}
</section>
<!-- End Section -->