<%@ page import="canon.Site" %>
<!-- Start Modal Related Product -->
<div class="modal inmodal" id="modalRelatedProduct" tabindex="-1" role="dialog" aria-hidden="true" data-url="${createLink(controller: 'adminProduct', action: 'getProductList')}">
	<div class="modal-dialog modal-lg">
		<div class="modal-content animated fadeIn">
			<div class="modal-header">
				<h3 class="modal-title"></h3>
			</div>
			<div class="modal-body padding-bottom10">
				<select class="form-control dual_select js-selector" multiple></select>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-white" data-dismiss="modal">Cancel</button>
				<button type="button" class="btn btn-primary js-save-btn">Save</button>
			</div>
		</div>
	</div>
</div>
<!-- End Modal Related Product -->

<!-- Start Modal Specification -->
<div class="modal inmodal" id="modalSpecification" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content animated fadeIn">
			<div class="modal-header">
				<div class="pull-right select-title-right">
					<select class="select form-control w100" data-placeholder="Select Product">
						<option value="Lorem Ispum">Lorem Ispum</option>
						<option value="Lorem Ispum">Lorem Ispum</option>
						<option value="Lorem Ispum" selected>Digital SLR (EOS)</option>
						<option value="Lorem Ispum">Lorem Ispum</option>
						<option value="Lorem Ispum">Lorem Ispum</option>
						<option value="Lorem Ispum">Lorem Ispum</option>
						<option value="Lorem Ispum">Lorem Ispum</option>
					</select>
				</div>
				<h3 class="modal-title">Full Specification</h3>
			</div>
			<div class="modal-body padding-bottom10">
				<di class="wrap-ibox-collapse">
					<!-- Start Collapse -->
					<div class="ibox collapsed margin-bottom0">
						<div class="ibox-title">
							<h5>Type</h5>
							<div class="ibox-tools">
								<a class="collapse-link">
									<i class="fa fa-chevron-up"></i>
								</a>
							</div>
						</div>
						<div class="ibox-content">
							<!-- Start Form Group -->
							<div class="form-group row margin-bottom0">
								<label class="col-md-3">Type</label>
								<div class="col-md-9 input-group m-b">
									<input type="text" class="form-control">
									<span class="input-group-addon"><i class="fa fa-trash"></i></span>
								</div>
							</div>
							<!-- End Form Group -->
							<!-- Start Form Group -->
							<div class="form-group row margin-bottom0">
								<label class="col-md-3">Effective Pixels</label>
								<div class="col-md-9 input-group m-b">
									<input type="text" class="form-control">
									<span class="input-group-addon"><i class="fa fa-trash"></i></span>
								</div>
							</div>
							<!-- End Form Group -->
							<!-- Start Form Group -->
							<div class="form-group row margin-bottom0">
								<label class="col-md-3">Aspect Ratio</label>
								<div class="col-md-9 input-group m-b">
									<input type="text" class="form-control">
									<span class="input-group-addon"><i class="fa fa-trash"></i></span>
								</div>
							</div>
							<!-- End Form Group -->
							<!-- Start Form Group -->
							<div class="form-group row margin-bottom0">
								<label class="col-md-3">Dust Deletion Feature</label>
								<div class="col-md-9 input-group m-b">
									<input type="text" class="form-control">
									<span class="input-group-addon"><i class="fa fa-trash"></i></span>
								</div>
							</div>
							<!-- End Form Group -->
						</div>
					</div>
					<!-- End Collapse -->
					<!-- Start Collapse -->
					<div class="ibox collapsed margin-bottom0">
						<div class="ibox-title">
							<h5>Image Sensor</h5>
							<div class="ibox-tools">
								<a class="collapse-link">
									<i class="fa fa-chevron-up"></i>
								</a>
							</div>
						</div>
						<div class="ibox-content">
							<!-- Start Form Group -->
							<div class="form-group row margin-bottom0">
								<label class="col-md-3">Type</label>
								<div class="col-md-9 input-group m-b">
									<input type="text" class="form-control">
									<span class="input-group-addon"><i class="fa fa-trash"></i></span>
								</div>
							</div>
							<!-- End Form Group -->
							<!-- Start Form Group -->
							<div class="form-group row margin-bottom0">
								<label class="col-md-3">Effective Pixels</label>
								<div class="col-md-9 input-group m-b">
									<input type="text" class="form-control">
									<span class="input-group-addon"><i class="fa fa-trash"></i></span>
								</div>
							</div>
							<!-- End Form Group -->
							<!-- Start Form Group -->
							<div class="form-group row margin-bottom0">
								<label class="col-md-3">Aspect Ratio</label>
								<div class="col-md-9 input-group m-b">
									<input type="text" class="form-control">
									<span class="input-group-addon"><i class="fa fa-trash"></i></span>
								</div>
							</div>
							<!-- End Form Group -->
							<!-- Start Form Group -->
							<div class="form-group row margin-bottom0">
								<label class="col-md-3">Dust Deletion Feature</label>
								<div class="col-md-9 input-group m-b">
									<input type="text" class="form-control">
									<span class="input-group-addon"><i class="fa fa-trash"></i></span>
								</div>
							</div>
							<!-- End Form Group -->
						</div>
					</div>
					<!-- End Collapse -->
					<!-- Start Collapse -->
					<div class="ibox collapsed margin-bottom0">
						<div class="ibox-title">
							<h5>Recording System</h5>
							<div class="ibox-tools">
								<a class="collapse-link">
									<i class="fa fa-chevron-up"></i>
								</a>
							</div>
						</div>
						<div class="ibox-content">
							<!-- Start Form Group -->
							<div class="form-group row margin-bottom0">
								<label class="col-md-3">Type</label>
								<div class="col-md-9 input-group m-b">
									<input type="text" class="form-control">
									<span class="input-group-addon"><i class="fa fa-trash"></i></span>
								</div>
							</div>
							<!-- End Form Group -->
							<!-- Start Form Group -->
							<div class="form-group row margin-bottom0">
								<label class="col-md-3">Effective Pixels</label>
								<div class="col-md-9 input-group m-b">
									<input type="text" class="form-control">
									<span class="input-group-addon"><i class="fa fa-trash"></i></span>
								</div>
							</div>
							<!-- End Form Group -->
							<!-- Start Form Group -->
							<div class="form-group row margin-bottom0">
								<label class="col-md-3">Aspect Ratio</label>
								<div class="col-md-9 input-group m-b">
									<input type="text" class="form-control">
									<span class="input-group-addon"><i class="fa fa-trash"></i></span>
								</div>
							</div>
							<!-- End Form Group -->
							<!-- Start Form Group -->
							<div class="form-group row margin-bottom0">
								<label class="col-md-3">Dust Deletion Feature</label>
								<div class="col-md-9 input-group m-b">
									<input type="text" class="form-control">
									<span class="input-group-addon"><i class="fa fa-trash"></i></span>
								</div>
							</div>
							<!-- End Form Group -->
						</div>
					</div>
					<!-- End Collapse -->
					<!-- Start Collapse -->
					<div class="ibox collapsed margin-bottom0">
						<div class="ibox-title">
							<h5>Image Processing During Shooting</h5>
							<div class="ibox-tools">
								<a class="collapse-link">
									<i class="fa fa-chevron-up"></i>
								</a>
							</div>
						</div>
						<div class="ibox-content">
							<!-- Start Form Group -->
							<div class="form-group row margin-bottom0">
								<label class="col-md-3">Type</label>
								<div class="col-md-9 input-group m-b">
									<input type="text" class="form-control">
									<span class="input-group-addon"><i class="fa fa-trash"></i></span>
								</div>
							</div>
							<!-- End Form Group -->
							<!-- Start Form Group -->
							<div class="form-group row margin-bottom0">
								<label class="col-md-3">Effective Pixels</label>
								<div class="col-md-9 input-group m-b">
									<input type="text" class="form-control">
									<span class="input-group-addon"><i class="fa fa-trash"></i></span>
								</div>
							</div>
							<!-- End Form Group -->
							<!-- Start Form Group -->
							<div class="form-group row margin-bottom0">
								<label class="col-md-3">Aspect Ratio</label>
								<div class="col-md-9 input-group m-b">
									<input type="text" class="form-control">
									<span class="input-group-addon"><i class="fa fa-trash"></i></span>
								</div>
							</div>
							<!-- End Form Group -->
							<!-- Start Form Group -->
							<div class="form-group row margin-bottom0">
								<label class="col-md-3">Dust Deletion Feature</label>
								<div class="col-md-9 input-group m-b">
									<input type="text" class="form-control">
									<span class="input-group-addon"><i class="fa fa-trash"></i></span>
								</div>
							</div>
							<!-- End Form Group -->
						</div>
					</div>
					<!-- End Collapse -->
					<!-- Start Collapse -->
					<div class="ibox collapsed margin-bottom0">
						<div class="ibox-title">
							<h5>Viewfinder</h5>
							<div class="ibox-tools">
								<a class="collapse-link">
									<i class="fa fa-chevron-up"></i>
								</a>
							</div>
						</div>
						<div class="ibox-content">
							<!-- Start Form Group -->
							<div class="form-group row margin-bottom0">
								<label class="col-md-3">Type</label>
								<div class="col-md-9 input-group m-b">
									<input type="text" class="form-control">
									<span class="input-group-addon"><i class="fa fa-trash"></i></span>
								</div>
							</div>
							<!-- End Form Group -->
							<!-- Start Form Group -->
							<div class="form-group row margin-bottom0">
								<label class="col-md-3">Effective Pixels</label>
								<div class="col-md-9 input-group m-b">
									<input type="text" class="form-control">
									<span class="input-group-addon"><i class="fa fa-trash"></i></span>
								</div>
							</div>
							<!-- End Form Group -->
							<!-- Start Form Group -->
							<div class="form-group row margin-bottom0">
								<label class="col-md-3">Aspect Ratio</label>
								<div class="col-md-9 input-group m-b">
									<input type="text" class="form-control">
									<span class="input-group-addon"><i class="fa fa-trash"></i></span>
								</div>
							</div>
							<!-- End Form Group -->
							<!-- Start Form Group -->
							<div class="form-group row margin-bottom0">
								<label class="col-md-3">Dust Deletion Feature</label>
								<div class="col-md-9 input-group m-b">
									<input type="text" class="form-control">
									<span class="input-group-addon"><i class="fa fa-trash"></i></span>
								</div>
							</div>
							<!-- End Form Group -->
						</div>
					</div>
					<!-- End Collapse -->
					<!-- Start Collapse -->
					<div class="ibox collapsed margin-bottom0">
						<div class="ibox-title">
							<h5>Autofocus (during viewfinder shooting)</h5>
							<div class="ibox-tools">
								<a class="collapse-link">
									<i class="fa fa-chevron-up"></i>
								</a>
							</div>
						</div>
						<div class="ibox-content">
							<!-- Start Form Group -->
							<div class="form-group row margin-bottom0">
								<label class="col-md-3">Type</label>
								<div class="col-md-9 input-group m-b">
									<input type="text" class="form-control">
									<span class="input-group-addon"><i class="fa fa-trash"></i></span>
								</div>
							</div>
							<!-- End Form Group -->
							<!-- Start Form Group -->
							<div class="form-group row margin-bottom0">
								<label class="col-md-3">Effective Pixels</label>
								<div class="col-md-9 input-group m-b">
									<input type="text" class="form-control">
									<span class="input-group-addon"><i class="fa fa-trash"></i></span>
								</div>
							</div>
							<!-- End Form Group -->
							<!-- Start Form Group -->
							<div class="form-group row margin-bottom0">
								<label class="col-md-3">Aspect Ratio</label>
								<div class="col-md-9 input-group m-b">
									<input type="text" class="form-control">
									<span class="input-group-addon"><i class="fa fa-trash"></i></span>
								</div>
							</div>
							<!-- End Form Group -->
							<!-- Start Form Group -->
							<div class="form-group row margin-bottom0">
								<label class="col-md-3">Dust Deletion Feature</label>
								<div class="col-md-9 input-group m-b">
									<input type="text" class="form-control">
									<span class="input-group-addon"><i class="fa fa-trash"></i></span>
								</div>
							</div>
							<!-- End Form Group -->
						</div>
					</div>
					<!-- End Collapse -->
				</di>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-white" data-dismiss="modal">Back</button>
				<button type="button" class="btn btn-primary">Save</button>
			</div>
		</div>
	</div>
</div>
<!-- End Modal Specification-->

<!-- Start Modal Add Location -->
<div class="modal inmodal" id="modalLocation" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated fadeIn">
            <div class="modal-header">
                <h3 class="modal-title"></h3>
            </div>
            <div class="modal-body padding-bottom10">
                <div class="row">
					<form class="js-modal-location-form">
						<div class="col-md-4">
							<div class="form-group margin-bottom20">
								<label><g:message code="admin.modal.siteSelection.label" default="Select Content Display Locations"/></label>
								<g:select class="select form-control" data-placeholder="Select Country" name="location" data-url="${createLink(controller: 'adminCategory', action: 'getLowestLevelProductCategories')}"
										  from="${siteList as java.util.List<Site>}" optionKey="id" optionValue="name" noSelection="['': 'Select Location']"/>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group margin-bottom20">
								<label><g:message code="admin.modal.productPrice.label" default="Product Price"/></label>
								<input name="sitePrice" type="text" class="form-control" placeholder="${message(code: 'admin.modal.typeInPrice.label', default: 'Type in Price')}">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group margin-bottom20">
								<label><g:message code="admin.modal.publishDate.label" default="Publish Date" /></label><br/>
								<small><em>(<g:message code="admin.modal.publishDate.default.label" default="if empty, Product will be published when approved" />)</em></small>
								<div class="input-group date">
									<input name="sitePublishDate" type="text" class="form-control published-date">
									<span class="input-group-addon">
										<i class="fa fa-calendar"></i>
									</span>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group margin-bottom20">
								<label><g:message code="admin.modal.archiveDate.label" default="Archive Date" /></label><br/><br/>
								<div class="input-group date">
									<input name="siteArchiveDate" type="text" class="form-control" placeholder="${message(code: 'admin.modal.archiveDate.default.label', default: 'default to 12 months')}">
									<span class="input-group-addon">
										<i class="fa fa-calendar"></i>
									</span>
								</div>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group margin-bottom20">
								<label><g:message code="admin.modal.productCategories.label" default="Product Categories"/></label>
								<select name="categories" class="form-control dual_select js-category-selector" multiple>
								</select>
							</div>
						</div>
					</form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal"><g:message code="admin.default.cancel.label" default="Cancel" /></button>
                <button type="button" class="btn btn-primary js-save-btn" data-close="true"><g:message code="admin.default.save.label" default="Save" /></button>
                <button type="button" class="btn btn-primary js-save-btn" data-close="false"><g:message code="admin.default.saveNAdd.label" default="Save & Add Another" /></button>
            </div>
        </div>
    </div>
</div>
<!-- End Modal Add location -->