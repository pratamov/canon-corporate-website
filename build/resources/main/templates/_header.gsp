<%@ page import="canon.Menu" %>
<!-- Start Navigation -->
<div class="overlay-search"></div>
<nav class="navbar navbar-default navbar-sticky bootsnav">

    <div class="container">
        <!-- Start Top Search -->
        <div class="wrap-search hide-medium">
            <div class="input-group">
                <span class="input-group-addon icon-search"><i class="fa fa-search"></i></span>
                <input type="text" class="input-search" placeholder="Search">
                <span class="input-group-addon close-search"><i class="fa fa-times"></i></span>
            </div>
        </div>
        <!-- End Top Search -->

        <!-- Start Attribute Navigation -->
        <div class="attr-nav">
            <ul class="custom hide-medium">
                <li>
                    <g:link controller="home" action="regionSelection" params="[site: params?.site, language: params?.language, segment: params?.segment]">
                        <asset:image src="globe-dekstop.png" class="img-language" width="16" />
                        ${siteSelected} (${(languageSelected as String).toUpperCase()})
                    </g:link>
                </li>
            </ul>
            <ul class="custom hide-medium">
                <li class="${params?.segment == 'consumer' ? 'active' : ''}">
                    <g:link controller="home" action="index" params="${[segment: 'consumer', site: params?.site, lang: params?.lang]}">
                        <g:message code="default.segment.consumer.label" default="Consumer"/>
                    </g:link>
                </li>
                <li class="${params?.segment == 'business' ? 'active' : ''}">
                    <g:link controller="home" action="index" params="${[segment: 'business', site: params?.site, lang: params?.lang]}">
                        <g:message code="default.segment.business.label" default="Business"/>
                    </g:link>
                </li>
            </ul>
            <ul>
                %{--<li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" >
                        <i class="fa fa-shopping-cart"></i>
                        --}%%{--<span class="badge left">3</span>--}%%{--
                    </a>
                </li>
                <li class="hide-medium"><a href="#"><i class="fa fa-comments"></i></a></li>--}%
                <li class="search-nav hide-medium"><a href="#"><i class="fa fa-search"></i></a></li>
            </ul>
            %{--<ul class="custom hide-medium">
                <li><a href="#">Login</a></li>
            </ul>--}%
        </div>
        <!-- End Attribute Navigation -->

        <!-- Start Header Navigation -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
                <i class="fa fa-bars"></i>
            </button>
            <a class="navbar-brand" href="${createLink(controller: "home", action: "index", params: [segment: params?.segment, site: params?.site, lang: params?.lang])}">
                <asset:image src="brand/logo.png" class="logo" alt=""/>
            </a>
        </div>
        <!-- End Header Navigation -->
    </div>
    <div class="wrap-collapse">
        <div class="overlay-search"></div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="navbar-menu">
            <div class="container">
                <div class="clone-search show-medium"></div>
                <div class="state-user show-medium">
                    <g:set var="currentSegment" value="${canon.enums.Segment.getBySlug(params?.segment) ?: canon.enums.Segment.CONSUMER}"/>
                    <g:set var="otherSegment" value="${currentSegment == canon.enums.Segment.CONSUMER ? canon.enums.Segment.BUSINESS : canon.enums.Segment.CONSUMER}"/>
                    <h5>${currentSegment?.value()}</h5>
                    <g:message default="Switch to" code="default.segment.switchTo.label"/>: <strong><g:link controller="home" action="index" params="[site: params?.site, lang: params?.lang, segment: otherSegment?.slug()]">${otherSegment?.value()}</g:link></strong>
                </div>
                <g:if test="${headerMenu?.leftMenus}">
                    <ul class="nav navbar-nav" data-in="fadeInDown" data-out="fadeOutUp">
                        <g:each in="${headerMenu?.leftMenus as java.util.List<canon.Menu>}" var="leftMenu">
                            <li><a href="${leftMenu?.url}">${leftMenu?.name}</a></li>
                        </g:each>
                    </ul>
                </g:if>
                <g:if test="${headerMenu?.rightMenus}">
                    <ul class="nav navbar-nav navbar-right">
                        <g:each in="${headerMenu?.rightMenus as java.util.List<canon.Menu>}" var="rightMenu">
                            <li><a href="${rightMenu?.url}">${rightMenu?.name}</a></li>
                        </g:each>
                    </ul>
                </g:if>
                <div class="text-center show-small language">
                    <g:link controller="home" action="regionSelection" params="[site: params?.site, lang: params?.lang, segment: params?.segment]">
                        ${siteSelected} (${(languageSelected as String).toUpperCase()})
                    </g:link>
                </div>
            </div>
        </div><!-- /.navbar-collapse -->
    </div>
</nav>
<!-- End Navigation -->

<div class="clearfix"></div>

<!-- Start Loading -->
<div class="wrap-loading">
    <ul class="loading">
        <li></li>
        <li></li>
        <li></li>
    </ul>
</div>
<!-- End Loading -->
