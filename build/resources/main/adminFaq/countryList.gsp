<%@ page contentType="text/html;charset=UTF-8" %>
<html>
	<head>
		<title>
			<g:message code="admin.dashboard.title" default="Admin Dashboard"/>
		</title>
		<meta name="layout" content="admin"/>
	</head>
	<body>
	        <!-- Start Page Heading -->
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-6">
                    <h2>FAQ List</h2>
                    <ol class="breadcrumb">
                        <li>
                        	<g:link action="index">FAQ</g:link>
                        </li>
                        <li class="active">
                            <strong>Country List</strong>
                        </li>
                    </ol>
                </div>
            </div>
            <!-- End Page Heading -->
			<!-- Start Content -->
			<div class="wrapper wrapper-content">
				<!-- Start Ibox -->
				<div class="ibox-content margin-bottom60">
					<div class="row">
						<div class="col-sm-8">
							<div class="form-inline">
								<div class="form-group margin-bottom20">
									<label>Date range</label>
									<div class="input-group date">
										<input type="text" class="form-control" value="02-10-2018">
										<span class="input-group-addon">
											<i class="fa fa-calendar"></i>
										</span>
									</div>
								</div>
								<div class="form-group margin-bottom20">
									<label>to</label>
									<div class="input-group date">
										<input type="text" class="form-control" value="02-10-2018">
										<span class="input-group-addon">
											<i class="fa fa-calendar"></i>
										</span>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="input-group"><input type="text" placeholder="Search keyword" class="input-sm form-control"> <span class="input-group-btn">
								<button type="button" class="btn btn-sm btn-primary">Search</button> </span>
							</div>
						</div>
					</div>
					<div class="table-responsive margin-top50">
						<table class="table table-striped sortable">
							<thead>
								<tr>
									<th class="input">
										<div class="checkbox single">
											<input id="checkboxd1" type="checkbox" class="checkall">
											<label for="checkboxd1"></label>
										</div>
									</th>
									<th data-sort="string" class="sort">Locations</th>
									<th data-sort="string" class="sort">Date </th>
								</tr>
							</thead>
							<tbody>
								<g:each in="${sites}" var="site" status="i">
									<tr>
										<td class="input">
											<div class="checkbox single">
												<input id="checkboxd2" type="checkbox">
												<label for="checkboxd2"></label>
											</div>
										</td>
										<td>
											<a href="#" class="single-link">
												<g:link action="categoryList" id="${site.id}">${site.name}</g:link>
											</a>
										</td>
										<td>Last Modified <g:formatDate date="${site.lastUpdated}" type="datetime" style="FULL"/></td>
									</tr>
								</g:each>
							</tbody>
						</table>
					</div>
					<!-- Start Pagination -->
					<div class="post-pagination">
						<ul>
							<li class="nav"><a href="#"><span class="fa fa-angle-left"></span></a></li>
							<li class="page"><input type="number" value="1"></li>
							<li>of</li>
							<li class="count">1</li>
							<li class="nav"><a href="#"><span class="fa fa-angle-right"></span></a></li>
						</ul>
					</div>
					<!-- End Pagination -->
				</div>
				<!-- End Ibox -->
			</div>
			<!-- End Content -->

			<!-- Start Footer -->
	        <div class="footer">
	            <div>
	                <strong>Copyright</strong> &copy; 2017 Canon. All rights reserved.
	            </div>
	        </div>
	        <!-- End Footer -->
	</body>
</html>