<%@ page contentType="text/html;charset=UTF-8" %>
<html>
	<head>
		<title>
			<g:message code="admin.dashboard.title" default="Admin Dashboard"/>
		</title>
		<meta name="layout" content="admin"/>
	</head>
	<body>
		<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#addquestion">Open Modal</button>
		<!-- Start Modal -->
		<div class="modal inmodal" id="addquestion" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog modal-lg">
				<div class="modal-content animated fadeIn">
					<div class="modal-header">
						<h3 class="modal-title">Question detail</h3>
					</div>
					<div class="modal-body padding-bottom10">
						<div class="row margin-bottom30">
							<div class="col-md-4">
								<label>Publish Date</label>
								<div class="input-group date">
									<input type="text" class="form-control" value="Select Date">
									<span class="input-group-addon">
									<i class="fa fa-calendar"></i>
									</span>
								</div>
							</div>
							<div class="col-md-4">
								<label>Archive Date</label>
								<div class="input-group date">
									<input type="text" class="form-control" value="Select Date">
									<span class="input-group-addon">
									<i class="fa fa-calendar"></i>
									</span>
								</div>
							</div>
						</div>
						<div class="row margin-bottom20">
							<div class="col-md-12">
								<label>Question - EN</label>
								<textarea class="form-control" rows="3" placeholder="Type In Question"></textarea>
							</div>
						</div>
						<div class="row margin-bottom30">
							<div class="col-md-12">
								<label>Question - CH</label>
								<textarea class="form-control" rows="3" placeholder="Type In Question"></textarea>
							</div>
						</div>
						<div class="row margin-bottom20">
							<div class="col-md-12">
								<label>Answer - EN</label>
								<div class="summernote">
									Type In Answer
								</div>
							</div>
						</div>
						<div class="row margin-bottom3">
							<div class="col-md-12">
								<label>Answer - CH</label>
								<div class="summernote">
									Type In Answer
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-white" data-dismiss="modal">Cancel</button>
						<button type="button" class="btn btn-primary">Save</button>
					</div>
				</div>
			</div>
		</div>
		<!-- End Modal -->
		<!-- Start Content -->
		<div class="wrapper wrapper-content">
			<!-- Start Ibox -->
			<div class="ibox-content margin-bottom60">
				<div class="row">
					<div class="col-sm-8">
						<div class="form-inline">
							<div class="form-group margin-bottom20">
								<label>Date range</label>
								<div class="input-group date">
									<input type="text" class="form-control" value="02-10-2018">
									<span class="input-group-addon">
									<i class="fa fa-calendar"></i>
									</span>
								</div>
							</div>
							<div class="form-group margin-bottom20">
								<label>to</label>
								<div class="input-group date">
									<input type="text" class="form-control" value="02-10-2018">
									<span class="input-group-addon">
									<i class="fa fa-calendar"></i>
									</span>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-4">
						<div class="input-group"><input type="text" placeholder="Search keyword" class="input-sm form-control"> <span class="input-group-btn">
							<button type="button" class="btn btn-sm btn-primary">Search</button> </span>
						</div>
					</div>
				</div>
				<div class="table-responsive margin-top50">
					<table class="table table-striped sortable">
						<thead>
							<tr>
								<th class="input">
									<div class="checkbox single">
										<input id="checkboxd1" type="checkbox" class="checkall">
										<label for="checkboxd1"></label>
									</div>
								</th>
								<th data-sort="string" class="sort">Category</th>
								<th data-sort="string" class="sort">State</th>
								<th data-sort="string" class="sort">Date</th>
							</tr>
						</thead>
						<tbody>
							<g:each in="${categories}" var="category" status="i">
								<tr>
									<td class="input">
										<div class="checkbox single">
											<input id="checkboxd2" type="checkbox">
											<label for="checkboxd2"></label>
										</div>
									</td>
									<td>
										<a href="#" class="single-link">
											<g:link action="faqCategoryDetail" id="${category.id}">${category.name}</g:link>
										</a>
									</td>
									<td>${category.status}</td>
									<td>Last Modified <g:formatDate date="${category.lastUpdated}" type="datetime" style="FULL"/></td>
								</tr>
							</g:each>
							<!--
							<tr>
								<td class="input">
									<div class="checkbox single">
										<input id="checkboxd2" type="checkbox">
										<label for="checkboxd2"></label>
									</div>
								</td>
								<td><a href="#" class="single-link">Product</a></td>
								<td>Last Modified 21 Nov 2017 1:48 PM(+8 GMT)</td>
							</tr>
							-->
						</tbody>
					</table>
				</div>
				<!-- Start Pagination -->
				<div class="post-pagination">
					<ul>
						<li class="nav"><a href="#"><span class="fa fa-angle-left"></span></a></li>
						<li class="page"><input type="number" value="1"></li>
						<li>of</li>
						<li class="count">1</li>
						<li class="nav"><a href="#"><span class="fa fa-angle-right"></span></a></li>
					</ul>
				</div>
				<!-- End Pagination -->
			</div>
			<!-- End Ibox -->
		</div>
		<!-- End Content -->
	</body>
</html>