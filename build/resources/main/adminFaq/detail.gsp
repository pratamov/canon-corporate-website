<%@ page contentType="text/html;charset=UTF-8" %>
<html>
	<head>
		<title>
			<g:message code="admin.dashboard.title" default="Admin Dashboard"/>
		</title>
		<meta name="layout" content="admin"/>
	</head>
	<body>
            <!-- Start Page Heading -->
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-6">
                    <h2>${faq.site.name} - ${faq.name}</h2>
                    <ol class="breadcrumb">
                        <li>
                        	<g:link action="index">FAQ</g:link>
                        </li>
                        <li>
                        	<g:link action="countryList">Country List</g:link>
                        </li>
                        <li>
                        	<g:link action="categoryList" params="[id: faq.site.id]">Category List</g:link>
                        </li>
                        <li class="active">
                            <strong>Detail</strong>
                        </li>
                    </ol>
                </div>
            </div>
            <!-- End Page Heading -->
		
			<!-- Start Modal -->
			<!-- Create new Faq Question-Answer -->
			<div class="modal inmodal" id="addquestion" tabindex="-1" role="dialog" aria-hidden="true">
				<div class="modal-dialog modal-lg">
					<div class="modal-content animated fadeIn">
						<g:form>
							<input name="faq_id" type="hidden" value="${faq.id}"/>
							<div class="modal-header">
								<h3 class="modal-title">Question detail</h3>
							</div>
							<div class="modal-body padding-bottom10">
								<div class="row margin-bottom30">
									<div class="col-md-4">
										<label>Publish Date</label>
										<div class="input-group date">
											<input name="faqItem_publishedDate" type="text" class="form-control" placeholder="Select Date">
											<span class="input-group-addon">
												<i class="fa fa-calendar"></i>
											</span>
										</div>
									</div>
									<div class="col-md-4">
										<label>Archive Date</label>
										<div class="input-group date">
											<input name="faqItem_archivedDate" type="text" class="form-control" placeholder="Select Date">
											<span class="input-group-addon">
												<i class="fa fa-calendar"></i>
											</span>
										</div>
									</div>
									<div class="col-md-4">
										<label>Label</label>
										<div class="input-group">
											<input id="addquestion_faqItem_label" name="faqItem_label" type="text" class="form-control" placeholder="Type In Label" required>
										</div>
									</div>
								</div>
								<div class="row margin-bottom20">
									<div class="col-md-12">
										<label>Question - EN</label>
										<textarea name="faqItem_content" class="form-control" rows="3" placeholder="Type In Question" required></textarea>
									</div>
								</div>
								<div class="row margin-bottom30">
									<div class="col-md-12">
										<label>Question - CH</label>
										<textarea name="faqItemTranslation_content" class="form-control" rows="3" placeholder="Type In Question" required></textarea>
									</div>
								</div>
								<div class="row margin-bottom20">
									<div class="col-md-12">
										<label>Answer - EN</label>
										<textarea name="faqItemAnswerEN_content" class="summernote" rows="3" placeholder="Type In Answer" required></textarea>
									</div>
								</div>
								<div class="row margin-bottom3">
									<div class="col-md-12">
										<label>Answer - CH</label>
										<textarea name="faqItemAnswerCH_content" class="summernote" rows="3" placeholder="Type In Answer" required></textarea>
									</div>
								</div>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-white" data-dismiss="modal">Cancel</button>
								<g:actionSubmit class="btn btn-primary" value="Save" action="addQuestion"/>
							</div>
						</g:form>
					</div>
				</div>
			</div>
			<div class="modal inmodal" id="updatequestion" tabindex="-1" role="dialog" aria-hidden="true">
				<div class="modal-dialog modal-lg">
					<div class="modal-content animated fadeIn">
						<g:form>
							<input id="updatequestion_faqItem_id" name="faqItem_id" type="hidden"/>
							<input id="updatequestion_faq_id" name="faq_id" type="hidden" value="${faq.id}"/>
							<div class="modal-header">
								<h3 class="modal-title">Question detail</h3>
							</div>
							<div class="modal-body padding-bottom10">
								<div class="row margin-bottom30">
									<div class="col-md-4">
										<label>Publish Date</label>
										<div class="input-group date">
											<input id="updatequestion_faqItem_publishedDate" name="faqItem_publishedDate" type="text" class="form-control" placeholder="Select Date">
											<span class="input-group-addon">
												<i class="fa fa-calendar"></i>
											</span>
										</div>
									</div>
									<div class="col-md-4">
										<label>Archive Date</label>
										<div class="input-group date">
											<input id="updatequestion_faqItem_archivedDate" name="faqItem_archivedDate" type="text" class="form-control" placeholder="Select Date">
											<span class="input-group-addon">
												<i class="fa fa-calendar"></i>
											</span>
										</div>
									</div>
									<div class="col-md-4">
										<label>Label</label>
										<div class="input-group">
											<input id="updatequestion_faqItem_label" name="faqItem_label" type="text" class="form-control" placeholder="Type In Label" required>
										</div>
									</div>
								</div>
								<div class="row margin-bottom20">
									<div class="col-md-12">
										<label>Question - EN</label>
										<textarea id="updatequestion_faqItem_content" name="faqItem_content" class="form-control" rows="3" placeholder="Type In Question" required></textarea>
									</div>
								</div>
								<div class="row margin-bottom30">
									<div class="col-md-12">
										<label>Question - CH</label>
										<textarea id="updatequestion_faqItemTranslation_content" name="faqItemTranslation_content" class="form-control" rows="3" placeholder="Type In Question" required></textarea>
									</div>
								</div>
								<div class="row margin-bottom20">
									<div class="col-md-12">
										<label>Answer - EN</label>
										<textarea id="updatequestion_faqItemAnswerEN_content" name="faqItemAnswerEN_content" class="summernote" rows="3" placeholder="Type In Answer" required></textarea>
									</div>
								</div>
								<div class="row margin-bottom3">
									<div class="col-md-12">
										<label>Answer - CH</label>
										<textarea id="updatequestion_faqItemAnswerCH_content" name="faqItemAnswerCH_content" class="summernote" rows="3" placeholder="Type In Answer" required></textarea>
									</div>
								</div>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-white" data-dismiss="modal">Cancel</button>
								<g:actionSubmit class="btn btn-primary" value="Save" action="updateQuestion"/>
							</div>
						</g:form>
					</div>
				</div>
			</div>
			<!-- End Modal -->

            <g:form>
				<input name="faq_id" type="hidden" value="${faq.id}"/>
				<!-- Start Content -->
				<div class="wrapper wrapper-content">
					<!-- Start Ibox -->
					<div class="ibox-title">
						<h3 class="title">Category Detail</h3>
					</div>
					<div class="ibox-content margin-bottom30">
						<div class="form-group margin-bottom20">
							<label>Name - EN</label>
							<input name="faq_name" type="text" class="form-control" placeholder="Category Name" value="${category.english.name}" required />
						</div>
						<div class="form-group">
							<label>Name - CH</label>
							<input name="faqTranslation_name" type="text" class="form-control" placeholder="Category Name" value="${category.chinese.name}" required />
						</div>
					</div>
					<!-- End Ibox -->
					<!-- Start Ibox -->
					<div class="ibox-title">
						<h3 class="title">FAQ Content</h3>
					</div>
					<div class="ibox-content margin-bottom80">
						<div class="panel-border"  style="height:600px; overflow-y:scroll">
							<% label_index = 0 %>
							<g:if test="${0 == labels.size()}">
								<a href="#" class="btn btn-primary" data-toggle="modal" data-target="#addquestion">Create New Question</a>
							</g:if>
							<g:each in="${labels}" var="label" status="l">
								<% label_index += 1 %>
								<!-- Start Collapse -->
								<div class="ibox collapsed margin-bottom0">
									<div class="ibox-title">
										<div class="row">
											<div class="col-md-10 col-sm-9">
												<g:if test="${label_index == labels.size()}">
													<a href="#" data-toggle="modal" data-target="#addquestion" data-label="" class="add">
														<i class="fa fa-plus"></i>
													</a>
												</g:if>
												<input name="labels[${label}]" type="text" class="form-control" value="${label}" />
											</div>
											<div class="col-md-2 col-sm-3">
												<div class="ibox-tools">
													<!--a href="#" class="single-link sort"></a-->
													<a class="collapse-link">
													<i class="fa fa-chevron-up"></i>
													</a>
												</div>
											</div>
										</div>
									</div>
									<% content_index = 0 %>
									<g:each in="${contents}" var="content">
										<g:if test="${label == content.question.english.label}">
											<div class="ibox-content">
												<div class="row">
													<% content_index += 1 %>
													<div class="col-md-10 col-sm-9">
														<div class="row">
															<div class="col-md-2 label-panel">
																<a id="question${label}${content_index}" href="#" data-toggle="modal" data-target="#addquestion" data-label="${label}" class="add addQuestion" style="display:none">
																	<i class="fa fa-plus"></i>
																</a>
																<label>
																	<a class="updateQuestion" href="#" data-toggle="modal" data-target="#updatequestion">
																		Question ${content_index}
																	</a>
																	<input type="hidden" 
																		class="updatequestion_faqItem_id" 
																		value="${content.question.english.id}"/>
																	<input type="hidden" 
																		class="updatequestion_faqItem_publishedDate" 
																		value="${content.question.english.publishedDate}"/>
																	<input type="hidden" 
																		class="updatequestion_faqItem_archivedDate" 
																		value="${content.question.english.archivedDate}"/>
																	<input type="hidden" 
																		class="updatequestion_faqItem_label" 
																		value="${content.question.english.label}"/>
																	<input type="hidden" 
																		class="updatequestion_faqItem_content" 
																		value="${content.question.english.content}"/>
																	<input type="hidden" 
																		class="updatequestion_faqItemTranslation_content" 
																		value="${content.question.chinese.content}"/>
																	<input type="hidden" 
																		class="updatequestion_faqItemAnswerEN_content" 
																		value="${content.answer.english.content}"/>
																	<input type="hidden" 
																		class="updatequestion_faqItemAnswerCH_content" 
																		value="${content.answer.chinese.content}"/>
																</label>
															</div>
															<div class="col-md-10">
																<input type="text" class="form-control" value="${content.question.english.content}" disabled />
															</div>
														</div>
													</div>
													<div class="col-md-2 col-sm-3">
														<div class="ibox-tools">
															<!--a href="#" class="single-link sort"></a-->
															<g:link 
																class="single-link"
																action="archiveQuestion" 
																onclick="if (confirm('Delete selected item?')){return true;}else{event.stopPropagation(); event.preventDefault();};"
																params="[faqItem_id: content.question.english.id]">
																<i class="fa fa-trash"></i>
															</g:link>
															<a class="collapse-link">
																<i class="fa fa-chevron-up"></i>
															</a>
														</div>
													</div>
												</div>
											</div>
										</g:if>
									</g:each>
									<script>
										document.getElementById('question${label}${content_index}').style.display = 'block';
									</script>
								</div>
								<!-- End Collapse -->
							</g:each>
						</div>
					</div>
					<!-- End Ibox -->
				</div>
				<!-- End Content -->
				<!-- Start Footer -->
		        <div class="footer" style="z-index: 2">
		        	<div>
	                    <div class="pull-right">
	                    	<g:link class="btn btn-default margin-left10" action="detail" params="[id: faq.id]">Cancel</g:link>
	                    	<g:actionSubmit class="btn btn-primary margin-left10" value="Save" action="updateCategory"/>
	                    </div>
	                    <span class="copy"><strong>Copyright</strong> &copy; 2017 Canon. All rights reserved.</span>
	                </div>
		        </div>
	        </g:form>
	        <!-- End Footer -->
	</body>
</html>