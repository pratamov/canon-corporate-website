<%@ page contentType="text/html;charset=UTF-8" %>
<html>
	<head>
		<title>
			<g:message code="admin.dashboard.title" default="Admin Dashboard"/>
		</title>
		<meta name="layout" content="admin"/>
	</head>
	<body>
		<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#addquestion">Open Modal</button>
		<!-- Start Modal -->
		<div class="modal inmodal" id="addquestion" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog modal-lg">
				<div class="modal-content animated fadeIn">
					<div class="modal-header">
						<h3 class="modal-title">Question detail</h3>
					</div>
					<div class="modal-body padding-bottom10">
						<div class="row margin-bottom30">
							<div class="col-md-4">
								<label>Publish Date</label>
								<div class="input-group date">
									<input type="text" class="form-control" value="Select Date">
									<span class="input-group-addon">
									<i class="fa fa-calendar"></i>
									</span>
								</div>
							</div>
							<div class="col-md-4">
								<label>Archive Date</label>
								<div class="input-group date">
									<input type="text" class="form-control" value="Select Date">
									<span class="input-group-addon">
									<i class="fa fa-calendar"></i>
									</span>
								</div>
							</div>
						</div>
						<div class="row margin-bottom20">
							<div class="col-md-12">
								<label>Question - EN</label>
								<textarea class="form-control" rows="3" placeholder="Type In Question"></textarea>
							</div>
						</div>
						<div class="row margin-bottom30">
							<div class="col-md-12">
								<label>Question - CH</label>
								<textarea class="form-control" rows="3" placeholder="Type In Question"></textarea>
							</div>
						</div>
						<div class="row margin-bottom20">
							<div class="col-md-12">
								<label>Answer - EN</label>
								<div class="summernote">
									Type In Answer
								</div>
							</div>
						</div>
						<div class="row margin-bottom3">
							<div class="col-md-12">
								<label>Answer - CH</label>
								<div class="summernote">
									Type In Answer
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-white" data-dismiss="modal">Cancel</button>
						<button type="button" class="btn btn-primary">Save</button>
					</div>
				</div>
			</div>
		</div>
		<!-- End Modal -->
		<!-- Start Content -->
		<div class="wrapper wrapper-content">
			<!-- Start Ibox -->
			<div class="ibox-title">
				<h3 class="title">Category Detail</h3>
			</div>
			<div class="ibox-content margin-bottom30">
				<div class="form-group margin-bottom20">
					<label>Name - EN</label>
					<input type="text" class="form-control" placeholder="Category Name" value="${categoryDetail.contentEN}" />
				</div>
				<div class="form-group">
					<label>Name - CH</label>
					<input type="text" class="form-control" placeholder="Category Name" value="${categoryDetail.contentZH}" />
				</div>
				<div class="pull-right">
                    <button class="btn btn-default margin-left10">Cancel</button>
                    <button class="btn btn-primary margin-left10">Save</button>
                </div>
			</div>
			<!-- End Ibox -->
			<!-- Start Ibox -->
			<div class="ibox-title">
				<h3 class="title">FAQ Content</h3>
			</div>
			<div class="ibox-content margin-bottom80">
				<div class="panel-border"  style="height:600px; overflow-y:scroll">

					<g:each in="${labels}" var="label" status="l">
						<!-- Start Collapse -->
						<div class="ibox collapsed margin-bottom0">
							<div class="ibox-title">
								<div class="row">
									<div class="col-md-10 col-sm-9">
										<input type="text" class="form-control" value="${label}" />
									</div>
									<div class="col-md-2 col-sm-3">
										<div class="ibox-tools">
											<a href="#" class="single-link sort"></a>
											<a href="#" class="single-link"><i class="fa fa-trash"></i></a>
											<a class="collapse-link">
											<i class="fa fa-chevron-up"></i>
											</a>
										</div>
									</div>
								</div>
							</div>
							<div class="ibox-content">
								<div class="row">
									<g:set var="index" value="${0}"></g:set>
									<g:each in="${faqContents}" var="content" status="c">
										<g:if test="${label == content.label}">
											<g:set var="index" value="${index+1}"></g:set>
											<div class="col-md-10 col-sm-9">
												<div class="row">
													<div class="col-md-2 label-panel">
														<a href="#" class="add"><i class="fa fa-plus"></i></a>
														<label>Question ${index}</label>
													</div>
													<div class="col-md-10">
														<input type="text" class="form-control" value="${content.contentEN}" disabled />
													</div>
												</div>
											</div>
										</g:if>
									</g:each>
									<div class="col-md-2 col-sm-3">
										<div class="ibox-tools">
											<a href="#" class="single-link sort"></a>
											<a href="#" class="single-link"><i class="fa fa-trash"></i></a>
											<a class="collapse-link">
											<i class="fa fa-chevron-up"></i>
											</a>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- End Collapse -->
					</g:each>
				</div>
			</div>
			<!-- End Ibox -->
		</div>
		<!-- End Content -->
	</body>
</html>