<%@ page contentType="text/html;charset=UTF-8" %>
<html>
	<head>
		<title>
			<g:message code="admin.dashboard.title" default="Admin Dashboard"/>
		</title>
		<meta name="layout" content="admin"/>
	</head>
	<body>
		<!-- Start Page Heading -->
		<div class="row wrapper border-bottom white-bg page-heading">
		    <div class="col-lg-6">
		        <h2>${site.name}</h2>
		        <ol class="breadcrumb">
		            <li>
                    	<g:link action="index">FAQ</g:link>
		            </li>
		            <li>
                    	<g:link action="countryList">Country List</g:link>
		             </li>
		            <li class="active">
		                <a href="#">Category List</a>
		            </li>
		        </ol>
		    </div>
		    <div class="col-lg-6 text-right">
		        <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#addcategory">Create New Category</a>
		    </div>
		</div>
		<!-- End Page Heading -->

		<!-- Start Modal -->
		<div class="modal inmodal" id="addcategory" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog modal-lg">
				<div class="modal-content animated fadeIn">
					<g:form>
						<input name="site_id" type="hidden" value="${site.id}"/>
						<div class="modal-header">
							<h3 class="modal-title">Category detail</h3>
						</div>
						<div class="modal-body padding-bottom10">
							<div class="row margin-bottom20">
								<div class="col-md-12">
									<label>Name - EN</label>
									<textarea name="faq_name" class="form-control" rows="3" placeholder="Type In Name" required></textarea>
								</div>
							</div>
							<div class="row margin-bottom30">
								<div class="col-md-12">
									<label>Name - CH</label>
									<textarea name="faqTranslation_name" class="form-control" rows="3" placeholder="Type In Name" required></textarea>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-white" data-dismiss="modal">Cancel</button>
							<g:actionSubmit class="btn btn-primary" value="Save" action="addCategory" />
						</div>
					</g:form>
				</div>
			</div>
		</div>
		<!-- End Modal -->
		<!-- Start Content -->
		<div class="wrapper wrapper-content">
			<!-- Start Ibox -->
			<div class="ibox-content margin-bottom60">
				<div class="row">
					<div class="col-sm-8">
						<div class="form-inline">
							<div class="form-group margin-bottom20">
								<label>Date range</label>
								<div class="input-group date">
									<input type="text" class="form-control" value="02-10-2018">
									<span class="input-group-addon">
										<i class="fa fa-calendar"></i>
									</span>
								</div>
							</div>
							<div class="form-group margin-bottom20">
								<label>to</label>
								<div class="input-group date">
									<input type="text" class="form-control" value="02-10-2018">
									<span class="input-group-addon">
									<i class="fa fa-calendar"></i>
									</span>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-4">
						<div class="input-group"><input type="text" placeholder="Search keyword" class="input-sm form-control"> <span class="input-group-btn">
							<button type="button" class="btn btn-sm btn-primary">Search</button> </span>
						</div>
					</div>
				</div>
				<g:form>
					<div class="row">
						<div class="col-sm-7 margin-bottom20">
			                <div class="btn-group">
			                	<g:link action="categoryList" id="${site.id}" params="[archived: false]" class="${!archived ? 'active ' : ''}btn btn-sm btn-white">
			                		All (${unarchivedCount})
			                	</g:link>
			                	<g:link action="categoryList" id="${site.id}" params="[archived: true]" class="${archived ? 'active ' : ''}btn btn-sm btn-white">
			                		Archive (${archivedCount})
			                	</g:link>
			                </div>
			            </div>
			            <div class="col-sm-5 text-right">
			                <div class="btn-group">
								<input name="site_id" type="hidden" value="${site.id}"/>
			                    <g:actionSubmit class="btn btn-sm btn-white" value="Move to Archive" action="archiveCategory" />
			                </div>
			            </div>
					</div>
					<div class="table-responsive margin-top50">
						<table class="table table-striped sortable">
							<thead>
								<tr>
									<th class="input">
										<div class="checkbox single">
											<input id="checkboxd0" type="checkbox" class="checkall">
											<label for="checkboxd0"></label>
										</div>
									</th>
									<th data-sort="string" class="sort">Category</th>
									<th data-sort="string" class="sort">State</th>
									<th data-sort="string" class="sort">Date</th>
								</tr>
							</thead>
							<tbody>
								<g:each in="${categories}" var="category" status="i">
									<tr>
										<td class="input">
											<div class="checkbox single">
												<input name="faq_id_list" value="${category.english.id}" id="checkboxd${category.english.id}" type="checkbox">
												<label for="checkboxd${category.english.id}"></label>
											</div>
										</td>
										<td>
											<a href="#" class="single-link">
												<g:link action="detail" id="${category.english.id}">${category.english.name}</g:link>
											</a>
										</td>
										<td>${category.english.status}</td>
										<td>Last Modified <g:formatDate date="${category.english.lastUpdated}" type="datetime" style="FULL"/>
										</td>
									</tr>
								</g:each>
							</tbody>
						</table>
					</div>
				</g:form>
				<!-- Start Pagination -->
				<div class="post-pagination">
					<ul>
						<li class="nav"><a href="#"><span class="fa fa-angle-left"></span></a></li>
						<li class="page"><input type="number" value="1"></li>
						<li>of</li>
						<li class="count">1</li>
						<li class="nav"><a href="#"><span class="fa fa-angle-right"></span></a></li>
					</ul>
				</div>
				<!-- End Pagination -->
			</div>
			<!-- End Ibox -->
		</div>
		<!-- End Content -->
		<!-- Start Footer -->
        <div class="footer">
            <div>
                <strong>Copyright</strong> &copy; 2017 Canon. All rights reserved.
            </div>
        </div>
        <!-- End Footer -->
	</body>
</html>