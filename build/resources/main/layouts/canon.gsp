<!doctype html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en">  <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="keywords" content="">

    <link rel="icon" type="image/x-ico" href="/assets/favicon.ico"/>

    <g:layoutHead/>
    <title><g:layoutTitle default="Canon"/></title>

    <!-- CSS -->
    <asset:stylesheet src="application.css"/>

    <!--[if lt IE 9]>
        <asset:javascript src="html5shiv.js"/>
        <asset:javascript src="respond.js"/>
    <![endif]-->

</head>
<body class="demo">
    <g:render template="/templates/header"/>
    <g:render template="/templates/mustache" />
    <g:layoutBody/>
    <g:render template="/templates/footer"/>

    <!-- START JAVASCRIPT -->
    <asset:javascript src="application.js"/>

</body>
</html>
