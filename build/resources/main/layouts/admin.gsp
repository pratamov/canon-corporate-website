<!doctype html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en">  <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="keywords" content="">

    <g:layoutHead/>
    <title><g:layoutTitle default="Canon"/></title>

    <!-- CSS -->
    <asset:stylesheet src="css/application.css"/>

    <!--[if lt IE 9]>
        <asset:javascript src="html5shiv.js"/>
        <asset:javascript src="respond.js"/>
    <![endif]-->

</head>
<body>

<!-- Start Wrapper -->
<div id="wrapper">

    <!-- Start Nav -->
    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav metismenu" id="side-menu">
                <li class="nav-header">
                    <div class="dropdown profile-element">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold">Theodore Twombly</strong>
                            </span> <span class="text-muted text-xs block">theodore@canon.com.sg</span> </span> </a>
                        <ul class="dropdown-menu animated fadeInRight m-t-xs">
                            <li><a href="#">Logout</a></li>
                        </ul>
                    </div>
                    <div class="logo-element">
                        IN+
                    </div>
                </li>
                <li class="${controllerName=='admin' ? 'active' : ''}">
                    <g:link controller="admin" action="index"><span class="nav-label">Dashboard</span></g:link>
                </li>
                <li>
                    <a href="#"><span class="nav-label">Administratve</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="#">Dropdown menu</a></li>
                        <li><a href="#">Dropdown menu</a></li>
                        <li><a href="#">Dropdown menu</a></li>
                        <li><a href="#">Dropdown menu</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#"><span class="nav-label">Pages</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="#">Dropdown menu</a></li>
                        <li><a href="#">Dropdown menu</a></li>
                        <li><a href="#">Dropdown menu</a></li>
                        <li><a href="#">Dropdown menu</a></li>
                    </ul>
                </li>
                <li class="${controllerName=='adminProduct' ? 'active' : ''}">
                    <a href="#">
                        <span class="nav-label">
                            <g:message code="admin.menu.products.label" default="Products"/>
                        </span>
                        <span class="fa arrow"></span>
                    </a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="#">Product Category</a></li>
                        <li class="${controllerName=='adminProduct' ? 'active' : ''}">
                            <g:link controller="adminProduct" action="index">
                                <g:message code="admin.menu.productList.label" default="Product List"/>
                            </g:link>
                        </li>
                        <li><a href="#">Product Template</a></li>
                        <li><a href="#">Bundle</a></li>
                        <li><a href="#">Catalogue</a></li>
                        <li><a href="#">Promotion</a></li>
                        <li><a href="#">Cross-sell</a></li>
                        <li><a href="#">Product Specs</a></li>
                        <li><a href="#">Product Filter</a></li>
                        <li><a href="#">Product Settings</a></li>
                    </ul>
                </li>
                <li class="${controllerName == 'adminArticles' ? 'active' : ''}">
                    <a href="#"><span class="nav-label">Articles</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="#"><g:message code="admin.menu.articleCategory.label" default="Article Category"/></a></li>
                        <li class="${controllerName == 'adminArticle' && ['index', 'create'].contains(actionName) ? 'active' : ''}"><g:link controller="adminArticle" action="index"><g:message code="admin.menu.articleList.label" default="Article List"/></g:link></li>
                        <li><a href="#"><g:message code="admin.menu.articleTemplate.label" default="Article Template"/></a></li>
                        <li><a href="#"><g:message code="admin.menu.articleSettings.label" default="Article Settings"/></a></li>
                    </ul>
                </li>
                <li>
                    <a href="#"><span class="nav-label">Events</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="#">Dropdown menu</a></li>
                        <li><a href="#">Dropdown menu</a></li>
                        <li><a href="#">Dropdown menu</a></li>
                        <li><a href="#">Dropdown menu</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#"><span class="nav-label">Courses</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="#">Dropdown menu</a></li>
                        <li><a href="#">Dropdown menu</a></li>
                        <li><a href="#">Dropdown menu</a></li>
                        <li><a href="#">Dropdown menu</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#"><span class="nav-label">Digital Asset Mgmt</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="#">Dropdown menu</a></li>
                        <li><a href="#">Dropdown menu</a></li>
                        <li><a href="#">Dropdown menu</a></li>
                        <li><a href="#">Dropdown menu</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#"><span class="nav-label">Dealers</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="#">Dropdown menu</a></li>
                        <li><a href="#">Dropdown menu</a></li>
                        <li><a href="#">Dropdown menu</a></li>
                        <li><a href="#">Dropdown menu</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#"><span class="nav-label">FAQ</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><g:link controller="adminFaq">List</g:link></li>
                        <li><a href="#">Settings</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#"><span class="nav-label">Forms</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="#">Dropdown menu</a></li>
                        <li><a href="#">Dropdown menu</a></li>
                        <li><a href="#">Dropdown menu</a></li>
                        <li><a href="#">Dropdown menu</a></li>
                    </ul>
                </li>
                <li>
                    <a href="index.html"><span class="nav-label">Lists</span></a>
                </li>
                <li>
                    <a href="#"><span class="nav-label">Errors</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="#">Dropdown menu</a></li>
                        <li><a href="#">Dropdown menu</a></li>
                        <li><a href="#">Dropdown menu</a></li>
                        <li><a href="#">Dropdown menu</a></li>
                    </ul>
                </li>
                <li>
                    <a href="index.html"><span class="nav-label">Reports</span></a>
                </li>
                <li>
                    <a href="index.html"><span class="nav-label">Feedback</span></a>
                </li>
                <li>
                    <a href="index.html"><span class="nav-label">Contact Us</span></a>
                </li>
                <li>
                    <a href="#"><span class="nav-label">Settings</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="#">Dropdown menu</a></li>
                        <li><a href="#">Dropdown menu</a></li>
                        <li><a href="#">Dropdown menu</a></li>
                        <li><a href="#">Dropdown menu</a></li>
                    </ul>
                </li>
            </ul>

        </div>
    </nav>
    <!-- End Nav -->

    <!-- Start Page Wrapper -->
    <div id="page-wrapper" class="gray-bg">
        <!-- Start Head -->
        <div class="row border-bottom">
            <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header show-medium">
                    <a class="navbar-minimalize minimalize-styl-2 btn btn-primary" href="#"><i class="fa fa-bars"></i></a>
                </div>
                <ul class="nav navbar-top-links navbar-right">
                    <li>
                        <a href="#">
                            <i class="fa fa-question-circle"></i> Help
                        </a>
                    </li>
                    <li>
                        <g:link controller="logout" action="index">
                            <i class="fa fa-sign-out"></i> Log out</g:link>
                    </li>
                </ul>
            </nav>
        </div>
        <!-- Start Head -->

        <g:layoutBody/>

        <!-- Start Footer -->
        <div class="footer">
            <div>
                <g:pageProperty name="page.footer"/>
                <strong>Copyright</strong> &copy; 2017 Canon. All rights reserved.
            </div>
        </div>
        <!-- End Footer -->

    </div>
    <!-- End Page Wrapper -->

</div>
<!-- End Wrapper -->

<g:render template="/templates/admin/dam" />
<g:render template="/templates/admin/admin_mustache" />
<g:render template="/templates/admin/admin_modals" />

<!-- Grails-app -->
<asset:javascript src="js/application.js"/>

<g:if test="${actionName == 'inline'}">
<!-- Start of KEditor styles -->
<div data-type="keditor-style" data-href="${asset.assetPath(src: 'application.css')}"></div>
<div data-type="keditor-style" data-href="${asset.assetPath(src: 'css/plugins/keditor/keditor-1.1.5.min.css')}"></div>
<script data-type="keditor-style" type="text/css">
    body.opened-keditor-sidebar .keditor-content-area {
        padding: 0 20px 0 0;
    }
    body.opened-keditor-sidebar .keditor-content-area .container {
        width: 100%;
    }
    .inner-page.absolute {
        position: relative;
    }
    [data-disable="true"].keditor-component:hover .keditor-toolbar {
        display: none;
    }
    .keditor-container.showed-keditor-toolbar > .keditor-toolbar {
        display: none;
    }
    .section.keditor-ui {
        padding: 30px 0;
    }
</script>
<!-- End of KEditor styles -->

<!-- Start of KEditor scripts -->
<script type="text/javascript">
    var bsTooltip = $.fn.tooltip;
    var bsButton = $.fn.button;
</script>
<script src="/assets/js/plugins/jquery-ui-1.11.4/jquery-ui.min.js"></script>
<script type="text/javascript">
    $.widget.bridge('uibutton', $.ui.button);
    $.widget.bridge('uitooltip', $.ui.tooltip);
    $.fn.tooltip = bsTooltip;
    $.fn.button = bsButton;
</script>
<script type="text/javascript" src="/assets/js/plugins/jquery.nicescroll-3.6.6/jquery.nicescroll.min.js"></script>
<script type="text/javascript" src="/assets/js/plugins/ckeditor-4.5.6/ckeditor.js"></script>
<script type="text/javascript" src="/assets/js/plugins/ckeditor-4.5.6/adapters/jquery.js"></script>
<script type="text/javascript" src="/assets/js/plugins/keditor/keditor-1.1.5.js"></script>
<script type="text/javascript" src="/assets/js/plugins/keditor/keditor-components-1.1.5.js"></script>
<asset:javascript src="js/itools.js"/>
<asset:javascript src="js/icomp.js"/>
<asset:javascript src="js/inline.js"/>
<!-- End of KEditor scripts -->



</g:if>

<!-- Start of FAQ Question Update -->

<script>
    $('.addQuestion').click(function() {
        var label = $(this).attr('data-label');
        $('#addquestion_faqItem_label').val(label);
    });
    $('.updateQuestion').click(function() {
        $('#updatequestion_faqItem_id').val($(this).siblings('.updatequestion_faqItem_id').val());
        $('#updatequestion_faqItem_publishedDate').val($(this).siblings('.updatequestion_faqItem_publishedDate').val());
        $('#updatequestion_faqItem_archivedDate').val($(this).siblings('.updatequestion_faqItem_archivedDate').val());
        $('#updatequestion_faqItem_label').val($(this).siblings('.updatequestion_faqItem_label').val());
        $('#updatequestion_faqItem_content').val($(this).siblings('.updatequestion_faqItem_content').val());
        $('#updatequestion_faqItemTranslation_content').val($(this).siblings('.updatequestion_faqItemTranslation_content').val());
        $('#updatequestion_faqItemAnswerEN_content').summernote("code", $(this).siblings('.updatequestion_faqItemAnswerEN_content').val());
        $('#updatequestion_faqItemAnswerCH_content').summernote("code", $(this).siblings('.updatequestion_faqItemAnswerCH_content').val());
    })
</script>
<!-- End of FAQ Question Update -->

</body>
</html>
