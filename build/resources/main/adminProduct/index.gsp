<%--
  Created by IntelliJ IDEA.
  User: marjan
  Date: 11/12/17
  Time: 18.29
--%>

<%@ page import="canon.enums.Status" contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title><g:message code="admin.dashboard.title" default="Admin Dashboard"/></title>
    <meta name="layout" content="admin"/>
</head>

<body>

<!-- Start Page Heading -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-6">
        <h2><g:message code="admin.product.list.selectProduct.label" default="Select Product"/></h2>
        <ol class="breadcrumb">
            <li>
                <g:link controller="adminProduct" action="index"><g:message code="admin.product.label" default="Product"/></g:link>
            </li>
            <li class="active">
                <strong><g:message code="admin.product.list.productList.label" default="Product List"/></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-6 text-right">
        <g:link controller="adminProduct" action="create" class="btn btn-primary"><g:message code="admin.product.addNew.label" default="Add New Product"/></g:link>
    </div>
</div>
<!-- End Page Heading -->

<!-- Start Content -->
<div class="wrapper wrapper-content">

    <!-- Start Ibox -->
    <div class="ibox-content margin-bottom60">
        <form method="get" action="${createLink(controller: 'adminProduct', action: 'index')}">
            <div class="row">
                <div class="col-sm-8">
                    <div class="form-inline">
                        <div class="form-group margin-bottom20">
                            <label for="dateFrom"><g:message code="admin.product.list.dateRange.label" default="Date range"/></label>
                            <div class="input-group date js-date">
                                <input id="dateFrom" name="dateFrom" type="text" class="form-control" value="${params?.dateFrom}">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </span>
                            </div>
                        </div>
                        <div class="form-group margin-bottom20">
                            <label for="dateTo"><g:message code="admin.product.list.to.label" default="to"/></label>
                            <div class="input-group date js-date">
                                <input id="dateTo" name="dateTo" type="text" class="form-control" value="${params?.dateTo}">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </span>
                            </div>
                        </div>
                    </div>

                    %{--<table class="filter-select">
                        <tr>
                            <td>
                                <label><g:message code="admin.product.list.filterBy.label" default="Filter By"/></label>
                            </td>
                            <td>
                                <div class="form-group margin-bottom20">
                                    <g:select class="select form-control" name="site" from="${siteList}" value="${params?.site}"
                                              optionValue="name" optionKey="id" noSelection="${['': message(code: 'admin.product.list.allLocations.label', default: 'All Locations')]}"/>
                                </div>
                                <div class="form-group margin-bottom20">
                                    <!-- Start Wrap Dropdown list -->
                                    <div class="wrap-dropdown-list">
                                        <div class="input-group">
                                            <input type="text" class="form-control list-value" placeholder="${message(code: 'admin.product.list.selectOnlyOne.label', default: 'Select Only One')}"/>
                                            <span class="input-group-addon">
                                                <i class="fa fa-refresh"></i>
                                            </span>
                                        </div>
                                        <ul class="list-collapse margin-bottom30">
                                            <!-- Start Dropdown -->
                                            <li class="dropdown-list on">
                                                <div class="checkbox ">
                                                    <input id="checkbox61" type="checkbox" checked="checked">
                                                    <label for="checkbox61" >
                                                        Photography
                                                    </label>
                                                </div>
                                                <!-- Start Dropdown Content -->
                                                <ul class="dropdown-content">
                                                    <li class="dropdown-list on">
                                                        <div class="checkbox ">
                                                            <input id="checkboxd82" type="checkbox" checked="checked">
                                                            <label for="checkboxd82">
                                                                Interchangeable Lens Cameras
                                                            </label>
                                                        </div>
                                                        <!-- Start Dropdown Content -->
                                                        <ul class="dropdown-content">
                                                            <li class="dropdown-list on">
                                                                <div class="checkbox ">
                                                                    <input id="checkboxd91" type="checkbox" checked="checked">
                                                                    <label for="checkboxd91">
                                                                        Range Filter - DSLR (EOS)
                                                                    </label>
                                                                </div>
                                                                <!-- Start Dropdown Content -->
                                                                <ul class="dropdown-content">
                                                                    <li>
                                                                        <div class="checkbox">
                                                                            <input id="checkboxd922" type="checkbox" checked="checked">
                                                                            <label for="checkboxd922">
                                                                                Mangawa
                                                                            </label>
                                                                        </div>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                            <li class="dropdown-list">
                                                                <div class="checkbox main">
                                                                    <input id="checkboxd822" type="checkbox">
                                                                    <label for="checkboxd822">
                                                                        Range Filter - Mirrorless (EOS M)
                                                                    </label>
                                                                </div>
                                                                <!-- Start Dropdown Content -->
                                                                <ul class="dropdown-content">
                                                                    <li>
                                                                        <div class="checkbox">
                                                                            <input id="checkboxd912" type="checkbox">
                                                                            <label for="checkboxd912">
                                                                                Lorem ipsum
                                                                            </label>
                                                                        </div>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li class="dropdown-list">
                                                        <div class="checkbox main">
                                                            <input id="checkboxd182" type="checkbox">
                                                            <label for="checkboxd182">
                                                                Bogor
                                                            </label>
                                                        </div>
                                                        <!-- Start Dropdown Content -->
                                                        <ul class="dropdown-content">
                                                            <li>
                                                                <div class="checkbox">
                                                                    <input id="checkboxd191" type="checkbox">
                                                                    <label for="checkboxd191">
                                                                        Indramayu
                                                                    </label>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                                <!-- End Dropdown Content -->
                                            </li>
                                            <li class="dropdown-list">
                                                <div class="checkbox main">
                                                    <input id="checkboxd283" type="checkbox">
                                                    <label for="checkboxd283">
                                                        Printing
                                                    </label>
                                                </div>
                                                <!-- Start Dropdown Content -->
                                                <ul class="dropdown-content">
                                                    <li>
                                                        <div class="checkbox">
                                                            <input id="checkboxd291" type="checkbox">
                                                            <label for="checkboxd291">
                                                                Indramayu
                                                            </label>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="checkbox">
                                                            <input id="checkboxd292" type="checkbox">
                                                            <label for="checkboxd292">
                                                                Ciganea
                                                            </label>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li class="dropdown-list">
                                                <div class="checkbox main">
                                                    <input id="checkboxd383" type="checkbox">
                                                    <label for="checkboxd383">
                                                        Scanning
                                                    </label>
                                                </div>
                                                <!-- Start Dropdown Content -->
                                                <ul class="dropdown-content">
                                                    <li>
                                                        <div class="checkbox">
                                                            <input id="checkboxd391" type="checkbox">
                                                            <label for="checkboxd391">
                                                                Flatbed
                                                            </label>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="checkbox">
                                                            <input id="checkboxd392" type="checkbox">
                                                            <label for="checkboxd392">
                                                                Flatbed with Film
                                                            </label>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="checkbox">
                                                            <input id="checkboxd392" type="checkbox">
                                                            <label for="checkboxd392">
                                                                Document
                                                            </label>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </li>
                                            <!-- End Dropdown -->
                                            <!-- Start Dropdown -->
                                            <g:each in="${consumerList}" var="consumerCategory">
                                                <li class="dropdown-list">
                                                    <div class="checkbox">
                                                        <input id="category__${consumerCategory?.id}" type="checkbox" name="category" value="${consumerCategory?.id}" ${consumerCategory?.id == params?.category ? 'checked' : ''}>
                                                        <label for="category__${consumerCategory?.id}">
                                                            ${consumerCategory?.name}
                                                        </label>
                                                    </div>
                                                </li>
                                            </g:each>
                                            <!-- End Dropdown -->
                                        </ul>
                                    </div>
                                    <!-- End Wrap Dropdown list -->
                                </div>
                            </td>
                        </tr>
                    </table>--}%
                </div>
                <div class="col-sm-4">
                    <div class="input-group"><input type="text" name="q" value="${params?.q ?: ''}" placeholder="${message(code: 'admin.product.list.searchKeyword.label', default: 'Search keyword')}" class="input-sm form-control"> <span class="input-group-btn">
                        <button type="submit" class="btn btn-sm btn-primary"><g:message code="admin.product.list.search.label" default="Search"/></button></span></div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-7 margin-bottom20">
                    <div data-toggle="buttons" class="btn-group">
                        <g:each in="${statusList}" var="status">
                            <label class="btn btn-sm btn-white ${status.name() == params.status ? 'active' : ''}">
                                <input type="radio" id="option8" name="status" value="${status.name()}"
                                       class="js-auto-submit" ${status.name() == params.status ? 'checked' : ''}>
                                <g:message code="canon.enums.Status.${status.name()}.label" default="" /> (${countByStatus.get(status.name())})
                            </label>
                        </g:each>
                    </div>
                </div>
                <div class="col-sm-5 text-right">
                    <div data-toggle="buttons" class="btn-group">
                        <button class="btn btn-sm btn-white js-products-action" data-url="${createLink(controller: 'adminProduct', action: 'duplicate')}"><g:message code="admin.product.list.duplicate.label" default="Duplicate"/></button>
                        <g:if test="${params.status == Status.ARCHIVED.name()}">
                        	<button class="btn btn-sm btn-white js-products-action" data-url="${createLink(controller: 'adminProduct', action: 'restoreProducts')}"><g:message code="admin.product.list.restore.label" default="Restore"/></button>
                        </g:if>
                        <g:else>
                        	<button class="btn btn-sm btn-white js-products-action" data-url="${createLink(controller: 'adminProduct', action: 'archiveProducts')}"><g:message code="admin.product.list.moveToArchive.label" default="Move to Archive"/></button>
                        </g:else>
                    </div>
                </div>
            </div>
        </form>
        <div class="table-responsive margin-top50">
            <table class="table table-striped sortable">
                <thead>
                <tr>
                    <th class="input">
                        <div class="checkbox single">
                            <input id="product__all" type="checkbox" class="checkall" name="input[]">
                            <label for="product__all"></label>
                        </div>
                    </th>
                    <th data-url="${createLink(params: params - [sort: params?.sort, order: params?.order] + [sort: 'name', order: (params?.sort == 'name' ? (params?.order == 'asc' ? 'desc' : 'asc') : 'asc')])}" class="sort js-sort"><g:message code="admin.product.name.label" default="Product Name"/></th>
                    <th data-url="${createLink(params: params - [sort: params?.sort, order: params?.order] + [sort: params?.status == Status.PUBLISHED.name() ? 'publishedDate' : 'lastUpdated', order: (['publishedDate', 'lastUpdated'].contains(params?.sort) ? (params?.order == 'asc' ? 'desc' : 'asc') : 'asc')])}" class="sort js-sort"><g:message code="admin.default.date.label" default="Date"/></th>
                    <th><g:message code="admin.default.status.label" default="Status"/></th>
                </tr>
                </thead>
                <tbody>
                <g:each in="${productList}" var="product">
                    <g:set var="productUrl" value="${createLink(controller: 'adminProduct', action: 'edit', id: product[0])}"/>
                    <g:set var="lastModified" value="${product[4]}"/>
                    <g:if test="${product[2] && product[2] == Status.PUBLISHED.name()}">
                        <g:set var="productUrl" value="${createLink(controller: 'adminProduct', action: 'location', id: product[0])}"/>
                        <g:set var="lastModified" value="${product[3]}"/>
                    </g:if>
                    <tr>
                        <td class="input">
                            <div class="checkbox single">
                                <input id="product__${product[0]}" type="checkbox" name="id[]" value="${product[0]}">
                                <label for="product__${product[0]}"></label>
                            </div>
                        </td>
                        <td>
                            <a href="${productUrl}" class="single-link">${product[1]}</a>
                        </td>
                        <td>${raw(message(code: 'admin.product.list.date.label', default: "Last modified <br/> {0}",
                                args: [formatDate(date: lastModified, formatName: 'admin.product.list.date.format')]))}</td>
                        <td><g:message code="canon.enums.Status.${product[2]}.label" default="" /></td>
                    </tr>
                </g:each>
                </tbody>
            </table>
        </div>
        <!-- Start Pagination -->
        <g:set var="max" value="${Math.min(params?.int('max', 10), 30)}"/>
        <g:set var="page" value="${params?.int('page') ?: 1}"/>
        <g:set var="totalCount" value="${countByStatus?.get(params?.status) ?: 0}"/>
        <g:set var="totalPage" value="${Math.ceil(totalCount/max) as java.lang.Integer}"/>
        <g:if test="${totalCount > max}">
            <div class="post-pagination">
                <ul>
                    <li class="nav">
                        <g:if test="${page > 1}">
                            <g:link controller="adminProduct" action="index" params="${params - [page: params?.page] + [page: page - 1]}">
                                <span class="fa fa-angle-left"></span>
                            </g:link>
                        </g:if>
                        <g:else>
                            <span class="fa fa-angle-left"></span>
                        </g:else>
                    </li>
                    <li class="page">
                        <input name="page" type="number" value="${page}" class="js-input-page"
                               data-redirect-url="${createLink(controller: 'adminProduct', action: 'index', params: params - [page: params?.page] + [page: '---input-page---'])}" data-total-page="${totalPage}" data-page-placeholder="---input-page---">
                    </li>
                    <li><g:message code="admin.product.list.of.label" default="of"/> </li>
                    <li class="count">${totalPage}</li>
                    <li class="nav">
                        <g:if test="${page < totalPage}">
                            <g:link controller="adminProduct" action="index" params="${params - [page: params?.page] + [page: page + 1]}">
                                <span class="fa fa-angle-right"></span>
                            </g:link>
                        </g:if>
                        <g:else>
                            <span class="fa fa-angle-right"></span>
                        </g:else>
                    </li>
                </ul>
            </div>
        </g:if>
        <!-- End Pagination -->

    </div>
    <!-- End Ibox -->
</div>
<!-- End Content -->
</body>
</html>