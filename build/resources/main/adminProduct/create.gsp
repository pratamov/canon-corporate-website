<%--
  Created by IntelliJ IDEA.
  User: marjan
  Date: 11/12/17
  Time: 18.29
--%>

<%@ page import="canon.enums.Status" contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title><g:message code="admin.dashboard.title" default="Admin Dashboard"/></title>
    <meta name="layout" content="admin"/>
    <g:render template="/adminProduct/selling_point"/>
</head>

<body>
<!-- Start Page Heading -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><g:message code="admin.product.create.createANewProduct.label" default="Create a new product"/></h2>
        <ol class="breadcrumb">
            <li>
                <g:link controller="adminProduct" action="index"><g:message code="admin.product.label" default="Product"/></g:link>
            </li>
            <li>
                <g:link controller="adminProduct" action="index"><g:message code="admin.product.list.label" default="Product List"/></g:link>
            </li>
            <li class="active">
                <strong><g:message code="admin.product.addNew.label" default="Add New Product"/></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2"></div>
</div>
<!-- End Page Heading -->
<g:hasErrors bean="${this.product}">
    <ul class="errors" role="alert">
        <g:eachError bean="${this.product}" var="error">
            <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
        </g:eachError>
    </ul>
</g:hasErrors>
<g:form controller="adminProduct" action="save" method="POST">
    <g:render template="form_content"
              model="[product: product, languageList: languageList, siteList: siteList, user: user, flows: flows, currentWorkflow: currentWorkFlow]" />
</g:form>
    <content tag="footer">
        <div class="pull-right">
            <button class="btn btn-default margin-left10"><g:message code="admin.default.cancel.label" default="Cancel"/></button>
            <button class="btn btn-primary margin-left10 js-submit-form" data-flow="NO_CHANGES"><g:message code="admin.default.saveChanges.label" default="Save Changes"/></button>
            <button class="btn btn-primary margin-left10 js-submit-form" data-flow="NEXT"><g:message code="admin.default.submit.label" default="Submit"/></button>
        </div>
    </content>
</body>
</html>