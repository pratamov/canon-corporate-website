<%@ page import="canon.enums.MediaType; canon.ProductRelation" %>
<!-- Start Content -->
<div class="wrapper wrapper-content">
    <!-- Start box -->
    <div class="ibox-title">
        <h3 class="title"><g:message code="admin.product.create.productOverview.label" default="Product Overview"/></h3>
    </div>
    %{--<g:render template="workFlow" model="[user: user, flows: flows, current: currentWorkflow]" />--}%
    <!-- End Ibox -->
    <!-- Start Ibox -->
    <div class="ibox-content margin-bottom30">
        <div class="form-group margin-bottom20">
            <label><g:message code="admin.product.name.label" default="Product Name" /></label>
            <input name="name" type="text" class="form-control" value="${product?.name}" />
        </div>
        <div class="row">
            <div class="col-md-3">
                <div class="form-group margin-bottom20">
                    <label><g:message code="admin.product.create.productLanguage.label" default="Product Language" /></label>
                    <select name="language" class="select form-control" data-placeholder="Select Product Language">
                        <g:each in="${languageList}" var="language">
                            <option value="${language.id}" ${language.id?.equalsIgnoreCase("en") ? 'selected' : ''}>${language.name}</option>
                        </g:each>
                    </select>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group margin-bottom20">
                    <label><g:message code="admin.product.create.productPrice.label" default="Product Price" /></label> (<g:message code="admin.product.create.forAsiaOnly.label" default="For Asia only"/>)
                    <input type="text" name="price" class="form-control" value="${product?.price}" />
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group margin-bottom20">
                    <label><g:message code="admin.product.create.productAvailabilityDate.label" default="Product Availability Date"/></label>
                    <div class="input-group date">
                        <input type="text" name="availabilityDateString" class="form-control" value="${formatDate(format: 'd MMM yyyy', date: product?.availabilityDate, locale: Locale.US)}">
                        <span class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group margin-bottom20">
                    <label><g:message code="admin.product.create.productDiscontinuedDate.label" default="Product Discontinued Date" /></label>
                    <div class="input-group date">
                        <input type="text" name="discontinuedDateString" class="form-control" value="${formatDate(format: 'd MMM yyyy', date: product?.discontinuedDate, locale: Locale.US)}">
                        <span class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group margin-bottom20">
            <label><g:message code="admin.product.create.productDescription.label" default="Product Description" /></label>
            <textarea name="description" class="form-control" rows="5" placeholder="${message(code: 'admin.product.create.typeInDescription.label', default: 'Type in Descrition')}"></textarea>
        </div>
        <div class="form-group margin-bottom20">
            <label><g:message code="admin.product.create.productSubText.label" default="Product Sub Text" /></label>
            <textarea name="subText" class="form-control" rows="5" placeholder="Type Product Name"></textarea>
        </div>
        <div class="form-group margin-bottom20">
            <label><g:message code="admin.product.create.productSellingPoint.label" default="Product Selling Product" /></label>
            <g:if test="${product?.productSellingPoints}">
                <g:each in="${product?.productSellingPoints}" var="sellPoint" status="i">
                    <div class="input-group crud margin-bottom10">
                        <input type="text" name="sellingPoints[]" class="form-control" value="${sellPoint?.content}">
                        <div class="input-group-btn">
                            <g:if test="${i == product?.productSellingPoints?.size() - 1}">
                                <button class="btn btn-default" type="button"><span class="fa fa-plus"></span></button>
                            </g:if>
                            <g:else>
                                <button class="btn btn-default" type="button"><span class="fa fa-trash"></span></button>
                            </g:else>
                        </div>
                    </div>
                </g:each>
            </g:if>
            <g:else>
                <div id="sellingPointContainer">
                </div>
                <div class="input-group crud margin-bottom10">
                    <input type="text" name="sellingPoints[]" class="form-control" id="sellingPointText">
                    <div class="input-group-btn">
                        <button class="btn btn-default" type="button" id="sellingPointAdd"><span class="fa fa-plus"></span></button>
                    </div>
                </div>
            </g:else>
        </div>
    </div>
    <!-- End Ibox -->

    <!-- Start Ibox -->
    <div class="ibox-title">
        <h3 class="title"><g:message code="admin.product.create.productImage.label" default="Product Image" /></h3>
    </div>
    <div class="ibox-content margin-bottom30">
        <div class="row js-product-images-preview">
            <g:each in="${product?.productMedias}" var="productMedia">
                <div class="col-md-2 js-product-image">
                    <g:hiddenField name="mediaId" value="${productMedia?.media?.id}"/>
                    <g:if test="${productMedia?.media?.mediaType != MediaType.IMAGE}">
                        <span class="img-label">${productMedia?.media?.mediaType?.name()?.toLowerCase()}</span>
                    </g:if>
                    <img src="${productMedia?.media?.url}" class="img-responsive" width="140" height="140" alt="" />
                    <a href="#" class="img-delete js-remove-product-image"><i class="fa fa-trash"></i></a>
                </div>
            </g:each>
            <div class="col-md-2 image-thumbnail">
                <a href="javascript:void(0)" class="upload-image js-insert-thumbnail-btn">
                    <span class="fa fa-plus"></span>
                </a>
            </div>
        </div>
    </div>
    <!-- End Ibox -->

    <div class="row">
        <div class="col-md-6">
            <!-- Start Ibox -->
            <div class="ibox-title">
                <button type="button" class="btn btn-default btn-sm pull-right" data-toggle="modal" data-target="#modalRelatedProduct" data-title="${message(code: 'admin.product.create.relatedProducts.label', default: 'Related Products')}"><g:message code="admin.default.browse.label" default="Browse" /></button>
                <h3 class="title"><g:message code="admin.product.create.relatedProducts.label" default="Related Products" /></h3>
            </div>
            <div class="ibox-content padding-bottom0 margin-bottom30 wrap-cell ${(productRelationMap?.get('product') as List<ProductRelation>)?.size() < 1 ? 'hidden' : ''} js-content">
                <ul class="block-list">
                    <g:each in="${productRelationMap?.get('product') as List<ProductRelation>}" var="productRelation">
                        <li>
                            <a href="#" class="btn btn-danger btn-sm pull-right js-remove-item"><g:message code="admin.default.remove.label" default="Remove"/></a>
                            <h5 class="title js-selected-item" data-id="${productRelation?.product?.id}">${productRelation?.product?.name}</h5>
                        </li>
                    </g:each>
                </ul>
            </div>
            <!-- End Ibox -->
        </div>
        <div class="col-md-6">
            <!-- Start Ibox -->
            <div class="ibox-title">
                <button type="button" class="btn btn-default btn-sm pull-right" data-toggle="modal" data-target="#modalRelatedProduct" data-title="${message(code: 'admin.modal.productRelation.title.label', default: 'Related {0}', args: [message(code: 'admin.product.create.accessories.label', default: 'Accesories')])}"><g:message code="admin.default.browse.label" default="Browse" /></button>
                <h3 class="title"><g:message code="admin.product.create.accessories.label" default="Accessories" /></h3>
            </div>
            <div class="ibox-content padding-bottom0 margin-bottom30 wrap-cell ${(productRelationMap?.get('accessories') as List<ProductRelation>)?.size() < 1 ? 'hidden' : ''} js-content">
                <ul class="block-list">
                    <g:each in="${productRelationMap?.get('accessories') as List<ProductRelation>}" var="productRelation">
                        <li>
                            <a href="#" class="btn btn-danger btn-sm pull-right js-remove-item"><g:message code="admin.default.remove.label" default="Remove"/></a>
                            <h5 class="title">${productRelation?.product?.name}</h5>
                        </li>
                    </g:each>
                </ul>
            </div>
            <!-- End Ibox -->
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <!-- Start Ibox -->
            <div class="ibox-title">
                <button type="button" class="btn btn-default btn-sm pull-right" data-toggle="modal" data-target="#modalRelatedProduct" data-title="${message(code: 'admin.modal.productRelation.title.label', default: 'Related {0}', args: [message(code: 'admin.product.create.consumable.label', default: 'Consumable')])}"><g:message code="admin.default.browse.label" default="Browse" /></button>
                <h3 class="title"><g:message code="admin.product.create.consumable.label" default="Consumable" /></h3>
            </div>
            <div class="ibox-content padding-bottom0 margin-bottom30 wrap-cell ${(productRelationMap?.get('consumable') as List<ProductRelation>)?.size() < 1 ? 'hidden' : ''} js-content">
                <ul class="block-list">
                    <g:each in="${productRelationMap?.get('consumable') as List<ProductRelation>}" var="productRelation">
                        <li>
                            <a href="#" class="btn btn-danger btn-sm pull-right js-remove-item"><g:message code="admin.default.remove.label" default="Remove"/></a>
                            <h5 class="title">${productRelation?.product?.name}</h5>
                        </li>
                    </g:each>
                </ul>
            </div>
            <!-- End Ibox -->
        </div>
        <div class="col-md-6">
            <!-- Start Ibox -->
            <div class="ibox-title">
                <button type="button" class="btn btn-default btn-sm pull-right"><g:message code="admin.default.browse.label" default="Browse" /></button>
                <h3 class="title"><g:message code="product.overview.awards.label" default="Awards" /></h3>
            </div>
            <div class="ibox-content padding-bottom0 margin-bottom30 wrap-cell ${product?.productAwards?.size() < 1 ? 'hidden' : ''}">
                <ul class="block-list">
                    <g:each in="${product?.productAwards?}" var="productAward">
                        <li>
                            <g:hiddenField name="awardId[]" value="${productAward?.award?.id}"/>
                            <a href="#" class="btn btn-danger btn-sm pull-right js-remove-item"><g:message code="admin.default.remove.label" default="Remove"/></a>
                            <h5 class="title"><a href="${productAward?.award?.imageUrl}">${productAward?.award?.name}</a></h5>
                        </li>
                    </g:each>
                </ul>
            </div>
            <!-- End Ibox -->
        </div>
    </div>
    <div class="row margin-bottom30">
        <div class="col-md-6">
            <!-- Start Ibox -->
            <div class="ibox-title">
                <button type="button" class="btn btn-default btn-sm pull-right"><g:message code="admin.default.browse.label" default="Browse" /></button>
                <h3 class="title"><g:message code="admin.product.create.brochure.label" default="Brochure" /></h3>
            </div>
            <div class="ibox-content padding-bottom0 margin-bottom30 wrap-cell hidden"></div>
            <!-- End Ibox -->
        </div>
        <div class="col-md-6">
            <!-- Start Ibox -->
            <div class="ibox-title">
                <button type="button" class="btn btn-default btn-sm pull-right"><g:message code="admin.default.browse.label" default="Browse" /></button>
                <h3 class="title"><g:message code="admin.product.create.otherDocuments.label" default="Other Documents"/></h3>
            </div>
            <div class="ibox-content padding-bottom0 margin-bottom30 wrap-cell hidden"></div>
            <!-- End Ibox -->
        </div>
    </div>

    <!-- Start Ibox -->
    <div class="ibox-title">
        <h3 class="title"><g:message code="admin.product.create.productSpecification.label" default="Product Specification"/></h3>
    </div>
    <div class="ibox-content margin-bottom30">
        <div class="row">
            <div class="col-md-3">
                <div class="form-group margin-bottom20">
                    <label><g:message code="admin.product.create.fullProductSpecification.label" default="Full Product Specification" /></label>
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalSpecification">
                        <g:message code="admin.product.create.inputSpecification.label" default="Input Specification" />
                    </button>
                </div>
            </div>
            <div class="col-md-9">
                <div class="form-group margin-bottom20">
                    <label><g:message code="admin.product.create.supportLink.label" default="Support Link" /></label>
                    <input name="supportLink" type="text" class="form-control" placeholder="${message(code: 'admin.product.create.typeLink.label', default: 'Type Link')}">
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label><g:message code="admin.product.create.productNotes.label" default="Product Notes"/></label>
                    <textarea name="specNote" class="form-control" rows="5" placeholder="${message(code: 'admin.product.create.typeInNotes.label', default: 'Type in Notes')}"></textarea>
                </div>
            </div>
        </div>
    </div>
    <!-- End Ibox -->

    <!-- Start Ibox -->
    <div class="ibox-content margin-bottom30">
        <!-- Start Panel -->
        %{--<div class="panel panel-default">
            <div class="panel-heading">
                <strong><g:message code="admin.product.create.locationSettings.label" default="Location Settings"/></strong>
            </div>
            <div class="panel-body">
                <!-- Start Table -->
                <table class="table table-striped js-product-location-table">
                    <div class="hidden js-product-categories">
                        <g:each in="${product?.productCategories}" var="productCategory">
                            <g:hiddenField class="js-category-site-${productCategory?.category?.id}" name="category[]" value="${productCategory?.category?.id}"/>
                        </g:each>
                    </div>
                    <thead>
                    <tr>
                        <th><g:message code="admin.product.create.contentLocation.label" default="Content Location" /></th>
                        <th><g:message code="admin.product.create.productPrice.label" default="Product Price" /></th>
                        <th><g:message code="admin.product.create.publishDate.label" default="Publish Date" /></th>
                        <th><g:message code="admin.product.create.archiveDate.label" default="Archive Date" /></th>
                        <th><g:message code="admin.default.action.label" default="Action" /></th>
                    </tr>
                    </thead>
                    <tbody>
                    <g:each in="${product?.productSites}" var="productLocation">
                        <tr class="js-site-${productLocation?.site?.id}">
                            <g:hiddenField name="locationId" value="${productLocation?.site?.id}"/>
                            <g:hiddenField name="locationPrice" value="${productLocation?.price}"/>
                            <g:hiddenField name="locationPublishedDate" value="${productLocation?.publishedDate}"/>
                            <g:hiddenField name="locationArchivedDate" value="${productLocation?.archivedDate}"/>
                            <td><a href="#" class="single-link" data-toggle="modal" data-target="#modalLocation" data-title="${message(code: 'admin.modal.editLocation.label', default: 'Edit Location')}">${productLocation?.site?.name}</a></td>
                            <td>${productLocation?.price ? formatNumber(number: productLocation?.price, currencyCode: productLocation?.currencyCode, type: 'currency', locale: Locale.US) : '-'}</td>
                            <td>${productLocation?.publishedDate ? formatDate(date: productLocation?.publishedDate, formatName: 'admin.product.list.location.date.format', locale: Locale.US) : '-'}</td>
                            <td>${productLocation?.archivedDate ? formatDate(date: productLocation?.archivedDate, formatName: 'admin.product.list.location.date.format', locale: Locale.US) : '-'}</td>
                            <td>
                                <g:if test="${productLocation?.site?.id != product?.site?.id}">
                                    <a href="#" class="js-delete-location"><span class="fa fa-trash"></span></a>
                                </g:if>
                            </td>
                        </tr>
                    </g:each>
                    </tbody>
                </table>
                <!-- End Table -->
                <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#modalLocation" data-title="${message(code: 'admin.modal.addLocation.label', default: 'Add Location')}"><g:message code="admin.product.create.addNewLocation.label" default="Add new location"/></a>
            </div>
        </div>--}%
        <!-- End Panel -->

        <!-- Start Panel -->
        <div class="panel panel-default">
            <div class="panel-heading">
                <strong><g:message code="admin.product.create.socialShare.label" default="Social Share" /></strong> (<g:message code="admin.default.optional.label" default="optional"/>)
            </div>
            <div class="panel-body">
                <div class="form-horizontal" action="/action_page.php">
                    <div class="form-group">
                        <label class="control-label col-sm-2 text-left"><g:message code="admin.default.title.label" default="Title" /></label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control">
                            <small><em><g:message code="admin.product.create.socialShare.blankNotice.label" default="If the field is blank, the value will be pulled from {0}." args="['product title']" /></em></small>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2 text-left">Description</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control">
                            <small><em>If the field is blank, the value will be pulled from text below hero image (synopsis)</em></small>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2 text-left">Image</label>
                        <div class="col-sm-4">
                            <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                <div class="form-control" data-trigger="fileinput">
                                    <i class="glyphicon glyphicon-file fileinput-exists"></i>
                                    <span class="fileinput-filename"></span>
                                </div>
                                <span class="input-group-addon btn btn-default btn-file">
                                    <span class="fileinput-new">Select file</span>
                                    <span class="fileinput-exists">Change</span>
                                    <input type="file" name="..."/>
                                </span>
                                <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                            </div>
                            <small><em>If the field is blank, the hero image will be used.</em></small>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Panel -->

        <!-- Start Panel -->
        <div class="panel panel-default">
            <div class="panel-heading">
                <strong>Tag</strong>
            </div>
            <div class="panel-body">
                <div class="form-group">
                    <p>Select up to 3 tags to associate with the page</p>
                    <select class="select-multiple form-control" data-placeholder="Select Tags">
                        <option></option>
                        <option value="1">Photography</option>
                        <option value="2">Printing</option>
                        <option value="3">Scanning</option>
                        <option value="4">Videiography</option>
                        <option value="5">Presentation</option>
                        <option value="6">Printing</option>
                    </select>
                </div>
            </div>
        </div>
        <!-- End Panel -->
    </div>
    <!-- End Ibox -->

    <div class="row margin-bottom40">
        <div class="col-md-8">
            <!-- Start Ibox -->
            <div class="ibox-title">
                <button class="btn btn-primary pull-right margin-left10">Inline Editor</button>
                <h3 class="title">Preview</h3>
            </div>
            <div class="preview-page margin-bottom30">
                <div class="content">our preview will appear here. Click on the Edit Content button below to start creating your product.</div>
            </div>
        </div>
        <div class="col-md-4">
            <!-- Start Ibox -->
            <div class="ibox-title">
                <h3 class="title"><g:message code="admin.comments.label" default="Comments" /></h3>
            </div>
            <div class="ibox-content">
                <div class="form-group">
                    <textarea class="form-control" placeholder="Add a comment" rows="4"></textarea>
                </div>
                <p><a href="#" class="primary-color">Replycomment.pdf</a></p>
                <div class="text-right">
                    <div class="fileinput fileinput-new" data-provides="fileinput">
                        <span class="btn btn-default btn-file"><span class="fileinput-new"><g:message code="admin.comments.uploadFile.label" default="Upload File" /></span>
                            <span class="fileinput-exists"><g:message code="admin.default.change.label" default="Change" /></span><input type="file" name="commentFile"/></span>
                        <span class="fileinput-filename"></span>
                        <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">×</a>
                    </div>
                    <button class="btn btn-primary">Submit</button>
                </div>
            </div>
            <div class="ibox-content" style="height:180px;overflow-y:scroll">
                <ul class="list-comment">
                    <li>
                        <h5 class="title">
                            Jason Huang
                            <em>28/10/2017, 05:30pm, GMT+8</em>
                        </h5>
                        <p>@Jack Toh As Johnny Mcclean is current on medical leave, I have submitted the article for your editing.</p>
                        <p><a href="#" class="primary-color">Replycomment.pdf</a></p>
                        <div class="action"><a href="#" class="link">Edit</a> | <a href="#" class="link">Delete</a></div>
                    </li>
                    <li>
                        <h5 class="title">
                            Elson Loh
                            <em>25/10/2017, 9:00a.m, GMT+8</em>
                        </h5>
                        <p>@Jonny Mcclean This is the second draft. Edits made: Video segment has been updated. Images have been updated with the latest images from Alice Keys.</p>
                        <div class="action"><a href="#" class="link">Edit</a> | <a href="#" class="link">Delete</a></div>
                    </li>
                    <li>
                        <h5 class="title">
                            Johnny Mcclean
                            <em>24/10/2017, 04:07pm, GMT+8</em>
                        </h5>
                        <p>@Elson Loh Please see the comments below for changes required:</p>
                        <p>In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus.</p>
                        <p>Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus</p>
                        <div class="action"><a href="#" class="link">Edit</a> | <a href="#" class="link">Delete</a></div>
                    </li>
                </ul>
            </div>
            <!-- End Ibox -->
        </div>
    </div>
</div>
<!-- End Content -->