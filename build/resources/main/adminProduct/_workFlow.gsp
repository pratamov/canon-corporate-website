<div class="ibox-content">
    <g:hiddenField name="username" value="${user.username}"/>
    <div class="form-group margin-bottom20">
        <label><g:message code="admin.workflow.select.label" default="Select Workflow"/></label>
        <g:if test="${actionName == 'create'}">
            <g:select class="form-control js-flow-selector" name="flow" from="${flows}" data-url="${createLink(controller: 'adminFlow', action: 'getWorkflowByFlow')}" data-get-user="${createLink(controller: 'adminUser', action: 'findAll')}"
                      optionKey="id" optionValue="name" noSelection="['': message(code: 'admin.workflow.select.label', default: 'Select Workflow')]"/>
        </g:if>
    </div>
    <g:if test="${actionName == 'create'}">
        <div class="row js-workflow-list"></div>
    </g:if>
</div>