<%--
  Created by IntelliJ IDEA.
  User: marjan
  Date: 11/12/17
  Time: 18.29
--%>

<%@ page import="canon.enums.Status" contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title><g:message code="admin.dashboard.title" default="Admin Dashboard"/></title>
    <meta name="layout" content="admin"/>
</head>

<body>
<!-- Start Page Heading -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-6">
        <h2>Create a new article</h2>
        <ol class="breadcrumb">
            <li>
                <g:link controller="adminContent" action="index">Article</g:link>
            </li>
            <li>
                <g:link controller="adminContent" action="index">Article List</g:link>
            </li>
            <li class="active">
                <strong>Inline</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-6 text-right">
        <a href="#" class="btn btn-primary pull-right js-inline-submit">Save Changes</a>
        <a href="#" class="btn btn-default js-inline-cancel">Exit Editor</a>
    </div>
</div>

<!-- Start Content -->
<div class="wrapper wrapper-content">

    <div class="row margin-bottom70">
        <div class="col-md-12">
            <!-- Star Ibox -->
            <div class="ibox-content" style="overflow:hidden">

                <!-- Start Wrap PageComponent -->
                <div class="wrap-component" id="content-area">
                    <div class="clearfix"></div>

                    <!-- Start Content -->
                    <div class="inner-page">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-7 col-sm-4 col-xs-6">
                                    <!-- Breadcrumbs -->
                                    <ol class="breadcrumb">
                                        <li><a href="#">Articles</a></li>
                                        <li class="sub"><a href="#">Tips & tutorial</a></li>
                                        <li class="active"><span>The Difference Between Vibrance and Saturation</span></li>
                                    </ol>
                                </div>
                                <div class="col-md-5 col-sm-8 col-xs-6 text-right">
                                    <span class="text">Archives</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Content -->
                    <div class="clearfix"></div>
                    <div class="section padding-bottom0"></div>

                    <div class="section">
                        <div class="container">
                            <div class="article">
                                <h3 class="time">13th September 2017, 03:53pm</h3>
                                <h1 class="title">The Difference Between Vibrance and Saturation</h1>
                                <ul class="clone-share list-inline show-medium margin-bottom10"></ul>
                                <p>The rich colours of the evening sky can add a dramatic element to your landscape photography, especially if it involves flowers shot in backlight.
                                Here, we share two such photos and some tips from the photographers about how they achieved the shot. (Reported by: Rika Takemoto, Yoshiki Fujiwara)</p>
                            </div>
                        </div>
                    </div>

                    <section class="section">
                        <div class="container">
                            <div data-type="container-content">

                                <g:each in="${componentList as List<canon.PageComponent>}" var="c" status="i">
                                    <g:renderInline component="${c}" site="${params?.site}" segment="${params?.segment}" />
                                </g:each>

                            </div>
                        </div>
                    </section>

                    <div class="section">
                        <div class="container">

                            <!-- Start Section Detail -->
                            <section class="section padding-top30 m-padding-bottom20" data-type="component-related-products" data-title="Related products">
                                <!-- Start Features -->
                                <h1 class="title-section text-center">Related Products</h1>
                                <div class="wrap-carousel-in-small">
                                    <div class="row carousel-in-small">
                                        <div class="col-sm-4 col-xs-12">
                                            <!-- Start Post Item -->
                                            <div class="post-item match-height related">
                                                <img src="/assets/product/img1.png" alt="" />
                                                <div class="content">
                                                    <h3 class="title"><a href="#">PowerShot G1 X Mark II</a></h3>
                                                </div>
                                            </div>
                                            <!-- End Post Item -->
                                        </div>
                                        <div class="col-sm-4 col-xs-12">
                                            <!-- Start Post Item -->
                                            <div class="post-item match-height related">
                                                <img src="/assets/product/img2.png" alt="" />
                                                <div class="content">
                                                    <h3 class="title"><a href="#">EOS 200D</a></h3>
                                                </div>
                                            </div>
                                            <!-- End Post Item -->
                                        </div>
                                        <div class="col-sm-4 col-xs-12">
                                            <!-- Start Post Item -->
                                            <div class="post-item match-height related">
                                                <img src="/assets/product/img3.png" alt="" />
                                                <div class="content">
                                                    <h3 class="title"><a href="#">PIXMA TS9050 Series</a></h3>
                                                </div>
                                            </div>
                                            <!-- End Post Item -->
                                        </div>
                                    </div>
                                </div>
                                <!-- End Content -->
                            </section>
                            <!-- End Section Detail -->

                            <!-- Start Section Detail -->
                            <section class="section padding-top30 m-padding-bottom10" data-type="component-related-articles" data-title="Suggested articles">
                                <!-- Start Features -->
                                <h1 class="title-section text-center">Suggested Articles</h1>
                                <div class="wrap-carousel-mobile">
                                    <div class="row">
                                        <div class="col-sm-4 col-xs-12">
                                            <!-- Start Post Item -->
                                            <div class="post-item">
                                                <span class="badge right gray">Product</span>
                                                <img src="/assets/product-detail/img1.jpg" alt="" />
                                                <div class="content text-left">
                                                    <h3 class="title match-height"><a href="#">Perfect Portraiture Photography</a></h3>
                                                    <p class="margin-bottom0">
                                                        <strong>29 August 2017</strong>
                                                    </p>
                                                </div>
                                            </div>
                                            <!-- End Post Item -->
                                        </div>
                                        <div class="col-sm-4 col-xs-12">
                                            <!-- Start Post Item -->
                                            <div class="post-item">
                                                <span class="badge right gray">Tips & tutorial</span>
                                                <img src="/assets/product-detail/img1.jpg" alt="" />
                                                <div class="content text-left">
                                                    <h3 class="title match-height"><a href="#">Composition Techniques for Wide-Angle Lenses</a></h3>
                                                    <p class="margin-bottom0">
                                                        <strong>29 August 2017</strong>
                                                    </p>
                                                </div>
                                            </div>
                                            <!-- End Post Item -->
                                        </div>
                                        <div class="col-sm-4 col-xs-12">
                                            <!-- Start Post Item -->
                                            <div class="post-item">
                                                <span class="badge right gray">Tips & tutorial</span>
                                                <img src="/assets/product-detail/img1.jpg" alt="" />
                                                <div class="content text-left">
                                                    <h3 class="title match-height"><a href="#">Photographing Flowers</a></h3>
                                                    <p class="margin-bottom0">
                                                        <strong>29 August 2017</strong>
                                                    </p>
                                                </div>
                                            </div>
                                            <!-- End Post Item -->
                                        </div>
                                    </div>
                                </div>
                                <!-- End Content -->
                            </section>
                            <!-- End Section Detail -->
                        </div>
                    </section>

                    <div class="clearfix"></div>
                </div>
                <!-- End Wrap PageComponent -->

            </div>
            <!-- End Ibox -->
        </div>
    </div>

</div>
<!-- End Content -->

<content tag="footer">
    <div class="pull-right">
        <button class="btn btn-default margin-left10 js-inline-cancel">Cancel</button>
        <button class="btn btn-primary margin-left10 js-inline-submit" data-flow="NO_CHANGES">Save Changes</button>
        <button class="btn btn-primary margin-left10 js-inline-submit" data-flow="NEXT">Submit</button>
    </div>
</content>
</body>
</html>