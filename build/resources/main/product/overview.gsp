<%--
Created by IntelliJ IDEA.
User: muyalif
Date: 05/12/17
Time: 16.42
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
  <title></title>
  <meta name="layout" content="canon"/>
</head>

<body>
  <!-- Start Wrap Component -->
  <div class="wrap-component">
      <!-- Start Content -->
      <g:render template="/templates/product_header"/>
      <!-- End Content -->

      <div class="clear-inner-page"></div>

      <div class="clearfix"></div>

      <!-- Social Share -->
      <div class="section-share hide-medium">
          <div class="container">
              <a href="#" class="toggle-share show-medium"><span class="fa fa-share-alt"></span></a>
              <ul class="social-share hide-medium">
                  <li><a href="#"><span class="fa fa-facebook"></span></a></li>
                  <li><a href="#"><span class="fa fa-twitter"></span></a></li>
                  <li><a href="#"><span class="fa fa-feed"></span></a></li>
              </ul>
          </div>
      </div>

      <div class="clearfix"></div>

      <!-- Start Content Overview -->
      <div class="content-detail m-padding-top30 padding-bottom0">
          <div class="container">
              <!-- Start Overview -->
              <div class="overview">
                  <div class="row">
                      <div class="col-md-7 col-md-push-5">
                          <!-- Start Slider -->
                          <div class="wrap-slide-image nav-noborder">
                              <div class="owl-carousel owl-theme owl-image">
                                  <g:each in="${product?.productMedias}" var="img">
                                      <div class="item">
                                          <img src="${img?.mediaUrl}" alt=""/>
                                      </div>
                                  </g:each>
                              </div>
                          </div>
                          <!-- End Slider -->
                          <div class="clone-social-share hide-mei"></div>
                      </div>
                      <div class="col-md-5 col-md-pull-7">
                          <h1 class="title">${product?.name}</h1>
                          <h2 class="sub-title">${product?.subText}</h2>
                          <p>
                              ${product?.description}
                          </p>
                      </div>
                  </div>
              </div>
              <!-- End Overview -->
          </div>
      </div>
      <!-- End Content Overview -->

      <div class="clearfix"></div>

      <!-- Start Content -->
      <div class="section m-padding-bottom10">
          <div class="container">
              <!-- Start Promotion Box -->
              <!-- <div class="promotion-box">
                  <h1 class="title">Promotion</h1>
                  <p>
                      For a limited time only, from 1 September to 30 September 2017, you will receive a <strong>FREE month worth of ink</strong> with every purchase of an imageCLASS MF631Cn. Terms & conditions apply.
                  </p>
                  <a href="#" class="btn btn-primary">Find out more</a>
              </div> -->
              <!-- End Promotion Box -->

              <div class="row padding-top30 main-row-slide">
                  <div class="col-md-12">
                      <!-- Start Item Features -->
                      <div class="row slide-in-small">
                          <g:each in="${product?.productAwards}" var="pa">
                              <div class="col-sm-4 col-xs-12">
                                  <div class="box-panel">
                                      <img src="${pa?.award?.imageUrl}" alt="${pa?.award?.name}">
                                  </div>
                              </div>
                          </g:each>
                      </div>
                      <!-- End Item Features -->
                  </div>
              </div>

              <div class="row padding-top30">
                  <g:each in="${mainFeature}" var="feature">
                      <div class="col-md-4 col-sm-4">
                          <!-- Start Box Panel -->
                          <div class="box-panel match-height post center-small">
                              <img src="${feature.iconUrl}" alt="${feature.iconTitle}"/>
                              <div class="content">
                                  <h3 class="title"><a href="#">${feature.name}</a></h3>
                                  <p>${feature.description}</p>
                              </div>
                          </div>
                          <!-- End Box Panel -->
                      </div>
                  </g:each>
                  
                  <g:each in="${restFeature}" var="feature">
                      <div class="col-md-3 col-sm-6">
                          <!-- Start Box Panel -->
                          <div class="box-panel match-height post center-small">
                              <div class="content">
                                  <h3 class="title"><a href="#">${feature.name}</a></h3>
                                  <p>${feature.description}</p>
                              </div>
                          </div>
                          <!-- End Box Panel -->
                      </div>
                  </g:each>
                  
                  <div class="col-md-4 col-sm-4">
                      <!-- Start Box Panel -->
                      <div class="box-panel match-height post">
                          <img src="/assets/product-detail/slide.jpg" alt=""/>
                          <div class="content">
                              <h3 class="title"><a href="#">Canon Cartridge Recycling Programme</a></h3>
                              <a href="" class="btn btn-default">Find out more</a>
                          </div>
                      </div>
                      <!-- End Box Panel -->
                  </div>
                  <div class="col-md-4 col-sm-4">
                      <!-- Start Box Panel -->
                      <div class="box-panel match-height post">
                          <img src="/assets/product-detail/slide.jpg" alt=""/>
                          <div class="content">
                              <h3 class="title"><a href="#">Supplies Ink and Paper</a></h3>
                              <a href="" class="btn btn-default">Find out more</a>
                          </div>
                      </div>
                      <!-- End Box Panel -->
                  </div>
                  <div class="col-md-4 col-sm-4">
                      <!-- Start Box Panel -->
                      <div class="box-panel match-height post">
                          <img src="/assets/product-detail/slide.jpg" alt=""/>
                          <div class="content">
                              <h3 class="title"><a href="#">Windows 7 & Vista Support</a></h3>
                              <a href="" class="btn btn-default">Find out more</a>
                          </div>
                      </div>
                      <!-- End Box Panel -->
                  </div>
              </div>
          </div>
      </div>
      <!-- Start Content -->

      <div class="section padding-bottom0">
            <g:if test="${relatedConsumable}">
                <div class="container">
                    <!-- Start Related Products -->
                    <h1 class="title-section text-center">Related Consumables</h1>
                    <div class="wrap-carousel-in-small">
                        <div class="row carousel-in-small">
                            <g:each in="${relatedConsumable}" var="rel">
                                <div class="col-sm-4 col-xs-12">
                                    <!-- Start Post Item -->
                                    <div class="post-item match-height related">
                                        <img src="${rel?.getDefaultMedia()?.mediaUrl}" alt="" />
                                        <div class="content">
                                            <h3 class="title">
                                                <g:link controller="product" action="show" id="${rel.slug}">
                                                    ${rel?.name}
                                                </g:link>
                                            </h3>
                                        </div>
                                    </div>
                                    <!-- End Post Item -->
                                </div>
                            </g:each>
                        </div>
                    </div>
                    <!-- End Related Products -->
                    <div class="text-right margin-top10">
                        <a href="#" class="btn btn-default btn-sm more">View all consumables <span class="fa fa-angle-right"></span></a>
                    </div>
                </div>
            </g:if>
      </div>

      <div class="section">
          <div class="container m-padding-top30">
                <g:if test="${relatedProducts}">
                    <!-- Start Related Products -->
                    <h1 class="title-section text-center">Related Products</h1>
                    <div class="wrap-carousel-in-small">
                        <div class="row carousel-in-small">
                            <g:each in="${relatedProducts}" var="rel">
                                <div class="col-sm-4 col-xs-12">
                                    <!-- Start Post Item -->
                                    <div class="post-item match-height related">
                                        <img src="${rel?.getDefaultMedia()?.mediaUrl}" alt="" />
                                        <div class="content">
                                            <h3 class="title">
                                                <g:link controller="product" action="show" id="${rel.slug}">
                                                    ${rel?.name}
                                                </g:link>
                                            </h3>
                                        </div>
                                    </div>
                                    <!-- End Post Item -->
                                </div>
                            </g:each>
                        </div>
                    </div>
                    <!-- End Related Products -->
                </g:if>

              <div class="row">
                  <div class="col-md-8 col-md-offset-2">
                      <!-- Start Info -->
                      <div class="left-small margin-bottom10">
                          <g:render template="/templates/product_footer"/>
                      </div>
                      <!-- End Info -->
                  </div>
              </div>
          </div>
      </div>
      <!-- End Content -->
  </div>
  <!-- End Wrap Component -->
</body>
</html>