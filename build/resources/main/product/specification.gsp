<%--
  Created by IntelliJ IDEA.
  User: muyalif
  Date: 05/12/17
  Time: 16.43
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title></title>
    <meta name="layout" content="canon"/>
</head>

<body>
    <!-- Start Wrap Component -->
    <div class="wrap-component">
        <g:render template="/templates/product_header"/>

        <div class="clear-inner-page"></div>

        <div class="clearfix"></div>

        <!-- Start Content -->
        <div class="content-detail padding-bottom30">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1">

                        <!-- Start Main Specification -->
                        <div class="main-specification">
                            <img src="${productImage}" class="img-responsive" alt=""/>
                            <div class="link">
                                <g:if test="${productManual}">
                                    <a href="${productManual}" class="btn btn-default margin-bottom20">View online manual</a><br />
                                </g:if>
                                <g:if test="${productBrochure}">
                                    <a href="${productBrochure}" class="btn btn-primary">Download Brochure</a>
                                </g:if>
                            </div>
                        </div>
                        <!-- End Main Specification -->

                        <div class="section-table-info">
                            <g:each in="${productSpecifications}" var="group">
                                <!-- Start Wrap Table -->
                                <div class="wrap-table-info">
                                    <h3 class="table-title">${group.name}</h3>
                                    <table class="table table-info">
                                        <g:each in="${group.specList}" var="spec">
                                            <tr>
                                                <td class="title">${spec.name}</td>
                                                <td>
                                                    ${raw(spec.value?.replace("\n", "<br />"))}
                                                </td>
                                            </tr>
                                        </g:each>
                                    </table>
                                </div>
                                <!-- End Wrap Table -->
                            </g:each>
                            
                        </div>

                        <p>
                            ${product?.specNote}
                        </p>

                        <!-- Start Info -->
                        <div class="left-small">
                            <g:render template="/templates/product_footer"/>
                        </div>
                        <!-- End Info -->

                    </div>
                </div>
            </div>
        </div>
        <!-- End Content -->
    </div>
    <!-- End Wrap Component -->
</body>
</html>