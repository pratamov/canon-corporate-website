<%--
  Created by IntelliJ IDEA.
  User: muyalif
  Date: 06/12/17
  Time: 10.10
--%>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>
        <g:message code="product.category.title" default="Canon - Product Category"/>
    </title>
    <meta name="layout" content="canon"/>
</head>

<body>
<!-- Start Wrap Component -->
<div class="wrap-component">
    <g:each in="${categoryList}" var="category">
        <!-- Start Content -->
        <div class="parallax content-white" data-parallax="scroll" data-image-src="${category?.backgroundUrl}">
            <div class="overlay"></div>
            <div class="container">
                <div class="row">
                    <div class="col-sm-offset-1 col-sm-10">

                        <!-- Start Category -->
                        <div class="content-category">
                            <div class="row">
                                <div class="col-xs-6">
                                    <div class="vertical-middle">
                                        <div class="content">
                                            <h1 class="title">${category?.name}</h1>
                                            <h4 class="sub-title">${category?.subText}</h4>
                                            <g:link class="btn btn-primary" controller="product" action="list" params="[segment: params?.segment, category: category?.slug, lang: params?.lang, site: params?.site]">
                                                <g:message code="product.category.button.viewProducts.label" default="View Products"/>
                                            </g:link>
                                        </div>
                                    </div>
                                </div>
                                <g:if test="${category?.defaultMedia?.mediaUrl}">
                                    <div class="col-xs-6">
                                        <img class="getheight" src="${category?.defaultMedia?.mediaUrl}" alt="" />
                                    </div>
                                </g:if>
                            </div>
                        </div>
                        <!-- End Category -->

                    </div>
                </div>
            </div>
        </div>
        <!-- End Content -->
    </g:each>
</div>
<!-- End Wrap Component -->
</body>
</html>