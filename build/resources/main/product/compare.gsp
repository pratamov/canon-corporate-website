<%--
  Created by IntelliJ IDEA.
  User: muyalif
  Date: 05/12/17
  Time: 16.58
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title></title>
    <meta name="layout" content="canon"/>
</head>

<body>
<!-- Start Wrap Component -->
<div class="wrap-component">
    <g:render template="/templates/product_header"/>

    <div class="clear-inner-page"></div>

    <div class="clearfix"></div>

    <!-- Start Contetn Overview -->
    <div class="content-detail padding-bottom30 m-padding-top30">
        <div class="container">
            <!-- Start Compare -->
            <div class="wrap-compare">
                <div class="row">
                    <div class="col-md-12">

                        <!-- Start Products -->
                        <div class="wrap-carousel-in-small">
                            <div class="row carousel-in-small">

                                <div class="col-sm-4 col-xs-12 select-column product-compare" data-index="1">
                                    <select id="product-a" class="form-control select input-sm" disabled="disabled">
                                        <option value="" disabled="disabled">Select Product</option>
                                        <g:each in="${products}" var="p" status="i">
                                            <g:if test="${p?.id==productA?.id}">
                                                <option value="${p?.slug}" selected="selected">${p?.name}</option>
                                            </g:if>
                                            <g:else>
                                                <option value="${p?.slug}">${p?.name}</option>
                                            </g:else>
                                        </g:each>
                                    </select>

                                <!-- Start Post Item -->
                                    <g:if test="${productA}">
                                        <div class="post-item match-height">
                                            <img src="${productA?.getDefaultMedia()?.media?.url}" alt="" />
                                            <div class="content">
                                                <h5 class="title margin-bottom10 m-margin-bottom0"><a href="#">${productA?.name}</a></h5>
                                            </div>
                                        </div>
                                        <h2 class="price">
                                            <g:if test="${price}">
                                                ${formatNumber(number: product?.price, type:'currency', currencyCode: product?.currencyCode, locale: 'en_US')}
                                            </g:if>
                                            <g:else>
                                                -
                                            </g:else>
                                        </h2>
                                    </g:if>
                                <!-- End Post Item -->
                                </div>
                                <div class="col-sm-4 col-xs-12 product-compare" data-index="2">
                                    <select id="product-b" class="form-control select input-sm select-product-compare">
                                        <option value="" disabled="disabled" selected="selected">Select Product</option>
                                        <g:each in="${products}" var="p" status="i">
                                            <g:if test="${p?.id==productB?.id}">
                                                <option value="${p?.slug}" selected="selected">${p?.name}</option>
                                            </g:if>
                                            <g:else>
                                                <option value="${p?.slug}">${p?.name}</option>
                                            </g:else>
                                        </g:each>
                                    </select>

                                <!-- Start Post Item -->
                                    <div id="overview-peoduct-b" style="display: ${productB?'':'none'}">
                                        <div class="post-item match-height">
                                            <img src="${productB?.getDefaultMedia()?.media?.url}" alt="" />
                                            <div class="content">
                                                <h5 class="title margin-bottom10 m-margin-bottom0"><a href="#" id="product-b-name">${productB?.name}</a></h5>
                                            </div>
                                        </div>
                                        <h2 class="price">-</h2>
                                    </div>

                                <!-- End Post Item -->
                                </div>
                                <div class="col-sm-4 col-xs-12 product-compare" data-index="3">
                                    <select id="product-c" class="form-control select input-sm select-product-compare">
                                        <option value="" disabled="disabled" selected="selected">Select Product</option>
                                        <g:each in="${products}" var="p" status="i">
                                            <g:if test="${p?.id==productC?.id}">
                                                <option value="${p?.slug}" selected="selected">${p?.name}</option>
                                            </g:if>
                                            <g:else>
                                                <option value="${p?.slug}">${p?.name}</option>
                                            </g:else>
                                        </g:each>
                                    </select>
                                    <div id="overview-peoduct-c" style="display: ${productC?'':'none'}">
                                        <div class="post-item match-height">
                                            <img src="${productC?.getDefaultMedia()?.media?.url}" alt="" />
                                            <div class="content">
                                                <h5 class="title margin-bottom10 m-margin-bottom0"><a href="#" id="product-c-name">${productC?.name}</a></h5>
                                            </div>
                                        </div>
                                        <h2 class="price">SGD 650</h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Products -->

                    </div>
                </div>

                <!-- Start Wrap Table -->
                <div id="specs-table">
                    <g:each in="${groups}" var="group" status="i">
                        <div class="wrap-table-info table-compare">
                            <h3 class="table-title">${group?.key}</h3>
                            <table class="table table-info">
                                <g:each in="${group?.value}" var="spec" status="j">
                                    <tr class="">
                                        <td class="title">${spec.name}</td>
                                        <td>${specs.get(spec.name)}</td>
                                    </tr>
                                </g:each>
                            </table>
                        </div>
                    </g:each>
                </div>
            </div>
            <!-- End Compare -->

            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <!-- Start Info -->
                    <div class="post-info left-small margin-bottom20">
                        <span class="fa fa-circle"></span>All prices above are recommended retail price in SGD, unless otherwise stated.
                    </div>
                    <!-- End Info -->
                </div>
            </div>
        </div>
    </div>
    <!-- End Content Overview -->
</div>
<!-- End Wrap Component -->
</body>
</html>