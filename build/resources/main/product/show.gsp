<%--
  Created by IntelliJ IDEA.
  User: muyalif
  Date: 04/12/17
  Time: 17.02
--%>

<%@ page import="canon.ProductComponent" contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>
        <g:message code="product.detail.title" default="Canon - {0}" args="${[product?.name]}"/>
    </title>
    <meta name="layout" content="canon"/>
</head>

<body>
    <!-- Start Wrap PageComponent -->
    <div class="wrap-component">
        <g:render template="/templates/product_header"/>

        <g:each in="${productComponentList as List<ProductComponent>}" var="pc" status="i">
            <g:renderComp component="${pc.component}" product="${product}" />
            <g:if test="${i==0}">
                <!-- Social Share -->
                <div class="section-share">
                    <div class="container">
                        <a href="#" class="toggle-share show-medium"><span class="fa fa-share-alt"></span></a>
                        <ul class="social-share hide-medium">
                            <li><a href="#"><span class="fa fa-facebook"></span></a></li>
                            <li><a href="#"><span class="fa fa-twitter"></span></a></li>
                            <li><a href="#"><span class="fa fa-feed"></span></a></li>
                        </ul>
                    </div>
                </div>
            </g:if>
        </g:each>
    </div>
    <g:render template="/templates/product_footer"/>
    <!-- End Wrap PageComponent -->
</body>
</html>