
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title></title>
    <meta name="layout" content="canon"/>
</head>

<body>
    <!-- Start Content -->
    <g:each in="${categoryList}" var="category">
        <div class="parallax content-white" data-parallax="scroll" data-image-src="${category?.backgroundUrl}">
            <div class="overlay"></div>
            <div class="container">
                <div class="row">
                    <div class="col-sm-offset-1 col-sm-10">

                        <!-- Start Category -->
                        <div class="content-category">
                            <div class="row">
                                <div class="col-xs-6">
                                    <div class="vertical-middle">
                                        <div class="content">
                                            <h1 class="title">${category?.name}</h1>
                                            <h4 class="sub-title">${category?.subText}</h4>
                                            <g:link class="btn btn-primary" controller="category" action="show" id="${category?.slug}">
                                                <g:message code="category.button.view.label" default="View Products"/>
                                            </g:link>
                                        </div>
                                    </div>
                                </div>
                                <g:if test="${categoryMediaMap.get(category?.id)?.mediaUrl}">
                                    <div class="col-xs-6">
                                        <img class="getheight" src="${categoryMediaMap.get(category?.id)?.mediaUrl}" alt="" />
                                    </div>
                                </g:if>
                            </div>
                        </div>
                        <!-- End Category -->

                    </div>
                </div>
            </div>
        </div>
    </g:each>
    
    <!-- End Content -->
    <script type="text/javascript">
        window.onload = function () {
            setInterval(function () {
                $('.parallax').parallax();
            }, 500);
        }
    </script>
</body>
</html>