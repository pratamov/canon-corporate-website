/***
 * Custom error placement for server-side validation.
 */
$.extend($.fn, {
    serverErrorPlacement: function(jsonError){
        if ( !$( this[ 0 ] ).is( "form" ) ) {
            return;
        }

        var $form = $( this[ 0 ] );
        $form.find($form.validate().settings.errorElement + "." + $form.validate().settings.errorClass).remove();
        $form.find(".has-error").removeClass("has-error");

        for(var i = 0; i < jsonError.errors.length; i++ ){
            var errorFieldName = jsonError.errors[i].field;
            var errorMessage = jsonError.errors[i].message;

            $form.find("[data-field-name='"+errorFieldName+"']").each(function( index ) {
                var $errorFieldElement = $(this);
                var fieldElId = $errorFieldElement.attr("id");

                var divError = "<" + $form.validate().settings.errorElement + "></"+ $form.validate().settings.errorElement + ">";
                var $errorMsgEl = $(divError).addClass($form.validate().settings.errorClass).attr("id",fieldElId+"-error").attr("for",fieldElId).html(errorMessage);

                $form.validate().settings.errorPlacement($errorMsgEl, $errorFieldElement);
                if ($errorFieldElement.hasClass("valid")) {
                    $errorFieldElement.removeClass("valid");
                }

                if (!$errorFieldElement.hasClass($form.validate().settings.errorClass)) {
                    $errorFieldElement.addClass($form.validate().settings.errorClass);
                }
            });
        }
    }
});
