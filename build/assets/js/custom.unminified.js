var core = {
    // ------------------------------------------------------------------------------ //
    // Initial function
    // ------------------------------------------------------------------------------ //
    init: function(){
        this.event();
        this.layout();
    },
    // ------------------------------------------------------------------------------ //
    // Event
    // ------------------------------------------------------------------------------ //
    event: function(){
        // Summernote
        // ==================== //
        $('.summernote').summernote();

        $('.summernote.disabled').next().find(".note-editable").attr("contenteditable", false);

        // Sort Table
        // ==================== //
        $("table.sortable").stupidtable();
        
        // Tag Input
        // ==================== //
        $('.tagsinput').tagsinput({
            tagClass: 'label label-primary'
        });

        // Dropdown List
        // ==================== //
        $('.wrap-list-check li').each(function(){
            $('.main input', this).on('change', function(){
                var list = $(this).closest('li');
                if(this.checked){
                    list.addClass('on');
                }else{
                    list.removeClass('on');
                    var elem = list.find('ul').find('li');
                    elem.removeClass('on');
                    for(var i=0; i < elem.length; i++){
                        var input = elem[i].getElementsByTagName("input")[0];
                        input.checked = false;
                    }
                }
            });
        });
        $('.clearCheck').on('click', function(e){
            e.preventDefault();
            $('.wrap-list-check li').removeClass('on');
            $('.checkbox input').prop('checked', false);
        });

        // Typehead 
        // ==================== //
        $('.typeahead').typeahead({
            source: [
                {"name": "Afghanistan", "code": "AF", "ccn0": "040"},
                {"name": "Land Islands", "code": "AX", "ccn0": "050"},
                {"name": "Albania", "code": "AL","ccn0": "060"},
                {"name": "Algeria", "code": "DZ","ccn0": "070"}
            ]
        });

        // Tooltips 
        // ==================== //
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })

        // Check all
        // ==================== //
        $(".checkall").on('click', function(){
            if(this.checked){
                $('.checkbox.single input').prop('checked', true);
            }else{
                $('.checkbox.single input').prop('checked', false);
            }
        });
        
        // Select 2
        // ==================== //
        $(".select").each(function(){
            var placeholder = $(this).data('placeholder');
            $(this).select2({
                placeholder: placeholder,
                allowClear: true
            });
        });
        $(".select-multiple").each(function(){
            var placeholder = $(this).data('placeholder');
            $(this).select2({
                placeholder: placeholder,
                multiple:true,
                tokenSeparators: [','],
            });
        });

        // Date Input
        // ==================== //
        $('.date').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            //forceParse: false,
            calendarWeeks: true,
            autoclose: true,
            format: "dd M yyyy"
        });

        // Dropzone
        // ==================== //
        Dropzone.options.dropzoneForm = {
            paramName: "file", // The name that will be used to transfer the file
            maxFilesize: 2, // MB
            dictDefaultMessage: "<strong>Drop files here or click to upload. </strong></br> (This is just a demo dropzone. Selected files are not actually uploaded.)"
        };
        Dropzone.options.dropzoneForm2 = {
            paramName: "file", // The name that will be used to transfer the file
            maxFilesize: 2, // MB
            dictDefaultMessage: "asdas"
        };
        

        // Modal on show
        // ==================== //
        $( ".modal" ).on('shown.bs.modal', function(){
            $( ".modal" ).find('.select').each(function(){
                var placeholder = $(this).data('placeholder');
                $(this).select2({
                    placeholder: placeholder,
                    allowClear: true
                });
            });
        });

        $("#GotoPreview").on( "click", function() {
                $('#modalRequestFeedback').modal('hide');  
        });

        $("#GotoSuccesful, #GobackRequestFeedback").on( "click", function() {
            $('#modalPreviewFeedback').modal('hide');  
        });

        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            $( ".tab-pane" ).find('.select').each(function(){
                var placeholder = $(this).data('placeholder');
                $(this).select2({
                    placeholder: placeholder,
                    allowClear: true
                });
            });
        })

        // Dual Lisbox
        // ===================== //
        $('.dual_select').bootstrapDualListbox({
            selectorMinimalHeight: 160
        });

        // Input Preview Image
        // ===================== //
        var typingTimer;                //timer identifier
        var doneTypingInterval = 500;  //time in ms, 5 second for example
        var $input = $('.input-image');

        //on keyup, start the countdown
        $input.on('keyup', function () {
            clearTimeout(typingTimer);
            typingTimer = setTimeout(doneTyping, doneTypingInterval);
        });

        //on keydown, clear the countdown 
        $input.on('keydown', function () {
            clearTimeout(typingTimer);
        });

        //user is "finished typing," do something
        function doneTyping () {
            var urlImage = $input.val();
            $('#preview-image').attr('src', urlImage);
        }

        // ------------------------------------ //
        // Dropdown List
        // ------------------------------------ //
        $('.wrap-dropdown-list').each(function(){
            var $this = $(this);
            var $input = $('.list-value', this);
            var $list = $('.list-collapse', this);
            var arrName = [];
            
            $('.dropdown-list .arrow', this).on('click', function(){
                $(this).closest('.dropdown-list').closest('ul').find('.dropdown-list').removeClass('on');
                var checkist = $(this).closest('.dropdown-list').hasClass('on');
                $(this).closest('.dropdown-list').toggleClass('on');
            });
            
            $('.checkbox > input', this).on('change', function(e){
                var parentUl = $(this).closest('.checkbox').closest('li').closest('ul');
                var getParent = function(parent){
                    var ul = parent.closest('li').closest('ul');
                    if(ul.length >= 1){
                        getParent(ul);
                        var checkInput = ul.find('.dropdown-list.on').find('.checkbox').eq(0);
                        var inputSelected = checkInput.find('input').eq(0);
                        var label = checkInput.find('label').eq(0).text();
                        inputSelected.prop('checked', true);
                        arrName.push($.trim(label));
                    }
                }

                var findChild = $(this).closest('li.dropdown-list').find('ul').eq(0).find('li');
                var getChild = function(child){
                    console.log(child);
                    var checkInput = child.find('.checkbox');
                    var inputSelected = checkInput.find('input');
                    inputSelected.prop('checked', false);
                }

                if(this.checked){
                    arrName = [];
                    $('.wrap-dropdown-list .checkbox > input').prop('checked', false);
                    $(this).prop('checked', true);
                    var label = $(this).closest('.checkbox').find('label').eq(0).text();
                    getParent(parentUl);
                    arrName.push($.trim(label));
                    $input.val(arrName.join(" > "));
                }else{
                    var label = $(this).closest('.checkbox').find('label').eq(0).text();
                    var index = arrName.indexOf($.trim(label));
                    if (index !== -1) {
                        arrName.splice(index, 1);
                    }

                    getChild(findChild);
                    arrName = [];
                    $this.find('input:checked').each(function(index){
                        var label = $(this).closest('.checkbox').find('label').text();
                        arrName.push($.trim(label));
                        console.log(arrName);
                        $input.val(arrName.join(" > "));
                    });
                }
            });

            $input.on('focus', function(){
                $this.addClass('on');
            });

            $('.show-dropdown', this).on('click', function(){
                $this.addClass('on');
            });
            $('.hide-dropdown', this).on('click', function(){
                $this.removeClass('on');
            });

            $input.keyup(function(){
                var searchText = $(this).val();
                $this.find('ul > li').each(function(){
                    
                    var currentLiText = $('.checkbox > label', this).text().toLowerCase(),
                        showCurrentLi = currentLiText.toLowerCase().indexOf(searchText) !== -1;
                    
                    $(this).toggle(showCurrentLi);
                    
                });     
            });

            $('.refresh', this).on('click', function(){
                $input.val('');
                $this.removeClass('on');
            });

            $('input:checked', this).each(function(index){
                var label = $(this).closest('.checkbox').find('label').text();
                arrName.push($.trim(label));
                $input.val(arrName.join(" > "));
            });
        });
    },

    // ------------------------------------------------------------------------------ //
    // Layout
    // ------------------------------------------------------------------------------ //
    layout : function(){
        
    },

    // ------------------------------------------------------------------------------ //
    // Reset Grid
    // ------------------------------------------------------------------------------ //
    ResetGridColumns : function(){
        $('.row.grid-same').each(function() {
            // find all columns
            var $cs = $(this).children('[class*="col-"]');

            // reset the height
            $cs.css('height', 'auto');

            // set the heights per row
            var rowWidth = $(this).width();
            var $curCols = $();
            var curMax = 0;
            var curWidth = 0;
            $cs.each(function() {
                var w = $(this).width();
                var h = $(this).height();
                if(curWidth+w <= rowWidth) {
                    $curCols = $curCols.add(this);
                    curWidth+= w;
                    if(h>curMax) curMax = h;
                } else {
                    if($curCols.length>1) $curCols.css('height', curMax+'px');
                    $curCols = $(this);
                    curWidth = w;
                    curMax = h;
                }
            });
            if($curCols.length>1) $curCols.css('height', curMax+'px');
        });
    }
};


(function($) {
    "use strict";
    // ------------------------------------------------------------------------------ //
    // Initaial on Load
    // ------------------------------------------------------------------------------ //
    $(window).on('load', function(){
        core.init();
    });

    // ------------------------------------------------------------------------------ //
    // Resize window
    // ------------------------------------------------------------------------------ //
    $(window).on('resize', function(){
        core.layout();
        core.ResetGridColumns();
        setTimeout(function(){
            core.layout();
            core.ResetGridColumns();
        }, 500);
    });
})(jQuery);
