/**!
 * KEditor - Kademi content editor
 * @copyright: Kademi (http://kademi.co)
 * @author: Kademi (http://kademi.co)
 * @version: 1.1.5
 * @dependencies: $, $.fn.draggable, $.fn.droppable, $.fn.sortable, Bootstrap (optional), FontAwesome (optional)
 */
!function(e){var t=e.keditor,n=t.log;t.components.googlemap={getContent:function(e,t){n('getContent "googlemap" component',e);var o=e.children(".keditor-component-content");return o.find(".googlemap-cover").remove(),o.html()},settingEnabled:!0,settingTitle:"Google Map Settings",initSettingForm:function(t,o){n('initSettingForm "googlemap" component'),t.append('<form class="form-horizontal">   <div class="form-group">       <div class="col-sm-12">           <button type="button" class="btn btn-block btn-primary btn-googlemap-edit">Update Map</button>       </div>   </div>   <div class="form-group">       <label class="col-sm-12">Aspect Ratio</label>       <div class="col-sm-12">           <button type="button" class="btn btn-sm btn-default btn-googlemap-169">16:9</button>           <button type="button" class="btn btn-sm btn-default btn-googlemap-43">4:3</button>       </div>   </div></form>');var i=t.find(".btn-googlemap-edit");i.on("click",function(t){t.preventDefault();var n=prompt("Please enter Google Map embed code in here:"),i=e(n),s=i.attr("src");i.length>0&&s&&s.length>0?o.getSettingComponent().find(".embed-responsive-item").attr("src",s):alert("Your Google Map embed code is invalid!")});var s=t.find(".btn-googlemap-169");s.on("click",function(e){e.preventDefault(),o.getSettingComponent().find(".embed-responsive").removeClass("embed-responsive-4by3").addClass("embed-responsive-16by9")});var a=t.find(".btn-googlemap-43");a.on("click",function(e){e.preventDefault(),o.getSettingComponent().find(".embed-responsive").removeClass("embed-responsive-16by9").addClass("embed-responsive-4by3")})}}}(jQuery);
//# sourceMappingURL=keditor-component-googlemap-1.1.5.min.js.map

