/**
 * Created by marjan on 12/01/18.
 */
(function ($) {
    $.extend({
        TextIComp: function(htmlString) {
            var _map = {
                content: ""
            };
            _map.content = htmlString;
            return _map;
        },
        ImageIComp: function(htmlString) {
            var _map = {
                alt: "",
                caption: "",
                copyright: "",
                imageUrl: "",
                mediaId: null,
                source: "",
                title: ""
            };
            var _cDom = $(htmlString);
            var _img = _cDom.find(".component-image-image");
            _map.caption = _cDom.find(".component-image-caption").html();
            _map.title = _cDom.find(".component-image-title").html();

            _map.alt = _img.attr("alt");
            _map.copyright = _img.data("copyright");
            _map.imageUrl = _img.attr("src");
            _map.mediaId = _img.data("id");
            _map.source = _img.data("source");
            return _map;
        },
        TextLeftIComp: function(htmlString) {
            var _map = {
                alt: "",
                caption: "",
                copyright: "",
                imageUrl: "",
                mediaId: null,
                source: "",
                title: ""
            };
            var _cDom = $(htmlString);
            var _img = _cDom.find(".component-text-image-image");
            _map.content = _cDom.find(".component-text-image-content").html();

            _map.alt = _img.attr("alt");
            _map.copyright = _img.data("copyright");
            _map.imageUrl = _img.attr("src");
            _map.mediaId = _img.data("id");
            _map.source = _img.data("source");
            return _map;
        },
        TextRightIComp: function(htmlString) {
            var _map = {
                alt: "",
                caption: "",
                copyright: "",
                imageUrl: "",
                mediaId: null,
                source: "",
                title: ""
            };
            var _cDom = $(htmlString);
            var _img = _cDom.find(".component-text-image-image");
            _map.content = _cDom.find(".component-text-image-content").html();

            _map.alt = _img.attr("alt");
            _map.copyright = _img.data("copyright");
            _map.imageUrl = _img.attr("src");
            _map.mediaId = _img.data("id");
            _map.source = _img.data("source");
            return _map;
        }
    });
})(jQuery);

(function ($) {
    var KEditor = $.keditor;
    var flog = KEditor.log;

    CKEDITOR.disableAutoInline = true;

    // Text component
    // ---------------------------------------------------------------------
    KEditor.components['text'] = {
        options: {
            toolbarGroups: [
                {name: 'document', groups: ['mode', 'document', 'doctools']},
                {name: 'editing', groups: ['find', 'selection', 'spellchecker', 'editing']},
                {name: 'forms', groups: ['forms']},
                {name: 'basicstyles', groups: ['basicstyles', 'cleanup']},
                {name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align', 'bidi', 'paragraph']},
                {name: 'links', groups: ['links']},
                {name: 'insert', groups: ['insert']},
                {name: 'clipboard', groups: ['clipboard', 'undo']},
                {name: 'styles', groups: ['styles']},
                {name: 'colors', groups: ['colors']},
                {name: 'tools', groups: ['tools']},
                {name: 'others', groups: ['others']}
            ],
            title: false,
            allowedContent: true, // DISABLES Advanced Content Filter. This is so templates with classes: allowed through
            bodyId: 'editor',
            templates_replaceContent: false,
            enterMode: 'P',
            forceEnterMode: true,
            format_tags: 'p;h1;h2;h3;h4;h5;h6',
            removePlugins: 'table,magicline,tabletools',
            removeButtons: 'Save,NewPage,Preview,Print,Templates,PasteText,PasteFromWord,Find,Replace,SelectAll,Scayt,Form,HiddenField,ImageButton,Button,Select,Textarea,TextField,Radio,Checkbox,Outdent,Indent,Blockquote,CreateDiv,Language,Table,HorizontalRule,Smiley,SpecialChar,PageBreak,Iframe,Styles,BGColor,Maximize,About,ShowBlocks,BidiLtr,BidiRtl,Flash,Image,Subscript,Superscript,Anchor',
            minimumChangeMilliseconds: 100
        },

        init: function (contentArea, container, component, keditor) {
            flog('init "text" component', component);

            var self = this;
            var options = keditor.options;

            var componentContent = component.children('.keditor-component-content');
            componentContent.prop('contenteditable', true);

            componentContent.on('input', function (e) {
                if (typeof options.onComponentChanged === 'function') {
                    options.onComponentChanged.call(contentArea, e, component);
                }

                if (typeof options.onContainerChanged === 'function') {
                    options.onContainerChanged.call(contentArea, e, container);
                }

                if (typeof options.onContentChanged === 'function') {
                    options.onContentChanged.call(contentArea, e);
                }
            });

            var editor = componentContent.ckeditor(self.options).editor;
            editor.on('instanceReady', function () {
                flog('CKEditor is ready', component);

                if (typeof options.onComponentReady === 'function') {
                    options.onComponentReady.call(contentArea, component, editor);
                }
            });
        },

        getContent: function (component, keditor) {
            flog('getContent "text" component', component);

            var componentContent = component.find('.keditor-component-content');
            var id = componentContent.attr('id');
            var editor = CKEDITOR.instances[id];
            if (editor) {
                return editor.getData();
            } else {
                return componentContent.html();
            }
        },

        destroy: function (component, keditor) {
            flog('destroy "text" component', component);

            var id = component.find('.keditor-component-content').attr('id');
            var editor = CKEDITOR.instances[id];
            if (editor) {
                editor.destroy();
            }
        }
    };

})(jQuery);

(function ($) {
    var KEditor = $.keditor;
    var flog = KEditor.log;
    CKEDITOR.disableAutoInline = true;

    $.keditor.components['image'] = {
        options: {
            toolbarGroups: [
                {name: 'document', groups: ['mode', 'document', 'doctools']},
                {name: 'editing', groups: ['find', 'selection', 'spellchecker', 'editing']},
                {name: 'basicstyles', groups: ['basicstyles', 'cleanup']},
                {name: 'links', groups: ['links']},
                {name: 'clipboard', groups: ['clipboard', 'undo']},
                {name: 'colors', groups: ['colors']}
            ],
            title: false,
            allowedContent: true, // DISABLES Advanced Content Filter. This is so templates with classes: allowed through
            bodyId: 'editor',
            templates_replaceContent: false,
            enterMode: 'br',
            forceEnterMode: true,
            format_tags: 'p;h1;h2;h3;h4;h5;h6',
            removePlugins: 'table,magicline,tabletools',
            removeButtons: 'Save,NewPage,Preview,Print,Templates,PasteText,PasteFromWord,Find,Replace,SelectAll,Scayt,Form,HiddenField,ImageButton,Button,Select,Textarea,TextField,Radio,Checkbox,Outdent,Indent,Blockquote,CreateDiv,Language,Table,HorizontalRule,Smiley,SpecialChar,PageBreak,Iframe,Styles,BGColor,Maximize,About,ShowBlocks,BidiLtr,BidiRtl,Flash,Image,Subscript,Superscript,Anchor',
            minimumChangeMilliseconds: 100
        },
        init: function (contentArea, container, component, keditor) {
            flog('init "image" component', component);

            var self = this;
            var options = keditor.options;

            var componentContent = component.find('.component-image-title, .component-image-caption');
            componentContent.prop('contenteditable', true);

            componentContent.on('input', function (e) {
                if (typeof options.onComponentChanged === 'function') {
                    options.onComponentChanged.call(contentArea, e, component);
                }

                if (typeof options.onContainerChanged === 'function') {
                    options.onContainerChanged.call(contentArea, e, container);
                }

                if (typeof options.onContentChanged === 'function') {
                    options.onContentChanged.call(contentArea, e);
                }
            });

            var editor = componentContent.ckeditor(self.options).editor;
            editor.on('instanceReady', function () {
                flog('CKEditor is ready', component);

                if (typeof options.onComponentReady === 'function') {
                    options.onComponentReady.call(contentArea, component, editor);
                }
            });
        },
        getContent: function (component, keditor) {
            flog('getContent "image" component', component);

            var componentContent = component.find('.keditor-component-content');
            var id = componentContent.attr('id');
            var editor = CKEDITOR.instances[id];
            if (editor) {
                return editor.getData();
            } else {
                return componentContent.html();
            }
        },
        destroy: function (component, keditor) {
            flog('destroy "image" component', component);

            var id = component.find('.component-image-title, .component-image-caption').attr('id');
            var editor = CKEDITOR.instances[id];
            if (editor) {
                editor.destroy();
            }
        },
        settingEnabled: true,
        settingTitle: 'Image Settings',
        initSettingForm: function (form, keditor) {
            flog('initSettingForm "image" component');

            form.append(
                '<form class="form-horizontal">' +
                '   <div class="form-group">' +
                '       <div class="col-sm-12">' +
                '           <img src="" class="img-responsive thumbnail-image" />' +
                '       </div>' +
                '   </div>' +
                '   <div class="form-group">' +
                '       <div class="col-sm-12">' +
                '           <button type="button" class="btn btn-block btn-primary btn-image-edit">Change Image</button>' +
                '       </div>' +
                '   </div>' +
                '</form>'
            );
        },
        showSettingForm: function (form, component, keditor) {
            flog('showSettingForm "image" component', component);
            var _img = component.find(".component-image-image");
            if (_img) {
                var _tImg = form.find('.thumbnail-image');
                _tImg.attr("src", _img.attr("src"));

                var options = {
                    selectedClass: "selected",
                    callback: function (data) {
                        if (data) {
                            form.find(".thumbnail-image").attr("src", data.thumbnail ? data.thumbnail : data.url);
                            _img.attr("src", data.url);
                            _img.attr("alt", data.alt);
                            _img.data("alt", data.alt);
                            _img.data("copyright", data.copyright);
                            _img.data("id", data.id);
                            _img.data("source", data.source);
                        }
                    }
                };
                form.find('.btn-image-edit').damlist(options);
            }
        }
    };
})(jQuery);

(function ($) {
    var KEditor = $.keditor;
    var flog = KEditor.log;
    CKEDITOR.disableAutoInline = true;

    $.keditor.components['text-left'] = {
        options: {
            toolbarGroups: [
                {name: 'document', groups: ['mode', 'document', 'doctools']},
                {name: 'editing', groups: ['find', 'selection', 'spellchecker', 'editing']},
                {name: 'forms', groups: ['forms']},
                {name: 'basicstyles', groups: ['basicstyles', 'cleanup']},
                {name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align', 'bidi', 'paragraph']},
                {name: 'links', groups: ['links']},
                {name: 'insert', groups: ['insert']},
                {name: 'clipboard', groups: ['clipboard', 'undo']},
                {name: 'styles', groups: ['styles']},
                {name: 'colors', groups: ['colors']},
                {name: 'tools', groups: ['tools']},
                {name: 'others', groups: ['others']}
            ],
            title: false,
            allowedContent: true, // DISABLES Advanced Content Filter. This is so templates with classes: allowed through
            bodyId: 'editor',
            templates_replaceContent: false,
            enterMode: 'P',
            forceEnterMode: true,
            format_tags: 'p;h1;h2;h3;h4;h5;h6',
            removePlugins: 'table,magicline,tabletools',
            removeButtons: 'Save,NewPage,Preview,Print,Templates,PasteText,PasteFromWord,Find,Replace,SelectAll,Scayt,Form,HiddenField,ImageButton,Button,Select,Textarea,TextField,Radio,Checkbox,Outdent,Indent,Blockquote,CreateDiv,Language,Table,HorizontalRule,Smiley,SpecialChar,PageBreak,Iframe,Styles,BGColor,Maximize,About,ShowBlocks,BidiLtr,BidiRtl,Flash,Image,Subscript,Superscript,Anchor',
            minimumChangeMilliseconds: 100
        },
        init: function (contentArea, container, component, keditor) {
            flog('init "image" component', component);

            var self = this;
            var options = keditor.options;

            var componentContent = component.find('.component-text-image-content');
            componentContent.prop('contenteditable', true);

            componentContent.on('input', function (e) {
                if (typeof options.onComponentChanged === 'function') {
                    options.onComponentChanged.call(contentArea, e, component);
                }

                if (typeof options.onContainerChanged === 'function') {
                    options.onContainerChanged.call(contentArea, e, container);
                }

                if (typeof options.onContentChanged === 'function') {
                    options.onContentChanged.call(contentArea, e);
                }
            });

            var editor = componentContent.ckeditor(self.options).editor;
            editor.on('instanceReady', function () {
                flog('CKEditor is ready', component);

                if (typeof options.onComponentReady === 'function') {
                    options.onComponentReady.call(contentArea, component, editor);
                }
            });
        },
        getContent: function (component, keditor) {
            flog('getContent "image" component', component);

            var componentContent = component.find('.keditor-component-content');
            var id = componentContent.attr('id');
            var editor = CKEDITOR.instances[id];
            if (editor) {
                return editor.getData();
            } else {
                return componentContent.html();
            }
        },
        destroy: function (component, keditor) {
            flog('destroy "image" component', component);

            var id = component.find('.component-text-image-content').attr('id');
            var editor = CKEDITOR.instances[id];
            if (editor) {
                editor.destroy();
            }
        },
        settingEnabled: true,
        settingTitle: 'Image Settings',
        initSettingForm: function (form, keditor) {
            flog('initSettingForm "image" component');

            form.append(
                '<form class="form-horizontal">' +
                '   <div class="form-group">' +
                '       <div class="col-sm-12">' +
                '           <img src="" class="img-responsive thumbnail-image" />' +
                '       </div>' +
                '   </div>' +
                '   <div class="form-group">' +
                '       <div class="col-sm-12">' +
                '           <button type="button" class="btn btn-block btn-primary btn-image-edit">Change Image</button>' +
                '       </div>' +
                '   </div>' +
                '</form>'
            );
        },
        showSettingForm: function (form, component, keditor) {
            flog('showSettingForm "image" component', component);
            var _img = component.find(".component-text-image-image");
            if (_img) {
                var _tImg = form.find('.thumbnail-image');
                _tImg.attr("src", _img.attr("src"));

                var options = {
                    selectedClass: "selected",
                    callback: function (data) {
                        if (data) {
                            form.find(".thumbnail-image").attr("src", data.thumbnail ? data.thumbnail : data.url);
                            _img.attr("src", data.url);
                            _img.attr("alt", data.alt);
                            _img.data("alt", data.alt);
                            _img.data("copyright", data.copyright);
                            _img.data("id", data.id);
                            _img.data("source", data.source);
                        }
                    }
                };
                form.find('.btn-image-edit').damlist(options);
            }
        }
    };
})(jQuery);

(function ($) {
    var KEditor = $.keditor;
    var flog = KEditor.log;
    CKEDITOR.disableAutoInline = true;

    $.keditor.components['text-right'] = {
        options: {
            toolbarGroups: [
                {name: 'document', groups: ['mode', 'document', 'doctools']},
                {name: 'editing', groups: ['find', 'selection', 'spellchecker', 'editing']},
                {name: 'forms', groups: ['forms']},
                {name: 'basicstyles', groups: ['basicstyles', 'cleanup']},
                {name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align', 'bidi', 'paragraph']},
                {name: 'links', groups: ['links']},
                {name: 'insert', groups: ['insert']},
                {name: 'clipboard', groups: ['clipboard', 'undo']},
                {name: 'styles', groups: ['styles']},
                {name: 'colors', groups: ['colors']},
                {name: 'tools', groups: ['tools']},
                {name: 'others', groups: ['others']}
            ],
            title: false,
            allowedContent: true, // DISABLES Advanced Content Filter. This is so templates with classes: allowed through
            bodyId: 'editor',
            templates_replaceContent: false,
            enterMode: 'P',
            forceEnterMode: true,
            format_tags: 'p;h1;h2;h3;h4;h5;h6',
            removePlugins: 'table,magicline,tabletools',
            removeButtons: 'Save,NewPage,Preview,Print,Templates,PasteText,PasteFromWord,Find,Replace,SelectAll,Scayt,Form,HiddenField,ImageButton,Button,Select,Textarea,TextField,Radio,Checkbox,Outdent,Indent,Blockquote,CreateDiv,Language,Table,HorizontalRule,Smiley,SpecialChar,PageBreak,Iframe,Styles,BGColor,Maximize,About,ShowBlocks,BidiLtr,BidiRtl,Flash,Image,Subscript,Superscript,Anchor',
            minimumChangeMilliseconds: 100
        },
        init: function (contentArea, container, component, keditor) {
            flog('init "image" component', component);

            var self = this;
            var options = keditor.options;

            var componentContent = component.find('.component-text-image-content');
            componentContent.prop('contenteditable', true);

            componentContent.on('input', function (e) {
                if (typeof options.onComponentChanged === 'function') {
                    options.onComponentChanged.call(contentArea, e, component);
                }

                if (typeof options.onContainerChanged === 'function') {
                    options.onContainerChanged.call(contentArea, e, container);
                }

                if (typeof options.onContentChanged === 'function') {
                    options.onContentChanged.call(contentArea, e);
                }
            });

            var editor = componentContent.ckeditor(self.options).editor;
            editor.on('instanceReady', function () {
                flog('CKEditor is ready', component);

                if (typeof options.onComponentReady === 'function') {
                    options.onComponentReady.call(contentArea, component, editor);
                }
            });
        },
        getContent: function (component, keditor) {
            flog('getContent "image" component', component);

            var componentContent = component.find('.keditor-component-content');
            var id = componentContent.attr('id');
            var editor = CKEDITOR.instances[id];
            if (editor) {
                return editor.getData();
            } else {
                return componentContent.html();
            }
        },
        destroy: function (component, keditor) {
            flog('destroy "image" component', component);

            var id = component.find('.component-text-image-content').attr('id');
            var editor = CKEDITOR.instances[id];
            if (editor) {
                editor.destroy();
            }
        },
        settingEnabled: true,
        settingTitle: 'Image Settings',
        initSettingForm: function (form, keditor) {
            flog('initSettingForm "image" component');

            form.append(
                '<form class="form-horizontal">' +
                '   <div class="form-group">' +
                '       <div class="col-sm-12">' +
                '           <img src="" class="img-responsive thumbnail-image" />' +
                '       </div>' +
                '   </div>' +
                '   <div class="form-group">' +
                '       <div class="col-sm-12">' +
                '           <button type="button" class="btn btn-block btn-primary btn-image-edit">Change Image</button>' +
                '       </div>' +
                '   </div>' +
                '</form>'
            );
        },
        showSettingForm: function (form, component, keditor) {
            flog('showSettingForm "image" component', component);
            var _img = component.find(".component-text-image-image");
            if (_img) {
                var _tImg = form.find('.thumbnail-image');
                _tImg.attr("src", _img.attr("src"));

                var options = {
                    selectedClass: "selected",
                    callback: function (data) {
                        if (data) {
                            form.find(".thumbnail-image").attr("src", data.thumbnail ? data.thumbnail : data.url);
                            _img.attr("src", data.url);
                            _img.attr("alt", data.alt);
                            _img.data("alt", data.alt);
                            _img.data("copyright", data.copyright);
                            _img.data("id", data.id);
                            _img.data("source", data.source);
                        }
                    }
                };
                form.find('.btn-image-edit').damlist(options);
            }
        }
    };
})(jQuery);
