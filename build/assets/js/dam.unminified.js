/**
 * Created by marjan on 15/01/18.
 */
'use strict';
(function ($) {
    var gDropzone;
    Dropzone.autoDiscover = false;
    var DAMUpload = function (target, options) {
        this.container = target;
        this.modal = options && options.modal ? options.modal : $(".dam-modal");
        this.isModal = options && options.isModal ? options.isModal : true;
        this.callback = options && options.callback ? options.callback : null;
        this.dropzone = null;
        this.data = {};
    };

    DAMUpload.prototype.init = function () {
        var self = this;
        self.container.find(".dam-upload-alt").val("");
        self.container.find(".dam-upload-copyright").val("");
        self.container.find(".dam-upload-name").val("");
        self.container.find(".dam-upload-url").val("");

        if (!gDropzone) {
            var dzOptions = {
                paramName: "file",
                uploadMultiple: false,
                autoProcessQueue: false,
                maxFiles: 1,
                init: function() {
                },
                maxfilesexceeded: function(file) {
                    this.removeAllFiles();
                    this.addFile(file);
                },
                sending: function(file) {
                    var _alt = self.container.find(".dam-upload-alt").val();
                    var _copyright = self.container.find(".dam-upload-copyright").val();
                    var _folder = self.container.find(".dam-upload-folder").val();
                    var _name = self.container.find(".dam-upload-name").val();
                    self.container.find(".dam-upload-form-alt").val(_alt ? _alt : "");
                    self.container.find(".dam-upload-form-copyright").val(_copyright ? _copyright : "");
                    self.container.find(".dam-upload-form-folder").val(_folder ? _folder : "");
                    self.container.find(".dam-upload-form-name").val(_name ? _name : "");
                },
                success: function(file, response) {
                    if (response) {
                        if (response.data) self.data = response.data;
                        if (typeof self.callback == "function") {
                            self.callback(self.data);
                            self.hide();
                        }
                    }
                }
            };
            gDropzone = new Dropzone(".dam-upload-form", dzOptions);
        }
        self.dropzone = gDropzone;
    };

    DAMUpload.prototype.show = function () {
        var self = this;
        if (self.isModal) {
            self.modal.modal("show");
        }
    };

    DAMUpload.prototype.hide = function () {
        var self = this;
        if (self.isModal) {
            self.modal.modal("hide");
        }
    };

    DAMUpload.prototype.activate = function () {
        var self = this;
        if (self.modal.length > 0) {
            self.modal.off("click", ".dam-insert-media");
            self.modal.on("click", ".dam-insert-media", function(e) {
                e.preventDefault();
                self.dropzone.processQueue();
            });
        }
    };

    // DAMList Plugins
    $.fn.damupload = function (options) {
        var _t = new DAMUpload($(this), options);
        _t.init();
        return _t;
    };

    $.fn.damupload.constructor = DAMUpload;

    // Export DAMUpload
    $.damupload = DAMUpload;
})(jQuery);

(function ($) {
    var DAMExternal = function (target, options) {
        this.container = target;
        this.modal = options && options.modal ? options.modal : $(".dam-modal");
        this.isModal = options && options.isModal ? options.isModal : true;
        this.callback = options && options.callback ? options.callback : null;
        this.dropzone = null;
        this.data = {};
    };

    DAMExternal.prototype.init = function () {
        var self = this;
        self.container.find(".dam-external-image").attr("src", "");
        self.container.find(".dam-external-image").attr("alt", "");
        self.container.find(".dam-external-from-alt").val("");
        self.container.find(".dam-external-from-copyright").val("");
        self.container.find(".dam-external-from-height").val("");
        self.container.find(".dam-external-from-name").val("");
        self.container.find(".dam-external-from-url").val("");
        self.container.find(".dam-external-from-width").val("");

        var _imgUrl = self.container.find(".dam-external-from-url");
        _imgUrl.off("change");
        _imgUrl.on("change", function () {
            var newImg = new Image();
            newImg.onload = function() {
                var height = newImg.height;
                var width = newImg.width;
                self.container.find(".dam-external-from-height").val(height);
                self.container.find(".dam-external-from-width").val(width);
            };
            newImg.src = $(this).val();
        });
    };

    DAMExternal.prototype.insert = function () {
        var self = this;
        var createUrl = self.modal.find(".dam-external-form").attr("action");
        $.ajax({
            method: "POST",
            dataType: "json",
            url: createUrl,
            data: self.modal.find(".dam-external-form").serialize(),
            success: function( response ) {
                if (response) {
                    if (response.data) self.data = response.data;
                    if (typeof self.callback == "function") {
                        self.callback(self.data);
                        self.hide();
                    }
                }
            },
            error: function(error) {
                alert(error);
            }
        });
    };

    DAMExternal.prototype.show = function () {
        var self = this;
        if (self.isModal) {
            self.modal.modal("show");
        }
    };

    DAMExternal.prototype.hide = function () {
        var self = this;
        if (self.isModal) {
            self.modal.modal("hide");
        }
    };

    DAMExternal.prototype.activate = function () {
        var self = this;
        if (self.modal.length > 0) {
            self.modal.off("click", ".dam-insert-media");
            self.modal.on("click", ".dam-insert-media", function(e) {
                e.preventDefault();
                self.insert();
            });
        }
    };

    // DAMExternal Plugins
    $.fn.damexternal = function (options) {
        var _t = new DAMExternal($(this), options);
        _t.init();
        return _t;
    };

    $.fn.damexternal.constructor = DAMExternal;

    // Export DAMExternal
    $.damexternal = DAMExternal;
})(jQuery);

(function ($) {
    var DAMList = function (target, options) {
        this.target = target;
        this.modal = options && options.modal ? options.modal : $(".dam-modal");
        this.container = options && options.container ? options.container : $(".dam-media-list");
        this.template = '<div class="dam-list-item" class="dam-list-item" data-url="{{url}}" data-id="{{id}}" data-source="{{source}}" ' +
            'data-name="{{name}}" data-type="{{type}}" data-size="{{size}}" data-dimension="{{dimension}}" data-thumbnail="{{thumbnail}}"></div>';
        if (options && options.template) {
            this.template = options.template;
        }
        this.selected = null;
        this.isModal = options && options.isModal ? options.isModal : true;
        this.selectedClass = options && options.selectedClass ? options.selectedClass : "selected";
        this.callback = options && options.callback ? options.callback : null;
        this.hasUploader = options && options.hasUploader ? options.hasUploader : true;
        this.hasExternal = options && options.hasExternal ? options.hasExternal : true;
        this.options = options;
    };

    DAMList.prototype.init = function () {
        var self = this;
        if (self.template) {
            Mustache.parse(self.template);
        }
        if (self.container.length > 0) {
            self.container.on("click", ".dam-list-item", function () {
                self.select($(this));
            });
            $(".dam-list-q").keypress(function (e) {
                var key = e.which;
                if (key == 13) {
                    self.refresh();
                }
            });
            $(".dam-list-form").on("submit", function (e) {
                e.preventDefault();
                self.refresh();
            });
        }
        self.target.on("click", function () {
            self.show();
            self.refresh();
        });

        self.activate();
        self.modal.on("click", ".dam-list-tab", function () {
            self.activate();
        });
        if (self.hasUploader) {
            var _du = self.modal.find('.dam-upload').damupload({
                callback: self.options.callback,
                modal: self.options.modal,
                isModal: self.options.isModal
            });
            self.modal.on("click", ".dam-upload-tab", function () {
                _du.activate();
            });
        }
        if (self.hasExternal) {
            var _de = self.modal.find('.dam-external').damexternal({
                callback: self.options.callback,
                modal: self.options.modal,
                isModal: self.options.isModal
            });
            self.modal.on("click", ".dam-external-tab", function () {
                _de.activate();
            });
        }
    };

    DAMList.prototype.select = function (selected) {
        var self = this;
        var _url = selected.data("url");
        var _mediaId = selected.data("id");
        var _source = selected.data("source");
        var _name = selected.data("name");
        var _type = selected.data("type");
        var _size = selected.data("size");
        var _width = selected.data("width");
        var _height = selected.data("height");
        var _thumbnail = selected.data("thumbnail");
        self.modal.find(".dam-info-name").text(_name);
        self.modal.find(".dam-info-type").text(_type);
        self.modal.find(".dam-info-size").text(_size);
        self.modal.find(".dam-info-dimension").text((_width ? _width : '-') + "x" + (_height ? _height : '-'));
        self.selected = selected;
        self.container.find("."+self.selectedClass).each(function () {
            $(this).removeClass(self.selectedClass);
        });
        selected.addClass(self.selectedClass);
    };

    DAMList.prototype.refresh = function () {
        var self = this;
        if ( self.container.length > 0 ) {
            self.container.html("");
            var listUrl = self.modal.find(".dam-list-form").attr("action");
            if (!listUrl) { // Try property data-url
                listUrl = self.modal.find(".dam-list-form").data("url");
            }
            $.ajax({
                method: "GET",
                dataType: "json",
                url: listUrl,
                data: self.modal.find(".dam-list-form").serialize(),
                success: function( data ) {
                    $.each(data.data, function(i, v) {
                        var _content = Mustache.render(self.template, v);
                        self.container.append(_content);
                    });

                    $(".dam-list-item").each(function () {
                        var _thumb = $(this).data("thumbnail");
                        $(this).css("background-image", "url('" + (_thumb ? _thumb : $(this).data("url")) + "')");
                        $(this).css("background-position", "center center");
                        $(this).css("background-size", "cover");
                    });

                    if (self.selected) {
                        // Reserved for prev selected and scroll
                    } else {
                        var _fMedia = self.container.find(".dam-list-item").first();
                        if (_fMedia.length > 0) {
                            self.select(_fMedia);
                        }
                    }
                },
                error: function(error) {
                    //alert(error);
                }
            });
        }
    };

    DAMList.prototype.getSelected = function () {
        var self = this;
        var _selected;
        if (self.selected) {
            var _url = self.selected.data("url");
            var _mediaId = self.selected.data("id");
            var _source = self.selected.data("source");
            var _name = self.selected.data("name");
            var _type = self.selected.data("type");
            var _size = self.selected.data("size");
            var _width = self.selected.data("width");
            var _height = self.selected.data("height");
            var _thumbnail = self.selected.data("thumbnail");
            _selected = {
                url: _url,
                id: _mediaId ? _mediaId : "",
                source: _source ? _source : "",
                name: _name ? _name : "",
                type: _type ? _type : "",
                size: _size ? _size : "",
                width: _width ? _width : "",
                height: _height ? _height: "",
                thumbnail: _thumbnail ? _thumbnail: _url
            };
        }
        return _selected;
    };

    DAMList.prototype.show = function () {
        var self = this;
        if (self.isModal) {
            self.modal.modal("show");
        }
    };

    DAMList.prototype.hide = function () {
        var self = this;
        if (self.isModal) {
            self.modal.modal("hide");
        }
    };

    DAMList.prototype.activate = function () {
        var self = this;
        if (self.modal.length > 0) {
            self.modal.find(".dam-tab").each(function () {
                $(this).removeClass("active");
            });
            self.modal.find(".dam-list-tab").addClass("active");
            self.modal.off("click", ".dam-insert-media");
            self.modal.on("click", ".dam-insert-media", function () {
                if (typeof self.callback == "function") {
                    self.callback(self.getSelected());
                    self.hide();
                }
            });
        }
    };

    // DAMList Plugins
    $.fn.damlist = function (options) {
        var _t = new DAMList($(this), options);
        _t.init();
        return _t;
    };

    $.fn.damlist.constructor = DAMList;

    // Export DAMList
    $.damlist = DAMList;
})(jQuery);

