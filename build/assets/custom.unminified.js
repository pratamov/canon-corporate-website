var core = {
    // ------------------------------------------------------------------------------ //
    // Initial function
    // ------------------------------------------------------------------------------ //
    init: function(){
        $('.wrap-loading').css('display','none');
        $('.wrap-component').css('display','block');
        this.owlMenu();        
        this.event();
        this.layout();
        this.ResetGridColumns();
        this.selectOption();
    },
    // ------------------------------------------------------------------------------ //
    // Event
    // ------------------------------------------------------------------------------ //
    event: function(){
        // ------------------------------------ //
        // Set Background Secction
        // ------------------------------------ //
        $('.bg').each(function(){
            var image = $(this).data('image');
            $(this).css('background-image','url('+ image +')');
        });

        // ------------------------------------ //
        // Input Range
        // ------------------------------------ //
        $('.range-input').each(function(){
            var min = $(this).data('min');
            var max = $(this).data('max');
            var from = $(this).data('from');
            var to = $(this).data('to');
            var prefix = $(this).data('prefix');
            $(this).ionRangeSlider({
                type: "double",
                grid: true,
                min: min,
                max: max,
                from: from,
                to: to,
                prefix: prefix
            });
        });

        // ------------------------------------ //
        // Set Equal Height
        // ------------------------------------ //
        $('.match-height').matchHeight();
        $('.equal-height').matchHeight();

        // ------------------------------------ //
        // Scroll Top
        // ------------------------------------ //
        $('.scroll-top a').on('click', function(){
            var body = $("html, body");
            body.stop().animate({scrollTop:0}, 500);
        });

        // ------------------------------------ //
        // Toggle Search Auto Complete
        // ------------------------------------ //
        $('.search-nav').on('click', function(e){
            e.preventDefault();
            $('.wrap-search').addClass('on');
            $('.overlay-search').addClass('on');
            $('.input-search').focus();
        });   
        $('.close-search').on('click', function(e){
            e.preventDefault();
            $('.wrap-search').removeClass('on');
            $('.overlay-search').removeClass('on');
        });
        $(window).on('resize', function(){
            $('.wrap-search').removeClass('on');
            $('.overlay-search').removeClass('on');
        });

        // ------------------------------------ //
        // Sseacrh On Mobile
        // ------------------------------------ //
        $('.clone-search').html($('.wrap-search').html());
        $('.clone-search .input-search').on('focus', function(){
            $('.wrap-search').addClass('on');
            $('.overlay-search').addClass('on');
        });
        $('.clone-search .input-search').on('blur', function(){
            $('.wrap-search').removeClass('on');
            $('.overlay-search').removeClass('on');
        });

        // ------------------------------------ //
        // Toggle Menus
        // ------------------------------------ //
        $('.wrap-menus').each(function(){
            var elem = $(this);
            $('a.toggle-menus', this).each(function(){
                var text = elem.find('li.active > a').html();
                $(this).html(text);
                $(this).on('click', function(e){
                    e.preventDefault();
                    elem.toggleClass('on');
                });
            }); 
        });

        // ------------------------------------ //
        // Search Autocomplete
        // ------------------------------------ //
        var data = [
            { id: 1, name: 'Toronto' },
            { id: 2, name: 'Montreal' },
            { id: 3, name: 'New York' },
            { id: 4, name: 'Buffalo' },
            { id: 5, name: 'Boston' },
            { id: 6, name: 'Columbus' },
            { id: 7, name: 'Dallas' },
            { id: 8, name: 'Vancouver' },
            { id: 9, name: 'Seattle' },
            { id: 10, name: 'Los Angeles' }
        ];
        this.searchAutocomplete($('.wrap-search .input-search'),data);
        this.searchAutocomplete($('.clone-search .input-search'),data);

        // ------------------------------------ //
        // Dropdown List
        // ------------------------------------ //
        $('.dropdown-list').each(function(){
            var list = $(this);
            $('.main input', this).on('change', function(){
                if(this.checked){
                    list.addClass('on');
                }else{
                    list.removeClass('on');
                }
            });
        });

        // ------------------------------------ //
        // Box Panel
        // ------------------------------------ //
        $('.box-panel, .wrap-table-info').each(function(){
            var collpase = $('.collapse', this).hasClass('in'),
                toggle = $('a.toggle', this);

            if(collpase){
                toggle.addClass('active');
            }else{
                toggle.removeClass('active');
            }

            $('a.toggle', this).each(function(){
                var $this = $(this);
                var target = $this.data('target');
                $this.on('click', function(e){e.preventDefault()});
                $(target).on('shown.bs.collapse', function () {
                    $this.addClass('active');
                });
                $(target).on('hidden.bs.collapse', function () {
                    $this.removeClass('active');
                });
            });
        });

        // Input Page pagination
        $('li.page input').each(function(){
            $(this).on('blur', function(){
                var inputVal = $(this).val();
                if(inputVal < 1){
                    $(this).val(1);
                }
            });
        });
    },

    // ------------------------------------------------------------------------------ //
    // Search Autocomplete
    // ------------------------------------------------------------------------------ //
    searchAutocomplete : function(element,object){
        // Seacrh Auto Complete
        $(element).typeahead({
            source: object,
            onSelect: function(item) {
                console.log(item);
                setTimeout(function(){
                    $('.wrap-search').removeClass('on');
                    $('.overlay-search').removeClass('on');
                }, 100);
            }
        });
    },

    // ------------------------------------------------------------------------------ //
    // Select Option
    // ------------------------------------------------------------------------------ //
    selectOption : function(){
        $(".select").each(function(){
            var placeholder = $(this).data('placeholder');
            $(this).select2({
                placeholder: placeholder,
                allowClear: true,
                minimumResultsForSearch: -1,
            });

            var inputSm = $(this).hasClass('input-sm');
            if(inputSm){
                $(this).wrap('<div class="wrap-select-sm"></div>')
            }
        });
    },

    // ------------------------------------------------------------------------------ //
    // Layout
    // ------------------------------------------------------------------------------ //
    layout : function(){
        // Vertical Content
        $('.wrap-vertical-content').each(function(){
            var height = $('.getheight', this).innerHeight();
            $('.vertical-middle', this).css('height','auto');
            $('.vertical-middle', this).css('height',height + 'px');
        });
        $('.content-category').each(function(){
            var height = $('.getheight', this).innerHeight();
            $('.vertical-middle', this).css('height','auto');
            $('.vertical-middle', this).css('height',height + 'px');
        });

        // Content Carousel
        core.slideInMobile({
            elem: '.carousel-in-small',
            autowidth: false,
            autoheight: false,
            stagePadding: 0,
            items: 2
        });

        core.slideInMobile({
            elem: '.wrap-carousel-mobile > .row',
            autowidth: false,
            autoheight: true,
            stagePadding: 30,
            items: 1,
            initResponsive : function(){
                $('.bg-img-post').matchHeight();
                $('.bg-img-post').each(function(){
                    var image = $('a img', this).attr('src');
                    $(this).css('background-image','url('+ image +')');
                    $('a', this).height($(this).innerHeight());
                });             
            },
            initFullWidth : function(){
                $('.bg-img-post a').css('height','');
                $('.bg-img-post').addClass('in-responsive');
                $('.bg-img-post').matchHeight();
                $('.bg-img-post').each(function(){
                    var image = $('a img', this).attr('src');
                    $(this).css('background-image','url('+ image +')');
                    $('a', this).height($(this).innerHeight());
                });

                $('.wrap-vertical-content').each(function(){
                    var getHeight = $(this).innerHeight();
                    $('.bg-img-post', this).css('height', getHeight);
                });
            }
        });
        
        core.slideInMobile({
            elem: '.slide-in-small',
            autowidth: true,
            autoheight: false,
            stagePadding: 40,
            items: 1
        });
    },

    // ------------------------------------------------------------------------------ //
    // Slide in mobile
    // ------------------------------------------------------------------------------ //
    slideInMobile : function(obj){
        var width = $(window).width();
        if(width < 767){ 
            $(obj.elem).each(function(){
                $(this).addClass('owl-carousel');
                $(this).owlCarousel({
                    loop:false,
                    autoHeight: obj.autoheight,
                    autowidth: obj.autowidth,
                    margin: 0,
                    stagePadding: obj.stagePadding,                    
                    nav:true,
                    items: obj.items,
                    dots: false,
                    nav: false,
                });
                if(obj.initResponsive != undefined){
                    obj.initResponsive();
                }
            });
        }else if(width > 767){
            $(obj.elem).trigger('destroy.owl.carousel');
            $(obj.elem).removeClass('owl-carousel');

            if(obj.initFullWidth != undefined){
                obj.initFullWidth();
            }
        }
        $('.match-height').matchHeight();
        $('.match-height.related').matchHeight();
        $('.bg-mode .title').matchHeight();
    },

    // ------------------------------------------------------------------------------ //
    // Reset Grid
    // ------------------------------------------------------------------------------ //
    ResetGridColumns : function(){
        $('.row.grid-same').each(function() {
            // find all columns
            var $cs = $(this).children('[class*="col-"]');

            // reset the height
            $cs.css('height', 'auto');

            // set the heights per row
            var rowWidth = $(this).width();
            var $curCols = $();
            var curMax = 0;
            var curWidth = 0;
            $cs.each(function() {
                var w = $(this).width();
                var h = $(this).height();
                if(curWidth+w <= rowWidth) {
                    $curCols = $curCols.add(this);
                    curWidth+= w;
                    if(h>curMax) curMax = h;
                } else {
                    if($curCols.length>1) $curCols.css('height', curMax+'px');
                    $curCols = $(this);
                    curWidth = w;
                    curMax = h;
                }
            });
            if($curCols.length>1) $curCols.css('height', curMax+'px');
        });
    },

    // ------------------------------------------------------------------------------ //
    // Format Number
    // ------------------------------------------------------------------------------ //
    formatNumber : function(nStr) {
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
        }
        return x1 + x2;
    },

    // ------------------------------------------------------------------------------ //
    // Carousel Menus
    // ------------------------------------------------------------------------------ //
    owlMenu : function(){
        var windowW = $(window).width();
        if(windowW > 767){
            $('.owl-menu').owlCarousel({
                loop:false,
                dots: false,
                nav: true,
                navText: ['<span class="fa fa-angle-left"></span>','<span class="fa fa-angle-right"></span>'],
                autoWidth: false,
                responsive:{
                    768:{
                        items:4
                    },
                    1000:{
                        items:5
                    }
                }
            });
        }else{
            $('.owl-menu').owlCarousel({
                loop:false,
                dots: false,
                nav: true,
                navText: ['<span class="fa fa-angle-left"></span>','<span class="fa fa-angle-right"></span>'],
                autoWidth: true
            });
        }
    }
};


(function($) {
    "use strict";
    // ------------------------------------------------------------------------------ //
    // Initaial on Load
    // ------------------------------------------------------------------------------ //
    $(window).on('load', function(){
        core.init();
        $('.select').select2();

        // ------------------------------------------------------------------------------ //
        // Owl Carouse Default
        // ------------------------------------------------------------------------------ //
        $('.owl-home').owlCarousel({
            loop:true,
            animateOut: 'fadeOutUp',
            animateIn: 'fadeInDown',
            smartSpeed:450,
            margin:10,
            items:1,
            autoplay: 5000,
            dots: true,
            nav: false
        });

        // ------------------------------------------------------------------------------ //
        // Owl Images
        // ------------------------------------------------------------------------------ //
        var owl = $('.owl-image').owlCarousel({
            loop:true,
            margin:0,
            nav:true,
            navText: ['<span class="fa fa-angle-left"></span>','<span class="fa fa-angle-right"></span>'],
            dots: true,
            items:1
        });

        // ------------------------------------------------------------------------------ //
        // Owl Content
        // ------------------------------------------------------------------------------ //
        var owlContent = $('.carousel-content').owlCarousel({
            loop:false,
            margin:30,
            nav:true,
            navText: ['<span class="fa fa-angle-left"></span>','<span class="fa fa-angle-right"></span>'],
            dots: false,
            responsive:{
                320:{
                    items:2
                },
                768:{
                    items:3
                }
            }
        });
        $('.carousel-content .post-item').matchHeight();
        
        owlContent.on('resized.owl.carousel', function(){
            $('.carousel-content .post-item').css('height',0);
            $('.carousel-content .post-item').matchHeight();
        });
        
        // ------------------------------------------------------------------------------ //
        // Init Breadcrumb
        // ------------------------------------------------------------------------------ //
        var arrCharlink = [],
            countSpanActive;
        $('.breadcrumb li').each(function(){
            var charLink = $('a', this).html(),
                charSpan = $('span', this).html()
            if(charLink != undefined){
                arrCharlink.push(charLink.length);
            }

            if(charSpan != undefined){
                arrCharlink.push(charSpan.length);
                countSpanActive = charSpan.length;
            }
        });
        if($('.breadcrumb').length){
            var sumArrCharLink = _.sum(arrCharlink);
            if(sumArrCharLink > 90 && countSpanActive < 70){
                $('.breadcrumb').addClass('truncate');
            }else if(sumArrCharLink > 90 && countSpanActive > 70){
                $('.breadcrumb').addClass('truncate');
                // $('.wrap-menus').addClass('open');
            }
        }

        // ------------------------------------------------------------------------------ //
        // Parallax Background
        // ------------------------------------------------------------------------------ //
        if($('.parallax').length){
            $('.parallax').parallax();
        }
    });

    // ------------------------------------------------------------------------------ //
    // Resize window
    // ------------------------------------------------------------------------------ //
    $(window).on('resize', function(){
        core.layout();
        core.ResetGridColumns();
        setTimeout(function(){
            core.layout();
            core.ResetGridColumns();
        }, 500);

        // ------------------------------------------------------------------------------ //
        // Reset Select
        // ------------------------------------------------------------------------------ //
        $('.select').select2({
            minimumResultsForSearch: -1
        });

        // ------------------------------------------------------------------------------ //
        // Parallax Background
        // ------------------------------------------------------------------------------ //
        if($('.parallax').length){
            $('.parallax').parallax();
        }
    });
    
    var scrollState = 0;
    $(window).on("scroll", function() {
        var scrollTop = $(window).scrollTop();
        if(scrollTop > 100){
            if(scrollState < scrollTop){
                $('nav.navbar').addClass('scroll-top');
            }else if(scrollState > scrollTop){
                $('nav.navbar').removeClass('scroll-top');
            }
        }
        scrollState = scrollTop;
    });
})(jQuery);
