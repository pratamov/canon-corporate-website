# Readme

## Changelog

1. [UPDATE]
    build.gradle
    ```
    // For Windows User

    grails {
        pathingJar = true
    }
    ```
2. [UPDATE]
    grails-app/domain/canon/FaqTranslation.groovy
    
    ```
    Faq faq
    ```
3. [CREATE]
    grails-app/services/canon/AdminFaqService.groovy
4. [CREATE]
    grails-app/controllers/canon/admin/AdminFaqController.groovy
5. [UPDATE]
    grails-app/controllers/UriMappings.groovy
    ```
    "/faq/$action?/$id?(.$format)?"(controller: "adminFaq")
    ```
6. [CREATE]
    grails-app/init/canon/FaqInit.groovy
7. [UPDATE]
    grails-app/init/BootStrap.groovy
    ```
    FaqInit.init()
    ```
8. [CREATE]
    grails-app/views/adminFaq/countryList.gsp
    grails-app/views/adminFaq/categoryList.gsp
    grails-app/views/adminFaq/detail.gsp
9. [UPDATE]
    grails-app/views/layouts/admin.gsp
    ```
    ...
    <li><g:link controller="adminFaq">List</g:link></li>
    <li><a href="#">Settings</a></li>
    ...
    <div class="footer" style="z-index: -1">
    ...
    ```
	
## Unimplemented Features

1. Update question label
2. Update question
3. Remove question