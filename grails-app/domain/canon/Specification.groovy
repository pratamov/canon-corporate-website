package canon

import canon.enums.SpecificationType

class Specification {

    static hasMany = [children: Specification]

    Group group

    @Deprecated
    Specification parent

    String name
    String unit
    String description
    SpecificationType specType = SpecificationType.SINGLE
    Integer specOrder
    Boolean comparable = Boolean.TRUE

    @Deprecated
    Boolean important = Boolean.FALSE

    User createdBy
    User modifiedBy
    Date dateCreated
    Date lastUpdated

    static constraints = {
        parent nullable: true
        unit nullable: true
        description nullable: true

        createdBy nullable: true
        modifiedBy nullable: true
    }

    static mapping = {
        description sqlType: "text"
    }
}
