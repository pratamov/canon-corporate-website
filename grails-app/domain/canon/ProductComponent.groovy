package canon

import canon.enums.Status
import canon.usertype.ComponentUserType

class ProductComponent {

    Product product
    canon.model.Component component
    Status status = Status.PUBLISHED

    User createdBy
    User modifiedBy
    Date dateCreated
    Date lastUpdated

    static constraints = {
        createdBy nullable: true
        modifiedBy nullable: true
    }

    static mapping = {
        component type: ComponentUserType, {
            column name: "comp_type"
            column name: "comp_content", sqlType: "text"
            column name: "comp_order", sqlType: "int"
        }
    }
}
