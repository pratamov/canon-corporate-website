package canon

import canon.enums.Status

class PageDetailFeedback {

    static hasMany = [pageDetailFeedbackFiles: PageDetailFeedbackFile]

    PageDetail pagePublication
    User user

    String content
    Status status = Status.APPROVED

    User modifiedBy
    Date dateCreated
    Date lastUpdated

    static constraints = {
        modifiedBy nullable: true
    }

    static mapping = {
        content sqlType: "text"
    }
}
