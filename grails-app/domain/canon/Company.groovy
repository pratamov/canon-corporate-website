package canon

import canon.enums.CompanyType
import canon.enums.Status

class Company {

    static hasMany = [braches: Branch]

    Country country
    CompanyType companyType

    String name
    Integer companyOrder
    Status status = Status.PENDING

    User createdBy
    User modifiedBy
    Date dateCreated
    Date lastUpdated

    static constraints = {
        createdBy nullable: true
        modifiedBy nullable: true
    }
}
