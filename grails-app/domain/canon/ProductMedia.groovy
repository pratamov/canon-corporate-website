package canon

class ProductMedia {

    Product product
    Media media

    Integer mediaOrder
    Boolean isDefault = Boolean.FALSE

    User createdBy
    User modifiedBy
    Date dateCreated
    Date lastUpdated

    static constraints = {
        mediaOrder nullable: true

        createdBy nullable: true
        modifiedBy nullable: true
    }

    static mapping = {
    }
}
