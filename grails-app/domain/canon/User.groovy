package canon

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString

@EqualsAndHashCode(includes='username')
@ToString(includes='username', includeNames=true, includePackage=false)
class User implements Serializable {

	static hasMany = [userRoles: UserRole]

	static transients = ['springSecurityService', 'authorities', 'profile']

    private static final long serialVersionUID = 1
	transient springSecurityService

	String username
	String password
	String email
	Boolean enabled = Boolean.TRUE

	String avatar
	String confirmCode
	Boolean isConfirmed = Boolean.FALSE

	Boolean accountExpired = Boolean.FALSE
	Boolean accountLocked = Boolean.FALSE
	Boolean passwordExpired = Boolean.FALSE

	User(String username, String password) {
		this()
		this.username = username
		this.password = password
	}

	User(String username, String password, String email) {
		this()
		this.username = username
		this.password = password
		this.email = email
	}

	def beforeInsert() {
		encodePassword()
        generateConfirmCode()
	}

	def beforeUpdate() {
		if (isDirty('password')) {
			encodePassword()
		}
	}

	static constraints = {
		username unique: true
		email nullable: true, unique: true, email: true
		avatar blank: true, nullable: true
        confirmCode nullable: true
	}

	static mapping = {
		password column: '`password`'
	}

	Set<Role> getAuthorities() {
		UserRole.findAllByUser(this)*.role
	}

    Profile getProfile() {
        Profile.findByUser(this)
    }

	protected void encodePassword() {
		password = springSecurityService?.passwordEncoder ? springSecurityService.encodePassword(password) : password
	}

	protected void generateConfirmCode() {
		confirmCode = UUID.randomUUID().toString().replace("-", "")
	}
}
