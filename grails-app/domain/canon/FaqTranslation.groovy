package canon

class FaqTranslation {

    Faq faq
    Language language

    String name // As Category Translation

    User createdBy
    User modifiedBy
    Date dateCreated
    Date lastUpdated

    static constraints = {
        createdBy nullable: true
        modifiedBy nullable: true
    }
}
