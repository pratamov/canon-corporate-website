package canon

import canon.enums.Status

class PageComment {

    Page page
    User user
    Language language

    String content
    Status status = Status.PENDING
    User approvedBy
    Date approvedDate

    User modifiedBy
    Date dateCreated
    Date lastUpdated

    static constraints = {
        approvedDate nullable: true
        approvedBy nullable: true

        modifiedBy nullable: true
    }

    static mapping = {
        content sqlType: "text"
    }
}
