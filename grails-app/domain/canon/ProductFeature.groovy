package canon

import canon.enums.Status

class ProductFeature {

    Product product
    Feature feature

    Status status = Status.INACTIVE
    Integer featureOrder

    User createdBy
    User modifiedBy
    Date dateCreated
    Date lastUpdated

    static constraints = {
        featureOrder nullable: true

        createdBy nullable: true
        modifiedBy nullable: true
    }
}
