package canon

import canon.enums.MediaSource
import canon.enums.MediaType
import canon.enums.Status

class Media {

    static hasMany = [mediaComponents: PageComponentMedia]

    Lookup folder

    String alt
    String copyright
    String title
    String description
    MediaSource source
    Status status

    String fileName
    Long fileSize
    Integer height
    MediaType mediaType
    String url
    String urlPoster
    String urlThumbnail
    Integer width

    User createdBy
    User modifiedBy
    Date dateCreated
    Date lastUpdated

    static constraints = {
        folder nullable: true

        alt blank: true, nullable: true
        copyright nullable: true, blank: true
        title nullable: true, blank: true
        description blank: true, nullable: true
        source nullable: true
        status nullable: true

        fileSize nullable: true
        height nullable: true
        url maxSize: 2048
        urlPoster blank: true, nullable: true, maxSize: 2048
        urlThumbnail blank: true, nullable: true, maxSize: 2048
        width nullable: true

        createdBy nullable: true
        modifiedBy nullable: true
    }

    static mapping = {
        description sqlType: "text"
        fileSize defaultValue: 0
        width defaultValue: 0
        height defaultValue: 0
    }

    def static listMedia(def params) {

        def sortOrder = "m.dateCreated";

        if (params.sort == "file") {
            sortOrder = "m.fileName"
        } else if (params.sort == "author") {
            sortOrder = "coalesce( m.createdBy.profile.firstName, m.createdBy.profile.firstName || ' ' || m.createdBy.profile.lastName, m.createdBy.username )";
        } else if (params.sort == "uploadedTo") {
            sortOrder = "page.headline"
        } else if (params.sort == "date") {
            sortOrder = "m.dateCreated"
        } else if (params.sort == "fileSize") {
            sortOrder = "m.fileSize"
        } else if (params.sort == "dimension") {
            sortOrder = " (m.width * m.height) "
        }

        sortOrder += (params.order != "desc") ? " asc" : " desc";

        def criteria = ' m.status = null '
        def words = params.search.split()
        def count = Math.min(words.length, 5)

        def qparams = [:]
        if (count > 0) {
            criteria += ' and ('
            for (i in 0..count-1) {
                if (i != 0) {
                    criteria += ' and '
                }
                criteria += ' (lower(m.title) like :title' + i + ' or lower(m.description) like :description' + i + ' or lower(m.fileName) like :fileName' + i + ''
                criteria += ' or lower(profile.firstName) like :firstName' + i + ' '
                criteria += ' or lower(profile.lastName) like :lastName' + i + ' '
                criteria += ' or m.id IN (select mComp.media.id from PageComponentMedia mComp join mComp.component comp join comp.page p WHERE lower(p.headline) like :pageHeadline'+i + ' ) '
                criteria += ' ) '
                qparams['title' + i] = '%' + words[i].toLowerCase() + '%'
                qparams['description' + i] = '%' + words[i].toLowerCase() + '%'
                qparams['fileName' + i] = '%' + words[i].toLowerCase() + '%'
                qparams['firstName' + i] = '%' + words[i].toLowerCase() + '%'
                qparams['lastName' + i] = '%' + words[i].toLowerCase() + '%'
                qparams['pageHeadline' + i] = '%' + words[i].toLowerCase() + '%'
            }
            criteria += ' ) '
        }

        if (params.year) {
            criteria += ' AND ' + ' year(m.dateCreated) = ' + params.year
            //qparams['year'] = params.year
        }

        def imageCriteria = criteria + " AND m.mediaType = 'image'";

        def videoCriteria = criteria + " AND m.mediaType = 'video'";

        def allTypeCriteria = criteria;

        if (params.type == "image") {
            criteria += " AND m.mediaType = 'image'";
        } else if (params.type == "video") {
            criteria += " AND m.mediaType = 'video'";
        }

        String selectHql = "select distinct m from Media as m " +
                "left outer join m.createdBy author " +
                "left outer join author.profile profile " +
                "left outer join m.mediaComponents mediaComponents " +
                "left outer join mediaComponents.component component " +
                "left outer join component.page page";

        String countHql = "select count(1) from Media as mi where mi.id IN (" +
                "select m.id from Media as m " +
                "left outer join m.createdBy author " +
                "left outer join author.profile profile " +
                "left outer join m.mediaComponents mediaComponents " +
                "left outer join mediaComponents.component component " +
                "left outer join component.page page "

        def results = executeQuery(selectHql + ' where ' + criteria + ' order by ' + sortOrder, qparams, [ max: params.max, offset: params.offset ]);

        def mediaCount = 0;
        def mediaCountResult = executeQuery(countHql + ' where ' + allTypeCriteria + " ) ", qparams)
        if (mediaCountResult && mediaCountResult.size() > 0) {
            mediaCount = mediaCountResult[0]
        }

        def imageCount = 0;
        def imageCountResult = executeQuery(countHql + ' where ' + imageCriteria + " ) ", qparams)
        if (imageCountResult && imageCountResult.size() > 0) {
            imageCount = imageCountResult[0]
        }

        def videoCount = 0;
        def videoCountResult = executeQuery(countHql + ' where ' + videoCriteria + " ) ", qparams)
        if (videoCountResult && videoCountResult.size() > 0) {
            videoCount = videoCountResult[0]
        }

        def listingResults = [:]
        listingResults.results = results
        listingResults.mediaCount = mediaCount
        listingResults.imageCount = imageCount
        listingResults.videoCount = videoCount

        return listingResults;
    }
}
