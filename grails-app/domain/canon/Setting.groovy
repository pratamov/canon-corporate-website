package canon

import canon.enums.Status

class Setting {

    Site site
    Language language

    String settingCode
    String settingName
    String settingType
    String settingValue
    Status status = Status.ACTIVE

    User createdBy
    User modifiedBy
    Date dateCreated
    Date lastUpdated

    static constraints = {
        site nullable: true
        language nullable: true
        settingCode(unique: ['site', 'language'])
        settingName blank: true, nullable: true
        settingValue blank: true, nullable: true

        createdBy nullable: true
        modifiedBy nullable: true
    }

    static mapping = {
        settingValue sqlType: "text"
    }
}
