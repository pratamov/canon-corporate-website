package canon

class PageSubject {

    static hasMany = [subjectTranslations: SubjectTranslation]

    Page page
    Subject subject

    User createdBy
    User modifiedBy
    Date dateCreated
    Date lastUpdated

    static constraints = {
        subject unique: "page"

        createdBy nullable: true
        modifiedBy nullable: true
    }
}
