package canon

import canon.enums.Status

class Branch {

    static hasMany = [branchProducts: BranchProduct, branchCategories: BranchCategory]

    Company company
    District district
    Media logo

    Integer branchOrder
    String name
    String address
    String addressExtended
    String email
    String fax
    BigDecimal latitude
    BigDecimal longitude
    String parts
    String phone
    String remarks
    String sales
    String service
    Status status = Status.PENDING
    String website

    User createdBy
    User modifiedBy
    Date dateCreated
    Date lastUpdated

    static constraints = {
        addressExtended nullable: true, blank: true
        email nullable: true, blank: true
        fax nullable: true, blank: true
        latitude nullable: true
        longitude nullable: true
        parts nullable: true, blank: true
        phone nullable: true, blank: true
        remarks nullable: true, blank: true
        sales nullable: true, blank: true
        service nullable: true, blank: true
        website nullable: true, blank: true

        logo nullable: true

        createdBy nullable: true
        modifiedBy nullable: true
    }
}
