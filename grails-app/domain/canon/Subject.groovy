package canon

import canon.enums.Status

class Subject {

    String name
    String slug
    Status status = Status.INACTIVE

    User createdBy
    User modifiedBy
    Date dateCreated
    Date lastUpdated

    static constraints = {
        name unique: true
        slug unique: true

        createdBy nullable: true
        modifiedBy nullable: true
    }
}
