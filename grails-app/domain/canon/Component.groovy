package canon

import canon.enums.Status

class Component {

    String name
    String description
    Status status = Status.INACTIVE
    Integer displayOrder

    User createdBy
    User modifiedBy
    Date dateCreated
    Date lastUpdated

    static constraints = {
        displayOrder nullable: true

        createdBy nullable: true
        modifiedBy nullable: true
    }

    static mapping = {
        description sqlType: "text"
    }
}
