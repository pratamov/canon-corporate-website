package canon

class ProductPrice {

    Product product
    Site site

    String name
    String currencyCode
    Integer quantity
    BigDecimal price

    User createdBy
    User modifiedBy
    Date dateCreated
    Date lastUpdated

    static constraints = {
        name nullable: true
        quantity nullable: true

        createdBy nullable: true
        modifiedBy nullable: true
    }
}
