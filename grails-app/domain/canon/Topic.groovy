package canon

class Topic {

    static hasMany = [groups: Group]

    String name
    String description

    User createdBy
    User modifiedBy
    Date dateCreated
    Date lastUpdated

    static constraints = {
        createdBy nullable: true
        modifiedBy nullable: true
    }

    static mapping = {
        description sqlType: "text"
    }
}
