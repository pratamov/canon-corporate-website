package canon

import canon.enums.Status

class Site {

    static hasMany = [siteLanguages: SiteLanguage]

    static transients = ['defaultLangCode', 'defaultLanguage', 'activated']

    Country country

    String name
    String code
    String description
    Boolean isCountryLevel = Boolean.FALSE
    Boolean isDefault = Boolean.FALSE
    Status status = Status.INACTIVE
    String timeZone

    User createdBy
    User modifiedBy
    Date dateCreated
    Date lastUpdated

    // Transient fields
    String defaultLangCode

    static constraints = {
        code unique: true
        country nullable: true
        description blank: true, nullable: true
        timeZone nullable: true, blank: true
        status blank: true, nullable: true

        createdBy nullable: true
        modifiedBy nullable: true
    }

    static mapping = {
        description sqlType: "text"
    }

    public Language getDefaultLanguage() {
        SiteLanguage siteLanguage = SiteLanguage.createCriteria().get {
            and {
                eq("site", this)
                eq("isDefault", true)
            }
            maxResults(1)
        }

        // Get the first if no default
        if (!siteLanguage) {
            siteLanguage = SiteLanguage.createCriteria().get {
                createAlias("language", "l")
                eq("site", this)
                order("l.id", "asc")
                maxResults(1)
            }
        }

        return siteLanguage?.language
    }

    boolean isActivated() {
        return this.status == Status.ACTIVE
    }
}
