package canon

import canon.enums.Status

class ProductSellingPoint {

    Product product
    String content
    Integer pointOrder
    Status status = Status.PUBLISHED

    User createdBy
    User modifiedBy
    Date dateCreated
    Date lastUpdated

    static constraints = {
        createdBy nullable: true
        modifiedBy nullable: true
    }

    static mapping = {
        content sqlType: "text"
    }
}
