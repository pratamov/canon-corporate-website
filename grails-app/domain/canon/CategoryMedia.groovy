package canon

import canon.enums.MediaSource

class CategoryMedia {

    Category category
    Media media
    MediaSource mediaSource

    String mediaUrl
    Integer mediaOrder

    User createdBy
    User modifiedBy
    Date dateCreated
    Date lastUpdated

    static constraints = {
        media nullable: true
        mediaOrder nullable: true
        mediaUrl maxSize: 2048

        createdBy nullable: true
        modifiedBy nullable: true
    }

    static mapping = {
    }
}
