package canon

import canon.enums.Status

class District {

    Site site

    String name
    Status status = Status.PENDING

    User createdBy
    User modifiedBy
    Date dateCreated
    Date lastUpdated

    static constraints = {
        createdBy nullable: true
        modifiedBy nullable: true
    }
}
