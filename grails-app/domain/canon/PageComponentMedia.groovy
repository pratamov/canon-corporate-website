package canon

@Deprecated
class PageComponentMedia {

    PageComponent pageComponent
    Media media

    User createdBy
    User modifiedBy
    Date dateCreated
    Date lastUpdated

    static constraints = {
        createdBy nullable: true
        modifiedBy nullable: true
    }
}
