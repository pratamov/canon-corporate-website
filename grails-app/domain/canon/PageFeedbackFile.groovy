package canon

class PageFeedbackFile {

    PageFeedback pageFeedback
    Media file

    User createdBy
    User modifiedBy
    Date dateCreated
    Date lastUpdated

    static constraints = {
        createdBy nullable: true
        modifiedBy nullable: true
    }
}
