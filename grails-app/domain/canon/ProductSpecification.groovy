package canon

class ProductSpecification {

    Product product
    Specification specification

    String specValue
    String specUnitValue
    Integer specOrder

    User createdBy
    User modifiedBy
    Date dateCreated
    Date lastUpdated

    static constraints = {
        specUnitValue nullable: true
        specOrder nullable: true

        createdBy nullable: true
        modifiedBy nullable: true
    }

    static mapping = {
        specValue sqlType: "text"
    }
}
