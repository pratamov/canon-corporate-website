package canon

class PageDetailWorkFlow {

    PageDetail pageDetail
    WorkFlow workFlow

    User assignedTo
    Boolean isCurrent = Boolean.FALSE

    User createdBy
    User modifiedBy
    Date dateCreated
    Date lastUpdated

    static constraints = {
        createdBy nullable: true
        modifiedBy nullable: true
    }
}
