package canon

import canon.enums.Status

@Deprecated
class Tag {

    Language language
    String name
    Status status = Status.INACTIVE

    User createdBy
    User modifiedBy
    Date dateCreated
    Date lastUpdated

    static constraints = {
        name unique: true

        createdBy nullable: true
        modifiedBy nullable: true
    }

    static mapping = {
    }
}
