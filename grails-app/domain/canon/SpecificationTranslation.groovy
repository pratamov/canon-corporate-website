package canon

class SpecificationTranslation {

    Specification specification
    Language language

    String name
    String unit
    String description

    User createdBy
    User modifiedBy
    Date dateCreated
    Date lastUpdated

    static constraints = {
        unit nullable: true
        description nullable: true

        createdBy nullable: true
        modifiedBy nullable: true
    }

    static mapping = {
        description sqlType: "text"
    }
}
