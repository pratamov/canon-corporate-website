package canon

import canon.enums.Status

class ProductDetailPublication {

    ProductDetail productDetail
    Language language

    Status status = Status.DRAFT
    Date publishedDate
    Date archivedDate

    User createdBy
    User modifiedBy
    Date dateCreated
    Date lastUpdated

    static constraints = {
        publishedDate nullable: true
        archivedDate nullable: true

        createdBy nullable: true
        modifiedBy nullable: true
    }
}
