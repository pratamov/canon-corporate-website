package canon

class PageDetail {

    static hasMany = [
            pageDetailFeedbackList: PageDetailFeedback,
            pageDetailWorkFlows   : PageDetailWorkFlow
    ]

    Page page
    Flow flow
    Site site

    User createdBy
    User modifiedBy
    Date dateCreated
    Date lastUpdated

    static constraints = {
        flow nullable: true

        createdBy nullable: true
        modifiedBy nullable: true
    }
}
