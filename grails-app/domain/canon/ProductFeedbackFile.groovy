package canon

class ProductFeedbackFile {

    ProductFeedback productFeedback
    Media file

    User createdBy
    User modifiedBy
    Date dateCreated
    Date lastUpdated

    static constraints = {
        createdBy nullable: true
        modifiedBy nullable: true
    }
}
