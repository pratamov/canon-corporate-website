package canon

import canon.enums.Status

class BranchCategory {

    Category category
    Branch branch
    Status status = Status.ACTIVE

    User createdBy
    User modifiedBy
    Date dateCreated
    Date lastUpdated

    static constraints = {
        createdBy nullable: true
        modifiedBy nullable: true
    }
}
