package canon

import canon.enums.DocumentType

class ProductDocument {

    Product product
    Media document

    Integer documentOrder
    DocumentType documentType

    User createdBy
    User modifiedBy
    Date dateCreated
    Date lastUpdated

    static constraints = {
        modifiedBy nullable: true
        createdBy nullable: true
    }
}
