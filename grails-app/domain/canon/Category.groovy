package canon

import canon.enums.CategoryType
import canon.enums.MediaSource
import canon.enums.Segment
import canon.enums.Status
import canon.utils.LocaleUtils
import org.springframework.context.i18n.LocaleContextHolder

class Category {

    static hasMany = [
            childrens: Category,
            categoryTranslations: CategoryTranslation,
            categoryMedias: CategoryMedia,
            products: Product,
            pages: Page
    ]

    static transients = ['translation', 'hierarchicalCategory', 'defaultMedia']

    Language language
    Category parent
    Site site

    Integer categoryOrder
    String description
    String name
    String slug
    Status status = Status.INACTIVE
    String subText

    CategoryType categoryType
    Segment segment

    String backgroundUrl
    MediaSource backgroundSource
    Media backgroundMedia

    User createdBy
    User modifiedBy
    Date dateCreated
    Date lastUpdated

    static constraints = {
        parent nullable: true

        description nullable: true
        slug unique: "segment"
        subText nullable: true

        segment nullable: true

        backgroundUrl nullable: true, blank: true
        backgroundSource nullable: true
        backgroundMedia nullable: true

        createdBy nullable: true
        modifiedBy nullable: true
    }

    static mapping = {
        subText sqlType: "text"
        description sqlType: "text"

        childrens sort: 'categoryOrder', order: 'asc'
    }

    static namedQueries = {
        withSegment { segment ->
            eq("segment", segment)
        }
        mainCategories {
            isNull "parent"
        }
    }

    CategoryTranslation getTranslation() {
        Locale locale = LocaleContextHolder.getLocale()
        String lCode = LocaleUtils.getLanguage(locale)
        CategoryTranslation t = CategoryTranslation.createCriteria().get {
            createAlias("language", "l")
            and {
                eq("category", this)
                eq("l.id", lCode)
            }
            maxResults(1)
        }
        return t
    }

    List<Category> getHierarchicalCategory(){

        Category categoryTemp = this

        List<Category> categoryHierarchyList = new ArrayList<>();
        categoryHierarchyList.add(this)

        while(categoryTemp.parent){
            categoryTemp = categoryTemp.parent
            categoryHierarchyList.add(0, categoryTemp)
        }

        return categoryHierarchyList
    }

    CategoryMedia getDefaultMedia() {
        return CategoryMedia.findByCategory(this)
    }
}
