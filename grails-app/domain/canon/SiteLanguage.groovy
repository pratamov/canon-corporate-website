package canon

class SiteLanguage {

    Site site
    Language language
    Boolean isDefault = Boolean.FALSE

    User createdBy
    User modifiedBy
    Date dateCreated
    Date lastUpdated

    static constraints = {
        site unique: "language"

        createdBy nullable: true
        modifiedBy nullable: true
    }
}
