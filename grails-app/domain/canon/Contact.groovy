package canon


class Contact {

    String name
    String email
    String subject
    String message

    Date dateCreated
    Date lastUpdated

    static constraints = {
        email email: true
    }

    static mapping = {
        message sqlType: "text"
    }
}
