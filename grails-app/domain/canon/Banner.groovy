package canon

import canon.enums.BannerLocation
import canon.enums.Segment
import canon.enums.Status

class Banner {

    static hasMany = [bannerItems: BannerItem]

    Site site
    Segment segment
    Language language
    String title

    BannerLocation location
    String conditionKey
    String conditionValue

    String status = Status.DRAFT
    Date startDate
    Date endDate

    User createdBy
    User modifiedBy
    Date dateCreated
    Date lastUpdated

    static constraints = {
        location nullable: true
        conditionKey nullable: true, blank: true
        conditionValue nullable: true, blank: true

        modifiedBy nullable: true
        createdBy nullable: true
    }
}
