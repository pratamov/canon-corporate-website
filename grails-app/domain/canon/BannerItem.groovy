package canon

import canon.enums.BannerType

class BannerItem {

    Banner banner
    BannerType bannerType
    Product product

    canon.model.Media background
    String buttonLabel
    String buttonUrl
    String description
    canon.model.Media image
    Integer itemOrder
    String title

    User createdBy
    User modifiedBy
    Date dateCreated
    Date lastUpdated

    static embedded = ['background', 'image']

    static constraints = {
        product nullable: true

        background nullable: true
        buttonLabel nullable: true, blank: true
        buttonUrl nullable: true, blank: true
        description nullable: true, blank: true
        image nullable: true
        title nullable: true, blank: true

        modifiedBy nullable: true
        createdBy nullable: true
    }

    static mapping = {
        description sqlType: "text"
    }
}
