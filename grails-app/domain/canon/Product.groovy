package canon

import canon.enums.ProductLevel
import canon.enums.Status

class Product {

    static hasMany = [
            productAwards        : ProductAward,
            productCategories    : ProductCategory,
            productComponents    : ProductComponent,
            productDocuments     : ProductDocument,
            productFeatures      : ProductFeature,
            productFeedbackList  : ProductFeedback,
            productMedias        : ProductMedia,
            productPrices        : ProductPrice,
            productPromotions    : ProductPromotion,
            productRelations     : ProductRelation,
            productDetails       : ProductDetail,
            productSellingPoints : ProductSellingPoint,
            productSpecifications: ProductSpecification,
            productViews         : ProductView
    ]

    static def ES_SORTABLE_FIELDS = [
            az: [field: "names.name.keyword", order: "asc"],
            za: [field: "names.name.keyword", order: "desc"],
            newest: [field: "published_date", order: "desc"],
            oldest: [field: "published_date", order: "asc"]
            //mostViewed: [field: "view_count", order: "desc"],
            //highestPrice: [field: "price_min", order: "desc"],
            //lowestPrice: [field: "price_min", order: "asc"]
    ]

    static def PRICE_RANGE = [
            0: [low: 0, high: 1000],
            1: [low: 1001, high: 2000],
            2: [low: 2001, high: 3000]/*,
            3: [low: 3001, high: 4000],
            4: [low: 4001, high: 5000]*/
    ]

    static transients = ['ES_SORTABLE_FIELDS', 'PRICE_RANGE', 'defaultMedia', 'viewCount']

    Language language
    Product parent
    Site site
    Topic topic

    Date availabilityDate
    canon.model.Banner banner
    String description
    Date discontinuedDate
    ProductLevel productLevel
    String name
    String slug
    String specNote
    String subText
    String supportLink

    @Deprecated
    String currencyCode
    @Deprecated
    BigDecimal price

    Boolean isFeatured = Boolean.FALSE
    Date featuredEndDate
    Date featuredStartDate

    // Versioning
    Product master
    Date publishedDate
    Integer publishedVersion
    Status status = Status.DRAFT

    // Meta data
    String metaDescription
    Media metaMedia
    String metaTitle

    // Draft - Archived
    Date archivedDate
    String legacySlug
    Status legacyStatus = Status.DRAFT
    Product origin

    User createdBy
    User modifiedBy
    Date dateCreated
    Date lastUpdated

    def beforeValidate() {
    }

    def beforeInsert() {
        // Frozen fields sync from master
        if (parent) {
            slug = parent.slug
            availabilityDate = parent.availabilityDate
            discontinuedDate = parent.discontinuedDate
        }
    }

    def beforeUpdate() {
        if (![Status.ARCHIVED, Status.DELETED].contains(getPersistentValue("status"))) {	
            legacyStatus = getPersistentValue("status")
        }
        legacySlug = slug
    }

    static embedded = ['banner']

    static constraints = {
        banner nullable: true
        parent nullable: true
        topic nullable: true

        subText nullable: true
        description nullable: true
        availabilityDate nullable: true
        discontinuedDate nullable: true
        currencyCode nullable: true
        price nullable: true
        slug maxSize: 255

        productLevel nullable: true
        specNote nullable: true, blank: true
        supportLink nullable: true, blank: true

        featuredEndDate nullable: true
        featuredStartDate nullable: true

        master nullable: true
        publishedDate nullable: true
        publishedVersion nullable: true

        metaDescription nullable: true, blank: true
        metaMedia nullable: true
        metaTitle nullable: true, blank: true

        archivedDate nullable: true
        legacySlug nullable: true, blank: true
        origin nullable: true

        createdBy nullable: true
        modifiedBy nullable: true
    }

    static mapping = {
        subText sqlType: "text"
        description sqlType: "text"
        specNote sqlType: "text"

        metaDescription sqlType: "text"
    }

    ProductMedia getDefaultMedia() {
        ProductMedia pm = ProductMedia.findByProductAndIsDefault(this, Boolean.TRUE)
        if (pm) return pm

        return ProductMedia.findByProduct(this)
    }

    Integer getViewCount() {
        ProductView.countByProduct(this)
    }

    ProductPromotion getActivePromotion(){
        ProductPromotion prodPromotion = ProductPromotion.findByProductAndStatus(this, Status.PUBLISHED)

        return prodPromotion
    }

    List<Award> getActiveAwards() {
        return ProductAward.createCriteria().list {
            createAlias("award", "a")
            projections{
                property("award")
            }
            eq("product", this)
            eq("a.status", Status.ACTIVE)
            order("a.year", "desc")
        }
    }
}
