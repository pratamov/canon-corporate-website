package canon

class ProductAward {

    Product product
    Award award
    Site site

    User createdBy
    User modifiedBy
    Date dateCreated
    Date lastUpdated

    static constraints = {
        product unique: "award"

        createdBy nullable: true
        modifiedBy nullable: true
    }
}
