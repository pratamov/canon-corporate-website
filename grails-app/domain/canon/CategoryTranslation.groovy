package canon

class CategoryTranslation {

    Category category
    Language language
    String name

    User createdBy
    User modifiedBy
    Date dateCreated
    Date lastUpdated

    static constraints = {
        category unique: "language"

        modifiedBy nullable: true
        createdBy nullable: true
    }
}
