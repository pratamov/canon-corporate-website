package canon

class FaqItemTranslation {

    FaqItem faqItem
    Language language

    String label
    String content

    User createdBy
    User modifiedBy
    Date dateCreated
    Date lastUpdated

    static constraints = {
        createdBy nullable: true
        modifiedBy nullable: true
    }
}
