package canon

import canon.enums.Status

class Award {

    String name
    Integer year
    Status status = Status.INACTIVE
    String imageUrl

    User createdBy
    User modifiedBy
    Date dateCreated
    Date lastUpdated

    static constraints = {
        year nullable: true
        createdBy nullable: true
        modifiedBy nullable: true
    }
}
