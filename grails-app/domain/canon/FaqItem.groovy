package canon

import canon.enums.Status

class FaqItem {

    Faq faq

    String label
    String content
    Date publishedDate
    Status status = Status.DRAFT

    // Draft - Archived
    Date archivedDate
    String legacySlug
    Status legacyStatus = Status.DRAFT

    User createdBy
    User modifiedBy
    Date dateCreated
    Date lastUpdated

    static constraints = {
        publishedDate nullable: true

        archivedDate nullable: true
        legacySlug nullable: true, blank: true

        createdBy nullable: true
        modifiedBy nullable: true
    }
}
