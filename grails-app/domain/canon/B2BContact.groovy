package canon

class B2BContact {

    String fullUrl;
    String environment;
    String inputParams;
    String returnValue;
    Integer flag;
    String ipAddress;
    Date dateCreated;
    Date lastUpdated

    static constraints = {
        returnValue nullable: true

        dateCreated nullable: true
        lastUpdated nullable: true
    }

    static mapping = {
        inputParams sqlType: "text"
    }
}
