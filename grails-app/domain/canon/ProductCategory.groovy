package canon

class ProductCategory {

    Product product
    Category category

    User createdBy
    User modifiedBy
    Date dateCreated
    Date lastUpdated

    static constraints = {
        product unique: "category"

        createdBy nullable: true
        modifiedBy nullable: true
    }

    static namedQueries = {
        withProductId { productId ->
            eq("product.id", productId)
        }
    }
}
