package canon

import canon.enums.Status

class ProductFeedback {

    static hasMany = [productFeedbackFiles: ProductFeedbackFile]

    Product product
    User user

    String content
    Status status = Status.APPROVED

    User modifiedBy
    Date dateCreated
    Date lastUpdated

    static constraints = {
        modifiedBy nullable: true
    }

    static mapping = {
        content sqlType: "text"
    }
}
