package canon

import canon.enums.Status

class Feature {

    Language language

    String name
    String description

    Media iconMedia
    String iconTitle
    String iconSubtitle
    Media media

    Status status = Status.DRAFT

    User createdBy
    User modifiedBy
    Date dateCreated
    Date lastUpdated

    static constraints = {
        iconMedia nullable: true
        media nullable: true

        createdBy nullable: true
        modifiedBy nullable: true
    }

    static mapping = {
        description sqlType: "text"
    }
}
