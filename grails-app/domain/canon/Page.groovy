package canon

import canon.enums.*
import org.apache.commons.lang.StringUtils

class Page {

    static hasMany = [
            pageCategories: PageCategory,
            pageComponents: PageComponent,
            pageComments  : PageComment,
            pageDetails   : PageDetail,
            productFedbackList: ProductFeedback,
            pageProducts  : PageProduct,
            pageSubjects  : PageSubject,
            pageTags      : PageTag
    ]

    static def ES_SORTABLE_FIELDS = [
            az: [field: "titles.title.keyword", order: "asc"],
            za: [field: "titles.title.keyword", order: "desc"]
            //mostViewed: [field: "view_count", order: "desc"]
    ]

    static transients = ['ES_SORTABLE_FIELDS', 'firstHeroComponent', 'firstTextComponent', 'mainCategory']

    Language language
    Page parent
    Site site

    canon.model.Banner banner
    String content // Used only if: contentType = ContentTemplate.GENERIC
    ContentType contentType
    ContentTemplate contentTemplate
    ContentPrefix contentPrefix = ContentPrefix.NONE
    ContentSetting contentSetting = ContentSetting.NONE
    String deck
    String headline
    Segment segment // Used only if: contentType = ContentType.PAGE
    String slug

    @Deprecated
    Boolean isHomepage = Boolean.FALSE

    Boolean isFeatured = Boolean.FALSE
    Date featuredEndDate
    Date featuredStartDate

    // Versioning
    Page master
    Date publishedDate
    Integer publishedVersion
    Status status = Status.DRAFT

    // Sponsored content:
    // contentSetting = ContentSetting.SPONSORED
    String partnerName
    String partnerLogo
    String facebookLink
    String twitterLink
    String instagramLink

    // Meta data
    String metaDescription
    String metaTitle
    Media metaMedia

    // Draft - Archived
    Date archivedDate
    String legacySlug
    Status legacyStatus = Status.DRAFT
    Page origin

    User createdBy
    User modifiedBy
    Date dateCreated
    Date lastUpdated

    // Transient fields
    PageComponent firstHeroComponent
    PageComponent firstTextComponent

    def beforeValidate() {
    }

    def beforeInsert() {
        // Frozen fields sync from master
        if (parent) {
            slug = parent.slug
        }
    }

    def beforeUpdate() {
        if (status != Status.ARCHIVED && status != Status.DELETED) {
            legacyStatus = status
        }
        legacySlug = slug
    }

    static embedded = ['banner']

    static constraints = {
        parent nullable: true

        banner nullable: true
        content nullable: true, blank: true
        deck nullable: true, blank: true
        headline nullable: false, blank: true
        segment nullable: true
        slug maxSize: 255

        featuredEndDate nullable: true
        featuredStartDate nullable: true

        master nullable: true
        publishedDate nullable: true
        publishedVersion nullable: true

        partnerName blank: true, nullable: true
        partnerLogo blank: true, nullable: true
        facebookLink blank: true, nullable: true
        twitterLink blank: true, nullable: true
        instagramLink blank: true, nullable: true

        metaTitle nullable: true, blank: true
        metaMedia nullable: true
        metaDescription nullable: true, blank: true

        archivedDate nullable: true
        legacySlug nullable: true, blank: true
        origin nullable: true

        createdBy nullable: true
        modifiedBy nullable: true
    }

    static mapping = {
        content sqlType: "text"
        deck sqlType: "text"
        metaDescription sqlType: "text"
    }

    PageComponent getFirstHeroComponent() {
        if (firstHeroComponent!=null) {
            return firstHeroComponent
        }

        firstHeroComponent = PageComponent.createCriteria().get {
            and {
                eq("page", this)
                eq("component.compType", ComponentType.HERO.name())
                eq("status", Status.PUBLISHED)
            }
            order("component.compOrder", "asc")
            maxResults(1)
        }
        return firstHeroComponent
    }

    PageComponent getFirstTextComponent() {
        if (firstTextComponent!=null) {
            return firstTextComponent
        }

        firstTextComponent = PageComponent.createCriteria().get {
            and {
                eq("page", this)
                eq("component.compType", ComponentType.TEXT.name())
                eq("status", Status.PUBLISHED)
            }
            order("component.compOrder", "asc")
            maxResults(1)
        }
        return firstTextComponent
    }

    Category getMainCategory(String segmentSlug) {
        if (StringUtils.isBlank(segmentSlug)) return null
        try {
            Segment sg = Segment.getBySlug(segmentSlug)
            PageCategory pc = PageCategory.inMainCategories(id, sg).get()
            return pc?.category
        } catch (Exception e) {
            log.error(e?.message, e)
        }
        return null
    }
}
