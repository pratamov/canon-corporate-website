package canon

class PageProduct {

    Page page
    Product product

    User createdBy
    User modifiedBy
    Date dateCreated
    Date lastUpdated

    static constraints = {
        page unique: "product"

        createdBy nullable: true
        modifiedBy nullable: true
    }
}
