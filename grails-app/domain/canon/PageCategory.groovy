package canon

class PageCategory {

    Page page
    Category category

    User createdBy
    User modifiedBy
    Date dateCreated
    Date lastUpdated

    static constraints = {
        page unique: "category"

        createdBy nullable: true
        modifiedBy nullable: true
    }

    static namedQueries = {
        withPageId { pageId ->
            eq("page.id", pageId)
        }
        inMainCategories { pageId, segment ->
            withPageId(pageId)
            category {
                withSegment(segment)
                mainCategories()
            }
        }
    }
}
