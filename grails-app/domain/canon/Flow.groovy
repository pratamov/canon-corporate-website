package canon

import canon.enums.FlowType
import canon.enums.Status

class Flow {

    static hasMany = [workFlows: WorkFlow]

    String name
    FlowType flowType
    Status status = Status.ACTIVE

    User createdBy
    User modifiedBy
    Date dateCreated
    Date lastUpdated

    static constraints = {
        createdBy nullable: true
        modifiedBy nullable: true
    }
}
