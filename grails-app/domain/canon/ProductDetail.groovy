package canon

class ProductDetail {

    static hasMany = [
            productDetailFeedbackList: ProductDetailFeedback,
            productDetailPublications: ProductDetailPublication,
            productDetailWorkFlows   : ProductDetailWorkFlow
    ]

    Product product
    Flow flow
    Site site

    String currencyCode
    BigDecimal price

    User createdBy
    User modifiedBy
    Date dateCreated
    Date lastUpdated

    static constraints = {
        flow nullable: true

        currencyCode nullable: true
        price nullable: true

        createdBy nullable: true
        modifiedBy nullable: true
    }

    static namedQueries = {
        withProductId { productId ->
            eq("product.id", productId)
        }
    }
}
