package canon

class ProductTag {

    Product product
    Language language
    String tag

    User createdBy
    User modifiedBy
    Date dateCreated
    Date lastUpdated

    static constraints = {
        product unique: "tag"

        createdBy nullable: true
        modifiedBy nullable: true
    }
}
