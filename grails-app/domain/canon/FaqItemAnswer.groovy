package canon

class FaqItemAnswer {

    FaqItem faqItem
    Language language

    String content

    User createdBy
    User modifiedBy
    Date dateCreated
    Date lastUpdated

    static constraints = {
        createdBy nullable: true
        modifiedBy nullable: true
    }
}
