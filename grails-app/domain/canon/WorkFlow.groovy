package canon

import canon.enums.AssignmentType

class WorkFlow {

    Flow flow

    String name
    Integer flowOrder
    Boolean isStart = Boolean.FALSE
    Boolean isEnd = Boolean.FALSE
    AssignmentType assignmentType = AssignmentType.USER

    User createdBy
    User modifiedBy
    Date dateCreated
    Date lastUpdated

    static constraints = {
        flowOrder unique: "flow"

        createdBy nullable: true
        modifiedBy nullable: true
    }
}
