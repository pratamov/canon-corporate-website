package canon

class ProductDetailFeedbackFile {

    ProductDetailFeedback productDetailFeedback
    Media file

    User createdBy
    User modifiedBy
    Date dateCreated
    Date lastUpdated

    static constraints = {
        createdBy nullable: true
        modifiedBy nullable: true
    }
}
