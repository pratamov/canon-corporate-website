package canon

import org.apache.commons.lang.StringUtils

class Profile {

    static transients = ['fullName']

    private static final long serialVersionUID = 1

    User user
    Language language
    Country country

    String firstName
    String lastName
    String title
    String about
    String phone
    String address

    User createdBy
    User modifiedBy
    Date dateCreated
    Date lastUpdated

    static constraints = {
        user unique: "language"
        language nullable: true
        country nullable: true

        lastName blank: true, nullable: true
        title blank: true, nullable: true
        about blank: true, nullable: true
        phone blank: true, nullable: true
        address blank: true, nullable: true
    }

    static mapping = {
        about sqlType: "text"
    }

    public String getFullName() {
        StringBuilder nameBuilder = new StringBuilder(firstName)
        if (StringUtils.isNotBlank(lastName)) {
            nameBuilder.append(" ").append(lastName)
        }
        return nameBuilder.toString()
    }
}
