package canon

import canon.enums.ProductPromotionType
import canon.enums.Status

class ProductPromotion {

    Product product
    String promotionName
    String promotionDesc
    String currencyCode
    BigDecimal amount
    BigDecimal percentage
    Integer quantity
    Status status = Status.DRAFT
    ProductPromotionType promotionType = ProductPromotionType.PERCENTAGE

    User createdBy
    User modifiedBy

    Date dateCreated
    Date lastUpdated

    static constraints = {
        promotionName nullable: true
        promotionDesc nullable: true
        currencyCode nullable: true
        amount nullable: true
        percentage nullable: true
        quantity nullable: true

        createdBy nullable: true
        modifiedBy nullable: true
    }
}
