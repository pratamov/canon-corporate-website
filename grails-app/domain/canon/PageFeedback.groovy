package canon

import canon.enums.Status

class PageFeedback {

    static hasMany = [pageFeedbackFiles: PageFeedbackFile]

    Page page
    User user

    String content
    Status status = Status.APPROVED

    User modifiedBy
    Date dateCreated
    Date lastUpdated

    static constraints = {
        modifiedBy nullable: true
    }

    static mapping = {
        content sqlType: "text"
    }
}
