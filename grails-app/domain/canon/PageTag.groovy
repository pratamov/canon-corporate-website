package canon

class PageTag {

    Page page
    Language language
    String tag

    User createdBy
    User modifiedBy
    Date dateCreated
    Date lastUpdated

    static constraints = {
        page unique: "tag"

        createdBy nullable: true
        modifiedBy nullable: true
    }
}
