package canon

class ProductView {

    Product product

    String sessionId
    String ipAddress
    Date visitDate

    Date dateCreated
    Date lastUpdated

    static constraints = {
        product unique: "sessionId"

        visitDate nullable: false
        sessionId blank: false
    }
}
