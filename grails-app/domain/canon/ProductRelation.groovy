package canon

import canon.enums.RelationType

class ProductRelation {

    Product product
    Product relation
    RelationType relationType
    Site site

    User createdBy
    User modifiedBy
    Date dateCreated
    Date lastUpdated

    static constraints = {
        site nullable: true

        createdBy nullable: true
        modifiedBy nullable: true
    }
}
