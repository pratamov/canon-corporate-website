package canon

class Group {

    static hasMany = [specifications: Specification]

    Topic topic

    String name
    String description
    Integer groupOrder

    User createdBy
    User modifiedBy
    Date dateCreated
    Date lastUpdated

    static constraints = {
        createdBy nullable: true
        modifiedBy nullable: true
    }

    static mapping = {
        table 'grp'
        description sqlType: "text"
    }
}
