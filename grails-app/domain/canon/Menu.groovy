package canon

import canon.enums.MenuLocation
import canon.enums.MenuType
import canon.enums.Segment
import canon.enums.Status

class Menu {

    static hasMany = [children: Menu]

    Site site
    Page page
    Language language
    Segment segment
    Menu parent
    Boolean isMain = false

    String name
    MenuType type
    String url
    Integer orderNo

    MenuLocation location
    Status status
    Boolean isGlobal = true

    User createdBy
    User modifiedBy

    Date dateCreated
    Date lastUpdated

    static constraints = {
        site nullable: true
        page nullable: true
        parent nullable: true
        isMain nullable: true

        type nullable: true, blank: true
        url nullable: true, blank: true
        orderNo blank: true, nullable: true
        location nullable: true, blank: true
        status blank: true, nullable: true
        isGlobal nullable: true

        createdBy nullable: true
        modifiedBy nullable: true

        dateCreated nullable: true
        lastUpdated nullable: true
    }

    static mapping = {
        children sort: 'orderNo', order: 'asc'
    }
}
