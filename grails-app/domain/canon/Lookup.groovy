package canon

import canon.enums.LookupType

class Lookup {

    String name
    LookupType lookupType

    User createdBy
    User modifiedBy
    Date dateCreated
    Date lastUpdated

    static constraints = {
        createdBy nullable: true
        modifiedBy nullable: true
    }
}
