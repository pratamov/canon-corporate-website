package canon

import canon.enums.Status

class Faq {

    Site site

    String name // As Category
    Status status = Status.DRAFT

    // Draft - Archived
    Date archivedDate
    String legacySlug
    Status legacyStatus = Status.DRAFT

    User createdBy
    User modifiedBy
    Date dateCreated
    Date lastUpdated

    static constraints = {
        createdBy nullable: true
        modifiedBy nullable: true

        archivedDate nullable: true
        legacySlug nullable: true, blank: true
    }
}
