package canon

import canon.enums.ContentType
import canon.enums.Segment
import canon.enums.Status
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import grails.transaction.Transactional
import grails.web.servlet.mvc.GrailsParameterMap

@Transactional
class PageService {

    def elasticSearchService

    public List<Page> getFeaturedArticles(String siteCode, String contentTypeName, String segmentSlug, Long categoryId) {
        Date date = new Date()
        Site site = Site.findByCode(siteCode)
        ContentType contentType = ContentType.ARTICLE
        Segment segment
        try {
            contentType = ContentType.valueOf(contentTypeName)
            if (segmentSlug) segment = Segment.getBySlug(segmentSlug)
        } catch (Exception e) {
            log.warn(e?.message)
        }
        List<Page> articles = Page.createCriteria().list {
            createAlias("pageDetails", "ps")
            if (categoryId || segment) {
                createAlias("pageCategories", "pc")
                createAlias("pc.category", "c")
            }
            and {
                eq("ps.site", site)
                eq("isHomepage", Boolean.FALSE)
                eq("status", Status.PUBLISHED)
                eq("contentType", contentType)
                le("publishedDate", date)
                if (categoryId) {
                    eq("c.id", categoryId)
                }
                if (segment) {
                    eq("c.segment", segment)
                }
            }
            order("publishedDate", "desc")
        }
        return articles
    }

    def searchArticles(GrailsParameterMap params, Map<String, Object> paramsOverride){
        def resultList = []
        int resultCount = 0
        def resultAggs = [:]
        try{
            def esResult = elasticSearchService.searchArticles(params, paramsOverride)
            if(esResult) {
                JsonObject json = esResult.getJsonObject()
                resultCount = json.getAsJsonObject("hits")?.get("total")?.getAsInt() ?: 0

                JsonArray hitsArray = json.getAsJsonObject("hits")?.getAsJsonArray("hits")
                hitsArray.each { JsonObject jso ->
                    def product = elasticSearchService.proceedArticle(jso, params?.lang)
                    if (product) resultList.add(product)
                }

                def foundAggs = json.get("aggregations")?.getAsJsonObject()
                resultAggs = elasticSearchService.proceedArticleAggs(params, foundAggs)
            }
        }catch (Exception e) {
            log.error(e?.message, e)
        }

        return [
                resultCount: resultCount,
                resultList: resultList,
                resultAggs: resultAggs
        ]
    }
}
