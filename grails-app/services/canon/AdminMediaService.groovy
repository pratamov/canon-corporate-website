package canon

import canon.enums.MediaSource
import canon.enums.Status
import canon.utils.AWTUtil
import canon.utils.ImageUtils
import canon.utils.MediaUtils
import grails.transaction.Transactional
import grails.web.servlet.mvc.GrailsParameterMap
import org.apache.commons.io.FilenameUtils
import org.jcodec.api.FrameGrab
import org.jcodec.common.model.ColorSpace
import org.jcodec.common.model.Picture
import org.jcodec.scale.ColorUtil
import org.jcodec.scale.Transform
import org.springframework.beans.factory.annotation.Value
import org.springframework.web.multipart.MultipartFile

import javax.imageio.ImageIO
import java.awt.*
import java.awt.image.BufferedImage
import java.text.SimpleDateFormat
import java.util.List

@Transactional
class AdminMediaService {

    @Value('${application.media.thumbnail.width}')
    Integer thumbnailWidth

    @Value('${application.media.thumbnail.height}')
    Integer thumbnailHeight

    @Value('${amazonaws.s3.baseUrl}')
    String awsBaseUrl

    def amazonS3Service
    def springSecurityService
    def messageSource

    def upload(MultipartFile file, Media media, boolean isImage, boolean isVideo) {
        String uuid = UUID.randomUUID().toString().replaceAll("-","")
        String tempdir = System.getProperty("java.io.tmpdir");
        String fileName = uuid + "_" + file.getOriginalFilename()

        String awsPath = getMediaS3Path(media.mediaType?.name()?.toLowerCase())
        String fileLocation = awsPath + "/" + fileName
        String mediaLocation = amazonS3Service.uploadFile(file.getInputStream(), fileLocation, file.getSize());

        File fileDest = new File(tempdir,fileName)
        file.transferTo(fileDest)

        if (isImage) {
            BufferedImage bufferedImage = new ImageUtils().readImage(fileDest)
            media.width = bufferedImage.getWidth()
            media.height = bufferedImage.getHeight()
            ImageIO.write(bufferedImage, FilenameUtils.getExtension(fileName), fileDest)

            String thumbnailFileName = FilenameUtils.getBaseName(fileName) + ".thumb." + FilenameUtils.getExtension(file.getOriginalFilename())
            File thumbnailFile = new File(tempdir + "/" + thumbnailFileName)
            Dimension dimension = ImageUtils.getScaledDimension(new Dimension(media.width, media.height), new Dimension(thumbnailWidth,thumbnailHeight))
            ImageUtils.createThumbnail(fileDest, thumbnailFile, (int) Math.ceil(dimension.getWidth()), (int) Math.ceil(dimension.getHeight()))
            String thumbnailLocation = awsPath + "/" + thumbnailFileName
            String thumbnailImageLocation = amazonS3Service.uploadFile(new FileInputStream(thumbnailFile), thumbnailLocation, thumbnailFile.size())
            media.urlThumbnail = thumbnailImageLocation
        } else if (isVideo) {
            String thumbnailFileNameOri = FilenameUtils.getBaseName(fileName) + ".png";
            String thumbnailFileName = FilenameUtils.getBaseName(fileName) + ".thumb.png";

            try {
                Picture frame = FrameGrab.getNativeFrame(fileDest, 0)
                Transform transform = ColorUtil.getTransform(frame.getColor(), ColorSpace.RGB)
                Picture rgb = Picture.create(frame.getWidth(), frame.getHeight(), ColorSpace.RGB)
                transform.transform(frame, rgb)
                BufferedImage bi = AWTUtil.toBufferedImage(rgb)

                File thumbnailFileOri = new File(tempdir + "/" + thumbnailFileNameOri)
                File thumbnailFile = new File(tempdir + "/" + thumbnailFileName)
                ImageIO.write(bi, "png", thumbnailFileOri)
                media.width = rgb.getWidth()
                media.height = rgb.getHeight()
                Dimension dimension = ImageUtils.getScaledDimension(new Dimension(media.width, media.height), new Dimension(thumbnailWidth,thumbnailHeight))
                ImageUtils.createThumbnail(thumbnailFileOri, thumbnailFile, (int) Math.ceil(dimension.getWidth()), (int) Math.ceil(dimension.getHeight()))
                String thumbnailLocationOri = awsPath + "/" + thumbnailFileNameOri
                String thumbnailLocation = awsPath + "/" + thumbnailFileName
                String thumbnailImageLocation = amazonS3Service.uploadFile(new FileInputStream(thumbnailFile), thumbnailLocation, thumbnailFile.size())
                String thumbnailImageLocationOri = amazonS3Service.uploadFile(new FileInputStream(thumbnailFileOri), thumbnailLocationOri, thumbnailFileOri.size())
                media.urlThumbnail = thumbnailImageLocation
                media.urlPoster = thumbnailImageLocationOri
            } catch (Throwable t) {
                String thumbnailImageLocation = awsBaseUrl + '/assets/default-image/component_video.png'
                String thumbnailImageLocationOri = awsBaseUrl + '/assets/default-image/component_video.png'
                media.urlThumbnail = thumbnailImageLocation
                media.urlPoster = thumbnailImageLocationOri
            }
        } else {
            // TODO:: Upload Document
        }

        media.fileName = file.getOriginalFilename()
        media.fileSize = file.getSize()
        media.url = mediaLocation
        media.source = MediaSource.INTERNAL
        media.status = Status.DRAFT

        media.createdBy = springSecurityService?.getCurrentUser()
        media.modifiedBy = springSecurityService?.getCurrentUser()

        media.validate()
        if (media.hasErrors()) {
            def errMsgList = media.errors.allErrors.collect{
                messageSource.getMessage(it, java.util.Locale.default)
            }
            return [
                    status: "error",
                    errors: errMsgList?.join("\n"),
                    message: "File has not been uploaded.",
                    success: false,
                    data: [:]
            ]
        }

        media.save(failOnError: true)
        return [
                status: "success",
                errors: [],
                message: "File has been successfully uploaded.",
                success: true,
                data: [
                        alt: media.alt,
                        copyright: media.copyright,
                        height: media.height,
                        id: media.id,
                        name: media.title,
                        source: media.source?.name(),
                        thumbnail: media.urlThumbnail,
                        type: media.mediaType?.name(),
                        url: media.url,
                        width: media.width
                ]
        ]
    }

    def createFromLink(Media media) {
        media.source = MediaSource.EXTERNAL
        media.fileName = FilenameUtils.getBaseName(media.url)
        media.mediaType = MediaUtils.forType(media.url)

        media.validate()
        if (media.hasErrors()) {
            def errMsgList = media.errors.allErrors.collect{
                messageSource.getMessage(it, java.util.Locale.default)
            }
            return [
                    status: "error",
                    errors: errMsgList?.join("\n"),
                    message: "Media has not been saved.",
                    success: false,
                    data: [:]
            ]
        }

        media.save flush: true

        return [
                status: "success",
                errors: [],
                message: "Media has been saved.",
                success: true,
                data: [
                        alt: media.alt,
                        copyright: media.copyright,
                        height: media.height,
                        id: media.id,
                        name: media.title,
                        source: media.source?.name(),
                        thumbnail: media.urlThumbnail,
                        type: media.mediaType?.name(),
                        url: media.url,
                        width: media.width
                ]
        ]
    }

    def search(GrailsParameterMap params) {
        Integer max = Math.min(params?.int("max", 10), 30)
        Integer page = params?.int("page", 1)
        Integer offset = (page - 1) * max

        String keyword = '%' + params?.q + '%'
        List<Media> medias = Media.createCriteria().list(max: max, offset: offset) {
            if (params?.q) {
                or {
                    like("fileName", keyword)
                    like("title", keyword)
                }
            }
            order("dateCreated", "desc")
        }

        def result = []
        medias?.each { Media media ->
            result.push([
                    alt: media.alt,
                    copyright: media.copyright,
                    height: media.height,
                    id: media.id,
                    name: media.title,
                    source: media.source?.name(),
                    thumbnail: media.urlThumbnail,
                    type: media.mediaType?.name(),
                    url: media.url,
                    width: media.width
            ])
        }
        return result
    }

    def getMediaFolders() {
        return [
                "images": "images",
                "videos": "videos"
        ]
    }

    private String getMediaS3Path(String mediaType){
        return "media/" + mediaType + "/" + new SimpleDateFormat("yyyy/MM/dd").format(new Date())
    }
}
