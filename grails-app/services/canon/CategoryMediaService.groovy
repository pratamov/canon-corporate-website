package canon

import grails.transaction.Transactional

@Transactional
@Deprecated // Will merge into Category Service
class CategoryMediaService {

    @Deprecated // moved into category service
    CategoryMedia getFirstByCategory(Category category) {
        CategoryMedia mediaResult = CategoryMedia.createCriteria().get() {
            eq("category", category)
            maxResults(1)
            order("mediaOrder")
        }

        return mediaResult
    }
}
