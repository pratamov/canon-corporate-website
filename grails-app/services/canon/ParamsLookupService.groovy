package canon

import grails.plugin.cache.Cacheable
import grails.transaction.Transactional
import org.apache.commons.lang.StringUtils

@Transactional
class ParamsLookupService {

    def generalService

    @Cacheable('countryLookup')
    public Country checkCountry(String search) {
        if (StringUtils.isBlank(search)) return null

        return Country.findByCode(search)
    }

    @Cacheable('siteLookup')
    public Site checkSite(String search) {
        if (StringUtils.isBlank(search)) return null

        Map<String, Site> map = generalService.getSitesMap()
        return map.get(search?.toLowerCase())
    }
    @Cacheable('languageSelected')
    public Language getLanguageSelected(String search) {
        if (StringUtils.isBlank(search)) return null

        Map<String, Language> map = generalService.getLanguagesMap()
        if (map.containsKey(search.toLowerCase())) {
            return map.get(search.toLowerCase())
        }

        return null
    }

}
