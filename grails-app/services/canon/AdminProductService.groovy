package canon

import canon.enums.RelationType
import canon.enums.Status
import canon.utils.SlugCodec
import grails.transaction.Transactional
import grails.web.servlet.mvc.GrailsParameterMap

import java.text.SimpleDateFormat

@Transactional
class AdminProductService {

    def getProductList(GrailsParameterMap params, def paramsOverride) {

        Integer max = Math.min(params?.int("max", 10), 30)
        Integer page = params?.int("page", 1)
        Integer offset = (page - 1) * max
        String sort = params?.sort ?: params.status == Status.PUBLISHED ? "publishedDate" : "lastUpdated"
        String order = params?.order ?: "desc"

        def paramsMap = [:]
        params.each {
            paramsMap.put(it.key, it.value)
        }

        if (paramsOverride) {
            paramsOverride?.each {
                paramsMap[it.key] = it.value
            }
        }

        Status statusFilter
        try {
            if (paramsMap.get("status")) statusFilter = Status.valueOf(paramsMap.status as String)
        } catch (Exception e) {e.printStackTrace()}

        return Product.createCriteria().list(max: max, offset: offset, order: order, sort: sort) {
            projections {
                property('id')
                property('name')
                property('status')
                property('publishedDate')
                property('lastUpdated')
            }
            isNull("parent")
            if (paramsMap.get("q")) {
                like("name", "%" + paramsMap.get("q") + "%")
            }
            if (statusFilter) {
                eq("status", statusFilter)
            }
            if (paramsMap.get("dateFrom")) {
                ge(statusFilter == Status.PUBLISHED ? "publishedDate" : "lastUpdated", paramsMap.get("dateFrom"))
            }
            if (paramsMap.get("dateTo")) {
                le(statusFilter == Status.PUBLISHED ? "publishedDate" : "lastUpdated", paramsMap.get("dateTo"))
            }
        }
    }

    def getProductCount(GrailsParameterMap params, def paramsOverride) {

        Integer max = Math.min(params?.int("max", 10), 30)
        Integer page = params?.int("page", 1)
        Integer offset = (page - 1) * max

        def paramsMap = [:]
        params.each {
            paramsMap.put(it.key, it.value)
        }

        if (paramsOverride) {
            paramsOverride?.each {
                paramsMap[it.key] = it.value
            }
        }

        Status statusFilter
        try {
            if (paramsMap.get("status")) statusFilter = Status.valueOf(paramsMap.status as String)
        } catch (Exception e) {e.printStackTrace()}

        return Product.createCriteria().get() {
            projections {
                countDistinct('id')
            }
            isNull("parent")
            if (paramsMap.get("q")) {
                like("name", "%" + paramsMap.get("q") + "%")
            }
            if (statusFilter) {
                eq("status", statusFilter)
            }
            if (paramsMap.get("dateFrom")) {
                ge(statusFilter == Status.PUBLISHED ? "publishedDate" : "lastUpdated", paramsMap.get("dateFrom"))
            }
            if (paramsMap.get("dateTo")) {
                lt(statusFilter == Status.PUBLISHED ? "publishedDate" : "lastUpdated", paramsMap.get("dateTo"))
            }
        }
    }

    def saveProduct(GrailsParameterMap params, Product product) {
        SimpleDateFormat sdf = new SimpleDateFormat("d MMM yyyy")


        //Set product attribute value
        product.slug = SlugCodec.encode(product.name)
        Date avDate = null
        Date dcDate = null
        try{
            avDate = sdf.parse(params?.availabilityDateString)
            dcDate = sdf.parse(params?.discontinuedDateString)

        }catch (Exception e) {
            e.printStackTrace()
        }

        product.availabilityDate = avDate
        product.discontinuedDate = dcDate

        //Product location
        def locationSiteIds = params?.list('locationSiteId')*.toLong()
        locationSiteIds?.eachWithIndex{id, idx ->
            Site site = Site.findById(id)
            Category category = Category.findById(params?.list('locationCategoryId')[idx]?.toLong())
            String currencyCode = params?.list('locationCurrencyCode')[idx]
            BigDecimal price = params?.list('locationPrice')[idx] ? new BigDecimal(params?.list('locationPrice')[idx]) : null
            Date publishedDate = params?.list('locationPublishedDate')[idx] ? sdf.parse(params?.list('locationPublishedDate')[idx]) : null
            Date archivedDate = params?.list('locationArchivedDate')[idx] ? sdf.parse(params?.list('locationArchivedDate')[idx]) : null

            product.addToProductDetails(new ProductDetail(site: site, currencyCode: currencyCode, price: price, publishedDate: publishedDate, archivedDate:  archivedDate))
            product.addToProductCategories(new ProductCategory(category: category))
        }

        product.validate()
        if (product.hasErrors()){
            return product
        } else {
            return product.save(failOnError: true)
        }
    }

    def updateProduct(GrailsParameterMap params, Product product) {
        SimpleDateFormat sdf = new SimpleDateFormat("d MMM yyyy")
        //Set product attribute value
        product.slug = SlugCodec.encode(product.name)

        Date avDate = null
        Date dcDate = null
        try{
            avDate = sdf.parse(params?.availabilityDateString)
            dcDate = sdf.parse(params?.discontinuedDateString)
        }catch (Exception e) {
            e.printStackTrace()
        }

        product.availabilityDate = avDate
        product.discontinuedDate = dcDate

        //Product location
        /*def locationSiteIds = params?.list('locationSiteId')*.toLong()
        locationSiteIds?.eachWithIndex{id, idx ->
            Site site = Site.findById(id)
            Category category = Category.findById(params?.list('locationCategoryId')[idx]?.toLong())
            String currencyCode = params?.list('locationCurrencyCode')[idx]
            BigDecimal price = params?.list('locationPrice')[idx] ? new BigDecimal(params?.list('locationPrice')[idx]) : null
            Date publishedDate = params?.list('locationPublishedDate')[idx] ? sdf.parse(params?.list('locationPublishedDate')[idx]) : null
            Date archivedDate = params?.list('locationArchivedDate')[idx] ? sdf.parse(params?.list('locationArchivedDate')[idx]) : null

            product.addToProductPublications(new ProductDetail(site: site, currencyCode: currencyCode, price: price, publishedDate: publishedDate, archivedDate:  archivedDate))
            product.addToProductCategories(new ProductCategory(category: category))
        }*/

        product.validate()
        if (product.hasErrors()){
            return product
        } else {
            return product.save(failOnError: true)
        }
    }

    def duplicateProduct(List<Long> productIds){
        List<Product> products = Product.findAllByIdInList(productIds)

        products.each {Product origin ->
            Product duplicate = duplicateProduct(origin)

            duplicate.validate()
            if(!duplicate.hasErrors()) {
                duplicate.save(failOnError: true)
            }
        }
    }

    def duplicateProduct(Product product) {
        String duplicateSign = "Copy of "

        Product duplicate = new Product()

        def productProperties = ["language", "availabilityDate", "discontinuedDate", "description",
                                 "subText", "topic", "specNote", "supportLink", "metaDescription",
                                 "metaMedia", "metaTitle", "slug", "site", "banner"]

        productProperties.each {
            duplicate[it] = product[it]
        }

        // Product custom properties
        duplicate.name = duplicateSign + product.name

        // Duplicate relations
        product.productSellingPoints?.each {ProductSellingPoint psp ->
            duplicate.addToProductSellingPoints(
                    new ProductSellingPoint(
                            content: psp.content,
                            pointOrder: psp.pointOrder
                    )
            )
        }

        product.productMedias?.each {ProductMedia pm ->
            duplicate.addToProductMedias(
                    new ProductMedia(
                            media: pm.media,
                            mediaOrder: pm.mediaOrder,
                            isDefault: pm.isDefault
                    ))
        }

        product.productRelations?.each {ProductRelation pr ->
            if ([RelationType.CONSUMABLE, RelationType.ACCESSORIES].contains(pr.relationType)) {
                duplicate.addToProductRelations(
                        new ProductRelation(
                                relation: pr.relation,
                                relationType: pr.relationType,
                                site: pr.site
                        )
                )
            }
        }

        product.productDocuments?.each {ProductDocument pd ->
            duplicate.addToProductDocuments(
                    new ProductDocument(
                            document: pd.document,
                            documentOrder: pd.documentOrder,
                            documentType: pd.documentType
                    ))
        }

        product.productSpecifications?.each {ProductSpecification ps ->
            duplicate.addToProductSpecifications(
                    new ProductSpecification(
                            specification: ps.specification,
                            specValue: ps.specValue,
                            specUnitValue: ps.specUnitValue,
                            specOrder: ps.specOrder
                    )
            )
        }

        product.productComponents?.each {ProductComponent pc ->
            duplicate.addToProductComponents(
                    new ProductComponent(
                            component: pc.component
                    )
            )
        }

        return duplicate
    }

    def deleteProducts(List<Long> productIds){
        List<Product> products = []
        if(productIds.size() > 0) products = Product.findAllByIdInList(productIds)

        products?.each {Product p ->
            p.status = Status.DELETED
            p.save(failOnError: true)
        }
    }

    def archiveProducts (List<Long> productIds){
        List<Product> products = []
        if(productIds.size() > 0) products = Product.findAllByIdInList(productIds)

        products?.each {Product p ->
            p.status = Status.ARCHIVED
            p.save(failOnError: true)
        }
    }
	
	def restoreProducts(List<Long> productIds){
		List<Product> products = []
		if(productIds.size() > 0) products = Product.findAllByIdInList(productIds)

		products?.each {Product p ->
			p.status = p.getLegacyStatus()
			p.save(failOnError: true)
		}
	}

    def deleteProductSellingPoints(List<Long> productSellingPointIds){
        List<ProductSellingPoint> productSellingPoints = []
        if(productSellingPoints.size() > 0) productSellingPoints = ProductSellingPoint.findAllByIdInList(productSellingPointIds)

        productSellingPoints?.each {Product p ->
            p.status = Status.DELETED
            p.save(failOnError: true)
        }
    }

    def saveProductSellingPoint(GrailsParameterMap params, ProductSellingPoint productSellingPoint) {
    }

    def findAll() {
        def productList = Product.createCriteria().list(sort: "name") {
            projections {
                property ("id")
                property ("name")
            }
        }

        def result = []
        productList?.each {
            result.push([
                    id: it[0],
                    name: it[1]
            ])
        }

        return result
    }
}




