package canon

import canon.model.contact.asia.ContactUsForm
import grails.transaction.Transactional

@Transactional
class ContactService {

    def submitAsiaContact(canon.model.contact.asia.ContactUsForm contactUsForm, B2BContact b2BContact) {
        b2BContact.save(flush: true, failOnError: true);
    }
}
