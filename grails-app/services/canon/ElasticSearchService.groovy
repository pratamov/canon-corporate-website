package canon

import canon.enums.*
import canon.model.comp.TextComp
import canon.utils.ContentUtils
import com.google.gson.JsonObject
import grails.plugin.cache.Cacheable
import grails.plugins.rest.client.RestBuilder
import grails.transaction.Transactional
import grails.web.mapping.LinkGenerator
import grails.web.servlet.mvc.GrailsParameterMap
import groovy.json.JsonBuilder
import groovyx.gpars.GParsPool
import io.searchbox.client.JestClient
import io.searchbox.client.JestClientFactory
import io.searchbox.client.JestResult
import io.searchbox.client.config.HttpClientConfig
import io.searchbox.core.Delete
import io.searchbox.core.Index
import io.searchbox.core.Search
import org.apache.commons.lang.StringUtils
import org.springframework.beans.factory.annotation.Value

import javax.annotation.PostConstruct
import java.sql.Timestamp
import java.text.SimpleDateFormat

@Transactional
class ElasticSearchService {

    @Value('${elasticsearch.hosts}')
    private List<String> serverList

    @Value('${elasticsearch.index}')
    private String indexName

    @Value('${elasticsearch.types.articles}')
    private String articleTypeName

    @Value('${elasticsearch.types.products}')
    private String productsTypeName

    private JestClient jestClient
    private restBuilder = new RestBuilder()

    LinkGenerator grailsLinkGenerator

    @PostConstruct
    def init() {
        this.getESConnection()
    }

    def indexingArticle(Long id) {
        indexingArticle(Page.findById(id))
    }

    def indexingArticle(Page page) {
        if (page == null) {
            return false
        }

        def banner = [:]
        if (page.getBanner()) {
            banner = [copyright: page.getBanner()?.copyright, id: page.getBanner()?.mediaId, url: page.getBanner()?.url]
        }

        def categories = []
        page?.pageCategories?.each {
            categories.add([
                    id: it?.category?.id,
                    language: it?.category?.language?.id,
                    name: it?.category?.name,
                    segement: it?.category?.segment?.name(),
                    slug: it?.category?.slug
            ])
        }

        def contents = []
        if (page.contentTemplate == ContentTemplate.GENERIC) {
            contents.add([content: page.content, language: page.language?.id])
        } else {
            List<PageComponent> pcts = PageComponent.createCriteria().list() {
                and {
                    eq('page', page)
                    eq('status', Status.PUBLISHED)
                    'in'('component.compType', [ComponentType.TEXT.name()])
                }
                order("component.compOrder", "asc")
            }
            StringBuilder sb = new StringBuilder()
            pcts?.each {
                if (it.component?.compType == ComponentType.TEXT) {
                    TextComp textComp = it.component?.getAsComponent() as TextComp
                    sb.append(ContentUtils.cleanUp(textComp.getContent()))
                    sb.append("\n")
                }
            }
            contents.add([content: sb.toString(), language: page.language?.id])
        }

        def titles = []
        titles.add([language: page.language?.id, title: page.headline])

        def languages = []
        def sites = []
        languages.add(page?.language?.id)
        List<Page> pages = Page.findAllByParent(page)
        pages?.each {
            if (it.status == Status.PUBLISHED) {
                languages.add(it.language?.id)
                sites.add([id: it.site?.id, name: it.site?.name])
                titles.add([language: it.language?.id, title: it.headline])
            }
        }

        def shortDescription = []
        String shortDesc = ""
        if (StringUtils.isNotBlank(page.deck)) {
            shortDesc = page?.deck
        } else {
            PageComponent pct = page.getFirstTextComponent()
            if (pct) {
                TextComp textComp = pct.component?.getAsComponent() as TextComp
                if (textComp) {
                    shortDesc = ContentUtils.truncate(textComp?.getContent())
                }
            }
        }
        shortDescription.add([language: page.language?.id, content: shortDesc])

        def subjects = []
        page?.pageSubjects?.each {
            subjects.add([id: it.subject?.id, name: it.subject?.name, slug: it.subject?.slug])
        }

        def tags = []
        def pt = PageTag.createCriteria().list {
            createAlias("language", "l")
            projections {
                property("tag")
                property("l.id")
            }
            eq("page", page)
        }
        pt?.each {
            tags.push([language: it[1], name: it[0]])
        }

        def source = [:]
        source.put("banner", banner)
        source.put("categories", categories)
        source.put("content_setting", page.contentSetting?.name())
        source.put("content_template", page.contentTemplate?.name())
        source.put("content_type", page.contentType?.name())
        source.put("contents", contents)
        source.put("created_by", [id: page?.createdBy?.id, name: page?.createdBy?.username])
        source.put("date_created", page?.dateCreated)
        source.put("deck", page?.deck)
        source.put("languages", languages)
        source.put("last_updated", page?.lastUpdated)
        source.put("modified_by", [id: page?.createdBy?.id, name: page?.createdBy?.username])
        source.put("published_date", page?.publishedDate)
        source.put("published_version", page?.publishedVersion)
        source.put("short_descriptions", shortDescription)
        source.put("sites", sites)
        source.put("slug", page?.slug)
        source.put("status", page?.status?.name())
        source.put("subjects", subjects)
        source.put("tags", tags)
        source.put("title", page?.headline)
        source.put("titles", titles)
        source.put("updated_at", page?.lastUpdated)

        try {
            JestClient client = this.getESConnection()
            Index index = new Index.Builder(source).index(indexName).type(articleTypeName).id(page?.id?.toString()).build()
            JestResult jestResult = client.execute(index)
            if (jestResult?.responseCode == 403) {
                throw new RuntimeException(jestResult?.jsonObject?.get("Message")?.getAsString())
            } else if (jestResult?.responseCode == 400) {
                throw new RuntimeException(jestResult?.jsonObject?.getAsJsonObject("error")?.get("reason")?.getAsString())
            }
        } catch (Exception e) {
            log.error(e?.message, e)
            return false
        }
        return true
    }

    def indexingArticles(Boolean publishedOnly) {
        def exceptionList = [Status.RELEASE]

        def pages = Page.createCriteria().list() {
            projections {
                property("id")
            }
            and {
                not {
                    'in'("status", exceptionList)
                }
                if (publishedOnly) {
                    eq("status", Status.PUBLISHED)
                }
            }
        }

        log.info("Articles to be indexed: " + pages?.size())

        int c = 0
        GParsPool.withPool(30) {
            pages?.eachParallel { Long id ->
                try {
                    indexingArticle(id)
                } catch (Exception e) {
                    log.error(e?.message, e)
                }

                if (c % 100 == 0) {
                    log.info("Articles indexed: " + c)
                }
                c += 1
            }
        }
        log.info("Articles indexed: " + c)
        log.info("Indexing articles completed.")
    }

    public Boolean isDocumentExists(String id, String type) {

        try {
            Set<String> servers = getESServers()
            for (String server : servers) {
                String docUrl = server + "/" + indexName + "/" + type + "/" + id
                def result = restBuilder.head(docUrl)
                if (result.status == 200) {
                    return true
                }
            }

        } catch (Exception e) {
            log.error(e?.message, e)
        }

        return false
    }

    def cleanUpIndexedArticles() {
        // Delete Trashed and Archived articles
        def list = [Status.ARCHIVED, Status.DELETED]
        def pageList = Page.createCriteria().list() {
            and {
                'in'("status", list)
            }
        }

        pageList?.each { Page page ->
            deleteDocumentIndex(page)
        }
    }

    def deleteDocumentIndex(Page page) {
        JestClient client = this.getESConnection()

        def result = [:]

        if (page == null) {
            result.status = false
            result.id = null
            return result
        }

        try {
            Delete delete = new Delete.Builder(page?.id?.toString()).index(indexName).type(articleTypeName).build()
            JestResult jestResult = client.execute(delete)

            result.status = jestResult.isSucceeded()
            result.id = page?.id

        } catch (Exception e) {
            log.error(e?.message, e)

            result.status = false
            result.id = page?.id
        }

        return result
    }

    def indexingProduct(Long id) {
        indexingProduct(Product.findById(id))
    }

    def indexingProduct(Product product) {
        if (product == null) {
            return false
        }

        def source = [:]
        def productMedias = product?.productMedias

        // Banner
        Long bannerId = null
        String bannerUrl = null
        if (productMedias?.size() > 0) {
            ProductMedia productMedia = product.defaultMedia
            if (productMedia) {
                bannerId = productMedia?.media?.id
                bannerUrl = productMedia?.media?.url
            }
        }
        source.put("banner", [id: bannerId, url: bannerUrl])

        def categories = []
        product.productCategories?.each { ProductCategory pc ->
            categories.push([id: pc?.category?.id, name: pc?.category?.name, slug: pc?.category?.slug])
        }
        source.put("categories", categories)

        def sites = []
        product.productDetails?.each { ProductDetail ps ->
            sites.push([id: ps?.site?.id, name: ps?.site?.name])
        }
        source.put("sites", sites)

        source.put("created_at", product?.dateCreated)
        source.put("created_by", [id: product?.createdBy?.id, name: product?.createdBy?.username])
        source.put("description", product.description)

        /*def distributors = []
        product?.productDistributors?.each {
            distributors.push([id: it?.distributor?.id, name: it?.distributor?.name, descriptions: it?.distributor?.description])
        }
        source.put("distributors", distributors)*/

        /*def features = []
        product?.productFeatures?.each {
            features.push([id: it?.feature?.id, language: product?.language, name: it?.feature?.name, descriptions: it?.feature?.description])
        }
        source.put("features", features)*/

        def medias = []
        productMedias?.each {
            medias.push([id: it?.media?.id, url: it?.media?.url])
        }
        source.put("medias", medias)

        source.put("language", product?.language?.id)
        source.put("name", product.name)
        source.put("popular", Boolean.TRUE) // TODO: Replace this with proper popular formula
        source.put("price", product?.price)
        source.put("product_level", product?.productLevel?.name())
        source.put("published_date", product?.publishedDate)
        source.put("slug", product?.slug)

        def specifications = []
        product?.productSpecifications?.each {
            specifications.push([id: it?.specification?.id, important: it?.specification?.important, language: product?.language?.id, name: it?.specification?.name,
                                 parent: it?.specification?.parent?.id, product_spec_order: it?.specOrder, spec_order: it?.specification?.specOrder, spec_type: it?.specification?.specType, spec_value: it?.specValue])
        }
        source.put("specifications", specifications)

        source.put("status", product.status?.name())
        source.put("updated_at", product?.lastUpdated)
        source.put("sub_text", product.subText)
        source.put("view_count", product?.viewCount)


        // Translations
        def names = []
        def subTexts = []
        def descriptions = []
        def languages = []

        if (StringUtils.isNotBlank(product.name)) names.push([language: product.language?.id, name: product.name])
        if (StringUtils.isNotBlank(product.subText)) subTexts.push([language: product.language?.id, sub_text: product.subText])
        if (StringUtils.isNotBlank(product.description)) descriptions.push([language: product.language?.id, description: product.description])
        languages.push(product?.language?.id)

        /*product.productTranslations?.each {
            if (StringUtils.isNotBlank(it.name)) names.push([language: it.language?.id, name: it.name])
            if (StringUtils.isNotBlank(it.subText)) subTexts.push([language: it.language?.id, sub_text: it.subText])
            if (StringUtils.isNotBlank(it.mainDesc)) descriptions.push([language: it.language?.id, description: it.description])
            languages.push(it.language.id)
        }*/
        source.put("descriptions", descriptions)
        source.put("languages", languages)
        source.put("names", names)
        source.put("sub_texts", subTexts)

        try {
            JestClient client = this.getESConnection()
            Index index = new Index.Builder(source).index(indexName).type(productsTypeName).id(product?.id?.toString()).build()
            JestResult jestResult = client.execute(index)
            if (jestResult?.responseCode == 403) {
                throw new RuntimeException(jestResult?.jsonObject?.get("Message")?.getAsString())
            } else if (jestResult?.responseCode == 400) {
                throw new RuntimeException(jestResult?.jsonObject?.getAsJsonObject("error")?.get("reason")?.getAsString())
            }
        } catch (Exception e) {
            log.error(e?.message, e)
            return false
        }
        return true
    }

    def indexingProducts(Boolean publishedOnly) {
        def exceptionList = [Status.RELEASE]

        def products = Product.createCriteria().list {
            projections {
                property("id")
            }
            and {
                not { 'in'("status", exceptionList) }
                if (publishedOnly) {
                    eq("status", Status.PUBLISHED)
                }
            }
        }

        log.info("Products to be indexed: " + products?.size())

        GParsPool.withPool(30) {
            int c = 0
            products?.eachParallel { Long id ->
                try {
                    indexingProduct(id)
                } catch (Exception e) {
                    log.error(e?.message, e)
                }

                if (c % 100 == 0) {
                    log.info("Products indexed: " + c)
                }
                c += 1
            }
            log.info("Products indexed: " + c)
        }
        log.info("Indexing Products completed.")
    }

    def deleteDocumentIndex(Product product) {

        def result = [:]
        if (product == null) {
            result.status = false
            result.id = null
            return result
        }

        try {
            JestClient client = this.getESConnection()
            Delete delete = new Delete.Builder(product?.id?.toString()).index(indexName).type(productsTypeName).build()
            JestResult jestResult = client.execute(delete)

            result.status = jestResult.isSucceeded()
            result.id = product?.id

        } catch (Exception e) {
            log.error(e?.message, e)

            result.status = false
            result.id = product?.id
        }

        return result
    }

    def searchAll(GrailsParameterMap params) {

        def dTime = new Timestamp(System.currentTimeMillis())

        def multiMatch = [
                query: params?.get("q"),
                fields: [
                        "title.search^4", "deck^3", "author.name^2", "tags^2", "content^1",
                        "names.name.search^4",
                        "keyword_aliases^3",
                        "cuisine_groups.group^3",
                        "cuisines.cuisine^3",
                        "areas.name^2",
                        "descriptions.desc_main^1"
                ],
                type: "best_fields",
                operator: "and",
                slop: 10
        ]

        def sortFields = ["_score", [(params?.sort?:"published_date"): params?.order ?: "desc"]]

        def terms = []
        def statusFilter = [term: [status: Status.PUBLISHED.value()]]
        terms.push(statusFilter)

        def dateFilter = [range: [published_date: [lte: dTime]]]
        terms.push(dateFilter)

        def categories = []
        if (params['category.id']) {
            categories.push(params?.long("category.id"))
            def catFilters = [terms: ["category.id": categories]]
            terms.push(catFilters)
        }

        def sites = []
        if (params?.site) {
            sites = Site.createCriteria().list {
                projections {
                    distinct("id")
                }
                eq("code", params?.site)
                eq("status", Status.ACTIVE.value())
            }
            if (sites?.size() > 0) {
                terms.push([terms: ["sites.id": sites]])
            }
        }

        if (params?.country && sites?.size() < 1) {
            sites = Site.createCriteria().list {
                projections {
                    distinct("id")
                }
                country {
                    eq("code", params?.country)
                }
                eq("status", Status.ACTIVE.value())
            }
            if (sites?.size() > 0) {
                terms.push([terms: ["sites.id": sites]])
            }
        }
        // ignore contents of inactivated sites
        if (!sites) {
            sites = Site.createCriteria().list {
                projections {
                    distinct("id")
                }
                eq("status", Status.ACTIVE.value())
            }
            if (sites) {
                terms.push([terms: ["sites.id": sites]])
            }
        }

        if (params?.lang) {
            String lang = getOriginalFilter(params.lang as String, ParamType.LANGUAGE.name())
            terms.push([term: [languages: lang]])
        }

        def shouldFilters = []
        /*
        if (params?.lang) {
            def langFilter = [bool: [must: ["term": [lang: params?.lang]]]]
            def langFilterExists = [bool: [must_not: [exists: ["field": "lang"]]]]
            shouldFilters.addAll([langFilter, langFilterExists])
        }
        */

        JsonBuilder builder = new JsonBuilder ()
        def map = builder {
            from params?.int("offset") ?: 0
            size params?.int("max") ?: 10
            sort sortFields
            query {
                bool {
                    must {
                        multi_match multiMatch
                    }
                    filter {
                        bool {
                            must (terms)
                            if (shouldFilters?.size() > 0) {
                                should (shouldFilters)
                                minimum_number_should_match 1
                            }
                        }
                    }
                }
            }
        }

        String query = builder.toString()

        JestClient client = this.getESConnection()
        def searchBuilder = new Search.Builder(query).addIndex(indexName)
        if (params?.list("types")?.contains("articles")) {
            searchBuilder.addType(articleTypeName)
        }
        if (params?.list("types")?.contains("restaurants")) {
            searchBuilder.addType(productsTypeName)
        }
        if (StringUtils.isBlank(params?.list("types")?.join(""))) {
            searchBuilder.addType(articleTypeName).addType(productsTypeName)
        }

        Search search = searchBuilder.build()
        JestResult result = null
        try {
            result = client.execute(search)
        } catch (Exception e) {
            log.error(e?.message, e)
        }

        return result
    }

    def searchProducts(GrailsParameterMap params, Map<String, Object> paramsOverride) {

        Date date = new Date()
        params.max = Math.min(params?.int("max") ?: 30, 100)
        params?.page = params?.int("page") ?: 1
        params.sort = params?.sort ?: 'az'
        String lang = params?.lang

        Integer offset = (params?.page - 1) * params?.max

        if (StringUtils.isNotBlank(lang)) {
            lang = getOriginalFilter(lang, ParamType.LANGUAGE.name())
        }

        def multiMatch = [
                query: params?.get("q"),
                fields: [
                        "names.name.search^4",
                        "descriptions.description^1"
                ],
                type: "best_fields",
                operator: "and",
                slop: 10
        ]

        String sortedField = "names.name.keyword"
        String sortDirection = "asc"
        def sortableField = Product.ES_SORTABLE_FIELDS?.get(params?.sort as String)
        if (sortableField) {
            sortedField = sortableField?.field
            sortDirection = sortableField?.order
        }

        def sortFields = [[(sortedField): sortDirection]]
        if("relevance"?.equalsIgnoreCase(sortedField) || "az"?.equalsIgnoreCase(params?.sort as String) || "za"?.equalsIgnoreCase(params?.sort as String)) {
            sortFields = []
            if ("relevance"?.equalsIgnoreCase(sortedField)) {
                sortFields.push("_score")
            }
            sortFields = [
                    [
                            "names.name.keyword": [
                                    "order" : sortDirection,
                                    "nested_path" : "names",
                                    "nested_filter" : [
                                            "term" : [ "names.language": lang ]
                                    ]
                            ]
                    ]
            ]
        }

        // Filters
        def filters = []
        filters.push([term: [status: Status.PUBLISHED.value()]])
        filters.push([range: [published_date: [lte: date]]])
        filters.push([term: [languages: lang]])
        if (params?.category || paramsOverride.get("category")) {
            String categorySlug = paramsOverride.get("category") ?: params?.category
            Category category = Category.findBySlug(categorySlug)
            if (category) {
                def categories = []
                categories.add(category.slug)
                category.childrens?.each {
                    categories.add(it.slug)
                }
                filters.push([nested: [path: "categories", query: [bool: [filter: [terms: ["categories.slug": categories]]]]]])
            }
        }
        if (params?.price) {
            Integer priceId = params?.int("price")
            if (priceId != null) {
                def price = Product.PRICE_RANGE.get(priceId)
                if (price) {
                    filters.push([range: [price: [gte: price.low, lte: price.high]]])
                }
            }
        }
        if (params?.productLevel) filters.push([term: [product_level: params?.productLevel]])


        // Aggregations
        def aggs = [:]
        aggs.put("product_levels", [terms: [field: "product_level", size: 10000]])

        def topHitsCategory = [nested_info: [top_hits: [_source: [includes: ["categories.name"]], size: 1]]]
        aggs.put("categories", [nested: [path: "categories"], "aggs": [category_info: [terms: [field: "categories.id", size: 10000], aggs: topHitsCategory]]])


        JsonBuilder builder = new JsonBuilder ()
        def map = builder {
            from offset
            size params?.int("max")
            sort sortFields
            query {
                bool {
                    if (params?.get("q")) {
                        must {
                            multi_match multiMatch
                        }
                    }
                    filter {
                        bool {
                            must (filters)
                        }
                    }
                }
            }
            aggregations aggs
        }

        JestResult jestResult = null
        try {
            String query = builder?.toString()
            JestClient client = this.getESConnection()
            def searchBuilder = new Search.Builder(query).addIndex(indexName)
            searchBuilder.addType(productsTypeName)
            Search search = searchBuilder.build()
            jestResult = client.execute(search)
            if (jestResult?.responseCode == 403) {
                throw new RuntimeException(jestResult?.jsonObject?.get("Message")?.getAsString())
            } else if (jestResult?.responseCode == 400) {
                throw new RuntimeException(jestResult?.jsonObject?.getAsJsonObject("error")?.get("reason")?.getAsString())
            }
        } catch (Exception e) {
            log.error(e?.message, e)
        }
        return jestResult
    }

    def proceedProduct(JsonObject jso, String lang) {
        def content = [:]
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
        String originalLang = getOriginalFilter(lang, ParamType.LANGUAGE.name())

        def source = jso.get("_source")?.getAsJsonObject()
        def type = jso.get("_type")?.getAsString()
        def id = jso.get("_id")?.getAsLong()

        content.id = id
        content.searchType = type

        Long bannerId = source.getAsJsonObject("banner")?.get("id")?.getAsLong()
        String bannerUrl = source.getAsJsonObject("banner")?.get("url")?.getAsString()
        content.banner = [id: bannerId, url: bannerUrl]

        content.description = ""
        source.getAsJsonArray("descriptions")?.each { JsonObject jo ->
            if (jo?.get("language")?.getAsString() == originalLang) {
                String description = jo?.getAsJsonObject()?.get("description")?.getAsString()
                if (StringUtils.isNotBlank(description)) {
                    content.description = description
                }
            }
        }

        source.getAsJsonArray("names")?.each { JsonObject jo ->
            if (jo?.get("language")?.getAsString() == originalLang) {
                content.name = jo?.get("name")?.getAsString()
            }
        }

        content.popular = source.get("popular")?.getAsBoolean()
        content.price = source.get("price")?.getAsDouble()
        content.priceMax = source.get("price_max")?.getAsDouble()
        content.priceMin = source.get("price_min")?.getAsDouble()
        content.productLevel = source.get("product_level")?.getAsString()

        content.publishedDate = null
        try {
            String publishedDate = source.get("published_date")?.getAsString()
            content.publishedDate = sdf.parse(publishedDate)
        } catch (Exception e) {
            log.warn(e?.message)
        }

        content.shareUrl = grailsLinkGenerator.link(absolute: true, controller: "product", action: "show", id: content.slug, params: [country: content.country, site: content.site])
        content.slug = source.get("slug")?.getAsString()

        content.specifications = []
        source.getAsJsonArray("specifications")?.each { JsonObject jo ->
            if (jo.get("important").getAsBoolean()) {
                content.specifications.add([name: jo.get("name")?.getAsString(), unit: jo.get("unit")?.getAsString(), specOrder: jo.get("product_spec_order")?.getAsInt() ?: jo.get("spec_order")?.getAsShort(), specValue: jo.get("spec_value")?.getAsString()])
            }
        }

        if (content.specifications?.size() > 1) {
            content.specifications?.sort{it?.specOrder}
        }

        return content
    }

    def proceedProductAggs(GrailsParameterMap params, JsonObject json) {
        def aggs = [:]

        def aggsLevels = json?.getAsJsonObject("product_levels")?.getAsJsonArray("buckets")
        def levels = [:]
        aggsLevels?.each { JsonObject j ->
            def k = j?.get("key")?.getAsString()
            def c = j?.get("doc_count")?.getAsInt()
            levels.put(k, c)
        }
        def newLevels = [:]
        ProductLevel.values().each {
            if (levels?.containsKey(it.name())) {
                newLevels.put(it.name(), levels.get(it.name()))
            }
        }
        aggs.put("product_levels", newLevels)

        def aggsCategories = json?.getAsJsonObject("categories")?.getAsJsonObject("category_info")?.getAsJsonArray("buckets")
        def categories = [:]
        aggsCategories?.each { JsonObject j ->
            int c = j?.get("doc_count")?.getAsInt()
            def key = j?.getAsJsonObject("nested_info")?.get("key")?.getAsLong()
            def categoryHits = j?.getAsJsonObject("nested_info")?.getAsJsonObject("hits")?.getAsJsonArray("hits")?.get(0)?.getAsJsonObject()?.getAsJsonObject("_source")?.getAsJsonObject("categories")
            String categoryName = categoryHits?.get("name")?.getAsString()
            String slug = categoryHits?.get("slug")?.getAsString()

            if (key != params.long("range")) {
                categories.put(key, [name: categoryName, slug: slug, count: c])
            }
        }
        aggs.put("categories", categories)

        return aggs
    }

    def searchArticles(GrailsParameterMap params, Map<String, Object> paramsOverride) {

        Date date = new Date()
        params.max = Math.min(params?.int("max") ?: 30, 100)
        params?.page = params?.int("page") ?: 1
        params.sort = params?.sort ?: 'az'
        String lang = params?.lang

        Integer offset = (params?.page - 1) * params?.max
        CategoryType categoryType = CategoryType.ARTICLE
        try {
            if (StringUtils.isNotBlank(params?.categoryType)) {
                String categoryTypeName = params?.categoryType?.trim()
                categoryType = CategoryType.valueOf(categoryTypeName?.toUpperCase())
            }
        } catch (Exception e) { log.warn(e?.message) }

        if (StringUtils.isNotBlank(lang)) {
            lang = getOriginalFilter(lang, ParamType.LANGUAGE.name())
        }

        def nestedMultiMatch = [
                "path": "titles",
                "query": [
                        "multi_match": [
                                "query": params.q,
                                "fields": ["titles.title.search^4", "contents.content.search^2"]
                        ]
                ]
        ]

        String sortedField = "names.name.keyword"
        String sortDirection = "asc"
        def sortableField = Product.ES_SORTABLE_FIELDS?.get(params?.sort as String)
        if (sortableField) {
            sortedField = sortableField?.field
            sortDirection = sortableField?.order
        }

        def sortFields = [[(sortedField): sortDirection]]
        if("relevance"?.equalsIgnoreCase(sortedField) || "az"?.equalsIgnoreCase(params?.sort as String) || "za"?.equalsIgnoreCase(params?.sort as String)) {
            sortFields = []
            if ("relevance"?.equalsIgnoreCase(sortedField)) {
                sortFields.push("_score")
            }
            sortFields = [
                    [
                            "titles.title.keyword": [
                                    "order": sortDirection,
                                    "nested_path": "titles",
                                    "nested_filter": [
                                            "term": [ "titles.language": lang ]
                                    ]
                            ]
                    ]
            ]
        }

        // Filters
        def filters = []
        filters.push([terms: [content_type: [ContentType.ARTICLE.name(), ContentType.COURSE.name(), ContentType.EVENT.name(), ContentType.SOLUTION.name()]]])
        filters.push([term: [status: Status.PUBLISHED.name()]])
        filters.push([range: [published_date: [lte: date]]])
        filters.push([term: [languages: lang]])
        if (params?.category || paramsOverride.get("category")) {
            String categorySlug = paramsOverride.get("category") ?: params?.category
            Category category = Category.findBySlugAndCategoryType(categorySlug, categoryType)
            if (category) {
                def categories = []
                categories.add(category.id)
                category.childrens?.each {
                    categories.add(it.id)
                }
                filters.push([nested: [path: "categories", query: [bool: [filter: [terms: ["categories.id": categories]]]]]])
            }
        }
        if (params?.subject) {
            def subjects = params?.list('subject')*.toLong()
            if (subjects && subjects?.size() > 0) {
                filters.push([nested: [path: "subjects", query: [bool: [filter: [terms: ["subjects.id": subjects]]]]]])
            }
        }

        // Aggregations
        def aggs = [:]
        def topHitsCategory = [nested_info: [top_hits: [_source: [includes: ["categories.name"]], size: 1]]]
        aggs.put("categories", [nested: [path: "categories"], "aggs": [category_info: [terms: [field: "categories.id", size: 10000], aggs: topHitsCategory]]])
        def topHitsTopics = [nested_info: [top_hits: [_source: [includes: ["subjects.id", "subjects.name"]], size: 1]]]
        aggs.put("subjects", [nested: [path: "subjects"], "aggs": [subject_info: [terms: [field: "subjects.id", size: 10000], aggs: topHitsTopics]]])


        JsonBuilder builder = new JsonBuilder ()
        def map = builder {
            from offset
            size params?.int("max")
            sort sortFields
            query {
                bool {
                    if (params?.get("q")) {
                        must {
                            nested nestedMultiMatch
                        }
                    }
                    filter {
                        bool {
                            must (filters)
                        }
                    }
                }
            }
            aggregations aggs
        }

        JestResult jestResult = null
        try {
            String query = builder?.toString()
            JestClient client = this.getESConnection()
            def searchBuilder = new Search.Builder(query).addIndex(indexName)
            searchBuilder.addType(articleTypeName)
            Search search = searchBuilder.build()
            jestResult = client.execute(search)
            if (jestResult?.responseCode == 403) {
                throw new RuntimeException(jestResult?.jsonObject?.get("Message")?.getAsString())
            } else if (jestResult?.responseCode == 400) {
                throw new RuntimeException(jestResult?.jsonObject?.getAsJsonObject("error")?.get("reason")?.getAsString())
            }
        } catch (Exception e) {
            log.error(e?.message, e)
        }
        return jestResult
    }

    def proceedArticle(JsonObject jso, String lang) {
        def content = [:]
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")

        def source = jso.get("_source")?.getAsJsonObject()
        def type = jso.get("_type")?.getAsString()
        def id = jso.get("_id")?.getAsLong()

        content.id = id
        content.searchType = type

        Long bannerId = source.getAsJsonObject("banner")?.get("id")?.getAsLong()
        String bannerUrl = source.getAsJsonObject("banner")?.get("url")?.getAsString()
        String bannerCopyright = source.getAsJsonObject("banner")?.get("copyright")?.getAsString()
        content.banner = [copyright: bannerCopyright, mediaId: bannerId, url: bannerUrl]

        ContentType contentType = ContentType.ARTICLE
        if (source.get("contentType")?.getAsString()) {
            try {
                contentType = ContentType.valueOf(source.get("contentType")?.getAsString())
            } catch (Exception e) { log.warn(e?.message) }
        }
        content.contentType = contentType

        content.shotDescription = ""
        source.getAsJsonArray("short_descriptions")?.each { JsonObject jo ->
            if (jo?.get("language")?.getAsString()?.equalsIgnoreCase(lang)) {
                String description = jo?.getAsJsonObject()?.get("content")?.getAsString()
                if (StringUtils.isNotBlank(description)) {
                    content.shortDescription = description
                }
            }
        }

        source.getAsJsonArray("titles")?.each { JsonObject jo ->
            if (jo?.get("language")?.getAsString()?.equalsIgnoreCase(lang)) {
                content.title = jo?.get("title")?.getAsString()
            }
        }

        content.publishedDate = null
        try {
            String publishedDate = source.get("published_date")?.getAsString()
            content.publishedDate = sdf.parse(publishedDate)
        } catch (Exception e) {
            log.warn(e?.message)
        }

        content.slug = source.get("slug")?.getAsString()

        return content
    }

    def proceedArticleAggs(GrailsParameterMap params, JsonObject json) {
        def aggs = [:]

        def aggsCategories = json?.getAsJsonObject("categories")?.getAsJsonObject("category_info")?.getAsJsonArray("buckets")
        def categories = [:]
        aggsCategories?.each { JsonObject j ->
            int c = j?.get("doc_count")?.getAsInt()
            def key = j?.get("key")?.getAsLong()
            def hits = j?.getAsJsonObject("nested_info")?.getAsJsonObject("hits")?.getAsJsonArray("hits")?.get(0)?.getAsJsonObject()?.getAsJsonObject("_source")?.getAsJsonObject("categories")
            String name = hits?.get("name")?.getAsString()
            String slug = hits?.get("slug")?.getAsString()
            categories.put(key, [name: name, slug: slug, count: c])
        }
        aggs.put("categories", categories)

        def aggsSubjects = json?.getAsJsonObject("subjects")?.getAsJsonObject("subject_info")?.getAsJsonArray("buckets")
        def subjects = [:]
        aggsSubjects?.each { JsonObject j ->
            int c = j?.get("doc_count")?.getAsInt()
            def key = j?.get("key")?.getAsLong()
            def hits = j?.getAsJsonObject("nested_info")?.getAsJsonObject("hits")?.getAsJsonArray("hits")?.get(0)?.getAsJsonObject()?.getAsJsonObject("_source")?.getAsJsonObject("subjects")
            Long id = hits?.get("id")?.getAsLong()
            String name = hits?.get("name")?.getAsString()
            subjects.put(key, [id: id, name: name, count: c])
        }
        subjects = subjects?.sort { a, b ->
            a.value.name <=> b.value.name
        }
        aggs.put("subjects", subjects)

        return aggs
    }

    public String getOriginalFilter(String search, String paramTypeName) {
        return getOriginalFilter(search, paramTypeName, "")
    }

    @Cacheable('paramsLookupOriginal')
    public String getOriginalFilter(String search, String paramTypeName, String lang) {
        ParamType paramType = ParamType.valueOf(paramTypeName)
        if (paramType) {
            if (paramType == ParamType.LANGUAGE){
                Language language = Language.findById(search)
                if (language) {
                    search = language.id
                }
            }
        }

        return search
    }

    public JestClient getESConnection() {
        if (jestClient == null) {
            log.info("Initializing connection to elasticsearch server...")
            Set<String> servers = getESServers()
            servers?.each {
                log.info("* " + it)
            }
            HttpClientConfig clientConfig = new HttpClientConfig.Builder(servers)
                    .multiThreaded(true)
                    .readTimeout(10000)
                    .build()

            JestClientFactory factory = new JestClientFactory()
            factory.setHttpClientConfig(clientConfig)
            jestClient = factory.getObject()
        }
        return jestClient
    }

    private Set<String> getESServers() {
        String [] serverListArray = serverList.get(0).split(",")
        Set<String> servers = new LinkedHashSet<String>()
        for (int i = 0; i < serverListArray.length; i++) {
            servers.add(serverListArray[i])
        }

        return servers
    }
}
