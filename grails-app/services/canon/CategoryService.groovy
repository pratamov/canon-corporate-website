package canon

import canon.enums.CategoryType
import canon.enums.Segment
import canon.enums.Status
import grails.plugin.cache.Cacheable
import grails.transaction.Transactional
import org.hibernate.sql.JoinType

@Transactional
class CategoryService {

    /**
     * Moved  to domain method
     * @param category
     * @return
     */
    @Deprecated
    CategoryMedia getFirstMediaByCategory(Category category) {
        CategoryMedia mediaResult = CategoryMedia.createCriteria().get() {
            eq("category", category)
            maxResults(1)
            order("mediaOrder")
        }

        return mediaResult
    }

    List<Category> getMainProductCategory (Site site, Language lang, Segment segment) {
        List<Category> result = Category.createCriteria().list(sort: "categoryOrder") {
            createAlias('categoryTranslations', 'ct', JoinType.LEFT_OUTER_JOIN)
            eq("site", site)
            eq("segment", segment)
            eq("categoryType", CategoryType.PRODUCT)
            or {
                eq('language', lang)
                eq('ct.language', lang)
            }
            isNull("parent")
            eq("status", Status.ACTIVE)
        }

        return result
    }

    @Cacheable("subProductCategory")
    List<Category> getSubProductCategory(String categorySlug) {

        List<Category> result = Category.createCriteria().list(sort: "categoryOrder") {
            createAlias("parent", "p")
            eq("p.slug", categorySlug)
            eq("status", Status.ACTIVE)
        }

        return result
    }
}
