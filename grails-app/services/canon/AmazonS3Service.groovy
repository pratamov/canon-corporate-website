package canon

import canon.utils.FileUtils
import com.amazonaws.auth.AWSCredentials
import com.amazonaws.auth.BasicAWSCredentials
import com.amazonaws.services.s3.AmazonS3
import com.amazonaws.services.s3.AmazonS3Client
import com.amazonaws.services.s3.model.*
import grails.transaction.Transactional
import org.apache.commons.io.FilenameUtils
import org.apache.commons.lang.StringUtils
import org.springframework.beans.factory.annotation.Value

@Transactional
class AmazonS3Service {

    @Value('${amazonaws.s3.bucketName}')
    String bucketName

    @Value('${amazonaws.s3.accessKeyId}')
    String accessKeyId

    @Value('${amazonaws.s3.secretAccessKey}')
    String secretAccessKey

    @Value('${amazonaws.s3.awsRegion}')
    String awsRegion

    @Value('${amazonaws.s3.baseUrl}')
    String baseUrl

    @Value('${amazonaws.s3.header.cacheControl}')
    String cacheControl

    Map mimeExtensionMap = [
            "png" : "image/png",
            "jpg": "image/jpeg",
            "gif": "image/gif",
            "tiff": "image/tiff",
            "pdf": "application/pdf",
            "mpeg": "video/mpeg",
            "mp4": "video/mp4",
            "mov": "video/quicktime",
            "wmv": "video/x-ms-wmv",
            "html": "text/html",
            "xml": "text/xml",
            "mp3": "audio/mpeg",
            "flv": "application/octet-stream"
    ]

    String uploadFile(FileInputStream fileInputStream, String fileName, long fileSize) {

        ObjectMetadata objectMetadata = new ObjectMetadata();
        objectMetadata.setContentLength(fileSize);
        if (StringUtils.isNotBlank(cacheControl)) {
            objectMetadata.setCacheControl(cacheControl);
        }

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        org.apache.commons.io.IOUtils.copy(fileInputStream, baos);
        byte[] bytes = baos.toByteArray();
        ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
        String contentType = FileUtils.contentType(bais);
        if (StringUtils.isNotBlank(contentType)) {
            objectMetadata.setContentType(contentType);
        }

        PutObjectRequest por = null;
        try {
            bais.reset();
            por = new PutObjectRequest(this.bucketName,
                    fileName, bais, objectMetadata);
            por.setCannedAcl(CannedAccessControlList.PublicRead);
            AmazonS3 s3Client = new AmazonS3Client(
                    new BasicAWSCredentials(accessKeyId, secretAccessKey));
            s3Client.putObject(por);
            String path = FilenameUtils.getFullPath(fileName);
            String baseFileName = URLEncoder.encode(FilenameUtils.getBaseName(fileName), "UTF-8");
            String ext = FilenameUtils.getExtension(fileName);
            return baseUrl + "/" + path + baseFileName + "." + ext;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (fileInputStream != null){
                fileInputStream.close();
            }
            if (baos != null) {
                baos.close();
            }
            if (bais != null) {
                bais.close();
            }
        }

        return null;
    }

    boolean isExist(String file) {

        // Amazon-s3 credentials
        AWSCredentials myCredentials = new BasicAWSCredentials(accessKeyId, secretAccessKey);
        AmazonS3Client s3Client = new AmazonS3Client(myCredentials);

        ObjectListing objects = s3Client.listObjects(new ListObjectsRequest().withBucketName(bucketName).withPrefix(file));

        for (S3ObjectSummary objectSummary: objects.getObjectSummaries()) {
            if (objectSummary.getKey().equals(file)) {
                return true;
            }
        }
        return false;
    }

    def deleteFile(String filePath) {

        AmazonS3 s3Client = new AmazonS3Client(new BasicAWSCredentials(accessKeyId, secretAccessKey));
        try {
            s3Client.deleteObject(bucketName, URLDecoder.decode(FilenameUtils.getName(filePath), "UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace()
        }
    }

    def getFileList(String prefix) {

        def files = []
        if (StringUtils.isNotBlank(prefix)) {
            // Amazon-s3 credentials
            AWSCredentials myCredentials = new BasicAWSCredentials(accessKeyId, secretAccessKey)
            AmazonS3Client s3Client = new AmazonS3Client(myCredentials)

            ObjectListing objects = s3Client.listObjects(new ListObjectsRequest().withBucketName(bucketName).withPrefix(prefix))

            for (S3ObjectSummary objectSummary: objects.getObjectSummaries()) {

                String path = FilenameUtils.getFullPath(objectSummary.key)
                String baseFileName = FilenameUtils.getBaseName(objectSummary.key)
                String ext = FilenameUtils.getExtension(objectSummary.key)

                def newFolderList = []
                def folderList = path?.split("/")
                folderList.collect { it ->
                    newFolderList.push(URLEncoder.encode(it, "UTF-8"))
                }
                String encodedPath = newFolderList.join("/")
//                files.push(encodedPath + '/' + URLEncoder.encode(baseFileName, "UTF-8") + "." + ext)

                def media = [
                        mediaName: baseFileName,
                        mediaUrl: encodedPath + '/' + URLEncoder.encode(baseFileName, "UTF-8") + "." + ext,
                        mediaType: mimeExtensionMap.get(ext)
                ]
                files.push(media)
            }
        }
        return files?.drop(1)
    }

    String uploadRestaurantUserMedia(FileInputStream fileInputStream, String fileName, long fileSize) {

        ObjectMetadata objectMetadata = new ObjectMetadata()
        objectMetadata.setContentLength(fileSize)

        PutObjectRequest por
        try {
            por = new PutObjectRequest(this.bucketName, fileName, fileInputStream, objectMetadata)
            por.setCannedAcl(CannedAccessControlList.PublicRead)
            AmazonS3 s3Client = new AmazonS3Client(new BasicAWSCredentials(this.accessKeyId, this.secretAccessKey))
            s3Client.putObject(por)

            return this.baseUrl + "/" + fileName
        } catch (Exception e) {
            e.printStackTrace()
        }

        return null
    }

}
