package canon

import grails.transaction.Transactional

@Transactional
class HomeService {

    /**
     * Moved into product service
     *
     * @param lang
     * @return
     */
    @Deprecated
    def getFeatureProduct(Language lang) {

        List<Product> products = Product.createCriteria().list {
            and {
                eq('language', lang)
            }
            order('publishedDate', 'desc')
            maxResults(4)
        }



        return products

    }
}
