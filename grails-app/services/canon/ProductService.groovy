package canon

import canon.enums.RelationType
import canon.enums.Segment
import canon.enums.Status
import canon.model.comp.HeroComp
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import grails.transaction.Transactional
import grails.web.servlet.mvc.GrailsParameterMap

@Transactional
class ProductService {

    def elasticSearchService

    List<Product> getProductByCategory(GrailsParameterMap params) {
        Category mainCategory = Category.findBySlug(params?.id)
        Category subCategory = Category.findBySlugAndParent(params?.subCategory, mainCategory)



        return new ArrayList<Product>()
    }

    def searchProducts(GrailsParameterMap params, Map<String, Object> paramsOverride){
        def resultList = []
        int resultCount = 0
        def aggs = [:]
        try{
            def esResult = elasticSearchService.searchProducts(params, paramsOverride)
            if(esResult) {
                JsonObject json = esResult.getJsonObject()
                resultCount = json.getAsJsonObject("hits")?.get("total")?.getAsInt() ?: 0

                JsonArray hitsArray = json.getAsJsonObject("hits")?.getAsJsonArray("hits")
                hitsArray.each { JsonObject jso ->
                    def product = elasticSearchService.proceedProduct(jso, params?.lang)
                    if (product) resultList.add(product)
                }

                def foundAggs = json.get("aggregations")?.getAsJsonObject()
                aggs = elasticSearchService.proceedProductAggs(params, foundAggs)
            }
        }catch (Exception e) {
            log.error(e?.message, e)
        }

        return [
                productCount: resultCount,
                productList: resultList,
                aggs: aggs
        ]
    }

    def generateProductSpec(Set<ProductSpecification> productSpecificationList) {

        def productSpecifications = []

        if (productSpecificationList) {

            productSpecificationList.each { 
                def specGroup = productSpecifications?.find { ps -> ps.name.equals(it.specification.group.name) }

                if (specGroup) {
                    specGroup.specList << [name: it.specification.name, value: it.specValue, specOrder: it.specification.specOrder]
                } else {
                    def specList = [[name: it.specification.name, value: it.specValue, specOrder: it.specification.specOrder]]
                    
                    productSpecifications << [name: it.specification.group.name, groupOrder: it.specification.group.groupOrder, specList: specList]
                }
            }
            
            productSpecifications.sort { it.groupOrder }
        
            productSpecifications.each { 
                it.specList.sort { spec -> spec.specOrder }
            }
        }
        
        return productSpecifications
    }

    /**
     * @see Product#getDefaultMedia()
     * @param product
     * @return
     */
    @Deprecated
    ProductMedia getFirstMediaByProduct(Product product){
        ProductMedia mediaResult = ProductMedia.createCriteria().get() {
            eq("product", product)
            maxResults(1)
            order("mediaOrder")
        }

        return mediaResult
    }

    List<Product> getFeaturedProducts(String siteCode, String categorySlug, String segmentSlug) {
        Date currentDate = new Date()
        Site site = Site.findByCode(siteCode)
        Category category = Category.findBySlug(categorySlug)
        Segment segment = Segment.CONSUMER
        if (segmentSlug) {
            try {
                segment = Segment.getBySlug(segmentSlug)
            } catch (Exception e) {}
        }

        List<Product> products = Product.createCriteria().list(max:4) {
            createAlias("productDetails", "ps")
            createAlias("productCategories", "pc")
            createAlias("pc.category", "c")
            and {
                eq("ps.site", site)
                eq("isFeatured", Boolean.TRUE)
                le("featuredStartDate", currentDate)
                ge("featuredEndDate", currentDate)
                if (category){
                    eq("pc.category", category)
                }
                eq("c.segment", segment)
            }
            order("ps.publishedDate", "desc")
        }
        return products
    }

    List<Award> getProductAwards(Product product) {
        return ProductAward.createCriteria().list {
            projections{
                property("award")
            }
            eq("product", product)
        }
    }

    def getProductRelations(Product product){
        def relations = [:]

        RelationType.values().each {RelationType rt ->
            relations.put(rt, getProductRelationsOverview(product, rt))
        }

        return relations
    }

    List<Feature> getProductFeatures(Product product){
        return ProductFeature.createCriteria().list(sort: "featureOrder") {
            projections {
                property("feature")
            }
            eq("product", product)
            eq("status", Status.ACTIVE)
        }
    }

    List<ProductComponent> getPublishedProductComponents(Product product){
        List<ProductComponent> productComponentList = ProductComponent.createCriteria().list {
            and {
                eq("product", product)
                eq("status", Status.PUBLISHED)
            }
            order("component.compOrder", "asc")
        }

        return productComponentList
    }

    List<Product> getProductRelation(Product product, RelationType type){
        return ProductRelation.createCriteria().list {
            projections{
                property("relation")
            }
            eq("product", product)
            eq("relationType", type)

            order("lastUpdated", "desc")
        }
    }

    List<Product> getProductRelationsOverview(Product product, RelationType type){
        return ProductRelation.createCriteria().list {
            projections{
                property("relation")
            }
            eq("product", product)
            eq("relationType", type)
            maxResults(3)
            order("lastUpdated", "desc")
        }
    }

    def getProductRelatedReads(Product product) {
        def result = []

        List<Page> articles = PageProduct.createCriteria().list(max: 3) {
            createAlias("page", "p")
            projections {
                property("page")
            }
            eq("product", product)
            eq("p.isHomepage", Boolean.FALSE)
        }

        articles?.each {Page p ->
            def article = [:]

            article.put("id", p.id)
            article.put("slug", p.slug)
            article.put("title", p.headline)

            String articleTag = PageTag.createCriteria().get {
                projections{
                    property("tag")
                }
                eq("page", p)
                maxResults(1)
            }
            if (articleTag) {
                article.put("tagName", articleTag)
            }

            HeroComp articleHero = p.getFirstHeroComponent()?.component?.getAsComponent() as HeroComp
            if (articleHero) {
                article.put("bgId", articleHero?.mediaId)
                article.put("bgUrl", articleHero?.imageUrl)
            }

            result.add(article)
        }

        return result
    }
}
