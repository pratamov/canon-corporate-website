package canon

import grails.transaction.Transactional

import java.text.SimpleDateFormat
import grails.web.servlet.mvc.GrailsParameterMap
import canon.utils.SlugCodec

@Transactional
class AdminPageService {

    def savePage(GrailsParameterMap params, Page page) {
        SimpleDateFormat sdf = new SimpleDateFormat("d MMM yyyy")


        //Set product attribute value
        page.slug = SlugCodec.encode(page.headline)
        /*
        Date avDate = null
        Date dcDate = null
        try{
            avDate = sdf.parse(params?.availabilityDateString)
            dcDate = sdf.parse(params?.discontinuedDateString)

        }catch (Exception e) {
            e.printStackTrace()
        }

        page.availabilityDate = avDate
        page.discontinuedDate = dcDate
        */

        def tags = params?.list('tags')
        tags?.eachWithIndex { tag ->
            page.addToPageTags(new PageTag(tag: tag))
        }

        def topics = params?.list('topics')*.toLong()
        topics?.eachWithIndex { id, idx ->
            Subject subject = Subject.findById(id)
            page.addToPageSubjects(new PageSubject(subject: subject))
        }

        //Product location
        def locationSiteIds = params?.list('locationSiteId')*.toLong()
        locationSiteIds?.eachWithIndex{id, idx ->
            Site site = Site.findById(id)
            Category category = Category.findById(params?.list('locationCategoryId')[idx]?.toLong())

            Date publishedDate = params?.list('locationPublishedDate')[idx] ? sdf.parse(params?.list('locationPublishedDate')[idx]) : null
            Date archivedDate = params?.list('locationArchivedDate')[idx] ? sdf.parse(params?.list('locationArchivedDate')[idx]) : null

            page.addToPageDetails(new PageDetail(site: site, publishedDate: publishedDate, archivedDate:  archivedDate))
            page.addToPageCategories(new PageCategory(category: category))
        }

        page.validate()
        if (page.hasErrors()){
            return page
        } else {
            return page.save(failOnError: true)
        }
    }
}
