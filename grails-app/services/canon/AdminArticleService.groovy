package canon

import canon.enums.Status
import canon.enums.ContentType
import grails.transaction.Transactional
import grails.web.servlet.mvc.GrailsParameterMap

@Transactional
class AdminArticleService {

    def getProductList(GrailsParameterMap params, def paramsOverride) {
        Integer max = Math.min(params?.int("max", 20), 30)
        Integer page = params?.int("page", 1)
        Integer offset = (page - 1) * max

        def paramsMap = [:]
        params.each {
            paramsMap.put(it.key, it.value)
        }

        if (paramsOverride) {
            paramsOverride?.each {
                paramsMap[it.key] = it.value
            }
        }

        List<Status> statusList = paramsMap.statusList
        List<ContentType> typeList = paramsMap.typeList

        ContentType type = paramsMap.type? typeList.find { it.name().equals(paramsMap.type) } : null
        Status status = paramsMap.type? statusList.find { it.name().equals(paramsMap.status) } : Status.PUBLISHED
        
        List<Page> pageList = Page.createCriteria().list(max:max, offset:offset) {
            createAlias("pageDetails", "ps")
            createAlias("ps.site", "s")
            and {
                if (type) {
                    eq("contentType", type)
                } else {
                    or {
                        typeList.each {
                            eq("contentType", it)
                        }
                    }
                }
                
                if (status) eq("status", status)
                if (paramsMap.dateFrom) ge("publishedDate", paramsMap.dateFrom)
                if (paramsMap.dateTo) le("publishedDate", paramsMap.dateTo)
                if (paramsMap.q) ilike("headline","%$paramsMap.q%")
                if (paramsMap.site) eq("s.id", Long.valueOf(paramsMap.site as String))
            }
        }
        
        def pageListClone = []

        pageList.each {
            def pages = [:]
            pages.title = it.headline
            pages.author = (it.createdBy?.profile?.firstName?:"") + " " + (it.createdBy?.profile?.lastName?:"")
            pages.publishedDate = it.publishedDate
           
            def categories = ""
            it.pageCategories.each { pc ->
                def name = pc.category?.name
                categories += name? name + ", ":""
            }
            pages.categories = categories? categories[0..-3] : ""

            def locations = ""
            it.pageDetails.each { ps ->
                def name = ps.site?.name
                locations += name? name + ", ":""
            }
            pages.locations = locations? locations[0..-3] : ""

            pageListClone << pages
        }

        return pageListClone
    }

    def countBy(String by, GrailsParameterMap params, def paramsOverride) {
        def paramsMap = [:]
        params.each {
            paramsMap.put(it.key, it.value)
        }

        if (paramsOverride) {
            paramsOverride?.each {
                paramsMap[it.key] = it.value
            }
        }

        List<Status> statusList = paramsMap.statusList
        List<ContentType> typeList = paramsMap.typeList

        ContentType type = paramsMap.type? typeList.find { it.name().equals(paramsMap.type) } : null

        def result = Page.createCriteria().list {
            createAlias("pageDetails", "ps")
            createAlias("ps.site", "s")
            and {
                if (by.equals("status") && type) {
                    eq("contentType", type)
                } else {
                    or {
                        typeList.each {
                            eq("contentType", it)
                        }
                    }
                }

                or {
                    statusList.each {
                        eq("status", it)
                    }
                }
                
                if (paramsMap.dateFrom) ge("publishedDate", paramsMap.dateFrom)
                if (paramsMap.dateTo) le("publishedDate", paramsMap.dateTo)
                if (paramsMap.q) ilike("headline","%$paramsMap.q%")
                if (paramsMap.site) eq("s.id", Long.valueOf(paramsMap.site as String))
            }
            projections {
                if (by.equals("status")) {
                    groupProperty("status")
                } else {
                    groupProperty("contentType")
                }

                count()
            }
        }

        def resultMap = result.collectEntries { [(it[0]): it[1]]}

        return resultMap
    }
}