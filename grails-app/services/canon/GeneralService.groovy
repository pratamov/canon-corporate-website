package canon

import canon.enums.MenuLocation
import canon.enums.MenuType
import canon.enums.Segment
import canon.enums.Status
import grails.plugin.cache.Cacheable
import grails.transaction.Transactional
import org.apache.commons.lang.StringUtils

@Transactional
class GeneralService {

    @Cacheable('generalAllSites')
    public List<Site> findAllSites(){
        List<Site> sites = Site.createCriteria().list {
            eq("status", Status.ACTIVE)
            order("name", "asc")
        }
        sites?.each {
            it?.country
            it?.siteLanguages?.each { SiteLanguage sl ->
                sl.id
            }
            it.defaultLangCode = it.getDefaultLanguage()?.id
        }

        return sites
    }

    @Cacheable('generalLanguages')
    public List<Language> findAllLanguages(){
        List<Language> languages = Language.createCriteria().list {
            eq("status", Status.ACTIVE)
            order("name", "asc")
        }

        return languages
    }

//    @Cacheable('currencies')
//    public List<String> getCurrencies(String siteCode) {
//        if (StringUtils.isBlank(siteCode)) {
//            return new ArrayList<>()
//        }
//
//        if (StringUtils.isNotBlank(siteCode)) {
//            List<String> currencyList = SiteCurrency.createCriteria().list {
//                projections {
//                    property("currency")
//                }
//                createAlias("site", "s")
//                eq("s.code", siteCode)
//                order("currency", "asc")
//            }
//            return currencyList
//        }
//    }

    @Cacheable('siteOfCountry')
    public Site getSiteOfCountry(String countryCode) {
        if (StringUtils.isBlank(countryCode)) return null

        Site site = Site.createCriteria().get {
            and {
                eq("isCountryLevel", true)
                country {
                    eq("code", countryCode)
                }
            }
            maxResults(1)
        }

        if (site) {
            site.defaultLanguage
            site.defaultLangCode = site?.defaultLanguage?.id
        }

        return site
    }

    @Cacheable('siteLanguages')
    public List<Language> getSiteLanguages(String siteCode) {
        List<Language> languages = new ArrayList<>()

        if (StringUtils.isNotBlank(siteCode)) {
            def siteLanguages = SiteLanguage.createCriteria().list {
                createAlias("site", "s")
                //eq("isEnabled", true)
                eq("s.code", siteCode)
            }

            siteLanguages?.each { SiteLanguage sl ->
                languages.push(sl.language)
            }
        }
        return languages
    }

    @Cacheable('appSettings')
    def getAppSettings(String lang, String siteCode) {

        def settings = [:]
        def language = Language.findByIdAndStatus(lang ?: 'en', Status.INACTIVE)
        if (language == null) {
            language = Language.findByStatus(Status.INACTIVE)
        }

        def apps = Setting.createCriteria().list() {
            and {
                if (language != null) {
                    eq("lang", language)
                }
                eq("status", Status.ACTIVE)
            }
        }
        apps?.each { Setting setting ->
            settings.put(setting?.settingCode, setting)
        }

        /* Search settings in active city and override the global configs */
        if (StringUtils.isNotBlank(siteCode)) {
            Site site = Site.findByCode(siteCode)
            if (site) {
                def appsCity = Setting.createCriteria().list() {
                    and {
                        if (language != null) {
                            eq("language", language)
                        }
                        eq("status", Status.ACTIVE)
                        eq("site", site)
                    }
                }
                appsCity?.each { Setting setting ->
                    settings.put(setting?.settingCode, setting)
                }
            }
        }
        return settings
    }

    public Map<String, Site> getSitesMap() {
        List<Site> sites = findAllSites()
        Map<String, Site> map = new HashMap<>()
        sites?.each {
            map.put(it.code?.toLowerCase(), it)
        }
        return map
    }

    public Map<String, Language> getLanguagesMap() {
        List<Language> languages = findAllLanguages()
        Map<String, Language> map = new HashMap<>()
        languages?.each {
            map.put(it.id?.toLowerCase(), it)
        }
        return map
    }

    @Cacheable("headerMenu")
    def getHeaderMenu(String siteCode, String langId, String segmentSlug) {
        Site site = Site.findByCode(siteCode)
        Language lang = Language.findById(langId)
        Segment segment = Segment.getBySlug(segmentSlug)

        List<Menu> menus = Menu.createCriteria().list(sort: "orderNo") {
            eq("site", site)
            eq("language", lang)
            eq("segment", segment)
            eq("status", Status.PUBLISHED)
        }

        List<Menu> rightMenus = []
        List<Menu> leftMenus = []
        menus?.each {Menu m ->
            if (m.location == MenuLocation.HEADER_RIGHT) {
                rightMenus.add(m)
            } else if (m.location == MenuLocation.HEADER_LEFT) {
                leftMenus.add(m)
            }
        }

        return [
                "rightMenus": rightMenus,
                "leftMenus": leftMenus,
        ]
    }
}
