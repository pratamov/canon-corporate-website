package canon

import grails.transaction.Transactional

@Transactional
class AdminUserService {

    def serviceMethod() {

    }

    def findAllUser() {
        def users = User.createCriteria().list(sort: "username") {
            projections {
                property ("id")
                property ("username")
            }
        }

        def result = []
        users?.each {
            result.push([
                    id: it[0],
                    name: it[1]
            ])
        }

        return result;
    }
}
