package canon

import grails.converters.JSON
import grails.transaction.Transactional

@Transactional
class AdminFlowService {

    def serviceMethod() {

    }

    def getWorkflowByFlow(Flow flow){
        List<WorkFlow> workFlows = WorkFlow.createCriteria().list(sort: "flowOrder") {
            projections{
                property ("id")
                property ("name")
                property ("isStart")
                property ("isEnd")
                property ("flowOrder")
            }
            eq("flow", flow)
        }

        def result = []
        workFlows?.each {
            result.push([
                    id: it[0],
                    name: it[1],
                    isStart: it[2],
                    isEnd: it[3],
                    order: it[4]
            ])
        }

        return result
    }
}
