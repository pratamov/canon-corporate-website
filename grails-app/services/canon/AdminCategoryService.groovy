package canon

import canon.enums.CategoryType
import canon.enums.Segment
import grails.gorm.DetachedCriteria
import grails.transaction.Transactional
import org.hibernate.criterion.Projections
import org.hibernate.criterion.Subqueries
import org.hibernate.sql.JoinType

@Transactional
class AdminCategoryService {

    def getLowestLevelCategories(Segment segment) {

        def lowestCategories = Category.createCriteria().list(sort: "name") {
            createAlias("parent", "p")
            projections{
                property("id")
                property("name")
                property("p.name")
            }

            eq("segment", segment)
            isEmpty("childrens")
        }

        return lowestCategories
    }

    def findProductCategoriesBySite(Long siteId){
        def mainCategories = Category.createCriteria().list {
            createAlias("site", "s")
            projections{
                property("id")
                property("name")
                property("segment")
            }
            eq("s.id", siteId)
            eq("categoryType", CategoryType.PRODUCT)
            isNull("parent")
        }

        def categories = []
        mainCategories?.each {
            categories.add(getCategoryTree(it as Object[], new HashMap<String, Object>()))
        }

        def consumerCategories = []
        def businessCategories = []
        categories?.each {
            if (it?.segment == Segment.CONSUMER) {
                consumerCategories.add(it)
            } else {
                businessCategories.add(it)
            }
        }

        return [
                consumer : consumerCategories,
                business : businessCategories
        ]
    }

    private def getCategoryTree(Object[] obj, Map<String, Object> categories){
        Long categoryId = obj[0] as Long
        String categoryName = obj[1]
        Segment categorySegment = obj[2] as Segment
        categories.put("id", categoryId)
        categories.put("name", categoryName)
        categories.put("segment", categorySegment)
        categories.put("children", [])

        def categoryChildrenCount = Category.createCriteria().list {
            createAlias("parent", "p")
            projections {
                count("id")
            }
            eq("p.id", categoryId)
        }
        if (categoryChildrenCount[0] > 0) {
            def children = Category.createCriteria().list {
                createAlias("parent", "p")
                projections{
                    property("id")
                    property("name")
                    property("segment")
                }
                eq("p.id", categoryId)
            }

            children?.each {
                categories?.get("children")?.add(getCategoryTree(it as Object[], new HashMap<String, Object>()))
            }
        }

        return categories
    }

    def getLowestLevelCategories(Segment segment,def categoryTypes) {

        def lowestCategories = Category.createCriteria().list(sort: "name") {
            //createAlias("parent", "p")
            projections{
                property("id")
                property("name")
                //property("p.name")
            }

            inList("categoryType",categoryTypes)
            eq("segment", segment)
            isEmpty("childrens")
        }

        return lowestCategories
    }

    def getLowestLevelCategories(Site site, CategoryType type) {

        def lowestCategories = Category.createCriteria().list(sort: "name") {
            createAlias("parent", "p", JoinType.LEFT_OUTER_JOIN)
            createAlias("p.parent", "pp", JoinType.LEFT_OUTER_JOIN)
            projections{
                distinct(["id", "name", "segment","p.name", "pp.name"])
            }
            eq("site", site)
            eq("categoryType", type)
            isEmpty("childrens")
        }

        def result = []
        lowestCategories?.each {
            String name = (it[2] as Segment).value() + " > "

            name += it[4] ? it[4] + " > " : ""
            name += it[3] ? it[3] + " > " : ""
            name += it[1]

            result.push([
                    id: it[0],
                    name: name,
                    segment: it[2]
            ])
        }

        Collections.sort(result, new Comparator() {
            @Override
            int compare(Object o1, Object o2) {
                return (o1.name as String).compareTo(o2.name)
            }
        })

        return result
    }
}
