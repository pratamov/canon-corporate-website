package canon

import grails.transaction.Transactional
import grails.web.servlet.mvc.GrailsParameterMap

@Transactional
class AdminFaqService {

	def getSites(){

		def sites = Site.findAll()
		return sites

	}

	def getFaqs(Long siteId, boolean archived = false){
        Language en = Language.findById("en")
        Language zh_CN = Language.findById("zh_CN")

		def ret = []

        def site = Site.findById(siteId)
        def faqs = Faq.findAllBySite(site)

        for (faq in faqs){

        	if ((archived && (faq.archivedDate != null && faq.archivedDate.before(new Date()))) || 
        		(!archived && (faq.archivedDate == null || faq.archivedDate.after(new Date())))){
        		def faqTranslation = FaqTranslation.findByFaqAndLanguage(faq, zh_CN)

	        	ret << [
	        		english: faq,
	        		chinese: faqTranslation
	        	]
        	}

        }

        return ret
	}

	def getFaq(Long faqId){
        Language en = Language.findById("en")
        Language zh_CN = Language.findById("zh_CN")

		def faq = Faq.findById(faqId)
		def faqTranslation = FaqTranslation.findByFaqAndLanguage(faq, zh_CN)

		return [
			english: faq,
			chinese: faqTranslation
		]
	}

	def getFaqItems(Long faqId){
        Language en = Language.findById("en")
        Language zh_CN = Language.findById("zh_CN")

		def ret = []

		def faq = Faq.findById(faqId)

		def faqItems = FaqItem.findAllByFaq(faq)
		
		Date time = new Date()

		for (faqItem in faqItems){
			if (faqItem.archivedDate != null && faqItem.archivedDate.before(time)) {}
			else {

				def faqItemTranslation = FaqItemTranslation.findByFaqItemAndLanguage(faqItem, zh_CN)
				def faqItemAnswer = FaqItemAnswer.findByFaqItem(faqItem)
				def faqItemAnswerTranslation = FaqItemAnswer.findByFaqItemAndLanguage(faqItem, zh_CN)

				ret << [
					question: [
						english: faqItem,
						chinese: faqItemTranslation
					],
					answer: [
						english: faqItemAnswer,
						chinese: faqItemAnswerTranslation
					]
				]

			}

		}

		return ret

	}

	def getFaqItemLabels(Long faqId) {

		def faq = Faq.findById(faqId)

		def labels = FaqItem.withCriteria {
			or {
				isNull("archivedDate")
				gt("archivedDate", new Date())
			}
            eq("faq", faq)
            projections {
                distinct("label")
            }
        }

        return labels
	}

	def getUser(){

		User user = springSecurityService.getCurrentUser()
		return user
		
	}




	// ------------------------------------------------------------------------------------------------------

	
	//Params: faq_id, faq_name, faqTranslation_name

	
	def createFaq(GrailsParameterMap params, Site site){
        Language en = Language.findById("en")
        Language zh_CN = Language.findById("zh_CN")

		Faq faqEN = 
			new Faq(site: site, dateCreated: new Date(), lastUpdated: new Date())

		faqEN = bindFaq(params, faqEN)

		FaqTranslation faqZH = 
			new FaqTranslation(faq: faqEN, language: zh_CN, dateCreated: new Date(), lastUpdated: new Date())
		faqZH = bindFaqTranslation(params, faqZH)

		faqEN.save(failOnError: true)
		faqZH.save(failOnError: true)

	}

	// Params: faq_id, faqItem_id, faqItem_label faqItem_content, faqItemTranslation_content, faqItemAnswer_content, faqItemAnswerTranslation_content

	def createFaqItem(GrailsParameterMap params){
        Language en = Language.findById("en")
        Language zh_CN = Language.findById("zh_CN")

        Faq faq = Faq.findById(params?.faq_id)

		FaqItem faqItemEN = 
			new FaqItem(faq: faq, dateCreated: new Date(), lastUpdated: new Date())
		faqItemEN = bindFaqItem(params, faqItemEN)

		FaqItemTranslation faqItemZH = 
			new FaqItemTranslation(faqItem: faqItemEN, language: zh_CN, dateCreated: new Date(), lastUpdated: new Date())
		faqItemZH = bindFaqItemTranslation(params, faqItemZH)

		FaqItemAnswer faqItemAnswerEN = 
			new FaqItemAnswer(faqItem: faqItemEN, language: en, dateCreated: new Date(), lastUpdated: new Date())
		faqItemAnswerEN = bindFaqItemAnswer(params, faqItemAnswerEN)

		FaqItemAnswer faqItemAnswerZH = 
			new FaqItemAnswer(faqItem: faqItemEN, language: zh_CN, dateCreated: new Date(), lastUpdated: new Date())
		faqItemAnswerZH = bindFaqItemAnswerTranslation(params, faqItemAnswerZH)

		faqItemEN.save(failOnError: true)
		faqItemZH.save(failOnError: true)
		faqItemAnswerEN.save(failOnError: true)
		faqItemAnswerZH.save(failOnError: true)

	}


	//Params: faq_id, faq_name, faqTranslation_name

	def updateFaq(GrailsParameterMap params){

		Faq faqEN = Faq.findById(params?.faq_id)
		faqEN = bindFaq(params, faqEN)

		FaqTranslation faqZH = Faq.findByFaq(faqEN)
		faqZH = bindFaqTranslation(params, faqZH)

		faqEN.save(failOnError: true)
		faqZH.save(failOnError: true)

	}

	//Params: faqItem_id, faqItem_content, faqItemTranslation_content, faqItemAnswer_content, faqItemAnswerTranslation_content

	def updateFaqItem(GrailsParameterMap params){
        Language en = Language.findById("en")
        Language zh_CN = Language.findById("zh_CN")

		FaqItem faqItemEN = FaqItem.findById(params?.faqItem_id)
		faqItemEN = bindFaqItem(params, faqItemEN)

		FaqItemTranslation faqItemZH = FaqItemTranslation.findByFaqItem(faqItemEN)
		faqItemZH = bindFaqItemTranslation(params, faqItemZH)

		FaqItemAnswer faqItemAnswerEN = FaqItemAnswer.findByFaqItemAndLanguage(faqItemEN, en)
		faqItemAnswerEN = bindFaqItemAnswer(params, faqItemAnswerEN)

		FaqItemAnswer faqItemAnswerZH = FaqItemAnswer.findByFaqItemAndLanguage(faqItemEN, zh_CN)
		faqItemAnswerZH = bindFaqItemAnswerTranslation(params, faqItemAnswerZH)


		faqItemEN.save(failOnError: true)
		faqItemZH.save(failOnError: true)
		faqItemAnswerEN.save(failOnError: true)
		faqItemAnswerZH.save(failOnError: true)

	}

    def bindFaq(GrailsParameterMap params, Faq faq){
        User user = springSecurityService.getCurrentUser()

        faq.name = params?.faq_name
        faq.modifiedBy = user
        faq.lastUpdated = new Date()

        return faq
    }

    def bindFaqTranslation(GrailsParameterMap params, FaqTranslation faqTranslation){
        User user = springSecurityService.getCurrentUser()

        faqTranslation.name = params?.faqTranslation_name
        faqTranslation.modifiedBy = user
        faqTranslation.lastUpdated = new Date()

        return faqTranslation
    }

    def bindFaqItem(GrailsParameterMap params, FaqItem faqItem){
        User user = springSecurityService.getCurrentUser()

        faqItem.label = params?.faqItem_label
        faqItem.content = params?.faqItem_content
        faqItem.modifiedBy = user
        faqItem.lastUpdated = new Date()

        return faqItem
    }

    def bindFaqItemTranslation(GrailsParameterMap params, FaqItemTranslation faqItemTranslation){
        User user = springSecurityService.getCurrentUser()

        faqItemTranslation.content = params?.faqItemTranslation_content
        faqItemTranslation.modifiedBy = user
        faqItemTranslation.lastUpdated = params?.faqItemTranslation_lastUpdated

        return faqItemTranslation
    }

    def bindFaqItemAnswer(GrailsParameterMap params, FaqItemAnswer faqItemAnswer){
        User user = springSecurityService.getCurrentUser()

        faqItemAnswer.content = params?.faqItemAnswer_content
        faqItemAnswer.modifiedBy = user
        faqItemAnswer.lastUpdated = new Date()

        return faqItemAnswer
    }

    def bindFaqItemAnswerTranslation(GrailsParameterMap params, FaqItemAnswer faqItemAnswer){
    	User user = springSecurityService.getCurrentUser()

        faqItemAnswer.content = params?.faqItemAnswerTranslation_content
        faqItemAnswer.modifiedBy = user
        faqItemAnswer.lastUpdated = new Date()

        return faqItemAnswer
    }
}
