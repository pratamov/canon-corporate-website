// ------------------------------------------------------------------------------ //
// Article Solution
// ------------------------------------------------------------------------------ //
(function($) {
    "use strict";
    
    $(window).on('load', function(){
        // ------------------------------------------------------------------------------ //
        // Owl Images
        // ------------------------------------------------------------------------------ //
        $('.owl-image').owlCarousel({
            loop:true,
            margin:0,
            nav:true,
            navText: ['<span class="fa fa-angle-left"></span>','<span class="fa fa-angle-right"></span>'],
            dots: true,
            items:1
        }); 
    }); 
    
    // ------------------------------------------------------------------------------ //
    // Owl View & Sticky Share
    // ------------------------------------------------------------------------------ //
    var setSticky = function(){
        var windowWidth = $(window).width();
        if(windowWidth > 992){
            $('.section-share').sticky({topSpacing:150});
        }else{
            $(".section-share").unstick();
        }
    }
    $(window).on('load', setSticky);
    $(window).on('resize', setSticky);

})(jQuery);

// ------------------------------------------------------------------------------ //
// Article
// ------------------------------------------------------------------------------ //
(function($) {
    "use strict";
    
    $(window).on('load', function(){
        // ------------------------------------------------------------------------------ //
        // Toggle Shaere
        // ------------------------------------------------------------------------------ //
        $('.section-share').each(function(){
            var elem = $(this);
            $('a.toggle-share', this).on('click', function(e){
                e.preventDefault();
                $('.section-share').toggleClass('on');
            });
        });
    
        $('ul.clone-share').html($('ul.social-share').html());
    });
    
    // ------------------------------------------------------------------------------ //
    // Sticky
    // ------------------------------------------------------------------------------ //
    var stickySet = function(){
        var windowWidth = $(window).width();
        $('.owl-view').trigger('destroy.owl.carousel');
    
        if(windowWidth > 992){
            $('.section-share').sticky({topSpacing:150});
        }else{
            $(".section-share").unstick();
        }
    }
    $(window).on('load', stickySet);
    $(window).on('resize', stickySet);

})(jQuery);

// ------------------------------------------------------------------------------ //
// Contact Us
// ------------------------------------------------------------------------------ //
(function($) {
    "use strict";
    // ------------------------------------------------------------------------------ //
    // Select Option
    // ------------------------------------------------------------------------------ //
    $('.nav-result').each(function(){
        var text = $('.container ul li.active a', this).html();
        $('.toggle-nav-result', this).html(text);
        $('.toggle-nav-result', this).on('click', function(e){
            e.preventDefault();
            $(this).closest('.nav-result').toggleClass('on');
        });
    });

    $('ul.list-tab-nav').each(function(){
        var elem = $(this).data('toggle-content');
        $('a', this).on('shown.bs.tab', function(event){
            var text = $(this).html();
            $(elem).html(text);
            $(this).closest('.nav-result').removeClass('on');
        });
    });

    // ------------------------------------------------------------------------------ //
    // Toggle Panel Box
    // ------------------------------------------------------------------------------ //
    $('.title-contact').on('click', function(e){
        e.preventDefault();
        $(this).closest('.wrap-contact').toggleClass('on');
        $('.select').select2({
            minimumResultsForSearch: -1
        });
    });
})(jQuery);

// ------------------------------------------------------------------------------ //
// Login Panel on mobile screen
// ------------------------------------------------------------------------------ //
(function($) {
    "use strict";
    $('.section-login .box-panel').each(function(){
        var elemBox = $(this);
        $('.title', this).on('click', function(){
            elemBox.toggleClass('on');
        });
    });
})(jQuery);

// ------------------------------------------------------------------------------ //
// Page Compare
// ------------------------------------------------------------------------------ //
(function($) {
    "use strict";
    $(window).on('load', function(){
        var activateGrid = function(){
            $('.carousel-in-small .owl-item').each(function(){
                if($(this).hasClass('active') && $(this).find('.post-item').length != 0){
                    var getIndex = $('.product-compare', this).data('index');
                    $('.table-compare table.table-info tr').each(function(){
                        $('td', this).eq(getIndex).addClass('show');
                    });
                }
            });
        };
        activateGrid();

        // ------------------------------------------------------------------------------ //
        // Wrap Compare
        // ------------------------------------------------------------------------------ //
        $('.wrap-compare .carousel-in-small').on('changed.owl.carousel', function(event) {
            setTimeout(function(){
                $('.table-compare table.table-info tr td').removeClass('show');
                activateGrid();
            }, 500);
        }); 
    
        // Tabble Info Toggle
        $('.wrap-table-info').each(function(){
            var elem = $(this);
            $('.table-title', this).on('click', function(e){
                e.preventDefault();
                elem.toggleClass('on');
            });
        });
    
        $('.select').select2({
            minimumResultsForSearch: -1
        });
    });
    
    // ------------------------------------------------------------------------------ //
    // Set Column Table Compare
    // ------------------------------------------------------------------------------ //
    var tableCompare = function(){
        var getWidth = $('.select-column').innerWidth() - 60;
        $('.table-compare table tr td').each(function(){
            if(!$(this).hasClass('title')){
                $(this).width(getWidth);
            }
        });
    };
    $(window).on('load', tableCompare);
    $(window).on('resize', tableCompare);


    // ------------------------------------------------------------------------------ //
    // Do ajax comparing
    // ------------------------------------------------------------------------------ //
    $('.select-product-compare').on('select2:select', function (e) {
        compareProduct(tableCompare);
    });
})(jQuery);

// ------------------------------------------------------------------------------ //
// Product Detail
// ------------------------------------------------------------------------------ //
(function($) {
    "use strict";
    // ------------------------------------------------------------------------------ //
    // Owl View & Sticky Share
    // ------------------------------------------------------------------------------ //
    var owlSet = function(){
        var windowWidth = $(window).width();
        $('.owl-view').trigger('destroy.owl.carousel');
        if(windowWidth > 992){
            $('.owl-view').owlCarousel({
                margin: 0,
                nav: false,
                dots: false,
                responsive:{
                    1000:{
                        items:5
                    },
                    1200:{
                        items:6
                    }
                }
            });
            $('.section-share').sticky({topSpacing:150});
        }else{
            $('.owl-view').owlCarousel({
                margin: 0,
                nav: false,
                dots: false,
                autoWidth: true
            });
            $(".section-share").unstick();
        }
    }

    // ------------------------------------------------------------------------------ //
    // 360 VIew Product
    // ------------------------------------------------------------------------------ //
    var rotateImage = function(path){
        console.log(path);
        $('ol.threesixty_images li').remove();

        var rotateImg = $('.rotate-img').ThreeSixty({
            totalFrames: 52, // Total no. of image you have for 360 slider
            endFrame: 52, // end frame for the auto spin animation
            currentFrame: 1, // This the start frame for auto spin
            imgList: '.threesixty_images', // selector for image list
            progress: '.spinner', // selector to show the loading progress
            imagePath: path, // path of the image assets
            filePrefix: '', // file prefix if any
            ext: '.png', // extension for the assets
            navigation: false,
            responsive: true
        });
        $('.custom_next').bind('click', function(e) {
            e.preventDefault();
            rotateImg.next();
        });
    };

    $(window).on('load', function(){
        // ------------------------------------------------------------------------------ //
        // Box Panel Tabs
        // ------------------------------------------------------------------------------ //
        $('.box-panel').each(function(){
            $('.link', this).on('click', function(){
                $('.box-panel').removeClass('active');
                $(this).closest('.box-panel').addClass('active');
            });
        });

        // ------------------------------------------------------------------------------ //
        // Owl Images
        // ------------------------------------------------------------------------------ //
        $('.owl-image').owlCarousel({
            loop:true,
            margin:0,
            nav:true,
            navText: ['<span class="fa fa-angle-left"></span>','<span class="fa fa-angle-right"></span>'],
            dots: true,
            items:1,
            autoHeight:true
        }); 

        // ------------------------------------------------------------------------------ //
        // Control Image View
        // ------------------------------------------------------------------------------ //
        $('.control li').each(function(){
            $('a', this).on('click', function(e){
                e.preventDefault();
                var target = $(this).attr('href');
                $('.content-view').addClass('hide');
                $(target).removeClass('hide');

                $('.control li').removeClass('active');
                $(this).closest('li').addClass('active');

                // ------------------------------------------------------------------------------ //
                // ZoomIn
                // ------------------------------------------------------------------------------ //
                if(target == "#zoomin"){
                    var imgSrc = $('.current-image').attr('src');
                    $(target).find('img').attr('src',imgSrc);
                    $('.panzoom').panzoom({
                        $zoomIn: $(".zoom-in"),
                        $zoomOut: $(".zoom-out"),
                        $zoomRange: $(".zoom-range"),
                        $reset: $(".reset")
                    });
                    $('.panzoom').panzoom("reset");
                }
            });
        });
        owlSet();

        // ------------------------------------------------------------------------------ //
        // List View
        // ------------------------------------------------------------------------------ //
        $('.list-view').each(function(){
            var path = $(this).data('path'),
                type = $(this).data('type'),
                run = function(){
                    if(type == 'image'){
                        $('.control').hide();
                        $('#rotate').addClass('hide');
                        $('#zoomin').removeClass('hide');
                        $('.img-zoom').attr('src',path);
                        $('.panzoom').panzoom({
                            $zoomIn: $(".zoom-in"),
                            $zoomOut: $(".zoom-out"),
                            $zoomRange: $(".zoom-range"),
                            $reset: $(".reset")
                        });
                        $('.panzoom').panzoom("reset");
                    }else{
                        $('#zoomin').addClass('hide');
                        $('#rotate').removeClass('hide');
                        $('.control').show();
                        rotateImage(path);
                    }
                };

            // Event Click
            $('a', this).on('click', function(e){
                $('.list-view').removeClass('active');
                $(this).closest('.list-view').addClass('active');
                e.preventDefault();
                run();
                var body = $("html, body"),
                    offsetTopElem = $('.wrap-view').offset().top;
                body.stop().animate({scrollTop:offsetTopElem});
            });

            // Init
        if($(this).hasClass('active')){
            run();
        }
        });
    });

    $(window).on('resize', function(){
        if($(".list-view[data-type='rotate']").hasClass('active')){
            $('#zoomin').addClass('hide');
            $('#rotate').removeClass('hide');
            $('.control li').removeClass('active');
            $('.control li').eq(1).addClass('active');
        }

        owlSet();
    });
})(jQuery);


// ------------------------------------------------------------------------------ //
// Product Overview
// ------------------------------------------------------------------------------ //
(function($) {
    "use strict";
    $(window).on('load', function(){
    
        // ------------------------------------------------------------------------------ //
        // Clone Social Share
        // ------------------------------------------------------------------------------ //
        $('.clone-social-share').html($('.section-share').html());
        $('.clone-social-share a.toggle-share').on('click', function(e){
            e.preventDefault();
            $('.clone-social-share').toggleClass('on');
        });
    });
    
    // ------------------------------------------------------------------------------ //
    // Sticky Share
    // ------------------------------------------------------------------------------ //
    var stickySet = function(){ 
        var windowWidth = $(window).width();
        if(windowWidth > 992){
            $('.section-share').sticky({topSpacing:150});
        }else{
            $(".section-share").unstick();
        }
    }
    $(window).on('load', stickySet);
    $(window).on('resize', stickySet);
})(jQuery);

// ------------------------------------------------------------------------------ //
// Product Purchase
// ------------------------------------------------------------------------------ //
(function($) {
    "use strict";
    var totalArray = [],
    sumValue,
    baseValue = 2899;
    $('.attr-product').each(function(){
    var $this = $(this), 
        target = $this.data('target'),
        index = $this.data('index'),
        activate = function(){
            var activeValue = $this.find('.active').data('value');
            $(target).find('span').html(core.formatNumber(activeValue));
            totalArray[index] = activeValue;
            sumValue = _.sum(totalArray)
            $('.total-value').html(core.formatNumber(baseValue + sumValue));
        };

    activate();

    $('.item-attr', this).each(function(){
        var valueItem = $(this).data('value');
        $('.value', this).html(core.formatNumber(valueItem));

        $(this).on('click', function(){
            $(this).closest('.attr-product').find('.item-attr').removeClass('active');
            $(this).addClass('active');
            activate();
            // $(target).find('span').html(core.formatNumber(activeValue));
        });
    });
    });
})(jQuery);

// ------------------------------------------------------------------------------ //
// Promotion Page
// ------------------------------------------------------------------------------ //
(function($) {
    "use strict";
    $(window).on('load', function(){
        $('.equal-height').matchHeight();
    
        // ------------------------------------------------------------------------------ //
        // Toggle More widget sidebar
        // ------------------------------------------------------------------------------ //
        $('.toggle-more').each(function(){
            var getId = $(this).attr('id'),
                count = 3,
                checkList = $('li', this).length;
    
            if(checkList > count){
                $('#' + getId).showmore({
                    visible:3,
                    childElement:"li",
                    showMoreText : "Show more",
                    showLessText : "Show less",
                    showMoreClass : "show_more",
                    showLessClass : "show_less"
                });
            }
        });
    
        // ------------------------------------------------------------------------------ //
        // Sidebar Widget
        // ------------------------------------------------------------------------------ //
        $('.sidebar .widget').each(function(){
            $(this).addClass('animated');
            var btn = $(this).data('toggle');
            var elem = $(this).attr('id');
            $(btn).on('click', function(e){
                e.preventDefault();
                $('#'+ elem).addClass('on');
                $('#'+ elem).removeClass('slideOutLeft');
                $('#'+ elem).addClass('slideInLeft');
    
                $('.select').select2({
                    minimumResultsForSearch: -1
                });
    
                $('nav.navbar').addClass('hide');
            });
    
            $('.close', this).on('click', function(e){
                e.preventDefault();
                $('#'+ elem).removeClass('slideInLeft');
                $('#'+ elem).addClass('slideOutLeft');
                setTimeout(function(){
                    $('#'+ elem).removeClass('on');
                }, 500);
                $('nav.navbar').removeClass('hide');
            });
        });
    });
    
    $(window).on('resize', function(){
        $('.sidebar .widget').removeClass('on');
        $('.sidebar .widget').removeClass('slideOutLeft');
        $('.sidebar .widget').removeClass('slideInLeft');
        $('nav.navbar').removeClass('hide');
    });
})(jQuery);

// ------------------------------------------------------------------------------ //
// Tab On Mobile
// [where-to-buy.html]
// ------------------------------------------------------------------------------ //
(function($) {
    "use strict";
    $(window).on('load', function(){
    
        $('ul.nav-location li').each(function(){
            $('a', this).on('click', function(e){    
                e.preventDefault();
                var getId = $(this).attr('href');
                $('.content-location').removeClass('in');
                $(getId).addClass('in');
                mapcore.initMap();
                $('.select').select2({
                    minimumResultsForSearch: -1
                });
    
                // Nav Active
                $('ul.nav-location li').removeClass('active');
                $(this).closest('li').addClass('active');
            });
        });
    });
})(jQuery);