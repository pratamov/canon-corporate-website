/**
 * This is a manifest file that'll be compiled into application.js.
 *
 * Any JavaScript file within this directory can be referenced here using a relative path.
 *
 * You're free to add application-wide JavaScript to this file, but it's generally better
 * to create separate JavaScript files as needed.
 *
 */
//= require jquery.min
//= require mustache.min
//= require lib/jquery.validate
//= require lib/jquery.validate.custom
//= require main
//= require custom
//= require page-setting

var CANON = CANON || {};
CANON.pub = CANON.pub || {};

CANON.pub.constants = {
    product: {
        COMPARE_URI : "/comparing/specs/",
    }
};

if (typeof jQuery !== 'undefined') {
    (function($) {
        $('#spinner').ajaxStart(function() {
            $(this).fadeIn();
        }).ajaxStop(function() {
            $(this).fadeOut();
        });

        $(".js-auto-submit").on("change", function () {
            $(this).closest("form").submit();
        });
    })(jQuery);
}

window.onload = function () {
    if ($('.parallax')){
        setInterval(function () {
            $('.parallax').parallax();
        }, 500);
    }
};

var compareProduct = function(tableCompare){
    var producta = $('#product-a').val();
    var productb = $('#product-b').val();
    var productc = $('#product-c').val();

    var urlTemplate = CANON.pub.constants.product.COMPARE_URI + "{{producta}}/{{productb}}/{{productc}}";

    var comparingUrl = Mustache.render(urlTemplate, {producta:producta,productb:productb,productc:productc});

    $.get( comparingUrl, function( data ) {
        //console.log(data);
        var specsTemplate = $("#specs-compare-template").html();
        Mustache.parse(specsTemplate);

        if(productb && data.productb){
            $('#product-b-name').html(data.productb.name);
            $('#overview-peoduct-b').show();
            $('#overview-peoduct-b').find('img').attr('src',data.productb.img);
            if(data.productb.price){
                $('#overview-peoduct-b').find('.price').html(data.productb.price);
            }else{
                $('#overview-peoduct-b').find('.price').html('-');
            }
        }

        if(productc && data.productc){
            $('#product-c-name').html(data.productc.name);
            $('#overview-peoduct-c').show();
            $('#overview-peoduct-c').find('img').attr('src',data.productc.img);
            if(data.productc.price){
                $('#overview-peoduct-c').find('.price').html(data.productc.price);
            }else{
                $('#overview-peoduct-c').find('.price').html('-');
            }
        }

        var div = Mustache.render(specsTemplate, data);

        $('#specs-table').html(div);

        //console.log($('#specs-table'));
        tableCompare();


    });
};


// ------------------------------------------------------------------------------ //
// Contact Us
// ------------------------------------------------------------------------------ //
if ($("#form-contact-us-asia").length > 0) {

    function canonAsiaContactChange(){
        var typeOfEnquiry = $contactAsiaForm.find("#type_of_enquiry").val();
        var specificEnquiry = $contactAsiaForm.find("#specific_enquiry").val();
        var countrySelection = $contactAsiaForm.find("#country_of_residence").val();

        if((specificEnquiry=="contact.formfield.select.others.Warranty_others") && (countrySelection=="thailand")) {
            window.open("http://www.canon.co.th/member");
        }

        if((typeOfEnquiry== "contact.formfield.select.sales") &&  (specificEnquiry== 'contact.formfield.select.sales.SuppliesPaperConsumables' )&& (countrySelection=="thailand") ){
            window.open("http://www.canon.co.th/estore");
        }
        if((specificEnquiry== 'contact.formfield.select.others.FeedbackComplaints_others' )&& (countrySelection=="thailand") ){
            window.open("http://www.canon.co.th/feedback");
        }
        if((specificEnquiry== 'contact.formfield.select.others.JobsCareers_others' )&& (countrySelection=="thailand") ){
            window.open("http://www.canon.co.th/personal/web/company/about/careers?languageCode=TH");
        }
    }

    var $contactAsiaForm = $("#form-contact-us-asia");

    $contactAsiaForm.find("#type_of_enquiry").change(function(e){
        var $this = $(this);
        var enquiryItemArray = $.grep(ENQUIRY_LIST, function(e){ return e.key == $this.val(); });
        var enquiryItem = enquiryItemArray[0];

        $contactAsiaForm.find("#specific_enquiry").find("option").each(function(i, el){
            if (el.value != '') {
                el.remove();
            }
        });
        if (enquiryItem) {
            for(var i=0; i < enquiryItem.types.length; i++) {
                var el = enquiryItem.types[i];
                $contactAsiaForm.find("#specific_enquiry").append($("<option></option>").attr('value', el.key).text(el.name));
            }
        }
        canonAsiaContactChange();
    });

    $contactAsiaForm.find("#specific_enquiry").change(function(e){
        canonAsiaContactChange();
    });

    $contactAsiaForm.find("#country_of_residence").change(function(e){
        var $this = $(this);
        if ($this.find("option:selected").data("url") != '') {
            window.open($this.find("option:selected").data("url"));
        }

        canonAsiaContactChange()
    });

    $('#form-contact-us-asia').validate({
        errorElement: "span",
        errorClass: "has-error",
        success: function(label,element) {
            $(element).closest(".form-group").find(".has-error").removeClass("has-error");
        },
        errorPlacement: function (error, element) {
            error.insertAfter(element);
        }
    });

    $('#form-contact-us-asia').bind('submit', function (e) {
        e.preventDefault();
        if ($(this).valid()) {
            var $form = $(this);

            var url = $form.attr('action');

            var postData = {
                typeOfEnquiry: $form.find("[name=typeOfEnquiry]").val(),
                specificEnquiry: $form.find("[name=specificEnquiry]").val(),
                salutation: $form.find("[name=salutation]").val(),
                name: $form.find("[name=name]").val(),
                emailAddress: $form.find("[name=emailAddress]").val(),
                retypeEmailAddress: $form.find("[name=retypeEmailAddress]").val(),
                contactNo: $form.find("[name=contactNo]").val(),
                countryOfResidence: $form.find("[name=countryOfResidence]").val(),
                message: $form.find("[name=message]").val()
            };

            $.ajax({
                "type": "POST",
                "url": url,
                "data": JSON.stringify(postData),
                "contentType": "application/json",
                "success": function (json) {
                    if (json.status == "success") {
                        alert("Success");
                        $form.find("#type_of_enquiry").select2("val","");
                        $form.find("#specific_enquiry").select2("val","");
                        $form.find("#salutation").select2("val","");
                        $form.find("#country_of_residence").select2("val","");
                        $form.find("#name").val("");
                        $form.find("#emailAddress").val("");
                        $form.find("#retypeEmailAddress").val("");
                        $form.find("#contactNo").val("");
                        $form.find("#message").val("");
                    }
                },
                "dataType": "json"
            }).fail(function (jqxhr, textStatus, error) {
                if (jqxhr.status == 400) {
                    $form.serverErrorPlacement(jqxhr.responseJSON.errors);
                } else {

                }
            });
        }
    });



}