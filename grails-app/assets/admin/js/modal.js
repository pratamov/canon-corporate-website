/**
 * Created by muyalif on 16/01/18.
 */
'use strict';
(function ($) {
    $('#modalRelatedProduct')
        .on('shown.bs.modal', function (e) {
            var _this = $(this);
            var _relatedTarget = $(e.relatedTarget);
            var _content = _relatedTarget.parent().siblings('.js-content');
            var _getAllUrl = _this.data('url');

            var _selectedItem = [];
            _content.find('.js-selected-item').each(function () {
                _selectedItem.push($(this).data('id'));
            });

            _this.find('.modal-title').text(_relatedTarget.data('title'));

            var _selector = _this.find('.js-selector');
            $.get(_getAllUrl, {}, function (response) {
                var _data = response.data;
                for (var i in _data) {
                    var item = _data[i];
                    var _option = $('<option></option>');
                    _option.attr('value', item.id);
                    _option.text(item.name);

                    if(_selectedItem.indexOf(item.id) > -1){
                        _option.attr('selected', true);
                    }

                    _selector.append(_option);
                }

                _selector.bootstrapDualListbox('refresh');
        });

        _this.find('.js-save-btn').on('click', function () {

            var _list = _content.find('ul');

            if (_selector.find(':selected').length > 0) {
                _content.removeClass('hidden');
                _list.children('li').remove();
            }

            _selector.find(':selected').each(function () {
                var id = $(this).val();
                var name = $(this).text();

                var template = $('#wrap-cell-list-template').html();

                var rendered = Mustache.render(template, {
                    id: id,
                    name: name
                });

                _list.append(rendered);
            });

            _list.find('.js-remove-item').on('click', function (e) {
                e.preventDefault();

                $(this).parent().remove();

                if ($(this).closest('ul').length < 1) {
                    $(this).closest('.js-content').removeClass('hidden');
                }
            });

            _this.modal('toggle');
        })
    }).on('hidden.bs.modal', function () {
        var _this = $(this);

        var _selector = _this.find('.js-selector');
        _selector.children('option').remove();

        _this.find('.js-save-btn').unbind('click');
    });

    /*$("#modalLocation")
        .on("shown.bs.modal", function (e) {
           var _this = $(this);
           var _relatedTarget = $(e.relatedTarget);

           var _title = _relatedTarget.data("title");
           _this.find('.modal-title').text(_title);

           var _siteId = _relatedTarget.closest('tr').find('input[name=locationId]').val();

           var _siteSelector = _this.find('select[name=location]');
           if (_siteId) {

               var _getCategoryUrl = _this.find('select[name=location]').data('url');
               var _categoryList = _this.find('.js-category-selector');

               _siteSelector.val(_siteId);
               _siteSelector.attr('disabled', true);

               loadCategoriesBySite(_getCategoryUrl, _siteId, _categoryList);
           } else {
               validateLocationList(_siteSelector);
           }

           var _sitePrice = _relatedTarget.closest('tr').find('input[name=locationPrice]').val();
           var _sitePublishDate = _relatedTarget.closest('tr').find('input[name=locationPublishedDate]').val();
           var _siteArchiveDate = _relatedTarget.closest('tr').find('input[name=locationArchivedDate]').val();

           if(_sitePrice) _this.find('input[name=sitePrice]').val(_sitePrice);
           if(_sitePublishDate) _this.find('input[name=sitePublishDate]').val(_sitePublishDate);
           if(_siteArchiveDate) _this.find('input[name=siteArchiveDate]').val(_siteArchiveDate);

            _this.find('.date').each(function () {
                $(this).datepicker({
                    todayBtn: "linked",
                    keyboardNavigation: false,
                    forceParse: false,
                    calendarWeeks: true,
                    autoclose: true,
                    format: "d M yyyy"
                });
            });
        })
        .on("hidden.bs.modal", function () {
            var _this = $(this);

            resetModalLocation(_this);
        })
        .on('change', 'select[name=location]', function () {
            var _this = $(this);
            var _siteId = _this.val();
            var _getCategoryUrl = _this.data('url');
            var _categoryList = _this.closest('.modal-body').find('.js-category-selector');

            loadCategoriesBySite(_getCategoryUrl, _siteId, _categoryList);
        })
        .on('click', '.js-save-btn', function () {
            var _this = $(this);
            var _isClose = _this.data('close');
            var _modal = _this.closest('.modal-content');

            var siteId = _modal.find('select[name=location]').find('option:selected').val();
            var siteName = _modal.find('select[name=location]').find('option:selected').text();
            var sitePrice = _modal.find('input[name=sitePrice]').val();
            var sitePublishDate = _modal.find('input[name=sitePublishDate]').val();
            var siteArchiveDate = _modal.find('input[name=siteArchiveDate]').val();
            var siteCategories = _modal.find('select[name=categories]').val();

            var data = {
                siteId: siteId,
                siteName: siteName,
                sitePrice: sitePrice,
                sitePublishDate: sitePublishDate,
                siteArchiveDate: siteArchiveDate,
                siteCategories: siteCategories
            };

            updateLocationTableRow(data);

            if(_isClose) {
                _modal.closest('.modal').modal('hide');
            } else {
                resetModalLocation(_modal);
            }

        });
    
    var loadCategoriesBySite = function (url, sideId, selector) {

        var _selectedCategories = [];
        $('.js-product-categories').children('input').each(function () {
            _selectedCategories.push($(this).val());
        });

        console.log(_selectedCategories);

        if(sideId) {
            $.get(url, {siteId: sideId}, function (response) {
                if(response.success) {
                    var _categories = response.data;

                    for (var i in _categories) {
                        var _category = _categories[i];
                        var _option = $('<option></option>');
                        _option.val(_category.id);
                        _option.text(_category.name);

                        console.log('caregory id : ' + _category.id);
                        if (_selectedCategories.indexOf(_category.id) > -1){
                            _option.attr('selected', true);
                        }

                        selector.append(_option);
                    }

                    selector.bootstrapDualListbox('refresh');
                }
            });
        }
    };

    var updateLocationTableRow = function (data) {
        var _siteId = data.siteId;
        var _template = $("#location-table-row-template").html();
        var _tbody = $('.js-product-location-table').find('tbody');

        console.log('site id : ' + _siteId);
        var _tableRow = _tbody.find('.js-site-' + _siteId);
    console.log(_tableRow);
        if(_tableRow.length > 0) {
            $('input[name=locationId]').val(_siteId);
            $('input[name=locationPrice]').val(_siteId);
            $('input[name=locationPublishedDate]').val(_siteId);
            $('input[name=locationArchivedDate]').val(_siteId);
            $('td:eq(0) a').text(data.siteName);
            $('td:eq(1)').text(data.sitePrice);
            $('td:eq(2)').text(data.sitePublishDate);
            $('td:eq(3)').text(data.siteArchiveDate);
        } else {
            var _rendered = Mustache.render(_template, {
                siteId: _siteId,
                siteName: data.siteName,
                sitePrice: data.sitePrice,
                sitePublishDate: data.sitePublishDate,
                siteArchiveDate: data.siteArchiveDate
            });

            _tbody.append(_rendered);
        }

        updateProductCategories(_siteId, data.siteCategories);
    };
    
    var updateProductCategories = function (siteId, newCategories) {
        var _className = 'js-category-site-' + siteId;
        var _productCategoryList = $('.js-product-categories');

        var _existingCategories = [];
        _productCategoryList.find('.' + _className).each(function () {
            var _existingCategory = $(this).val();

            if (newCategories.indexOf(_existingCategory) > -1) {
                _existingCategories.push(_existingCategory);
            } else {
                $(this).remove();
            }
        });

        for (var i in newCategories) {
            var _newCategory = newCategories[i];
            if(!(_existingCategories.indexOf(_newCategory) > -1)){
                var _template = '<input type="hidden" class="{{className}}" name="category" value="{{categoryId}}"/>';
                var _rendered = Mustache.render(_template, {className: _className, categoryId: _newCategory});

                _productCategoryList.append(_rendered);
            }
        }
    };

    var resetModalLocation = function(modal) {
        modal.find('input').val('');
        modal.find('option').attr('selected', false);

        modal.find('select[name=location]').attr('disabled', false);

        modal.find('.js-category-selector').children('option').remove();
        modal.find('.js-category-selector').bootstrapDualListbox('refresh');

        validateLocationList(modal.find('select[name=location]'));
    };

    var validateLocationList = function (loc) {
        var _selectedSite = [];
        $('.js-product-location-table').find('input[name=locationId]').each(function () {
            _selectedSite.push($(this).val());
        });

        loc.children('option').each(function () {
            if(_selectedSite.indexOf($(this).val()) > -1) {
                $(this).attr('disabled', true);
            }
        });
    };*/
})(jQuery);