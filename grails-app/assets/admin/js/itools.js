/**
 * Created by marjan on 11/01/18.
 */
'use strict';
(function ($) {
    var IComponent = function (compContent, compOrder, compType) {
        this.compContent = compContent;
        this.compOrder = compOrder;
        this.compType = compType;
    };

    // ITolls Class
    var ITolls = function (target, options) {
        this.contentArea = target;
        this.snippetsUrl = "/admin/tool/snippets";
        if (options && options.snippetsUrl) {
            this.snippetsUrl = options.snippetsUrl
        }
    };

    ITolls.prototype.init = function (selector) {
        var self = this;
        if (selector) self.contentArea = $(selector);
        if (self.contentArea.length > 0) {
            $.keditor.debug = false;
            self.contentArea.keditor({
                nestedContainerEnabled: false,
                containerSettingEnabled: false,
                snippetsUrl: self.snippetsUrl,
                iframeMode: true,
                onReady: function () {},
                onInitFrame: function (frame, frameHead, frameBody) { },
                onSidebarToggled: function (isOpened) { },
                onInitContentArea: function (contentArea) { },
                onContentChanged: function (event) { },
                onInitContainer: function (container) {},
                onBeforeContainerDeleted: function (event, selectedContainer) { },
                onContainerDeleted: function (event, selectedContainer) { },
                onContainerChanged: function (event, changedContainer) { },
                onContainerDuplicated: function (event, originalContainer, newContainer) { },
                onContainerSelected: function (event, selectedContainer) { },
                onContainerSnippetDropped: function (event, newContainer, droppedContainer) {},
                onComponentReady: function (component) { },
                onInitComponent: function (component) { },
                onBeforeComponentDeleted: function (event, selectedComponent) { },
                onComponentDeleted: function (event, selectedComponent) { },
                onComponentChanged: function (event, changedComponent) { },
                onComponentDuplicated: function (event, originalComponent, newComponent) { },
                onComponentSelected: function (event, selectedComponent) { },
                onComponentSnippetDropped: function (event, newComponent, droppedComponent) {
                    $(newComponent).addClass("section");
                },
                onBeforeDynamicContentLoad: function (dynamicElement, component) { },
                onDynamicContentLoaded: function (dynamicElement, response, status, xhr) { },
                onDynamicContentError: function (dynamicElement, response, status, xhr) { }
            });
        }
    };

    ITolls.prototype.toCompType = function (dataType) {
        if (dataType) dataType = dataType.replace("component-", "");
        return dataType;
    };

    ITolls.prototype.typeToProcessor = function (compType) {
        var _fName = "";
        if (compType) {
            var _cNames = compType.split("-");
            var i;
            for (i in _cNames) {
                _fName += _cNames[i].charAt(0).toUpperCase() + _cNames[i].slice(1);
            }
        }
        return _fName + "IComp";
    };

    ITolls.prototype.convertToContent = function (htmlString, compType) {
        var self = this;
        var _compContent = {};
        (function (functionName, htmlString) {
            var _cType = self.typeToProcessor(functionName);
            var _fn = $[_cType];
            if (typeof _fn === 'function') {
                _compContent = _fn(htmlString);
            }
        })(compType, htmlString);
        return _compContent;
    };

    ITolls.prototype.toComponents = function(contents) {
        var self = this;
        var componentList = [];
        $(contents).find("section[data-type^='component-']").each(function( index ) {
            var _cType = self.toCompType($(this).data("type"));
            var _cContent = self.convertToContent($(this).html(), _cType);
            var _c = new IComponent(_cContent, index, _cType);
            if (typeof _c.compOrder != "undefined" && typeof _c.compContent != "undefined" && typeof _c.compType != "undefined") {
                componentList.push(_c);
            }
        });
        return componentList;
    };

    ITolls.prototype.getJsonComponents = function () {
        var self = this;
        var _comps = [];
        if (self.contentArea && self.contentArea.length > 0) {
            var contents = self.contentArea.keditor('getContent');
            _comps = self.toComponents(contents);
        }
        return _comps;
    };

    // ITolls Plugins
    $.fn.itools = function (options) {
        var _t = new ITolls($(this), options);
        _t.init();
        return _t;
    };

    $.fn.itools.constructor = ITolls;

    // Export ITolls
    $.itools = ITolls;
})(jQuery);
