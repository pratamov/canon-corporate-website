/**
 * This is a manifest file that'll be compiled into application.js.
 *
 * Any JavaScript file within this directory can be referenced here using a relative path.
 *
 * You're free to add application-wide JavaScript to this file, but it's generally better
 * to create separate JavaScript files as needed.
 *
 */
//= require js/jquery-3.1.1.min
//= require js/mustache.min
//= require js/main.js
//= require js/custom.js
//= require js/dam.js
//= require js/modal.js

if (typeof jQuery !== 'undefined') {
    (function($) {

        var locationTrTemplate = $("#page-location-tr-template").html();
        var sellingPointTemplate = $("#selling-point-template").html();
        Mustache.parse(locationTrTemplate);
        Mustache.parse(sellingPointTemplate);

        $('#spinner').ajaxStart(function() {
            $(this).fadeIn();
        }).ajaxStop(function() {
            $(this).fadeOut();
        });

        $("table.sortable").stupidtable();

        /*$('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
        });

        // Check all
        $("#checkall").on('ifChecked', function(event){
            $('input').iCheck('check');
        });
        $("#checkall").on('ifUnchecked', function(event){
            $('input').iCheck('unCheck');
        });*/

        $(".js-auto-submit").on("change", function () {
            $(this).closest("form").submit();
        });

        // Check all
        $(".checkall").on('click', function(){
            if(this.checked){
                $('.checkbox.single input').prop('checked', true);
            }else{
                $('.checkbox.single input').prop('checked', false);
            }
        });

        $('.js-date').each(function () {
            $(this).datepicker({
                todayBtn: "linked",
                keyboardNavigation: false,
                forceParse: false,
                calendarWeeks: true,
                autoclose: true,
                format: "d M yyyy"
            });
        });

        $('.js-sort').on('click', function () {
           var _redirect = $(this).data('url');

           window.location = _redirect;
        });

        // placeholder
        $(".select").each(function(){
            var placeholder = $(this).data('placeholder');
            $(this).select2({
                placeholder: placeholder,
                allowClear: true
            });
        });

        $( ".modal" ).on('shown.bs.modal', function(){
            var _modal = $(this);

            $( ".modal" ).find('.select').each(function(){
                var placeholder = $(this).data('placeholder');
                $(this).select2({
                    placeholder: placeholder,
                    allowClear: true
                });
            });

        });

        $('.js-table-product-location').on('click', '.js-delete-location', function (e) {
            e.preventDefault();

            $(this).closest('tr').remove();
        });

        $('.js-submit-form').on('click', function () {
            var _form = $(this).closest('body').find('form');

            _form.find('input[name="flow"]').val($(this).data('flow'));

            _form.submit();
        });

        $('.js-input-page').keypress(function (e) {

            if(e.which == 13){
                var totalPage = $(this).data('total-page');
                var url = $(this).data('redirect-url');
                var page = $(this).val() > totalPage ? totalPage : $(this).val();
                var pagePlaceholder = $(this).data('page-placeholder');

                window.location = url.replace(pagePlaceholder, page);
            } else if(e.which < 48 || e.which > 57){
                e.preventDefault();
            }
        });

        $('.js-products-action').on('click', function () {
            var _tbody = $(this).closest('.ibox-content').find('.table tbody');
            var productIds = getCheckedItemsValues(_tbody);
            var url = $(this).data('url');

            $.post(url, $.param({ids: productIds}, true))
                .done(function () {
                    location.reload();
            });
        });

        var resetInputField = function (inputWrapper) {
            inputWrapper
                .find("input,textarea,select").val('').end()
                .find("input[type=checkbox], input[type=radio]").prop("checked", "").end();
        };

        var getCheckedItemsValues = function (element) {
            var checkedItems = new Array();
            $(element).find('input[type="checkbox"]:checked').each(function () {
                checkedItems.push($(this).val());
            });

            return checkedItems
        };

        // ------------------------------------ //
        // Create Page
        // ------------------------------------ //
        $('.js-table-page-location').on('click', '.js-delete-location', function (e) {
            e.preventDefault();

            $(this).closest('tr').remove();
        });

        $('.modal').find('.js-page-save-location').on('click', function () {
            var _this = $(this);
            var _modal = _this.closest('.modal');
            var closeModal = _this.data('close-modal-after-save');
            var _tblBody = $('.js-table-page-location').find('tbody');
            var _contentWrapper = _modal.find('.js-page-location-detail');

            addpageLocation(_tblBody, _contentWrapper);

            resetInputField(_modal);
            if(closeModal) {
                _modal.modal('toggle');
            }
        });

        $('.js-remove-item').on('click', function (e) {
            e.preventDefault();

            $(this).parent().remove();
        });

        var addpageLocation = function (tblBody, contentWrapper) {
            var _site = $(contentWrapper.find('.js-select-location-site').prop('selectedOptions'));
            var _category = $(contentWrapper.find('.js-select-location-category').prop('selectedOptions'));

            var _publishedDate = $(contentWrapper.find("input[name='location-published-date']"));
            var _archivedDate = $(contentWrapper.find("input[name='location-archived-date']"));

            var _content = Mustache.render(locationTrTemplate,{
                "siteId":_site.val(),
                "categoryId":_category.val(),
                "siteName":_site.text(),
                "categoryName":_category.text(),
                "publishedDate":_publishedDate.val(),
                "archivedDate":_archivedDate.val()
            });

            tblBody.append(_content);
        };

        $('.js-page-submit-form').on('click', function () {
            var _form = $(this).closest('body').find('form[name="page-form"]');


            _form.find('input[name="flow"]').val($(this).data('flow'));

            _form.submit();
        });

        var getSelected = function getSelects(id) {
            var selected = [];
            $(id).find('option').each(function() {
                var $this = $(this);
                if($this.attr('selected')){
                    selected.push( $this.val() );
                }

            });

            return selected;
        };

        $('#page-tags').select2({
            multiple:true
        }).val(getSelected('#page-tags')).trigger('change');

        $('#page-topics').select2({
            multiple:true
        }).val(getSelected('#page-topics')).trigger('change');

        /*// ------------------------------------ //
        // Admin Modal
        // ------------------------------------ //
        $('#modalUpload').on('show.bs.modal', function (e) {
            var _this = $(this);
            var title = $(e.relatedTarget).data('title');

            _this.find('.modal-title').text(title);

            var getDirectoriesUrl = _this.find('input[name=getDirectoryUrl]').val();
            $.get(getDirectoriesUrl, function (response) {
                var _data = response.data;
                var _directorySelector = _this.find('.js-directories-selection');

                _directorySelector.empty();
                _directorySelector.append($("<option></option>"));
                $.each(_data, function (k, v) {
                    _directorySelector.append($("<option></option>").attr("value", v).text(k));
                })
            });

            $('.js-search-dam').on('click', function () {
                var _this = $(this);
                var searchUrl = _this.data('url');

                var query = _this.closest('.input-group').find('input[name=q]').val();

                var data = {};
                data["q"] = query;

                $.get(searchUrl, data, function (response) {
                    if (response.success) {
                        console.log(response);
                    }
                })
            });

        });*/

        // --------------------------------------- //
        // Workflow
        // --------------------------------------- //
        $('.js-flow-selector').on('change', function () {
            var _this = $(this);
            var _workflowList = _this.closest('.ibox-content').find('.js-workflow-list');
            var _url = _this.data('url');
            var _getUserUrl = _this.data('get-user');

            var _selectedFlow = _this.val();
            _workflowList.empty();

            var _currentUser = _this.closest('.ibox-content').find('input[name=username]').val();
            var _template = $("#workflow-list-template").html();
            $.get(_url, {flowId: _selectedFlow}, function (response) {
                for(var i in response) {
                    var _item = response[i];

                    var rendered = Mustache.render(_template, {
                        id: _item.id,
                        name: _item.name,
                        username: _currentUser,
                        isStart: _item.isStart,
                        selectable: !_item.isStart,
                        deadline: !_item.isStart
                    });

                    _workflowList.append(rendered);
                }

                _workflowList.find('.date').each(function () {
                    $(this).datepicker({
                        todayBtn: "linked",
                        keyboardNavigation: false,
                        forceParse: false,
                        calendarWeeks: true,
                        autoclose: true,
                        format: "d M yyyy"
                    });
                });

                _workflowList.find('.js-user-selector').append($('<option></option>'));
                $.get(_getUserUrl, {}, function (response) {
                    for (var i in response) {
                        var _user = response[i];
                        _workflowList.find('.js-user-selector').append($('<option></option>').attr('value', _user.id).text(_user.name))
                    }

                    _workflowList.find('.js-user-selector').each(function(){
                        var placeholder = $(this).data('placeholder');
                        $(this).select2({
                            placeholder: placeholder,
                            allowClear: true
                        });
                    });
                });
            });
        });

        $('#sellingPointAdd').on('click', function () {
            var _sellingPointText = $("#sellingPointText").val();
            var _sellingPointContainer = $("#sellingPointContainer");
            var _content = Mustache.render(sellingPointTemplate,{
                "timestamp":new Date().getTime(),
                "content":_sellingPointText
            });
            _sellingPointContainer.append(_content);
            $("#sellingPointText").val("");
        });

        $('#sellingPointContainer').on('click','.selling-point-remove',function() {
            var _sellingPointTemp = $(this).attr('temp');
            $("."+_sellingPointTemp).remove();
        });

        // Insert product image
        var _thumbnailInsertBtn = $('.js-insert-thumbnail-btn');
        var productImageDamOption = {
            callback: function (data) {

                var _isLabeled = data.type !== 'IMAGE';

                var _template = $('#product-image-preview-template').html();
                var _imageData = {
                    mediaId: data.id,
                    mediaType: data.type,
                    mediaUrl: data.thumbnail ? data.thumbnail : data.url,
                    mediaAlt: data.alt,
                    isLabeled: _isLabeled
                };

                var _imagePreview = Mustache.render(_template, _imageData);

                _thumbnailInsertBtn.parent().before(_imagePreview);
            }
        };

        _thumbnailInsertBtn.damlist(productImageDamOption);

        $('.js-product-images-preview').on("click", '.js-remove-product-image', function (e) {
            e.preventDefault();

            var _this = $(this);
            var _image = _this.closest('.js-product-image');
            _image.remove();
        });
        // End of Insert image thumbnail

    })(jQuery);
}

