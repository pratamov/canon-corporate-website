import canon.*
import grails.util.Holders

class BootStrap {

    def init = { servletContext ->
        String dbCreationMode = Holders.config.getProperty("dataSource.dbCreate")

        if (dbCreationMode == "update") {
            return
        }

        UserInit.init()
        FlowInit.init()
        CountryInit.init()
        SiteInit.init()
        LanguageInit.init()
        MediaInit.init()
        MenuInit.init()
        TopicInit.init()
        CategoryInit.init()
        ArticleCategoryInit.init()
        FeatureInit.init()
        AwardInit.init()
        TagInit.init()
        ProductInit.init()
        ProductMediaInit.init()
        ProductCategoryInit.init()
        ProductFeatureInit.init()
        ProductSpecInit.init()
        ProductPromotionInit.init()
        ProductAwardInit.init()
        ProductPriceInit.init()
        ProductRelationInit.init()
        BannerInit.init()
        SubjectInit.init()
        PageInit.init()
        SettingInit.init()

        ProductComponentInit.init()
		
		FaqInit.init()
    }

    def destroy = {
    }
}
