package canon

class UserInit {

    def static init() {

        Role roleAdmin = Role.findByAuthority("ROLE_ADMIN")
        if (!roleAdmin) {
            roleAdmin = new Role("ROLE_ADMIN").save(failOnError: true)
        }

        Role roleAuthor = Role.findByAuthority("ROLE_AUTHOR")
        if (!roleAuthor) {
            roleAuthor = new Role("ROLE_AUTHOR").save(failOnError:true)
        }

        Role roleUser = Role.findByAuthority("ROLE_USER")
        if (!roleUser) {
            roleUser = new Role("ROLE_USER").save(failOnError:true)
        }

        // Admin
        User userAdmin = User.findByUsername("admin")
        if (!userAdmin) {
            userAdmin = new User("admin", "Pass1234", "admin@canon.asia").save(failOnError: true)
        }

        if (!UserRole.get(userAdmin.id, roleAdmin.id)) {
            UserRole.create(userAdmin, roleAdmin, true)
        }

        // Bootstrap
        User userBootstrap = User.findByUsername("bootstrap")
        if (!userBootstrap) {
            userBootstrap = new User("bootstrap", "Pass1234").save(failOnError: true)
        }

        if (!UserRole.get(userBootstrap.id, roleAuthor.id)) {
            UserRole.create(userBootstrap, roleAuthor, true)
        }
    }
}
