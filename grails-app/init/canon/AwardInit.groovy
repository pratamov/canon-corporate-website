package canon

import canon.enums.Status

class AwardInit {
    def static init() {
        User userBootstrap = User.findByUsername("bootstrap")

        def awards = [
                [name:'BLI BUYER LAB',year:2017,imageUrl:'/assets/product-detail/awards/dr-x10c-award.png',status:Status.ACTIVE],
                [name:'TIPA AWARDS',year:2012,imageUrl:'/assets/product-detail/awards/img02.png',status:Status.ACTIVE],
                [name:'TECH AWARDS',year:2014,imageUrl:'/assets/product-detail/awards/img03.png',status:Status.ACTIVE],
                [name:'DESIGN TALENT AWARD',year:2017, imageUrl:'/assets/product-detail/awards/img01.png',status:Status.ACTIVE],
                [name:'E Photo Zine AWARD',year:2017, imageUrl:'https://www.canon.co.uk/Images/e-photo-zine-highly-recommended-220w_tcm14-1602424.jpg',status:Status.ACTIVE],
                [name:'Photography Blog AWARD',year:2017, imageUrl:'http://www.photographyblog.com/images/photographyblog/badges/badge-stars-@2x-4-5.jpg',status:Status.ACTIVE]
        ]

        awards.each{ a ->
            def name = a.name
            Award award = Award.findByName(name)
            if(award==null){
                new Award(a).save(failOnError:true)
            }
        }
    }
}
