package canon

import canon.constant.SettingCodeConstant
import canon.enums.Status
import groovy.json.JsonOutput
import org.apache.commons.lang.StringUtils

/**
 * Created by User on 11/12/2017.
 */
class SettingInit {
    def static init() {
        getSettingList().each {Setting s ->
            Setting gSetting = Setting.findBySettingCodeAndSite(s.settingCode, s.site);
            if (!gSetting) {
                s.save(flush: true, failOnError:true)
            }
        }
    }

    private static List<Setting> getSettingList(){
        Site site = Site.findByCode('singapore')
        Language language = Language.findById('en')

        List<Setting> result = new ArrayList<>()
        result.add(new Setting(site: site, language: language, settingCode:'link-desktop-1-1',settingName:'About Us',
                settingType: 'Company', settingValue:'#'))
        result.add(new Setting(site: site, language: language, settingCode:'link-desktop-1-2',settingName:'Press Room',
                settingType: 'Company', settingValue:'#'))
        result.add(new Setting(site: site, language: language, settingCode:'link-desktop-1-3',settingName:'Term of Use',
                settingType: 'Company', settingValue:'#'))
        result.add(new Setting(site: site, language: language, settingCode:'link-desktop-1-4',settingName:'Privacy Policy',
                settingType: 'Company', settingValue:'#'))
        result.add(new Setting(site: site, language: language, settingCode:'link-desktop-1-5',settingName:'Careers',
                settingType: 'Company', settingValue:'#'))
        result.add(new Setting(site: site, language: language, settingCode:'link-desktop-1-6',settingName:'FAQ',
                settingType: 'Company', settingValue:'#'))

        result.add(new Setting(site: site, language: language, settingCode:'link-desktop-2-1',settingName:'Photography',
                settingType: 'Products', settingValue:'#'))
        result.add(new Setting(site: site, language: language, settingCode:'link-desktop-2-2',settingName:'Printing',
                settingType: 'Products', settingValue:'#'))
        result.add(new Setting(site: site, language: language, settingCode:'link-desktop-2-3',settingName:'Scanning',
                settingType: 'Products', settingValue:'#'))
        result.add(new Setting(site: site, language: language, settingCode:'link-desktop-2-4',settingName:'Videography',
                settingType: 'Products', settingValue:'#'))
        result.add(new Setting(site: site, language: language, settingCode:'link-desktop-2-5',settingName:'Presentation',
                settingType: 'Products', settingValue:'#'))
        result.add(new Setting(site: site, language: language, settingCode:'link-desktop-2-6',settingName:'Other',
                settingType: 'Products', settingValue:'#'))

        result.add(new Setting(site: site, language: language, settingCode:'link-desktop-3-1',settingName:'Support & Downloads',
                settingType: 'Services', settingValue:'#'))
        result.add(new Setting(site: site, language: language, settingCode:'link-desktop-3-2',settingName:'Where to buy',
                settingType: 'Services', settingValue:'#'))
        result.add(new Setting(site: site, language: language, settingCode:'link-desktop-3-3',settingName:'Service & Repair',
                settingType: 'Services', settingValue:'#'))
        result.add(new Setting(site: site, language: language, settingCode:'link-desktop-3-4',settingName:'EOS 1 Club',
                settingType: 'Services', settingValue:'#'))
        result.add(new Setting(site: site, language: language, settingCode:'link-desktop-3-5',settingName:'Canon Professional Service',
                settingType: 'Services', settingValue:'#'))

        result.add(new Setting(site: site, language: language, settingCode:'link-desktop-4-1',settingName:'Registration',
                settingType: 'Warranty', settingGroup:'Warranty', settingValue:'#'))
        result.add(new Setting(site: site, language: language, settingCode:'link-desktop-4-2',settingName:'Product Warranty',
                settingType: 'Warranty', settingGroup:'Warranty', settingValue:'#'))
        result.add(new Setting(site: site, language: language, settingCode:'link-desktop-4-3',settingName:'Information for tourist',
                settingType: 'Warranty', settingGroup:'Warranty', settingValue:'#'))

        result.add(new Setting(site: site, language: language, settingCode:'link-desktop-5-1',settingName:'Snapshot',
                settingType: 'Campaign', settingValue:'#'))
        result.add(new Setting(site: site, language: language, settingCode:'link-desktop-5-2',settingName:'EOS World',
                settingType: 'Campaign', settingValue:'#'))

        result.add(new Setting(site: site, language: language, settingCode:'facebook',settingName:'Facebook',
                settingType: 'Subscribe', settingValue:'https://www.facebook.com/'))
        result.add(new Setting(site: site, language: language, settingCode:'instagram',settingName:'Instagram',
                settingType: 'Subscribe', settingValue:'https://www.instagram.com/'))
        result.add(new Setting(site: site, language: language, settingCode:'youtube',settingName:'Youtube',
                settingType: 'Subscribe', settingValue:'https://www.youtube.com/'))
        result.add(new Setting(site: site, language: language, settingCode:'twitter',settingName:'Twitter',
                settingType: 'Subscribe', settingValue:'https://twitter.com/'))

        //Region setting
        result.add(new Setting(settingCode: SettingCodeConstant.REGION_SELECTOR, settingType: 'Global', settingValue: JsonOutput.toJson(regionList()).toString()))


        //Contact Setting
        result.addAll(getContactSettingList());

        return result
    }

    private static def regionList() {
        return [
                [
                        name: "Asia",
                        languages:[
                                [
                                        name: "English",
                                        url: "/?site=asia&language=en&segment=consumer"
                                ]
                        ]
                ],
                [
                        name: "Singapore",
                        languages:[
                                [
                                        name: "English",
                                        url: "/?site=singapore&language=en&segment=consumer"
                                ]
                        ]
                ],
                [
                        name: "Indonesia",
                        languages:[
                                [
                                        name: "English",
                                        url: "/?site=indonesia&language=en&segment=consumer"
                                ],
                                [
                                        name: "Bahasa",
                                        url: "/?site=indonesia&language=id&segment=consumer"
                                ]
                        ]
                ],
                [
                        name: "Malaysia",
                        languages:[
                                [
                                        name: "English",
                                        url: "/?site=malaysia&language=en&segment=consumer"
                                ]
                        ]
                ],
                [
                        name: "Hongkong",
                        languages:[
                                [
                                        name: "English",
                                        url: "http://www.canon.com.hk/en/corporate/home/index.do"
                                ],
                                [
                                        name: "Traditional Chinese",
                                        url: "http://www.canon.com.hk/tc/corporate/home/index.do"
                                ]
                        ]
                ],
                [
                        name: "India",
                        languages:[
                                [
                                        name: "English",
                                        url: "/?site=india&language=en&segment=consumer"
                                ]
                        ]
                ],
                [
                        name: "Philippines",
                        languages:[
                                [
                                        name: "English",
                                        url: "http://www.canon.com.ph/"
                                ]
                        ]
                ],
                [
                        name: "Thailand",
                        languages:[
                                [
                                        name: "English",
                                        url: "/?site=thailand&language=en&segment=consumer"
                                ],
                                [
                                        name: "Thai",
                                        url: "/?site=thailand&language=th&segment=consumer"
                                ]
                        ]
                ],[
                        name: "Taiwan",
                        languages:[
                                [
                                        name: "Chinese",
                                        url: "http://www.canon.com.tw/"
                                ]
                        ]
                ],
                [
                        name: "Vietnam",
                        languages:[
                                [
                                        name: "English",
                                        url: "/?site=vietnam&language=en&segment=consumer"
                                ],
                                [
                                        name: "Vietnamese",
                                        url: "/?site=vietnam&language=vn&segment=consumer"
                                ]
                        ]
                ],
        ]
    }


    private static String ASIA_EMAIL_LIST = "emailAdd.bangladesh.contact.formfield.select.sales.contact.formfield.select.sales.GlobalServices = GSD_Enquiries@canon.com.sg\n" +
            "emailAdd.brunei.contact.formfield.select.sales.contact.formfield.select.sales.GlobalServices = GSD_Enquiries@canon.com.sg\n" +
            "emailAdd.bhutan.contact.formfield.select.sales.contact.formfield.select.sales.GlobalServices = GSD_Enquiries@canon.com.sg\n" +
            "emailAdd.cambodia.contact.formfield.select.sales.contact.formfield.select.sales.GlobalServices = GSD_Enquiries@canon.com.sg\n" +
            "emailAdd.china.contact.formfield.select.sales.contact.formfield.select.sales.GlobalServices = GSD_Enquiries@canon.com.sg\n" +
            "emailAdd.hongkong.contact.formfield.select.sales.contact.formfield.select.sales.GlobalServices = GSD_Enquiries@canon.com.sg\n" +
            "emailAdd.laos.contact.formfield.select.sales.contact.formfield.select.sales.GlobalServices = GSD_Enquiries@canon.com.sg\n" +
            "emailAdd.maldives.contact.formfield.select.sales.contact.formfield.select.sales.GlobalServices = GSD_Enquiries@canon.com.sg\n" +
            "emailAdd.myanmar.contact.formfield.select.sales.contact.formfield.select.sales.GlobalServices = GSD_Enquiries@canon.com.sg\n" +
            "emailAdd.nepal.contact.formfield.select.sales.contact.formfield.select.sales.GlobalServices = GSD_Enquiries@canon.com.sg\n" +
            "emailAdd.pakistan.contact.formfield.select.sales.contact.formfield.select.sales.GlobalServices = GSD_Enquiries@canon.com.sg\n" +
            "emailAdd.philippines.contact.formfield.select.sales.contact.formfield.select.sales.GlobalServices = GSD_Enquiries@canon.com.sg\n" +
            "emailAdd.sri_lanka.contact.formfield.select.sales.contact.formfield.select.sales.GlobalServices = GSD_Enquiries@canon.com.sg\n" +
            "emailAdd.taiwan.contact.formfield.select.sales.contact.formfield.select.sales.GlobalServices = GSD_Enquiries@canon.com.sg\n" +
            "emailAdd.indonesia.contact.formfield.select.sales.contact.formfield.select.sales.GlobalServices = GSD_Enquiries@canon.com.sg\n" +
            "emailAdd.malaysia.contact.formfield.select.sales.contact.formfield.select.sales.GlobalServices = bis_enquiry@cmm.canon.com.my \n" +
            "emailAdd.vietnam.contact.formfield.select.sales.contact.formfield.select.sales.GlobalServices = GSD_Enquiries@canon.com.sg\n" +
            "emailAdd.singapore.contact.formfield.select.sales.contact.formfield.select.sales.GlobalServices =  globalservices@canon.com.sg\n" +
            "emailAdd.thailand.contact.formfield.select.sales.contact.formfield.select.sales.GlobalServices = GSD_Enquiries@canon.com.sg\n" +
            "emailAdd.india.contact.formfield.select.sales.contact.formfield.select.sales.GlobalServices = narendra.sharma@canon.co.in\n" +
            "\n" +
            "emailAdd.brunei.contact.formfield.select.sales.contact.formfield.select.sales.Copiers = cspl_office@canon.com.sg\n" +
            "emailAdd.brunei.contact.formfield.select.sales.contact.formfield.select.sales.IndusPrinters=soho@canon.com.sg\n" +
            "emailAdd.brunei.contact.formfield.select.sales.contact.formfield.select.sales.LargeFormatPrinters=lfp@canon.com.sg\n" +
            "emailAdd.brunei.contact.formfield.select.sales.contact.formfield.select.sales.OfficeHomePrinters=cspl_consumer@canon.com.sg\n" +
            "emailAdd.brunei.contact.formfield.select.sales.contact.formfield.select.sales.PhotographicProduct=cspl_consumer@canon.com.sg\n" +
            "emailAdd.brunei.contact.formfield.select.sales.contact.formfield.select.sales.SuppliesPaperConsumables=cspl_consumer@canon.com.sg\n" +
            "emailAdd.brunei.contact.formfield.select.sales.contact.formfield.select.sales.OpthalmicOpticalProduct=contact@dc.canon.com.sg\n" +
            "emailAdd.brunei.contact.formfield.select.sales.contact.formfield.select.sales.Productionprinters=contact@dc.canon.com.sg\n" +
            "emailAdd.brunei.contact.formfield.select.sales.contact.formfield.select.sales.Other=contact@dc.canon.com.sg\n" +
            "emailAdd.brunei.contact.formfield.select.services.contact.formfield.select.services.Copiers_services=cspl_wsss@canon.com.sg\n" +
            "emailAdd.brunei.contact.formfield.select.services.contact.formfield.select.services.BusinessSolutionsSoftware_services=cspl_wsss@canon.com.sg\n" +
            "emailAdd.brunei.contact.formfield.select.services.contact.formfield.select.services.LndustrialPrinters_services=cspl_wsss@canon.com.sg\n" +
            "emailAdd.brunei.contact.formfield.select.services.contact.formfield.select.services.OfficeHomePrinters_services=cspl_wsss@canon.com.sg\n" +
            "emailAdd.brunei.contact.formfield.select.services.contact.formfield.select.services.PhotographicProduct_services=cspl_wsss@canon.com.sg\n" +
            "emailAdd.brunei.contact.formfield.select.services.contact.formfield.select.services.Productionprinters=cspl_wsss@canon.com.sg\n" +
            "emailAdd.brunei.contact.formfield.select.services.contact.formfield.select.services.Other_services=cspl_wsss@canon.com.sg\n" +
            "emailAdd.brunei.contact.formfield.select.others.contact.formfield.select.others.Warranty_others=cspl_consumer@canon.com.sg\n" +
            "emailAdd.brunei.contact.formfield.select.others.contact.formfield.select.others.FeedbackComplaints_others=cdfeedback@canon.com.sg\n" +
            "emailAdd.brunei.contact.formfield.select.others.contact.formfield.select.others.MediaSponsorships_others=cspl_general@canon.com.sg\n" +
            "\n" +
            "emailAdd.bangladesh.contact.formfield.select.sales.contact.formfield.select.sales.Copiers = cspl_office@canon.com.sg\n" +
            "emailAdd.bangladesh.contact.formfield.select.sales.contact.formfield.select.sales.IndusPrinters=soho@canon.com.sg\n" +
            "emailAdd.bangladesh.contact.formfield.select.sales.contact.formfield.select.sales.LargeFormatPrinters=lfp@canon.com.sg\n" +
            "emailAdd.bangladesh.contact.formfield.select.sales.contact.formfield.select.sales.OfficeHomePrinters=cspl_consumer@canon.com.sg\n" +
            "emailAdd.bangladesh.contact.formfield.select.sales.contact.formfield.select.sales.PhotographicProduct=cspl_consumer@canon.com.sg\n" +
            "emailAdd.bangladesh.contact.formfield.select.sales.contact.formfield.select.sales.SuppliesPaperConsumables=cspl_consumer@canon.com.sg\n" +
            "emailAdd.bangladesh.contact.formfield.select.sales.contact.formfield.select.sales.OpthalmicOpticalProduct=contact@dc.canon.com.sg\n" +
            "emailAdd.bangladesh.contact.formfield.select.sales.contact.formfield.select.sales.Productionprinters=contact@dc.canon.com.sg\n" +
            "emailAdd.bangladesh.contact.formfield.select.sales.contact.formfield.select.sales.Other=contact@dc.canon.com.sg\n" +
            "emailAdd.bangladesh.contact.formfield.select.services.contact.formfield.select.services.Copiers_services=cspl_wsss@canon.com.sg\n" +
            "emailAdd.bangladesh.contact.formfield.select.services.contact.formfield.select.services.BusinessSolutionsSoftware_services=cspl_wsss@canon.com.sg\n" +
            "emailAdd.bangladesh.contact.formfield.select.services.contact.formfield.select.services.LndustrialPrinters_services=cspl_wsss@canon.com.sg\n" +
            "emailAdd.bangladesh.contact.formfield.select.services.contact.formfield.select.services.OfficeHomePrinters_services=cspl_wsss@canon.com.sg\n" +
            "emailAdd.bangladesh.contact.formfield.select.services.contact.formfield.select.services.PhotographicProduct_services=cspl_wsss@canon.com.sg\n" +
            "emailAdd.bangladesh.contact.formfield.select.services.contact.formfield.select.services.Productionprinters=cspl_wsss@canon.com.sg\n" +
            "emailAdd.bangladesh.contact.formfield.select.services.contact.formfield.select.services.Other_services=cspl_wsss@canon.com.sg\n" +
            "emailAdd.bangladesh.contact.formfield.select.others.contact.formfield.select.others.Warranty_others=cspl_consumer@canon.com.sg\n" +
            "emailAdd.bangladesh.contact.formfield.select.others.contact.formfield.select.others.FeedbackComplaints_others=cdfeedback@canon.com.sg\n" +
            "emailAdd.bangladesh.contact.formfield.select.others.contact.formfield.select.others.MediaSponsorships_others=cspl_general@canon.com.sg\n" +
            "\n" +
            "emailAdd.bhutan.contact.formfield.select.sales.contact.formfield.select.sales.Copiers = cspl_office@canon.com.sg\n" +
            "emailAdd.bhutan.contact.formfield.select.sales.contact.formfield.select.sales.IndusPrinters=soho@canon.com.sg\n" +
            "emailAdd.bhutan.contact.formfield.select.sales.contact.formfield.select.sales.LargeFormatPrinters=lfp@canon.com.sg\n" +
            "emailAdd.bhutan.contact.formfield.select.sales.contact.formfield.select.sales.OfficeHomePrinters=cspl_consumer@canon.com.sg\n" +
            "emailAdd.bhutan.contact.formfield.select.sales.contact.formfield.select.sales.PhotographicProduct=cspl_consumer@canon.com.sg\n" +
            "emailAdd.bhutan.contact.formfield.select.sales.contact.formfield.select.sales.SuppliesPaperConsumables=cspl_consumer@canon.com.sg\n" +
            "emailAdd.bhutan.contact.formfield.select.sales.contact.formfield.select.sales.OpthalmicOpticalProduct=contact@dc.canon.com.sg\n" +
            "emailAdd.bhutan.contact.formfield.select.sales.contact.formfield.select.sales.Productionprinters=contact@dc.canon.com.sg\n" +
            "emailAdd.bhutan.contact.formfield.select.sales.contact.formfield.select.sales.Other=contact@dc.canon.com.sg\n" +
            "emailAdd.bhutan.contact.formfield.select.services.contact.formfield.select.services.Copiers_services=cspl_wsss@canon.com.sg\n" +
            "emailAdd.bhutan.contact.formfield.select.services.contact.formfield.select.services.BusinessSolutionsSoftware_services=cspl_wsss@canon.com.sg\n" +
            "emailAdd.bhutan.contact.formfield.select.services.contact.formfield.select.services.LndustrialPrinters_services=cspl_wsss@canon.com.sg\n" +
            "emailAdd.bhutan.contact.formfield.select.services.contact.formfield.select.services.OfficeHomePrinters_services=cspl_wsss@canon.com.sg\n" +
            "emailAdd.bhutan.contact.formfield.select.services.contact.formfield.select.services.PhotographicProduct_services=cspl_wsss@canon.com.sg\n" +
            "emailAdd.bhutan.contact.formfield.select.services.contact.formfield.select.services.Productionprinters=cspl_wsss@canon.com.sg\n" +
            "emailAdd.bhutan.contact.formfield.select.services.contact.formfield.select.services.Other_services=cspl_wsss@canon.com.sg\n" +
            "emailAdd.bhutan.contact.formfield.select.others.contact.formfield.select.others.Warranty_others=cspl_consumer@canon.com.sg\n" +
            "emailAdd.bhutan.contact.formfield.select.others.contact.formfield.select.others.FeedbackComplaints_others=cdfeedback@canon.com.sg\n" +
            "emailAdd.bhutan.contact.formfield.select.others.contact.formfield.select.others.MediaSponsorships_others=cspl_general@canon.com.sg\n" +
            "\n" +
            "\n" +
            "emailAdd.cambodia.contact.formfield.select.sales.contact.formfield.select.sales.Copiers = cspl_office@canon.com.sg\n" +
            "emailAdd.cambodia.contact.formfield.select.sales.contact.formfield.select.sales.IndusPrinters=soho@canon.com.sg\n" +
            "emailAdd.cambodia.contact.formfield.select.sales.contact.formfield.select.sales.LargeFormatPrinters=lfp@canon.com.sg\n" +
            "emailAdd.cambodia.contact.formfield.select.sales.contact.formfield.select.sales.OfficeHomePrinters=cspl_consumer@canon.com.sg\n" +
            "emailAdd.cambodia.contact.formfield.select.sales.contact.formfield.select.sales.PhotographicProduct=cspl_consumer@canon.com.sg\n" +
            "emailAdd.cambodia.contact.formfield.select.sales.contact.formfield.select.sales.SuppliesPaperConsumables=cspl_consumer@canon.com.sg\n" +
            "emailAdd.cambodia.contact.formfield.select.sales.contact.formfield.select.sales.OpthalmicOpticalProduct=contact@dc.canon.com.sg\n" +
            "emailAdd.cambodia.contact.formfield.select.sales.contact.formfield.select.sales.Productionprinters=contact@dc.canon.com.sg\n" +
            "emailAdd.cambodia.contact.formfield.select.sales.contact.formfield.select.sales.Other=contact@dc.canon.com.sg\n" +
            "emailAdd.cambodia.contact.formfield.select.services.contact.formfield.select.services.Copiers_services=cspl_wsss@canon.com.sg\n" +
            "emailAdd.cambodia.contact.formfield.select.services.contact.formfield.select.services.BusinessSolutionsSoftware_services=cspl_wsss@canon.com.sg\n" +
            "emailAdd.cambodia.contact.formfield.select.services.contact.formfield.select.services.LndustrialPrinters_services=cspl_wsss@canon.com.sg\n" +
            "emailAdd.cambodia.contact.formfield.select.services.contact.formfield.select.services.OfficeHomePrinters_services=cspl_wsss@canon.com.sg\n" +
            "emailAdd.cambodia.contact.formfield.select.services.contact.formfield.select.services.PhotographicProduct_services=cspl_wsss@canon.com.sg\n" +
            "emailAdd.cambodia.contact.formfield.select.services.contact.formfield.select.services.Productionprinters=cspl_wsss@canon.com.sg\n" +
            "emailAdd.cambodia.contact.formfield.select.services.contact.formfield.select.services.Other_services=cspl_wsss@canon.com.sg\n" +
            "emailAdd.cambodia.contact.formfield.select.others.contact.formfield.select.others.Warranty_others=cspl_consumer@canon.com.sg\n" +
            "emailAdd.cambodia.contact.formfield.select.others.contact.formfield.select.others.FeedbackComplaints_others=cdfeedback@canon.com.sg\n" +
            "emailAdd.cambodia.contact.formfield.select.others.contact.formfield.select.others.MediaSponsorships_others=cspl_general@canon.com.sg\n" +
            "\n" +
            "\n" +
            "emailAdd.china.contact.formfield.select.sales.contact.formfield.select.sales.Copiers = cspl_office@canon.com.sg\n" +
            "emailAdd.china.contact.formfield.select.sales.contact.formfield.select.sales.IndusPrinters=soho@canon.com.sg\n" +
            "emailAdd.china.contact.formfield.select.sales.contact.formfield.select.sales.LargeFormatPrinters=lfp@canon.com.sg\n" +
            "emailAdd.china.contact.formfield.select.sales.contact.formfield.select.sales.OfficeHomePrinters=cspl_consumer@canon.com.sg\n" +
            "emailAdd.china.contact.formfield.select.sales.contact.formfield.select.sales.PhotographicProduct=cspl_consumer@canon.com.sg\n" +
            "emailAdd.china.contact.formfield.select.sales.contact.formfield.select.sales.SuppliesPaperConsumables=cspl_consumer@canon.com.sg\n" +
            "emailAdd.china.contact.formfield.select.sales.contact.formfield.select.sales.OpthalmicOpticalProduct=contact@dc.canon.com.sg\n" +
            "emailAdd.china.contact.formfield.select.sales.contact.formfield.select.sales.Productionprinters=contact@dc.canon.com.sg\n" +
            "emailAdd.china.contact.formfield.select.sales.contact.formfield.select.sales.Other=contact@dc.canon.com.sg\n" +
            "emailAdd.china.contact.formfield.select.services.contact.formfield.select.services.Copiers_services=cspl_wsss@canon.com.sg\n" +
            "emailAdd.china.contact.formfield.select.services.contact.formfield.select.services.BusinessSolutionsSoftware_services=cspl_wsss@canon.com.sg\n" +
            "emailAdd.china.contact.formfield.select.services.contact.formfield.select.services.LndustrialPrinters_services=cspl_wsss@canon.com.sg\n" +
            "emailAdd.china.contact.formfield.select.services.contact.formfield.select.services.OfficeHomePrinters_services=cspl_wsss@canon.com.sg\n" +
            "emailAdd.china.contact.formfield.select.services.contact.formfield.select.services.PhotographicProduct_services=cspl_wsss@canon.com.sg\n" +
            "emailAdd.china.contact.formfield.select.services.contact.formfield.select.services.Productionprinters=cspl_wsss@canon.com.sg\n" +
            "emailAdd.china.contact.formfield.select.services.contact.formfield.select.services.Other_services=cspl_wsss@canon.com.sg\n" +
            "emailAdd.china.contact.formfield.select.others.contact.formfield.select.others.Warranty_others=cspl_consumer@canon.com.sg\n" +
            "emailAdd.china.contact.formfield.select.others.contact.formfield.select.others.FeedbackComplaints_others=cdfeedback@canon.com.sg\n" +
            "emailAdd.china.contact.formfield.select.others.contact.formfield.select.others.MediaSponsorships_others=cspl_general@canon.com.sg\n" +
            "\n" +
            "emailAdd.hongkong.contact.formfield.select.sales.contact.formfield.select.sales.Copiers = cspl_office@canon.com.sg\n" +
            "emailAdd.hongkong.contact.formfield.select.sales.contact.formfield.select.sales.IndusPrinters=soho@canon.com.sg\n" +
            "emailAdd.hongkong.contact.formfield.select.sales.contact.formfield.select.sales.LargeFormatPrinters=lfp@canon.com.sg\n" +
            "emailAdd.hongkong.contact.formfield.select.sales.contact.formfield.select.sales.OfficeHomePrinters=cspl_consumer@canon.com.sg\n" +
            "emailAdd.hongkong.contact.formfield.select.sales.contact.formfield.select.sales.PhotographicProduct=cspl_consumer@canon.com.sg\n" +
            "emailAdd.hongkong.contact.formfield.select.sales.contact.formfield.select.sales.SuppliesPaperConsumables=cspl_consumer@canon.com.sg\n" +
            "emailAdd.hongkong.contact.formfield.select.sales.contact.formfield.select.sales.OpthalmicOpticalProduct=contact@dc.canon.com.sg\n" +
            "emailAdd.hongkong.contact.formfield.select.sales.contact.formfield.select.sales.Productionprinters=contact@dc.canon.com.sg\n" +
            "emailAdd.hongkong.contact.formfield.select.sales.contact.formfield.select.sales.Other=contact@dc.canon.com.sg\n" +
            "emailAdd.hongkong.contact.formfield.select.services.contact.formfield.select.services.Copiers_services=cspl_wsss@canon.com.sg\n" +
            "emailAdd.hongkong.contact.formfield.select.services.contact.formfield.select.services.BusinessSolutionsSoftware_services=cspl_wsss@canon.com.sg\n" +
            "emailAdd.hongkong.contact.formfield.select.services.contact.formfield.select.services.LndustrialPrinters_services=cspl_wsss@canon.com.sg\n" +
            "emailAdd.hongkong.contact.formfield.select.services.contact.formfield.select.services.OfficeHomePrinters_services=cspl_wsss@canon.com.sg\n" +
            "emailAdd.hongkong.contact.formfield.select.services.contact.formfield.select.services.PhotographicProduct_services=cspl_wsss@canon.com.sg\n" +
            "emailAdd.hongkong.contact.formfield.select.services.contact.formfield.select.services.Productionprinters=cspl_wsss@canon.com.sg\n" +
            "emailAdd.hongkong.contact.formfield.select.services.contact.formfield.select.services.Other_services=cspl_wsss@canon.com.sg\n" +
            "emailAdd.hongkong.contact.formfield.select.others.contact.formfield.select.others.Warranty_others=cspl_consumer@canon.com.sg\n" +
            "emailAdd.hongkong.contact.formfield.select.others.contact.formfield.select.others.FeedbackComplaints_others=cdfeedback@canon.com.sg\n" +
            "emailAdd.hongkong.contact.formfield.select.others.contact.formfield.select.others.MediaSponsorships_others=cspl_general@canon.com.sg\n" +
            "\n" +
            "\n" +
            "emailAdd.laos.contact.formfield.select.sales.contact.formfield.select.sales.Copiers = cspl_office@canon.com.sg\n" +
            "emailAdd.laos.contact.formfield.select.sales.contact.formfield.select.sales.IndusPrinters=soho@canon.com.sg\n" +
            "emailAdd.laos.contact.formfield.select.sales.contact.formfield.select.sales.LargeFormatPrinters=lfp@canon.com.sg\n" +
            "emailAdd.laos.contact.formfield.select.sales.contact.formfield.select.sales.OfficeHomePrinters=cspl_consumer@canon.com.sg\n" +
            "emailAdd.laos.contact.formfield.select.sales.contact.formfield.select.sales.PhotographicProduct=cspl_consumer@canon.com.sg\n" +
            "emailAdd.laos.contact.formfield.select.sales.contact.formfield.select.sales.SuppliesPaperConsumables=cspl_consumer@canon.com.sg\n" +
            "emailAdd.laos.contact.formfield.select.sales.contact.formfield.select.sales.OpthalmicOpticalProduct=contact@dc.canon.com.sg\n" +
            "emailAdd.laos.contact.formfield.select.sales.contact.formfield.select.sales.Productionprinters=contact@dc.canon.com.sg\n" +
            "emailAdd.laos.contact.formfield.select.sales.contact.formfield.select.sales.Other=contact@dc.canon.com.sg\n" +
            "emailAdd.laos.contact.formfield.select.services.contact.formfield.select.services.Copiers_services=cspl_wsss@canon.com.sg\n" +
            "emailAdd.laos.contact.formfield.select.services.contact.formfield.select.services.BusinessSolutionsSoftware_services=cspl_wsss@canon.com.sg\n" +
            "emailAdd.laos.contact.formfield.select.services.contact.formfield.select.services.LndustrialPrinters_services=cspl_wsss@canon.com.sg\n" +
            "emailAdd.laos.contact.formfield.select.services.contact.formfield.select.services.OfficeHomePrinters_services=cspl_wsss@canon.com.sg\n" +
            "emailAdd.laos.contact.formfield.select.services.contact.formfield.select.services.PhotographicProduct_services=cspl_wsss@canon.com.sg\n" +
            "emailAdd.laos.contact.formfield.select.services.contact.formfield.select.services.Productionprinters=cspl_wsss@canon.com.sg\n" +
            "emailAdd.laos.contact.formfield.select.services.contact.formfield.select.services.Other_services=cspl_wsss@canon.com.sg\n" +
            "emailAdd.laos.contact.formfield.select.others.contact.formfield.select.others.Warranty_others=cspl_consumer@canon.com.sg\n" +
            "emailAdd.laos.contact.formfield.select.others.contact.formfield.select.others.FeedbackComplaints_others=cdfeedback@canon.com.sg\n" +
            "emailAdd.laos.contact.formfield.select.others.contact.formfield.select.others.MediaSponsorships_others=cspl_general@canon.com.sg\n" +
            "\n" +
            "\n" +
            "emailAdd.maldives.contact.formfield.select.sales.contact.formfield.select.sales.Copiers = cspl_office@canon.com.sg\n" +
            "emailAdd.maldives.contact.formfield.select.sales.contact.formfield.select.sales.IndusPrinters=soho@canon.com.sg\n" +
            "emailAdd.maldives.contact.formfield.select.sales.contact.formfield.select.sales.LargeFormatPrinters=lfp@canon.com.sg\n" +
            "emailAdd.maldives.contact.formfield.select.sales.contact.formfield.select.sales.OfficeHomePrinters=cspl_consumer@canon.com.sg\n" +
            "emailAdd.maldives.contact.formfield.select.sales.contact.formfield.select.sales.PhotographicProduct=cspl_consumer@canon.com.sg\n" +
            "emailAdd.maldives.contact.formfield.select.sales.contact.formfield.select.sales.SuppliesPaperConsumables=cspl_consumer@canon.com.sg\n" +
            "emailAdd.maldives.contact.formfield.select.sales.contact.formfield.select.sales.OpthalmicOpticalProduct=contact@dc.canon.com.sg\n" +
            "emailAdd.maldives.contact.formfield.select.sales.contact.formfield.select.sales.Productionprinters=contact@dc.canon.com.sg\n" +
            "emailAdd.maldives.contact.formfield.select.sales.contact.formfield.select.sales.Other=contact@dc.canon.com.sg\n" +
            "emailAdd.maldives.contact.formfield.select.services.contact.formfield.select.services.Copiers_services=cspl_wsss@canon.com.sg\n" +
            "emailAdd.maldives.contact.formfield.select.services.contact.formfield.select.services.BusinessSolutionsSoftware_services=cspl_wsss@canon.com.sg\n" +
            "emailAdd.maldives.contact.formfield.select.services.contact.formfield.select.services.LndustrialPrinters_services=cspl_wsss@canon.com.sg\n" +
            "emailAdd.maldives.contact.formfield.select.services.contact.formfield.select.services.OfficeHomePrinters_services=cspl_wsss@canon.com.sg\n" +
            "emailAdd.maldives.contact.formfield.select.services.contact.formfield.select.services.PhotographicProduct_services=cspl_wsss@canon.com.sg\n" +
            "emailAdd.maldives.contact.formfield.select.services.contact.formfield.select.services.Productionprinters=cspl_wsss@canon.com.sg\n" +
            "emailAdd.maldives.contact.formfield.select.services.contact.formfield.select.services.Other_services=cspl_wsss@canon.com.sg\n" +
            "emailAdd.maldives.contact.formfield.select.others.contact.formfield.select.others.Warranty_others=cspl_consumer@canon.com.sg\n" +
            "emailAdd.maldives.contact.formfield.select.others.contact.formfield.select.others.FeedbackComplaints_others=cdfeedback@canon.com.sg\n" +
            "emailAdd.maldives.contact.formfield.select.others.contact.formfield.select.others.MediaSponsorships_others=cspl_general@canon.com.sg\n" +
            "\n" +
            "\n" +
            "\n" +
            "emailAdd.myanmar.contact.formfield.select.sales.contact.formfield.select.sales.Copiers = cspl_office@canon.com.sg\n" +
            "emailAdd.myanmar.contact.formfield.select.sales.contact.formfield.select.sales.IndusPrinters=soho@canon.com.sg\n" +
            "emailAdd.myanmar.contact.formfield.select.sales.contact.formfield.select.sales.LargeFormatPrinters=lfp@canon.com.sg\n" +
            "emailAdd.myanmar.contact.formfield.select.sales.contact.formfield.select.sales.OfficeHomePrinters=cspl_consumer@canon.com.sg\n" +
            "emailAdd.myanmar.contact.formfield.select.sales.contact.formfield.select.sales.PhotographicProduct=cspl_consumer@canon.com.sg\n" +
            "emailAdd.myanmar.contact.formfield.select.sales.contact.formfield.select.sales.SuppliesPaperConsumables=cspl_consumer@canon.com.sg\n" +
            "emailAdd.myanmar.contact.formfield.select.sales.contact.formfield.select.sales.OpthalmicOpticalProduct=contact@dc.canon.com.sg\n" +
            "emailAdd.myanmar.contact.formfield.select.sales.contact.formfield.select.sales.Productionprinters=contact@dc.canon.com.sg\n" +
            "emailAdd.myanmar.contact.formfield.select.sales.contact.formfield.select.sales.Other=contact@dc.canon.com.sg\n" +
            "emailAdd.myanmar.contact.formfield.select.services.contact.formfield.select.services.Copiers_services=cspl_wsss@canon.com.sg\n" +
            "emailAdd.myanmar.contact.formfield.select.services.contact.formfield.select.services.BusinessSolutionsSoftware_services=cspl_wsss@canon.com.sg\n" +
            "emailAdd.myanmar.contact.formfield.select.services.contact.formfield.select.services.LndustrialPrinters_services=cspl_wsss@canon.com.sg\n" +
            "emailAdd.myanmar.contact.formfield.select.services.contact.formfield.select.services.OfficeHomePrinters_services=cspl_wsss@canon.com.sg\n" +
            "emailAdd.myanmar.contact.formfield.select.services.contact.formfield.select.services.PhotographicProduct_services=cspl_wsss@canon.com.sg\n" +
            "emailAdd.myanmar.contact.formfield.select.services.contact.formfield.select.services.Productionprinters=cspl_wsss@canon.com.sg\n" +
            "emailAdd.myanmar.contact.formfield.select.services.contact.formfield.select.services.Other_services=cspl_wsss@canon.com.sg\n" +
            "emailAdd.myanmar.contact.formfield.select.others.contact.formfield.select.others.Warranty_others=cspl_consumer@canon.com.sg\n" +
            "emailAdd.myanmar.contact.formfield.select.others.contact.formfield.select.others.FeedbackComplaints_others=cdfeedback@canon.com.sg\n" +
            "emailAdd.myanmar.contact.formfield.select.others.contact.formfield.select.others.MediaSponsorships_others=cspl_general@canon.com.sg\n" +
            "\n" +
            "\n" +
            "\n" +
            "emailAdd.nepal.contact.formfield.select.sales.contact.formfield.select.sales.Copiers = cspl_office@canon.com.sg\n" +
            "emailAdd.nepal.contact.formfield.select.sales.contact.formfield.select.sales.IndusPrinters=soho@canon.com.sg\n" +
            "emailAdd.nepal.contact.formfield.select.sales.contact.formfield.select.sales.LargeFormatPrinters=lfp@canon.com.sg\n" +
            "emailAdd.nepal.contact.formfield.select.sales.contact.formfield.select.sales.OfficeHomePrinters=cspl_consumer@canon.com.sg\n" +
            "emailAdd.nepal.contact.formfield.select.sales.contact.formfield.select.sales.PhotographicProduct=cspl_consumer@canon.com.sg\n" +
            "emailAdd.nepal.contact.formfield.select.sales.contact.formfield.select.sales.SuppliesPaperConsumables=cspl_consumer@canon.com.sg\n" +
            "emailAdd.nepal.contact.formfield.select.sales.contact.formfield.select.sales.OpthalmicOpticalProduct=contact@dc.canon.com.sg\n" +
            "emailAdd.nepal.contact.formfield.select.sales.contact.formfield.select.sales.Productionprinters=contact@dc.canon.com.sg\n" +
            "emailAdd.nepal.contact.formfield.select.sales.contact.formfield.select.sales.Other=contact@dc.canon.com.sg\n" +
            "emailAdd.nepal.contact.formfield.select.services.contact.formfield.select.services.Copiers_services=cspl_wsss@canon.com.sg\n" +
            "emailAdd.nepal.contact.formfield.select.services.contact.formfield.select.services.BusinessSolutionsSoftware_services=cspl_wsss@canon.com.sg\n" +
            "emailAdd.nepal.contact.formfield.select.services.contact.formfield.select.services.LndustrialPrinters_services=cspl_wsss@canon.com.sg\n" +
            "emailAdd.nepal.contact.formfield.select.services.contact.formfield.select.services.OfficeHomePrinters_services=cspl_wsss@canon.com.sg\n" +
            "emailAdd.nepal.contact.formfield.select.services.contact.formfield.select.services.PhotographicProduct_services=cspl_wsss@canon.com.sg\n" +
            "emailAdd.nepal.contact.formfield.select.services.contact.formfield.select.services.Productionprinters=cspl_wsss@canon.com.sg\n" +
            "emailAdd.nepal.contact.formfield.select.services.contact.formfield.select.services.Other_services=cspl_wsss@canon.com.sg\n" +
            "emailAdd.nepal.contact.formfield.select.others.contact.formfield.select.others.Warranty_others=cspl_consumer@canon.com.sg\n" +
            "emailAdd.nepal.contact.formfield.select.others.contact.formfield.select.others.FeedbackComplaints_others=cdfeedback@canon.com.sg\n" +
            "emailAdd.nepal.contact.formfield.select.others.contact.formfield.select.others.MediaSponsorships_others=cspl_general@canon.com.sg\n" +
            "\n" +
            "\n" +
            "emailAdd.pakistan.contact.formfield.select.sales.contact.formfield.select.sales.Copiers = cspl_office@canon.com.sg\n" +
            "emailAdd.pakistan.contact.formfield.select.sales.contact.formfield.select.sales.IndusPrinters=soho@canon.com.sg\n" +
            "emailAdd.pakistan.contact.formfield.select.sales.contact.formfield.select.sales.LargeFormatPrinters=lfp@canon.com.sg\n" +
            "emailAdd.pakistan.contact.formfield.select.sales.contact.formfield.select.sales.OfficeHomePrinters=cspl_consumer@canon.com.sg\n" +
            "emailAdd.pakistan.contact.formfield.select.sales.contact.formfield.select.sales.PhotographicProduct=cspl_consumer@canon.com.sg\n" +
            "emailAdd.pakistan.contact.formfield.select.sales.contact.formfield.select.sales.SuppliesPaperConsumables=cspl_consumer@canon.com.sg\n" +
            "emailAdd.pakistan.contact.formfield.select.sales.contact.formfield.select.sales.OpthalmicOpticalProduct=contact@dc.canon.com.sg\n" +
            "emailAdd.pakistan.contact.formfield.select.sales.contact.formfield.select.sales.Productionprinters=contact@dc.canon.com.sg\n" +
            "emailAdd.pakistan.contact.formfield.select.sales.contact.formfield.select.sales.Other=contact@dc.canon.com.sg\n" +
            "emailAdd.pakistan.contact.formfield.select.services.contact.formfield.select.services.Copiers_services=cspl_wsss@canon.com.sg\n" +
            "emailAdd.pakistan.contact.formfield.select.services.contact.formfield.select.services.BusinessSolutionsSoftware_services=cspl_wsss@canon.com.sg\n" +
            "emailAdd.pakistan.contact.formfield.select.services.contact.formfield.select.services.LndustrialPrinters_services=cspl_wsss@canon.com.sg\n" +
            "emailAdd.pakistan.contact.formfield.select.services.contact.formfield.select.services.OfficeHomePrinters_services=cspl_wsss@canon.com.sg\n" +
            "emailAdd.pakistan.contact.formfield.select.services.contact.formfield.select.services.PhotographicProduct_services=cspl_wsss@canon.com.sg\n" +
            "emailAdd.pakistan.contact.formfield.select.services.contact.formfield.select.services.Productionprinters=cspl_wsss@canon.com.sg\n" +
            "emailAdd.pakistan.contact.formfield.select.services.contact.formfield.select.services.Other_services=cspl_wsss@canon.com.sg\n" +
            "emailAdd.pakistan.contact.formfield.select.others.contact.formfield.select.others.Warranty_others=cspl_consumer@canon.com.sg\n" +
            "emailAdd.pakistan.contact.formfield.select.others.contact.formfield.select.others.FeedbackComplaints_others=cdfeedback@canon.com.sg\n" +
            "emailAdd.pakistan.contact.formfield.select.others.contact.formfield.select.others.MediaSponsorships_others=cspl_general@canon.com.sg\n" +
            "\n" +
            "emailAdd.philippines.contact.formfield.select.sales.contact.formfield.select.sales.Copiers = cspl_office@canon.com.sg\n" +
            "emailAdd.philippines.contact.formfield.select.sales.contact.formfield.select.sales.IndusPrinters=soho@canon.com.sg\n" +
            "emailAdd.philippines.contact.formfield.select.sales.contact.formfield.select.sales.LargeFormatPrinters=lfp@canon.com.sg\n" +
            "emailAdd.philippines.contact.formfield.select.sales.contact.formfield.select.sales.OfficeHomePrinters=cspl_consumer@canon.com.sg\n" +
            "emailAdd.philippines.contact.formfield.select.sales.contact.formfield.select.sales.PhotographicProduct=cspl_consumer@canon.com.sg\n" +
            "emailAdd.philippines.contact.formfield.select.sales.contact.formfield.select.sales.SuppliesPaperConsumables=cspl_consumer@canon.com.sg\n" +
            "emailAdd.philippines.contact.formfield.select.sales.contact.formfield.select.sales.OpthalmicOpticalProduct=contact@dc.canon.com.sg\n" +
            "emailAdd.philippines.contact.formfield.select.sales.contact.formfield.select.sales.Productionprinters=contact@dc.canon.com.sg\n" +
            "emailAdd.philippines.contact.formfield.select.sales.contact.formfield.select.sales.Other=contact@dc.canon.com.sg\n" +
            "emailAdd.philippines.contact.formfield.select.services.contact.formfield.select.services.Copiers_services=cspl_wsss@canon.com.sg\n" +
            "emailAdd.philippines.contact.formfield.select.services.contact.formfield.select.services.BusinessSolutionsSoftware_services=cspl_wsss@canon.com.sg\n" +
            "emailAdd.philippines.contact.formfield.select.services.contact.formfield.select.services.LndustrialPrinters_services=cspl_wsss@canon.com.sg\n" +
            "emailAdd.philippines.contact.formfield.select.services.contact.formfield.select.services.OfficeHomePrinters_services=cspl_wsss@canon.com.sg\n" +
            "emailAdd.philippines.contact.formfield.select.services.contact.formfield.select.services.PhotographicProduct_services=cspl_wsss@canon.com.sg\n" +
            "emailAdd.philippines.contact.formfield.select.services.contact.formfield.select.services.Productionprinters=cspl_wsss@canon.com.sg\n" +
            "emailAdd.philippines.contact.formfield.select.services.contact.formfield.select.services.Other_services=cspl_wsss@canon.com.sg\n" +
            "emailAdd.philippines.contact.formfield.select.others.contact.formfield.select.others.Warranty_others=cspl_consumer@canon.com.sg\n" +
            "emailAdd.philippines.contact.formfield.select.others.contact.formfield.select.others.FeedbackComplaints_others=cdfeedback@canon.com.sg\n" +
            "emailAdd.philippines.contact.formfield.select.others.contact.formfield.select.others.MediaSponsorships_others=cspl_general@canon.com.sg\n" +
            "\n" +
            "\n" +
            "emailAdd.sri_lanka.contact.formfield.select.sales.contact.formfield.select.sales.Copiers = cspl_office@canon.com.sg\n" +
            "emailAdd.sri_lanka.contact.formfield.select.sales.contact.formfield.select.sales.IndusPrinters=soho@canon.com.sg\n" +
            "emailAdd.sri_lanka.contact.formfield.select.sales.contact.formfield.select.sales.LargeFormatPrinters=lfp@canon.com.sg\n" +
            "emailAdd.sri_lanka.contact.formfield.select.sales.contact.formfield.select.sales.OfficeHomePrinters=cspl_consumer@canon.com.sg\n" +
            "emailAdd.sri_lanka.contact.formfield.select.sales.contact.formfield.select.sales.PhotographicProduct=cspl_consumer@canon.com.sg\n" +
            "emailAdd.sri_lanka.contact.formfield.select.sales.contact.formfield.select.sales.SuppliesPaperConsumables=cspl_consumer@canon.com.sg\n" +
            "emailAdd.sri_lanka.contact.formfield.select.sales.contact.formfield.select.sales.OpthalmicOpticalProduct=contact@dc.canon.com.sg\n" +
            "emailAdd.sri_lanka.contact.formfield.select.sales.contact.formfield.select.sales.Productionprinters=contact@dc.canon.com.sg\n" +
            "emailAdd.sri_lanka.contact.formfield.select.sales.contact.formfield.select.sales.Other=contact@dc.canon.com.sg\n" +
            "emailAdd.sri_lanka.contact.formfield.select.services.contact.formfield.select.services.Copiers_services=cspl_wsss@canon.com.sg\n" +
            "emailAdd.sri_lanka.contact.formfield.select.services.contact.formfield.select.services.BusinessSolutionsSoftware_services=cspl_wsss@canon.com.sg\n" +
            "emailAdd.sri_lanka.contact.formfield.select.services.contact.formfield.select.services.LndustrialPrinters_services=cspl_wsss@canon.com.sg\n" +
            "emailAdd.sri_lanka.contact.formfield.select.services.contact.formfield.select.services.OfficeHomePrinters_services=cspl_wsss@canon.com.sg\n" +
            "emailAdd.sri_lanka.contact.formfield.select.services.contact.formfield.select.services.PhotographicProduct_services=cspl_wsss@canon.com.sg\n" +
            "emailAdd.sri_lanka.contact.formfield.select.services.contact.formfield.select.services.Productionprinters=cspl_wsss@canon.com.sg\n" +
            "emailAdd.sri_lanka.contact.formfield.select.services.contact.formfield.select.services.Other_services=cspl_wsss@canon.com.sg\n" +
            "emailAdd.sri_lanka.contact.formfield.select.others.contact.formfield.select.others.Warranty_others=cspl_consumer@canon.com.sg\n" +
            "emailAdd.sri_lanka.contact.formfield.select.others.contact.formfield.select.others.FeedbackComplaints_others=cdfeedback@canon.com.sg\n" +
            "emailAdd.sri_lanka.contact.formfield.select.others.contact.formfield.select.others.MediaSponsorships_others=cspl_general@canon.com.sg\n" +
            "\n" +
            "emailAdd.taiwan.contact.formfield.select.sales.contact.formfield.select.sales.Copiers = cspl_office@canon.com.sg\n" +
            "emailAdd.taiwan.contact.formfield.select.sales.contact.formfield.select.sales.IndusPrinters=soho@canon.com.sg\n" +
            "emailAdd.taiwan.contact.formfield.select.sales.contact.formfield.select.sales.LargeFormatPrinters=lfp@canon.com.sg\n" +
            "emailAdd.taiwan.contact.formfield.select.sales.contact.formfield.select.sales.OfficeHomePrinters=cspl_consumer@canon.com.sg\n" +
            "emailAdd.taiwan.contact.formfield.select.sales.contact.formfield.select.sales.PhotographicProduct=cspl_consumer@canon.com.sg\n" +
            "emailAdd.taiwan.contact.formfield.select.sales.contact.formfield.select.sales.SuppliesPaperConsumables=cspl_consumer@canon.com.sg\n" +
            "emailAdd.taiwan.contact.formfield.select.sales.contact.formfield.select.sales.OpthalmicOpticalProduct=contact@dc.canon.com.sg\n" +
            "emailAdd.taiwan.contact.formfield.select.sales.contact.formfield.select.sales.Productionprinters=contact@dc.canon.com.sg\n" +
            "emailAdd.taiwan.contact.formfield.select.sales.contact.formfield.select.sales.Other=contact@dc.canon.com.sg\n" +
            "emailAdd.taiwan.contact.formfield.select.services.contact.formfield.select.services.Copiers_services=cspl_wsss@canon.com.sg\n" +
            "emailAdd.taiwan.contact.formfield.select.services.contact.formfield.select.services.BusinessSolutionsSoftware_services=cspl_wsss@canon.com.sg\n" +
            "emailAdd.taiwan.contact.formfield.select.services.contact.formfield.select.services.LndustrialPrinters_services=cspl_wsss@canon.com.sg\n" +
            "emailAdd.taiwan.contact.formfield.select.services.contact.formfield.select.services.OfficeHomePrinters_services=cspl_wsss@canon.com.sg\n" +
            "emailAdd.taiwan.contact.formfield.select.services.contact.formfield.select.services.PhotographicProduct_services=cspl_wsss@canon.com.sg\n" +
            "emailAdd.taiwan.contact.formfield.select.services.contact.formfield.select.services.Productionprinters=cspl_wsss@canon.com.sg\n" +
            "emailAdd.taiwan.contact.formfield.select.services.contact.formfield.select.services.Other_services=cspl_wsss@canon.com.sg\n" +
            "emailAdd.taiwan.contact.formfield.select.others.contact.formfield.select.others.Warranty_others=cspl_consumer@canon.com.sg\n" +
            "emailAdd.taiwan.contact.formfield.select.others.contact.formfield.select.others.FeedbackComplaints_others=cdfeedback@canon.com.sg\n" +
            "emailAdd.taiwan.contact.formfield.select.others.contact.formfield.select.others.MediaSponsorships_others=cspl_general@canon.com.sg\n" +
            "\n" +
            "emailAdd.indonesia.contact.formfield.select.sales.contact.formfield.select.sales.Copiers = samafitro@samafitro.co.id \n" +
            "emailAdd.indonesia.contact.formfield.select.sales.contact.formfield.select.sales.IndusPrinters=samafitro@samafitro.co.id \n" +
            "emailAdd.indonesia.contact.formfield.select.sales.contact.formfield.select.sales.LargeFormatPrinters=canonsupport@datascrip.co.id\n" +
            "emailAdd.indonesia.contact.formfield.select.sales.contact.formfield.select.sales.OfficeHomePrinters=canonsupport@datascrip.co.id\n" +
            "emailAdd.indonesia.contact.formfield.select.sales.contact.formfield.select.sales.PhotographicProduct=canonsupport@datascrip.co.id\n" +
            "emailAdd.indonesia.contact.formfield.select.sales.contact.formfield.select.sales.SuppliesPaperConsumables=canonservice@datascrip.co.id\n" +
            "emailAdd.indonesia.contact.formfield.select.sales.contact.formfield.select.sales.OpthalmicOpticalProduct=contact@dc.canon.com.sg\n" +
            "emailAdd.indonesia.contact.formfield.select.sales.contact.formfield.select.sales.Productionprinters=contact@dc.canon.com.sg\n" +
            "emailAdd.indonesia.contact.formfield.select.sales.contact.formfield.select.sales.Other=contact@dc.canon.com.sg\n" +
            "emailAdd.indonesia.contact.formfield.select.services.contact.formfield.select.services.Copiers_services=samafitro@samafitro.co.id \n" +
            "emailAdd.indonesia.contact.formfield.select.services.contact.formfield.select.services.BusinessSolutionsSoftware_services=samafitro@samafitro.co.id \n" +
            "emailAdd.Indonesia.contact.formfield.select.services.contact.formfield.select.services.LndustrialPrinters_services=samafitro@samafitro.co.id\n" +
            "emailAdd.indonesia.contact.formfield.select.services.contact.formfield.select.services.OfficeHomePrinters_services=canonservice@datascrip.co.id\n" +
            "emailAdd.indonesia.contact.formfield.select.services.contact.formfield.select.services.PhotographicProduct_services=canonservice@datascrip.co.id\n" +
            "emailAdd.indonesia.contact.formfield.select.services.contact.formfield.select.services.Productionprinters=cspl_wsss@canon.com.sg\n" +
            "emailAdd.indonesia.contact.formfield.select.services.contact.formfield.select.services.Other_services=canonservice@datascrip.co.id\n" +
            "emailAdd.indonesia.contact.formfield.select.others.contact.formfield.select.others.Warranty_others=canonservice@datascrip.co.id\n" +
            "emailAdd.indonesia.contact.formfield.select.others.contact.formfield.select.others.FeedbackComplaints_others=ccc_feedback@datascrip.co.id\n" +
            "emailAdd.indonesia.contact.formfield.select.others.contact.formfield.select.others.MediaSponsorships_others=canonsupport@datascrip.co.id\n" +
            "\n" +
            "\n" +
            "\n" +
            "emailAdd.malaysia.contact.formfield.select.sales.contact.formfield.select.sales.Copiers = helpdesk@agent.cvgs.net\n" +
            "emailAdd.malaysia.contact.formfield.select.sales.contact.formfield.select.sales.IndusPrinters=helpdesk@agent.cvgs.net\n" +
            "emailAdd.malaysia.contact.formfield.select.sales.contact.formfield.select.sales.LargeFormatPrinters=helpdesk@agent.cvgs.net\n" +
            "emailAdd.malaysia.contact.formfield.select.sales.contact.formfield.select.sales.OfficeHomePrinters=helpdesk@agent.cvgs.net\n" +
            "emailAdd.malaysia.contact.formfield.select.sales.contact.formfield.select.sales.PhotographicProduct=helpdesk@agent.cvgs.net\n" +
            "emailAdd.malaysia.contact.formfield.select.sales.contact.formfield.select.sales.SuppliesPaperConsumables=helpdesk@agent.cvgs.net\n" +
            "emailAdd.malaysia.contact.formfield.select.sales.contact.formfield.select.sales.OpthalmicOpticalProduct=helpdesk@agent.cvgs.net\n" +
            "emailAdd.malaysia.contact.formfield.select.sales.contact.formfield.select.sales.Productionprinters=helpdesk@agent.cvgs.net\n" +
            "emailAdd.malaysia.contact.formfield.select.sales.contact.formfield.select.sales.Other=helpdesk@agent.cvgs.net\n" +
            "emailAdd.malaysia.contact.formfield.select.services.contact.formfield.select.services.Copiers_services=helpdesk@agent.cvgs.net\n" +
            "emailAdd.malaysia.contact.formfield.select.services.contact.formfield.select.services.BusinessSolutionsSoftware_services=helpdesk@agent.cvgs.net\n" +
            "emailAdd.malaysia.contact.formfield.select.services.contact.formfield.select.services.LndustrialPrinters_services=helpdesk@agent.cvgs.net\n" +
            "emailAdd.malaysia.contact.formfield.select.services.contact.formfield.select.services.OfficeHomePrinters_services=helpdesk@agent.cvgs.net\n" +
            "emailAdd.malaysia.contact.formfield.select.services.contact.formfield.select.services.PhotographicProduct_services=helpdesk@agent.cvgs.net\n" +
            "emailAdd.malaysia.contact.formfield.select.services.contact.formfield.select.services.Productionprinters=helpdesk@agent.cvgs.net\n" +
            "emailAdd.malaysia.contact.formfield.select.services.contact.formfield.select.services.Other_services=helpdesk@agent.cvgs.net\n" +
            "emailAdd.malaysia.contact.formfield.select.others.contact.formfield.select.others.Warranty_others=helpdesk@agent.cvgs.net\n" +
            "emailAdd.malaysia.contact.formfield.select.others.contact.formfield.select.others.FeedbackComplaints_others=helpdesk@cmm.canon.com.my \n" +
            "emailAdd.malaysia.contact.formfield.select.others.contact.formfield.select.others.MediaSponsorships_others=helpdesk@cmm.canon.com.my \n" +
            "\n" +
            "emailAdd.vietnam.contact.formfield.select.sales.contact.formfield.select.sales.Copiers =cmv@canon.com.sg  \n" +
            "emailAdd.vietnam.contact.formfield.select.sales.contact.formfield.select.sales.IndusPrinters=cmv@canon.com.sg \n" +
            "emailAdd.vietnam.contact.formfield.select.sales.contact.formfield.select.sales.LargeFormatPrinters=cmv@canon.com.sg \n" +
            "emailAdd.vietnam.contact.formfield.select.sales.contact.formfield.select.sales.OfficeHomePrinters=cmv@canon.com.sg \n" +
            "emailAdd.vietnam.contact.formfield.select.sales.contact.formfield.select.sales.PhotographicProduct=cmv@canon.com.sg \n" +
            "emailAdd.vietnam.contact.formfield.select.sales.contact.formfield.select.sales.SuppliesPaperConsumables=cmv@canon.com.sg \n" +
            "emailAdd.vietnam.contact.formfield.select.sales.contact.formfield.select.sales.OpthalmicOpticalProduct=contact@dc.canon.com.sg\n" +
            "emailAdd.vietnam.contact.formfield.select.sales.contact.formfield.select.sales.Productionprinters=contact@dc.canon.com.sg\n" +
            "emailAdd.vietnam.contact.formfield.select.sales.contact.formfield.select.sales.Other=contact@dc.canon.com.sg\n" +
            "emailAdd.vietnam.contact.formfield.select.services.contact.formfield.select.services.Copiers_services=cmv@canon.com.sg \n" +
            "emailAdd.vietnam.contact.formfield.select.services.contact.formfield.select.services.BusinessSolutionsSoftware_services=cmv@canon.com.sg \n" +
            "emailAdd.vietnam.contact.formfield.select.services.contact.formfield.select.services.LndustrialPrinters_services=cmv@canon.com.sg \n" +
            "emailAdd.vietnam.contact.formfield.select.services.contact.formfield.select.services.Productionprinters=cspl_wsss@canon.com.sg\n" +
            "emailAdd.vietnam.contact.formfield.select.services.contact.formfield.select.services.OfficeHomePrinters_services=cmv@canon.com.sg \n" +
            "emailAdd.vietnam.contact.formfield.select.services.contact.formfield.select.services.PhotographicProduct_services=cmv@canon.com.sg \n" +
            "emailAdd.vietnam.contact.formfield.select.services.contact.formfield.select.services.Other_services=cmv@canon.com.sg \n" +
            "emailAdd.vietnam.contact.formfield.select.others.contact.formfield.select.others.Warranty_others=cmv@canon.com.sg \n" +
            "emailAdd.vietnam.contact.formfield.select.others.contact.formfield.select.others.FeedbackComplaints_others=cmv@canon.com.sg  \n" +
            "emailAdd.vietnam.contact.formfield.select.others.contact.formfield.select.others.MediaSponsorships_others=cmv@canon.com.sg \n" +
            "\n" +
            "\n" +
            "emailAdd.singapore.contact.formfield.select.sales.contact.formfield.select.sales.Copiers = contact@dc.canon.com.sg\n" +
            "emailAdd.singapore.contact.formfield.select.sales.contact.formfield.select.sales.IndusPrinters=contact@dc.canon.com.sg\n" +
            "emailAdd.singapore.contact.formfield.select.sales.contact.formfield.select.sales.LargeFormatPrinters=contact@dc.canon.com.sg\n" +
            "emailAdd.singapore.contact.formfield.select.sales.contact.formfield.select.sales.OfficeHomePrinters=contact@dc.canon.com.sg\n" +
            "emailAdd.singapore.contact.formfield.select.sales.contact.formfield.select.sales.PhotographicProduct=contact@dc.canon.com.sg\n" +
            "emailAdd.singapore.contact.formfield.select.sales.contact.formfield.select.sales.OpthalmicOpticalProduct=contact@dc.canon.com.sg\n" +
            "emailAdd.singapore.contact.formfield.select.sales.contact.formfield.select.sales.Productionprinters=contact@dc.canon.com.sg\n" +
            "emailAdd.singapore.contact.formfield.select.sales.contact.formfield.select.sales.Other=contact@dc.canon.com.sg\n" +
            "emailAdd.singapore.contact.formfield.select.sales.contact.formfield.select.sales.SuppliesPaperConsumables=contact@dc.canon.com.sg\n" +
            "emailAdd.singapore.contact.formfield.select.services.contact.formfield.select.services.Copiers_services=contact@dc.canon.com.sg\n" +
            "emailAdd.singapore.contact.formfield.select.services.contact.formfield.select.services.BusinessSolutionsSoftware_services=appsup@canon.com.sg\n" +
            "emailAdd.singapore.contact.formfield.select.services.contact.formfield.select.services.LndustrialPrinters_services=soho@canon.com.sg\n" +
            "emailAdd.singapore.contact.formfield.select.services.contact.formfield.select.services.Productionprinters=contact@dc.canon.com.sg\n" +
            "emailAdd.singapore.contact.formfield.select.services.contact.formfield.select.services.OfficeHomePrinters_services=contact@dc.canon.com.sg\n" +
            "emailAdd.singapore.contact.formfield.select.services.contact.formfield.select.services.PhotographicProduct_services=contact@dc.canon.com.sg\n" +
            "emailAdd.singapore.contact.formfield.select.services.contact.formfield.select.services.Other_services=contact@dc.canon.com.sg\n" +
            "emailAdd.singapore.contact.formfield.select.others.contact.formfield.select.others.Warranty_others=contact@dc.canon.com.sg\n" +
            "emailAdd.singapore.contact.formfield.select.others.contact.formfield.select.others.FeedbackComplaints_others=cdfeedback@canon.com.sg\n" +
            "emailAdd.singapore.contact.formfield.select.others.contact.formfield.select.others.MediaSponsorships_others=general@canon.com.sg\n" +
            "\n" +
            "emailAdd.thailand.contact.formfield.select.sales.contact.formfield.select.sales.Copiers=cmt-info@cmt.canon.co.th\n" +
            "emailAdd.thailand.contact.formfield.select.sales.contact.formfield.select.sales.IndusPrinters=cmt-info@cmt.canon.co.th\n" +
            "emailAdd.thailand.contact.formfield.select.sales.contact.formfield.select.sales.LargeFormatPrinters=cmt-info@cmt.canon.co.th\n" +
            "emailAdd.thailand.contact.formfield.select.sales.contact.formfield.select.sales.OfficeHomePrinters=cmt-info@cmt.canon.co.th\n" +
            "emailAdd.thailand.contact.formfield.select.sales.contact.formfield.select.sales.PhotographicProduct=cmt-info@cmt.canon.co.th\n" +
            "emailAdd.thailand.contact.formfield.select.sales.contact.formfield.select.sales.OpthalmicOpticalProduct=contact@dc.canon.com.sg\n" +
            "emailAdd.thailand.contact.formfield.select.sales.contact.formfield.select.sales.Productionprinters=contact@dc.canon.com.sg\n" +
            "emailAdd.thailand.contact.formfield.select.sales.contact.formfield.select.sales.Other=contact@dc.canon.com.sg\n" +
            "emailAdd.thailand.contact.formfield.select.sales.contact.formfield.select.sales.SuppliesPaperConsumables=www.canon.co.th/estore\n" +
            "emailAdd.thailand.contact.formfield.select.services.contact.formfield.select.services.Copiers_services=cmt-info@cmt.canon.co.th\n" +
            "emailAdd.thailand.contact.formfield.select.services.contact.formfield.select.services.BusinessSolutionsSoftware_services=cmt-info@cmt.canon.co.th\n" +
            "emailAdd.thailand.contact.formfield.select.services.contact.formfield.select.services.LndustrialPrinters_services=cmt-info@cmt.canon.co.th\n" +
            "emailAdd.thailand.contact.formfield.select.services.contact.formfield.select.services.OfficeHomePrinters_services=cmt-info@cmt.canon.co.th\n" +
            "emailAdd.thailand.contact.formfield.select.services.contact.formfield.select.services.PhotographicProduct_services=cmt-info@cmt.canon.co.th\n" +
            "emailAdd.thailand.contact.formfield.select.services.contact.formfield.select.services.Productionprinters=cspl_wsss@canon.com.sg\n" +
            "emailAdd.thailand.contact.formfield.select.services.contact.formfield.select.services.Other_services=cmt-info@cmt.canon.co.th\n" +
            "emailAdd.thailand.contact.formfield.select.others.contact.formfield.select.others.Warranty_others=http://www.canon.co.th/member/\n" +
            "emailAdd.thailand.contact.formfield.select.others.contact.formfield.select.others.RequestForOnSiteServices_others=cmt-service@cmt.canon.co.th\n" +
            "emailAdd.thailand.contact.formfield.select.others.contact.formfield.select.others.SubmitMeterReading_others=cmt-meter@cmt.canon.co.th\n" +
            "emailAdd.thailand.contact.formfield.select.others.contact.formfield.select.others.OrderConsumablesForCopiers_others=em-toner@cmt.canon.co.th\n" +
            "emailAdd.thailand.contact.formfield.select.others.contact.formfield.select.others.BillingEnquiries_others=cmt-billing@cmt.canon.co.th\n" +
            "emailAdd.thailand.contact.formfield.select.others.contact.formfield.select.others.RelocateCopiers_others=cmt-service@cmt.canon.co.th\n" +
            "emailAdd.thailand.contact.formfield.select.others.contact.formfield.select.others.FeedbackComplaints_others=www.canon.co.th/feedback\n" +
            "emailAdd.thailand.contact.formfield.select.others.contact.formfield.select.others.MediaSponsorships_others=cmt-corpcomm@cmt.canon.co.th\n" +
            "\n" +
            "emailAdd.india.contact.formfield.select.sales.contact.formfield.select.sales.Copiers = canonsupport@coecipl.com\n" +
            "emailAdd.india.contact.formfield.select.sales.contact.formfield.select.sales.IndusPrinters=CustomerSupport@canon.co.in\n" +
            "emailAdd.india.contact.formfield.select.sales.contact.formfield.select.sales.LargeFormatPrinters=canonsupport@coecipl.com\n" +
            "emailAdd.india.contact.formfield.select.sales.contact.formfield.select.sales.OfficeHomePrinters=canonsupport@coecipl.com\n" +
            "emailAdd.india.contact.formfield.select.sales.contact.formfield.select.sales.PhotographicProduct=canonsupport@coecipl.com\n" +
            "emailAdd.india.contact.formfield.select.sales.contact.formfield.select.sales.SuppliesPaperConsumables=canonsupport@coecipl.com\n" +
            "emailAdd.india.contact.formfield.select.sales.contact.formfield.select.sales.OpthalmicOpticalProduct=contact@dc.canon.com.sg\n" +
            "emailAdd.india.contact.formfield.select.sales.contact.formfield.select.sales.Productionprinters=contact@dc.canon.com.sg\n" +
            "emailAdd.india.contact.formfield.select.sales.contact.formfield.select.sales.Other=contact@dc.canon.com.sg\n" +
            "emailAdd.india.contact.formfield.select.services.contact.formfield.select.services.Copiers_services=canonsupport@coecipl.com\n" +
            "emailAdd.india.contact.formfield.select.services.contact.formfield.select.services.BusinessSolutionsSoftware_services=canonsupport@coecipl.com\n" +
            "emailAdd.india.contact.formfield.select.services.contact.formfield.select.services.LndustrialPrinters_services=CustomerSupport@canon.co.in\n" +
            "emailAdd.india.contact.formfield.select.services.contact.formfield.select.services.OfficeHomePrinters_services=canonsupport@coecipl.com\n" +
            "emailAdd.india.contact.formfield.select.services.contact.formfield.select.services.PhotographicProduct_services=canonsupport@coecipl.com\n" +
            "emailAdd.india.contact.formfield.select.services.contact.formfield.select.services.Productionprinters=cspl_wsss@canon.com.sg\n" +
            "emailAdd.india.contact.formfield.select.services.contact.formfield.select.services.Other_services=canonsupport@coecipl.com\n" +
            "emailAdd.india.contact.formfield.select.others.contact.formfield.select.others.Warranty_others=canonsupport@coecipl.com\n" +
            "emailAdd.india.contact.formfield.select.others.contact.formfield.select.others.FeedbackComplaints_others= customer.delight@canon.co.in\n" +
            "emailAdd.india.contact.formfield.select.others.contact.formfield.select.others.MediaSponsorships_others=customer.delight@canon.co.in";

    private static List<Setting> getContactSettingList(){
        List<Site> sites = Site.findAllByStatus(Status.ACTIVE);
        Language language = Language.findById('en');

        List<Setting> result = new ArrayList<>();

        for (int i = 0; i < sites.size(); i++) {
            Site site = sites.get(i);
            result.add(new Setting(settingCode: SettingCodeConstant.CONTACT_TYPE_OF_ENQUIRY, settingType: 'Contact',
                    settingName: SettingCodeConstant.CONTACT_TYPE_OF_ENQUIRY_NAME,
                    settingValue: SettingCodeConstant.CONTACT_TYPE_OF_ENQUIRY_SELECTOR_DEFAULT,
                    site: site, language: language));
        }

        result.add(new Setting(settingCode: SettingCodeConstant.CONTACT_SALUTATION, settingType: 'Contact',
                settingName: SettingCodeConstant.CONTACT_SALUTATION_NAME,
                settingValue: SettingCodeConstant.CONTACT_SALUTATION_DEFAULT,
                language: language));

        Site siteAsia = Site.findByCode("asia");

        result.add(new Setting(settingCode: SettingCodeConstant.CONTACT_LEFT_CONTENT, settingType: 'Contact',
                settingName: SettingCodeConstant.CONTACT_LEFT_CONTENT_NAME,
                settingValue: "<h4 class=\"font-light margin-bottom40\"> <b>Address</b><br/> 1 Fusionopolis Place,<br/> #15-10 Galaxis<br/> Singapore 138522</h4><p> <b>Contact number</b> (65) 6799 8888<br /> <b>Fax number</b> (65) 6799 8882<br /></p><p> <b>Logistics operations</b><br /> Menlo Worldwide Asia Pacific Pte Ltd<br /> 30 Boon Lay Way<br /> Singapore 609957<br /></p>",
                site: siteAsia,language: language));

        result.add(new Setting(settingCode: SettingCodeConstant.CONTACT_COUNTRIES, settingType: 'Contact',
                settingName: SettingCodeConstant.CONTACT_COUNTRIES_NAME,
                settingValue: SettingCodeConstant.CONTACT_COUNTRIES_DEFAULT,
                site: siteAsia,language: language));


        String [] asiaEmailArray = ASIA_EMAIL_LIST.split("[\n]");
        for (int i = 0; i < asiaEmailArray.length; i++) {
            String emailConfig = asiaEmailArray[i];
            if (StringUtils.isNotBlank(emailConfig)) {
                String [] emailConfigMap = emailConfig.split("=");
                String key = emailConfigMap[0].trim();
                String value = emailConfigMap[1].trim();
                result.add(new Setting(settingCode: key, settingType: 'Contact',
                        settingName: "Contact Email : " + key,
                        settingValue: value,
                        site: siteAsia,language: language));
            }
        }



        return result;
    }
}
