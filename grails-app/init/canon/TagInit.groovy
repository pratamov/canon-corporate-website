package canon

import canon.enums.Status

class TagInit {

    static init() {
        Language en = Language.findById("en")
        prepareData().each {
            Tag tag = Tag.findByName(it.name)
            if(!tag) {
                new Tag(name: it.name, status: it.status, language: en).save(failOnError: true)
            }
        }
    }

    private static def prepareData() {
        return [
                [name: "Tips & tutorials", status: Status.ACTIVE],
                [name: "General", status: Status.ACTIVE]
        ]
    }
}
