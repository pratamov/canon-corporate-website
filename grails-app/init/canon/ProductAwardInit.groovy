package canon

class ProductAwardInit {

    static init() {
        prepareData().each {k,v ->
            Product product = Product.findBySlug(k)
            if (product) {
                v.each {map ->
                    Site site = Site.findByCode(map.site)
                    Award award = Award.findByNameAndYear(map.award, map.year)
                    if(award) {
                        ProductAward pa = ProductAward.findByProductAndAwardAndSite(product, award, site)
                        if (!pa) new ProductAward(product: product, award: award, site: site).save(failOnError: true)
                    }
                }
            }
        }
    }

    private static def prepareData(){
        return [
                "eos-5d-mark-iii": [
                        [award: "TIPA AWARDS", year:2012 , site: 'asia']
                ],
                "eos-200d-colour": [
                        [award: "E Photo Zine AWARD", year: 2017, site: 'asia'],
                        [award: "Photography Blog AWARD", year: 2017, site: 'asia']
                ]
        ]
    }
}
