package canon

import canon.enums.Status

/**
 * Created by User on 05/12/2017.
 */
class LanguageInit {

    def static init(){
        def lang1 = Language.findById("en")
        if (lang1 == null) {
            lang1 = new Language(isoCode3: 'eng', name: 'English', status: Status.ACTIVE)
            lang1.id = "en"
            lang1.save(failOnError: true)
        }
        def lang2 = Language.findById("zh_CN")
        if (lang2 == null) {
            lang2 = new Language(isoCode3: 'chi', name: '中文', status: Status.ACTIVE)
            lang2.id = "zh_CN"
            lang2.save(failOnError: true)
        }
        def lang3 = Language.findById("de")
        if (lang3 == null) {
            lang3 = new Language(isoCode3: 'deu', name: 'German', status: Status.ACTIVE)
            lang3.id = "de"
            lang3.save(failOnError: true)
        }
        /*
        def lang4 = Language.findById("fr")
        if (lang4 == null) {
            new Language(id: 'fr',isoCode:'fra', name: 'French', status: Status.ACTIVE).save(failOnError: true)
        }
        def lang5 = Language.findById("it")
        if (lang5 == null) {
            new Language(id: 'it',isoCode:'ita', name: 'Italian', status: Status.ACTIVE).save(failOnError: true)
        }
        def lang6 = Language.findById("es")
        if (lang6 == null) {
            new Language(id: 'es',isoCode:'spa', name: 'Spanish', status: Status.ACTIVE).save(failOnError: true)
        }
        def lang7= Language.findById("nl")
        if (lang7 == null) {
            new Language(id: 'nl',isoCode:'dut', name: 'Dutch', status: Status.ACTIVE).save(failOnError: true)
        }
        def lang8 = Language.findById("zh_TW")
        if (lang8 == null) {
            new Language(id: 'zh_TW',isoCode:'zho', name: '中文', status: Status.ACTIVE).save(failOnError: true)
        }
        */
    }
}
