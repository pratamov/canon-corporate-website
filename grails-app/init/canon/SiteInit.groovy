package canon

import canon.enums.Status

class SiteInit {

    static init() {

        prepareData()?.each {Site site ->
            Site s = Site.findByCode(site.code)
            if(!s) site.save(failOnError: true)
        }

    }

    private static List<Site> prepareData() {
        return [
                new Site(code: 'asia', name: 'Asia', status: Status.ACTIVE, isDefault: Boolean.TRUE),
                new Site(country: Country.findByCode("SG"), code: 'singapore', name: 'Singapore', timeZone: 'UTC+08:00', status: Status.ACTIVE),
                new Site(country: Country.findByCode("ID"), code: 'indonesia', name: 'Indonesia', timeZone: 'UTC+07:00', status: Status.ACTIVE),
                new Site(country: Country.findByCode("MY"), code: 'malaysia', name: 'Malaysia', timeZone: 'UTC+08:00', status: Status.ACTIVE),
                new Site(country: Country.findByCode("IN"), code: 'india', name: 'India', timeZone: 'UTC+05:30', status: Status.ACTIVE),
                new Site(country: Country.findByCode("TH"), code: 'thailand', name: 'Thailand', timeZone: 'UTC+07:00', status: Status.ACTIVE),
                new Site(country: Country.findByCode("VN"), code: 'vietnam', name: 'Vietnam', timeZone: 'UTC+07:00', status: Status.ACTIVE),
                new Site(country: Country.findByCode("PH"), code: 'philippines', name: 'Philippines', timeZone: 'UTC+08:00', status: Status.ACTIVE),
        ]
    }
}
