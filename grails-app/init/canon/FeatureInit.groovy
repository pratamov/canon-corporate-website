package canon

import canon.enums.MediaSource
import canon.enums.MediaType
import canon.enums.Status

class FeatureInit {
    def static init() {
        User userBootstrap = User.findByUsername("bootstrap")

        prepareFeatures().each {Feature f ->
            Feature feature = Feature.findByName(f?.name)
            if(!feature) f.save(failOnError: true)
        }

    }

    private static def prepareFeatures(){
        Language en = Language.findById("en")
        List<Feature> features = new ArrayList<>()

        features.add(new Feature(name: "Dual Pixel", description: "Dual pixel features",
                iconMedia: new Media(url: "/assets/icon/dual_pixel_red.svg", fileName: "dual_pixel_red", mediaType: MediaType.IMAGE, source: MediaSource.INTERNAL,status: Status.PUBLISHED),
                iconTitle: "Dual Pixel", iconSubtitle: "CMOS AF", status: Status.PUBLISHED, language: en))
        features.add(new Feature(name: "45", description: "AF Points",
                iconMedia: new Media(url: "/assets/icon/af_points_red.svg", fileName: "dual_pixel_red", mediaType: MediaType.IMAGE, source: MediaSource.INTERNAL,status: Status.PUBLISHED),
                iconTitle: "45", iconSubtitle: "AF Points", status: Status.PUBLISHED, language: en))
        features.add(new Feature(name: "26.2", description: "Full frame censor",
                iconMedia: new Media(url: "/assets/icon/sensor_red.svg", fileName: "dual_pixel_red", mediaType: MediaType.IMAGE, source: MediaSource.INTERNAL,status: Status.PUBLISHED),
                iconTitle: "26.2", iconSubtitle: "Full frame censor", status: Status.PUBLISHED, language: en))

        // EOS 200D Kit
        features.add(new Feature(name: "24.2-megapixel APS-C sensor, Dual Pixel CMOS AF", description: "<div class=\"col-sm-6 margin-bottom20\"><img src=\"/assets/features/EOS-200D_Harbour.jpeg\" class=\"img-responsive\" alt=\"\"/></div><div class=\"col-sm-6\"><p class=\"margin-bottom0\">The EOS 200D features a large 24.2 MP, " +
                "APS-C-size CMOS sensor and the new DIGIC 7 image processor, enabling users to shoot in low-light conditions such as at night or indoors by setting their native ISO speed settings at up to 25600 (expandable to ISO 51200) to achieve images that retain all the details and colours with minimal noise. " +
                "Combining Canon's proprietary technology – Dual Pixel CMOS AF and a Vari-angle touchscreen LCD panel, enable you to focus and capture your subject easily while in Live View mode.</p></div>",
                iconMedia: new Media(url: "https://canon-corpweb-dev.s3.amazonaws.com/images/icon/dual_pixel_red.svg", fileName: "dual_pixel_red", mediaType: MediaType.IMAGE, source: MediaSource.INTERNAL,status: Status.PUBLISHED),
                iconTitle: "24.2 MP", iconSubtitle: "APS-C sensor", status: Status.PUBLISHED, language: en))
        features.add(new Feature(name: "Fun movie shooting functions, beginner-friendly Guided mode", description: "<div class=\"col-sm-6 margin-bottom20\"><img src=\"/assets/features/EOS-200D_Gallery_Image_7.jpeg\" class=\"img-responsive\" alt=\"\"/></div><div class=\"col-sm-6\"><p>Movies can be shot in Full HD 60p / 50p, " +
                "which is capable of capturing smooth footage of lively pets and children. 5 Creative filters for movies, such as ‘Old movies' and 'Dramatic B&W', are available for additional dramatic effect, as are functions to shoot HDR* and time-lapse movies** in Full HD. " +
                "For shooting still images, the EOS 200D also features a new 'Guided' menu interface where important camera settings are explained through graphical illustrations and photos, so even entry-level users can easily understand menu options, and then configure settings seamlessly by simply touching the large LCD monitor.</p><br/>" +
                "<p>*If <SCN> mode is selected during movie shooting, HDR movie shooting will apply.</p><br/><p class=\"margin-bottom0\">** When shooting time-lapse movie with EOS 200D, minimum shutter speed will be 1/30 for NTSC, and 1/25 for PAL.</p></div>",
                iconMedia: new Media(url: "https://canon-corpweb-dev.s3.amazonaws.com/images/icon/sensor_red.svg", fileName: "dual_pixel_red", mediaType: MediaType.IMAGE, source: MediaSource.INTERNAL,status: Status.PUBLISHED),
                iconTitle: "60p / 50p", iconSubtitle: "Shot in Full HD", status: Status.PUBLISHED, language: en))
        features.add(new Feature(name: "Share self-portraits and group portraits easily on social media", description: "<div class=\"col-sm-6 margin-bottom20\"><img src=\"/assets/features/Canon_EOS_200D_ShallowFocus_Portrait.jpeg\" class=\"img-responsive\" alt=\"\"/></div><div class=\"col-sm-6\"><p>Self-portraits can be taken by simply by rotating the 3.0-inch Vari-angle LCD monitor. " +
                "Wi-Fi / NFC, of course, features on the EOS 200D, allowing easy connection to a smartphone for the transfer of high-quality images that you can share on social media. One push of the dedicated Wi-Fi button allows for quick connection, especially with previously-paired devices*. " +
                "And with Bluetooth low energy connectivity**, a low-powered, constant connection is possible, allowing you to operate the camera remotely and switch to Wi-Fi seamlessly when you need to transfer files to your smart devices***. With such easy sharing of high quality images and videos, the EOS 200D is a very convenient DSLR camera to have on hand.</p><br/>" +
                "<p>*Establishing pairing beforehand is required. In some cases, an iOS smart device may require operation from the device side.</p><br/><p>**The following system requirements must be met for Bluetooth connectivity: <br/>For Android, Android 5.0 or later and Bluetooth 4.0 (Bluetooth compatible) or later are required. " +
                "For iOS, Bluetooth 4.0 (Bluetooth compatible) or later is required (iPhone 4s or later, iPad 3rd generation or later, iPod touch 5th generation or later). " +
                "Some devices that fulfill the above requirements may not be able to connect via Bluetooth.</p><br/><p class=\"margin-bottom0\">***Bluetooth, Wi-Fi and NFC connectivity with the camera requires Canon's dedicated application, Camera Connect, available for download free-of-charge from the App Store and Google Play.</p></div>",
                iconMedia: new Media(url: "https://canon-corpweb-dev.s3.amazonaws.com/images/icon/af_points_red.svg", fileName: "dual_pixel_red", mediaType: MediaType.IMAGE, source: MediaSource.INTERNAL,status: Status.PUBLISHED),
                iconTitle: "Wi-Fi / NFC", iconSubtitle: "Easy Connection", status: Status.PUBLISHED, language: en))
        return features
    }
}
