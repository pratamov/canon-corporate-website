package canon

import canon.enums.CategoryType
import canon.enums.MediaSource
import canon.enums.Segment
import canon.enums.Status
import grails.util.Holders

/**
 * Created by User on 05/12/2017.
 */
class CategoryInit {
    def static init() {

        //Preparation
        String awsBaseUrl = Holders.config.getProperty("amazonaws.s3.baseUrl")
        User userBootstrap = User.findByUsername("bootstrap")

        Media media1 = Media.findByTitle("Product Category Photography Media")
        Media media2 = Media.findByTitle("Product Category Printing Media")
        Media media3 = Media.findByTitle("Product Category Scanning Media")
        Media media4 = Media.findByTitle("Product Category Videography Media")
        Media media5 = Media.findByTitle("Product Category Presentation Media")
        Media media6 = Media.findByTitle("Product Category Business Software")
        Media media7 = Media.findByTitle("Product Category Network Visual Solutions")
        Media media8 = Media.findByTitle("Product Category Industrial Equipment")
        Media media9 = Media.findByTitle("Product Category Medical")

        /*Sub Category Media*/
        Media media_interchangeable_lens_cameras = Media.findByTitle("Product Category Interchangeable Lens Cameras")
        Media media_digital_compact_cameras = Media.findByTitle("Product Category Digital Compact Cameras")
        Media media_lenses = Media.findByTitle("Product Category Lenses")
        Media media_accessories = Media.findByTitle("Product Category Accessories")
        Media media_pixma = Media.findByTitle("Product Category PIXMA")
        Media media_maxify = Media.findByTitle("Product Category MAXIFY")
        Media media_laser = Media.findByTitle("Product Category Laser")
        Media media_selphy = Media.findByTitle("Product Category SELPHY")
        Media media_printer_supplies = Media.findByTitle("Product Category Supplies")
        Media media_flatbed = Media.findByTitle("Product Category Flatbed")
        Media media_flatbed_with_film = Media.findByTitle("Product Category Flatbed with Film")
        Media media_scanner_document = Media.findByTitle("Product Category Document")
        Media media_legria = Media.findByTitle("Product Category LEGRIA")
        Media media_professional_camcoder = Media.findByTitle("Product Category Professional Camcoders")
        Media media_cinema_eos_system = Media.findByTitle("Product Category Cinema EOS System")
        Media media_cinema_eos_lenses = Media.findByTitle("Product Category Cinema EOS Lenses")
        Media media_ef_lenses = Media.findByTitle("Product Category EF Lenses")
        Media media_video_accessories = Media.findByTitle("Product Category Video Accessories")
        Media media_projectors = Media.findByTitle("Product Category Projectors")
        Media media_presenters = Media.findByTitle("Product Category Presenters")
        Media media_binocular = Media.findByTitle("Product Category Binoculars")
        Media media_fax_machines = Media.findByTitle("Product Category Fax Machines")
        Media media_network_visual_solutions = Media.findByTitle("Product Category Network Visual Solutions")
        Media media_connect_station = Media.findByTitle("Product Category Connect Station")
        Media media_calculator = Media.findByTitle("Product Category Calculator")
        Media media_hd_photobook = Media.findByTitle("Product Category HD Photobook")
        Media media_multi_functional_device = Media.findByTitle("Product Category Multi Functional Devices")
        Media media_production_printing_system = Media.findByTitle("Product Category Production Printing Systems")
        Media media_tube_and_plate = Media.findByTitle("Product Category Tube and Plate")
        Media media_documents_solution = Media.findByTitle("Product Category Document Solutions")
        Media media_print_solutions = Media.findByTitle("Product Category Print Solutions")
        Media media_fleet_solutions = Media.findByTitle("Product Category Fleet Solutions")
        Media media_production_solutions = Media.findByTitle("Product Category Production Solutions")
        Media media_large_format_solutions = Media.findByTitle("Product Category Large Format Solutions")
        Media media_rm_monitoring_and_recording_software = Media.findByTitle("Product Category RM - Monitoring & Recording Software")
        Media media_ptz_network_cameras = Media.findByTitle("Product Category PTZ Network Cameras")
        Media media_fixed_dome_network_cameras = Media.findByTitle("Product Category Fixed Dome Network Cameras")
        Media media_fixed_box_network_camera = Media.findByTitle("Product Category Fixed Box Network Camera")
        Media media_broadcast_equipment = Media.findByTitle("Product Category Broadcast Equipment")
        Media media_fpd_exposure_equipment = Media.findByTitle("Product Category FPD Exposure Equipment")
        Media media_semiconductor_device = Media.findByTitle("Product Category Semiconductor Device Manufacturing Equipment")
        Media media_storage_device_manufacturing = Media.findByTitle("Product Category Storage Device Manufacturing Equipment")
        Media media_electronic_device_manufacturing = Media.findByTitle("Product Category Electronic Device Manufacturing and R&D Equipment")
        Media media_panel_device_manufacturing = Media.findByTitle("Product Category Panel Device Manufacturing Equipment")
        Media media_vacuum_component = Media.findByTitle("Product Category Vacuum Components")
        Media media_digital_retinal_camera = Media.findByTitle("Product Category Digital Retinal Cameras")
        Media media_keratometer = Media.findByTitle("Product Category Auto-refractors / Keratometers / Tonometers")


        Language english = Language.findById("en")
        //Site siteSg = Site.findByCode("singapore")
        Site asia = Site.findByCode("asia")

        // Consumer Main Category
        Category categoryC1 = Category.findBySlug("photography")
        if (!categoryC1) {
            categoryC1 = new Category(categoryType: CategoryType.PRODUCT, name: "Photography", slug: "photography", subText: "Imaging masterpiece", categoryOrder: 0, backgroundUrl: awsBaseUrl + "images/bg/img04.jpg", createdBy: userBootstrap, site: asia, language: english, segment: Segment.CONSUMER, status: Status.ACTIVE).save(failOnError: true)
        }

        CategoryMedia cmC1 = CategoryMedia.findByCategoryAndMedia(categoryC1, media1)
        if (!cmC1) {
            cmC1 = new CategoryMedia(category: categoryC1, media: media1, mediaUrl: media1?.url, mediaSource: MediaSource.INTERNAL).save(failOnError: true)
        }

        Category categoryC2 = Category.findBySlug("consumer-printing")
        if (!categoryC2) {
            categoryC2 = new Category(categoryType: CategoryType.PRODUCT,name: "Printing", slug: "consumer-printing", subText: "Vivid expression", categoryOrder: 1, backgroundUrl: awsBaseUrl + "images/bg/img05.jpg", createdBy: userBootstrap, site: asia, language: english, segment: Segment.CONSUMER, status: Status.ACTIVE).save(failOnError: true)
        }

        CategoryMedia cmC2 = CategoryMedia.findByCategoryAndMedia(categoryC2, media2)
        if (!cmC2) {
            cmC2 = new CategoryMedia(category: categoryC2, media: media2, mediaUrl: media2?.url, mediaSource: MediaSource.INTERNAL).save(failOnError: true)
        }

        Category categoryC3 = Category.findBySlug("scanning")
        if (!categoryC3) {
            categoryC3 = new Category(categoryType: CategoryType.PRODUCT,name: "Scanning", slug: "scanning", subText: "Pixel accurate", categoryOrder: 2, backgroundUrl: awsBaseUrl + "images/bg/img06.jpg", createdBy: userBootstrap, site: asia, language: english, segment: Segment.CONSUMER, status: Status.ACTIVE).save(failOnError: true)
        }

        CategoryMedia cmC3 = CategoryMedia.findByCategoryAndMedia(categoryC3, media3)
        if (!cmC3) {
            cmC3 = new CategoryMedia(category: categoryC3, media: media3, mediaUrl: media3?.url, mediaSource: MediaSource.INTERNAL).save(failOnError: true)
        }

        Category categoryC4 = Category.findBySlug("consumer-videography")
        if (!categoryC4) {
            categoryC4 = new Category(categoryType: CategoryType.PRODUCT,name: "Videography", slug: "consumer-videography", subText: "Cinematic perspective", categoryOrder: 3, backgroundUrl: awsBaseUrl + "images/bg/img08.jpg", createdBy: userBootstrap, site: asia, language: english, segment: Segment.CONSUMER, status: Status.ACTIVE).save(failOnError: true)
        }

        CategoryMedia cmC4 = CategoryMedia.findByCategoryAndMedia(categoryC4, media4)
        if (!cmC4) {
            cmC4 = new CategoryMedia(category: categoryC4, media: media4, mediaUrl: media4?.url, mediaSource: MediaSource.INTERNAL).save(failOnError: true)
        }

        Category categoryC5 = Category.findBySlug("consumer-presentation")
        if (!categoryC5) {
            categoryC5 = new Category(categoryType: CategoryType.PRODUCT,name: "Presentation", slug: "consumer-presentation", subText: "Visually stunning", categoryOrder: 4, backgroundUrl: awsBaseUrl + "images/bg/img09.jpg", createdBy: userBootstrap, site: asia, language: english, segment: Segment.CONSUMER, status: Status.ACTIVE).save(failOnError: true)
        }

        CategoryMedia cmC5 = CategoryMedia.findByCategoryAndMedia(categoryC5, media5)
        if (!cmC5) {
            cmC5 = new CategoryMedia(category: categoryC5, media: media5, mediaUrl: media5?.url, mediaSource: MediaSource.INTERNAL).save(failOnError: true)
        }

        Category categoryC6 = Category.findBySlug("consumer-others")
        if (!categoryC6) {
            categoryC6 = new Category(categoryType: CategoryType.PRODUCT,name: "Others", slug: "consumer-others", subText: "More from Canon", categoryOrder: 5, backgroundUrl: awsBaseUrl + "images/bg/img07.jpg", createdBy: userBootstrap, site: asia, language: english, segment: Segment.CONSUMER, status: Status.ACTIVE).save(failOnError: true)
        }

        // Business Main Category
        Category categoryB1 = Category.findBySlug("business-printing")
        if (!categoryB1) {
            categoryB1 = new Category(categoryType: CategoryType.PRODUCT,name: "Printing", slug: "business-printing", subText: "Vivid expression", categoryOrder: 0, backgroundUrl: awsBaseUrl + "images/bg/img05.jpg", createdBy: userBootstrap, site: asia, language: english, segment: Segment.BUSINESS, status: Status.ACTIVE).save(failOnError: true)
        }

        CategoryMedia cmB1 = CategoryMedia.findByCategoryAndMedia(categoryB1, media2)
        if (!cmB1) {
            cmB1 = new CategoryMedia(category: categoryB1, media: media2, mediaUrl: media2?.url, mediaSource: MediaSource.INTERNAL).save(failOnError: true)
        }

        Category categoryB2 = Category.findBySlug("software")
        if (!categoryB2) {
            categoryB2 = new Category(categoryType: CategoryType.PRODUCT,name: "Software", slug: "software", subText: "Office Environment", categoryOrder: 1, backgroundUrl: awsBaseUrl + "images/bg/img10.jpg", createdBy: userBootstrap, site: asia, language: english, segment: Segment.BUSINESS, status: Status.ACTIVE).save(failOnError: true)
        }

        CategoryMedia cmB2 = CategoryMedia.findByCategoryAndMedia(categoryB2, media6)
        if (!cmB2) {
            cmB2 = new CategoryMedia(category: categoryB2, media: media6, mediaUrl: media6?.url, mediaSource: MediaSource.INTERNAL).save(failOnError: true)
        }

        Category categoryB3 = Category.findBySlug("network-visual-solutions")
        if (!categoryB3) {
            categoryB3 = new Category(categoryType: CategoryType.PRODUCT,name: "Network Visual Solutions", slug: "network-visual-solutions", subText: "High-Quality Video Images", categoryOrder: 2, backgroundUrl: awsBaseUrl + "images/bg/img11.jpg", createdBy: userBootstrap, site: asia, language: english, segment: Segment.BUSINESS, status: Status.ACTIVE).save(failOnError: true)
        }

        CategoryMedia cmB3 = CategoryMedia.findByCategoryAndMedia(categoryB3, media7)
        if (!cmB3) {
            cmB3 = new CategoryMedia(category: categoryB3, media: media7, mediaUrl: media7?.url, mediaSource: MediaSource.INTERNAL).save(failOnError: true)
        }

        Category categoryB4 = Category.findBySlug("business-videography")
        if (!categoryB4) {
            categoryB4 = new Category(categoryType: CategoryType.PRODUCT,name: "Videography", slug: "business-videography", subText: "Cinematic perspective", categoryOrder: 3, backgroundUrl: awsBaseUrl + "images/bg/img08.jpg", createdBy: userBootstrap, site: asia, language: english, segment: Segment.BUSINESS, status: Status.ACTIVE).save(failOnError: true)
        }

        CategoryMedia cmB4 = CategoryMedia.findByCategoryAndMedia(categoryB4, media4)
        if (!cmB4) {
            cmB4 = new CategoryMedia(category: categoryB4, media: media4, mediaUrl: media4?.url, mediaSource: MediaSource.INTERNAL).save(failOnError: true)
        }

        Category categoryB5 = Category.findBySlug("business-presentation")
        if (!categoryB5) {
            categoryB5 = new Category(categoryType: CategoryType.PRODUCT,name: "Presentation", slug: "business-presentation", subText: "Visually stunning", categoryOrder: 4, backgroundUrl: awsBaseUrl + "images/bg/img09.jpg", createdBy: userBootstrap, site: asia, language: english, segment: Segment.BUSINESS, status: Status.ACTIVE).save(failOnError: true)
        }

        CategoryMedia cmB5 = CategoryMedia.findByCategoryAndMedia(categoryB5, media5)
        if (!cmB5) {
            cmB5 = new CategoryMedia(category: categoryB5, media: media5, mediaUrl: media5?.url, mediaSource: MediaSource.INTERNAL).save(failOnError: true)
        }


        Category categoryB6 = Category.findBySlug("industrial-equipment")
        if (!categoryB6) {
            categoryB6 = new Category(categoryType: CategoryType.PRODUCT, name: "Industrial Equipment", slug: "industrial-equipment", subText: "Lithography Equipments", categoryOrder: 5, backgroundUrl: awsBaseUrl + "images/bg/img04.jpg", createdBy: userBootstrap, site: asia, language: english, segment: Segment.BUSINESS, status: Status.ACTIVE).save(failOnError: true)
        }

        CategoryMedia cmB6 = CategoryMedia.findByCategoryAndMedia(categoryB6, media8)
        if (!cmB6) {
            cmB6 = new CategoryMedia(category: categoryB6, media: media8, mediaUrl: media8?.url, mediaSource: MediaSource.INTERNAL).save(failOnError: true)
        }

        Category categoryB7 = Category.findBySlug("medical")
        if (!categoryB7) {
            categoryB7 = new Category(categoryType: CategoryType.PRODUCT,name: "Medical", slug: "medical", subText: "Optics Technology ", categoryOrder: 6, backgroundUrl: awsBaseUrl + "images/bg/img06.jpg", createdBy: userBootstrap, site: asia, language: english, segment: Segment.BUSINESS, status: Status.ACTIVE).save(failOnError: true)
        }

        CategoryMedia cmB7 = CategoryMedia.findByCategoryAndMedia(categoryB7, media9)
        if (!cmB7) {
            cmB7 = new CategoryMedia(category: categoryB7, media: media9, mediaUrl: media9?.url, mediaSource: MediaSource.INTERNAL).save(failOnError: true)
        }

        Category categoryB8 = Category.findBySlug("business-others")
        if (!categoryB8) {
            categoryB8 = new Category(categoryType: CategoryType.PRODUCT,name: "Others", slug: "business-others", subText: "More from Canon", categoryOrder: 7, backgroundUrl: awsBaseUrl + "images/bg/img07.jpg", createdBy: userBootstrap, site: asia, language: english, segment: Segment.BUSINESS, status: Status.ACTIVE).save(failOnError: true)
        }

        // Sub Categories Consumer
        Category categoryC1a = Category.findBySlug("interchangeable-lens-camera")
        if(!categoryC1a){
            categoryC1a = new Category(
                    categoryType: CategoryType.PRODUCT, name: "Interchangeable Lens Cameras", slug: "interchangeable-lens-camera", subText: "", categoryOrder: 0, backgroundUrl: null, createdBy: userBootstrap, parent: categoryC1, site: asia, language: english, segment: Segment.CONSUMER, status: Status.ACTIVE).save(failOnError: true)
        }
        CategoryMedia cmC1a = CategoryMedia.findByCategoryAndMedia(categoryC1a, media_interchangeable_lens_cameras)
        if (!cmC1a) {
            cmC1a = new CategoryMedia(category: categoryC1a, media: media_interchangeable_lens_cameras, mediaUrl: media_interchangeable_lens_cameras?.url, mediaSource: MediaSource.INTERNAL).save(failOnError: true)
        }

        Category sbPhotography2 = Category.findBySlug("digital-compact-cameras")
        if(!sbPhotography2){
            sbPhotography2 = new Category(
                    categoryType: CategoryType.PRODUCT, name: "Digital Compact Cameras", slug: "digital-compact-cameras", subText: "", categoryOrder: 1, backgroundUrl: null, createdBy: userBootstrap, parent: categoryC1, site: asia, language: english, segment: Segment.CONSUMER, status: Status.ACTIVE).save(failOnError: true)
        }
        CategoryMedia cmsbPhotography2 = CategoryMedia.findByCategoryAndMedia(sbPhotography2, media_digital_compact_cameras)
        if (!cmsbPhotography2) {
            cmsbPhotography2 = new CategoryMedia(category: sbPhotography2, media: media_digital_compact_cameras, mediaUrl: media_digital_compact_cameras?.url, mediaSource: MediaSource.INTERNAL).save(failOnError: true)
        }

        Category sbPhotography3 = Category.findBySlug("lenses")
        if(!sbPhotography3){
            sbPhotography3 = new Category(
                    categoryType: CategoryType.PRODUCT, name: "Lenses", slug: "lenses", subText: "", categoryOrder: 2, backgroundUrl: null, createdBy: userBootstrap, parent: categoryC1, site: asia, language: english, segment: Segment.CONSUMER, status: Status.ACTIVE).save(failOnError: true)
        }
        CategoryMedia cmsbPhotography3 = CategoryMedia.findByCategoryAndMedia(sbPhotography3, media_lenses)
        if (!cmsbPhotography3) {
            cmsbPhotography3 = new CategoryMedia(category: sbPhotography3, media: media_lenses, mediaUrl: media_lenses?.url, mediaSource: MediaSource.INTERNAL).save(failOnError: true)
        }

        Category sbPhotography4 = Category.findBySlug("photography-accessories")
        if(!sbPhotography4){
            sbPhotography4 = new Category(
                    categoryType: CategoryType.PRODUCT, name: "Accessories", slug: "photography-accessories", subText: "", categoryOrder: 3, backgroundUrl: null, createdBy: userBootstrap, parent: categoryC1, site: asia, language: english, segment: Segment.CONSUMER, status: Status.ACTIVE).save(failOnError: true)
        }
        CategoryMedia cmsbPhotography4 = CategoryMedia.findByCategoryAndMedia(sbPhotography4, media_accessories)
        if (!cmsbPhotography4) {
            cmsbPhotography4 = new CategoryMedia(category: sbPhotography4, media: media_accessories, mediaUrl: media_accessories?.url, mediaSource: MediaSource.INTERNAL).save(failOnError: true)
        }

        // Sub Categories Printer
        Category sbPrinting1 = Category.findBySlug("pixma")
        if(!sbPrinting1){
            sbPrinting1 = new Category(
                    categoryType: CategoryType.PRODUCT, name: "PIXMA", slug: "pixma", subText: "", categoryOrder: 0, backgroundUrl: null, createdBy: userBootstrap, parent: categoryC2, site: asia, language: english, segment: Segment.CONSUMER, status: Status.ACTIVE).save(failOnError: true)
        }
        CategoryMedia cmsbPrinting1 = CategoryMedia.findByCategoryAndMedia(sbPrinting1, media_pixma)
        if (!cmsbPrinting1) {
            cmsbPrinting1 = new CategoryMedia(category: sbPrinting1, media: media_pixma, mediaUrl: media_pixma?.url, mediaSource: MediaSource.INTERNAL).save(failOnError: true)
        }

        Category sbPrinting2 = Category.findBySlug("maxify")
        if(!sbPrinting2){
            sbPrinting2 = new Category(
                    categoryType: CategoryType.PRODUCT, name: "MAXIFY", slug: "maxify", subText: "", categoryOrder: 1, backgroundUrl: null, createdBy: userBootstrap, parent: categoryC2, site: asia, language: english, segment: Segment.CONSUMER, status: Status.ACTIVE).save(failOnError: true)
        }
        CategoryMedia cmsbPrinting2 = CategoryMedia.findByCategoryAndMedia(sbPrinting2, media_maxify)
        if (!cmsbPrinting2) {
            cmsbPrinting2 = new CategoryMedia(category: sbPrinting2, media: media_maxify, mediaUrl: media_maxify?.url, mediaSource: MediaSource.INTERNAL).save(failOnError: true)
        }

        Category sbPrinting3 = Category.findBySlug("laser")
        if(!sbPrinting3){
            sbPrinting3 = new Category(
                    categoryType: CategoryType.PRODUCT, name: "Laser", slug: "laser", subText: "", categoryOrder: 2, backgroundUrl: null, createdBy: userBootstrap, parent: categoryC2, site: asia, language: english, segment: Segment.CONSUMER, status: Status.ACTIVE).save(failOnError: true)
        }
        CategoryMedia cmsbPrinting3 = CategoryMedia.findByCategoryAndMedia(sbPrinting3, media_laser)
        if (!cmsbPrinting3) {
            cmsbPrinting3 = new CategoryMedia(category: sbPrinting3, media: media_laser, mediaUrl: media_laser?.url, mediaSource: MediaSource.INTERNAL).save(failOnError: true)
        }

        Category sbPrinting4 = Category.findBySlug("selphy")
        if(!sbPrinting4){
            sbPrinting4 = new Category(
                    categoryType: CategoryType.PRODUCT, name: "SELPHY", slug: "selphy", subText: "", categoryOrder: 3, backgroundUrl: null, createdBy: userBootstrap, parent: categoryC2, site: asia, language: english, segment: Segment.CONSUMER, status: Status.ACTIVE).save(failOnError: true)
        }
        CategoryMedia cmsbPrinting4 = CategoryMedia.findByCategoryAndMedia(sbPrinting4, media_selphy)
        if (!cmsbPrinting4) {
            cmsbPrinting4 = new CategoryMedia(category: sbPrinting4, media: media_selphy, mediaUrl: media_selphy?.url, mediaSource: MediaSource.INTERNAL).save(failOnError: true)
        }

        Category sbPrinting5 = Category.findBySlug("printing-supplies")
        if(!sbPrinting5){
            sbPrinting5 = new Category(
                    categoryType: CategoryType.PRODUCT, name: "Supplies", slug: "printing-supplies", subText: "", categoryOrder: 4, backgroundUrl: null, createdBy: userBootstrap, parent: categoryC2, site: asia, language: english, segment: Segment.CONSUMER, status: Status.ACTIVE).save(failOnError: true)
        }
        CategoryMedia cmsbPrinting5 = CategoryMedia.findByCategoryAndMedia(sbPrinting5, media_printer_supplies)
        if (!cmsbPrinting5) {
            cmsbPrinting5 = new CategoryMedia(category: sbPrinting5, media: media_printer_supplies, mediaUrl: media_printer_supplies?.url, mediaSource: MediaSource.INTERNAL).save(failOnError: true)
        }

        // Sub Categories Scanning
        Category sbScanning1 = Category.findBySlug("flatbed")
        if(!sbScanning1){
            sbScanning1 = new Category(
                    categoryType: CategoryType.PRODUCT, name: "Flatbed", slug: "flatbed", subText: "", categoryOrder: 0, backgroundUrl: null, createdBy: userBootstrap, parent: categoryC3, site: asia, language: english, segment: Segment.CONSUMER, status: Status.ACTIVE).save(failOnError: true)
        }
        CategoryMedia cmsbScanning1 = CategoryMedia.findByCategoryAndMedia(sbScanning1, media_flatbed)
        if (!cmsbScanning1) {
            cmsbScanning1 = new CategoryMedia(category: sbScanning1, media: media_flatbed, mediaUrl: media_flatbed?.url, mediaSource: MediaSource.INTERNAL).save(failOnError: true)
        }

        Category sbScanning2 = Category.findBySlug("flatbed-with-film")
        if(!sbScanning2){
            sbScanning2 = new Category(
                    categoryType: CategoryType.PRODUCT, name: "Flatbed with Film", slug: "flatbed-with-film", subText: "", categoryOrder: 1, backgroundUrl: null, createdBy: userBootstrap, parent: categoryC3, site: asia, language: english, segment: Segment.CONSUMER, status: Status.ACTIVE).save(failOnError: true)
        }
        CategoryMedia cmsbScanning2 = CategoryMedia.findByCategoryAndMedia(sbScanning2, media_flatbed_with_film)
        if (!cmsbScanning2) {
            cmsbScanning2 = new CategoryMedia(category: sbScanning2, media: media_flatbed_with_film, mediaUrl: media_flatbed_with_film?.url, mediaSource: MediaSource.INTERNAL).save(failOnError: true)
        }

        Category sbScanning3 = Category.findBySlug("document")
        if(!sbScanning3){
            sbScanning3 = new Category(
                    categoryType: CategoryType.PRODUCT, name: "Document", slug: "document", subText: "", categoryOrder: 2, backgroundUrl: null, createdBy: userBootstrap, parent: categoryC3, site: asia, language: english, segment: Segment.CONSUMER, status: Status.ACTIVE).save(failOnError: true)
        }
        CategoryMedia cmsbScanning3 = CategoryMedia.findByCategoryAndMedia(sbScanning3, media_scanner_document)
        if (!cmsbScanning3) {
            cmsbScanning3 = new CategoryMedia(category: sbScanning3, media: media_scanner_document, mediaUrl: media_scanner_document?.url, mediaSource: MediaSource.INTERNAL).save(failOnError: true)
        }

        // Sub Categories Videography
        Category sbVideography1 = Category.findBySlug("legria")
        if(!sbVideography1){
            sbVideography1 = new Category(
                    categoryType: CategoryType.PRODUCT, name: "LEGRIA", slug: "legria", subText: "", categoryOrder: 0, backgroundUrl: null, createdBy: userBootstrap, parent: categoryC4, site: asia, language: english, segment: Segment.CONSUMER, status: Status.ACTIVE).save(failOnError: true)
        }
        CategoryMedia cmsbVideography1 = CategoryMedia.findByCategoryAndMedia(sbVideography1, media_legria)
        if (!cmsbVideography1) {
            cmsbVideography1 = new CategoryMedia(category: sbVideography1, media: media_legria, mediaUrl: media_legria?.url, mediaSource: MediaSource.INTERNAL).save(failOnError: true)
        }

        Category sbVideography2 = Category.findBySlug("professional-camcoders")
        if(!sbVideography2){
            sbVideography2 = new Category(
                    categoryType: CategoryType.PRODUCT, name: "Professional Camcoders", slug: "professional-camcoders", subText: "", categoryOrder: 1, backgroundUrl: null, createdBy: userBootstrap, parent: categoryC4, site: asia, language: english, segment: Segment.CONSUMER, status: Status.ACTIVE).save(failOnError: true)
        }
        CategoryMedia cmsbVideography2 = CategoryMedia.findByCategoryAndMedia(sbVideography2, media_professional_camcoder)
        if (!cmsbVideography2) {
            cmsbVideography2 = new CategoryMedia(category: sbVideography2, media: media_professional_camcoder, mediaUrl: media_professional_camcoder?.url, mediaSource: MediaSource.INTERNAL).save(failOnError: true)
        }

        Category sbVideography3 = Category.findBySlug("cinema-eos-system")
        if(!sbVideography3){
            sbVideography3 = new Category(
                    categoryType: CategoryType.PRODUCT, name: "Cinema EOS System", slug: "cinema-eos-system", subText: "", categoryOrder: 2, backgroundUrl: null, createdBy: userBootstrap, parent: categoryC4, site: asia, language: english, segment: Segment.CONSUMER, status: Status.ACTIVE).save(failOnError: true)
        }
        CategoryMedia cmsbVideography3 = CategoryMedia.findByCategoryAndMedia(sbVideography3, media_cinema_eos_system)
        if (!cmsbVideography3) {
            cmsbVideography3 = new CategoryMedia(category: sbVideography3, media: media_cinema_eos_system, mediaUrl: media_cinema_eos_system?.url, mediaSource: MediaSource.INTERNAL).save(failOnError: true)
        }

        Category sbVideography4 = Category.findBySlug("cinema-eos-lenses")
        if(!sbVideography4){
            sbVideography4 = new Category(
                    categoryType: CategoryType.PRODUCT, name: "Cinema EOS Lenses", slug: "cinema-eos-lenses", subText: "", categoryOrder: 3, backgroundUrl: null, createdBy: userBootstrap, parent: categoryC4, site: asia, language: english, segment: Segment.CONSUMER, status: Status.ACTIVE).save(failOnError: true)
        }
        CategoryMedia cmsbVideography4 = CategoryMedia.findByCategoryAndMedia(sbVideography4, media_cinema_eos_lenses)
        if (!cmsbVideography4) {
            cmsbVideography4 = new CategoryMedia(category: sbVideography4, media: media_cinema_eos_lenses, mediaUrl: media_cinema_eos_lenses?.url, mediaSource: MediaSource.INTERNAL).save(failOnError: true)
        }

        Category sbVideography5 = Category.findBySlug("ef-lenses")
        if(!sbVideography5){
            sbVideography5 = new Category(
                    categoryType: CategoryType.PRODUCT, name: "EF Lenses", slug: "ef-lenses", subText: "", categoryOrder: 4, backgroundUrl: null, createdBy: userBootstrap, parent: categoryC4, site: asia, language: english, segment: Segment.CONSUMER, status: Status.ACTIVE).save(failOnError: true)
        }
        CategoryMedia cmsbVideography5 = CategoryMedia.findByCategoryAndMedia(sbVideography5, media_ef_lenses)
        if (!cmsbVideography5) {
            cmsbVideography5 = new CategoryMedia(category: sbVideography5, media: media_ef_lenses, mediaUrl: media_ef_lenses?.url, mediaSource: MediaSource.INTERNAL).save(failOnError: true)
        }

        Category sbVideography6 = Category.findBySlug("videography-accessories")
        if(!sbVideography6){
            sbVideography6 = new Category(
                    categoryType: CategoryType.PRODUCT, name: "Accessories", slug: "videography-accessories", subText: "", categoryOrder: 5, backgroundUrl: null, createdBy: userBootstrap, parent: categoryC4, site: asia, language: english, segment: Segment.CONSUMER, status: Status.ACTIVE).save(failOnError: true)
        }
        CategoryMedia cmsbVideography6 = CategoryMedia.findByCategoryAndMedia(sbVideography6, media_video_accessories)
        if (!cmsbVideography6) {
            cmsbVideography6 = new CategoryMedia(category: sbVideography6, media: media_video_accessories, mediaUrl: media_video_accessories?.url, mediaSource: MediaSource.INTERNAL).save(failOnError: true)
        }

        // Sub Categories Presentation
        Category sbPresentation1 = Category.findBySlug("projectors")
        if(!sbPresentation1){
            sbPresentation1 = new Category(
                    categoryType: CategoryType.PRODUCT, name: "Projectors", slug: "projectors", subText: "", categoryOrder: 0, backgroundUrl: null, createdBy: userBootstrap, parent: categoryC5, site: asia, language: english, segment: Segment.CONSUMER, status: Status.ACTIVE).save(failOnError: true)
        }
        CategoryMedia cmsbPresentation1 = CategoryMedia.findByCategoryAndMedia(sbPresentation1, media_projectors)
        if (!cmsbPresentation1) {
            cmsbPresentation1 = new CategoryMedia(category: sbPresentation1, media: media_projectors, mediaUrl: media_projectors?.url, mediaSource: MediaSource.INTERNAL).save(failOnError: true)
        }

        Category sbPresentation2 = Category.findBySlug("presenters")
        if(!sbPresentation2){
            sbPresentation2 = new Category(
                    categoryType: CategoryType.PRODUCT, name: "Presenters", slug: "presenters", subText: "", categoryOrder: 1, backgroundUrl: null, createdBy: userBootstrap, parent: categoryC5, site: asia, language: english, segment: Segment.CONSUMER, status: Status.ACTIVE).save(failOnError: true)
        }
        CategoryMedia cmsbPresentation2 = CategoryMedia.findByCategoryAndMedia(sbPresentation2, media_presenters)
        if (!cmsbPresentation2) {
            cmsbPresentation2 = new CategoryMedia(category: sbPresentation2, media: media_presenters, mediaUrl: media_presenters?.url, mediaSource: MediaSource.INTERNAL).save(failOnError: true)
        }

        // Sub Categories Others
        Category sbOthers1 = Category.findBySlug("others-supplies")
        if(!sbOthers1){
            sbOthers1 = new Category(
                    categoryType: CategoryType.PRODUCT, name: "Supplies", slug: "others-supplies", subText: "", categoryOrder: 0, backgroundUrl: null, createdBy: userBootstrap, parent: categoryC6, site: asia, language: english, segment: Segment.CONSUMER, status: Status.ACTIVE).save(failOnError: true)
        }
        CategoryMedia cmsbOthers1 = CategoryMedia.findByCategoryAndMedia(sbOthers1, media_printer_supplies)
        if (!cmsbOthers1) {
            cmsbOthers1 = new CategoryMedia(category: sbOthers1, media: media_printer_supplies, mediaUrl: media_printer_supplies?.url, mediaSource: MediaSource.INTERNAL).save(failOnError: true)
        }

        Category sbOthers2 = Category.findBySlug("binoculars")
        if(!sbOthers2){
            sbOthers2 = new Category(
                    categoryType: CategoryType.PRODUCT, name: "Binoculars", slug: "binoculars", subText: "", categoryOrder: 1, backgroundUrl: null, createdBy: userBootstrap, parent: categoryC6, site: asia, language: english, segment: Segment.CONSUMER, status: Status.ACTIVE).save(failOnError: true)
        }
        CategoryMedia cmsbOthers2 = CategoryMedia.findByCategoryAndMedia(sbOthers2, media_binocular)
        if (!cmsbOthers2) {
            cmsbOthers2 = new CategoryMedia(category: sbOthers2, media: media_binocular, mediaUrl: media_binocular?.url, mediaSource: MediaSource.INTERNAL).save(failOnError: true)
        }

        Category sbOthers3 = Category.findBySlug("fax-machines")
        if(!sbOthers3){
            sbOthers3 = new Category(
                    categoryType: CategoryType.PRODUCT, name: "Fax Machines", slug: "fax-machines", subText: "", categoryOrder: 2, backgroundUrl: null, createdBy: userBootstrap, parent: categoryC6, site: asia, language: english, segment: Segment.CONSUMER, status: Status.ACTIVE).save(failOnError: true)
        }
        CategoryMedia cmsbOthers3 = CategoryMedia.findByCategoryAndMedia(sbOthers3, media_fax_machines)
        if (!cmsbOthers3) {
            cmsbOthers3 = new CategoryMedia(category: sbOthers3, media: media_fax_machines, mediaUrl: media_fax_machines?.url, mediaSource: MediaSource.INTERNAL).save(failOnError: true)
        }

        Category sbOthers4 = Category.findBySlug("network-visual-solutions")
        if(!sbOthers4){
            sbOthers4 = new Category(
                    categoryType: CategoryType.PRODUCT, name: "Network Visual Solutions", slug: "network-visual-solutions", subText: "", categoryOrder: 3, backgroundUrl: null, createdBy: userBootstrap, parent: categoryC6, site: asia, language: english, segment: Segment.CONSUMER, status: Status.ACTIVE).save(failOnError: true)
        }
        CategoryMedia cmsbOthers4 = CategoryMedia.findByCategoryAndMedia(sbOthers4, media_network_visual_solutions)
        if (!cmsbOthers4) {
            cmsbOthers4 = new CategoryMedia(category: sbOthers4, media: media_network_visual_solutions, mediaUrl: media_network_visual_solutions?.url, mediaSource: MediaSource.INTERNAL).save(failOnError: true)
        }

        Category sbOthers5 = Category.findBySlug("connect-station")
        if(!sbOthers5){
            sbOthers5 = new Category(
                    categoryType: CategoryType.PRODUCT, name: "Connect Station", slug: "connect-station", subText: "", categoryOrder: 4, backgroundUrl: null, createdBy: userBootstrap, parent: categoryC6, site: asia, language: english, segment: Segment.CONSUMER, status: Status.ACTIVE).save(failOnError: true)
        }
        CategoryMedia cmsbOthers5 = CategoryMedia.findByCategoryAndMedia(sbOthers5, media_connect_station)
        if (!cmsbOthers5) {
            cmsbOthers5 = new CategoryMedia(category: sbOthers5, media: media_connect_station, mediaUrl: media_connect_station?.url, mediaSource: MediaSource.INTERNAL).save(failOnError: true)
        }

        Category sbOthers6 = Category.findBySlug("calculator")
        if(!sbOthers6){
            sbOthers6 = new Category(
                    categoryType: CategoryType.PRODUCT, name: "Calculator", slug: "calculator", subText: "", categoryOrder: 5, backgroundUrl: null, createdBy: userBootstrap, parent: categoryC6, site: asia, language: english, segment: Segment.CONSUMER, status: Status.ACTIVE).save(failOnError: true)
        }
        CategoryMedia cmsbOthers6 = CategoryMedia.findByCategoryAndMedia(sbOthers6, media_calculator)
        if (!cmsbOthers6) {
            cmsbOthers6 = new CategoryMedia(category: sbOthers6, media: media_calculator, mediaUrl: media_calculator?.url, mediaSource: MediaSource.INTERNAL).save(failOnError: true)
        }

        Category sbOthers7 = Category.findBySlug("hd-photobook")
        if(!sbOthers7){
            sbOthers7 = new Category(
                    categoryType: CategoryType.PRODUCT, name: "HD Photobook", slug: "hd-photobook", subText: "", categoryOrder: 6, backgroundUrl: null, createdBy: userBootstrap, parent: categoryC6, site: asia, language: english, segment: Segment.CONSUMER, status: Status.ACTIVE).save(failOnError: true)
        }
        CategoryMedia cmsbOthers7 = CategoryMedia.findByCategoryAndMedia(sbOthers7, media_hd_photobook)
        if (!cmsbOthers7) {
            cmsbOthers7 = new CategoryMedia(category: sbOthers7, media: media_hd_photobook, mediaUrl: media_hd_photobook?.url, mediaSource: MediaSource.INTERNAL).save(failOnError: true)
        }

        /*Business Sub Category*/
        /*Printing*/
        Category bPrinting1 = Category.findBySlug("multi-functional-devices")
        if(!bPrinting1){
            bPrinting1 = new Category(
                    categoryType: CategoryType.PRODUCT, name: "Multi Functional Devices", slug: "multi-functional-devices", subText: "", categoryOrder: 0, backgroundUrl: null, createdBy: userBootstrap, parent: categoryB1, site: asia, language: english, segment: Segment.BUSINESS, status: Status.ACTIVE).save(failOnError: true)
        }
        CategoryMedia cmbPrinting1 = CategoryMedia.findByCategoryAndMedia(bPrinting1, media_multi_functional_device)
        if (!cmbPrinting1) {
            cmbPrinting1 = new CategoryMedia(category: bPrinting1, media: media_multi_functional_device, mediaUrl: media_multi_functional_device?.url, mediaSource: MediaSource.INTERNAL).save(failOnError: true)
        }

        Category bPrinting2 = Category.findBySlug("business-pixma")
        if(!bPrinting2){
            bPrinting2 = new Category(
                    categoryType: CategoryType.PRODUCT, name: "PIXMA", slug: "business-pixma", subText: "", categoryOrder: 1, backgroundUrl: null, createdBy: userBootstrap, parent: categoryB1, site: asia, language: english, segment: Segment.BUSINESS, status: Status.ACTIVE).save(failOnError: true)
        }
        CategoryMedia cmbPrinting2 = CategoryMedia.findByCategoryAndMedia(bPrinting2, media_pixma)
        if (!cmbPrinting2) {
            cmbPrinting2 = new CategoryMedia(category: bPrinting2, media: media_pixma, mediaUrl: media_pixma?.url, mediaSource: MediaSource.INTERNAL).save(failOnError: true)
        }

        Category bPrinting3 = Category.findBySlug("business-maxify")
        if(!bPrinting3){
            bPrinting3 = new Category(
                    categoryType: CategoryType.PRODUCT, name: "MAXIFY", slug: "business-maxify", subText: "", categoryOrder: 2, backgroundUrl: null, createdBy: userBootstrap, parent: categoryB1, site: asia, language: english, segment: Segment.BUSINESS, status: Status.ACTIVE).save(failOnError: true)
        }
        CategoryMedia cmbPrinting3 = CategoryMedia.findByCategoryAndMedia(bPrinting3, media_maxify)
        if (!cmbPrinting3) {
            cmbPrinting3 = new CategoryMedia(category: bPrinting3, media: media_maxify, mediaUrl: media_maxify?.url, mediaSource: MediaSource.INTERNAL).save(failOnError: true)
        }

        Category bPrinting4 = Category.findBySlug("business-laser")
        if(!bPrinting4){
            bPrinting4 = new Category(
                    categoryType: CategoryType.PRODUCT, name: "Laser", slug: "business-laser", subText: "", categoryOrder: 3, backgroundUrl: null, createdBy: userBootstrap, parent: categoryB1, site: asia, language: english, segment: Segment.BUSINESS, status: Status.ACTIVE).save(failOnError: true)
        }
        CategoryMedia cmbPrinting4 = CategoryMedia.findByCategoryAndMedia(bPrinting4, media_laser)
        if (!cmbPrinting4) {
            cmbPrinting4 = new CategoryMedia(category: bPrinting4, media: media_laser, mediaUrl: media_laser?.url, mediaSource: MediaSource.INTERNAL).save(failOnError: true)
        }

        Category bPrinting5 = Category.findBySlug("large-format")
        if(!bPrinting5){
            bPrinting5 = new Category(
                    categoryType: CategoryType.PRODUCT, name: "Large Format", slug: "large-format", subText: "", categoryOrder: 4, backgroundUrl: null, createdBy: userBootstrap, parent: categoryB1, site: asia, language: english, segment: Segment.BUSINESS, status: Status.ACTIVE).save(failOnError: true)
        }
        CategoryMedia cmbPrinting5 = CategoryMedia.findByCategoryAndMedia(bPrinting5, media_large_format_solutions)
        if (!cmbPrinting5) {
            cmbPrinting5 = new CategoryMedia(category: bPrinting5, media: media_large_format_solutions, mediaUrl: media_large_format_solutions?.url, mediaSource: MediaSource.INTERNAL).save(failOnError: true)
        }

        Category bPrinting6 = Category.findBySlug("production-printing-systems")
        if(!bPrinting6){
            bPrinting6 = new Category(
                    categoryType: CategoryType.PRODUCT, name: "Production Printing Systems", slug: "production-printing-systems", subText: "", categoryOrder: 5, backgroundUrl: null, createdBy: userBootstrap, parent: categoryB1, site: asia, language: english, segment: Segment.BUSINESS, status: Status.ACTIVE).save(failOnError: true)
        }
        CategoryMedia cmbPrinting6 = CategoryMedia.findByCategoryAndMedia(bPrinting6, media_production_printing_system)
        if (!cmbPrinting6) {
            cmbPrinting6 = new CategoryMedia(category: bPrinting6, media: media_production_printing_system, mediaUrl: media_production_printing_system?.url, mediaSource: MediaSource.INTERNAL).save(failOnError: true)
        }

        Category bPrinting7 = Category.findBySlug("tube-and-plate")
        if(!bPrinting7){
            bPrinting7 = new Category(
                    categoryType: CategoryType.PRODUCT, name: "Tube and Plate", slug: "tube-and-plate", subText: "", categoryOrder: 6, backgroundUrl: null, createdBy: userBootstrap, parent: categoryB1, site: asia, language: english, segment: Segment.BUSINESS, status: Status.ACTIVE).save(failOnError: true)
        }
        CategoryMedia cmbPrinting7 = CategoryMedia.findByCategoryAndMedia(bPrinting7, media_tube_and_plate)
        if (!cmbPrinting7) {
            cmbPrinting7 = new CategoryMedia(category: bPrinting7, media: media_tube_and_plate, mediaUrl: media_tube_and_plate?.url, mediaSource: MediaSource.INTERNAL).save(failOnError: true)
        }

        Category bPrinting8 = Category.findBySlug("business-printing-supplies")
        if(!bPrinting8){
            bPrinting8 = new Category(
                    categoryType: CategoryType.PRODUCT, name: "Supplies", slug: "business-printing-supplies", subText: "", categoryOrder: 7, backgroundUrl: null, createdBy: userBootstrap, parent: categoryB1, site: asia, language: english, segment: Segment.BUSINESS, status: Status.ACTIVE).save(failOnError: true)
        }
        CategoryMedia cmbPrinting8 = CategoryMedia.findByCategoryAndMedia(bPrinting8, media_printer_supplies)
        if (!cmbPrinting8) {
            cmbPrinting8 = new CategoryMedia(category: bPrinting8, media: media_printer_supplies, mediaUrl: media_printer_supplies?.url, mediaSource: MediaSource.INTERNAL).save(failOnError: true)
        }

        /*Software*/
        Category bSoftware1 = Category.findBySlug("document-solutions")
        if(!bSoftware1){
            bSoftware1 = new Category(
                    categoryType: CategoryType.PRODUCT, name: "Document Solutions", slug: "document-solutions", subText: "", categoryOrder: 0, backgroundUrl: null, createdBy: userBootstrap, parent: categoryB2, site: asia, language: english, segment: Segment.BUSINESS, status: Status.ACTIVE).save(failOnError: true)
        }
        CategoryMedia cmbSoftware1 = CategoryMedia.findByCategoryAndMedia(bSoftware1, media_documents_solution)
        if (!cmbSoftware1) {
            cmbSoftware1 = new CategoryMedia(category: bSoftware1, media: media_documents_solution, mediaUrl: media_documents_solution?.url, mediaSource: MediaSource.INTERNAL).save(failOnError: true)
        }

        Category bSoftware2 = Category.findBySlug("print-solutions")
        if(!bSoftware2){
            bSoftware2 = new Category(
                    categoryType: CategoryType.PRODUCT, name: "Print Solutions", slug: "print-solutions", subText: "", categoryOrder: 1, backgroundUrl: null, createdBy: userBootstrap, parent: categoryB2, site: asia, language: english, segment: Segment.BUSINESS, status: Status.ACTIVE).save(failOnError: true)
        }
        CategoryMedia cmbSoftware2 = CategoryMedia.findByCategoryAndMedia(bSoftware2, media_print_solutions)
        if (!cmbSoftware2) {
            cmbSoftware2 = new CategoryMedia(category: bSoftware2, media: media_print_solutions, mediaUrl: media_print_solutions?.url, mediaSource: MediaSource.INTERNAL).save(failOnError: true)
        }

        Category bSoftware3 = Category.findBySlug("fleet-solutions")
        if(!bSoftware3){
            bSoftware3 = new Category(
                    categoryType: CategoryType.PRODUCT, name: "Fleet Solutions", slug: "fleet-solutions", subText: "", categoryOrder: 2, backgroundUrl: null, createdBy: userBootstrap, parent: categoryB2, site: asia, language: english, segment: Segment.BUSINESS, status: Status.ACTIVE).save(failOnError: true)
        }
        CategoryMedia cmbSoftware3 = CategoryMedia.findByCategoryAndMedia(bSoftware3, media_fleet_solutions)
        if (!cmbSoftware3) {
            cmbSoftware3 = new CategoryMedia(category: bSoftware3, media: media_fleet_solutions, mediaUrl: media_fleet_solutions?.url, mediaSource: MediaSource.INTERNAL).save(failOnError: true)
        }

        Category bSoftware4 = Category.findBySlug("production-solutions")
        if(!bSoftware4){
            bSoftware4 = new Category(
                    categoryType: CategoryType.PRODUCT, name: "Production Solutions", slug: "production-solutions", subText: "", categoryOrder: 3, backgroundUrl: null, createdBy: userBootstrap, parent: categoryB2, site: asia, language: english, segment: Segment.BUSINESS, status: Status.ACTIVE).save(failOnError: true)
        }
        CategoryMedia cmbSoftware4 = CategoryMedia.findByCategoryAndMedia(bSoftware4, media_production_solutions)
        if (!cmbSoftware4) {
            cmbSoftware4 = new CategoryMedia(category: bSoftware4, media: media_production_solutions, mediaUrl: media_production_solutions?.url, mediaSource: MediaSource.INTERNAL).save(failOnError: true)
        }

        Category bSoftware5 = Category.findBySlug("large-format-solutions")
        if(!bSoftware5){
            bSoftware5 = new Category(
                    categoryType: CategoryType.PRODUCT, name: "Large Format Solutions", slug: "large-format-solutions", subText: "", categoryOrder: 4, backgroundUrl: null, createdBy: userBootstrap, parent: categoryB2, site: asia, language: english, segment: Segment.BUSINESS, status: Status.ACTIVE).save(failOnError: true)
        }
        CategoryMedia cmbSoftware5 = CategoryMedia.findByCategoryAndMedia(bSoftware5, media_large_format_solutions)
        if (!cmbSoftware5) {
            cmbSoftware5 = new CategoryMedia(category: bSoftware5, media: media_large_format_solutions, mediaUrl: media_large_format_solutions?.url, mediaSource: MediaSource.INTERNAL).save(failOnError: true)
        }

        /*Network Visual Solutions*/
        Category bNvs1 = Category.findBySlug("rm-monitoring-and-recording-software")
        if(!bNvs1){
            bNvs1 = new Category(
                    categoryType: CategoryType.PRODUCT, name: "RM - Monitoring & Recording Software", slug: "rm-monitoring-and-recording-software", subText: "", categoryOrder: 0, backgroundUrl: null, createdBy: userBootstrap, parent: categoryB3, site: asia, language: english, segment: Segment.BUSINESS, status: Status.ACTIVE).save(failOnError: true)
        }
        CategoryMedia cmbNvs1 = CategoryMedia.findByCategoryAndMedia(bNvs1, media_rm_monitoring_and_recording_software)
        if (!cmbNvs1) {
            cmbNvs1 = new CategoryMedia(category: bNvs1, media: media_rm_monitoring_and_recording_software, mediaUrl: media_rm_monitoring_and_recording_software?.url, mediaSource: MediaSource.INTERNAL).save(failOnError: true)
        }

        Category bNvs2 = Category.findBySlug("ptz-network-cameras")
        if(!bNvs2){
            bNvs2 = new Category(
                    categoryType: CategoryType.PRODUCT, name: "PTZ Network Cameras", slug: "ptz-network-cameras", subText: "", categoryOrder: 1, backgroundUrl: null, createdBy: userBootstrap, parent: categoryB3, site: asia, language: english, segment: Segment.BUSINESS, status: Status.ACTIVE).save(failOnError: true)
        }
        CategoryMedia cmbNvs2 = CategoryMedia.findByCategoryAndMedia(bNvs2, media_ptz_network_cameras)
        if (!cmbNvs2) {
            cmbNvs2 = new CategoryMedia(category: bNvs2, media: media_ptz_network_cameras, mediaUrl: media_ptz_network_cameras?.url, mediaSource: MediaSource.INTERNAL).save(failOnError: true)
        }

        Category bNvs3 = Category.findBySlug("fixed-dome-network-cameras")
        if(!bNvs3){
            bNvs3 = new Category(
                    categoryType: CategoryType.PRODUCT, name: "Fixed Dome Network Cameras", slug: "fixed-dome-network-cameras", subText: "", categoryOrder: 2, backgroundUrl: null, createdBy: userBootstrap, parent: categoryB3, site: asia, language: english, segment: Segment.BUSINESS, status: Status.ACTIVE).save(failOnError: true)
        }
        CategoryMedia cmbNvs3 = CategoryMedia.findByCategoryAndMedia(bNvs3, media_fixed_dome_network_cameras)
        if (!cmbNvs3) {
            cmbNvs3 = new CategoryMedia(category: bNvs3, media: media_fixed_dome_network_cameras, mediaUrl: media_fixed_dome_network_cameras?.url, mediaSource: MediaSource.INTERNAL).save(failOnError: true)
        }

        Category bNvs4 = Category.findBySlug("fixed-box-network-cameras")
        if(!bNvs4){
            bNvs4 = new Category(
                    categoryType: CategoryType.PRODUCT, name: "Fixed Box Network Cameras", slug: "fixed-box-network-cameras", subText: "", categoryOrder: 3, backgroundUrl: null, createdBy: userBootstrap, parent: categoryB3, site: asia, language: english, segment: Segment.BUSINESS, status: Status.ACTIVE).save(failOnError: true)
        }
        CategoryMedia cmbNvs4 = CategoryMedia.findByCategoryAndMedia(bNvs4, media_fixed_box_network_camera)
        if (!cmbNvs4) {
            cmbNvs4 = new CategoryMedia(category: bNvs4, media: media_fixed_box_network_camera, mediaUrl: media_fixed_box_network_camera?.url, mediaSource: MediaSource.INTERNAL).save(failOnError: true)
        }

        /*Videography*/
        Category bVideography1 = Category.findBySlug("business-legria")
        if(!bVideography1){
            bVideography1 = new Category(
                    categoryType: CategoryType.PRODUCT, name: "LEGRIA", slug: "business-legria", subText: "", categoryOrder: 0, backgroundUrl: null, createdBy: userBootstrap, parent: categoryB4, site: asia, language: english, segment: Segment.BUSINESS, status: Status.ACTIVE).save(failOnError: true)
        }
        CategoryMedia cmbVideography1 = CategoryMedia.findByCategoryAndMedia(bVideography1, media_legria)
        if (!cmbVideography1) {
            cmbVideography1 = new CategoryMedia(category: bVideography1, media: media_legria, mediaUrl: media_legria?.url, mediaSource: MediaSource.INTERNAL).save(failOnError: true)
        }

        Category bVideography2 = Category.findBySlug("business-professional-camcoders")
        if(!bVideography2){
            bVideography2 = new Category(
                    categoryType: CategoryType.PRODUCT, name: "Professional Camcoders", slug: "business-professional-camcoders", subText: "", categoryOrder: 1, backgroundUrl: null, createdBy: userBootstrap, parent: categoryB4, site: asia, language: english, segment: Segment.BUSINESS, status: Status.ACTIVE).save(failOnError: true)
        }
        CategoryMedia cmbVideography2 = CategoryMedia.findByCategoryAndMedia(bVideography2, media_professional_camcoder)
        if (!cmbVideography2) {
            cmbVideography2 = new CategoryMedia(category: bVideography2, media: media_professional_camcoder, mediaUrl: media_professional_camcoder?.url, mediaSource: MediaSource.INTERNAL).save(failOnError: true)
        }

        Category bVideography3 = Category.findBySlug("business-cinema-eos-system")
        if(!bVideography3){
            bVideography3 = new Category(
                    categoryType: CategoryType.PRODUCT, name: "Cinema EOS System", slug: "business-cinema-eos-system", subText: "", categoryOrder: 2, backgroundUrl: null, createdBy: userBootstrap, parent: categoryB4, site: asia, language: english, segment: Segment.BUSINESS, status: Status.ACTIVE).save(failOnError: true)
        }
        CategoryMedia cmbVideography3 = CategoryMedia.findByCategoryAndMedia(bVideography3, media_cinema_eos_system)
        if (!cmbVideography3) {
            cmbVideography3 = new CategoryMedia(category: bVideography3, media: media_cinema_eos_system, mediaUrl: media_cinema_eos_system?.url, mediaSource: MediaSource.INTERNAL).save(failOnError: true)
        }

        Category bVideography4 = Category.findBySlug("business-cinema-eos-lenses")
        if(!bVideography4){
            bVideography4 = new Category(
                    categoryType: CategoryType.PRODUCT, name: "Cinema EOS Lenses", slug: "business-cinema-eos-lenses", subText: "", categoryOrder: 3, backgroundUrl: null, createdBy: userBootstrap, parent: categoryB4, site: asia, language: english, segment: Segment.BUSINESS, status: Status.ACTIVE).save(failOnError: true)
        }
        CategoryMedia cmbVideography4 = CategoryMedia.findByCategoryAndMedia(bVideography4, media_cinema_eos_lenses)
        if (!cmbVideography4) {
            cmbVideography4 = new CategoryMedia(category: bVideography4, media: media_cinema_eos_lenses, mediaUrl: media_cinema_eos_lenses?.url, mediaSource: MediaSource.INTERNAL).save(failOnError: true)
        }

        Category bVideography5 = Category.findBySlug("business-ef-lenses")
        if(!bVideography5){
            bVideography5 = new Category(
                    categoryType: CategoryType.PRODUCT, name: "EF Lenses", slug: "business-ef-lenses", subText: "", categoryOrder: 4, backgroundUrl: null, createdBy: userBootstrap, parent: categoryB4, site: asia, language: english, segment: Segment.BUSINESS, status: Status.ACTIVE).save(failOnError: true)
        }
        CategoryMedia cmbVideography5 = CategoryMedia.findByCategoryAndMedia(bVideography5, media_ef_lenses)
        if (!cmbVideography5) {
            cmbVideography5 = new CategoryMedia(category: bVideography5, media: media_ef_lenses, mediaUrl: media_ef_lenses?.url, mediaSource: MediaSource.INTERNAL).save(failOnError: true)
        }

        Category bVideography6 = Category.findBySlug("business-videography-accessories")
        if(!bVideography6){
            bVideography6 = new Category(
                    categoryType: CategoryType.PRODUCT, name: "Accessories", slug: "business-videography-accessories", subText: "", categoryOrder: 5, backgroundUrl: null, createdBy: userBootstrap, parent: categoryB4, site: asia, language: english, segment: Segment.BUSINESS, status: Status.ACTIVE).save(failOnError: true)
        }
        CategoryMedia cmbVideography6 = CategoryMedia.findByCategoryAndMedia(bVideography6, media_video_accessories)
        if (!cmbVideography6) {
            cmbVideography6 = new CategoryMedia(category: bVideography6, media: media_video_accessories, mediaUrl: media_video_accessories?.url, mediaSource: MediaSource.INTERNAL).save(failOnError: true)
        }

        /*Presentation*/
        Category bPresentation1 = Category.findBySlug("business-projectors")
        if(!bPresentation1){
            bPresentation1 = new Category(
                    categoryType: CategoryType.PRODUCT, name: "Projectors", slug: "business-projectors", subText: "", categoryOrder: 0, backgroundUrl: null, createdBy: userBootstrap, parent: categoryB5, site: asia, language: english, segment: Segment.BUSINESS, status: Status.ACTIVE).save(failOnError: true)
        }
        CategoryMedia cmbPresentation1 = CategoryMedia.findByCategoryAndMedia(bPresentation1, media_projectors)
        if (!cmbPresentation1) {
            cmbPresentation1 = new CategoryMedia(category: bPresentation1, media: media_projectors, mediaUrl: media_projectors?.url, mediaSource: MediaSource.INTERNAL).save(failOnError: true)
        }

        Category bPresentation2 = Category.findBySlug("business-presenters")
        if(!bPresentation2){
            bPresentation2 = new Category(
                    categoryType: CategoryType.PRODUCT, name: "Presenters", slug: "business-presenters", subText: "", categoryOrder: 1, backgroundUrl: null, createdBy: userBootstrap, parent: categoryB5, site: asia, language: english, segment: Segment.BUSINESS, status: Status.ACTIVE).save(failOnError: true)
        }
        CategoryMedia cmbPresentation2 = CategoryMedia.findByCategoryAndMedia(bPresentation2, media_presenters)
        if (!cmbPresentation2) {
            cmbPresentation2 = new CategoryMedia(category: bPresentation2, media: media_presenters, mediaUrl: media_presenters?.url, mediaSource: MediaSource.INTERNAL).save(failOnError: true)
        }

        Category bPresentation3 = Category.findBySlug("broadcast-equipment")
        if(!bPresentation3){
            bPresentation3 = new Category(
                    categoryType: CategoryType.PRODUCT, name: "Broadcast Equipment", slug: "broadcast-equipment", subText: "", categoryOrder: 2, backgroundUrl: null, createdBy: userBootstrap, parent: categoryB5, site: asia, language: english, segment: Segment.BUSINESS, status: Status.ACTIVE).save(failOnError: true)
        }
        CategoryMedia cmbPresentation3 = CategoryMedia.findByCategoryAndMedia(bPresentation3, media_broadcast_equipment)
        if (!cmbPresentation3) {
            cmbPresentation3 = new CategoryMedia(category: bPresentation3, media: media_broadcast_equipment, mediaUrl: media_broadcast_equipment?.url, mediaSource: MediaSource.INTERNAL).save(failOnError: true)
        }

        /*Industrial Equipment*/
        Category bIndustrialEquipment1 = Category.findBySlug("fpd-exposure-equipment")
        if(!bIndustrialEquipment1){
            bIndustrialEquipment1 = new Category(
                    categoryType: CategoryType.PRODUCT, name: "FPD Exposure Equipment", slug: "fpd-exposure-equipment", subText: "", categoryOrder: 0, backgroundUrl: null, createdBy: userBootstrap, parent: categoryB6, site: asia, language: english, segment: Segment.BUSINESS, status: Status.ACTIVE).save(failOnError: true)
        }
        CategoryMedia cmbIndustrialEquipment1 = CategoryMedia.findByCategoryAndMedia(bIndustrialEquipment1, media_fpd_exposure_equipment)
        if (!cmbIndustrialEquipment1) {
            cmbIndustrialEquipment1 = new CategoryMedia(category: bIndustrialEquipment1, media: media_fpd_exposure_equipment, mediaUrl: media_fpd_exposure_equipment?.url, mediaSource: MediaSource.INTERNAL).save(failOnError: true)
        }

        Category bIndustrialEquipment2 = Category.findBySlug("semiconductor-device-manufacturing-equipment")
        if(!bIndustrialEquipment2){
            bIndustrialEquipment2 = new Category(
                    categoryType: CategoryType.PRODUCT, name: "Semiconductor Device Manufacturing Equipment", slug: "semiconductor-device-manufacturing-equipment", subText: "", categoryOrder: 1, backgroundUrl: null, createdBy: userBootstrap, parent: categoryB6, site: asia, language: english, segment: Segment.BUSINESS, status: Status.ACTIVE).save(failOnError: true)
        }
        CategoryMedia cmbIndustrialEquipment2 = CategoryMedia.findByCategoryAndMedia(bIndustrialEquipment2, media_semiconductor_device)
        if (!cmbIndustrialEquipment2) {
            cmbIndustrialEquipment2 = new CategoryMedia(category: bIndustrialEquipment2, media: media_semiconductor_device, mediaUrl: media_semiconductor_device?.url, mediaSource: MediaSource.INTERNAL).save(failOnError: true)
        }

        Category bIndustrialEquipment3 = Category.findBySlug("storage-device-manufacturing-equipment")
        if(!bIndustrialEquipment3){
            bIndustrialEquipment3 = new Category(
                    categoryType: CategoryType.PRODUCT, name: "Storage Device Manufacturing Equipment", slug: "storage-device-manufacturing-equipment", subText: "", categoryOrder: 2, backgroundUrl: null, createdBy: userBootstrap, parent: categoryB6, site: asia, language: english, segment: Segment.BUSINESS, status: Status.ACTIVE).save(failOnError: true)
        }
        CategoryMedia cmbIndustrialEquipment3 = CategoryMedia.findByCategoryAndMedia(bIndustrialEquipment3, media_storage_device_manufacturing)
        if (!cmbIndustrialEquipment3) {
            cmbIndustrialEquipment3 = new CategoryMedia(category: bIndustrialEquipment3, media: media_storage_device_manufacturing, mediaUrl: media_storage_device_manufacturing?.url, mediaSource: MediaSource.INTERNAL).save(failOnError: true)
        }

        Category bIndustrialEquipment4 = Category.findBySlug("electronic-device-manufacturing-and-r-and-d-equipment")
        if(!bIndustrialEquipment4){
            bIndustrialEquipment4 = new Category(
                    categoryType: CategoryType.PRODUCT, name: "Electronic Device Manufacturing and R&D Equipment", slug: "electronic-device-manufacturing-and-r-and-d-equipment", subText: "", categoryOrder: 3, backgroundUrl: null, createdBy: userBootstrap, parent: categoryB6, site: asia, language: english, segment: Segment.BUSINESS, status: Status.ACTIVE).save(failOnError: true)
        }
        CategoryMedia cmbIndustrialEquipment4 = CategoryMedia.findByCategoryAndMedia(bIndustrialEquipment4, media_electronic_device_manufacturing)
        if (!cmbIndustrialEquipment4) {
            cmbIndustrialEquipment4 = new CategoryMedia(category: bIndustrialEquipment4, media: media_electronic_device_manufacturing, mediaUrl: media_electronic_device_manufacturing?.url, mediaSource: MediaSource.INTERNAL).save(failOnError: true)
        }

        Category bIndustrialEquipment5 = Category.findBySlug("panel-device-manufacturing-equipment")
        if(!bIndustrialEquipment5){
            bIndustrialEquipment5 = new Category(
                    categoryType: CategoryType.PRODUCT, name: "Panel Device Manufacturing Equipment", slug: "panel-device-manufacturing-equipment", subText: "", categoryOrder: 4, backgroundUrl: null, createdBy: userBootstrap, parent: categoryB6, site: asia, language: english, segment: Segment.BUSINESS, status: Status.ACTIVE).save(failOnError: true)
        }
        CategoryMedia cmbIndustrialEquipment5 = CategoryMedia.findByCategoryAndMedia(bIndustrialEquipment5, media_panel_device_manufacturing)
        if (!cmbIndustrialEquipment5) {
            cmbIndustrialEquipment5 = new CategoryMedia(category: bIndustrialEquipment5, media: media_panel_device_manufacturing, mediaUrl: media_panel_device_manufacturing?.url, mediaSource: MediaSource.INTERNAL).save(failOnError: true)
        }

        Category bIndustrialEquipment6 = Category.findBySlug("vacuum-components")
        if(!bIndustrialEquipment6){
            bIndustrialEquipment6 = new Category(
                    categoryType: CategoryType.PRODUCT, name: "Vacuum Components", slug: "vacuum-components", subText: "", categoryOrder: 5, backgroundUrl: null, createdBy: userBootstrap, parent: categoryB6, site: asia, language: english, segment: Segment.BUSINESS, status: Status.ACTIVE).save(failOnError: true)
        }
        CategoryMedia cmbIndustrialEquipment6 = CategoryMedia.findByCategoryAndMedia(bIndustrialEquipment6, media_vacuum_component)
        if (!cmbIndustrialEquipment6) {
            cmbIndustrialEquipment6 = new CategoryMedia(category: bIndustrialEquipment6, media: media_vacuum_component, mediaUrl: media_vacuum_component?.url, mediaSource: MediaSource.INTERNAL).save(failOnError: true)
        }

        /*Medical*/
        Category bMedical1 = Category.findBySlug("digital-retinal-cameras")
        if(!bMedical1){
            bMedical1 = new Category(
                    categoryType: CategoryType.PRODUCT, name: "Digital Retinal Cameras", slug: "digital-retinal-cameras", subText: "", categoryOrder: 0, backgroundUrl: null, createdBy: userBootstrap, parent: categoryB7, site: asia, language: english, segment: Segment.BUSINESS, status: Status.ACTIVE).save(failOnError: true)
        }
        CategoryMedia cmbMedical1 = CategoryMedia.findByCategoryAndMedia(bMedical1, media_digital_retinal_camera)
        if (!cmbMedical1) {
            cmbMedical1 = new CategoryMedia(category: bMedical1, media: media_digital_retinal_camera, mediaUrl: media_digital_retinal_camera?.url, mediaSource: MediaSource.INTERNAL).save(failOnError: true)
        }

        Category bMedical2 = Category.findBySlug("keratometers")
        if(!bMedical2){
            bMedical2 = new Category(
                    categoryType: CategoryType.PRODUCT, name: "Auto-refractors / Keratometers / Tonometers", slug: "keratometers", subText: "", categoryOrder: 1, backgroundUrl: null, createdBy: userBootstrap, parent: categoryB7, site: asia, language: english, segment: Segment.BUSINESS, status: Status.ACTIVE).save(failOnError: true)
        }
        CategoryMedia cmbMedical2 = CategoryMedia.findByCategoryAndMedia(bMedical2, media_keratometer)
        if (!cmbMedical2) {
            cmbMedical2 = new CategoryMedia(category: bMedical2, media: media_keratometer, mediaUrl: media_keratometer?.url, mediaSource: MediaSource.INTERNAL).save(failOnError: true)
        }

        /*Other*/
        Category bOther1 = Category.findBySlug("business-others-supplies")
        if(!bOther1){
            bOther1 = new Category(
                    categoryType: CategoryType.PRODUCT, name: "Supplies", slug: "business-others-supplies", subText: "", categoryOrder: 0, backgroundUrl: null, createdBy: userBootstrap, parent: categoryB8, site: asia, language: english, segment: Segment.BUSINESS, status: Status.ACTIVE).save(failOnError: true)
        }
        CategoryMedia cmbOther1 = CategoryMedia.findByCategoryAndMedia(bOther1, media_printer_supplies)
        if (!cmbOther1) {
            cmbOther1 = new CategoryMedia(category: bOther1, media: media_printer_supplies, mediaUrl: media_printer_supplies?.url, mediaSource: MediaSource.INTERNAL).save(failOnError: true)
        }

        Category bOther2 = Category.findBySlug("business-others-scanners")
        if(!bOther2){
            bOther2 = new Category(
                    categoryType: CategoryType.PRODUCT, name: "Scanners", slug: "business-others-scanners", subText: "", categoryOrder: 1, backgroundUrl: null, createdBy: userBootstrap, parent: categoryB8, site: asia, language: english, segment: Segment.BUSINESS, status: Status.ACTIVE).save(failOnError: true)
        }
        CategoryMedia cmbOther2 = CategoryMedia.findByCategoryAndMedia(bOther2, media_flatbed)
        if (!cmbOther2) {
            cmbOther2 = new CategoryMedia(category: bOther2, media: media_flatbed, mediaUrl: media_flatbed?.url, mediaSource: MediaSource.INTERNAL).save(failOnError: true)
        }

        Category bOther3 = Category.findBySlug("business-others-fax-machines")
        if(!bOther3){
            bOther3 = new Category(
                    categoryType: CategoryType.PRODUCT, name: "Fax Machines", slug: "business-others-fax-machines", subText: "", categoryOrder: 2, backgroundUrl: null, createdBy: userBootstrap, parent: categoryB8, site: asia, language: english, segment: Segment.BUSINESS, status: Status.ACTIVE).save(failOnError: true)
        }
        CategoryMedia cmbOther3 = CategoryMedia.findByCategoryAndMedia(bOther3, media_fax_machines)
        if (!cmbOther3) {
            cmbOther3 = new CategoryMedia(category: bOther3, media: media_fax_machines, mediaUrl: media_fax_machines?.url, mediaSource: MediaSource.INTERNAL).save(failOnError: true)
        }

        Category bOther4 = Category.findBySlug("business-others-calculator")
        if(!bOther4){
            bOther4 = new Category(
                    categoryType: CategoryType.PRODUCT, name: "Calculator", slug: "business-others-calculator", subText: "", categoryOrder: 3, backgroundUrl: null, createdBy: userBootstrap, parent: categoryB8, site: asia, language: english, segment: Segment.BUSINESS, status: Status.ACTIVE).save(failOnError: true)
        }
        CategoryMedia cmbOther4 = CategoryMedia.findByCategoryAndMedia(bOther4, media_calculator)
        if (!cmbOther4) {
            cmbOther4 = new CategoryMedia(category: bOther4, media: media_calculator, mediaUrl: media_calculator?.url, mediaSource: MediaSource.INTERNAL).save(failOnError: true)
        }

        // business solution category
        Category bSolutionFeatures = Category.findBySlug("business-solutions-by-features")
        if(!bSolutionFeatures){
            bSolutionFeatures = new Category(
                    categoryType: CategoryType.SOLUTION, name: "Solution By Features", slug: "business-solutions-by-features", subText: "", categoryOrder: 3, backgroundUrl: null, createdBy: userBootstrap, site: asia, language: english, segment: Segment.BUSINESS, status: Status.ACTIVE).save(failOnError: true)
        }
        CategoryMedia bmSolutionFeatures = CategoryMedia.findByCategoryAndMedia(bSolutionFeatures, media_documents_solution)
        if (!bmSolutionFeatures) {
            bmSolutionFeatures = new CategoryMedia(category: bSolutionFeatures, media: media_documents_solution, mediaUrl: media_documents_solution?.url, mediaSource: MediaSource.INTERNAL).save(failOnError: true)
        }
    }
}
