package canon

class ProductCategoryInit {
    def static init() {
        User userBootstrap = User.findByUsername("bootstrap")
        //Language en = Language.findById("en")
        //Site sg = Site.findByCode("singapore")
        //Site asia = Site.findByCode("asia")

        getCategoryMap().each {k,v ->
            Category category = Category.findBySlug(k)
            v.each {String s ->
                Product product = Product.findBySlug(s)
                ProductCategory pageCategory = ProductCategory.findByProductAndCategory(product, category)
                if (!pageCategory) {
                    new ProductCategory(product: product, category: category, createdBy: userBootstrap).save(failOnError: true)
                }
            }
        }
    }

    private static Map<String, List<String>> getCategoryMap() {

        // Map key : slug of category, value : slug of products
        Map<String, List<String>> result = new HashMap<>()

        def interchangableLensCamera = [
                "eos-1300d-kit-ef-s18-55-is-ii",
                "eos-200d-colour",
                "eos-750d",
                "eos-5d-mark-iii"
        ]
        result.put("interchangeable-lens-camera", interchangableLensCamera)

        def digitalCompactCamera = [
                "powershot-g1-x-mark-iii",
                "powershot-d30",
                "powershot-sx430-is"
        ]
        result.put("digital-compact-cameras", digitalCompactCamera)

        def pixma = [
                "pixma-ts9170",
                "pixma-ts8170",
                "pixma-tr8570"
        ]
        result.put("pixma", pixma)

        def laser = [
                "imageclass-mf232w",
                "imageclass-mf631cn",
                "imageclass-mf633cdw"
        ]
        result.put("laser", laser)

        def selphy = [
                "selphy-cp1300",
                "selphy-cp1200"
        ]
        result.put("selphy", selphy)

        def projectors = [
                "xeed-wux450st",
                "rayo-i5",
                "rayo-r4"
        ]
        result.put("projectors", projectors)

        def flatbed = [
                "lide-120",
                "lide-220"
        ]
        result.put("flatbed", flatbed)

        def professionalCamcorders = [
                "xa11",
                "xa15",
                "xa30"
        ]
        result.put("professional-camcoders", professionalCamcorders)

        return result
    }
}
