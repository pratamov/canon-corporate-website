package canon

class ProductMediaInit {
    def static init() {
        User userBootstrap = User.findByUsername("bootstrap")

        def productList = Product.findAll()

        if(productList){
            productList.each {
                def media = Media.findByFileName(it?.slug)
                if(media){
                    ProductMedia pm = ProductMedia.findByProductAndMedia(it, media)
                    if(pm == null){
                        new ProductMedia(product: it, media: media, isDefault: true, createdBy: userBootstrap).save(failOnErrors: true)
                    }
                }
            }
        }

        //Award award1 = Award.findByName("Product Category Photography Media")
        //Award award2 = Award.findByName("Product Category Printing Media")
        //Award award3 = Award.findByName("Product Category Scanning Media")

        prepareData().each {k, v ->
            Product product = Product.findBySlug(k)
            if (product) {
                v.each {String mediaFileName ->
                    Media media = Media.findByFileName(mediaFileName)
                    if (media) {
                        ProductMedia pm = ProductMedia.findByProductAndMedia(product, media)
                        if(!pm) new ProductMedia(product: product, media: media, mediaUrl: media.url).save(failOnErrors: true)
                    }
                }
            }
        }
    }

    private static def prepareData() {
        return [
                "eos-200d-colour": [
                        "eos-200d-colour-l2",
                        "eos-200d-colour-l3",
                        "eos-200d-colour-l4",
                        "eos-200d-colour-l5",
                        "eos-200d-colour-l6"
                ]
        ]
    }
}
