package canon

import canon.enums.ProductLevel
import canon.enums.ProductPromotionType
import canon.enums.Status

import java.text.SimpleDateFormat

class ProductInit {

    def static init() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss")
        //Language en = Language.findById("en")
        //Site sg = Site.findByCode("singapore")
        Site asia = Site.findByCode("asia")
        Flow flow = Flow.findByName("Product Workflow")

        getProductList().each { Product p ->
            p.site = asia
            p.addToProductDetails(new ProductDetail(product: p, site: asia, flow: flow))
            p.save(flush: true, failOnError:true)
        }
        getProductList().each { Product p ->
            getProductPromotionList().each { ProductPromotion pp ->
                if (pp.product.slug == p.slug) {
                    pp.save(failOnError: true)
                }
            }
        }
    }

    private static List<Product> getProductList() {
        Language en = Language.findById("en")
        SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy")
        Date featuredStartDate = sdf.parse("25092016")
        Date featuredEndDate = sdf.parse("25092018")
        Site site = Site.findByIsDefault(Boolean.TRUE)

        List<Product> result = new ArrayList<>()

        // Interchangeable Lens Cameras
        result.add(new Product(language: en, name: "EOS 1300D Kit (EF S18-55 IS II)", slug: "eos-1300d-kit-ef-s18-55-is-ii", subText: "Step up your joy in photography",
                description: "The EOS 1300D packs in all the fun of photography, which is why we recommend it to users looking for their very first EOS DSLR camera. It uses an 18-megapixel APS-C size sensor and the DIGIC 4+ image processor—which even professional photographers recognize as high performance core features. It also has abundant auto shooting features and is compatible with over 70 EF/EF-S lenses, allowing you to take beautiful photos like a pro regardless of subject and genre. Wi-Fi and NFC compatibility allows easy file transfer to other devices. All the more convenient for sharing those wonderful photos on social media!",
                productLevel: ProductLevel.SEMI_PROFESSIONAL, status: Status.PUBLISHED, publishedDate: sdf.parse("25082016"), isFeatured: Boolean.TRUE, featuredStartDate: featuredStartDate, featuredEndDate: featuredEndDate, price: 669.00))
        result.add(new Product(language: en, name: "EOS 200D Kit (EF-S18-55 IS STM)", slug: "eos-200d-colour", subText: "Embrace creative expression with the compact and sleek DSLR",
                description: "The light and compact EOS 200D fits snugly into your everyday bag. This stylish DSLR comes in 2 colour options – white and silver. Equipped with cutting technologies packed into its compact body, capture beautiful and impressionable images and share them seamlessly on your social media feeds via the in-built Wi-Fi capability.",
                productLevel: ProductLevel.PROFESSIONAL, status: Status.PUBLISHED, publishedDate: sdf.parse("25052016"), isFeatured: Boolean.TRUE, featuredStartDate: featuredStartDate, featuredEndDate: featuredEndDate, price: 949.00))
        result.add(new Product(language: en, name: "EOS 750D (Body)", slug: "eos-750d", subText: "",
                description: "The EOS 750D features a 24.2-megapixel CMOS sensor, and is specifically designed for casual photographers who prize excellent image quality, a simple and user-friendly interface. It offers creative freedom through its new features – 19-point all cross-type AF and creative filters and the new Hybrid CMOS AF III.",
                productLevel: ProductLevel.PROFESSIONAL, status: Status.PUBLISHED, publishedDate: sdf.parse("25072016"), isFeatured: Boolean.TRUE, featuredStartDate: featuredStartDate, featuredEndDate: featuredEndDate, price: 949.00))
        result.add(new Product(language: en, name: "EOS 5D Mark III (Body)", slug: "eos-5d-mark-iii", subText: "",
                description: "EOS 5D Mark III is generated by a full-frame 22.3-megapixel CMOS sensor that can shoot up to 6 frames per second (fps). Powered by Canon's state-of-the-art DIGIC 5+ image processor, the EOS 5D MARK III is capable of incredible ISO speeds of up to 25600 (expandable to 102400). A newly expanded 61-point Auto-Focus (AF) system makes it easier to track subjects with higher levels of precision and speeds.",
                productLevel: ProductLevel.PROFESSIONAL, status: Status.PUBLISHED, publishedDate: sdf.parse("25092016"), price: 4499.00, site: site))

        // Compact Cameras
        result.add(new Product(language: en, name: "PowerShot G1 X Mark III", slug: "powershot-g1-x-mark-iii", subText: "Compact companion for adventure seekers",
                description: "Pushing frontiers in technology to deliver DSLR quality from a compact camera, the PowerShot G1 X Mark III features an APS-C imaging sensor used in many DSLR cameras, and Dual Pixel CMOS AF to deliver high image quality and unprecedented high-speed AF with the convenience of a compact camera. In addition, the Touch & Drag AF feature on the PowerShot G1 X Mark III allows touch screen AF operation for even more precise AF even during viewfinder shooting.",
                productLevel: ProductLevel.BEGINNER, status: Status.PUBLISHED, publishedDate: new Date(), isFeatured: Boolean.TRUE, featuredStartDate: featuredStartDate, featuredEndDate: featuredEndDate))
        result.add(new Product(language: en, name: "PowerShot D30", slug: "powershot-d30", subText: "Adventure redefined",
                description: "Waterproof to 25m, shockproof to 2m and temperature-resistant to -10℃, this camera will rough it out with you, no additional casing needed—be it in the sea, on a snowy mountain, or even just splashing around with the kids.",
                productLevel: ProductLevel.BEGINNER, status: Status.PUBLISHED, publishedDate: new Date(), isFeatured: Boolean.TRUE, featuredStartDate: featuredStartDate, featuredEndDate: featuredEndDate))
        result.add(new Product(language: en, name: "PowerShot SX430 IS", slug: "powershot-sx430-is", subText: "Lightweight camera for your travel adventure",
                description: "The PowerShot SX430 IS is a 20.0-megapixel travel friendly camera with 45x optical zoom that gives you the ability to capture impressive close ups. Its ergonomic grip also allows for better handling, which is particularly useful for recording videos and ensuring stable images. With the built-in Wi-Fi/ NFC feature, sharing images on social media has never been simpler.",
                productLevel: ProductLevel.BEGINNER, status: Status.PUBLISHED, publishedDate: new Date(), isFeatured: Boolean.TRUE, featuredStartDate: featuredStartDate, featuredEndDate: featuredEndDate, site: site))

        // Pixma
        result.add(new Product(language: en, name: "PIXMA TS9170", slug: "pixma-ts9170", subText: "Wireless Photo All-In-One with Large 5.0\" LCD Display and Creative Filters", description: "This premium wireless Photo All-In-One features a 6-ink system with new Photo Blue ink for high quality photo printing.", status: Status.PUBLISHED, publishedDate: new Date(), isFeatured: Boolean.TRUE, featuredStartDate: featuredStartDate, featuredEndDate: featuredEndDate))
        result.add(new Product(language: en, name: "PIXMA TS8170", slug: "pixma-ts8170", subText: "Wireless Photo All-In-One with Large 4.3\" Touch-Screen and Auto Duplex Printing", description: "This advanced wireless Photo All-In-One features a 6-ink system with new Photo Blue ink for high quality photo printing.", status: Status.PUBLISHED, publishedDate: new Date(), isFeatured: Boolean.TRUE, featuredStartDate: featuredStartDate, featuredEndDate: featuredEndDate))
        result.add(new Product(language: en, name: "PIXMA TR8570", slug: "pixma-tr8570", subText: "High-productivity, Wireless Office All-In-One with 4.3\" Touch-Screen, Auto Duplex Printing and Fax", description: "High-productivity, Wireless All-In-One Printer with Auto Duplex Printing, Auto Document Feeder and Large 4.3\" Touch LCD Display.", status: Status.PUBLISHED, publishedDate: new Date(), isFeatured: Boolean.TRUE, featuredStartDate: featuredStartDate, featuredEndDate: featuredEndDate))

        // Laser Printers
        result.add(new Product(language: en, name: "imageCLASS MF232w", slug: "imageclass-mf232w", subText: "Compact All-in-One (Print, Copy, Scan) with wireless connectivity", description: "Enhance your productivity with this compact and user-friendly monochrone laser print which prints up to 23ppm (A4), with the first printout in less than 6 seconds. With both wired and wireless network capabilities, it facilitates the easy sharing of files and other resources.", status: Status.PUBLISHED, publishedDate: new Date(), isFeatured: Boolean.TRUE, featuredStartDate: featuredStartDate, featuredEndDate: featuredEndDate))
        result.add(new Product(language: en, name: "imageCLASS MF631Cn", slug: "imageclass-mf631cn", subText: "Compact 3-in-1 Colour Multifunction Printer for the modern business", description: "The imageCLASS MF631Cn delivers high-quality prints in a compact design.", status: Status.PUBLISHED, publishedDate: new Date(), isFeatured: Boolean.TRUE, featuredStartDate: featuredStartDate, featuredEndDate: featuredEndDate))
        result.add(new Product(language: en, name: "imageCLASS MF633Cdw", slug: "imageclass-mf633cdw", subText: "Versatile 3-in-1 Colour Multifunction Printer for the modern business", description: "The imageCLASS MF633Cdw is designed to meet the office document needs.", status: Status.PUBLISHED, publishedDate: new Date(), isFeatured: Boolean.TRUE, featuredStartDate: featuredStartDate, featuredEndDate: featuredEndDate))

        // Compact Photo Printers
        result.add(new Product(language: en, name: "SELPHY CP1300", slug: "selphy-cp1300", subText: "Mobile Wi-Fi printer with variety of print functions",
                description: "A compact, portable Wi-Fi printer that prints quality photos direct from your camera or smartphone anywhere, anytime, any occasion. With the use of an optional battery pack that print up to 54 photos, making this printer more portable than ever before. Packed with functions like the new Wi-Fi Shuffle Print mode, SELPHY CP1300 can combine images (from up to 8 to smart devices) to form a unique collage on a single photo. The new 2x6 inch layout feature (2 sets of 4 images in a 6 inch area) also adds a layer of versatility as users can create their very own photo booth style printouts.\n" + "\n" + "Finally, with the special film coating protecting the photos from dust, water and colour fading, memories can be preserved to last through the seasons.",
                productLevel: ProductLevel.SEMI_PROFESSIONAL, status: Status.PUBLISHED, publishedDate: new Date(), isFeatured: Boolean.TRUE, featuredStartDate: featuredStartDate, featuredEndDate: featuredEndDate))
        result.add(new Product(language: en, name: "SELPHY CP1200", slug: "selphy-cp1200", subText: "Fun and versatile printing on the move",
                description: "The Selphy CP1200 is a great companion for users at events such as family excursions, parties and other gatherings. Designed with fun in mind, the layout features easy to understand buttons to take the user along the three steps of printing: choose an image, choose the number of prints, and press the Print button.",
                productLevel: ProductLevel.SEMI_PROFESSIONAL, status: Status.PUBLISHED, publishedDate: new Date(), isFeatured: Boolean.TRUE, featuredStartDate: featuredStartDate, featuredEndDate: featuredEndDate, site: site))

        // Projectors
        result.add(new Product(language: en, name: "XEED WUX450ST", slug: "xeed-wux450st", subText: "", description: "Tapping on Canon’s LCOS technology with AISYS enhancement, the XEED WUX450ST projects bright and highly detailed images in true colour. With its 1.35x optical zoom, short throw ratio of 0.56:1, and 75% vertical lens shift, the XEED WUX450ST provides outstanding installation flexibility in various settings.", status: Status.PUBLISHED, publishedDate: new Date(), isFeatured: Boolean.TRUE, featuredStartDate: featuredStartDate, featuredEndDate: featuredEndDate))
        result.add(new Product(language: en, name: "RAYO i5", slug: "rayo-i5", subText: "Amazing WIRELESS convenience", description: "Designed with wireless functionality, the RAYO i5 mini projectors offer user the ability to play and enlarge content to a 158” image onto virtually any surface, from smart devices.", status: Status.PUBLISHED, publishedDate: new Date(), isFeatured: Boolean.TRUE, featuredStartDate: featuredStartDate, featuredEndDate: featuredEndDate))
        result.add(new Product(language: en, name: "RAYO R4 Mini projector", slug: "rayo-r4", subText: "Entertainment on the go", description: "The new Mini RAYO R4 isn’t just a projector; it’s the perfect companion for both work and leisure. Light and compact, it fits perfectly into your pocket, giving you the freedom to take it anywhere.", status: Status.PUBLISHED, publishedDate: new Date(), isFeatured: Boolean.TRUE, featuredStartDate: featuredStartDate, featuredEndDate: featuredEndDate))

        // Scanners
        result.add(new Product(language: en, name: "LiDE 120", slug: "lide-120", subText: "Compact flatbed scanner with SEND to cloud* function", description: "", status: Status.PUBLISHED, publishedDate: new Date(), isFeatured: Boolean.TRUE, featuredStartDate: featuredStartDate, featuredEndDate: featuredEndDate))
        result.add(new Product(language: en, name: "LiDE 220", slug: "lide-220", subText: "Compact flatbed scanner with SEND to cloud* function and upright scanning", description: "", status: Status.PUBLISHED, publishedDate: new Date(), isFeatured: Boolean.TRUE, featuredStartDate: featuredStartDate, featuredEndDate: featuredEndDate))

        // Professional Camcorders
        result.add(new Product(language: en, name: "XA11", slug: "xa11", subText: "Compact Full HD ENG camera with 20x zoom and 5-axis image stabilization",
                description: "A compact Full HD ENG camera that boasts a 20x zoom lens and 5-axis image stabilization, two SD card slots and 50P/60P recording in both AVCHD and MP4 formats, the XA11 is equipped with many features for added efficiency, such as support for both relay and simultaneous recording as well as the ability to simultaneously record video snippets in 3 Mbps mode for usage in fast-breaking news. The camera offers the imaging prowess of the Canon Full HD system, combining the excellent sensitivity of the CMOS sensor with the DIGIC DV 4 image processor for high image quality even in scenes with low illumination. With the ‘Highlight Priority’ and ‘Wide DR’ custom picture profiles in place, the recorded footage has more flexibility for minor image adjustments in post-production, ideal for scenes where achieving the ideal exposure is challenging. It is possible to record audio via two separate audio channels, each with the volume adjusted independently. Indeed, this is a highly versatile camera that is great value for money, ideal for a wide range of purposes such as shooting corporate promotional videos, weddings and other event coverage.",
                productLevel: ProductLevel.PROFESSIONAL, status: Status.PUBLISHED, publishedDate: new Date(), isFeatured: Boolean.TRUE, featuredStartDate: featuredStartDate, featuredEndDate: featuredEndDate))
        result.add(new Product(language: en, name: "XA15", slug: "xa15", subText: "Full HD ENG camera with HD-SDI terminals, 20x zoom and 5-axis image stabilization",
                description: "A compact Full HD ENG camera that boasts a 20x zoom lens and 5-axis image stabilization, two SD card slots and 50P/60P recording in both AVCHD and MP4 formats, the XA15 is equipped with various features for added efficiency, such as support for both relay and simultaneous recording and the ability to simultaneously record video snippets in 3 Mbps mode for usage in fast-breaking news. It is also equipped with an HD-SDI terminal, the standard interface in broadcasting, so that not only can you simultaneously pipe footage directly to the broadcast van, but also output to an external recorder to make uncompressed video recordings. The camera offers the imaging prowess of the Canon Full HD system, combining the excellent sensitivity of the CMOS sensor with the DIGIC DV 4 image processor for high image quality even in scenes with low illumination. With the ‘Highlight Priority’ and ‘Wide DR’ custom picture profiles in place, the recorded footage has more flexibility for minor image adjustments in post-production, ideal for scenes where achieving the ideal exposure is challenging. It is possible to record audio via two separate audio channels, each with the volume adjusted independently. This is a highly versatile camera that is great value for money, ideal for a wide range of purposes beyond news and reporting, such as shooting corporate promotional videos, weddings and other event coverage.",
                productLevel: ProductLevel.PROFESSIONAL, status: Status.PUBLISHED, publishedDate: new Date(), isFeatured: Boolean.TRUE, featuredStartDate: featuredStartDate, featuredEndDate: featuredEndDate))
        result.add(new Product(language: en, name: "XA30", slug: "xa30", subText: "",
                description: "The XA30 is a compact Full HD camcorder ideal for professional use. With its 20x optical zoom and a wide 26.8m* maximum angle-of-view, it is a powerful companion for filming news, documentary, and other on-location footage, regardless of the shooting conditions. It uses the newly developed HD CMOS PRO sensor, which realizes an improved S/N ratio for enhanced shooting in low light conditions. In addition, two new recording modes, Wide DR and Highlight Priority, enable an even wider range of imaging possibilities.",
                productLevel: ProductLevel.PROFESSIONAL, status: Status.PUBLISHED, publishedDate: new Date(), isFeatured: Boolean.TRUE, featuredStartDate: featuredStartDate, featuredEndDate: featuredEndDate, site: site))

        //Calculator
        result.add(new Product(language: en, name: "LS-12H", slug: "ls-12h", subText: "Calculate with Canon anytime, anywhere", description: "This stylish handheld 12-digit calculator with a large LCD display fits neatly in the palm of your hand. The LS-12H combines style, simplicity and efficiency and a strap hold to enhance the portability.", status: Status.PUBLISHED, publishedDate: new Date(), isFeatured: Boolean.TRUE, featuredStartDate: featuredStartDate, featuredEndDate: featuredEndDate))
        result.add(new Product(language: en, name: "AS-8", slug: "as-8", subText: "Calculate with Canon anytime, anywhere", description: "This compact 8-digit handheld calculator features the smart arc design and 360-degree upward folding durable cover, making the AS-8 ideal for use in an office or on the go.", status: Status.PUBLISHED, publishedDate: new Date(), isFeatured: Boolean.TRUE, featuredStartDate: featuredStartDate, featuredEndDate: featuredEndDate))
        result.add(new Product(language: en, name: "LC-10", slug: "lc-10", subText: "Light & Chic calculator that spice up your every day life", description: "The 10-digit credit card sized handheld calculator featured with trendy design, offers the optimum portability", status: Status.PUBLISHED, publishedDate: new Date(), isFeatured: Boolean.TRUE, featuredStartDate: featuredStartDate, featuredEndDate: featuredEndDate))

        //Cinema EOS System
        result.add(new Product(language: en, name: "EOS C200", slug: "eos-c200", subText: "Encompasses both mobility and the expressive power of 4K", description: "The 8.85 effective megapixels of the EOS C200's 4K Super 35mm, large-format CMOS sensor weaves ultra high-definition and richly textured imagery, aided by the newly-developed Dual DIGIC DV 6 image processing platform. The camera supports a new RAW format, Cinema RAW Light, which can record DCI 4K 60P/50P RAW images to a CFast 2.0 (VPG130) card without the need for an external recorder. 4K Cinema RAW Light files contain abundant information, which not only enables format conversion with less image degradation but is also ideal for HDR recording. Meanwhile, 4K UHD 60P/50P movies can be recorded in the versatile MP4 format to SD cards. Dual Pixel CMOS AF, which is supported via touchscreen operation, enables easy and convenient pinpoint focusing. A compatible sub-camera for the EOS C700 and C300 Mark II with its support for Canon Log and Canon Log-3, the EOS C200's lightweight, compact body also makes it suitable for use on the field, whether for on-scene reporting or for documentary shoots. *EOS C200: All-in-one model bundled with grip, handle, built-in viewfinder and 4.0 inch touchscreen LCD monitor for usage right out of the box", status: Status.PUBLISHED, publishedDate: new Date(), isFeatured: Boolean.TRUE, featuredStartDate: featuredStartDate, featuredEndDate: featuredEndDate))
        result.add(new Product(language: en, name: "EOS C100 DAF", slug: "eos-c100-daf", subText: "Versatile, compact storytelling", description: "An affordable video production camera offering a unique combination of professional HD imaging, compact design and lens versatility, the user-friendly C100 is ideally configured for single person use.", status: Status.PUBLISHED, publishedDate: new Date(), isFeatured: Boolean.TRUE, featuredStartDate: featuredStartDate, featuredEndDate: featuredEndDate))
        result.add(new Product(language: en, name: "EOS C100 Mark II", slug: "eos-c100-mark-ii", subText: null, description: "The new and improved EOS C100 Mark II offers fast the ability for fast turnaround time to match existing workflows in a compact configuration. Loaded with powerful features and enhanced ergonomics, the EOS C100 Mark II is ideal for solo operations such as run-and-gun style shoot coverage as well as operation on cranes, sliders and unmanned drones. Designed to be used right-out-of-the-box, many would find the learning curve of the EOS C100 Mark II, a gradual and pleasant one.", status: Status.PUBLISHED, publishedDate: new Date(), isFeatured: Boolean.TRUE, featuredStartDate: featuredStartDate, featuredEndDate: featuredEndDate))

        //Bags and Cases
        result.add(new Product(language: en, name: "RL AV-BP01", slug: "rl-av-bp01", subText: "Active Backpack", description: "Built for outdoor enthusiasts, created for your Canon DSLR cameras and lenses, this camera gear backpack doubles as an outdoor bag that lets you carry a full set of photographic equipment.", status: Status.PUBLISHED, publishedDate: new Date(), isFeatured: Boolean.TRUE, featuredStartDate: featuredStartDate, featuredEndDate: featuredEndDate))
        result.add(new Product(language: en, name: "RL PB-01", slug: "rl-pb-01", subText: "Professional Backpack", description: "Lightweight and full-sized, this fine, flagship Canon backpack provides more than enough space to carry a complete set of photographic Canon gear.", status: Status.PUBLISHED, publishedDate: new Date(), isFeatured: Boolean.TRUE, featuredStartDate: featuredStartDate, featuredEndDate: featuredEndDate))
        result.add(new Product(language: en, name: "RL PS-01", slug: "rl-ps-01", subText: "Professional Shoulder Bag", description: "Another flagship product, this Canon shoulder bag is made from 80% Chlorosulfonated Polyethylene (CSM) and 20% Fiber. Likewise, this can also hold a complete set of photographic Canon gear.", status: Status.PUBLISHED, publishedDate: new Date(), isFeatured: Boolean.TRUE, featuredStartDate: featuredStartDate, featuredEndDate: featuredEndDate))

        //Binoculars
        result.add(new Product(language: en, name: "10x30 IS II", slug: "10x30-is-ii", subText: "Compact and improved IS performance", description: "While maintaining the portability, 10x30 IS II is equipped with improved IS performance with more efficient power consumption.User can now enjoy up to 9 hours of stable and clear vision thanks to the improved battery life.", status: Status.PUBLISHED, publishedDate: new Date(), isFeatured: Boolean.TRUE, featuredStartDate: featuredStartDate, featuredEndDate: featuredEndDate))
        result.add(new Product(language: en, name: "10x30 IS", slug: "10x30-is", subText: "True vision with portability and an ultra-stable image", description: "Whether you’re starting out or a fulltime professional, see farther with the 10x30 IS. With excellent portability, easy access controls and image stabilisation, your vision is always clear and steady.", status: Status.PUBLISHED, publishedDate: new Date(), isFeatured: Boolean.TRUE, featuredStartDate: featuredStartDate, featuredEndDate: featuredEndDate))
        result.add(new Product(language: en, name: "10x32 IS", slug: "10x32-is", subText: "Delivering bright vision and superb image stabilization", description: "Suitable for a varied variety of scenarios which range from traveling to stargazing, the 10x32 IS binoculars is equipped with doublet field-flattener lenses and Super Spectra Coating, enabling users to achieve excellent optical performance with edge to edge sharpness and high contrast viewing.", status: Status.PUBLISHED, publishedDate: new Date(), isFeatured: Boolean.TRUE, featuredStartDate: featuredStartDate, featuredEndDate: featuredEndDate))

        //Fax machines
        result.add(new Product(language: en, name: "FAX-L3000", slug: "fax-l3000", subText: "Built with superb faxing capabilities for high volume fax users", description: "Built for high-volume faxing, the FAX-L3000 has a large 600-page capacity with an optional 500-sheet cassette. Yet, it takes as little as 1.62 seconds to scan an A4 document so you can fax more, fast.", status: Status.PUBLISHED, publishedDate: new Date(), isFeatured: Boolean.TRUE, featuredStartDate: featuredStartDate, featuredEndDate: featuredEndDate))
        result.add(new Product(language: en, name: "FAX-L170", slug: "fax-l170", subText: "The versatile office communications device with print functionality", description: "The FAX-L170 features a small footprint, an accompanying handset and comes with print, copy and fax functionality.", status: Status.PUBLISHED, publishedDate: new Date(), isFeatured: Boolean.TRUE, featuredStartDate: featuredStartDate, featuredEndDate: featuredEndDate))


        return result;
    }

    private static List<ProductPromotion> getProductPromotionList() {
        List<ProductPromotion> result = new ArrayList<>();

        result.add(new ProductPromotion(
                product: Product.findBySlug("eos-1300d-kit-ef-s18-55-is-ii"),
                currencyCode: "SGD",
                promotionType: ProductPromotionType.PERCENTAGE,
                percentage: new BigDecimal(25)
        ));

        return result;
    }
}
