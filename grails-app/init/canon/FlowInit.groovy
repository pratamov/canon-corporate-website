package canon

import canon.enums.AssignmentType
import canon.enums.FlowType
import canon.enums.Status

/**
 * Created by marjan on 13/12/17.
 */
class FlowInit {
    def static init() {
        User userBootstrap = User.findByUsername("bootstrap")

        Flow flow = Flow.findOrCreateByName("Product Workflow")
        flow.flowType = FlowType.PRODUCT
        flow.status = Status.ACTIVE
        flow.createdBy = userBootstrap
        flow.addToWorkFlows(new WorkFlow(name: "Requestor", flowOrder: 0, isStart: Boolean.TRUE, isEnd: Boolean.FALSE, assignmentType: AssignmentType.USER))
        flow.addToWorkFlows(new WorkFlow(name: "Preparer", flowOrder: 1, isStart: Boolean.FALSE, isEnd: Boolean.FALSE, assignmentType: AssignmentType.USER))
        flow.addToWorkFlows(new WorkFlow(name: "Confirmation", flowOrder: 3, isStart: Boolean.FALSE, isEnd: Boolean.FALSE, assignmentType: AssignmentType.USER))
        flow.addToWorkFlows(new WorkFlow(name: "Publishing", flowOrder: 4, isStart: Boolean.FALSE, isEnd: Boolean.TRUE, assignmentType: AssignmentType.USER))
        flow.save(failOnError: true)

        Flow pageFlow = Flow.findOrCreateByName("Page Workflow")
        pageFlow.flowType = FlowType.ARTICLE
        pageFlow.status = Status.ACTIVE
        pageFlow.createdBy = userBootstrap
        pageFlow.addToWorkFlows(new WorkFlow(name: "Requestor", flowOrder: 0, isStart: Boolean.TRUE, isEnd: Boolean.FALSE, assignmentType: AssignmentType.USER))
        pageFlow.addToWorkFlows(new WorkFlow(name: "Preparer", flowOrder: 1, isStart: Boolean.FALSE, isEnd: Boolean.FALSE, assignmentType: AssignmentType.USER))
        pageFlow.addToWorkFlows(new WorkFlow(name: "Confirmation", flowOrder: 3, isStart: Boolean.FALSE, isEnd: Boolean.FALSE, assignmentType: AssignmentType.USER))
        pageFlow.addToWorkFlows(new WorkFlow(name: "Publishing", flowOrder: 4, isStart: Boolean.FALSE, isEnd: Boolean.TRUE, assignmentType: AssignmentType.USER))
        pageFlow.save(failOnError: true)
    }
}
