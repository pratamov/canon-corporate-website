package canon

class ProductPriceInit {
    def static init() {

        Site asia = Site.findByCode("asia")
        User userBootstrap = User.findByUsername("bootstrap")

        def prices = [
                [priceName:'powershot-sx430-is',price:15595,currencyCode:'INR',quantity:1],
                [priceName:'eos-5d-mark-iii',price:207995,currencyCode:'INR',quantity:1],
                [priceName:'eos-1300d-kit-ef-s18-55-is-ii',price:31995,currencyCode:'INR',quantity:1],
                [priceName:'eos-c100-daf',price:4899,currencyCode:'SGD',quantity:1],
                [priceName:'powershot-sx430-is',price:339,currencyCode:'SGD',quantity:1],
                [priceName:'10x30-is-ii',price:2039,currencyCode:'MMK',quantity:1],
                [priceName:'imageclass-mf232w',price:229,currencyCode:'SGD',quantity:1],
                [priceName:'rayo-r4',price:399,currencyCode:'SGD',quantity:1],
                [priceName:'imageclass-mf633cdw',price:1720,currencyCode:'MMK',quantity:1],
                [priceName:'as-8',price:11.9,currencyCode:'MMK',quantity:1],
                [priceName:'eos-c100-daf',price:61375000,currencyCode:'IDR',quantity:1],
                [priceName:'lc-10',price:19.9,currencyCode:'MMK',quantity:1],
                [priceName:'eos-750d',price:49995,currencyCode:'INR',quantity:1],
                [priceName:'eos-1300d-kit-ef-s18-55-is-ii',price:18790,currencyCode:'THB',quantity:1],
                [priceName:'selphy-cp1200',price:199,currencyCode:'SGD',quantity:1],
                [priceName:'eos-1300d-kit-ef-s18-55-is-ii',price:5675000,currencyCode:'IDR',quantity:1],
                [priceName:'selphy-cp1200',price:8995,currencyCode:'INR',quantity:1],
                [priceName:'lide-220',price:2640000,currencyCode:'VND',quantity:1],
                [priceName:'lide-120',price:1590000,currencyCode:'VND',quantity:1],
                [priceName:'eos-5d-mark-iii',price:37800000,currencyCode:'IDR',quantity:1],
                [priceName:'xa30',price:53440000,currencyCode:'VND',quantity:1],
                [priceName:'ls-12h',price:30.9,currencyCode:'MMK',quantity:1],
                [priceName:'lide-120',price:395,currencyCode:'MMK',quantity:1],
                [priceName:'powershot-sx430-is',price:6400000,currencyCode:'VND',quantity:1],
                [priceName:'imageclass-mf631cn',price:499,currencyCode:'SGD',quantity:1],
                [priceName:'lide-120',price:860200,currencyCode:'IDR',quantity:1],
                [priceName:'eos-5d-mark-iii',price:4499,currencyCode:'SGD',quantity:1],
                [priceName:'eos-5d-mark-iii',price:62000000,currencyCode:'VND',quantity:1],
                [priceName:'xeed-wux450st',price:28800,currencyCode:'MMK',quantity:1],
                [priceName:'xa30',price:2399,currencyCode:'SGD',quantity:1],
                [priceName:'10x30-is-ii',price:749,currencyCode:'SGD',quantity:1],
                [priceName:'imageclass-mf631cn',price:1455,currencyCode:'MMK',quantity:1],
                [priceName:'imageclass-mf633cdw',price:549,currencyCode:'SGD',quantity:1],
                [priceName:'powershot-d30',price:3775000,currencyCode:'IDR',quantity:1],
                [priceName:'lide-120',price:4355,currencyCode:'INR',quantity:1],
                [priceName:'eos-750d',price:949,currencyCode:'SGD',quantity:1],
                [priceName:'lide-220',price:169,currencyCode:'SGD',quantity:1],
                [priceName:'powershot-d30',price:7500000,currencyCode:'VND',quantity:1],
                [priceName:'xa30',price:7499,currencyCode:'MMK',quantity:1],
                [priceName:'eos-1300d-kit-ef-s18-55-is-ii',price:10900000,currencyCode:'VND',quantity:1],
                [priceName:'powershot-d30',price:399,currencyCode:'SGD',quantity:1],
                [priceName:'eos-c100-daf',price:15300,currencyCode:'MMK',quantity:1],
                [priceName:'imageclass-mf232w',price:14899,currencyCode:'INR',quantity:1],
                [priceName:'lide-120',price:129,currencyCode:'SGD',quantity:1],
                [priceName:'powershot-d30',price:10490,currencyCode:'THB',quantity:1],
                [priceName:'selphy-cp1200',price:499,currencyCode:'MMK',quantity:1],
                [priceName:'eos-750d',price:2899,currencyCode:'MMK',quantity:1],
                [priceName:'imageclass-mf232w',price:830,currencyCode:'MMK',quantity:1],
                [priceName:'lide-220',price:1347500,currencyCode:'IDR',quantity:1],
                [priceName:'eos-5d-mark-iii',price:12099,currencyCode:'MMK',quantity:1],
                [priceName:'eos-750d',price:9075000,currencyCode:'IDR',quantity:1],
                [priceName:'eos-1300d-kit-ef-s18-55-is-ii',price:2099,currencyCode:'MMK',quantity:1],
                [priceName:'lide-220',price:5355,currencyCode:'INR',quantity:1],
                [priceName:'powershot-sx430-is',price:1059,currencyCode:'MMK',quantity:1],
                [priceName:'powershot-sx430-is',price:8990,currencyCode:'THB',quantity:1],
                [priceName:'imageclass-mf232w',price:7200000,currencyCode:'VND',quantity:1],
                [priceName:'eos-c100-daf',price:85650000,currencyCode:'VND',quantity:1],
                [priceName:'powershot-d30',price:1199,currencyCode:'MMK',quantity:1],
                [priceName:'eos-1300d-kit-ef-s18-55-is-ii',price:669,currencyCode:'SGD',quantity:1],
                [priceName:'selphy-cp1200',price:4510,currencyCode:'THB',quantity:1],
                [priceName:'lide-220',price:624,currencyCode:'MMK',quantity:1],
                [priceName:'eos-5d-mark-iii',price:109090,currencyCode:'THB',quantity:1],
                [priceName:'selphy-cp1200',price:1875000,currencyCode:'IDR',quantity:1],
                [priceName:'rayo-i5',price:699,currencyCode:'SGD']
        ]

        prices.each{
            ProductPrice pp = ProductPrice.findByNameAndCurrencyCode(it?.priceName,it?.currencyCode)
            if(pp==null) {
                pp = new ProductPrice(it)
                Product p = Product.findBySlug(it?.priceName)
                if (p != null) {
                    pp.site = asia
                    pp.product = p
                    pp.name = it?.priceName
                    pp.price = new BigDecimal(it?.price)
                    pp.currencyCode = it?.currencyCode
                    pp.createdBy = userBootstrap
                    pp.modifiedBy = userBootstrap
                    pp.save(failOnError: true)
                }
            }
        }
    }

}
