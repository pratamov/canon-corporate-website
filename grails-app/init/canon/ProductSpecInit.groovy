package canon

import canon.enums.Status

import java.text.SimpleDateFormat

/**
 * Created by User on 05/12/2017.
 */
class ProductSpecInit {
    static init() {

        // Inkjet Printers
        Product p7 = Product.findBySlug("pixma-ts9170")

        Product p8 = Product.findBySlug("pixma-ts8170")

        Product p9 = Product.findBySlug("pixma-tr8570")

        Topic inkjetPrinter = Topic.findByName('Inkjet Printers specifications')

        def p7Spec = [:]
        p7Spec.put('Copy',
                [['Compatible Media':'Size A4 / A5 / B5 / LTR / 4 x 6" / 5 x 7" / Square (5 x 5") / Card Size (91 x 55mm)\n' +
                        'Type Plain Paper Photo Paper Pro Platinum (PT-101) Photo Paper Plus Glossy II (PP-201) Photo Paper Pro Luster (LU-101) Photo Paper Plus Semi-Gloss (SG-201) Glossy Photo Paper "Everyday Use" (GP-508) Photo Paper Plus Glossy II (PP-208) Matte Photo Paper (MP-101) Printable Disc'],
                 ['Copy Speed (Colour, sFCOT)':'Approx. 19secs.'],
                 ['Copy Speed *9 Based on ISO / IEC 29183':'Document: Colour sFCOT / Simplex: Approx. 19sec.\n' +
                         'sESAT / Simplex: Approx. 6.5ipm'],
                 ['Density Adjustment':'9 positions, Auto intensity (AE copy)'],
                 ['Image quality':'3 positions (Draft, Standard, High)'],
                 ['Maximum Document Size':'A4 / LTR (216 x 297mm)'],
                 ['Multiple Copy':'Black / Colour max. 99 pages']
        ])

        p7Spec.put('General Specs',
                [['Acoustic Noise (PC Print)':'Photo (4 x 6") *24 Approx. 48.0dB(A)\n' +
                        'Plain Paper (A4, B / W) *25\n' +
                        'Quiet Mode "OFF" Approx. 49.0dB(A)\n' +
                        'Quiet Mode "ON" Approx. 42.5dB(A)'],
                 ['Camera Direct Spec.':'Compatible Digital Camera "PictBridge" compliant digital cameras, camcorders and camera phones\n' +
                         'Connecting type  WLAN\n' +
                         'File Format JPEG (Exif ver2.2 / 2.21 / 2.3 compliant) *17, PNG\n' +
                         'Layout *18\n' +
                         '1-up (bordered *15 / borderless *10) LTR, A4, 4 x 6", 5 x 7", 8 x 10", Square (5 x 5")\n' +
                         'Photo Index LTR, A4, 4 x 6", 5 x 7", 8 x 10", Square (5 x 5")\n' +
                         'Sticker 2 / 4 / 9 / 16-up 4 x 6"\n' +
                         '4-up (bordered) LTR, A4\n' +
                         'Print with shooting information LTR, A4, 4 x 6", 5 x 7", 8 x 10"\n' +
                         '20-up print with shooting information LTR, A4\n' +
                         '35-up contact print LTR, A4\n' +
                         'Disc Label Available\n' +
                         'File number & date print Available *21\n' +
                         'Print Quality *12 *13 *19 *20 Standard / High'],
                 ['Copy Speed *9 Based on ISO / IEC 29183':'Document: Colour sFCOT / Simplex: Approx. 19sec.\n' +
                         'sESAT / Simplex: Approx. 6.5ipm'],
                 ['Card Direct Spec.':'Compatible Storages SD Card, SDHC Card, SDXC Card, miniSD Card *27, micro SD Card *27, mini SDHC Card *27, micro SDHC Card *27, micro SDXC Card *27\n' +
                         'File Format JPEG and TIFF (Exif-compliant) taken by DCF (Ver.1.0 / 2.0) compliant digital cameras. (Exif ver 2.2 / 2.21 / 2.3 compliant)\n' +
                         'Storage function *14\n' +
                         'READ Function Available\n' +
                         'WRITE Function Available\n' +
                         'Print Quality *12 *13 Standard / High\n' +
                         'Layout\n' +
                         '1-up (bordered / borderless) *10 LTR, A4, 4 x 6", 5 x 7", 8 x 10", Square (5 x 5"), Card Size (91 x 55mm)\n' +
                         'Disc Label Available\n' +
                         'File number & date print *16 Only date print available'],
                 ['Compatible Operating System':'Windows 10 / Windows 8 / Windows 7 SP1 / Windows Vista SP2, Mac OS X v10.10.5 ～ v10.11, Mac OS v10.12'],
                 ['Dimensions (W x D x H)':'372 x 324 x 140mm'],
                 ['Environment':'Regulation RoHS (EU, China) *, WEEE (EU) *     *To be fixe\n' +
                         'Eco-Label Energy Star *     *To be fixed'],
                 ['Format':'A4'],
                 ['Functions':'Print, Scan, Copy'],
                 ['Interface':'USB 2.0 High Speed, Wireless LAN b/g/n, Wired LAN'],
                 ['LCD Display':'5.0in. Touch-Screen LCD'],
                ])

        def p8Spec = [:]
        p8Spec.put('Copy',
                [['Compatible Media':'Size A4 / A5 / B5 / LTR / 4 x 6" / 5 x 7" / Square (5 x 5") / Card Size (91 x 55mm)\n' +
                        'Type Plain Paper Photo Paper Pro Platinum (PT-101) Photo Paper Plus Glossy II (PP-201) Photo Paper Pro Luster (LU-101) Photo Paper Plus Semi-Gloss (SG-201) Glossy Photo Paper "Everyday Use" (GP-508) Matte Photo Paper (MP-101) Photo Paper Plus Glossy II PP-208 Printable Disc'],
                 ['Copy Speed (Colour, sFCOT)':'Approx. 19secs.'],
                 ['Copy Speed *9 Based on ISO / IEC 29183':'sFCOT / Simplex Approx. 19sec.\n' +
                         'sESAT / Simplex Approx. 6.5ipm'],
                 ['Density Adjustment':'9 positions, Auto intensity (AE copy)'],
                 ['Image quality':'3 positions (Draft, Standard, High)'],
                 ['Maximum Document Size':'A4 / LTR (216 x 297mm)'],
                 ['Multiple Copy':'Black / Colour max. 99 pages']
                ])

        p8Spec.put('General Specs',
                [['Acoustic Noise (PC Print)':'Photo ( 4 x 6") *24 Approx. 48.0dB(A)'],
                 ['Camera Direct Spec.':'Compatible Digital Camera "PictBridge" compliant digital cameras, camcorders and camera phones\n' +
                         'Connecting type  WLAN\n' +
                         'File Format JPEG (Exif ver2.2 / 2.21 / 2.3 compliant) *17, PNG\n' +
                         'Layout *18  \n' +
                         '1-up (bordered *15 / borderless *10) LTR, A4, 4 x 6", 5 x 7", 8 x 10", Square (5 x 5")\n' +
                         'Photo Index LTR, A4, 4 x 6", 5 x 7", 8 x 10", Square (5 x 5")\n' +
                         'Sticker 2 / 4 / 9 / 16-up 4 x 6"\n' +
                         '4-up (bordered) LTR, A4\n' +
                         'Print with shooting information LTR, A4, 4 x 6", 5 x 7", 8 x 10"\n' +
                         '20-up print with shooting information LTR, A4\n' +
                         '35-up contact print LTR, A4\n' +
                         'Disc Label Available\n' +
                         'File number & date print Available *21\n' +
                         'Print Quality *12 *13 *19 *20 Standard / High'],
                 ['Card Direct Spec.':'Compatible Storages SD Card, SDHC Card, SDXC Card, miniSD Card *27, micro SD Card *27, mini SDHC Card *27, micro SDHC Card *27, microSDXC Card *27\n' +
                         'File Format JPEG and TIFF (Exif-compliant) taken by DCF (Ver.1.0 / 2.0) compliant digital cameras. (Exif ver2.2 / 2.21 / 2.3 compliant)\n' +
                         'Storage function *14\n' +
                         'READ Function Available\n' +
                         'WRITE Function Available\n' +
                         'Print Quality *12 *13 Standard / High\n' +
                         'Layout\n' +
                         '1-up (bordered / borderless) *10 LTR, A4, 4 x 6", 5 x 7", 8 x 10", Square (5 x 5"), Card Size (91 x 55mm) \n' +
                         'Disc Label Available\n' +
                         'File number & date print *16 Only date print available'],
                 ['Compatible Operating System':'Windows 10 / Windows 8 / Windows 7 SP1 / Windows Vista SP2, Mac OS X v10.10.5 ～ v10.11, Mac OS v10.12'],
                 ['Dimensions (W x D x H)':'372 x 324 x 139mm'],
                 ['Environment':'Regulation RoHS (EU, China) *, WEEE (EU) *     *To be fixed\n' +
                         'Eco-Label Energy Star *     *To be fixed'],
                 ['Format':'A4'],
                 ['Functions':'Print, Scan, Copy'],
                 ['Interface':'USB 2.0 High Speed, Wireless LAN b/g/n'],
                 ['LCD Display':'4.3in. Touch-Screen LCD'],
                ])

        def products = [
                [p:p7,spec:p7Spec],
                [p:p8,spec:p8Spec]
        ]


        products.each {
            it.spec.each { key, data ->
                Group group = Group.findByTopicAndName(inkjetPrinter, key)

                data.each { spec ->
                    spec.each { specName, specVal ->

                        Specification s = Specification.findByGroupAndName(group, specName)
                        if (s != null) {
                            ProductSpecification pSpec = ProductSpecification.findByProductAndSpecification(it.p, s)
                            if (pSpec == null) {
                                if (specVal.length() > 254) {
                                    specVal = specVal.substring(0, 254)
                                }

                                new ProductSpecification(product: it.p, specification: s, specValue: specVal).save(failOnError: true)
                            }
                        }
                    }
                }
            }
        }

        prepareProductSpecification().each {k, v ->
            Product product = Product.findBySlug(k)
            if (product) {
                v.each { specMap ->
                    if (specMap.specValue){
                        Specification s = Specification.createCriteria().get {
                            eq("name", specMap.spec)
                            if (specMap.parent) {
                                parent{
                                    eq("name", specMap.parent)
                                }
                            }
                            group{
                                eq("name", specMap.group)
                                topic{
                                    eq("name", specMap.topic)
                                }
                            }
                        }

                        if(s) {
                            ProductSpecification ps = ProductSpecification.findByProductAndSpecification(product, s)
                            if(!ps) {
                                new ProductSpecification(product: product, specification: s, specValue: specMap.specValue).save(failOnError: true)
                            }
                        } else {
                            new Product()
                        }
                    }
                }
            }
        }
    }

    private static def prepareProductSpecification(){
        return [
                "eos-200d-colour": [
                        [topic: "DSLRs Specifications", group: "Type", parent: null, spec: "Type", specValue: "Digital, single-lens reflex, AF / AE camera with built-in flas"],
                        [topic: "DSLRs Specifications", group: "Type", parent: null, spec: "Recording Media", specValue: "SD memory card, SDHC memory card, SDXC memory card\n\n* Compatible with UHS-I"],
                        [topic: "DSLRs Specifications", group: "Type", parent: null, spec: "Sensor Size", specValue: "APS-C"],
                        [topic: "DSLRs Specifications", group: "Type", parent: null, spec: "Image sensor size", specValue: "Approx. 22.3 x 14.9mm"],
                        [topic: "DSLRs Specifications", group: "Type", parent: null, spec: "Compatible lenses", specValue: "Canon EF lenses (including EF-S lenses)\n\n* Excluding EF-M lenses\n(35 mm-equivalent focal length is approx. 1.6 times the lens focal length)"],
                        [topic: "DSLRs Specifications", group: "Type", parent: null, spec: "Lens mount", specValue: "Canon EF mount"],
                        [topic: "DSLRs Specifications", group: "Image Sensor", parent: null, spec: "Type", specValue: "CMOS sensor"],
                        [topic: "DSLRs Specifications", group: "Image Sensor", parent: null, spec: "Effective pixels", specValue: "Approx. 24.2 megapixels"],
                        [topic: "DSLRs Specifications", group: "Image Sensor", parent: null, spec: "Aspect ratio", specValue: "3:2"],
                        [topic: "DSLRs Specifications", group: "Image Sensor", parent: null, spec: "Dust delete feature", specValue: "Auto, Manual, Dust Delete Data appending"],
                        [topic: "DSLRs Specifications", group: "Recording System", parent: null, spec: "Recording format", specValue: "Design rule for Camera File System (DCF) 2.0"],
                        [topic: "DSLRs Specifications", group: "Recording System", parent: null, spec: "Image type", specValue: "JPEG, RAW (14-bit Canon original)\nRAW+JPEG Large simultaneous recording possible"],
                        [topic: "DSLRs Specifications", group: "Recording System", parent: null, spec: "Recorded pixels", specValue: ""],
                        [topic: "DSLRs Specifications", group: "Recording System", parent: "Recorded pixels", spec: "L (Large)", specValue: "24.0 megapixels (6000 x 4000)"],
                        [topic: "DSLRs Specifications", group: "Recording System", parent: "Recorded pixels", spec: "M (Medium)", specValue: "Approx. 10.6 megapixels (3984 x 2656)"],
                        [topic: "DSLRs Specifications", group: "Recording System", parent: "Recorded pixels", spec: "S1 (Small 1)", specValue: "Approx. 5.9 megapixels (2976 x 1984)"],
                        [topic: "DSLRs Specifications", group: "Recording System", parent: "Recorded pixels", spec: "S2 (Small 2)", specValue: "Approx. 3.8 megapixels (2400 x 1600)"],
                        [topic: "DSLRs Specifications", group: "Recording System", parent: "Recorded pixels", spec: "RAW", specValue: "24.0 megapixels (6000 x 4000)"],
                        [topic: "DSLRs Specifications", group: "Image Processing During Shooting", parent: null, spec: "Picture Style", specValue: "Auto, Standard, Portrait, Landscape, Fine Detail, Neutral, Faithful, Monochrome, User Defined 1 - 3"],
                        [topic: "DSLRs Specifications", group: "Image Processing During Shooting", parent: null, spec: "White balance", specValue: "Auto (Ambience priority), Auto (White priority), Preset (Daylight, Shade, Cloudy, Tungsten light, White fluorescent light, Flash), Custom \nWhite balance correction, and White balance bracketing provided\n\n* Flash color temperature information transmission possible"],
                        [topic: "DSLRs Specifications", group: "Image Processing During Shooting", parent: null, spec: "Noise reduction", specValue: "Applicable to long exposures and high ISO speed shots"],
                        [topic: "DSLRs Specifications", group: "Image Processing During Shooting", parent: null, spec: "Automatic image brightness correction", specValue: "Auto Lighting Optimizer"],
                        [topic: "DSLRs Specifications", group: "Image Processing During Shooting", parent: null, spec: "Highlight tone priority", specValue: "Provided"],
                        [topic: "DSLRs Specifications", group: "Image Processing During Shooting", parent: null, spec: "Lens aberration correction", specValue: "Peripheral illumination correction, Chromatic aberration correction, Distortion correction, Diffraction correction"],
                        [topic: "DSLRs Specifications", group: "Viewfinder", parent: null, spec: "Type", specValue: "Eye-level pentamirror"],
                        [topic: "DSLRs Specifications", group: "Viewfinder", parent: null, spec: "Coverage", specValue: "Vertical / Horizontal approx. 95% (with Eye point approx. 19mm)"],
                        [topic: "DSLRs Specifications", group: "Viewfinder", parent: null, spec: "Magnification", specValue: "Approx. 0.87x (-1m-1 with 50mm lens at infinity)"],
                        [topic: "DSLRs Specifications", group: "Viewfinder", parent: null, spec: "Eye point", specValue: "Approx. 19mm (from eyepiece lens center at -1m-1)"],
                        [topic: "DSLRs Specifications", group: "Viewfinder", parent: null, spec: "Built-in dioptric adjustment", specValue: "Approx. -3.0 - +1.0m-1 (dpt)"],
                        [topic: "DSLRs Specifications", group: "Viewfinder", parent: null, spec: "Focusing screen", specValue: "Fixed, Precision Matte"],
                        [topic: "DSLRs Specifications", group: "Viewfinder", parent: null, spec: "Mirror", specValue: "Quick-return type"],
                        [topic: "DSLRs Specifications", group: "Viewfinder", parent: null, spec: "Depth-of-field preview", specValue: "Provided"],
                        [topic: "DSLRs Specifications", group: "Autofocus", parent: null, spec: "Type", specValue: "TTL secondary image-registration, phase-difference detection with the dedicated AF sensor"],
                        [topic: "DSLRs Specifications", group: "Autofocus", parent: null, spec: "AF points", specValue: "9-point AF (Center point: cross-type and vertical line sensitive when supporting f/2.8)"],
                        [topic: "DSLRs Specifications", group: "Autofocus", parent: null, spec: "Focusing brightness range", specValue: "EV -0.5 - 18 (with the center AF point supporting f/2.8, One-Shot AF, at room temperature, ISO 100)"],
                        [topic: "DSLRs Specifications", group: "Autofocus", parent: null, spec: "AF operation", specValue: "One-Shot AF, AI Servo AF, AI Focus AF, Manual focusing (MF)"],
                        [topic: "DSLRs Specifications", group: "Autofocus", parent: null, spec: "AF-assist beam", specValue: "Small series of flashes fired by built-in flash"],
                        [topic: "DSLRs Specifications", group: "Exposure Control", parent: null, spec: "Metering modes", specValue: "63-zone TTL open-aperture metering\nEvaluative metering (linked to all AF points)\nPartial metering (approx. 9% of viewfinder at center)\nSpot metering (approx. 4% of viewfinder at center)\nCenter-weighted average metering"],
                        [topic: "DSLRs Specifications", group: "Exposure Control", parent: null, spec: "Metering brightness range", specValue: "EV 1 - 20 (at room temperature, ISO 100)"],
                        [topic: "DSLRs Specifications", group: "Exposure Control", parent: null, spec: "Shooting mode", specValue: ""],
                        [topic: "DSLRs Specifications", group: "Exposure Control", parent: "Shooting mode", spec: "Basic Zone modes", specValue: "Scene Intelligent Auto, Flash Off, Creative Auto, Special scene modes (Portrait, Group Photo, Landscape, Sports, Kids, Close-up, Food, Candlelight, Night Portrait, Handheld Night Scene, HDR Backlight Control), Creative filters (Grainy B / W, Soft focus, Fish-eye effect, Water painting effect, Toy camera effect, Miniature effect, HDR art standard, HDR art vivid, HDR art bold, HDR art embossed)"],
                        [topic: "DSLRs Specifications", group: "Exposure Control", parent: "Shooting mode", spec: "Creative Zone modes", specValue: "Program AE, Shutter-priority AE, Aperture-priority AE, Manual exposure"],
                        [topic: "DSLRs Specifications", group: "Exposure Control", parent: null, spec: "ISO speed (Recommended exposure index)", specValue: "Basic Zone modes: ISO speed set automatically\nCreative Zone modes: ISO Auto, ISO 100 - ISO 25600 set manually (whole-stop increments), and ISO expansion to H (equivalent to ISO 51200) provided"],
                        [topic: "DSLRs Specifications", group: "Exposure Control", parent: null, spec: "Exposure compensation", specValue: "Manual: ±5* stops in 1/3- or 1/2-stop increments\n\n* ±3 stops with [Shooting screen: Guided] set\nAEB: ±2 stops in 1/3- or 1/2-stop increments (can be combined with manual exposure compensation)"],
                        [topic: "DSLRs Specifications", group: "Exposure Control", parent: null, spec: "AE lock", specValue: "Auto: Applied in One-Shot AF with evaluative metering when focus is achieved"],
                        [topic: "DSLRs Specifications", group: "Shutter", parent: null, spec: "Type", specValue: "Electronically-controlled, focal-plane shutter"],
                        [topic: "DSLRs Specifications", group: "Shutter", parent: null, spec: "Shutter Speed", specValue: "1/4000sec. to 30sec. (Total shutter speed range. Available range varies by shooting mode.), Bulb, X-sync at 1/200sec."],
                        [topic: "DSLRs Specifications", group: "Flash", parent: null, spec: "Built-in flash", specValue: ""],
                        [topic: "DSLRs Specifications", group: "Flash", parent: "Built-in flash", spec: "Retractable, auto pop-up flash\n Guide No.", specValue: "Approx. 9.8 / 32.1 (ISO 100, in meters/feet)"],
                        [topic: "DSLRs Specifications", group: "Flash", parent: "Built-in flash", spec: "Flash coverage", specValue: "Approx. 18mm lens angle of view"],
                        [topic: "DSLRs Specifications", group: "Flash", parent: "Built-in flash", spec: "Recharge time", specValue: "Approx. 3sec."],
                        [topic: "DSLRs Specifications", group: "Flash", parent: null, spec: "External flash", specValue: "Compatible with EX-series Speedlites"],
                        [topic: "DSLRs Specifications", group: "Flash", parent: null, spec: "Flash metering", specValue: "E-TTL II autoflash"],
                        [topic: "DSLRs Specifications", group: "Flash", parent: null, spec: "Flash exposure compensation", specValue: "±2 stops in 1/3- or 1/2-stop increments"],
                        [topic: "DSLRs Specifications", group: "Flash", parent: null, spec: "FE lock", specValue: "Provided"],
                        [topic: "DSLRs Specifications", group: "Flash", parent: null, spec: "PC terminal", specValue: "None"],
                        [topic: "DSLRs Specifications", group: "Drive System", parent: null, spec: "Drive modes", specValue: "Single shooting, Continuous shooting, Silent single shooting, Silent continuous shooting, 10-sec. self-timer/ remote control*, 2-sec. delay, 10-sec. delay with continuous shooting\n\n*When Wireless Remote Control BR-E1 (sold separately) is used."],
                        [topic: "DSLRs Specifications", group: "Drive System", parent: null, spec: "Continuous shooting speed", specValue: "Max. approx. 5.0 shots/sec.*\n\n* Max. approx. 3.5 shots/sec. during Live View shooting or when [Servo AF] is set.\nSilent continuous shooting: Max. approx. 2.5 shots/sec."],
                        [topic: "DSLRs Specifications", group: "Drive System", parent: null, spec: "Max. burst (Approx.)", specValue: "* Figures are based on Canon's testing standards (3:2 aspect ratio, ISO 100 and Standard Picture Style) using a Canon’s standard testing 8 GB card.\n* Figures in parentheses are the number of shots when a Canon's standard testing UHS-I 16 GB card is used.\n* \"Full\" indicates that shooting is possible until the card becomes full."],
                        [topic: "DSLRs Specifications", group: "Drive System", parent: null, spec: "JPEG Large / Fine", specValue: "Full (Full)"],
                        [topic: "DSLRs Specifications", group: "Drive System", parent: null, spec: "RAW", specValue: "Approx. 6 shots (approx. 6 shots)"],
                        [topic: "DSLRs Specifications", group: "Drive System", parent: null, spec: "RAW+JPEG Large / Fine", specValue: "Approx. 6 shots (approx. 6 shots)"],
                        [topic: "DSLRs Specifications", group: "Live View Shooting", parent: null, spec: "Focus method", specValue: "Dual Pixel CMOS AF system"],
                        [topic: "DSLRs Specifications", group: "Live View Shooting", parent: null, spec: "AF method", specValue: "Face+Tracking, Smooth zone, Live 1-point AF\nManual focus (approx. 5x / 10x magnification possible)"],
                        [topic: "DSLRs Specifications", group: "Live View Shooting", parent: null, spec: "AF operation", specValue: "One-Shot AF, Servo AF"],
                        [topic: "DSLRs Specifications", group: "Live View Shooting", parent: null, spec: "Focusing brightness range", specValue: "EV -2 - 18 (at room temperature, ISO 100, One-Shot AF)"],
                        [topic: "DSLRs Specifications", group: "Live View Shooting", parent: null, spec: "Touch shutter", specValue: "Provided"],
                        [topic: "DSLRs Specifications", group: "Live View Shooting", parent: null, spec: "Metering mode", specValue: "Evaluative metering (315 zones), Partial metering\n(approx. 6.0% of Live View screen), Spot metering\n(approx. 2.6% of Live View screen), Center-weighted average metering"],
                        [topic: "DSLRs Specifications", group: "Live View Shooting", parent: null, spec: "Metering brightness range", specValue: "EV 0 - 20 (at room temperature, ISO 100)"],
                        [topic: "DSLRs Specifications", group: "Live View Shooting", parent: null, spec: "Exposure compensation", specValue: "±3 stops in 1/3-stop or 1/2-stop increments"],
                        [topic: "DSLRs Specifications", group: "Live View Shooting", parent: null, spec: "Creative Filters", specValue: "Provided"],
                        [topic: "DSLRs Specifications", group: "Live View Shooting", parent: null, spec: "Grid display", specValue: "Three types"],
                        [topic: "DSLRs Specifications", group: "Movie Shooting", parent: null, spec: "Recording format", specValue: "MP4\n\n* Time-lapse movie shooting: MOV"],
                        [topic: "DSLRs Specifications", group: "Movie Shooting", parent: null, spec: "Movie", specValue: "MPEG-4 AVC / H.264 Variable (average) bit rate"],
                        [topic: "DSLRs Specifications", group: "Movie Shooting", parent: null, spec: "Audio", specValue: "AAC"],
                        [topic: "DSLRs Specifications", group: "Movie Shooting", parent: null, spec: "Recording size and frame rate", specValue: ""],
                        [topic: "DSLRs Specifications", group: "Movie Shooting", parent: "Recording size and frame rate", spec: "1920 x 1080 (Full HD)", specValue: "59.94p / 50.00p / 29.97p / 25.00p / 23.98p"],
                        [topic: "DSLRs Specifications", group: "Movie Shooting", parent: "Recording size and frame rate", spec: "1280 x 720 (HD)", specValue: "59.94p / 50.00p / 29.97p / 25.00p"],
                        [topic: "DSLRs Specifications", group: "Movie Shooting", parent: "Recording size and frame rate", spec: "640 x 480 (VGA)", specValue: "29.97p / 25.00p"],
                        [topic: "DSLRs Specifications", group: "Movie Shooting", parent: null, spec: "Compression method", specValue: "IPB (Standard), IPB (Light)\n\n* Time-lapse movie shooting: ALL-I"],
                        [topic: "DSLRs Specifications", group: "Movie Shooting", parent: null, spec: "Bit rate", specValue: ""],
                        [topic: "DSLRs Specifications", group: "Movie Shooting", parent: "Bit rate", spec: "Full HD (59.94p / 50.00p) / IPB (Standard)", specValue: "Approx. 60Mbps"],
                        [topic: "DSLRs Specifications", group: "Movie Shooting", parent: "Bit rate", spec: "Full HD (29.97p / 25.00p / 23.98p) / IPB (Standard)", specValue: "Approx. 30Mbps"],
                        [topic: "DSLRs Specifications", group: "Movie Shooting", parent: "Bit rate", spec: "Full HD (29.97p / 25.00p) / IPB (Light)", specValue: "Approx. 12Mbps"],
                        [topic: "DSLRs Specifications", group: "Movie Shooting", parent: "Bit rate", spec: "HD (59.94p / 50.00p) / IPB (Standard)", specValue: "Approx. 26Mbps"],
                        [topic: "DSLRs Specifications", group: "Movie Shooting", parent: "Bit rate", spec: "HD (29.97p / 25.00p) / IPB (Light)", specValue: "Approx. 4Mbps"],
                        [topic: "DSLRs Specifications", group: "Movie Shooting", parent: "Bit rate", spec: "VGA (29.97p / 25.00p) (Standard)", specValue: "Approx. 9Mbps"],
                        [topic: "DSLRs Specifications", group: "Movie Shooting", parent: "Bit rate", spec: "VGA (29.97p / 25.00p) (Light)", specValue: "Approx. 3Mbps"],
                        [topic: "DSLRs Specifications", group: "Movie Shooting", parent: "Bit rate", spec: "HDR movie", specValue: "Approx. 30Mbps"],
                        [topic: "DSLRs Specifications", group: "Movie Shooting", parent: "Bit rate", spec: "Time lapse movie", specValue: "Approx. 90Mbps"],
                        [topic: "DSLRs Specifications", group: "Movie Shooting", parent: null, spec: "Focus system", specValue: "Dual Pixel CMOS AF system"],
                        [topic: "DSLRs Specifications", group: "Movie Shooting", parent: null, spec: "AF method", specValue: "Face+Tracking, Smooth zone, Live 1-point AF Manual focus (approx. 5x / 10x magnification available for focus check)"],
                        [topic: "DSLRs Specifications", group: "Movie Shooting", parent: null, spec: "Movie Servo AF", specValue: "Provided"],
                        [topic: "DSLRs Specifications", group: "Movie Shooting", parent: null, spec: "Digital zoom", specValue: "Approx. 3x - 10x"],
                        [topic: "DSLRs Specifications", group: "Movie Shooting", parent: null, spec: "Focusing brightness range", specValue: "EV -2 - 18 (at room temperature, ISO 100, One-Shot AF)"],
                        [topic: "DSLRs Specifications", group: "Movie Shooting", parent: null, spec: "Metering mode", specValue: "Center-weighted average and Evaluative metering with the image sensor\n\n* Automatically set by the AF method"],
                        [topic: "DSLRs Specifications", group: "Movie Shooting", parent: null, spec: "Metering brightness range", specValue: "EV 0 - 20 (at room temperature, ISO 100, with centerweighted average metering)"],
                        [topic: "DSLRs Specifications", group: "Movie Shooting", parent: null, spec: "Exposure control", specValue: "Autoexposure shooting (Program AE for movie shooting) and manual exposure"],
                        [topic: "DSLRs Specifications", group: "Movie Shooting", parent: null, spec: "Exposure compensation", specValue: "±3 stops in 1/3- or 1/2-stop increments"],
                        [topic: "DSLRs Specifications", group: "Movie Shooting", parent: null, spec: "ISO speed (Recommended exposure index)", specValue: ""],
                        [topic: "DSLRs Specifications", group: "Movie Shooting", parent: "ISO speed (Recommended exposure index)", spec: "For autoexposure shooting", specValue: "ISO 100 - ISO 12800 set automatically. In Creative Zone modes, the upper limit is expandable to H (equivalent to ISO 25600)."],
                        [topic: "DSLRs Specifications", group: "Movie Shooting", parent: "ISO speed (Recommended exposure index)", spec: "For manual exposure", specValue: "ISO Auto (ISO 100 – ISO 12800 set automatically), ISO 100 - ISO 12800 set manually (whole-stop increments), expandable to H (equivalent to ISO 25600)"],
                        [topic: "DSLRs Specifications", group: "Movie Shooting", parent: null, spec: "ISO speed settings", specValue: "Maximum limit for ISO Auto settable"],
                        [topic: "DSLRs Specifications", group: "Movie Shooting", parent: null, spec: "HDR movie shooting", specValue: "Possible"],
                        [topic: "DSLRs Specifications", group: "Movie Shooting", parent: null, spec: "Video snapshots", specValue: "Settable to 2sec. / 4sec. / 8sec."],
                        [topic: "DSLRs Specifications", group: "Movie Shooting", parent: null, spec: "Creative filters for movies", specValue: "Dream, Old Movies, Memory, Dramatic B&W, Miniature effect movie"],
                        [topic: "DSLRs Specifications", group: "Movie Shooting", parent: null, spec: "Sound recording", specValue: "Built-in stereo microphones, external stereo microphone terminal provided\nSound-recording level adjustable, wind filter provided, attenuator provided"],
                        [topic: "DSLRs Specifications", group: "Movie Shooting", parent: null, spec: "Grid display", specValue: "Three types"],
                        [topic: "DSLRs Specifications", group: "Movie Shooting", parent: null, spec: "Time-lapse movie", specValue: "Shooting interval (hours:minutes:seconds), Number of shots, Auto exposure (Fixed 1st frame, Each frame), LCD auto off, Beep as image shot settable"],
                        [topic: "DSLRs Specifications", group: "LCD Monitor", parent: null, spec: "Type", specValue: "TFT color liquid-crystal monitor"],
                        [topic: "DSLRs Specifications", group: "LCD Monitor", parent: null, spec: "Monitor size and dots", specValue: "Wide, 7.7cm (3.0-in.) (3:2) with approx. 1.04 million dots"],
                        [topic: "DSLRs Specifications", group: "LCD Monitor", parent: null, spec: "Brightness adjustment", specValue: "Manual (7 levels)"],
                        [topic: "DSLRs Specifications", group: "LCD Monitor", parent: null, spec: "Interface languages", specValue: "25"],
                        [topic: "DSLRs Specifications", group: "LCD Monitor", parent: null, spec: "Touch screen technology", specValue: "Capacitive sensing"],
                        [topic: "DSLRs Specifications", group: "Playback", parent: null, spec: "Image display format", specValue: "Single-image display (without shooting information), Single-image display (with basic information), Single image display (Shooting information displayed: Detailed information, Lens / histogram, White balance, Picture Style 1, Picture Style 2, Color space/noise reduction, Lens aberration correction), Index display (4 / 9 / 36 / 100 images)"],
                        [topic: "DSLRs Specifications", group: "Playback", parent: null, spec: "Zoom magnification", specValue: "Approx. 1.5x - 10x"],
                        [topic: "DSLRs Specifications", group: "Playback", parent: null, spec: "Highlight alert", specValue: "Overexposed highlights blink"],
                        [topic: "DSLRs Specifications", group: "Playback", parent: null, spec: "AF point display", specValue: "Provided (may not be displayed depending on shooting conditions)"],
                        [topic: "DSLRs Specifications", group: "Playback", parent: null, spec: "Image search", specValue: "Search conditions settable (Rating, Date, Folder, Protect, File type)"],
                        [topic: "DSLRs Specifications", group: "Playback", parent: null, spec: "Image browsing methods", specValue: "Single image, 10 images, specified number, date, folder, movies, stills, protect, rating"],
                        [topic: "DSLRs Specifications", group: "Playback", parent: null, spec: "Image rotation", specValue: "Possible"],
                        [topic: "DSLRs Specifications", group: "Playback", parent: null, spec: "Rating", specValue: "Provided"],
                        [topic: "DSLRs Specifications", group: "Playback", parent: null, spec: "Movie playback", specValue: "Possible (LCD monitor, HDMI), built-in speaker"],
                        [topic: "DSLRs Specifications", group: "Playback", parent: null, spec: "Image protect", specValue: "Possible"],
                        [topic: "DSLRs Specifications", group: "Playback", parent: null, spec: "Slide show", specValue: "Automatically play back all images or the images that match search conditions"],
                        [topic: "DSLRs Specifications", group: "Playback", parent: null, spec: "Background music", specValue: "Selectable for slide shows and movie playback"],
                        [topic: "DSLRs Specifications", group: "Post-Processing of Images", parent: null, spec: "Creative filters", specValue: "Grainy B/W, Soft focus, Fish-eye effect, Art bold effect, Water painting effect, Toy camera effect, Miniature effect"],
                        [topic: "DSLRs Specifications", group: "Post-Processing of Images", parent: null, spec: "Resize", specValue: "Possible"],
                        [topic: "DSLRs Specifications", group: "Post-Processing of Images", parent: null, spec: "Cropping", specValue: "Possible"],
                        [topic: "DSLRs Specifications", group: "Print Ordering", parent: null, spec: "DPOF", specValue: "DPOF Version 1.1 compliant"],
                        [topic: "DSLRs Specifications", group: "Wi-Fi", parent: null, spec: "Standards compliance", specValue: "IEEE 802.11b/g/n"],
                        [topic: "DSLRs Specifications", group: "Wi-Fi", parent: null, spec: "Transmission method", specValue: "DS-SS modulation (IEEE 802.11b)\nOFDM modulation (IEEE 802.11g/n)"],
                        [topic: "DSLRs Specifications", group: "Wi-Fi", parent: null, spec: "Transmission range", specValue: "Approx. 15m / 49.2ft.\n\n*When communicating with a smartphone\n*With no obstructions between the transmitting and receiving antennas and no radio interference"],
                        [topic: "DSLRs Specifications", group: "Wi-Fi", parent: null, spec: "Transmission frequency (central frequency)", specValue: "Frequency: 2412 to 2462 MHz\nChannels: 1 to 11 ch"],
                        [topic: "DSLRs Specifications", group: "Wi-Fi", parent: null, spec: "Connection method", specValue: "Camera access point mode, infrastructure*\n\n* Wi-Fi Protected Setup supported"],
                        [topic: "DSLRs Specifications", group: "Wi-Fi", parent: null, spec: "Security", specValue: "Authentication method: Open system, Shared key, WPA / WPA2-PSK\nEncryption: WEP, TKIP, AES"],
                        [topic: "DSLRs Specifications", group: "Wi-Fi", parent: null, spec: "Communication with a smartphone", specValue: "Images can be viewed, controlled, and received using a smartphone. \nRemote control of the camera using a smartphone is possible. \nImages can be sent to a smartphone."],
                        [topic: "DSLRs Specifications", group: "Wi-Fi", parent: null, spec: "Transfer images between cameras", specValue: "Transferring one image, Transferring selected images, Transferring resized images"],
                        [topic: "DSLRs Specifications", group: "Wi-Fi", parent: null, spec: "Connect to Connect Station", specValue: "Images can be sent to and saved on Connect Station."],
                        [topic: "DSLRs Specifications", group: "Wi-Fi", parent: null, spec: "Remote operation using EOS Utility", specValue: "Remote control functions and image viewing functions of EOS Utility can be used wirelessly."],
                        [topic: "DSLRs Specifications", group: "Wi-Fi", parent: null, spec: "Print from Wi-Fi printers", specValue: "Images can be sent to a Wi-Fi printer."],
                        [topic: "DSLRs Specifications", group: "Wi-Fi", parent: null, spec: "Send images to a Web service", specValue: "Images in the camera or links to images can be sent to registered Web services."],
                        [topic: "DSLRs Specifications", group: "NFC", parent: null, spec: "Standards compliance", specValue: "NFC Forum Type 3/4 Tag compliant (dynamic)"],
                        [topic: "DSLRs Specifications", group: "Bluetooth", parent: null, spec: "Standards compliance", specValue: "Bluetooth Specification Version 4.1 compliant (Bluetooth low energy technology)"],
                        [topic: "DSLRs Specifications", group: "Bluetooth", parent: null, spec: "Transmission method", specValue: "GFSK modulation"],
                        [topic: "DSLRs Specifications", group: "Customization Features", parent: null, spec: "Custom Functions", specValue: "11"],
                        [topic: "DSLRs Specifications", group: "Customization Features", parent: null, spec: "My Menu", specValue: "Up to 5 screens can be registered"],
                        [topic: "DSLRs Specifications", group: "Customization Features", parent: null, spec: "Copyright information", specValue: "Text entry and appending possible"],
                        [topic: "DSLRs Specifications", group: "Interface", parent: null, spec: "DIGITAL terminal", specValue: "Computer communication (Hi-Speed USB equivalent),\nConnect Station CS100 connection"],
                        [topic: "DSLRs Specifications", group: "Interface", parent: null, spec: "HDMI mini OUT terminal", specValue: "Type C (Auto switching of resolution), CEC-compatible"],
                        [topic: "DSLRs Specifications", group: "Interface", parent: null, spec: "External microphone IN terminal", specValue: "3.5mm diameter stereo mini-jack"],
                        [topic: "DSLRs Specifications", group: "Interface", parent: null, spec: "Remote control terminal", specValue: "For Remote Switch RS-60E3"],
                        [topic: "DSLRs Specifications", group: "Interface", parent: null, spec: "Wireless remote control", specValue: "Compatible with Wireless Remote Control BR-E1 (Bluetooth connection)"],
                        [topic: "DSLRs Specifications", group: "Interface", parent: null, spec: "Eye-Fi card", specValue: "Compatible"],
                        [topic: "DSLRs Specifications", group: "Power", parent: null, spec: "Battery", specValue: "Battery Pack LP-E17 (Quantity 1)\n\n* AC power usable with household power outlet accessories."],
                        [topic: "DSLRs Specifications", group: "Power", parent: null, spec: "Number of possible shots (Based on CIPA testing standards)", specValue: ""],
                        [topic: "DSLRs Specifications", group: "Power", parent: "Number of possible shots (Based on CIPA testing standards)", spec: "With viewfinder shooting", specValue: "Approx. 650 shots at room temperature (23°C / 73°F) Approx. 620 shots at low temperatures (0°C / 32°F)"],
                        [topic: "DSLRs Specifications", group: "Power", parent: "Number of possible shots (Based on CIPA testing standards)", spec: "With Live View shooting", specValue: "Approx. 260 shots at room temperature (23°C / 73°F) Approx. 240 shots at low temperatures (0°C / 32°F)"],
                        [topic: "DSLRs Specifications", group: "Power", parent: "Number of possible shots (Based on CIPA testing standards)", spec: "Movie shooting time", specValue: "Approx. 2hr. at room temperature (23°C / 73°F)\nApprox. 1hr. 45min. at low temperatures (0°C / 32°F)"],
                        [topic: "DSLRs Specifications", group: "Dimensions and Weight", parent: null, spec: "Dimensions (W x H x D)", specValue: "Approx. 122.4 x 92.6 x 69.8mm / 4.82 x 3.65 x 2.75in."],
                        [topic: "DSLRs Specifications", group: "Dimensions and Weight", parent: null, spec: "Weight", specValue: ""],
                        [topic: "DSLRs Specifications", group: "Dimensions and Weight", parent: "Weight", spec: "Black", specValue: "Approx. 453g / 15.98oz. (Including battery and card)\nApprox. 406g / 14.32oz. (Body only)"],
                        [topic: "DSLRs Specifications", group: "Dimensions and Weight", parent: "Weight", spec: "Silver", specValue: "Approx. 454g / 16.01oz. (Including battery and card)\nApprox. 407g / 14.36oz. (Body only)"],
                        [topic: "DSLRs Specifications", group: "Dimensions and Weight", parent: "Weight", spec: "White", specValue: "Approx. 456g / 16.08oz. (Including battery and card)\nApprox. 409g / 14.43oz. (Body only)"],
                        [topic: "DSLRs Specifications", group: "Operation Environment", parent: null, spec: "Working temperature range", specValue: "0°C - 40°C / 32°F - 104°F"],
                        [topic: "DSLRs Specifications", group: "Operation Environment", parent: null, spec: "Working humidity", specValue: "85% or less"]
                ]
        ]
    }
}
