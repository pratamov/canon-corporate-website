package canon

import canon.enums.Status
import canon.utils.SlugCodec

import java.text.SimpleDateFormat

class SubjectInit {

    static init() {
        Language en = Language.findById("en")

        prepareData()?.each { k, v ->
            Subject subject = Subject.findBySlug(k)
            if(!subject) {
                Language language = Language.findById(v?.language)
                new Subject(slug: k, name: v?.name, language: language, status: Status.ACTIVE).save(failOnError: true)
            }
        }
    }

    private static def prepareData() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss")
        return [
                (SlugCodec.encode("EOS")): [
                        name: "EOS",
                        language: "en",
                        status: Status.ACTIVE
                ],
                (SlugCodec.encode("Accessories")): [
                        name: "Accessories",
                        language: "en",
                        status: Status.ACTIVE
                ],
                (SlugCodec.encode("Low light photography")): [
                        name: "Low light photography",
                        language: "en",
                        status: Status.ACTIVE
                ],
                (SlugCodec.encode("Nature photography")): [
                        name: "Nature photography",
                        language: "en",
                        status: Status.ACTIVE
                ],
                (SlugCodec.encode("Photo techniques")): [
                        name: "Photo techniques",
                        language: "en",
                        status: Status.ACTIVE
                ],
                (SlugCodec.encode("Printers")): [
                        name: "Printers",
                        language: "en",
                        status: Status.ACTIVE
                ],
                (SlugCodec.encode("Sports Photography")): [
                        name: "Sports Photography",
                        language: "en",
                        status: Status.ACTIVE
                ],
                (SlugCodec.encode("Events")): [
                        name: "Events",
                        language: "en",
                        status: Status.ACTIVE
                ],
                (SlugCodec.encode("Photographer interviews")): [
                        name: "Photographer interviews",
                        language: "en",
                        status: Status.ACTIVE
                ]
        ]
    }
}
