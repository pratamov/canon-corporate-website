package canon

import canon.enums.*

import java.text.SimpleDateFormat

class BannerInit {

    static init() {
        Language en = Language.findById("en")
        //Site sg = Site.findByCode("singapore")
        Site asia = Site.findByCode("asia")

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss")
        //sdf.setTimeZone(TimeZone.getTimeZone(applicationTimeZone))

        // Homepage > Consumer
        Banner homeBanner = new Banner(title: "Asia Consumer Home Slider", segment: Segment.CONSUMER, language: en,
                site: asia, location: BannerLocation.HOMEPAGE, status: Status.PUBLISHED,
                startDate: sdf.parse("2017-12-01 12:00:00"), endDate: sdf.parse("2018-12-01 12:00:00"))
        homeBanner.addToBannerItems(new BannerItem(bannerType: BannerType.PRODUCT_DEALS, title: "Great Singapore Sale 2017<br />9 June - 13 August",
                background: new canon.model.Media("", "https://canon-corpweb-dev.s3.amazonaws.com/images/bg/img01.jpg", null, MediaType.IMAGE, MediaSource.EXTERNAL), itemOrder: 0))
        homeBanner.addToBannerItems(new BannerItem(bannerType: BannerType.PRODUCT_DEALS, title: "Great Singapore Sale 2018<br />9 September - 13 December",
                background: new canon.model.Media("", "https://canon-corpweb-dev.s3.amazonaws.com/images/bg/img02.jpg", null, MediaType.IMAGE, MediaSource.EXTERNAL), itemOrder: 1))
        homeBanner.addToBannerItems(new BannerItem(bannerType: BannerType.PRODUCT_DEALS, title: "Great Singapore Sale 2019<br />9 January - 13 May",
                background: new canon.model.Media("", "https://canon-corpweb-dev.s3.amazonaws.com/images/bg/img03.jpg", null, MediaType.IMAGE, MediaSource.EXTERNAL), itemOrder: 2))
        homeBanner.save(failOnError: true)

        // Homepage > Business
        Banner homeBusinessBanner = new Banner(title: "Asia Business Home Slider", segment: Segment.BUSINESS, language: en,
                site: asia, location: BannerLocation.HOMEPAGE, status: Status.PUBLISHED,
                startDate: sdf.parse("2017-12-01 12:00:00"), endDate: sdf.parse("2018-12-01 12:00:00"))
        homeBusinessBanner.addToBannerItems(new BannerItem(bannerType: BannerType.PRODUCT_DEALS, title: "Business Great Singapore Sale 2017<br />9 June - 13 August",
                background: new canon.model.Media("", "https://canon-corpweb-dev.s3.amazonaws.com/images/bg/img01.jpg", null, MediaType.IMAGE, MediaSource.EXTERNAL), itemOrder: 0))
        homeBusinessBanner.addToBannerItems(new BannerItem(bannerType: BannerType.PRODUCT_DEALS, title: "Business Great Singapore Sale 2018<br />9 September - 13 December",
                background: new canon.model.Media("", "https://canon-corpweb-dev.s3.amazonaws.com/images/bg/img02.jpg", null, MediaType.IMAGE, MediaSource.EXTERNAL), itemOrder: 1))
        homeBusinessBanner.addToBannerItems(new BannerItem(bannerType: BannerType.PRODUCT_DEALS, title: "Business Great Singapore Sale 2019<br />9 January - 13 May",
                background: new canon.model.Media("", "https://canon-corpweb-dev.s3.amazonaws.com/images/bg/img03.jpg", null, MediaType.IMAGE, MediaSource.EXTERNAL), itemOrder: 2))
        homeBusinessBanner.save(failOnError: true)
    }
}
