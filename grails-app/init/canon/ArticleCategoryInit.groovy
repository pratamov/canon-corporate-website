package canon

import canon.enums.CategoryType
import canon.enums.Segment
import canon.enums.Status

/**
 * Created by User on 05/12/2017.
 */
class ArticleCategoryInit {
    def static init() {
        prepareConsumerList().each { k, v ->
            Category category = Category.findBySlugAndSegment(k, v?.segment)
            if (!category) {
                Site site = Site.findByCode(v?.site)
                Language language = Language.findById(v?.language)

                category = new Category()
                category.site = site
                category.language = language
                category.name = v?.name
                category.slug = k
                category.categoryType = v?.categoryType
                category.status = v?.status
                category.categoryOrder = v?.categoryOrder
                category.segment = v?.segment
                category.save(failOnError: true)
            }
        }
        prepareBusinessList().each { k, v ->
            Category category = Category.findBySlugAndSegment(k, v?.segment)
            if (!category) {
                Site site = Site.findByCode(v?.site)
                Language language = Language.findById(v?.language)

                category = new Category()
                category.site = site
                category.language = language
                category.name = v?.name
                category.slug = k
                category.categoryType = v?.categoryType
                category.status = v?.status
                category.categoryOrder = v?.categoryOrder
                category.segment = v?.segment
                category.save(failOnError: true)
            }
        }
    }

    private static def prepareConsumerList() {
        return [
                "product": [
                        site: "asia",
                        language: "en",
                        name: "Product",
                        categoryType: CategoryType.ARTICLE,
                        categoryOrder: 0,
                        status: Status.ACTIVE,
                        segment: Segment.CONSUMER
                ],
                "general": [
                        site: "asia",
                        language: "en",
                        name: "General",
                        categoryType: CategoryType.ARTICLE,
                        categoryOrder: 1,
                        status: Status.ACTIVE,
                        segment: Segment.CONSUMER
                ],
                "tips-and-tutorial": [
                        site: "asia",
                        language: "en",
                        name: "Tips & tutorial",
                        categoryType: CategoryType.ARTICLE,
                        categoryOrder: 2,
                        status: Status.ACTIVE,
                        segment: Segment.CONSUMER
                ],
                "event": [
                        site: "asia",
                        language: "en",
                        name: "Event",
                        categoryType: CategoryType.EVENT,
                        categoryOrder: 0,
                        status: Status.ACTIVE,
                        segment: Segment.CONSUMER
                ],
                "travel": [
                        site: "asia",
                        language: "en",
                        name: "Travel",
                        categoryType: CategoryType.EVENT,
                        categoryOrder: 0,
                        status: Status.ACTIVE,
                        segment: Segment.CONSUMER
                ],
                "workshop": [
                        site: "asia",
                        language: "en",
                        name: "Workshop",
                        categoryType: CategoryType.EVENT,
                        categoryOrder: 0,
                        status: Status.ACTIVE,
                        segment: Segment.CONSUMER
                ],
                "course": [
                        site: "asia",
                        language: "en",
                        name: "Course",
                        categoryType: CategoryType.COURSE,
                        categoryOrder: 0,
                        status: Status.ACTIVE,
                        segment: Segment.CONSUMER
                ]
        ]
    }

    private static def prepareBusinessList() {
        return [
                "product": [
                        site: "asia",
                        language: "en",
                        name: "Product",
                        categoryType: CategoryType.ARTICLE,
                        categoryOrder: 0,
                        status: Status.ACTIVE,
                        segment: Segment.BUSINESS
                ],
                "general": [
                        site: "asia",
                        language: "en",
                        name: "General",
                        categoryType: CategoryType.ARTICLE,
                        categoryOrder: 1,
                        status: Status.ACTIVE,
                        segment: Segment.BUSINESS
                ],
                "tips-and-tutorial": [
                        site: "asia",
                        language: "en",
                        name: "Tips & tutorial",
                        categoryType: CategoryType.ARTICLE,
                        categoryOrder: 2,
                        status: Status.ACTIVE,
                        segment: Segment.BUSINESS
                ],
        ]
    }
}
