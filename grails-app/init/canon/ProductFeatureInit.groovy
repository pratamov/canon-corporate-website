package canon

import canon.enums.Status

class ProductFeatureInit {

    static init() {
        prepareData().each {k, v ->
            Product product = Product.findBySlug(k)
            v.each {String iconTitle ->
                Integer featureOrder = 0
                Feature feature = Feature.findByIconTitle(iconTitle)

                if(feature) {
                    ProductFeature pf = ProductFeature.findByProductAndFeature(product, feature)
                    if(!pf){
                        new ProductFeature(product: product, feature: feature, status: Status.ACTIVE, featureOrder: featureOrder++).save(failOnError: true)
                    }
                }
            }
        }
    }

    private static def prepareData() {
        return [
                "eos-200d-colour": [
                        "24.2 MP",
                        "60p / 50p",
                        "Wi-Fi / NFC"
                ]
        ]
    }
}
