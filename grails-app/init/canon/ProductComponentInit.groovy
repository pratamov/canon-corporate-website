package canon

import canon.enums.ComponentType
import canon.enums.RelationType

class ProductComponentInit {

    static init() {
        prepareData().each { k, v ->
            Product product = Product.findBySlug(k)
            v.each { c ->
                if (c.type?.name().startsWith("PROD_")) {
                    c.content.id = product.id
                }
                new ProductComponent(product: product, component: new canon.model.Component(c.type, c.content, c.order)).save(failOnError: true)
            }
        }
    }

    private static def prepareData() {
        // Media for EOS 200D Gallery
        Media media2 = Media.findByFileName("eos-200d-colour-gallery2")
        Media media4 = Media.findByFileName("eos-200d-colour-gallery4")
        Media media5 = Media.findByFileName("eos-200d-colour-gallery5")
        Media media6 = Media.findByFileName("eos-200d-colour-gallery6")
        Media media7 = Media.findByFileName("eos-200d-colour-gallery7")
        Media media8 = Media.findByFileName("eos-200d-colour-gallery8")
        Media media9 = Media.findByFileName("eos-200d-colour-gallery9")



        return [
                "eos-1300d-kit-ef-s18-55-is-ii": [
                        [type: ComponentType.PROD_BANNER,
                         content: [desc: "Step up your joy in photography", bgUrl: "/assets/bg/img04.jpg"],
                         order: 0],
                        [type: ComponentType.PROD_DESC,
                         content: [:],
                         order: 1]
                ],
                "eos-200d-colour": [
                        [
                                type: ComponentType.PROD_BANNER,
                                content: [desc: "Compact, simple and versatile – a camera that’s connected to your life", bgUrl: "/assets/bg/img10.jpg"],
                                order: 0
                        ],
                        [
                                type: ComponentType.PROD_PROMOTION,
                                content: [
                                        url: "http://www.canon-asia.com/personal/products/interchangeable-lens-camera/dslr-eos/eos-200d-colour?languageCode=EN"
                                ],
                                order: 1
                        ],
                        [
                                type: ComponentType.PROD_DESC,
                                content: [
                                        newSubText : "Embrace creative expression with the compact and sleek DSLR",
                                        newDesc : "The light and compact EOS 200D fits snugly into your everyday bag. This stylish DSLR comes in 2 colour options – white and silver. Equipped with cutting technologies packed into its compact body, capture beautiful and impressionable images and share them seamlessly on your social media feeds via the in-built Wi-Fi capability."
                                ],
                                order: 2
                        ],
                        [
                                type: ComponentType.PROD_IMAGE,
                                content: [:],
                                order: 3
                        ],
                        [
                                type: ComponentType.PROD_AWARD,
                                content: [:],
                                order: 4
                        ],
                        [
                                type: ComponentType.PROD_FEATURES,
                                content: [
                                        id: 2,
                                        features: [
                                                [id: 4],
                                                [id: 5],
                                                [id: 6]
                                        ]
                                ],
                                order: 5
                        ],
                        [
                                type: ComponentType.SLIDER,
                                content: [
                                        title : "Taken with the EOS 200D Kit (EF-S18-55 IS STM)",
                                        images : [
                                                [mediaId: media4.id, mediaUrl: media4.url],
                                                [mediaId: media5.id, mediaUrl: media5.url],
                                                [mediaId: media6.id, mediaUrl: media6.url],
                                                [mediaId: media2.id, mediaUrl: media2.url],
                                                [mediaId: media7.id, mediaUrl: media7.url],
                                                [mediaId: media8.id, mediaUrl: media8.url],
                                                [mediaId: media9.id, mediaUrl: media9.url],
                                        ]
                                ],
                                order: 6
                        ],
                        [
                                type: ComponentType.INFORMATION_CARD,
                                content: [
                                        sectionTitle: "More Information",
                                        cardTitle: "Enjoy your EOS!",
                                        mediaUrl: "http://media.canon-asia.com/local/asia/live/yourcanon/EOS_tips_Online-DSLR-tutorial.jpg",
                                        desc: "Are you new to DSLR photography? See how DSLRs can be fun and easy to use. Take photography to a higher plane, and experience the true joy of photography:<br/><ul><li>Part 1: Fun and easy digital SLRs (basic introduction)</li><li>Part 2: Using camera features for better photos (practical applications)</li><li>Part 3: Choosing lenses for different kinds of photos (interchangeable lens)</li><li>Part 4: Printing photos</li><li>Terminology</li></ul>",
                                        url: "http://web.canon.jp/imaging/enjoydslr/",
                                        buttonLabel: "Discover the real joy of photography"
                                ],
                                order: 7
                        ],
                        [
                                type: ComponentType.PROD_RELATION,
                                content: [
                                        id: 2,
                                        relationType: RelationType.PRODUCT
                                ],
                                order: 8
                        ],
                        [
                                type: ComponentType.PROD_RELATION,
                                content: [
                                        id: 2,
                                        relationType: RelationType.CONSUMABLE
                                ],
                                order: 9
                        ],
                        [
                                type: ComponentType.PROD_RELATION,
                                content: [
                                        id: 2,
                                        relationType: RelationType.ACCESSORIES
                                ],
                                order: 10
                        ],
                        [
                                type: ComponentType.PROD_RELATED_READS,
                                content: [
                                        id: 2
                                ],
                                order: 11
                        ]


                ],
                "eos-5d-mark-iii" : [
                        [       
                                type: ComponentType.PROD_IMAGE_MEDIUM,
				content: [
                                        subtitle: "Compact 3-in-1 Colour Multifunction Camera for the modern business",
                                        description: "The imageCLASS MF631Cn delivers high-quality prints in a compact design:",
                                        images:[
                                                [
                                                        mediaId: 101,
                                                        mediaUrl: "/assets/product/img7.png"
                                                ],
                                                [
                                                        mediaId: 102,
                                                        mediaUrl: "/assets/product/img7.png"
                                                ],
                                                [
                                                        mediaId: 103,
                                                        mediaUrl: "/assets/product/img7.png"
                                                ]
                                        ]
                         ],
                         order: 0]
                ]
        ]
    }
}
