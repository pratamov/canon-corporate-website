package canon

import canon.enums.RelationType

class ProductRelationInit {
    static init() {
        prepareData()?.each {k, v ->
            Product product = Product.findBySlug(k)
            if (product) {
                v.each {Map map ->
                    map.get("relations")?.each {String relSlug ->
                        Product relation = Product.findBySlug(relSlug)
                        if(relation) {
                            new ProductRelation(product: product, relation: relation, relationType: map.get("type")).save(failOnError:true)
                        }
                    }
                }
            }
        }
    }

    private static def prepareData(){
        return [
                "eos-200d-colour":[
                        [
                                type: RelationType.PRODUCT,
                                relations: [
                                        "eos-1300d-kit-ef-s18-55-is-ii",
                                        "eos-750d",
                                        "eos-5d-mark-iii"
                                ]
                        ],
                        [
                                type: RelationType.CONSUMABLE,
                                relations: [
                                        "pixma-ts9170",
                                        "pixma-ts8170",
                                        "pixma-tr8570",
                                        "imageclass-mf232w"
                                ]
                        ],
                        [
                                type: RelationType.ACCESSORIES,
                                relations: [
                                        "xa11",
                                        "xa15",
                                        "xa30",
                                        "eos-c200"
                                ]
                        ]
                ]
        ]
    }
}
