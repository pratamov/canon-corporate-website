package canon

import canon.enums.Status

class ProductPromotionInit {

    static init(){
        prepareData().each {k, v ->
            Product product = Product.findBySlug(k)
            if(product) {
                v.each {ProductPromotion pp ->
                    ProductPromotion prodPromotion = ProductPromotion.findByProductAndPromotionName(product, pp.promotionName)
                    if(!prodPromotion) {
                        pp.product = product
                        pp.save(failOnError: true)
                    }
                }
            }
        }
    }

    private static def prepareData() {
        ProductPromotion pp1 = new ProductPromotion(promotionName: "Promotion Example 1", promotionDesc: "<p>For a limited time only, from 1 September to 31 December 2017, you will receive a <strong>FREE Miniature USB Flash Drive</strong> with every purchase of an EOS 200D Kit. Terms & conditions apply.</p>",status: Status.PUBLISHED)

        return [
                "eos-200d-colour": [
                        pp1
                ]
        ]
    }
}
