package canon

import canon.enums.SpecificationType
import grails.util.Holders

/**
 * Created by fitra on 06/12/17.
 */
class TopicInit {
    static init() {

        //Preparation
        //String awsBaseUrl = Holders.config.getProperty("amazonaws.s3.baseUrl")
        //User userBootstrap = User.findByUsername("bootstrap")

        //Init Topic
        def topics = [
                'Scanners specifications',
                'Fax machines specifications',
                'Personal camcorders specifications',
                'Compact cameras specifications',
                'Projectors specifications',
                'Printing specifications',
                'Inkjet Printers specifications'
        ]

        def topicList = []

        topics.each{ topicName ->
            Topic topic = Topic.findByName(topicName)
            if (!topic) {
                topic = new Topic(name:topicName,description: topicName).save(failOnError: true)
                //new Site(country: countrySg, code: 'singapore', name: 'Singapore', timeZone: 'UTC+08:00').save(failOnError: true)
            }
            topicList.add(topic)
        }

        //Init Group
        def groups = [
                ['Copy','Fax','General Specs','Ink Cartridges',
                 'Network','Operating System','Paper','Print','Scan','Technology','Detector and Adjustment'],
                [],
                [],
                [],
                [],
                [],
                ['Copy','Fax','General Specs','Ink Cartridges',
                 'Network','Operating System','Paper','Print','Scan','Technology','Detector and Adjustment']
        ]

        def specs = [:]
        specs.put('Inkjet Printers specifications-Copy',
                ['Compatible Media',
                  'Copy Speed (Colour, sFCOT)',
                  'Copy Speed *9 Based on ISO / IEC 29183',
                  'Density Adjustment',
                  'Fit-to-page',
                  'Image quality',
                  'Maximum Document Size',
                  'Multiple Copy'
                ])

        specs.put('Inkjet Printers specifications-Fax',
                ['Applicable Line',
                 'Automatic Dialing',
                 'Black / Colour',
                 'Compression',
                 'Destination Number',
                 'ECM (Error Correction Mode)',
                 'Fax  Reception Memory',
                 'Fax Resolution',
                 'Fax Transmission Speed',
                 'Gradation',
                 'Modem Speed',
                 'PC FAX',
                 'Print Size ',
                 'Scanning Width',
                 'Transmission / Reception Memory *9',
                 'Transmission Speed *8',
                 'Type'
                ])

        specs.put('Inkjet Printers specifications-General Specs',
                ['Acoustic Noise (PC Print)',
                 'ADF',
                 'Auto Document Feeder',
                 'Camera Direct Spec.',
                 'Card Direct Spec.',
                 'Compatible Operating System',
                 'Dimensions (W x D x H)',
                 'Duty Cycle',
                 'Environment',
                 'Format',
                 'Functions',
                 'Interface',
                 'Languages',
                 'LCD Display',
                 'Memory',
                 'Monthly Duty Cycle',
                 'NFC/BLE',
                 'Operating Environment *12',
                 'Operation Panel',
                 'Other Features',
                 'PictBridge',
                 'Power',
                 'Power Consumption',
                 'Quick Start',
                 'Quiet mode',
                 'Recommended Environment *13',
                 'Storage environment',
                 'Typical Electricity Consumption (TEC)*14',
                 'USB Flash Memory',
                 'Weight'
                ])

        specs.put('Inkjet Printers specifications-Ink Cartridges',
                ['Cartridge Type',
                 'Compatible Ink Consumables (Optional)',
                 'Compatible Ink Consumables (Standard)',
                 'Number of Inks',
                 'Print Head / Ink'
                ])

        specs.put('Inkjet Printers specifications-Network',
                ['Cartridge Type',
                 'Compatible Ink Consumables (Optional)',
                 'Compatible Ink Consumables (Standard)',
                 'Number of Inks',
                 'Print Head / Ink'
                ])

        specs.put('Inkjet Printers specifications-Operating System',
                ['Macintosh',
                 'Windows'
                ])

        specs.put('Inkjet Printers specifications-Paper',
                ['ADF Paper Capacity (A4)',
                 'Maximum Printable Paper Length',
                 'Media Feeding',
                 'Minimum Printable Paper Length',
                 'Paper Feeding System',
                 'Paper Handling (Cassette1: Upper) (Maximum Number)',
                 'Paper Handling (Cassette2: Lower) (Maximum Number)',
                 'Paper Handling (Disc Tray)',
                 'Paper Handling (Manual Feed Slot) (Maximum Number) (Maximum Number = 1 sheet each)',
                 'Paper Handling Cassette (Maximum Number)',
                 'Paper Handling (Front Tray) (Maximum Number)',
                 'Paper Handling (Rear Tray)',
                 'Paper Input Capacity (Standard)',
                 'Paper Output',
                 'Paper Size',
                 'Paper Weight'
                ])

        specs.put('Inkjet Printers specifications-Print',
                ['ADF Paper Capacity (A4)',
                 'Maximum Printable Paper Length',
                 'Media Feeding',
                 'Minimum Printable Paper Length',
                 'Paper Feeding System',
                 'Paper Handling (Cassette1: Upper) (Maximum Number)',
                 'Paper Handling (Cassette2: Lower) (Maximum Number)',
                 'Paper Handling (Disc Tray)',
                 'Paper Handling (Manual Feed Slot) (Maximum Number) (Maximum Number = 1 sheet each)',
                 'Paper Handling Cassette (Maximum Number)',
                 'Paper Handling (Front Tray) (Maximum Number)',
                 'Paper Handling (Rear Tray)',
                 'Paper Input Capacity (Standard)',
                 'Paper Output',
                 'Paper Size',
                 'Paper Weight'
                ])

        specs.put('Inkjet Printers specifications-Scan',
                ['Duplex Scanning',
                 'Light source',
                 'Line Scanning Speed*5',
                 'Maximum Document Size ',
                 'Optical Resolution *4',
                 'Preview Speed*3',
                 'Scan Resolution',
                 'Scan Speed (Colour)',
                 'Scanning Speed*5 (Film)',
                 'Scanner Element',
                 'Scanner Type',
                 'Scanning Bit Depth (Input / Output)',
                 'Scanning Method',
                 'Scanning Speed *6 *15',
                 'Scanning Speed Reflective',
                 'Selectable Resolution *5'
                ])

        int index = 0
        groups.each{ t ->
            def gs = []
            def order = 0
            t.each{groupName ->
                Topic topic = topicList.get(index)
                Group group = Group.findByNameAndTopic(groupName,topic)
                if (!group) {
                    group = new Group(name:groupName,description: groupName, topic:topic, groupOrder: order++).save(failOnError: true)
                    //new Site(country: countrySg, code: 'singapore', name: 'Singapore', timeZone: 'UTC+08:00').save(failOnError: true)
                }
                gs.add(group)
            }
            Topic topic = topicList.get(index)
            topic.groups = gs
            index++
        }

        topicList.each{ topic ->
            topic.groups.each{ group ->
                def order = 0
                String keySpec = topic.name+'-'+group.name
                def listSpec = specs.get(keySpec)
                if(listSpec!=null) {
                    listSpec.each { specName ->
                        Specification spec = Specification.findByGroupAndName(group, specName)
                        if (spec == null) {
                            new Specification(name: specName,description: specName,unit: '-',group: group, specOrder: order++).save(failOnError: true)
                        }
                    }
                }
            }

        }

        prepareData().each {Topic t ->
            Topic topic = Topic.findByName(t.name)
            if(!topic) t.save(failOnError: true)
        }

        prepareChildrenSpecification().each {spec ->
            Specification parent = Specification.createCriteria().get {
                eq("specType", SpecificationType.SET)
                eq("name", spec.parent)
                group{
                    eq("name", spec.group)
                    topic{
                        eq("name", spec.topic)
                    }
                }
            }

            if (parent) {
                spec.children.each {String specName ->
                    Specification child = Specification.createCriteria().get {
                        eq("specType", SpecificationType.SINGLE)
                        eq("name", specName)
                        group{
                            eq("name", spec.group)
                            topic{
                                eq("name", spec.topic)
                            }
                        }
                    }

                    if (child) {
                        child.setParent(parent)
                        child.save(failOnError: true)
                    }
                }
            }
        }

    }

    private static List<Topic> prepareData() {
        List<Topic> topicList = new ArrayList<>()

        //Topic DSLRs Specification
        topicList.add(new Topic(name: "DSLRs Specifications", description: "DSLRs Specifications topic set")
                .addToGroups(new Group(name: "Type", description: "DSLRs Specifications - Type", groupOrder: 0)
                    .addToSpecifications(new Specification(name: "Type", description: "DSLRs Specifications - Type - Type", specOrder: 0))
                    .addToSpecifications(new Specification(name: "Recording Media", description: "DSLRs Specifications - Type - Recording Media", specOrder: 1))
                    .addToSpecifications(new Specification(name: "Sensor Size", description: "DSLRs Specifications - Type - Sensor Size", specOrder: 2))
                    .addToSpecifications(new Specification(name: "Image sensor size", description: "DSLRs Specifications - Type - Image sensor size", specOrder: 3))
                    .addToSpecifications(new Specification(name: "Compatible lenses", description: "DSLRs Specifications - Type - Compatible lenses", specOrder: 4))
                    .addToSpecifications(new Specification(name: "Lens mount", description: "DSLRs Specifications - Type - Lens mount", specOrder: 5)))
                .addToGroups(new Group(name: "Image Sensor", description: "DSLRs Specifications - Image Sensor", groupOrder: 1)
                    .addToSpecifications(new Specification(name: "Type", description: "DSLRs Specifications - Image Sensor - Type", specOrder: 0))
                    .addToSpecifications(new Specification(name: "Effective pixels", description: "DSLRs Specifications - Image Sensor - Effective pixels", specOrder: 1, important: Boolean.TRUE))
                    .addToSpecifications(new Specification(name: "Aspect ratio", description: "DSLRs Specifications - Image Sensor - Aspect ratio", specOrder: 2))
                    .addToSpecifications(new Specification(name: "Dust delete feature", description: "DSLRs Specifications - Image Sensor - Dust delete feature", specOrder: 3)))
                .addToGroups(new Group(name: "Recording System", description: "DSLRs Specifications - Recording System", groupOrder: 2)
                    .addToSpecifications(new Specification(name: "Recording format", description: "DSLRs Specifications - Recording System - Recording format", specOrder: 0))
                    .addToSpecifications(new Specification(name: "Image type", description: "DSLRs Specifications - Recording System - Image type", specOrder: 1))
                    .addToSpecifications(new Specification(name: "Recorded pixels", description: "DSLRs Specifications - Recording System - Recorded pixels", specOrder: 2, specType: SpecificationType.SET))
                    .addToSpecifications(new Specification(name: "L (Large)", description: "DSLRs Specifications - Recording System - Recorded pixels - L (Large)", specOrder: 0))
                    .addToSpecifications(new Specification(name: "M (Medium)", description: "DSLRs Specifications - Recording System - Recorded pixels - M (Medium)", specOrder: 1))
                    .addToSpecifications(new Specification(name: "S1 (Small 1)", description: "DSLRs Specifications - Recording System - Recorded pixels - S1 (Small 1)", specOrder: 2))
                    .addToSpecifications(new Specification(name: "S2 (Small 2)", description: "DSLRs Specifications - Recording System - Recorded pixels - S2 (Small 2)", specOrder: 3))
                    .addToSpecifications(new Specification(name: "RAW", description: "DSLRs Specifications - Recording System - Recorded pixels - RAW", specOrder: 4)))
                .addToGroups(new Group(name: "Image Processing During Shooting", description: "DSLRs Specifications - Image Processing During Shooting", groupOrder: 3)
                    .addToSpecifications(new Specification(name: "Picture Style", description: "DSLRs Specifications - Image Processing During Shooting - Picture Style", specOrder: 0))
                    .addToSpecifications(new Specification(name: "White balance", description: "DSLRs Specifications - Image Processing During Shooting - White balance", specOrder: 1))
                    .addToSpecifications(new Specification(name: "Noise reduction", description: "DSLRs Specifications - Image Processing During Shooting - Noise reduction", specOrder: 2))
                    .addToSpecifications(new Specification(name: "Automatic image brightness correction", description: "DSLRs Specifications - Image Processing During Shooting - Automatic image brightness correction", specOrder: 3))
                    .addToSpecifications(new Specification(name: "Highlight tone priority", description: "DSLRs Specifications - Image Processing During Shooting - Highlight tone priority", specOrder: 4))
                    .addToSpecifications(new Specification(name: "Lens aberration correction", description: "DSLRs Specifications - Image Processing During Shooting - Lens aberration correction", specOrder: 5)))
                .addToGroups(new Group(name: "Viewfinder", description: "DSLRs Specifications - Viewfinder", groupOrder: 4)
                    .addToSpecifications(new Specification(name: "Type", description: "DSLRs Specifications - Viewfinder - Type", specOrder: 0))
                    .addToSpecifications(new Specification(name: "Coverage", description: "DSLRs Specifications - Viewfinder - Coverage", specOrder: 1))
                    .addToSpecifications(new Specification(name: "Magnification", description: "DSLRs Specifications - Viewfinder - Magnification", specOrder: 2))
                    .addToSpecifications(new Specification(name: "Eye point", description: "DSLRs Specifications - Viewfinder - Eye point", specOrder: 3))
                    .addToSpecifications(new Specification(name: "Built-in dioptric adjustment", description: "DSLRs Specifications - Viewfinder - Built-in dioptric adjustment", specOrder: 4))
                    .addToSpecifications(new Specification(name: "Focusing screen", description: "DSLRs Specifications - Viewfinder - Focusing screen", specOrder: 5))
                    .addToSpecifications(new Specification(name: "Mirror", description: "DSLRs Specifications - Viewfinder - Mirror", specOrder: 6))
                    .addToSpecifications(new Specification(name: "Depth-of-field preview", description: "DSLRs Specifications - Viewfinder - Depth-of-field preview", specOrder: 7)))
                .addToGroups(new Group(name: "Autofocus", description: "DSLRs Specifications - Autofocus", groupOrder: 5)
                    .addToSpecifications(new Specification(name: "Type", description: "DSLRs Specifications - Autofocus - Type", specOrder: 0))
                    .addToSpecifications(new Specification(name: "AF points", description: "DSLRs Specifications - Autofocus - AF points", specOrder: 1, important: Boolean.TRUE))
                    .addToSpecifications(new Specification(name: "Focusing brightness range", description: "DSLRs Specifications - Autofocus - Focusing brightness range", specOrder: 2))
                    .addToSpecifications(new Specification(name: "AF operation", description: "DSLRs Specifications - Autofocus - AF operation", specOrder: 3))
                    .addToSpecifications(new Specification(name: "AF-assist beam", description: "DSLRs Specifications - Autofocus - AF-assist beam", specOrder: 4)))
                .addToGroups(new Group(name: "Exposure Control", description: "DSLRs Specifications - Exposure Control", groupOrder: 6)
                    .addToSpecifications(new Specification(name: "Metering modes", description: "DSLRs Specifications - Exposure Control - Type", specOrder: 0))
                    .addToSpecifications(new Specification(name: "Metering brightness range", description: "DSLRs Specifications - Exposure Control - Metering brightness range", specOrder: 1))
                    .addToSpecifications(new Specification(name: "Shooting mode", description: "DSLRs Specifications - Exposure Control - Shooting mode", specOrder: 2, specType: SpecificationType.SET))
                    .addToSpecifications(new Specification(name: "Basic Zone modes", description: "DSLRs Specifications - Exposure Control - Shooting mode - Basic Zone modes", specOrder: 0))
                    .addToSpecifications(new Specification(name: "Creative Zone modes", description: "DSLRs Specifications - Exposure Control - Shooting mode - Creative Zone modes", specOrder: 1))
                    .addToSpecifications(new Specification(name: "ISO speed (Recommended exposure index)", description: "DSLRs Specifications - Exposure Control - ISO speed", specOrder: 3, important: Boolean.TRUE))
                    .addToSpecifications(new Specification(name: "Exposure compensation", description: "DSLRs Specifications - Exposure Control - Exposure compensation", specOrder: 4))
                    .addToSpecifications(new Specification(name: "AE lock", description: "DSLRs Specifications - Exposure Control - AE lock", specOrder: 5)))
                .addToGroups(new Group(name: "Shutter", description: "DSLRs Specifications - Shutter", groupOrder: 7)
                    .addToSpecifications(new Specification(name: "Type", description: "DSLRs Specifications - Shutter - Type", specOrder: 0))
                    .addToSpecifications(new Specification(name: "Shutter Speed", description: "DSLRs Specifications - Shutter - Shutter Speed", specOrder: 1)))
                .addToGroups(new Group(name: "Flash", description: "DSLRs Specifications - Flash", groupOrder: 8)
                    .addToSpecifications(new Specification(name: "Built-in flash", description: "DSLRs Specifications - Flash - Built-in flash", specOrder: 0, specType: SpecificationType.SET))
                    .addToSpecifications(new Specification(name: "Retractable, auto pop-up flash\n Guide No.", description: "DSLRs Specifications - Flash - Built-in flash - Retractable, auto pop-up flash", specOrder: 0))
                    .addToSpecifications(new Specification(name: "Flash coverage", description: "DSLRs Specifications - Flash - Built-in flash - Flash coverage", specOrder: 1))
                    .addToSpecifications(new Specification(name: "Recharge time", description: "DSLRs Specifications - Flash - Built-in flash - Recharge time", specOrder: 2))
                    .addToSpecifications(new Specification(name: "External flash", description: "DSLRs Specifications - Flash - External flash", specOrder: 1))
                    .addToSpecifications(new Specification(name: "Flash metering", description: "DSLRs Specifications - Flash - Flash metering", specOrder: 2))
                    .addToSpecifications(new Specification(name: "Flash exposure compensation", description: "DSLRs Specifications - Flash - Flash exposure compensation", specOrder: 3))
                    .addToSpecifications(new Specification(name: "FE lock", description: "DSLRs Specifications - Flash - FE lock", specOrder: 4))
                    .addToSpecifications(new Specification(name: "PC terminal", description: "DSLRs Specifications - Flash - PC terminal", specOrder: 5)))
                .addToGroups(new Group(name: "Drive System", description: "DSLRs Specifications - Drive System", groupOrder: 9)
                    .addToSpecifications(new Specification(name: "Drive modes", description: "DSLRs Specifications - Drive System - Drive modes", specOrder: 0))
                    .addToSpecifications(new Specification(name: "Continuous shooting speed", description: "DSLRs Specifications - Drive System - Continuous shooting speed", specOrder: 1))
                    .addToSpecifications(new Specification(name: "Max. burst (Approx.)", description: "DSLRs Specifications - Drive System - Max. burst", specOrder: 2, specType: SpecificationType.SET))
                    .addToSpecifications(new Specification(name: "JPEG Large / Fine", description: "DSLRs Specifications - Drive System - External flash - JPEG Large / Fine", specOrder: 0))
                    .addToSpecifications(new Specification(name: "RAW", description: "DSLRs Specifications - Drive System - External flash - RAW", specOrder: 1))
                    .addToSpecifications(new Specification(name: "RAW+JPEG Large / Fine", description: "DSLRs Specifications - Drive System - External flash - RAW+JPEG Large / Fine", specOrder: 2)))
                .addToGroups(new Group(name: "Live View Shooting", description: "DSLRs Specifications - Live View Shooting", groupOrder: 10)
                    .addToSpecifications(new Specification(name: "Focus method", description: "DSLRs Specifications - Live View Shooting - Focus method", specOrder: 0))
                    .addToSpecifications(new Specification(name: "AF method", description: "DSLRs Specifications - Live View Shooting - AF method", specOrder: 1))
                    .addToSpecifications(new Specification(name: "AF operation", description: "DSLRs Specifications - Live View Shooting - AF operation", specOrder: 2))
                    .addToSpecifications(new Specification(name: "Focusing brightness range", description: "DSLRs Specifications - Live View Shooting - Focusing brightness range", specOrder: 3))
                    .addToSpecifications(new Specification(name: "Touch shutter", description: "DSLRs Specifications - Live View Shooting - Touch shutter", specOrder: 4))
                    .addToSpecifications(new Specification(name: "Metering mode", description: "DSLRs Specifications - Live View Shooting - Metering mode", specOrder: 5))
                    .addToSpecifications(new Specification(name: "Metering brightness range", description: "DSLRs Specifications - Live View Shooting - Metering brightness range", specOrder: 6))
                    .addToSpecifications(new Specification(name: "Exposure compensation", description: "DSLRs Specifications - Live View Shooting - Exposure compensation", specOrder: 7))
                    .addToSpecifications(new Specification(name: "Creative Filters", description: "DSLRs Specifications - Live View Shooting - Creative Filters", specOrder: 8))
                    .addToSpecifications(new Specification(name: "Grid display", description: "DSLRs Specifications - Live View Shooting - Grid display", specOrder: 9)))
                .addToGroups(new Group(name: "Movie Shooting", description: "DSLRs Specifications - Movie Shooting", groupOrder: 11)
                    .addToSpecifications(new Specification(name: "Recording format", description: "DSLRs Specifications - Movie Shooting - Recording format", specOrder: 0))
                    .addToSpecifications(new Specification(name: "Movie", description: "DSLRs Specifications - Movie Shooting - Movie", specOrder: 1))
                    .addToSpecifications(new Specification(name: "Audio", description: "DSLRs Specifications - Movie Shooting - Audio", specOrder: 2))
                    .addToSpecifications(new Specification(name: "Recording size and frame rate", description: "DSLRs Specifications - Movie Shooting - Recording size and frame rate", specOrder: 3, specType: SpecificationType.SET))
                    .addToSpecifications(new Specification(name: "1920 x 1080 (Full HD)", description: "DSLRs Specifications - Movie Shooting - Recording size and frame rate - 1920 x 1080 (Full HD)", specOrder: 0))
                    .addToSpecifications(new Specification(name: "1280 x 720 (HD)", description: "DSLRs Specifications - Movie Shooting - Recording size and frame rate - 1280 x 720 (HD)", specOrder: 1))
                    .addToSpecifications(new Specification(name: "640 x 480 (VGA)", description: "DSLRs Specifications - Movie Shooting - Recording size and frame rate - 640 x 480 (VGA)", specOrder: 2))
                    .addToSpecifications(new Specification(name: "Compression method", description: "DSLRs Specifications - Movie Shooting - Compression method", specOrder: 4))
                    .addToSpecifications(new Specification(name: "Bit rate", description: "DSLRs Specifications - Movie Shooting - Bit rate", specOrder: 5, specType: SpecificationType.SET))
                    .addToSpecifications(new Specification(name: "Full HD (59.94p / 50.00p) / IPB (Standard)", description: "DSLRs Specifications - Movie Shooting - Bit rate - Full HD (59.94p / 50.00p) / IPB (Standard)", specOrder: 0))
                    .addToSpecifications(new Specification(name: "Full HD (29.97p / 25.00p / 23.98p) / IPB (Standard)", description: "DSLRs Specifications - Movie Shooting - Bit rate - Full HD (29.97p / 25.00p / 23.98p) / IPB (Standard)", specOrder: 1))
                    .addToSpecifications(new Specification(name: "Full HD (29.97p / 25.00p) / IPB (Light)", description: "DSLRs Specifications - Movie Shooting - Bit rate - Full HD (29.97p / 25.00p) / IPB (Light)", specOrder: 2))
                    .addToSpecifications(new Specification(name: "HD (59.94p / 50.00p) / IPB (Standard)", description: "DSLRs Specifications - Movie Shooting - Bit rate - HD (59.94p / 50.00p) / IPB (Standard)", specOrder: 3))
                    .addToSpecifications(new Specification(name: "HD (29.97p / 25.00p) / IPB (Light)", description: "DSLRs Specifications - Movie Shooting - Bit rate - HD (29.97p / 25.00p) / IPB (Light)", specOrder: 4))
                    .addToSpecifications(new Specification(name: "VGA (29.97p / 25.00p) (Standard)", description: "DSLRs Specifications - Movie Shooting - Bit rate - VGA (29.97p / 25.00p) (Standard)", specOrder: 5))
                    .addToSpecifications(new Specification(name: "VGA (29.97p / 25.00p) (Light)", description: "DSLRs Specifications - Movie Shooting - Bit rate - VGA (29.97p / 25.00p) (Light)", specOrder: 6))
                    .addToSpecifications(new Specification(name: "HDR movie", description: "DSLRs Specifications - Movie Shooting - Bit rate - HDR movie", specOrder: 7))
                    .addToSpecifications(new Specification(name: "Time lapse movie", description: "DSLRs Specifications - Movie Shooting - Bit rate - Time-lapse movie", specOrder: 8))
                    .addToSpecifications(new Specification(name: "Focus system", description: "DSLRs Specifications - Movie Shooting - Focus system", specOrder: 6))
                    .addToSpecifications(new Specification(name: "AF method", description: "DSLRs Specifications - Movie Shooting - AF method", specOrder: 7))
                    .addToSpecifications(new Specification(name: "Movie Servo AF", description: "DSLRs Specifications - Movie Shooting - Movie Servo AF", specOrder: 8))
                    .addToSpecifications(new Specification(name: "Digital zoom", description: "DSLRs Specifications - Movie Shooting - Digital zoom", specOrder: 9))
                    .addToSpecifications(new Specification(name: "Focusing brightness range", description: "DSLRs Specifications - Movie Shooting - Focusing brightness range", specOrder: 10))
                    .addToSpecifications(new Specification(name: "Metering mode", description: "DSLRs Specifications - Movie Shooting - Metering mode", specOrder: 11))
                    .addToSpecifications(new Specification(name: "Metering brightness range", description: "DSLRs Specifications - Movie Shooting - Metering brightness range", specOrder: 12))
                    .addToSpecifications(new Specification(name: "Exposure control", description: "DSLRs Specifications - Movie Shooting - Exposure control", specOrder: 13))
                    .addToSpecifications(new Specification(name: "Exposure compensation", description: "DSLRs Specifications - Movie Shooting - Exposure compensation", specOrder: 14))
                    .addToSpecifications(new Specification(name: "ISO speed (Recommended exposure index)", description: "DSLRs Specifications - Movie Shooting - ISO speed", specOrder: 15, specType: SpecificationType.SET))
                    .addToSpecifications(new Specification(name: "For autoexposure shooting", description: "DSLRs Specifications - Movie Shooting - ISO speed - For autoexposure shooting", specOrder: 0))
                    .addToSpecifications(new Specification(name: "For manual exposure", description: "DSLRs Specifications - Movie Shooting - ISO speed - For manual exposure", specOrder: 1))
                    .addToSpecifications(new Specification(name: "ISO speed settings", description: "DSLRs Specifications - Movie Shooting - ISO speed settings", specOrder: 16))
                    .addToSpecifications(new Specification(name: "HDR movie shooting", description: "DSLRs Specifications - Movie Shooting - HDR movie shooting", specOrder: 17))
                    .addToSpecifications(new Specification(name: "Video snapshots", description: "DSLRs Specifications - Movie Shooting - Video snapshots", specOrder: 18))
                    .addToSpecifications(new Specification(name: "Creative filters for movies", description: "DSLRs Specifications - Movie Shooting - Creative filters for movies", specOrder: 19))
                    .addToSpecifications(new Specification(name: "Sound recording", description: "DSLRs Specifications - Movie Shooting - Sound recording", specOrder: 20))
                    .addToSpecifications(new Specification(name: "Grid display", description: "DSLRs Specifications - Movie Shooting - Grid display", specOrder: 21))
                    .addToSpecifications(new Specification(name: "Time-lapse movie", description: "DSLRs Specifications - Movie Shooting - Time-lapse movie", specOrder: 22)))
                .addToGroups(new Group(name: "LCD Monitor", description: "DSLRs Specifications - LCD Monitor", groupOrder: 12)
                    .addToSpecifications(new Specification(name: "Type", description: "DSLRs Specifications - LCD Monitor - Type", specOrder: 0))
                    .addToSpecifications(new Specification(name: "Monitor size and dots", description: "DSLRs Specifications - LCD Monitor - Monitor size and dots", specOrder: 1))
                    .addToSpecifications(new Specification(name: "Brightness adjustment", description: "DSLRs Specifications - LCD Monitor - Brightness adjustment", specOrder: 2))
                    .addToSpecifications(new Specification(name: "Interface languages", description: "DSLRs Specifications - LCD Monitor - Interface languages", specOrder: 3))
                    .addToSpecifications(new Specification(name: "Touch screen technology", description: "DSLRs Specifications - LCD Monitor - Touch screen technology", specOrder: 4)))
                .addToGroups(new Group(name: "Playback", description: "DSLRs Specifications - Playback", groupOrder: 13)
                    .addToSpecifications(new Specification(name: "Image display format", description: "DSLRs Specifications - Playback - Image display format", specOrder: 0))
                    .addToSpecifications(new Specification(name: "Zoom magnification", description: "DSLRs Specifications - Playback - Zoom magnification", specOrder: 1))
                    .addToSpecifications(new Specification(name: "Highlight alert", description: "DSLRs Specifications - Playback - Highlight alert", specOrder: 2))
                    .addToSpecifications(new Specification(name: "AF point display", description: "DSLRs Specifications - Playback - AF point display", specOrder: 3))
                    .addToSpecifications(new Specification(name: "Image search", description: "DSLRs Specifications - Playback - Image search", specOrder: 4))
                    .addToSpecifications(new Specification(name: "Image browsing methods", description: "DSLRs Specifications - Playback - Image browsing methods", specOrder: 5))
                    .addToSpecifications(new Specification(name: "Image rotation", description: "DSLRs Specifications - Playback - Image rotation", specOrder: 6))
                    .addToSpecifications(new Specification(name: "Rating", description: "DSLRs Specifications - Playback - Rating", specOrder: 7))
                    .addToSpecifications(new Specification(name: "Movie playback", description: "DSLRs Specifications - Playback - Movie playback", specOrder: 8))
                    .addToSpecifications(new Specification(name: "Image protect", description: "DSLRs Specifications - Playback - Image protect", specOrder: 9))
                    .addToSpecifications(new Specification(name: "Slide show", description: "DSLRs Specifications - Playback - Slide show", specOrder: 10))
                    .addToSpecifications(new Specification(name: "Background music", description: "DSLRs Specifications - Playback - Background music", specOrder: 11)))
                .addToGroups(new Group(name: "Post-Processing of Images", description: "DSLRs Specifications - Post-Processing of Images", groupOrder: 14)
                    .addToSpecifications(new Specification(name: "Creative filters", description: "DSLRs Specifications - Post-Processing of Images - Creative filters", specOrder: 0))
                    .addToSpecifications(new Specification(name: "Resize", description: "DSLRs Specifications - Post-Processing of Images - Resize", specOrder: 1))
                    .addToSpecifications(new Specification(name: "Cropping", description: "DSLRs Specifications - Post-Processing of Images - Cropping", specOrder: 2)))
                .addToGroups(new Group(name: "Print Ordering", description: "DSLRs Specifications - Print Ordering", groupOrder: 15)
                    .addToSpecifications(new Specification(name: "DPOF", description: "DSLRs Specifications - Print Ordering - DPOF", specOrder: 0)))
                .addToGroups(new Group(name: "Wi-Fi", description: "DSLRs Specifications - Wi-Fi", groupOrder: 16)
                    .addToSpecifications(new Specification(name: "Standards compliance", description: "DSLRs Specifications - Wi-Fi - Standards compliance", specOrder: 0))
                    .addToSpecifications(new Specification(name: "Transmission method", description: "DSLRs Specifications - Wi-Fi - Transmission method", specOrder: 1))
                    .addToSpecifications(new Specification(name: "Transmission range", description: "DSLRs Specifications - Wi-Fi - Transmission range", specOrder: 2))
                    .addToSpecifications(new Specification(name: "Transmission frequency (central frequency)", description: "DSLRs Specifications - Wi-Fi - Transmission frequency (central frequency)", specOrder: 3))
                    .addToSpecifications(new Specification(name: "Connection method", description: "DSLRs Specifications - Wi-Fi - Connection method", specOrder: 4))
                    .addToSpecifications(new Specification(name: "Security", description: "DSLRs Specifications - Wi-Fi - Security", specOrder: 5))
                    .addToSpecifications(new Specification(name: "Communication with a smartphone", description: "DSLRs Specifications - Wi-Fi - Communication with a smartphone", specOrder: 6))
                    .addToSpecifications(new Specification(name: "Transfer images between cameras", description: "DSLRs Specifications - Wi-Fi - Transfer images between cameras", specOrder: 7))
                    .addToSpecifications(new Specification(name: "Connect to Connect Station", description: "DSLRs Specifications - Wi-Fi - Connect to Connect Station", specOrder: 8))
                    .addToSpecifications(new Specification(name: "Remote operation using EOS Utility", description: "DSLRs Specifications - Wi-Fi - Remote operation using EOS Utility", specOrder: 9))
                    .addToSpecifications(new Specification(name: "Print from Wi-Fi printers", description: "DSLRs Specifications - Wi-Fi - Print from Wi-Fi printers", specOrder: 10))
                    .addToSpecifications(new Specification(name: "Send images to a Web service", description: "DSLRs Specifications - Wi-Fi - Send images to a Web service", specOrder: 11)))
                .addToGroups(new Group(name: "NFC", description: "DSLRs Specifications - NFC", groupOrder: 17)
                    .addToSpecifications(new Specification(name: "Standards compliance", description: "DSLRs Specifications - NFC - Standards compliance", specOrder: 0)))
                .addToGroups(new Group(name: "Bluetooth", description: "DSLRs Specifications - Bluetooth", groupOrder: 18)
                    .addToSpecifications(new Specification(name: "Standards compliance", description: "DSLRs Specifications - Bluetooth - Standards compliance", specOrder: 0))
                    .addToSpecifications(new Specification(name: "Transmission method", description: "DSLRs Specifications - Bluetooth - Transmission method", specOrder: 1)))
                .addToGroups(new Group(name: "Customization Features", description: "DSLRs Specifications - Customization Features", groupOrder: 19)
                    .addToSpecifications(new Specification(name: "Custom Functions", description: "DSLRs Specifications - Customization Features - Custom Functions", specOrder: 0))
                    .addToSpecifications(new Specification(name: "My Menu", description: "DSLRs Specifications - Customization Features - My Menu", specOrder: 1))
                    .addToSpecifications(new Specification(name: "Copyright information", description: "DSLRs Specifications - Customization Features - Copyright information", specOrder: 2)))
                .addToGroups(new Group(name: "Interface", description: "DSLRs Specifications - Interface", groupOrder: 20)
                    .addToSpecifications(new Specification(name: "DIGITAL terminal", description: "DSLRs Specifications - Interface - DIGITAL terminal", specOrder: 0))
                    .addToSpecifications(new Specification(name: "HDMI mini OUT terminal", description: "DSLRs Specifications - Interface - HDMI mini OUT terminal", specOrder: 1))
                    .addToSpecifications(new Specification(name: "External microphone IN terminal", description: "DSLRs Specifications - Interface - External microphone IN terminal", specOrder: 2))
                    .addToSpecifications(new Specification(name: "Remote control terminal", description: "DSLRs Specifications - Interface - Remote control terminal", specOrder: 3))
                    .addToSpecifications(new Specification(name: "Wireless remote control", description: "DSLRs Specifications - Interface - Wireless remote control", specOrder: 4))
                    .addToSpecifications(new Specification(name: "Eye-Fi card", description: "DSLRs Specifications - Interface - Eye-Fi card", specOrder: 5)))
                .addToGroups(new Group(name: "Power", description: "DSLRs Specifications - Power", groupOrder: 21)
                    .addToSpecifications(new Specification(name: "Battery", description: "DSLRs Specifications - Power - Battery", specOrder: 0))
                    .addToSpecifications(new Specification(name: "Number of possible shots (Based on CIPA testing standards)", description: "DSLRs Specifications - Power - Number of possible shots", specOrder: 1, specType: SpecificationType.SET))
                    .addToSpecifications(new Specification(name: "With viewfinder shooting", description: "DSLRs Specifications - Power - Number of possible shots - With viewfinder shooting", specOrder: 0))
                    .addToSpecifications(new Specification(name: "With Live View shooting", description: "DSLRs Specifications - Power - Number of possible shots - With Live View shooting", specOrder: 1))
                    .addToSpecifications(new Specification(name: "Movie shooting time", description: "DSLRs Specifications - Power - Number of possible shots - Movie shooting time", specOrder: 2)))
                .addToGroups(new Group(name: "Dimensions and Weight", description: "DSLRs Specifications - Dimensions and Weight", groupOrder: 22)
                    .addToSpecifications(new Specification(name: "Dimensions (W x H x D)", description: "DSLRs Specifications - Dimensions and Weight - Dimensions (W x H x D)", specOrder: 0))
                    .addToSpecifications(new Specification(name: "Weight", description: "DSLRs Specifications - Dimensions and Weight - Weight", specOrder: 1, specType: SpecificationType.SET))
                    .addToSpecifications(new Specification(name: "Black", description: "DSLRs Specifications - Dimensions and Weight - Weight - Black", specOrder: 0))
                    .addToSpecifications(new Specification(name: "Silver", description: "DSLRs Specifications - Dimensions and Weight - Weight - Silver", specOrder: 1))
                    .addToSpecifications(new Specification(name: "White", description: "DSLRs Specifications - Dimensions and Weight - Weight - White", specOrder: 2)))
                .addToGroups(new Group(name: "Operation Environment", description: "DSLRs Specifications - Operation Environment", groupOrder: 23)
                    .addToSpecifications(new Specification(name: "Working temperature range", description: "DSLRs Specifications - Operation Environment - Working temperature range", specOrder: 0))
                    .addToSpecifications(new Specification(name: "Working humidity", description: "DSLRs Specifications - Operation Environment - Working humidity", specOrder: 1)))
        )

        return topicList
    }

    private static def prepareChildrenSpecification(){
        return [
                [topic: "DSLRs Specifications", group: "Recording System", parent: "Recorded pixels", children: ["L (Large)", "M (Medium)", "S1 (Small 1)", "S2 (Small 2)", "RAW"]],
                [topic: "DSLRs Specifications", group: "Exposure Control", parent: "Shooting mode", children: ["Basic Zone modes", "Creative Zone modes"]],
                [topic: "DSLRs Specifications", group: "Flash", parent: "Built-in flash", children: ["Retractable, auto pop-up flash\n Guide No.", "Flash coverage", "Recharge time"]],
                [topic: "DSLRs Specifications", group: "Drive System", parent: "Max. burst (Approx.)", children: ["JPEG Large / Fine", "RAW", "RAW+JPEG Large / Fine"]],
                [topic: "DSLRs Specifications", group: "Movie Shooting", parent: "Recording size and frame rate", children: ["1920 x 1080 (Full HD)", "1280 x 720 (HD)", "640 x 480 (VGA)"]],
                [topic: "DSLRs Specifications", group: "Movie Shooting", parent: "Bit rate", children: ["Full HD (59.94p / 50.00p) / IPB (Standard)", "Full HD (29.97p / 25.00p / 23.98p) / IPB (Standard)", "Full HD (29.97p / 25.00p) / IPB (Light)", "HD (59.94p / 50.00p) / IPB (Standard)", "HD (29.97p / 25.00p) / IPB (Light)", "VGA (29.97p / 25.00p) (Standard)", "VGA (29.97p / 25.00p) (Light)", "HDR movie", "Time lapse movie"]],
                [topic: "DSLRs Specifications", group: "Movie Shooting", parent: "ISO speed (Recommended exposure index)", children: ["For autoexposure shooting", "For manual exposure"]],
                [topic: "DSLRs Specifications", group: "Power", parent: "Number of possible shots (Based on CIPA testing standards)", children: ["With viewfinder shooting", "With Live View shooting", "Movie shooting time"]],
                [topic: "DSLRs Specifications", group: "Dimensions and Weight", parent: "Weight", children: ["Black", "Silver", "White"]]
        ]
    }
}
