package canon

import canon.enums.ComponentType
import canon.enums.ContentTemplate
import canon.enums.ContentType
import canon.enums.MediaSource
import canon.enums.Segment
import canon.enums.Status
import canon.utils.SlugCodec
import grails.util.Holders
import groovy.json.JsonOutput

import java.text.SimpleDateFormat

class PageInit {

    static init() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss")

        Language en = Language.findById("en")
        //Site sg = Site.findByCode("singapore")
        Site asia = Site.findByCode("asia")
        Flow flow = Flow.findByName("Page Workflow")

        // Asia :: Homepage > Consumer
        Page home = new Page(site: asia, isHomepage: Boolean.TRUE, segment: Segment.CONSUMER, slug: "home", contentType: ContentType.PAGE, contentTemplate: ContentTemplate.PAGE, status: Status.PUBLISHED,
                language: en, publishedDate: sdf.parse("2017-12-01 12:00:00"), headline: "Homepage")
        home.addToPageDetails(new PageDetail(site: asia, status: Status.PUBLISHED, publishedDate: sdf.parse("2017-12-01 12:00:00"), language: en))
        home.addToPageComponents(new PageComponent(component: new canon.model.Component(ComponentType.BANNER, JsonOutput.toJson([id: Banner.findByTitle("Asia Consumer Home Slider").id]), 0), status: Status.PUBLISHED))
        home.addToPageComponents(new PageComponent(component: new canon.model.Component(ComponentType.FEATURED_PRODUCTS, JsonOutput.toJson([:]), 1), status: Status.PUBLISHED))
        home.addToPageComponents(new PageComponent(component: new canon.model.Component(ComponentType.FEATURED_ARTICLES, JsonOutput.toJson([title: "Articles", contentType: ContentType.ARTICLE.name()]), 2), status: Status.PUBLISHED))
        home.addToPageComponents(new PageComponent(component: new canon.model.Component(ComponentType.FEATURED_ARTICLES, JsonOutput.toJson([title: "Upcoming Events", contentType: ContentType.EVENT.name()]), 3), status: Status.PUBLISHED))
        home.save(failOnError: true)

        // Asia ::  Homepage > Business
        Page homeBusiness = new Page(site: asia, isHomepage: Boolean.TRUE, segment: Segment.BUSINESS, slug: "business-member-home", contentType: ContentType.PAGE, contentTemplate: ContentTemplate.PAGE, status: Status.PUBLISHED,
                language: en, publishedDate: sdf.parse("2017-12-12 12:00:00"), headline: "Homepage Business")
        homeBusiness.addToPageDetails(new PageDetail(site: asia, status: Status.PUBLISHED, publishedDate: sdf.parse("2017-12-02 12:00:00"), language: en))
        homeBusiness.addToPageComponents(new PageComponent(component: new canon.model.Component(ComponentType.BANNER, JsonOutput.toJson([id: Banner.findByTitle("Asia Business Home Slider").id]), 0), status: Status.PUBLISHED))
        homeBusiness.addToPageComponents(new PageComponent(component: new canon.model.Component(ComponentType.FEATURED_PRODUCTS, JsonOutput.toJson([:]), 1), status: Status.PUBLISHED))
        homeBusiness.addToPageComponents(new PageComponent(component: new canon.model.Component(ComponentType.FEATURED_ARTICLES, JsonOutput.toJson([title: "Articles", contentType: ContentType.ARTICLE.name()]), 2), status: Status.PUBLISHED))
        homeBusiness.addToPageComponents(new PageComponent(component: new canon.model.Component(ComponentType.FEATURED_ARTICLES, JsonOutput.toJson([title: "Upcoming Events", contentType: ContentType.EVENT.name()]), 3), status: Status.PUBLISHED))
        homeBusiness.save(failOnError: true)

        prepareData()?.each { k, v ->
            Page p = Page.findBySlug(k)
            if(!p) {
                Language language = Language.findById(v?.language)

                Page page = new Page(site: asia, language: language, banner: v?.banner, headline: v?.headline, slug: k, contentType: v?.contentType, contentTemplate: v?.contentTemplate,
                        status: v?.status, publishedDate: v?.publishedDate, isFeatured: v?.isFeatured)

                v?.products?.each {String s ->
                    Product product = Product.findBySlug(s)
                    if(product) {
                        page.addToPageProducts(new PageProduct(product: product))
                    }
                }

                v?.components?.each { Map map->
                    def contents = map.get("contents") as Map<String, Object>
                    if (!contents || contents?.isEmpty()) {
                        contents = [:]
                        Media media = Media.findByFileName(map?.media)
                        contents.mediaId = media?.id
                        contents.imageUrl = media?.url
                    }

                    canon.model.Component component = new canon.model.Component(map?.compType, JsonOutput.toJson(contents), map?.compOrder)
                    page.addToPageComponents(new PageComponent(component: component))
                }

                v?.categories?.each { String c ->
                    Category cat = Category.findBySlug(c)
                    if (cat) {
                        page.addToPageCategories(new PageCategory(category: cat))
                    }
                }

                v.subjects?.each { String s ->
                    Subject subject = Subject.findByName(s)
                    if (subject) {
                        page.addToPageSubjects(new PageSubject(subject: subject))
                    }
                }

                v.sites?.each { String s ->
                    Site site = Site.findByCode(s)
                    if (site) {
                        page.addToPageDetails(new PageDetail(site: site, flow: flow))
                    }
                }

                v?.tags?.each { String tag ->
                    if(tag) {
                        page.addToPageTags(new PageTag(tag: tag, language: en))
                    }
                }

                page.save(failOnError: true)
            }
        }
    }

    private static def prepareData() {
        String awsBaseUrl = Holders.config.getProperty("amazonaws.s3.baseUrl")
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss")
        return [
                "how-to-take-great-portraits-under-the-midday-sun": [
                        categories: ["tips-and-tutorial"],
                        language: "en",
                        headline: "How to Take Great Portraits Under the Midday Sun",
                        contentType: ContentType.ARTICLE,
                        contentTemplate: ContentTemplate.ARTICLE,
                        status: Status.PUBLISHED,
                        publishedDate: new Date(),
                        isFeatured: Boolean.FALSE,
                        segment: Segment.CONSUMER,
                        products:[
                                "eos-200d-colour"
                        ],
                        banner: new canon.model.Banner("", awsBaseUrl + "images/bg/img01.jpg", null, MediaSource.EXTERNAL, null, null, null, null, null),
                        subjects: ["Low light photography", "Photo techniques"],
                        tags: ["Tips & tutorials"],
                        sites: ["asia"]
                ],
                "10-tips-on-using-extreme-wide-angle-lenses-for-landscape-photography":[
                        categories: ["tips-and-tutorial"],
                        language: "en",
                        headline: "10 Tips on Using Extreme Wide Angle Lenses for Landscape Photography",
                        contentType: ContentType.ARTICLE,
                        contentTemplate: ContentTemplate.ARTICLE,
                        status: Status.PUBLISHED,
                        publishedDate: new Date(),
                        isFeatured: Boolean.FALSE,
                        segment: Segment.CONSUMER,
                        products:[
                                "eos-200d-colour"
                        ],
                        banner: new canon.model.Banner("", awsBaseUrl + "images/bg/img03.jpg", null, MediaSource.EXTERNAL, null, null, null, null, null),
                        subjects: ["Low light photography", "Nature photography", "Photo techniques"],
                        tags: ["Tips & tutorials"],
                        sites: ["asia"]
                ],
                (SlugCodec.encode("Bringing Memories To Life With Dynamic And Personalised Prints")): [
                        categories: ["product"],
                        language: "en",
                        products: [],
                        headline: "Bringing Memories To Life With Dynamic And Personalised Prints",
                        contentType: ContentType.ARTICLE,
                        contentTemplate: ContentTemplate.ARTICLE,
                        status: Status.PUBLISHED,
                        publishedDate: sdf.parse("2017-12-01 12:00:05"),
                        isFeatured: Boolean.TRUE,
                        segment: Segment.CONSUMER,
                        banner: new canon.model.Banner("", awsBaseUrl + "images/article/img1.png", null, MediaSource.EXTERNAL, null, null, null, null, null),
                        components: [
                                [
                                        compType: ComponentType.TEXT,
                                        compOrder: 0,
                                        contents: [content: "Packed in a stylish from with innovative features like the six-colour ink system and message-in-print app, the new, PIXMA printers redefine the power of printing for homes"]
                                ]
                        ],
                        tags: [],
                        subjects: ["Printers"],
                        sites: ["asia"]
                ],
                (SlugCodec.encode("Canon PhotoMarathon 2017")): [
                        categories: ["general"],
                        language: "en",
                        headline: "Canon PhotoMarathon 2017",
                        contentType: ContentType.ARTICLE,
                        contentTemplate: ContentTemplate.ARTICLE,
                        status: Status.PUBLISHED,
                        publishedDate: sdf.parse("2017-12-01 12:00:04"),
                        isFeatured: Boolean.TRUE,
                        segment: Segment.CONSUMER,
                        products: [],
                        banner: new canon.model.Banner("", awsBaseUrl + "images/article/img2.png", null, MediaSource.EXTERNAL, null, null, null, null, null),
                        components: [
                                [
                                        compType: ComponentType.TEXT,
                                        compOrder: 0,
                                        contents: [content: "Amet paulo homero an quo, vis iisque praesent sadipscing ex. Instructior vituperatoribus mel ea, eius dictas consequat ex mea, ferri affert neglegentur in sea."]
                                ]
                        ],
                        tags: [],
                        subjects: ["Events"],
                        sites: ["asia"]
                ],
                (SlugCodec.encode("The Difference Between Vibrance And Saturation")): [
                        categories: ["tips-and-tutorial"],
                        language: "en",
                        headline: "The Difference Between Vibrance And Saturation",
                        contentType: ContentType.ARTICLE,
                        contentTemplate: ContentTemplate.ARTICLE,
                        status: Status.PUBLISHED,
                        publishedDate: sdf.parse("2017-12-01 12:00:03"),
                        isFeatured: Boolean.TRUE,
                        segment: Segment.CONSUMER,
                        products: [],
                        banner: new canon.model.Banner("", awsBaseUrl + "images/article/img3.png", null, MediaSource.EXTERNAL, null, null, null, null, null),
                        components: [
                                [
                                        compType: ComponentType.TEXT,
                                        compOrder: 0,
                                        contents: [content: "The rich colours of the evening sky can add a dramatic element to your landscape photography, especially if it involves flowers shot in backlight. Here, we share two such photos and some tips from the photographers about how they achieved the shot. (Reported by: Rika Takemoto, Yoshiki Fujiwara)"]
                                ],
                                [
                                        compType: ComponentType.IMAGE,
                                        compOrder: 1,
                                        contents: [imageUrl: "https://canon-corpweb-dev.s3.amazonaws.com/images/article/img5.jpg",caption:"EOS 5D Mark III/ EF24-105mm f/4L IS USM/ FL: 47mm/ Aperture-priority AE (f/16, 1/25 sec, EV+1.3)/ ISO 200/ WB: White fluorescent light"]
                                ],
                                [
                                        compType: ComponentType.TEXT,
                                        compOrder: 2,
                                        contents: [content: "Step 1: Shoot downward from a high position<br />I aimed to create a dramatic image by framing the field of poppies together with the evening sun. The field extended all the way into the background, and in order to bring out that sense of scale, I initially shot at eye level, using a tripod. However, that made the elements in the frame look very scattered. I realized that I needed to create a point of interest in the frame by framing the image so that the poppies in front of me appeared larger and closer. Therefore, I found a location where there were some tall poppies directly in front of me, and also adjusted my tripod so that it was at waist level. I had a few different shooting positions in mind, but I eventually chose this one because it provided a good compositional balance between the height of the sun, the silhouettes of the mountains and the poppies in the foreground."]
                                ]
                        ],
                        tags: [],
                        subjects: ["Photo techniques"],
                        sites: ["asia"]
                ],
                (SlugCodec.encode("Canon Signature Zoo Photo-Walk With Julian W.")): [
                        categories: ["event"],
                        language: "en",
                        headline: "Canon Signature Zoo Photo-Walk With Julian W.",
                        contentType: ContentType.EVENT,
                        contentTemplate: ContentTemplate.ARTICLE,
                        status: Status.PUBLISHED,
                        publishedDate: sdf.parse("2017-12-01 12:00:05"),
                        isFeatured: Boolean.TRUE,
                        segment: Segment.CONSUMER,
                        products: [],
                        banner: new canon.model.Banner("", awsBaseUrl + "images/event/img1.png", null, MediaSource.EXTERNAL, null, null, null, null, null),
                        components: [
                                [
                                        compType: ComponentType.TEXT,
                                        compOrder: 0,
                                        contents: [content: "Lorem ipsum dolor sit amet, invidunt expetendis reprehendunt ex est, usu natum ornatus concludaturque et, no vis recteque reformidans. Iriure volutpat ullamcorper te nec."]
                                ]
                        ],
                        tags: [],
                        subjects: ["Photographer interviews"],
                        sites: ["asia"]
                ],
                (SlugCodec.encode("Mongolia Eagle Hunting Photo-Tour (7D6)")): [
                        categories: ["travel"],
                        language: "en",
                        headline: "Mongolia Eagle Hunting Photo-Tour (7D6)",
                        contentType: ContentType.EVENT,
                        contentTemplate: ContentTemplate.ARTICLE,
                        status: Status.PUBLISHED,
                        publishedDate: sdf.parse("2017-12-01 12:00:04"),
                        isFeatured: Boolean.TRUE,
                        segment: Segment.CONSUMER,
                        products: [],
                        banner: new canon.model.Banner("", awsBaseUrl + "images/event/img2.png", null, MediaSource.EXTERNAL, null, null, null, null, null),
                        components: [
                                [
                                        compType: ComponentType.TEXT,
                                        compOrder: 0,
                                        contents: [content: "Amet paulo homero an quo, vis iisque praesent sadipscing ex. Instructior vituperatoribus mel ea, eius dictas consequat ex mea, ferri affert neglegentur in sea."]
                                ]
                        ],
                        tags: [],
                        subjects: ["Events"],
                        sites: ["asia"]
                ],
                (SlugCodec.encode("Photoshop (SFC-Eligible)")): [
                        categories: ["workshop"],
                        language: "en",
                        headline: "Photoshop (SFC-Eligible)",
                        contentType: ContentType.EVENT,
                        contentTemplate: ContentTemplate.ARTICLE,
                        status: Status.PUBLISHED,
                        publishedDate: sdf.parse("2017-12-01 12:00:03"),
                        isFeatured: Boolean.TRUE,
                        segment: Segment.CONSUMER,
                        products: [],
                        banner: new canon.model.Banner("", awsBaseUrl + "images/event/img3.png", null, MediaSource.EXTERNAL, null, null, null, null, null),
                        components: [
                                [
                                        compType: ComponentType.TEXT,
                                        compOrder: 0,
                                        contents: [content: "Amet paulo homero an quo, vis iisque praesent sadipscing ex. Instructior vituperatoribus mel ea, eius dictas consequat ex mea."]
                                ]
                        ],
                        tags: [],
                        subjects: ["Photo techniques"],
                        sites: ["asia"]
                ],
                (SlugCodec.encode("Course1")): [
                        categories: ["course"],
                        language: "en",
                        headline: "Course 1",
                        contentType: ContentType.COURSE,
                        contentTemplate: ContentTemplate.ARTICLE,
                        status: Status.PUBLISHED,
                        publishedDate: sdf.parse("2017-12-01 12:00:03"),
                        isFeatured: Boolean.TRUE,
                        segment: Segment.CONSUMER,
                        products: [],
                        banner: new canon.model.Banner("", awsBaseUrl + "images/event/png.jpg", null, MediaSource.EXTERNAL, null, null, null, null, null),
                        components: [
                                [
                                        compType: ComponentType.TEXT,
                                        compOrder: 0,
                                        contents: [content: "Course1 Amet paulo homero an quo, vis iisque praesent sadipscing ex. Instructior vituperatoribus mel ea, eius dictas consequat ex mea."]
                                ]
                        ],
                        tags: [],
                        sites: ["asia"]
                ],
                (SlugCodec.encode("printer-for-office-solution")): [
                categories: ["business-solutions-by-features"],
                language: "en",
                headline: "Printer for Office Solution",
                contentType: ContentType.SOLUTION,
                contentTemplate: ContentTemplate.PAGE,
                status: Status.PUBLISHED,
                publishedDate: sdf.parse("2017-12-01 12:00:03"),
                isFeatured: Boolean.TRUE,
                segment: Segment.BUSINESS,
                products: [],
                banner: new canon.model.Banner("", awsBaseUrl + "images/article/img3.png", null, MediaSource.EXTERNAL, null, null, null, null, null),
                components: [
                        [
                                compType: ComponentType.TEXT,
                                compOrder: 0,
                                contents: [content: "Course1 Amet paulo homero an quo, vis iisque praesent sadipscing ex. Instructior vituperatoribus mel ea, eius dictas consequat ex mea."]
                        ]
                ],
                tags: [],
                sites: ["asia"]
                ],
                (SlugCodec.encode("printer-for-office-solution1")): [
                        categories: ["business-solutions-by-features"],
                        language: "en",
                        headline: "Camera Solutions For Business",
                        contentType: ContentType.SOLUTION,
                        contentTemplate: ContentTemplate.PAGE,
                        status: Status.PUBLISHED,
                        publishedDate: sdf.parse("2017-12-01 12:00:03"),
                        isFeatured: Boolean.TRUE,
                        segment: Segment.BUSINESS,
                        products: [],
                        banner: new canon.model.Banner("", awsBaseUrl + "images/article/img2.png", null, MediaSource.EXTERNAL, null, null, null, null, null),
                        components: [
                                [
                                        compType: ComponentType.TEXT,
                                        compOrder: 0,
                                        contents: [content: "Course1 Amet paulo homero an quo, vis iisque praesent sadipscing ex. Instructior vituperatoribus mel ea, eius dictas consequat ex mea."]
                                ]
                        ],
                        tags: [],
                        sites: ["asia"]
                ],
                (SlugCodec.encode("printer-for-office-solution2")): [
                        categories: ["business-solutions-by-features"],
                        language: "en",
                        headline: "Printer for Office Solution",
                        contentType: ContentType.SOLUTION,
                        contentTemplate: ContentTemplate.PAGE,
                        status: Status.PUBLISHED,
                        publishedDate: sdf.parse("2017-12-01 12:00:03"),
                        isFeatured: Boolean.TRUE,
                        segment: Segment.BUSINESS,
                        products: [],
                        banner: new canon.model.Banner("", awsBaseUrl + "images/article/img1.png", null, MediaSource.EXTERNAL, null, null, null, null, null),
                        components: [
                                [
                                        compType: ComponentType.TEXT,
                                        compOrder: 0,
                                        contents: [content: "Course1 Amet paulo homero an quo, vis iisque praesent sadipscing ex. Instructior vituperatoribus mel ea, eius dictas consequat ex mea."]
                                ]
                        ],
                        tags: [],
                        sites: ["asia"]
                ]
        ]
    }
}
