package canon

import canon.enums.MenuLocation
import canon.enums.MenuType
import canon.enums.Segment
import canon.enums.Status
import org.grails.plugins.web.taglib.ApplicationTagLib

class MenuInit {

    static init() {
        prepareData()?.each {k, v ->
            Site site = Site.findByCode(k)
            if(site) {
                v?.each {key, val ->
                    Language lang = Language.findById(key)
                    if (lang){
                        val?.each {Menu m ->
                            Menu menu = Menu.findByNameAndSiteAndLanguageAndSegment(m.name, site, lang, m.segment)
                            if (!menu) {
                                m.site = site
                                m.language = lang

                                m.save(failOnError: true)
                            }
                        }
                    }
                }
            }
        }
    }

    private static def prepareData() {
        return [
                "asia": [
                        "en" : [
                                new Menu(isMain: true, name: "Products", type: MenuType.LINK, url: "/product/category?segment=consumer&site=asia&lang=en", orderNo: 1, location: MenuLocation.HEADER_LEFT, status: Status.PUBLISHED, segment: Segment.CONSUMER),
                                new Menu(isMain: true, name: "Articles", type: MenuType.LINK, url: "/asia/articles?segment=consumer", orderNo: 2, location: MenuLocation.HEADER_LEFT, status: Status.PUBLISHED, segment: Segment.CONSUMER),
                                new Menu(isMain: true, name: "Events", type: MenuType.LINK, url: "/asia/events?segment=consumer", orderNo: 3, location: MenuLocation.HEADER_LEFT, status: Status.PUBLISHED, segment: Segment.CONSUMER),
                                new Menu(isMain: true, name: "Course", type: MenuType.LINK, url: "/asia/course?segment=consumer", orderNo: 4, location: MenuLocation.HEADER_LEFT, status: Status.PUBLISHED, segment: Segment.CONSUMER),
                                new Menu(isMain: true, name: "Contact", type: MenuType.LINK, url: "#", orderNo: 5, location: MenuLocation.HEADER_LEFT, status: Status.PUBLISHED, segment: Segment.CONSUMER),
                                new Menu(isMain: true, name: "Promotions", type: MenuType.LINK, url: "#", orderNo: 6, location: MenuLocation.HEADER_LEFT, status: Status.PUBLISHED, segment: Segment.CONSUMER),
                                new Menu(isMain: true, name: "Club Canon", type: MenuType.LINK, url: "#", orderNo: 7, location: MenuLocation.HEADER_LEFT, status: Status.PUBLISHED, segment: Segment.CONSUMER),
                                new Menu(isMain: true, name: "Warranty", type: MenuType.LINK, url: "#", orderNo: 1, location: MenuLocation.HEADER_RIGHT, status: Status.PUBLISHED, segment: Segment.CONSUMER),
                                new Menu(isMain: true, name: "Support", type: MenuType.LINK, url: "#", orderNo: 2, location: MenuLocation.HEADER_RIGHT, status: Status.PUBLISHED, segment: Segment.CONSUMER),

                                new Menu(isMain: true, name: "Products", type: MenuType.LINK, url: "/product/category?segment=business&site=asia&lang=en", orderNo: 1, location: MenuLocation.HEADER_LEFT, status: Status.PUBLISHED, segment: Segment.BUSINESS),
                                new Menu(isMain: true, name: "Solutions", type: MenuType.LINK, url: "/asia/solutions?segment=business", orderNo: 2, location: MenuLocation.HEADER_LEFT, status: Status.PUBLISHED, segment: Segment.BUSINESS),
                                new Menu(isMain: true, name: "Articles", type: MenuType.LINK, url: "/asia/articles?segment=business", orderNo: 3, location: MenuLocation.HEADER_LEFT, status: Status.PUBLISHED, segment: Segment.BUSINESS),
                                new Menu(isMain: true, name: "Events", type: MenuType.LINK, url: "/asia/events?segment=business", orderNo: 4, location: MenuLocation.HEADER_LEFT, status: Status.PUBLISHED, segment: Segment.BUSINESS),
                                new Menu(isMain: true, name: "Contact", type: MenuType.LINK, url: "#", orderNo: 5, location: MenuLocation.HEADER_LEFT, status: Status.PUBLISHED, segment: Segment.BUSINESS),
                                new Menu(isMain: true, name: "Warranty", type: MenuType.LINK, url: "#", orderNo: 8, location: MenuLocation.HEADER_RIGHT, status: Status.PUBLISHED, segment: Segment.BUSINESS),
                                new Menu(isMain: true, name: "Support", type: MenuType.LINK, url: "#", orderNo: 9, location: MenuLocation.HEADER_RIGHT, status: Status.PUBLISHED, segment: Segment.BUSINESS)
                        ]
                ],
                "singapore": [
                        "en" : [
                                new Menu(isMain: true, name: "Products", type: MenuType.LINK, url: "/product/category?segment=consumer&site=singapore&lang=en", orderNo: 1, location: MenuLocation.HEADER_LEFT, status: Status.PUBLISHED, segment: Segment.CONSUMER),
                                new Menu(isMain: true, name: "Articles", type: MenuType.LINK, url: "#", orderNo: 2, location: MenuLocation.HEADER_LEFT, status: Status.PUBLISHED, segment: Segment.CONSUMER),
                                new Menu(isMain: true, name: "Events", type: MenuType.LINK, url: "#", orderNo: 3, location: MenuLocation.HEADER_LEFT, status: Status.PUBLISHED, segment: Segment.CONSUMER),
                                new Menu(isMain: true, name: "Course", type: MenuType.LINK, url: "#", orderNo: 4, location: MenuLocation.HEADER_LEFT, status: Status.PUBLISHED, segment: Segment.CONSUMER),
                                new Menu(isMain: true, name: "Contact", type: MenuType.LINK, url: "#", orderNo: 5, location: MenuLocation.HEADER_LEFT, status: Status.PUBLISHED, segment: Segment.CONSUMER),
                                new Menu(isMain: true, name: "Promotions", type: MenuType.LINK, url: "#", orderNo: 6, location: MenuLocation.HEADER_LEFT, status: Status.PUBLISHED, segment: Segment.CONSUMER),
                                new Menu(isMain: true, name: "Club Canon", type: MenuType.LINK, url: "#", orderNo: 7, location: MenuLocation.HEADER_LEFT, status: Status.PUBLISHED, segment: Segment.CONSUMER),
                                new Menu(isMain: true, name: "Warranty", type: MenuType.LINK, url: "#", orderNo: 1, location: MenuLocation.HEADER_RIGHT, status: Status.PUBLISHED, segment: Segment.CONSUMER),
                                new Menu(isMain: true, name: "Support", type: MenuType.LINK, url: "#", orderNo: 2, location: MenuLocation.HEADER_RIGHT, status: Status.PUBLISHED, segment: Segment.CONSUMER),

                                new Menu(isMain: true, name: "Products", type: MenuType.LINK, url: "/product/category?segment=business&site=singapore&lang=en", orderNo: 1, location: MenuLocation.HEADER_LEFT, status: Status.PUBLISHED, segment: Segment.BUSINESS),
                                new Menu(isMain: true, name: "Solutions", type: MenuType.LINK, url: "#", orderNo: 2, location: MenuLocation.HEADER_LEFT, status: Status.PUBLISHED, segment: Segment.BUSINESS),
                                new Menu(isMain: true, name: "Articles", type: MenuType.LINK, url: "#", orderNo: 3, location: MenuLocation.HEADER_LEFT, status: Status.PUBLISHED, segment: Segment.BUSINESS),
                                new Menu(isMain: true, name: "Events", type: MenuType.LINK, url: "#", orderNo: 4, location: MenuLocation.HEADER_LEFT, status: Status.PUBLISHED, segment: Segment.BUSINESS),
                                new Menu(isMain: true, name: "Contact", type: MenuType.LINK, url: "#", orderNo: 5, location: MenuLocation.HEADER_LEFT, status: Status.PUBLISHED, segment: Segment.BUSINESS),
                                new Menu(isMain: true, name: "Warranty", type: MenuType.LINK, url: "#", orderNo: 8, location: MenuLocation.HEADER_RIGHT, status: Status.PUBLISHED, segment: Segment.BUSINESS),
                                new Menu(isMain: true, name: "Support", type: MenuType.LINK, url: "#", orderNo: 9, location: MenuLocation.HEADER_RIGHT, status: Status.PUBLISHED, segment: Segment.BUSINESS)
                        ]
                ]
        ]
    }
}
