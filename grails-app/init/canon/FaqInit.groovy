package canon

class FaqInit {

    static init() {
        Site asia = Site.findByCode('asia')
        initFaq(asia)

        Site singapore = Site.findByCode('singapore')
        initFaq(singapore)

        Site indonesia = Site.findByCode('indonesia')
        initFaq(indonesia)

        Site malaysia = Site.findByCode('malaysia')
        initFaq(malaysia)

        Site india = Site.findByCode('india')
        initFaq(india)

        Site thailand = Site.findByCode('thailand')
        initFaq(thailand)

        Site vietnam = Site.findByCode('vietnam')
        initFaq(vietnam)

        Site philippines = Site.findByCode('philippines')
        initFaq(philippines)

    }

    static initFaq(Site site) {
    	Faq product = 
    		new Faq(site: site, name: 'Product', dateCreated: new Date(), lastUpdated: new Date())
    			.save(failOnError: true)
    	initFaqTranslation(product)
    	initFaqItem(product)

    	Faq warranty = 
    		new Faq(site: site, name: 'Warranty', dateCreated: new Date(), lastUpdated: new Date())
    			.save(failOnError: true)
    	initFaqTranslation(warranty)
    	initFaqItem(warranty)

    	Faq service = 
    		new Faq(site: site, name: 'Service', dateCreated: new Date(), lastUpdated: new Date())
    			.save(failOnError: true)
    	initFaqTranslation(service)
    	initFaqItem(service)

    	Faq training = 
    		new Faq(site: site, name: 'Training', dateCreated: new Date(), lastUpdated: new Date())
    			.save(failOnError: true)
    	initFaqTranslation(training)
    	initFaqItem(training)

    	Faq redemption = 
    		new Faq(site: site, name: 'Redemption', dateCreated: new Date(), lastUpdated: new Date())
    			.save(failOnError: true)
    	initFaqTranslation(redemption)
    	initFaqItem(redemption)

    	Faq environmental = 
    		new Faq(site: site, name: 'Environmental', dateCreated: new Date(), lastUpdated: new Date())
    			.save(failOnError: true)
    	initFaqTranslation(environmental)
    	initFaqItem(environmental)

    	Faq other = 
    		new Faq(site: site, name: 'Other Matters', dateCreated: new Date(), lastUpdated: new Date())
    			.save(failOnError: true)
    	initFaqTranslation(other)
    	initFaqItem(other)

    	Faq troubleshoot = 
    		new Faq(site: site, name: 'Troubleshoot', dateCreated: new Date(), lastUpdated: new Date())
    			.save(failOnError: true)
    	initFaqTranslation(troubleshoot)
    	initFaqItem(troubleshoot)

    }

    static initFaqTranslation(Faq faq){
    	Language zh_CN = Language.findById('zh_CN')
    	new FaqTranslation(faq: faq, language: zh_CN, name: faq.name, dateCreated: new Date(), lastUpdated: new Date())
    		.save(failOnError: true)
    }

    static initFaqItem(Faq faq){
    	Language zh_CN = Language.findById('zh_CN')

    	FaqItem q1 = 
    		new FaqItem(faq: faq, label: 'General', content: 'Does canon ... ?', dateCreated: new Date(), lastUpdated: new Date())
    			.save(failOnError: true)
    	initFaqItemAnswer(q1)
    	initFaqItemTranslation(q1, zh_CN)

    	FaqItem q2 = 
    		new FaqItem(faq: faq, label: 'General', content: 'How come ... ?', dateCreated: new Date(), lastUpdated: new Date())
    			.save(failOnError: true)
    	initFaqItemAnswer(q2)
    	initFaqItemTranslation(q2, zh_CN)

    	FaqItem q3 = 
    		new FaqItem(faq: faq, label: 'SOHO / Printer-related', content: 'Is it possible ... ?', dateCreated: new Date(), lastUpdated: new Date())
    			.save(failOnError: true)
    	initFaqItemAnswer(q3)
    	initFaqItemTranslation(q3, zh_CN)

    	FaqItem q4 = 
    		new FaqItem(faq: faq, label: 'SOHO / Printer-related', content: 'Where do I ... ?', dateCreated: new Date(), lastUpdated: new Date())
    			.save(failOnError: true)
    	initFaqItemAnswer(q4)
    	initFaqItemTranslation(q4, zh_CN)


    }

    static initFaqItemAnswer(FaqItem faqItem){
    	Language en = Language.findById('en')
    	Language zh_CN = Language.findById('zh_CN')

    	new FaqItemAnswer(faqItem: faqItem, language: en, content: '(Answer in english)', dateCreated: new Date(), lastUpdated: new Date())
    		.save(failOnError: true)

    	new FaqItemAnswer(faqItem: faqItem, language: zh_CN, content: '(Answer in chinese)', dateCreated: new Date(), lastUpdated: new Date())
    		.save(failOnError: true)

    }

    static initFaqItemTranslation(FaqItem faqItem, Language language){

    	new FaqItemTranslation(faqItem: faqItem, language: language, label: faqItem.label, content: faqItem.content, dateCreated: new Date(), lastUpdated: new Date())
    		.save(failOnError: true)

    }

}
