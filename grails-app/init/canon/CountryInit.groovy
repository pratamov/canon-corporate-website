package canon

class CountryInit {

    static init() {
        // Countries
        String[] locales = Locale.getISOCountries()
        for (String countryCode : locales) {
            Locale obj = new Locale("", countryCode)
            new Country(code: obj.getCountry(), name: obj.getDisplayCountry()).save(failOnError: true)
        }
    }
}
