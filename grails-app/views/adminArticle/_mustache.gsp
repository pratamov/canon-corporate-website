<script id="page-location-tr-template" type="x-tmpl-mustache">
   <tr>
       <input type="hidden" name="locationSiteId" value="{{siteId}}" id="locationSiteId" />
       <input type="hidden" name="locationCategoryId" value="{{categoryId}}" id="locationCategoryId" />
       <input type="hidden" name="locationPublishedDate" value="{{publishedDate}}" id="locationPublishedDate" />
       <input type="hidden" name="locationArchivedDate" value="{{archivedDate}}" id="locationArchivedDate" />
       <td><a href="#" class="single-link">{{siteName}}</a></td>
       <td>{{categoryName}}</td>
       <td>{{publishedDate}}</td>
       <td>{{archivedDate}}</td>
       <td class="text-right"><a href="#" class="js-delete-location"><span class="fa fa-trash"></span></a></td>
   <tr>
</script>