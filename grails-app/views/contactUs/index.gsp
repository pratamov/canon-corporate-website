<%--
  Created by IntelliJ IDEA.
  User: rifai
  Date: 31/12/17
  Time: 2:43 AM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title><g:message code="public.contactus.header.label" default="Contact Us"/></title>
    <meta name="layout" content="canon"/>
</head>

<body>
<!-- Start Content -->
<div class="section">
    <div class="wrap-contact">
        <div class="container">
            <!-- Start Box Panel -->
            <div class="box-panel bg-map">
                <h1 class="title-contact text-center left-small">Contact Us</h1>
                <div class="content-contact">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2 text-center left-small">
                            <p class="margin-bottom20">
                                Have you tried visited "<a href="#" class="single-link">Frequently Asked Questions</a>" you may find the answer to your query there. If not we look forward to help you out here.
                            </p>
                        </div>
                    </div>
                    <hr class="line">
                    <div class="row margin-top30">
                        <div class="col-sm-5 col-sm-push-7 m-margin-bottom30">
                            <div class="form-group">
                                <select class="form-control select">
                                    <option value="" selected disabled>Select the type of question</option>
                                    <option value="1">Lorem ipsum dolor</option>
                                    <option value="2">Lorem ipsum dolor</option>
                                    <option value="3">Lorem ipsum dolor</option>
                                    <option value="4">Lorem ipsum dolor</option>
                                    <option value="5">Lorem ipsum dolor</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <input type="email" class="form-control" placeholder="helo@email.com">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Name">
                            </div>
                            <div class="form-group">
                                <textarea class="form-control" placeholder="Message" rows="5"></textarea>
                            </div>
                            <button class="btn btn-primary btn-block">Send</button>
                        </div>
                        <div class="col-sm-7 col-sm-pull-5 m-padding-bottom20">
                            <h3 class="font-light margin-bottom40">
                                Canon Customer Care Centre 1 Fusionopolis Place, #02-20 Glaxis (West Lobby) Singapore 138522
                            </h3>
                            <p>
                                Monday to Friday | 10:00am - 7:00pm<br />
                                Saturday | 10:00am - 2:30pm<br />
                                Closed on Sunday and Public Holidays
                            </p>
                            <a href="#" class="btn btn-default btn-sm more">See all locations <span class="fa fa-angle-right"></span></a><br />
                            <a href="#" class="btn btn-default btn-sm more">Send a feedback <span class="fa fa-angle-right"></span></a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Box Panel -->
        </div>
    </div>
</div>
<!-- End Content -->

<!-- Start Content -->
<div class="section">
    <div class="wrap-contact">
        <div class="container">
            <h1 class="title-section title-contact">Request for Business Services</h1>
        </div>
        <div class="padding-side-small">
            <div class="content-contact">
                <!-- Start Nav -->
                <div class="nav-result margin-bottom50">
                    <div class="container">
                        <a href="#" id="nav1" class="toggle-nav-result tab-nav"></a>
                        <ul class="list-tab-nav" data-toggle-content="#nav1">
                            <li class="active"><a data-toggle="tab" href="#onsite">On-Site Service</a></li>
                            <li><a data-toggle="tab" href="#meter">Meter Reading</a></li>
                            <li><a data-toggle="tab" href="#load">Load A Copier</a></li>
                            <li><a data-toggle="tab" href="#training">Copier Training</a></li>
                            <li><a data-toggle="tab" href="#consumable">Order Consumables</a></li>
                            <li><a data-toggle="tab" href="#support">Support</a></li>
                            <li><a data-toggle="tab" href="#billing">Billing</a></li>
                            <li><a data-toggle="tab" href="#information">Update Information</a></li>
                        </ul>
                    </div>
                </div>
                <!-- End Nav -->
                <div class="container">
                    <!-- Start Box Panel -->
                    <div class="box-panel">
                        <div class="tab-content">
                            <!-- Start Content Tab -->
                            <div id="onsite" class="tab-pane fade in active">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <h3 class="font-light margin-bottom40">
                                            Product and fault description
                                        </h3>
                                        <div class="form-group">
                                            <input type="text" class="form-control" placeholder="Serial Number">
                                        </div>
                                        <div class="form-group">
                                            <textarea class="form-control" placeholder="Describe fault" rows="5"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <h3 class="font-light margin-bottom40">
                                            Your details
                                        </h3>
                                        <div class="form-group">
                                            <input type="text" class="form-control" placeholder="Name">
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" placeholder="Contact Number">
                                        </div>
                                        <div class="form-group">
                                            <input type="email" class="form-control" placeholder="Email Address">
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" placeholder="Company Name">
                                        </div>
                                        <button class="btn btn-primary btn-block">Send</button>
                                    </div>
                                </div>
                            </div>
                            <!-- End Content Tab -->
                            <!-- Strat Content Tab -->
                            <div id="meter" class="tab-pane fade">
                                meter
                            </div>
                            <!-- End Content Tab -->
                            <!-- Strat Content Tab -->
                            <div id="load" class="tab-pane fade">
                                load
                            </div>
                            <!-- End Content Tab -->
                            <!-- Strat Content Tab -->
                            <div id="training" class="tab-pane fade">
                                training
                            </div>
                            <!-- End Content Tab -->
                            <!-- Strat Content Tab -->
                            <div id="consumable" class="tab-pane fade">
                                consumable
                            </div>
                            <!-- End Content Tab -->
                            <!-- Strat Content Tab -->
                            <div id="support" class="tab-pane fade">
                                support
                            </div>
                            <!-- End Content Tab -->
                            <!-- Strat Content Tab -->
                            <div id="billing" class="tab-pane fade">
                                billing
                            </div>
                            <!-- End Content Tab -->
                            <!-- Strat Content Tab -->
                            <div id="information" class="tab-pane fade">
                                information
                            </div>
                            <!-- End Content Tab -->
                        </div>
                    </div>
                    <!-- End Box Panel -->
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Content -->

<div class="section padding-bottom20 padding-top0"></div>

<!-- Start Content -->
<div class="section">
    <div class="wrap-contact">
        <div class="container">
            <h1 class="title-section title-contact">Request for Personal Services</h1>
        </div>
        <div class="padding-side-small">
            <div class="content-contact">
                <!-- Start Nav -->
                <div class="nav-result margin-bottom50">
                    <div class="container">
                        <a href="#" id="nav2" class="toggle-nav-result tab-nav"></a>
                        <ul class="list-tab-nav" data-toggle-content="#nav2">
                            <li class="active"><a data-toggle="tab" href="#repaire2">Camera Repaire</a></li>
                            <li><a data-toggle="tab" href="#training2">Training</a></li>
                            <li><a data-toggle="tab" href="#information2">Update Information</a></li>
                        </ul>
                    </div>
                </div>
                <!-- End Nav -->
                <div class="container">
                    <!-- Start Box Panel -->
                    <div class="box-panel margin-bottom50">
                        <div class="tab-content">
                            <!-- Strat Content Tab -->
                            <div id="repaire2" class="tab-pane fade in active">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <h3 class="font-light margin-bottom40">
                                            Product and fault description
                                        </h3>
                                        <div class="form-group">
                                            <input type="text" class="form-control" placeholder="Serial Number">
                                        </div>
                                        <div class="form-group">
                                            <textarea class="form-control" placeholder="Describe fault" rows="5"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <h3 class="font-light margin-bottom40">
                                            Your details
                                        </h3>
                                        <div class="form-group">
                                            <input type="text" class="form-control" placeholder="Name">
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" placeholder="Contact Number">
                                        </div>
                                        <div class="form-group">
                                            <input type="email" class="form-control" placeholder="Email Address">
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" placeholder="Company Name">
                                        </div>
                                        <button class="btn btn-primary btn-block">Send</button>
                                    </div>
                                </div>
                            </div>
                            <!-- End Content Tab -->
                            <!-- Strat Content Tab -->
                            <div id="training2" class="tab-pane fade">
                                Training
                            </div>
                            <!-- End Content Tab -->
                            <!-- Strat Content Tab -->
                            <div id="information2" class="tab-pane fade">
                                information
                            </div>
                            <!-- End Content Tab -->
                        </div>
                    </div>
                    <!-- End Box Panel -->
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Content -->

<!-- Start Content -->
<div class="section">
    <div class="wrap-contact">
        <div class="container">
            <!-- Start Box Panel -->
            <div class="box-panel single">
                <h3 class="font-light title-contact margin-bottom40 m-margin-bottom0">
                    Call Us
                </h3>
                <div class="wrap-list-border content-contact">
                    <ul class="list-border">
                        <li class="match-height col-20">
                            Sales Hostline<br />
                            6-TO-CANON<br />
                            (6-86-22666)
                        </li>
                        <li class="match-height col-20">
                            Service Hotline<br />
                            6799 8686
                        </li>
                        <li class="match-height col-20">
                            Main Line<br />
                            6799 8888
                        </li>
                        <li class="match-height col-20">
                            Fax Number<br />
                            6799 8882
                        </li>
                        <li class="match-height col-20">
                            Monday to Friday<br />
                            8:30am - 6:00pm<br /><br />
                            Closed on Public Holidays.
                        </li>
                    </ul>
                </div>
            </div>
            <!-- End Box Panel -->
        </div>
    </div>
</div>
<!-- End Content -->

</div>
<!-- End Wrap Component -->

<div class="clearfix"></div>
</body>
</html>