<g:set var="compProdPromotion" value="${component?.getAsComponent() as canon.model.comp.ProdPromotionComp}" />
<g:if test="${product?.activePromotion}">
    <!-- Start Content -->
    <div class="section">
        <div class="container">
            <!-- Start Promotion Box -->
            <div class="promotion-box">
                <h1 class="title"><g:message code="product.overview.promotion.label" default="Promotion"/></h1>
                <p>
                    ${raw(product?.activePromotion?.promotionDesc)}
                </p>
                <g:link class="btn btn-primary" controller="promotion" action="index">
                    <g:message code="product.overview.findOutMore.label" default="Find Out More"/>
                </g:link>
            </div>
            <!-- End Promotion Box -->
        </div>
    </div>
    <!-- End Content -->
    <div class="clearfix"></div>
</g:if>
