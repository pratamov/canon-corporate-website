<%@ page import="canon.FeatureComponent; canon.ProductFeature" %>
<!-- Start Content -->
<g:set var="compProdPromotion" value="${component?.getAsComponent() as canon.model.comp.ProdFeaturesComp}" />
<g:set var="productFeatures" value="${compProdPromotion?.featureList}"/>
<g:if test="${productFeatures}">
    <!-- Start Section Detail -->
    <div class="section">
        <div class="container">
            <!-- Start Features -->
            <h1 class="title-section text-center">
                <g:message code="product.overview.features.label" default="Features"/>
            </h1>
            <div class="row main-row-slide">
                <div class="col-md-10 col-md-offset-1">
                    <!-- Start Item Feature -->
                    <div class="row slide-in-small">
                        <g:each in="${productFeatures}" var="productFeature" status="i">
                            <div class="col-sm-4 col-xs-12 margin-bottom20">
                                <div class="box-panel match-height ${i == 0 ? 'active':''} text-left">
                                    <div class="content">
                                        <img src="${productFeature?.iconMedia?.url}" alt="" class="img-gray">
                                        <h4 class="title">${productFeature?.iconTitle}</h4>
                                        <p>${productFeature?.iconSubtitle}</p>
                                    </div>
                                    <a href="#feature-content__${productFeature?.id}" class="link" data-toggle="tab"></a>
                                </div>
                            </div>
                        </g:each>
                    </div>
                    <!-- End Item Feature -->
                </div>
            </div>
            <!-- End Content -->
        </div>
    </div>
    <!-- End Section Detail -->
    <!-- Start Section Detail -->
    <div class="section">
        <div class="container center-small">
            <div class="tab-content">
                <g:each in="${productFeatures}" var="productFeature" status="i">
                    <!-- Start Tab Pane -->
                    <div class="tab-pane fade ${i == 0 ? 'in active' : ''}" id="feature-content__${productFeature?.id}">
                        <div class="row">
                            ${raw(productFeature?.description)}
                        </div>
                    </div>
                    <!-- End Tab Pane -->
                </g:each>
            </div>
        </div>
    </div>
    <!-- End Section Detail -->
    <div class="clearfix"></div>
</g:if>
