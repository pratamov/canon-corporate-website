<g:set var="compImage" value="${component?.getAsComponent() as canon.model.comp.ImageComp}" />
<g:if test="${compImage}">
    <!-- Start Section -->
    <div class="section">
        <div class="container">
            <g:if test="${org.apache.commons.lang.StringUtils.isNotBlank(compImage.getTitle())}">
                <h3 class="font-light margin-bottom30 img-title">${compImage.getTitle()}</h3>
            </g:if>
            <div class="img-article">
                <img src="${compImage.getImageUrl()}" class="img-responsive" alt="" data-copyright="${compImage?.getCopyright() ?: ''}" />
                <g:if test="${org.apache.commons.lang.StringUtils.isNotBlank(compImage.getCaption())}">
                    <small class="margin-top10">
                        ${raw(compImage.getCaption())}
                    </small>
                </g:if>
            </div>
        </div>
    </div>
    <!-- End Section -->
</g:if>