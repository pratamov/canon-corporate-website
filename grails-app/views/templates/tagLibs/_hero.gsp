<g:set var="compHero" value="${component?.getAsComponent() as canon.model.comp.HeroComp}" />
<g:if test="${compHero}">
    <!-- Start Section -->
    <div class="section">
        <div class="container">
            <div class="img-article"><img src="${compHero?.imageUrl}" class="img-responsive" alt=""/></div>
        </div>
    </div>
    <!-- End Section -->
</g:if>