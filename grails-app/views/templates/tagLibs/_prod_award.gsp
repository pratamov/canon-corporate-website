<%@ page import="canon.Award; canon.ProductAward" %>
<g:set var="compProdAward" value="${component?.getAsComponent() as canon.model.comp.ProdAwardComp}" />
<g:if test="${product?.activeAwards}">
    <!-- Start Section Detail -->
    <div class="section">
        <div class="container">
            <!-- Start Award -->
            <h1 class="title-section text-center"><g:message code="product.overview.awards.label" default="Awards"/></h1>
            <div class="row main-row-slide">
                <div class="col-md-10 col-md-offset-1">
                    <!-- Start Item Features -->
                    <div class="row slide-in-small">
                    <g:each in="${product?.activeAwards as java.util.List<canon.Award>}" var="award">
                        <div class="col-sm-4 col-xs-12 margin-bottom20">
                            <div class="box-panel">
                                <img src="${award?.imageUrl}" alt="">
                            </div>
                        </div>
                    </g:each>
                    <!-- End Item Features -->
                </div>
            </div>
            <!-- End Awards -->
        </div>
    </div>
    <!-- End Section Detail -->
</g:if>
