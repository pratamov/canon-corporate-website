<!-- Start Content -->
<g:set var="compProdBanner" value="${component?.getAsComponent() as canon.model.comp.ProdBannerComp}" />
<div class="section-demo bg" data-image="${compProdBanner?.bgUrl}">
    <div class="overlay"></div>
    <div class="container content-demo">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="content">
                    <div class="row">
                        <div class="col-sm-6 col-sm-push-6">
                            <img src="${product?.getDefaultMedia()?.media?.url}" class="img-responsive" alt=""/>
                        </div>
                        <div class="col-sm-6 col-sm-pull-6">
                            <h1 class="title">${product?.name}</h1>
                            <p class="opacity50">
                                ${compProdBanner?.desc ?: product?.subText}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Content -->

<div class="clearfix"></div>