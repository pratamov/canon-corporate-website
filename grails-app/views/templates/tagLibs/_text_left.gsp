<g:set var="compTextImage" value="${component?.getAsComponent() as canon.model.comp.TextImageComp}" />
<!-- Start Section -->
<div class="section">
    <div class="row">
        <div class="col-md-6 component-text-left-content">
            ${raw(compTextImage?.getContent() ?: '')}
        </div>
        <div class="col-md-6 component-text-left-image">
            <img src="${compTextImage?.getImageUrl() ?: ''}" class="img-responsive" alt="" data-copyright="${compTextImage?.getCopyright() ?: ''}" />
        </div>
    </div>
</div>
<!-- End Section -->