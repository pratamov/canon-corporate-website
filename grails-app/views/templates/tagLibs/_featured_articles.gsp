<%@ page import="canon.enums.Segment; canon.utils.ContentUtils; canon.model.comp.FeaturedArticlesComp" %>
<g:set var="compFeaturedArticles" value="${component?.getAsComponent() as FeaturedArticlesComp}" />
<g:if test="${compFeaturedArticles && compFeaturedArticles.getFeaturedList()?.size() > 0}">
    <g:set var="catName" value="${compFeaturedArticles.category?.translation?.name ?: compFeaturedArticles.category?.name}" />
    <!-- Start Content -->
    <div class="section">
        <div class="container">
            <g:if test="${!compFeaturedArticles.getHideTitle()}">
                <h3 class="title-section">${compFeaturedArticles.title ?: catName ?: message(code: "component.FEATURED_ARTICLES."+compFeaturedArticles.contentType?.name()+".title", default: "Articles")}</h3>
            </g:if>
            <div class="wrap-carousel-mobile">
                <div class="row">
                    <g:set var="fFeaturedArticle" value="${compFeaturedArticles.getFeaturedList().get(0)}" />
                    <div class="col-sm-12">
                        <!-- Start Post Item -->
                        <div class="post-item match-height">
                            <span class="badge left gray">${fFeaturedArticle.getMainCategory(segment as String)?.translation?.name ?: fFeaturedArticle.getMainCategory(segment as String)?.name}</span>
                            <div class="row wrap-vertical-content">
                                <div class="col-sm-6 getheight">
                                    <div class="wrap-image bg-img-post">
                                        <g:set var="articleBgPos" value="${fFeaturedArticle.getBanner()?.getFeaturedLargeX() ? 'background-position: ' + fFeaturedArticle.getBanner()?.getFeaturedLargeX() + '%;' : ''}" />
                                        <g:link mapping="${ compFeaturedArticles.contentType?.name()?.toLowerCase()+'Details' }" id="${fFeaturedArticle?.slug}" params="${params}">
                                            <img src="${fFeaturedArticle.getBanner()?.getUrl()}" alt="${fFeaturedArticle?.headline}" />
                                        </g:link>
                                    </div>
                                </div>
                                <div class="col-sm-6 desc">
                                    <div class="vertical-middle">
                                        <div class="content left-medium">
                                            <h3 class="title">
                                                <g:link mapping="${ compFeaturedArticles.contentType?.name()?.toLowerCase()+'Details' }" id="${fFeaturedArticle?.slug}" params="${params}">
                                                    ${raw(fFeaturedArticle?.headline)}
                                                </g:link>
                                            </h3>
                                            <p><g:set var="firstTextComponent" value="${fFeaturedArticle.getFirstTextComponent()?.component?.getAsComponent() as canon.model.comp.TextComp}" />
                                                <strong><g:formatDate date="${fFeaturedArticle.publishedDate}" format="dd MMMM yyyy" /></strong>
                                                - ${raw(ContentUtils.truncate(fFeaturedArticle?.deck ?: firstTextComponent?.getContent()))}
                                            </p>
                                            <g:link mapping="${ compFeaturedArticles.contentType?.name()?.toLowerCase()+'Details' }" id="${fFeaturedArticle.slug}" params="${params}" class="btn btn-primary">
                                                <g:message code="component.FEATURED_ARTICLES.readMore.label" default="Read More" />
                                            </g:link>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Post Item -->
                    </div>
                    <g:if test="${compFeaturedArticles.getFeaturedList()?.size() > 1}">
                        <g:set var="sFeaturedArticle" value="${compFeaturedArticles.getFeaturedList().get(1)}" />
                        <div class="col-md-8 col-sm-7">
                            <!-- Start Post Item -->
                            <div class="post-item match-height">
                                <span class="badge left gray">${sFeaturedArticle.getMainCategory(segment as String)?.translation?.name ?: sFeaturedArticle.getMainCategory(segment as String)?.name}</span>
                                <div class="wrap-image bg-img-post">
                                    <g:set var="articleBgPos" value="${sFeaturedArticle.getBanner()?.getFeaturedMediumX() ? 'background-position: ' + sFeaturedArticle.getBanner()?.getFeaturedMediumX() + '%;' : ''}" />
                                    <g:link mapping="${ compFeaturedArticles.contentType?.name()?.toLowerCase()+'Details' }" id="${sFeaturedArticle.slug}" params="${params}">
                                        <img src="${sFeaturedArticle.getBanner().getUrl()}" alt="${sFeaturedArticle?.headline}" />
                                    </g:link>
                                </div>
                                <div class="content text-left">
                                    <h5 class="title">
                                        <g:link mapping="${ compFeaturedArticles.contentType?.name()?.toLowerCase()+'Details' }" id="${sFeaturedArticle.slug}" params="${params}">
                                            ${raw(sFeaturedArticle?.headline)}
                                        </g:link>
                                    </h5>
                                    <g:set var="firstTextComponent" value="${sFeaturedArticle.getFirstTextComponent()?.component?.getAsComponent() as canon.model.comp.TextComp}" />
                                    <p>${raw(ContentUtils.truncate(sFeaturedArticle?.deck ?: firstTextComponent?.getContent()))}</p>
                                </div>
                            </div>
                            <!-- End Post Item -->
                        </div>
                    </g:if>
                    <g:if test="${compFeaturedArticles.getFeaturedList()?.size() > 2}">
                        <g:set var="tFeaturedArticle" value="${compFeaturedArticles.getFeaturedList().get(2)}" />
                        <div class="col-md-4 col-sm-5">
                            <!-- Start Post Item -->
                            <div class="post-item match-height">
                                <span class="badge left gray">${tFeaturedArticle.getMainCategory(segment as String)?.translation?.name ?: tFeaturedArticle.getMainCategory(segment as String)?.name}</span>
                                <div class="wrap-image bg-img-post">
                                    <g:set var="articleBgPos" value="${tFeaturedArticle.getBanner().getFeaturedSmallX() ? 'background-position: ' + tFeaturedArticle.getBanner().getFeaturedSmallX() + '%;' : ''}" />
                                    <g:link mapping="${ compFeaturedArticles.contentType?.name()?.toLowerCase()+'Details' }" id="${tFeaturedArticle.slug}" params="${params}">
                                        <img src="${tFeaturedArticle.getBanner()?.getUrl()}" alt="${tFeaturedArticle?.headline}" />
                                    </g:link>
                                </div>
                                <div class="content text-left">
                                    <h5 class="title">
                                        <g:link mapping="${ compFeaturedArticles.contentType?.name()?.toLowerCase()+'Details' }" id="${tFeaturedArticle.slug}" params="${params}">
                                            ${raw(tFeaturedArticle?.headline)}
                                        </g:link>
                                    </h5>
                                    <strong><g:formatDate date="${tFeaturedArticle.publishedDate}" format="dd MMMM yyyy" /></strong>
                                </div>
                            </div>
                            <!-- End Post Item -->
                        </div>
                    </g:if>
                </div>
            </div>
            <g:if test="${!compFeaturedArticles.getHideViewAll()}">
                <div class="text-right">
                    <g:link mapping="${ compFeaturedArticles.contentType?.name()?.toLowerCase()+'List' }" params="[site: params?.site, segment: segment, category: compFeaturedArticles.category?.slug]" class="btn btn-default btn-sm more">
                        <g:message code="component.FEATURED_ARTICLES.${compFeaturedArticles.contentType?.name()}.viewAll.label" default="View all {0} articles" args="[catName?:'']" />
                        <span class="fa fa-angle-right"></span>
                    </g:link>
                </div>
            </g:if>
        </div>
    </div>
    <!-- End Content -->
    <div class="clearfix"></div>
</g:if>