<!-- Start Content -->
<g:set var="compSlider" value="${component?.getAsComponent() as canon.model.comp.SliderComp}" />
<g:if test="${compSlider?.images}">
    <!-- Start Section Detail -->
    <div class="section">
        <div class="container">
            <!-- Start Features -->
            <h1 class="title-section text-center">${compSlider?.title}</h1>
            <div class="padding-top30">
                <div class="wrap-slide-image">
                    <div class="owl-carousel owl-theme owl-image">
                        <g:each in="${compSlider?.images}" var="image">
                            <div class="item">
                                <img src="${image?.mediaUrl}" alt=""/>
                            </div>
                        </g:each>
                    </div>
                </div>
            </div>
            <!-- End Content -->
        </div>
    </div>
    <!-- End Section Detail -->
    <div class="clearfix"></div>
</g:if>