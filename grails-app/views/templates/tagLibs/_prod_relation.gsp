<g:set var="compProdRelation" value="${component?.getAsComponent() as canon.model.comp.ProdRelationComp}" />
<g:if test="${compProdRelation?.relatedProducts}">
    <div class="section">
        <div class="container">
            <!-- Start Related Products -->
            <h1 class="title-section text-center"><g:message code="product.overview.productRelation.${compProdRelation?.relationType?.name()}.label"/></h1>
            <div class="wrap-carousel-in-small">
                <div class="row carousel-in-small">
                    <g:each in="${compProdRelation?.relatedProducts}" var="relatedProduct">
                        <div class="col-sm-4 col-xs-12">
                            <!-- Start Post Item -->
                            <div class="post-item match-height related">
                                <g:set var="productUrl" value="${createLink([controller: 'product', action: 'show', id: relatedProduct?.slug, params: params])}"/>
                                <div class="wrap-image">
                                    <a href="${productUrl}"><img src="${relatedProduct?.mediaUrl}" alt="" /></a>
                                </div>

                                <div class="content">
                                    <h3 class="title">
                                        <a href="${productUrl}">${relatedProduct?.name}</a>
                                    </h3>
                                </div>
                            </div>
                            <!-- End Post Item -->
                        </div>
                    </g:each>
                </div>
            </div>
            <!-- End Related Products -->
            <g:if test="${compProdRelation?.countRelatedProducts > 3}">
                <div class="text-right margin-top10">
                    <a href="#" class="btn btn-default btn-sm more"><g:message code="product.overview.viewAll.${compProdRelation?.relationType?.name()}.label"/><span class="fa fa-angle-right"></span></a>
                </div>
            </g:if>
        </div>
    </div>
    <div class="clearfix"></div>
</g:if>