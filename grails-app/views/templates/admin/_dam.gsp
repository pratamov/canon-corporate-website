<!-- Start Modal Upload Media -->
<div class="modal inmodal dam-modal" id="dam-modal-1" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated fadeInZ">
            <div class="modal-header">
                <h3 class="modal-title"></h3>
            </div>
            <div class="modal-body padding-bottom10">

                <div class="tabs-container upload">
                    <ul class="nav nav-tabs">
                        <li class="dam-tab dam-list-tab active"><a data-toggle="tab" href="#tab-1"><g:message code="admin.modal.uploadImage.tab.dam.label" default="DAM"/></a></li>
                        <li class="dam-tab dam-upload-tab"><a data-toggle="tab" href="#tab-2"><g:message code="admin.modal.uploadImage.tab.upload.label" default="Upload"/></a></li>
                        <li class="dam-tab dam-external-tab"><a data-toggle="tab" href="#tab-3"><g:message code="admin.modal.uploadImage.tab.link.label" default="Link"/></a></li>
                    </ul>
                    <div class="tab-content min-">
                        <div id="tab-1" class="tab-pane dam-list active">
                            <div class="panel-body">
                                <g:form controller="adminMedia" action="search" method="GET" class="dam-list-form">
                                    <!-- Start Search -->
                                    <label><g:message code="admin.default.search.label" default="Search" /></label>
                                    <div class="input-group search margin-bottom10">
                                        <input name="q" type="text" class="form-control dam-list-q">
                                        <div class="input-group-btn">
                                            <button class="btn btn-default btn-modal"><span class="fa fa-search"></span></button>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="row dam-media-list"></div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="row js-dam-info">
                                                <div class="col-md-12">
                                                    <p>
                                                        <strong>File Name</strong><br />
                                                        <span class="dam-info-name"></span>
                                                    </p>
                                                </div>
                                                <div class="col-md-12">
                                                    <p>
                                                        <strong>File Type</strong><br />
                                                        <span class="dam-info-type"></span>
                                                    </p>
                                                </div>
                                                <div class="col-md-12">
                                                    <p>
                                                        <strong>File Size</strong><br />
                                                        <span class="dam-info-size"></span>
                                                    </p>
                                                </div>
                                                <div class="col-md-12">
                                                    <p>
                                                        <strong>Dimension</strong><br />
                                                        <span class="dam-info-dimension"></span>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </g:form>
                            </div>
                        </div>
                        <div id="tab-2" class="tab-pane dam-upload">
                            <div class="panel-body">
                                <g:form controller="adminMedia" action="upload" method="POST" enctype="multipart/form-data" class="dam-upload-form dropzone">
                                    <input class="dam-upload-form-alt" type="hidden" name="alt" />
                                    <input class="dam-upload-form-copyright" type="hidden" name="copyright" />
                                    <input class="dam-upload-form-folder" type="hidden" name="folder" />
                                    <input class="dam-upload-form-name" type="hidden" name="title" />
                                </g:form>
                                <div class="row margin-top20 dam-upload-props">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label><g:message code="admin.default.name.label" default="Name" /></label>
                                            <input class="form-control dam-upload-name" type="text" placeholder="Type in Name" />
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label><g:message code="admin.modal.uploadImage.copyrightText.label" default="Copyright Text" /></label>
                                            <input class="form-control dam-upload-copyright" type="text" placeholder="Type in Text" />
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label><g:message code="admin.modal.uploadImage.alternativeText.label" default="Alternative Text" /></label>
                                            <input class="form-control dam-upload-alt" type="text" placeholder="Type in Text"/>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label><g:message code="admin.modal.uploadImage.folder.label" default="Folder" /></label>
                                            <select class="select form-control dam-upload-folder" data-placeholder="Select Folder">
                                                <option value="" selected>${message(code: 'admin.modal.uploadImage.folderPlaceholder.label', default: 'Select Folder')}</option>
                                                <option value="Bahamas">Unfiled</option>
                                                <option value="Bahrain">Bahrain</option>
                                                <option value="Bangladesh">Bangladesh</option>
                                                <option value="Barbados">Barbados</option>
                                                <option value="Belarus">Belarus</option>
                                                <option value="Belgium">Belgium</option>
                                                <option value="Belize">Belize</option>
                                                <option value="Benin">Benin</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="tab-3" class="tab-pane dam-external">
                            <g:form controller="adminMedia" action="save" method="POST" class="dam-external-form">
                                <div class="panel-body">
                                    <!-- Start Search -->
                                    <label><g:message code="admin.modal.uploadImage.pasteAFileUrlHere.label" default="Paste a file URL here" /></label>
                                    <div class="input-group margin-bottom10">
                                        <input type="text" class="form-control input-image dam-external-from-url" name="url" />
                                        <input type="hidden" class="form-control dam-external-from-height" name="height" />
                                        <input type="hidden" class="form-control dam-external-from-width" name="width" />
                                        <div class="input-group-btn">
                                            <span class="btn btn-default"><span class="fa fa-link"></span></span>
                                        </div>
                                    </div>
                                    <div class="preview-image-link">
                                        <img id="preview-image" class="img-responsive dam-external-image" alt="">
                                        <div class="content">
                                            ${raw(message(code: 'admin.modal.uploadImage.imagePlaceholder.label',
                                                    default: 'If the URL is correct, you\'ll see the preview here.<br />Large files may take a while to appear.'))}
                                        </div>
                                    </div>
                                </div>

                                <div class="row margin-top20">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label><g:message code="admin.default.name.label" default="Name" /></label>
                                            <input class="form-control dam-external-from-name" type="text" name="title" placeholder="${message(code: 'admin.product.create.typeInName.label', default: 'Type in Name')}"/>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label><g:message code="admin.modal.uploadImage.copyrightText.label" default="Copyright Text" /></label>
                                            <input class="form-control dam-external-from-copyright" type="text" name="copyright" placeholder="${message(code: 'admin.product.create.typeInText.label', default:'Type in Text')}"/>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label><g:message code="admin.modal.uploadImage.alternativeText.label" default="Alternative Text" /></label>
                                            <input class="form-control dam-external-from-alt" type="text" name="alt" placeholder="Type in Text"/>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label><g:message code="admin.modal.uploadImage.folder.label" default="Folder" /></label>
                                            <select class="select form-control dam-external-from-folder" name="folder">
                                                <option value="">${message(code: 'admin.modal.uploadImage.folderPlaceholder.label', default: 'Select Folder')}</option>
                                                <option value="Bahamas">Unfiled</option>
                                                <option value="Bahrain">Bahrain</option>
                                                <option value="Bangladesh">Bangladesh</option>
                                                <option value="Barbados">Barbados</option>
                                                <option value="Belarus">Belarus</option>
                                                <option value="Belgium">Belgium</option>
                                                <option value="Belize">Belize</option>
                                                <option value="Benin">Benin</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </g:form>
                        </div>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal"><g:message code="admin.default.cancel.label" default="Cancel" /></button>
                <button type="button" class="btn btn-primary dam-insert-media"><g:message code="admin.default.confirm.label" default="Confirm" /></button>
            </div>
        </div>
    </div>
</div>
<!-- End Modal Upload Media -->