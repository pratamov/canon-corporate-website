<script id="workflow-list-template" type="x-tmpl-mustache">
    <div class="col-md-3">
        <input type="hidden" name="workflowId" value={{id}}>
        <div class="label-arrow {{#isStart}}active{{/isStart}}">{{name}}</div>
        <div class="form-group margin-bottom20">
            <label><g:message code="admin.workflow.assignTo.label" default="Assign to" /></label>
            {{#isStart}}
                <input type="text" class="form-control" disabled value={{username}} />
            {{/isStart}}
            {{#selectable}}
                <select class="form-control js-user-selector" data-placeholder="${message(code: 'admin.workflow.assignPlaceholder.label', default: 'Select user')}"></select>
            {{/selectable}}
        </div>
        {{#deadline}}
            <div class="form-group">
                <label><g:message code="admin.workflow.deadline.label" default="Deadline" /></label>
                <div class="input-group date">
                    <input type="text" class="form-control">
                    <span class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </span>
                </div>
            </div>
        {{/deadline}}
    </div>
</script>

<script id="dam-list-item-template" type="x-tmpl-mustache">
    <div class="dam-list-item" class="dam-list-item" data-url="{{url}}" data-media-id="{{mediaId}}" data-source="{{source}}"
        data-name="{{name}}" data-type="{{type}}" data-size="{{size}}" data-width="{{width}}" data-height="{{height}}" data-thumbnail="{{thumbnail}}"></div>
</script>

<script id="wrap-cell-list-template" type="x-tmpl-mustache">
    <li>
        <a href="#" class="btn btn-danger btn-sm pull-right js-remove-item"><g:message code="admin.default.remove.label" default="Remove" /></a>
        <h5 class="title js-selected-item" data-id="{{id}}">
            {{#isLink}}
                <a href="{{url}}">
            {{/isLink}}
            {{name}}
            {{#isLink}}
                </a>
            {{/isLink}}
        </h5>
    </li>
</script>

<script id="comment-template" type="x-tmpl-mustache">
    <li>
        <h5 class="title">
            {{username}}
            <em>{{commentDate}}</em>
        </h5>
        {{comment}}
        {{#isAttached}}
            <p><a href="{{attachedUrl}}" class="primary-color">{{attachedFileName}}</a></p>
        {{/isAttached}}
        <div class="action">
            <a href="#" class="link">
                <g:message code="admin.default.edit.label" default="Edit" />
            </a> |
            <a href="#" class="link">
                <g:message code="admin.default.delete.label" default="Delete" />
            </a>
        </div>
    </li>
</script>

<script id="product-image-preview-template" type="x-tmpl-mustache">
    <div class="col-md-2 js-product-image">
        <input type="hidden" name="mediaId" value="{{mediaId}}"/>
        {{#isLabeled}}
            <span class="img-label">{{mediaType}}</span>
        {{/isLabeled}}
        <img src="{{mediaUrl}}" class="img-responsive" width="140" height="140" alt="" />
        <a href="#" class="img-delete js-remove-product-image"><i class="fa fa-trash"></i></a>
    </div>
</script>

<script id="location-table-row-template" type="x-tmpl-mustache">
    <tr class="js-site-{{siteId}}">
        <input type=hidden name="locationId" value="{{siteId}}"/>
        <input type=hidden name="locationPrice" value="{{sitePrice}}"/>
        <input type=hidden name="locationPublishedDate" value="{{sitePublishDate}}"/>
        <input type=hidden name="locationArchivedDate" value="{{siteArchiveDate}}"/>
        <td><a href="#" class="single-link" data-toggle="modal" data-target="#modalLocation" data-title="${message(code: 'admin.modal.editLocation.label', default: 'Edit Location')}">{{siteName}}</a></td>
        <td>
            {{sitePrice}}{{^sitePrice}}-{{/sitePrice}}
        </td>
        <td>
            {{sitePublishDate}}{{^sitePublishDate}}-{{/sitePublishDate}}
        </td>
        <td>
            {{siteArchiveDate}}{{^siteArchiveDate}}-{{/siteArchiveDate}}
        </td>
        <td>
            <a href="#" class="js-delete-location"><span class="fa fa-trash"></span></a>
        </td>
    </tr>
</script>