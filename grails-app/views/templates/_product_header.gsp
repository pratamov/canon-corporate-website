<!-- Start Content -->
<div class="inner-page absolute">
    <div class="blurheader"></div>
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-7 col-sm-4 col-xs-6 hide-small">
                <ol class="breadcrumb">
                    <li>
                        <g:link controller="product" action="category" params="[segment: params?.segment, site: params?.site, lang: params?.lang]">
                            <g:message code="product.label" default="Product"/>
                        </g:link>
                    </li>
                    <li class="sub">
                        <g:link controller="product" action="list" params="[segment: params?.segment, category: productCategory?.category?.parent?.slug, subCategory: productCategory?.category?.slug, site: params?.site, lang: params?.lang]">
                            ${productCategory?.category?.name}
                        </g:link>
                    </li>
                    <li class="active">${product?.name}</li>
                </ol>
            </div>
            <div class="col-md-5 col-sm-8 col-xs-6 text-right">
                <!-- Menus -->
                <div class="wrap-menus open">
                    <a href="#" class="toggle-menus"><g:message code="product.header.selectMenu.label" default="Select Menu"/></a>
                    <ul class="list-inline menus">
                        <li class="${controllerName=='product' && actionName=='show' ? 'active':'' }">
                            <g:link controller="product" action="show" id="${product?.slug}" params="[segment: params?.segment, category: params?.category, subCategory: params?.subCategory, site:params?.site, lang: params?.lang]">
                                <g:message code="product.header.overview.label" default="Overview"/>
                            </g:link>
                        </li>
                        <li class="${controllerName=='product' && actionName=='specification' ? 'active':'' }">
                            <g:link controller="product" action="specification" id="${product?.slug}" params="[segment: params?.segment, category: params?.category, subCategory: params?.subCategory, site:params?.site, lang: params?.lang]">
                                <g:message code="product.header.specification.label" default="Specification"/>
                            </g:link>
                        </li>
                        <li class="${controllerName=='product' && actionName=='compare' ? 'active':'' }">
                            <g:link controller="product" action="compare" id="${product?.slug}" params="[segment: params?.segment, category: params?.category, subCategory: params?.subCategory, site:params?.site, lang: params?.lang]">
                                <g:message code="product.header.compare.label" default="Compare"/>
                            </g:link>
                        </li>
                        <li class="${controllerName=='product' && actionName=='whewhereToBuy' ? 'active':'' }">
                            <g:link controller="product" action="whereToBuy" id="${product?.slug}" params="[segment: params?.segment, category: params?.category, subCategory: params?.subCategory, site:params?.site, lang: params?.lang]">
                                <g:message code="product.header.whereToBuy.label" default="Where to Buy"/>
                            </g:link>
                        </li>
                    </ul>
                </div>
                <!-- Price Info -->
                <ul class="list-inline price-info">
                    <li class="price">
                        <g:if test="${product?.price}">
                            ${formatNumber(number: product?.price, type:'currency', currencyCode: "USD", locale: Locale.US)}
                        </g:if>
                        <g:else>
                            -
                        </g:else>
                        <span>RRP</span></li>
                    <li>
                        <a href="#">
                            <g:message code="product.buy.now.label" default="Buy Now"/>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- End Content -->