<div class="clearfix"></div>

<!-- Start Footer -->
<div class="footer">
    <div class="container">

        <div class="scroll-top show-small">
            <a href="#">
                <span class="fa fa-angle-up"></span>
                Back to top
            </a>
        </div>

        <!-- Start Main Footer -->
        <div class="main-footer">
            <div class="row grid-same">
                <div class="col-md-2 col-sm-3">
                    <!-- Start Widget -->
                    <div class="widget-footer hide-small">
                        <h6 class="title">Company</h6>
                        <ul class="footer-link">
                            <g:if test="${appSettings?.getAt("link-desktop-1-1")?.settingValue}">
                                <li>
                                    <a href="${appSettings?.getAt("link-desktop-1-1")?.settingValue}" target="${appSettings?.getAt("link-desktop-1-1")?.settingValue?.startsWith("http") ? '_blank' : '_self'}" ${appSettings?.getAt("link-desktop-1-1")?.settingValue?.startsWith("http")? raw('rel=\"noopener\"') : ''}>
                                        ${raw(appSettings?.getAt("link-desktop-1-1")?.settingName)}
                                    </a>
                                </li>
                            </g:if>
                            <g:if test="${appSettings?.getAt("link-desktop-1-2")?.settingValue}">
                                <li>
                                    <a href="${appSettings?.getAt("link-desktop-1-2")?.settingValue}" target="${appSettings?.getAt("link-desktop-1-2")?.settingValue?.startsWith("http") ? '_blank' : '_self'}" ${appSettings?.getAt("link-desktop-1-2")?.settingValue?.startsWith("http")? raw('rel=\"noopener\"') : ''}>
                                        ${raw(appSettings?.getAt("link-desktop-1-2")?.settingName)}
                                    </a>
                                </li>
                            </g:if>
                            <g:if test="${appSettings?.getAt("link-desktop-1-3")?.settingValue}">
                                <li>
                                    <a href="${appSettings?.getAt("link-desktop-1-3")?.settingValue}" target="${appSettings?.getAt("link-desktop-1-3")?.settingValue?.startsWith("http") ? '_blank' : '_self'}" ${appSettings?.getAt("link-desktop-1-3")?.settingValue?.startsWith("http")? raw('rel=\"noopener\"') : ''}>
                                        ${raw(appSettings?.getAt("link-desktop-1-3")?.settingName)}
                                    </a>
                                </li>
                            </g:if>
                            <g:if test="${appSettings?.getAt("link-desktop-1-4")?.settingValue}">
                                <li>
                                    <a href="${appSettings?.getAt("link-desktop-1-4")?.settingValue}" target="${appSettings?.getAt("link-desktop-1-4")?.settingValue?.startsWith("http") ? '_blank' : '_self'}" ${appSettings?.getAt("link-desktop-1-4")?.settingValue?.startsWith("http")? raw('rel=\"noopener\"') : ''}>
                                        ${raw(appSettings?.getAt("link-desktop-1-4")?.settingName)}
                                    </a>
                                </li>
                            </g:if>
                            <g:if test="${appSettings?.getAt("link-desktop-1-5")?.settingValue}">
                                <li>
                                    <a href="${appSettings?.getAt("link-desktop-1-5")?.settingValue}" target="${appSettings?.getAt("link-desktop-1-5")?.settingValue?.startsWith("http") ? '_blank' : '_self'}" ${appSettings?.getAt("link-desktop-1-5")?.settingValue?.startsWith("http")? raw('rel=\"noopener\"') : ''}>
                                        ${raw(appSettings?.getAt("link-desktop-1-5")?.settingName)}
                                    </a>
                                </li>
                            </g:if>
                            <g:if test="${appSettings?.getAt("link-desktop-1-6")?.settingValue}">
                                <li>
                                    <a href="${appSettings?.getAt("link-desktop-1-6")?.settingValue}" target="${appSettings?.getAt("link-desktop-1-6")?.settingValue?.startsWith("http") ? '_blank' : '_self'}" ${appSettings?.getAt("link-desktop-1-6")?.settingValue?.startsWith("http")? raw('rel=\"noopener\"') : ''}>
                                        ${raw(appSettings?.getAt("link-desktop-1-6")?.settingName)}
                                    </a>
                                </li>
                            </g:if>
                        </ul>
                    </div>
                    <!-- End Widget -->
                </div>
                <div class="col-md-2 col-sm-3">
                    <!-- Start Widget -->
                    <div class="widget-footer hide-small">
                        <h6 class="title">Products</h6>
                        <ul class="footer-link">
                            <g:if test="${appSettings?.getAt("link-desktop-2-1")?.settingValue}">
                                <li>
                                    <a href="${appSettings?.getAt("link-desktop-2-1")?.settingValue}" target="${appSettings?.getAt("link-desktop-2-1")?.settingValue?.startsWith("http") ? '_blank' : '_self'}" ${appSettings?.getAt("link-desktop-2-1")?.settingValue?.startsWith("http")? raw('rel=\"noopener\"') : ''}>
                                        ${raw(appSettings?.getAt("link-desktop-2-1")?.settingName)}
                                    </a>
                                </li>
                            </g:if>
                            <g:if test="${appSettings?.getAt("link-desktop-2-2")?.settingValue}">
                                <li>
                                    <a href="${appSettings?.getAt("link-desktop-2-2")?.settingValue}" target="${appSettings?.getAt("link-desktop-2-2")?.settingValue?.startsWith("http") ? '_blank' : '_self'}" ${appSettings?.getAt("link-desktop-2-2")?.settingValue?.startsWith("http")? raw('rel=\"noopener\"') : ''}>
                                        ${raw(appSettings?.getAt("link-desktop-2-2")?.settingName)}
                                    </a>
                                </li>
                            </g:if>
                            <g:if test="${appSettings?.getAt("link-desktop-2-3")?.settingValue}">
                                <li>
                                    <a href="${appSettings?.getAt("link-desktop-2-3")?.settingValue}" target="${appSettings?.getAt("link-desktop-2-3")?.settingValue?.startsWith("http") ? '_blank' : '_self'}" ${appSettings?.getAt("link-desktop-2-3")?.settingValue?.startsWith("http")? raw('rel=\"noopener\"') : ''}>
                                        ${raw(appSettings?.getAt("link-desktop-2-3")?.settingName)}
                                    </a>
                                </li>
                            </g:if>
                            <g:if test="${appSettings?.getAt("link-desktop-2-4")?.settingValue}">
                                <li>
                                    <a href="${appSettings?.getAt("link-desktop-2-4")?.settingValue}" target="${appSettings?.getAt("link-desktop-2-4")?.settingValue?.startsWith("http") ? '_blank' : '_self'}" ${appSettings?.getAt("link-desktop-2-4")?.settingValue?.startsWith("http")? raw('rel=\"noopener\"') : ''}>
                                        ${raw(appSettings?.getAt("link-desktop-2-4")?.settingName)}
                                    </a>
                                </li>
                            </g:if>
                            <g:if test="${appSettings?.getAt("link-desktop-2-5")?.settingValue}">
                                <li>
                                    <a href="${appSettings?.getAt("link-desktop-2-5")?.settingValue}" target="${appSettings?.getAt("link-desktop-2-5")?.settingValue?.startsWith("http") ? '_blank' : '_self'}" ${appSettings?.getAt("link-desktop-2-5")?.settingValue?.startsWith("http")? raw('rel=\"noopener\"') : ''}>
                                        ${raw(appSettings?.getAt("link-desktop-2-5")?.settingName)}
                                    </a>
                                </li>
                            </g:if>
                            <g:if test="${appSettings?.getAt("link-desktop-2-6")?.settingValue}">
                                <li>
                                    <a href="${appSettings?.getAt("link-desktop-2-6")?.settingValue}" target="${appSettings?.getAt("link-desktop-2-6")?.settingValue?.startsWith("http") ? '_blank' : '_self'}" ${appSettings?.getAt("link-desktop-2-6")?.settingValue?.startsWith("http")? raw('rel=\"noopener\"') : ''}>
                                        ${raw(appSettings?.getAt("link-desktop-2-6")?.settingName)}
                                    </a>
                                </li>
                            </g:if>
                        </ul>
                    </div>
                    <!-- End Widget -->
                </div>
                <div class="col-md-2 col-sm-3">
                    <!-- Start Widget -->
                    <div class="widget-footer hide-small">
                        <h6 class="title">Services</h6>
                        <ul class="footer-link">
                            <g:if test="${appSettings?.getAt("link-desktop-3-1")?.settingValue}">
                                <li>
                                    <a href="${appSettings?.getAt("link-desktop-3-1")?.settingValue}" target="${appSettings?.getAt("link-desktop-3-1")?.settingValue?.startsWith("http") ? '_blank' : '_self'}" ${appSettings?.getAt("link-desktop-3-1")?.settingValue?.startsWith("http")? raw('rel=\"noopener\"') : ''}>
                                        ${raw(appSettings?.getAt("link-desktop-3-1")?.settingName)}
                                    </a>
                                </li>
                            </g:if>
                            <g:if test="${appSettings?.getAt("link-desktop-3-2")?.settingValue}">
                                <li>
                                    <a href="${appSettings?.getAt("link-desktop-3-2")?.settingValue}" target="${appSettings?.getAt("link-desktop-3-2")?.settingValue?.startsWith("http") ? '_blank' : '_self'}" ${appSettings?.getAt("link-desktop-3-2")?.settingValue?.startsWith("http")? raw('rel=\"noopener\"') : ''}>
                                        ${raw(appSettings?.getAt("link-desktop-3-2")?.settingName)}
                                    </a>
                                </li>
                            </g:if>
                            <g:if test="${appSettings?.getAt("link-desktop-3-3")?.settingValue}">
                                <li>
                                    <a href="${appSettings?.getAt("link-desktop-3-3")?.settingValue}" target="${appSettings?.getAt("link-desktop-3-3")?.settingValue?.startsWith("http") ? '_blank' : '_self'}" ${appSettings?.getAt("link-desktop-3-3")?.settingValue?.startsWith("http")? raw('rel=\"noopener\"') : ''}>
                                        ${raw(appSettings?.getAt("link-desktop-3-3")?.settingName)}
                                    </a>
                                </li>
                            </g:if>
                            <g:if test="${appSettings?.getAt("link-desktop-3-4")?.settingValue}">
                                <li>
                                    <a href="${appSettings?.getAt("link-desktop-3-4")?.settingValue}" target="${appSettings?.getAt("link-desktop-3-4")?.settingValue?.startsWith("http") ? '_blank' : '_self'}" ${appSettings?.getAt("link-desktop-3-4")?.settingValue?.startsWith("http")? raw('rel=\"noopener\"') : ''}>
                                        ${raw(appSettings?.getAt("link-desktop-3-4")?.settingName)}
                                    </a>
                                </li>
                            </g:if>
                            <g:if test="${appSettings?.getAt("link-desktop-3-5")?.settingValue}">
                                <li>
                                    <a href="${appSettings?.getAt("link-desktop-3-5")?.settingValue}" target="${appSettings?.getAt("link-desktop-3-5")?.settingValue?.startsWith("http") ? '_blank' : '_self'}" ${appSettings?.getAt("link-desktop-3-5")?.settingValue?.startsWith("http")? raw('rel=\"noopener\"') : ''}>
                                        ${raw(appSettings?.getAt("link-desktop-3-5")?.settingName)}
                                    </a>
                                </li>
                            </g:if>
                        </ul>
                    </div>
                    <!-- End Widget -->
                </div>
                <div class="col-md-2 col-sm-3">
                    <!-- Start Widget -->
                    <div class="widget-footer hide-small">
                        <h6 class="title">Warranty</h6>
                        <ul class="footer-link">
                            <g:if test="${appSettings?.getAt("link-desktop-4-1")?.settingValue}">
                                <li>
                                    <a href="${appSettings?.getAt("link-desktop-4-1")?.settingValue}" target="${appSettings?.getAt("link-desktop-4-1")?.settingValue?.startsWith("http") ? '_blank' : '_self'}" ${appSettings?.getAt("link-desktop-4-1")?.settingValue?.startsWith("http")? raw('rel=\"noopener\"') : ''}>
                                        ${raw(appSettings?.getAt("link-desktop-4-1")?.settingName)}
                                    </a>
                                </li>
                            </g:if>
                            <g:if test="${appSettings?.getAt("link-desktop-4-2")?.settingValue}">
                                <li>
                                    <a href="${appSettings?.getAt("link-desktop-4-2")?.settingValue}" target="${appSettings?.getAt("link-desktop-4-2")?.settingValue?.startsWith("http") ? '_blank' : '_self'}" ${appSettings?.getAt("link-desktop-4-2")?.settingValue?.startsWith("http")? raw('rel=\"noopener\"') : ''}>
                                        ${raw(appSettings?.getAt("link-desktop-4-2")?.settingName)}
                                    </a>
                                </li>
                            </g:if>
                            <g:if test="${appSettings?.getAt("link-desktop-4-3")?.settingValue}">
                                <li>
                                    <a href="${appSettings?.getAt("link-desktop-4-3")?.settingValue}" target="${appSettings?.getAt("link-desktop-4-3")?.settingValue?.startsWith("http") ? '_blank' : '_self'}" ${appSettings?.getAt("link-desktop-4-3")?.settingValue?.startsWith("http")? raw('rel=\"noopener\"') : ''}>
                                        ${raw(appSettings?.getAt("link-desktop-4-3")?.settingName)}
                                    </a>
                                </li>
                            </g:if>
                        </ul>
                    </div>
                    <!-- End Widget -->
                </div>
                <div class="col-md-2 col-sm-3">
                    <!-- Start Widget -->
                    <div class="widget-footer hide-small">
                        <h6 class="title">Campaign</h6>
                        <ul class="footer-link">
                            <g:if test="${appSettings?.getAt("link-desktop-5-1")?.settingValue}">
                                <li>
                                    <a href="${appSettings?.getAt("link-desktop-5-1")?.settingValue}" target="${appSettings?.getAt("link-desktop-5-1")?.settingValue?.startsWith("http") ? '_blank' : '_self'}" ${appSettings?.getAt("link-desktop-5-1")?.settingValue?.startsWith("http")? raw('rel=\"noopener\"') : ''}>
                                        ${raw(appSettings?.getAt("link-desktop-5-1")?.settingName)}
                                    </a>
                                </li>
                            </g:if>
                            <g:if test="${appSettings?.getAt("link-desktop-5-2")?.settingValue}">
                                <li>
                                    <a href="${appSettings?.getAt("link-desktop-5-2")?.settingValue}" target="${appSettings?.getAt("link-desktop-5-2")?.settingValue?.startsWith("http") ? '_blank' : '_self'}" ${appSettings?.getAt("link-desktop-5-2")?.settingValue?.startsWith("http")? raw('rel=\"noopener\"') : ''}>
                                        ${raw(appSettings?.getAt("link-desktop-5-2")?.settingName)}
                                    </a>
                                </li>
                            </g:if>
                        </ul>
                    </div>
                    <!-- End Widget -->
                </div>
                <div class="col-md-2 col-sm-3">
                    <!-- Start Widget -->
                    <div class="widget-footer">
                        <h6 class="title hide-small">Subscribe</h6>
                        <ul class="grid-link">
                            <g:if test="${appSettings?.getAt("facebook")?.settingValue}">
                                <li>
                                    <a href="${appSettings?.getAt("facebook")?.settingValue}" target="${appSettings?.getAt("facebook")?.settingValue?.startsWith("http") ? '_blank' : '_self'}" ${appSettings?.getAt("facebook")?.settingValue?.startsWith("http")? raw('rel=\"noopener\"') : ''}>
                                        <span class="fa fa-facebook"></span>
                                    </a>
                                </li>
                            </g:if>
                            <g:if test="${appSettings?.getAt("facebook")?.settingValue}">
                                <li>
                                    <a href="${appSettings?.getAt("instagram")?.settingValue}" target="${appSettings?.getAt("instagram")?.settingValue?.startsWith("http") ? '_blank' : '_self'}" ${appSettings?.getAt("instagram")?.settingValue?.startsWith("http")? raw('rel=\"noopener\"') : ''}>
                                        <span class="fa fa-instagram"></span>
                                    </a>
                                </li>
                            </g:if>
                            <g:if test="${appSettings?.getAt("facebook")?.settingValue}">
                                <li>
                                    <a href="${appSettings?.getAt("youtube")?.settingValue}" target="${appSettings?.getAt("youtube")?.settingValue?.startsWith("http") ? '_blank' : '_self'}" ${appSettings?.getAt("youtube")?.settingValue?.startsWith("http")? raw('rel=\"noopener\"') : ''}>
                                        <span class="fa fa-youtube"></span>
                                    </a>
                                </li>
                            </g:if>
                            <g:if test="${appSettings?.getAt("twitter")?.settingValue}">
                                <li>
                                    <a href="${appSettings?.getAt("twitter")?.settingValue}" target="${appSettings?.getAt("twitter")?.settingValue?.startsWith("http") ? '_blank' : '_self'}" ${appSettings?.getAt("twitter")?.settingValue?.startsWith("http")? raw('rel=\"noopener\"') : ''}>
                                        <span class="fa fa-twitter"></span>
                                    </a>
                                </li>
                            </g:if>
                        </ul>
                    </div>
                    <!-- End Widget -->
                </div>
            </div>
        </div>
        <!-- End Main Footer -->

        <div class="scroll-top hide-small">
            <a href="#">
                <span class="fa fa-angle-up"></span>
                Back to top
            </a>
        </div>

        <!-- Start Sub Footer -->
        <div class="sub-footer">
            <div class="row">
                <div class="col-md-6">
                    Copyright <span class="fa fa-copyright"></span> 2017 Canon. All rights reserved.
                </div>
                <!-- <div class="col-md-6 text-right hide-small">
                        Singapore <img src="images/singapore.png" width="30" class="img-circle" alt="">
                    </div> -->
            </div>
        </div>
        <!-- End Sub Footer -->
    </div>
</div>
<!-- End Footer -->
