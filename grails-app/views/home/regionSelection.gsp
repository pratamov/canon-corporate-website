<%--
  Created by IntelliJ IDEA.
  User: muyalif
  Date: 02/01/18
  Time: 15.15
--%>

<%@ page import="org.json.JSONObject" contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title><g:message code="regionSelector.title.label" default="Canon - Region Selector"/></title>
    <meta name="layout" content="canon"/>
</head>

<body>
    <!-- Start Wrap Component -->
    <div class="wrap-component">
        <!-- Start Page Info -->
        <div class="page-info">
            <div class="container">
                <g:message code="regionSelector.label" default="Region selector"/>
            </div>
        </div>
        <!-- End Page Info -->

        <div class="clearfix"></div>

        <!-- Start Content -->
        <div class="section">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <div class="overview margin-top40">
                            <h1 class="title">
                                <g:message code="regionSelector.selectYourRegional.label" default="Select Your Regional"/>
                            </h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Content -->

        <!-- Start Section -->
    <div class="section">
        <div class="wrap-contact margin-top-min50">
            <div class="container">
                <div class="row">
                    <div class="col-sm-10 col-sm-offset-1">
                        <!-- Start Box Panel -->
                        <div class="box-panel region">
                            <div class="bg" data-image="${asset.assetPath(src: 'bg/world.png')}"></div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="select-regional">
                                        <h4 class="title">${asiaRegion?.getString('name')}</h4>
                                        <ul>
                                            <g:if test="${asiaRegion?.getJSONArray('languages')?.length() > 0}">
                                                <g:each in="${(0..< asiaRegion?.getJSONArray('languages')?.length())}" var="i">
                                                    <g:set var="language" value="${asiaRegion?.getJSONArray('languages')?.getJSONObject(i)}"/>
                                                    <li><a href="${language?.getString('url')}" class="single-link">${language?.getString('name')}</a></li>
                                                </g:each>
                                            </g:if>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Box Panel -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Section -->

    <!-- Start Section -->
    <div class="section">
        <div class="container">
            <div class="row grid-same">
                <g:each in="${otherRegions}" var="region">
                    <div class="col-sm-4">
                        <div class="select-regional">
                            <h4 class="title">${region?.getString('name')}</h4>
                            <ul>
                                <g:if test="${region?.getJSONArray('languages')?.length() > 0}">
                                    <g:each in="${(0..< region?.getJSONArray('languages')?.length())}" var="j">
                                        <g:set var="language" value="${region?.getJSONArray('languages')?.getJSONObject(j)}"/>
                                        <li><a href="${language?.getString('url')}" class="single-link">${language?.getString('name')}</a></li>
                                    </g:each>
                                </g:if>
                            </ul>
                        </div>
                    </div>
                </g:each>
            </div>
        </div>
    </div>
    <!-- End Section -->
    </div>
    <!-- End Wrap Component -->
</body>
</html>