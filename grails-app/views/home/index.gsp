<%--
  Created by IntelliJ IDEA.
  User: muyalif
  Date: 29/11/17
  Time: 11.17
--%>

<%@ page import="canon.PageComponent" contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title><g:message code="home.title.label" default="Canon - Home"/></title>
    <meta name="layout" content="canon"/>
</head>

<body>
<!-- Start Wrap Component -->
<div class="wrap-component">
    <!-- Start Home -->
    <g:each in="${pageComponents as List<canon.PageComponent>}" var="pc" status="i">
        <g:renderComp component="${pc.component}" site="${params?.site}" segment="${params?.segment}" />
    </g:each>
</div>
<!-- End Wrap Component -->
</body>
</html>