<%--
  Created by IntelliJ IDEA.
  User: muyalif
  Date: 06/12/17
  Time: 10.14
--%>
<%@ page import="canon.Product" contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>
        <g:message code="product.list.title" default="Canon - Product List"/>
    </title>
    <meta name="layout" content="canon">
</head>
<body>
<!-- Start Wrap PageComponent -->
<div class="wrap-component">
    <!-- Start Menu Category -->
    <div class="menu-category">
        <div class="container">

            <!-- Start Owl menu -->
            <div class="wrap-owl-menu">
                <div class="owl-menu owl-carousel">
                    <g:each in="${subCategories}" var="subCategory">
                        <div class="item ${subCategory?.slug == category?.slug ? 'active' : ''}">
                            <div class="content match-height">
                                <div class="wrap-image">
                                    <img src="${subCategory?.defaultMedia?.mediaUrl}" alt="">
                                </div>
                                <h5 class="title">${subCategory?.name}</h5>
                                <g:link class="link" controller="product" action="list"
                                        params="[segment: params?.segment, category: subCategory?.slug, site: params?.site, lang: params?.lang]" />
                            </div>
                        </div>
                    </g:each>
                </div>
            </div>
            <!-- End Owl menu -->

        </div>
    </div>
    <!-- End Menu Category -->

    <!-- Start Slide Banner -->
    <div class="slide-banner bg" data-image="${category?.backgroundUrl}" id="home">
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <!-- Start Carousel -->
                    <div class="wrap-carousel">
                        <div class="owl-carousel owl-home">
                            <g:each in="${featuredProducts as List<Product>}" var="featuredProduct">
                                <div class="item">
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <h1 class="font-light">
                                                ${featuredProduct?.name}
                                            </h1>
                                            <p>
                                                ${featuredProduct?.description}
                                            </p>
                                        </div>
                                        <div class="col-xs-6 text-center">
                                            <img src="${featuredProduct?.defaultMedia?.media?.url}" class="img-responsive" alt=""/>
                                        </div>
                                    </div>
                                </div>
                            </g:each>
                        </div>
                    </div>
                    <!-- End Carousel -->
                </div>
            </div>
        </div>
    </div>
    <!-- End Slide Banner -->

    <!-- Start Content -->
    <div class="section gray">
        <div class="container">
            <div class="row">
                <div class="col-md-3 sidebar">
                    <div class="content">
                        <g:form controller="product" action="list" method="GET">
                            <g:hiddenField name="max" value="${params?.max}"/>
                            <g:hiddenField name="page" value="${params?.page}"/>
                            <g:hiddenField name="site" value="${params?.site}"/>
                            <g:hiddenField name="lang" value="${params?.lang}"/>
                            <g:hiddenField name="segment" value="${params?.segment}"/>
                            <g:hiddenField name="category" value="${params?.category}"/>
                        %{--<!-- Start Widget -->
                        <div class="widget" id="view" data-toggle=".toggle-view">
                            <a class="close" href="#"><span class="fa fa-times"></span></a>
                            <h6 class="title">View By</h6>
                            <ul class="list-justify">
                                <li class="active"><a href="#"><sapn class="fa fa-th-large"></sapn></a></li>
                                <li><a href="#"><sapn class="fa fa-th-list"></sapn></a></li>
                            </ul>
                        </div>
                        <!-- End Widget -->--}%
                            <!-- Start Widget -->
                            <div class="widget" id="sort" data-toggle=".toggle-sort">
                                <a class="close" href="#"><span class="fa fa-times"></span></a>
                                <h6 class="title">
                                    <g:message code="product.subcategory.sortBy.label" default="Sort by"/>
                                </h6>
                                <select name="sort" class="form-control select input-sm margin-bottom20 margin-top20 js-auto-submit">
                                    <g:each in="${canon.Product.ES_SORTABLE_FIELDS}" var="sortableField">
                                        <option value="${sortableField.key}" ${(params?.sort && params?.sort == sortableField.key || !params?.sort && 'az' == sortableField.key) ? 'selected' : '' }>
                                            <g:message code="product.list.sort.${sortableField.key}.label" default="${sortableField.key}"/>
                                        </option>
                                    </g:each>
                                </select>
                                %{--<select class="form-control select input-sm margin-bottom5">
                                    <option value="">Select</option>
                                    <option value="1">Lorem Ipsum</option>
                                    <option value="2">Lorem Ipsum</option>
                                </select>--}%
                            </div>
                            <!-- End Widget -->
                            <!-- Start Widget -->
                            <div class="widget" id="filter" data-toggle=".toggle-filter">
                                <a class="close" href="#"><span class="fa fa-times"></span></a>
                                <h6 class="title">Filter by</h6>

                                <g:set var="aggsCategories" value="${aggs?.get('categories')}" />
                                <g:if test="${aggsCategories?.size() > 0}">
                                    <p class="sub-title">Range</p>
                                    <ul class="clear-list toggle-more" id="range">
                                        <g:each in="${aggsLevels}" var="ac">
                                            <g:set var="esCat" value="${ac.value}" />
                                            <li>
                                                <div class="checkbox">
                                                    <input type="checkbox" name="range" value="${ac.key}" id="${ac.key}" class="js-auto-submit" ${ac.key == params?.long("range") ? 'checked' : ''} />
                                                    <label for="${ac.key}">
                                                        ${esCat.name}
                                                    </label>
                                                </div>
                                            </li>
                                        </g:each>
                                    </ul>
                                </g:if>

                                <g:set var="aggsLevels" value="${aggs?.get('product_levels')}" />
                                <g:if test="${aggsLevels?.size() > 0}">
                                    <p class="sub-title">Level</p>
                                    <ul class="clear-list toggle-more" id="level">
                                        <g:each in="${aggsLevels}" var="al">
                                            <li>
                                                <div class="checkbox">
                                                    <input type="checkbox" name="productLevel" value="${al.key}" id="${al.key}" class="js-auto-submit" ${al.key == params?.productLevel ? 'checked' : ''} />
                                                    <label for="${al.key}">
                                                        <g:message code="canon.enums.ProductLevel.${al.key}.label" default="" />
                                                    </label>
                                                </div>
                                            </li>
                                        </g:each>
                                    </ul>
                                </g:if>

                                <p class="sub-title">Price</p>
                                <ul class="clear-list toggle-more" id="price">
                                    <g:each in="${Product.PRICE_RANGE}" var="pr">
                                        <li>
                                            <div class="checkbox">
                                                <input id="price-${pr.key}" type="checkbox" name="price" value="${pr.key}" class="js-auto-submit" ${pr.key == params?.int('price') ? 'checked' : ''} />
                                                <label for="price-${pr.key}">
                                                    $${pr.value?.low} — $${pr.value?.high}
                                                </label>
                                            </div>
                                        </li>
                                    </g:each>
                                </ul>

                                %{--<p class="sub-title margin-top10">Megapixel</p>
                                <ul class="clear-list toggle-more" id="megapixel">
                                    <li>
                                        <div class="checkbox">
                                            <input id="checkbox11" type="checkbox">
                                            <label for="checkbox11">
                                                18.0 — 28.0 MP
                                            </label>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="checkbox">
                                            <input id="checkbox12" type="checkbox">
                                            <label for="checkbox12">
                                                28.1 — 38.0 MP
                                            </label>
                                        </div>
                                    </li>
                                </ul>

                                <p class="sub-title margin-top10">ISO</p>
                                <ul class="clear-list toggle-more" id="iso">
                                    <li>
                                        <div class="checkbox">
                                            <input id="checkbox16" type="checkbox">
                                            <label for="checkbox16">
                                                100 — 3,200
                                            </label>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="checkbox">
                                            <input id="checkbox17" type="checkbox">
                                            <label for="checkbox17">
                                                100 — 6,400
                                            </label>
                                        </div>
                                    </li>
                                </ul>--}%
                            </div>
                            <!-- End Widget -->
                        </g:form>
                    </div>
                </div>
                <div class="col-md-9 main">
                    <div class="content">
                        <!-- Start Btn Toggle -->
                        <div class="btn-filter">
                            <a href="#" class="btn btn-default btn-sm toggle-filter">
                                Filter By
                            </a>
                            <a href="#" class="btn btn-default btn-sm toggle-sort">
                                <g:message code="product.subcategory.sortBy.label" default="Sort by"/>
                            </a>
                            %{--<a href="#" class="btn btn-default btn-sm toggle-view">
                                View By
                            </a>--}%
                        </div>
                        <!-- End Btn Toggle -->

                        <!-- Start Row -->
                        <div class="row">
                                <!-- Start Post Item -->
                                <g:each in="${resultList}" var="prd">
                                    <div class="col-md-4 col-sm-6 col-xs-12">
                                        <div class="post-item match-height border">
                                            <span class="badge stroke small left"><g:message code="canon.enums.ProductLevel.${prd?.productLevel}.label" default="" /></span>
                                            <g:if test="${prd?.popular}">
                                                <span class="badge small right"><g:message code="product.list.popular.label" default="Popular" /></span>
                                            </g:if>
                                            <img src="${prd?.banner?.url}" alt="" />
                                            <div class="content">
                                                <div class="equal-height">
                                                    <h5 class="title">
                                                        <g:link controller="product" action="show" id="${prd?.slug}" params="[segment: params?.segment, category: params?.category, subCategory: params?.subCategory, site: params?.site, lang: params?.lang]">
                                                            ${prd?.name}
                                                        </g:link>
                                                    </h5>
                                                    <p class="margin-bottom0">
                                                        <g:each in="${prd?.specifications}" var="spec" status="i">
                                                            ${spec?.specValue} ${i < prd?.specifications?.size() - 1 ? raw('<br>') : ''}
                                                        </g:each>
                                                    </p>
                                                </div>
                                                <hr />
                                                <h4 class="price">
                                                    <g:formatNumber number="${prd?.price}" currencyCode="USD" type="currency" locale="${java.util.Locale.US}"/>
                                                </h4>
                                            </div>
                                        </div>
                                    </div>
                                </g:each>
                                <!-- End Post Item -->
                        </div>
                        <!-- End Row -->
                    </div>
                    <!-- Start Pagination -->
                    <g:if test="${resultCount > params?.max}">
                        <g:set var="currentPage" value="${params?.page ?: 1}"/>
                        <g:set var="totalPage" value="${java.lang.Math.ceil(resultCount/params?.max) as java.lang.Integer}"/>
                        <div class="post-pagination">
                            <ul>
                                <li class="nav">
                                    <g:if test="${currentPage == 1}">
                                        <span class="fa fa-angle-left"></span>
                                    </g:if>
                                    <g:else>
                                        <g:link controller="product" action="list" params="${params - [page: params?.page] + [page: currentPage-1]}">
                                            <span class="fa fa-angle-left"></span>
                                        </g:link>
                                    </g:else>
                                </li>
                                <li class="page">${currentPage}</li>
                                <li><g:message code="product.list.pagination.separator.label" default="of"/></li>
                                <li class="count">${totalPage}</li>
                                <li class="nav">
                                    <g:if test="${currentPage == totalPage}">
                                        <span class="fa fa-angle-right"></span>
                                    </g:if>
                                    <g:else>
                                        <g:link controller="product" action="list" params="${params - [page: params?.page] + [page: currentPage+1]}">
                                            <span class="fa fa-angle-right"></span>
                                        </g:link>
                                    </g:else>
                                </li>
                            </ul>
                        </div>
                    </g:if>
                    <!-- End Pagination -->
                    <g:render template="/templates/product_footer"/>
                </div>
            </div>
        </div>
    </div>
    <!-- End Content -->
</div>
<!-- End Wrap PageComponent -->
</body>
</html>