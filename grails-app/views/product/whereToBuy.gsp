<%--
  Created by IntelliJ IDEA.
  User: muyalif
  Date: 05/12/17
  Time: 17.00
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title></title>
    <meta name="layout" content="canon"/>
</head>

<body>
    <!-- Start Wrap Component -->
    <div class="wrap-component">
        <!-- Start Page Info -->
        <div class="page-info">
            <div class="container">
                Where to buy
            </div>
        </div>
        <!-- End Page Info -->

        <div class="clearfix"></div>

        <!-- Start Content -->
        <div class="wtb">
            <div class="container">
                <h1 class="title">Where to buy</h1>
                <p class="desc">Search for product and where to buy</p>
                <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAYAAACM/rhtAAAAHElEQVRYhe3BAQEAAACCIP+vbkhAAQAAAAAAfBoZKAABpFjcSAAAAABJRU5ErkJggg=="/>
                <div class="row form-locations">
                    <div class="col-sm-4">
                        <select class="form-control select">
                            <option value="1">imageCLASS MF631Cn</option>
                            <option value="2">imageCLASS MF631Cn</option>
                            <option value="3">imageCLASS MF631Cn</option>
                            <option value="4">imageCLASS MF631Cn</option>
                            <option value="5">imageCLASS MF631Cn</option>
                        </select>
                    </div>
                    <div class="col-sm-5">
                        <select class="form-control select">
                            <option value="">All</option>
                            <option value="1">Lorem Ipsum</option>
                            <option value="2">Lorem Ipsum</option>
                        </select>
                    </div>
                    <div class="col-sm-3">
                        <button class="btn btn-primary btn-block">Search Now</button>
                    </div>
                </div>
            </div>

            <!-- Start Mobile Tab Location -->
            <ul class="nav-location">
                <li class="active"><a href="#list-location">List</a></li>
                <li><a href="#map-location">Map</a></li>
            </ul>
            <!-- End Tab Location -->

            <div class="container">
                <!-- Start Locations -->
                <div class="wrap-locations">
                    <div class="row">
                        <div class="col-sm-4 match-height content-location in" id="list-location">
                            <div class="input-location">
                                <select class="form-control select">
                                    <option value="">Dealers</option>
                                    <option value="1">Lorem Ipsum</option>
                                    <option value="2">Lorem Ipsum</option>
                                </select>
                            </div>
                            <!-- Start Wrap Item Location -->
                            <div class="wrap-item-location">
                                <div class="item-location"
                                     data-lat="-6.921167"
                                     data-lng="107.610467">
                                    <span class="badge">1</span>
                                    <h6 class="title-item">Aik Seng Photo</h6>
                                    <p class="desc">
                                        1 Rochor Canal Road<br />
                                        #01-48 Sim Lim Square<br />
                                        Singapore 188504
                                    </p>
                                </div>
                                <div class="item-location"
                                     data-lat="-6.814008"
                                     data-lng="107.621342">
                                    <span class="badge">2</span>
                                    <h6 class="title-item">Alan Photo Trading</h6>
                                    <p class="desc">
                                        1 Rochor Canal Road<br />
                                        #01-48 Sim Lim Square<br />
                                        Singapore 188504
                                    </p>
                                </div>
                                <div class="item-location"
                                     data-lat="-6.731393"
                                     data-lng="107.430676">
                                    <span class="badge">3</span>
                                    <h6 class="title-item">Alan Photo Trading</h6>
                                    <p class="desc">
                                        252 North Bridge Road #03-21 Singapore 179103<br />
                                        Raffles City Singapore
                                    </p>
                                </div>
                                <div class="item-location"
                                     data-lat="-6.897676"
                                     data-lng="107.655438">
                                    <span class="badge">4</span>
                                    <h6 class="title-item">Bally Photo Electronics Pte Ltd</h6>
                                    <p class="desc">
                                        68 Orchard Road<br />
                                        #04-43 Plaza Singapura<br />
                                        Singapore 238839
                                    </p>
                                </div>
                                <div class="item-location"
                                     data-lat="-6.982361"
                                     data-lng="107.559817">
                                    <span class="badge">5</span>
                                    <h6 class="title-item">Best Denki (Singapore) Pte Ltd</h6>
                                    <p class="desc">
                                        1 Rochor Canal Road<br />
                                        #01-48 Sim Lim Square<br />
                                        Singapore 188504
                                    </p>
                                </div>
                            </div>
                            <!-- End Wrap Item Location -->
                        </div>
                        <div class="col-sm-8 match-height content-location" id="map-location">
                            <div class="maps" id="map"></div>
                        </div>
                    </div>
                </div>
                <!-- End Locations -->
            </div>
        </div>
        <!-- End Content -->

        <!-- Start Content -->
        <div class="section">
            <div class="container">
                <h1 class="title-section text-center">Latest promotion from Canon</h1>
                <div class="row">
                    <div class="col-sm-12">
                        <!-- Start Post Item -->
                        <div class="post-item match-height">
                            <span class="badge right primary">Event</span>
                            <div class="row wrap-vertical-content">
                                <div class="col-sm-6 getheight">
                                    <img src="/assets/product/img4.png" alt="" />
                                </div>
                                <div class="col-sm-6 desc">
                                    <div class="vertical-middle">
                                        <div class="content left-medium">
                                            <h3 class="title">
                                                <a href="#">Canon signature Zoo Photo-walk with julian w.</a>
                                            </h3>
                                            <p>
                                                Lorem ipsum dolor sit amet, invidunt expetendis reprehendunt ex est, usu natum ornatus concludaturque et, no vis recteque reformidans. Iriure volutpat ullamcorper te nec.
                                            </p>
                                            <a href="#" class="btn btn-primary">Read More</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Post Item -->
                    </div>
                    <div class="col-md-8 col-sm-7">
                        <!-- Start Post Item -->
                        <div class="post-item match-height">
                            <span class="badge right primary">Travel</span>
                            <img src="/assets/event/img2.png" alt="" />
                            <div class="content text-left">
                                <h5 class="title"><a href="#">Mongolia Eagle hunting photo-tour (7D6)</a></h5>
                                <p>Amet paulo homero an quo, vis iisque praesent sadipscing ex. Instructior vituperatoribus mel ea, eius dictas consequat ex mea, ferri affert neglegentur in sea.</p>
                            </div>
                        </div>
                        <!-- End Post Item -->
                    </div>
                    <div class="col-md-4 col-sm-5">
                        <!-- Start Post Item -->
                        <div class="post-item match-height">
                            <span class="badge right primary">Workoshops</span>
                            <img src="/assets/event/img3.png" alt="" />
                            <div class="content text-left">
                                <h5 class="title"><a href="#">Photoshop (SFC-eligible)</a></h5>
                                <p>Amet paulo homero an quo, vis iisque praesent sadipscing ex. Instructior vituperatoribus mel ea, eius dictas consequat ex mea.</p>
                            </div>
                        </div>
                        <!-- End Post Item -->
                    </div>
                </div>
            </div>
        </div>
        <!-- End Content -->
    </div>
    <!-- End Wrap Component -->
</body>
</html>