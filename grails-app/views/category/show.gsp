

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title></title>
    <meta name="layout" content="canon">
</head>
<body>
    <!-- Start Wrap PageComponent -->
    <div class="wrap-component">
        <!-- Start Menu Category -->
        <div class="menu-category">
            <div class="container">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="owl-menu owl-carousel">
                            <g:each in="${subCategoryList}" var="subCategory" status="i">
                                <div class="item ${i == 0 ? 'active' : ''}">
                                    <div class="content match-height">
                                        <img src="${subCategory?.backgroundUrl}" alt="">
                                        <h5 class="title">${subCategory?.name}</h5>
                                        <a href="#" class="link"></a>
                                    </div>
                                </div>
                            </g:each>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Menu Category -->

        <!-- Start Slide Banner -->
        <div class="slide-banner bg" data-image="${category?.backgroundUrl}" id="home">
            <div class="overlay"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <!-- Start Carousel -->
                        <div class="wrap-carousel">
                            <div class="owl-carousel owl-home">
                                <div class="item">
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <h1 class="font-light">
                                                Digital SLR (EOS)
                                            </h1>
                                            <p>
                                                Whether a first-time hobbyist or professional photographer, Canon's range of DSLRs puts the most complete camera system in your hands. With leading optical technology, over 60 lenses to match and limitless upgrading options, Canon offers a photography system that gives you the widest imaging options. No wonder it is the number one choice among professional photographers.
                                            </p>
                                        </div>
                                        <div class="col-xs-6 text-center">
                                            <img src="/assets/category/camera.png" class="img-responsive" alt=""/>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <h1 class="font-light">
                                                Digital SLR (EOS)
                                            </h1>
                                            <p>
                                                Whether a first-time hobbyist or professional photographer, Canon's range of DSLRs puts the most complete camera system in your hands. With leading optical technology, over 60 lenses to match and limitless upgrading options, Canon offers a photography system that gives you the widest imaging options. No wonder it is the number one choice among professional photographers.
                                            </p>
                                        </div>
                                        <div class="col-xs-6 text-center">
                                            <img src="/assets/category/camera.png" class="img-responsive" alt=""/>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <h1 class="font-light">
                                                Digital SLR (EOS)
                                            </h1>
                                            <p>
                                                Whether a first-time hobbyist or professional photographer, Canon's range of DSLRs puts the most complete camera system in your hands. With leading optical technology, over 60 lenses to match and limitless upgrading options, Canon offers a photography system that gives you the widest imaging options. No wonder it is the number one choice among professional photographers.
                                            </p>
                                        </div>
                                        <div class="col-xs-6 text-center">
                                            <img src="/assets/category/camera.png" class="img-responsive" alt=""/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Carousel -->
                    </div>
                </div>
            </div>
        </div>
        <!-- End Slide Banner -->

        <!-- Start Content -->
        <div class="section gray">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 sidebar">
                        <div class="content">
                            <!-- Start Widget -->
                            <div class="widget" id="view" data-toggle=".toggle-view">
                                <a class="close" href="#"><span class="fa fa-times"></span></a>
                                <h6 class="title">View By</h6>
                                <ul class="list-justify">
                                    <li class="active"><a href="#"><sapn class="fa fa-th-large"></sapn></a></li>
                                    <li><a href="#"><sapn class="fa fa-th-list"></sapn></a></li>
                                </ul>
                            </div>
                            <!-- End Widget -->
                            <!-- Start Widget -->
                            <div class="widget" id="sort" data-toggle=".toggle-sort">
                                <a class="close" href="#"><span class="fa fa-times"></span></a>
                                <h6 class="title">Sort By</h6>
                                <select class="form-control select input-sm margin-bottom20 margin-top20">
                                    <option value="">Select</option>
                                    <option value="1">Lorem Ipsum</option>
                                    <option value="2">Lorem Ipsum</option>
                                </select>
                                <select class="form-control select input-sm margin-bottom5">
                                    <option value="">Select</option>
                                    <option value="1">Lorem Ipsum</option>
                                    <option value="2">Lorem Ipsum</option>
                                </select>
                            </div>
                            <!-- End Widget -->
                            <!-- Start Widget -->
                            <div class="widget" id="filter" data-toggle=".toggle-filter">
                                <a class="close" href="#"><span class="fa fa-times"></span></a>
                                <h6 class="title">Filter by</h6>
                                <p class="sub-title">Range</p>
                                <ul class="clear-list toggle-more" id="range">
                                    <li>
                                        <div class="checkbox">
                                            <input id="checkbox1" type="checkbox">
                                            <label for="checkbox1">
                                                Default
                                            </label>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="checkbox">
                                            <input id="checkbox2" type="checkbox">
                                            <label for="checkbox2">
                                                Mirrorless (EOS M)
                                            </label>
                                        </div>
                                    </li>
                                </ul>
                                <p class="sub-title">Level</p>
                                <ul class="clear-list toggle-more" id="level">
                                    <li>
                                        <div class="checkbox">
                                            <input id="checkbox3" type="checkbox">
                                            <label for="checkbox3">
                                                Beginner
                                            </label>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="checkbox">
                                            <input id="checkbox4" type="checkbox">
                                            <label for="checkbox4">
                                                Semi-professional
                                            </label>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="checkbox">
                                            <input id="checkbox5" type="checkbox">
                                            <label for="checkbox5">
                                                Professional
                                            </label>
                                        </div>
                                    </li>
                                </ul>

                                <p class="sub-title">Price</p>
                                <ul class="clear-list toggle-more" id="price">
                                    <li>
                                        <div class="checkbox">
                                            <input id="checkbox6" type="checkbox">
                                            <label for="checkbox6">
                                                $0 — $1000
                                            </label>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="checkbox">
                                            <input id="checkbox7" type="checkbox">
                                            <label for="checkbox7">
                                                $1001 — $2000
                                            </label>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="checkbox">
                                            <input id="checkbox8" type="checkbox">
                                            <label for="checkbox8">
                                                $2001 — $3000
                                            </label>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="checkbox">
                                            <input id="checkbox9" type="checkbox">
                                            <label for="checkbox9">
                                                $3001 — $4000
                                            </label>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="checkbox">
                                            <input id="checkbox10" type="checkbox">
                                            <label for="checkbox10">
                                                $4001 — $5000
                                            </label>
                                        </div>
                                    </li>
                                </ul>

                                <p class="sub-title margin-top10">Megapixel</p>
                                <ul class="clear-list toggle-more" id="megapixel">
                                    <li>
                                        <div class="checkbox">
                                            <input id="checkbox11" type="checkbox">
                                            <label for="checkbox11">
                                                18.0 — 28.0 MP
                                            </label>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="checkbox">
                                            <input id="checkbox12" type="checkbox">
                                            <label for="checkbox12">
                                                28.1 — 38.0 MP
                                            </label>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="checkbox">
                                            <input id="checkbox13" type="checkbox">
                                            <label for="checkbox13">
                                                38.1 — 48.0 MP
                                            </label>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="checkbox">
                                            <input id="checkbox14" type="checkbox">
                                            <label for="checkbox14">
                                                48.1 — 58.0 MP
                                            </label>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="checkbox">
                                            <input id="checkbox15" type="checkbox">
                                            <label for="checkbox15">
                                                58.1 — 68.0 MP
                                            </label>
                                        </div>
                                    </li>
                                </ul>

                                <p class="sub-title margin-top10">ISO</p>
                                <ul class="clear-list toggle-more" id="iso">
                                    <li>
                                        <div class="checkbox">
                                            <input id="checkbox16" type="checkbox">
                                            <label for="checkbox16">
                                                100 — 3,200
                                            </label>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="checkbox">
                                            <input id="checkbox17" type="checkbox">
                                            <label for="checkbox17">
                                                100 — 6,400
                                            </label>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="checkbox">
                                            <input id="checkbox18" type="checkbox">
                                            <label for="checkbox18">
                                                100 — 16,000
                                            </label>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="checkbox">
                                            <input id="checkbox19" type="checkbox">
                                            <label for="checkbox19">
                                                100 — 18,000
                                            </label>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="checkbox">
                                            <input id="checkbox20" type="checkbox">
                                            <label for="checkbox20">
                                                100 — 24,000
                                            </label>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <!-- End Widget -->
                        </div>
                    </div>
                    <div class="col-md-9 main">
                        <div class="content">
                            <!-- Start Btn Toggle -->
                            <div class="btn-filter">
                                <a href="#" class="btn btn-default btn-sm toggle-filter">
                                    Filter By
                                </a>
                                <a href="#" class="btn btn-default btn-sm toggle-sort">
                                    View By
                                </a>
                                <a href="#" class="btn btn-default btn-sm toggle-view">
                                    Sort By
                                </a>
                            </div>
                            <!-- End Btn Toggle -->

                            <!-- Start Row -->
                            <div class="row">
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <!-- Start Post Item -->
                                    <div class="post-item match-height border">
                                        <span class="badge stroke small left">Beginner</span>
                                        <img src="/assets/product/img5.png" alt="" />
                                        <div class="content">
                                            <div class="equal-height">
                                                <h5 class="title">
                                                    <a href="#">PowerShot G1 X Mark II</a>
                                                </h5>
                                                <p class="margin-bottom0">
                                                    18 MP APS-C CMOS Sensor<br>
                                                    9-Point AF<br>
                                                    ISO 100 - 6,400
                                                </p>
                                            </div>
                                            <hr />
                                            <h4 class="price">$699</h4>
                                        </div>
                                    </div>
                                    <!-- End Post Item -->
                                </div>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <!-- Start Post Item -->
                                    <div class="post-item match-height border">
                                        <span class="badge stroke small left">Beginner</span>
                                        <img src="/assets/product/img5.png" alt="" />
                                        <div class="content">
                                            <div class="equal-height">
                                                <h5 class="title">
                                                    <a href="#">PowerShot G1 X Mark II</a>
                                                </h5>
                                                <p class="margin-bottom0">
                                                    18 MP APS-C CMOS Sensor<br>
                                                    9-Point AF<br>
                                                    ISO 100 - 6,400
                                                </p>
                                            </div>
                                            <hr />
                                            <h4 class="price">$699</h4>
                                        </div>
                                    </div>
                                    <!-- End Post Item -->
                                </div>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <!-- Start Post Item -->
                                    <div class="post-item match-height border">
                                        <span class="badge stroke small left">Beginner</span>
                                        <img src="/assets/product/img5.png" alt="" />
                                        <div class="content">
                                            <div class="equal-height">
                                                <h5 class="title">
                                                    <a href="#">PowerShot G1 X Mark II</a>
                                                </h5>
                                                <p class="margin-bottom0">
                                                    18 MP APS-C CMOS Sensor<br>
                                                    9-Point AF<br>
                                                    ISO 100 - 6,400
                                                </p>
                                            </div>
                                            <hr />
                                            <h4 class="price">$699</h4>
                                        </div>
                                    </div>
                                    <!-- End Post Item -->
                                </div>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <!-- Start Post Item -->
                                    <div class="post-item match-height border">
                                        <span class="badge stroke small left">Beginner</span>
                                        <span class="badge small right">Popular</span>
                                        <img src="/assets/product/img5.png" alt="" />
                                        <div class="content">
                                            <div class="equal-height">
                                                <h5 class="title">
                                                    <a href="#">PowerShot G1 X Mark II</a>
                                                </h5>
                                                <p class="margin-bottom0">
                                                    18 MP APS-C CMOS Sensor<br>
                                                    9-Point AF<br>
                                                    ISO 100 - 6,400
                                                </p>
                                            </div>
                                            <hr />
                                            <h4 class="price">$699</h4>
                                        </div>
                                    </div>
                                    <!-- End Post Item -->
                                </div>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <!-- Start Post Item -->
                                    <div class="post-item match-height border">
                                        <span class="badge stroke small left">Beginner</span>
                                        <img src="/assets/product/img5.png" alt="" />
                                        <div class="content">
                                            <div class="equal-height">
                                                <h5 class="title">
                                                    <a href="#">PowerShot G1 X Mark II</a>
                                                </h5>
                                                <p class="margin-bottom0">
                                                    18 MP APS-C CMOS Sensor<br>
                                                    9-Point AF<br>
                                                    ISO 100 - 6,400
                                                </p>
                                            </div>
                                            <hr />
                                            <h4 class="price">$699</h4>
                                        </div>
                                    </div>
                                    <!-- End Post Item -->
                                </div>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <!-- Start Post Item -->
                                    <div class="post-item match-height border">
                                        <span class="badge stroke small left">Semi-professional</span>
                                        <span class="badge small right">Popular</span>
                                        <img src="/assets/product/img5.png" alt="" />
                                        <div class="content">
                                            <div class="equal-height">
                                                <h5 class="title">
                                                    <a href="#">PowerShot G1 X Mark II</a>
                                                </h5>
                                                <p class="margin-bottom0">
                                                    18 MP APS-C CMOS Sensor<br>
                                                    9-Point AF<br>
                                                    ISO 100 - 6,400
                                                </p>
                                            </div>
                                            <hr />
                                            <h4 class="price">$699</h4>
                                        </div>
                                    </div>
                                    <!-- End Post Item -->
                                </div>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <!-- Start Post Item -->
                                    <div class="post-item match-height border">
                                        <span class="badge stroke small left">Professional</span>
                                        <span class="badge small right">Popular</span>
                                        <img src="/assets/product/img5.png" alt="" />
                                        <div class="content">
                                            <div class="equal-height">
                                                <h5 class="title">
                                                    <a href="#">PowerShot G1 X Mark II</a>
                                                </h5>
                                                <p class="margin-bottom0">
                                                    18 MP APS-C CMOS Sensor<br>
                                                    9-Point AF<br>
                                                    ISO 100 - 6,400
                                                </p>
                                            </div>
                                            <hr />
                                            <h4 class="price">$699</h4>
                                        </div>
                                    </div>
                                    <!-- End Post Item -->
                                </div>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <!-- Start Post Item -->
                                    <div class="post-item match-height border">
                                        <span class="badge stroke small left">Professional</span>
                                        <span class="badge small right">Popular</span>
                                        <img src="/assets/product/img5.png" alt="" />
                                        <div class="content">
                                            <div class="equal-height">
                                                <h5 class="title">
                                                    <a href="#">PowerShot G1 X Mark II</a>
                                                </h5>
                                                <p class="margin-bottom0">
                                                    18 MP APS-C CMOS Sensor<br>
                                                    9-Point AF<br>
                                                    ISO 100 - 6,400
                                                </p>
                                            </div>
                                            <hr />
                                            <h4 class="price">$699</h4>
                                        </div>
                                    </div>
                                    <!-- End Post Item -->
                                </div>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <!-- Start Post Item -->
                                    <div class="post-item match-height border">
                                        <span class="badge stroke small left">Professional</span>
                                        <span class="badge small right">Popular</span>
                                        <img src="/assets/product/img5.png" alt="" />
                                        <div class="content">
                                            <div class="equal-height">
                                                <h5 class="title">
                                                    <a href="#">PowerShot G1 X Mark II</a>
                                                </h5>
                                                <p class="margin-bottom0">
                                                    18 MP APS-C CMOS Sensor<br>
                                                    9-Point AF<br>
                                                    ISO 100 - 6,400
                                                </p>
                                            </div>
                                            <hr />
                                            <h4 class="price">$699</h4>
                                        </div>
                                    </div>
                                    <!-- End Post Item -->
                                </div>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <!-- Start Post Item -->
                                    <div class="post-item match-height border">
                                        <span class="badge stroke small left">Semi-professional</span>
                                        <img src="/assets/product/img5.png" alt="" />
                                        <div class="content">
                                            <div class="equal-height">
                                                <h5 class="title">
                                                    <a href="#">PowerShot G1 X Mark II</a>
                                                </h5>
                                                <p class="margin-bottom0">
                                                    18 MP APS-C CMOS Sensor<br>
                                                    9-Point AF<br>
                                                    ISO 100 - 6,400
                                                </p>
                                            </div>
                                            <hr />
                                            <h4 class="price">$699</h4>
                                        </div>
                                    </div>
                                    <!-- End Post Item -->
                                </div>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <!-- Start Post Item -->
                                    <div class="post-item match-height border">
                                        <span class="badge stroke small left">Semi-professional</span>
                                        <img src="/assets/product/img5.png" alt="" />
                                        <div class="content">
                                            <div class="equal-height">
                                                <h5 class="title">
                                                    <a href="#">PowerShot G1 X Mark II</a>
                                                </h5>
                                                <p class="margin-bottom0">
                                                    18 MP APS-C CMOS Sensor<br>
                                                    9-Point AF<br>
                                                    ISO 100 - 6,400
                                                </p>
                                            </div>
                                            <hr />
                                            <h4 class="price">$699</h4>
                                        </div>
                                    </div>
                                    <!-- End Post Item -->
                                </div>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <!-- Start Post Item -->
                                    <div class="post-item match-height border">
                                        <span class="badge stroke small left">Semi-professional</span>
                                        <img src="/assets/product/img5.png" alt="" />
                                        <div class="content">
                                            <div class="equal-height">
                                                <h5 class="title">
                                                    <a href="#">PowerShot G1 X Mark II</a>
                                                </h5>
                                                <p class="margin-bottom0">
                                                    18 MP APS-C CMOS Sensor<br>
                                                    9-Point AF<br>
                                                    ISO 100 - 6,400
                                                </p>
                                            </div>
                                            <hr />
                                            <h4 class="price">$699</h4>
                                        </div>
                                    </div>
                                    <!-- End Post Item -->
                                </div>
                            </div>
                            <!-- End Row -->
                        </div>
                        <!-- Start Pagination -->
                        <div class="post-pagination">
                            <ul>
                                <li class="nav"><a href="#"><span class="fa fa-angle-left"></span></a></li>
                                <li class="page">1</li>
                                <li>of</li>
                                <li class="count">24</li>
                                <li class="nav"><a href="#"><span class="fa fa-angle-right"></span></a></li>
                            </ul>
                        </div>
                        <!-- End Pagination -->
                        <!-- Start Info -->
                        <div class="post-info">
                            <span class="fa fa-circle"></span>All prices above are recommended retail price in SGD, unless otherwise stated.
                        </div>
                        <!-- End Info -->
                    </div>
                </div>
            </div>
        </div>
        <!-- End Content -->
    </div>
    <!-- End Wrap PageComponent -->
</body>
</html>