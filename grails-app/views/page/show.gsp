<%@ page import="canon.Page; canon.PageProduct; canon.PageComponent; canon.model.comp.HeroComp; canon.model.comp.TextComp; canon.utils.ContentUtils; java.util.List" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title></title>
    <meta name="layout" content="canon">
</head>
<body>
<!-- Start Wrap Component -->
<div class="wrap-component">
    <div class="clearfix"></div>

    <!-- Start Content -->
    <div class="inner-page">
        <div class="container">
            <div class="row">
                <div class="col-md-7 col-sm-4 col-xs-6">
                    <!-- Breadcrumbs -->
                    <ol class="breadcrumb">
                        <li>
                            <g:link mapping="${ page.contentType?.name()?.toLowerCase()+'Main' }" params="[categoryType: page?.contentType?.name(), segment: params?.segment, site: params?.site, lang: params?.lang]">
                                ${page?.contentType?.name()}
                            </g:link>
                        </li>
                        <g:if test="${page?.getMainCategory(params?.segment as String)}">
                            <li class="sub">
                                <g:link mapping="${ page.contentType?.name()?.toLowerCase()+'List' }" params="[category: page?.getMainCategory(params?.segment as String)?.slug, segment: params?.segment, site: params?.site, lang: params?.lang]">
                                    ${page?.getMainCategory(params?.segment as String)?.name}
                                </g:link>
                            </li>
                        </g:if>
                        <li class="active">
                            <span>${raw(page?.headline)}</span>
                        </li>
                    </ol>
                </div>
                <div class="col-md-5 col-sm-8 col-xs-6 text-right">
                    <g:if test="${page.status == canon.enums.Status.ARCHIVED}">
                        <span class="text"><g:message code="public.article.details.archives.label" default="Archives" /></span>
                    </g:if>
                </div>
            </div>
        </div>
    </div>
    <!-- End Content -->
    <div class="clearfix"></div>

    <!-- Social Share -->
    <div class="section-share">
        <div class="container">
            <ul class="social-share hide-medium">
                <g:set var="shareUrl" value="${createLink(absolute: Boolean.TRUE, mapping: page.contentType?.name()?.toLowerCase()+'Details', id: page.slug,
                        params: [site: params?.site, segment: params?.segment, category: page?.getMainCategory(params?.segment as String)?.slug])}" />
                <li><a href="${shareUrl}"><span class="fa fa-facebook"></span></a></li>
                <li><a href="${shareUrl}"><span class="fa fa-twitter"></span></a></li>
                <li><a href="${shareUrl}"><span class="fa fa-feed"></span></a></li>
            </ul>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="section padding-bottom0"></div>

    <!-- Start Section -->
    <div class="section">
        <div class="container">
            <div class="article">
                <g:set var="dayOfMonth" value="${g.formatDate(date: page?.publishedDate, format: "d")}"/>
                <h3 class="time">${dayOfMonth+""+ContentUtils.daySuffix(Integer.valueOf(dayOfMonth))+" "}<g:formatDate date="${page?.publishedDate}" format="MMMM yyyy, hh:mma" /></h3>
                <h1 class="title">${raw(page?.headline)}</h1>
                <ul class="clone-share list-inline show-medium margin-bottom10"></ul>
                <g:if test="${page?.deck}">
                    <p>${raw(page?.deck ?: '')}</p>
                </g:if>
            </div>
        </div>
    </div>
    <!-- End Section -->

    <g:each in="${pageComponents as List<PageComponent>}" var="pc" status="i">
        <g:renderComp component="${pc.component}" site="${params?.site}" segment="${params?.segment}" />
    </g:each>
    <div class="clearfix"></div>

    <g:if test="${relatedProducts}">
        <!-- Start Section -->
        <div class="section">
            <div class="container">
                <!-- Start Related Products -->
                <h1 class="title-section text-center"><g:message code="public.article.details.relatedProducts.label" default="Related Products"/></h1>
                <div class="wrap-carousel-content">
                    <div class="owl-carousel carousel-content">
                        <!-- Start Post Item -->
                        <g:each in="${relatedProducts as List<PageProduct>}" var="related">
                            <div class="post-item related">
                                <div class="wrap-image">
                                    <a href="#"><img src="${related?.product?.productMedias?.getAt(0)?.mediaUrl}" alt="" /></a>
                                </div>
                                <div class="content">
                                    <h3 class="title">
                                        <g:link controller="product" action="show" id="${related?.product?.slug}" params="${params}">${related?.product?.name}</g:link>
                                    </h3>
                                </div>
                            </div>
                        </g:each>
                        <!-- End Post Item -->

                        <g:set var="firstRelated" value="${relatedProducts?.getAt(0) as PageProduct}" />
                        <!-- Start Post Item -->
                        <g:link controller="product" action="list" params="[segment: params?.segment, category: firstRelated?.product?.productCategories?.category?.parent?.slug, subCategory: firstRelated?.product?.productCategories?.category?.slug, site: params?.site, lang: params?.lang]">
                            <div class="post-item last-content">
                                <div class="content">
                                    <h4 class="title"><g:message code="public.article.details.seeAllProducts.label" default="See all products"/></h4>
                                </div>
                            </div>
                        </g:link>
                        <!-- End Post Item -->
                    </div>
                </div>
                <!-- End Related Products -->
            </div>
        </div>
        <!-- End Section -->
        <div class="clearfix"></div>
    </g:if>

    <g:if test="${suggestedArticles}">
        <!-- Start Section Detail -->
        <div class="section">
            <div class="container">
                <!-- Start Features -->
                <h1 class="title-section text-center"><g:message code="public.article.details.suggestedArticles.label" default="Suggested Articles"/></h1>
                <div class="wrap-carousel-mobile">
                    <div class="row">
                        <g:each in="${suggestedArticles as List<Page>}" var="suggest">
                        <div class="col-sm-4 col-xs-12">
                            <!-- Start Post Item -->
                            <div class="post-item">
                                <span class="badge right gray">${suggest?.getMainCategory(params?.segment as String)?.name}</span>
                                <g:set var="hero" value="${suggest.getFirstHeroComponent()?.component?.getAsComponent() as HeroComp}" />
                                <div class="wrap-image">
                                    <g:link mapping="${ suggest.contentType?.name()?.toLowerCase()+'Details' }" id="${suggest?.slug}" params="${params}">
                                        <img src="${hero?.imageUrl}" alt="" />
                                    </g:link>
                                </div>
                                <div class="content text-left">
                                    <h3 class="title match-height">
                                        <g:link mapping="${ suggest.contentType?.name()?.toLowerCase()+'Details' }" id="${suggest?.slug}" params="${params}">
                                            ${raw(suggest?.headline)}
                                        </g:link>
                                    </h3>
                                    <p class="margin-bottom0">
                                        <strong><g:formatDate date="${suggest?.publishedDate}" format="d MMMM yyyy" /></strong>
                                    </p>
                                </div>
                            </div>
                        </div>
                        </g:each>
                    </div>
                </div>
                <!-- End Content -->
            </div>
        </div>
        <!-- End Section Detail -->
    </g:if>
</div>
<!-- End Wrap Component -->
</body>
</html>