<%--
  Created by IntelliJ IDEA.
  User: muyalif
  Date: 29/11/17
  Time: 11.17
--%>

<%@ page import="canon.model.Component; canon.Category; canon.PageComponent" contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title><g:message code="home.title.label" default="Canon - Home"/></title>
    <meta name="layout" content="canon"/>
</head>

<body>

<!-- Start Wrap Component -->
<div class="wrap-component">

    <!-- Start Inner Page -->
    <div class="inner-page absolute">
        <div class="blurheader"></div>
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-7 col-sm-4 col-xs-6 hide-small">
                    <g:message code="canon.enums.CategoryType.${categoryType?.name()}.label" default="" />
                </div>
                <div class="col-md-5 col-sm-8 col-xs-6 text-right">
                    <!-- Menus -->
                    <div class="wrap-menus open">
                        <a href="#" class="toggle-menus"><g:message code="public.article.main.selectMenu.label" default="Select menu" /></a>
                        <ul class="list-inline menus">
                            <g:each in="${categories as List<canon.Category>}" var="cat">
                                <li>
                                    <g:link controller="page" action="list" params="[site: params?.site, segment: params?.segment, category: cat.slug, categoryType: cat.categoryType?.name()?.toLowerCase()]">
                                        ${cat?.translation?.name ?: cat?.name}
                                    </g:link>
                                </li>
                            </g:each>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Inner Page -->
    <div class="clearfix"></div>

    <!-- Start Content -->
    <div class="section-demo demo-text3 bg" data-image="${grailsApplication.config.amazonaws.s3.baseUrl?:''}images/bg/img13.jpg">
        <div class="overlay"></div>
        <div class="container">
            <div class="content">
                <h1 class="title"><g:message code="public.article.main.searchDiscover.label" default="Discover great reads" /></h1>
                <div class="row">
                    <div class="col-sm-10 col-sm-offset-1">
                        <div class="input-serach">
                            <g:form controller="page" action="list" method="GET">
                                <g:hiddenField name="site" value="${params?.site}" />
                                <g:hiddenField name="lang" value="${params?.lang}" />
                                <g:hiddenField name="segment" value="${params?.segment}" />
                                <g:hiddenField name="category" value="${params?.category}" />
                                <g:hiddenField name="categoryType" value="${params?.categoryType}" />
                                <input type="text" placeholder="${message(code: 'public.article.main.search.label', default: 'Search Articles')}" name="q" value="${params?.q ?: ''}" />
                            </g:form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Content -->
    <div class="section-fix"></div>

    <!-- Start Content -->
    <div class="section">
        <div class="container">
            <div class="row">
                <div class="col-md-3 sidebar">
                    <div class="content">
                        <g:form controller="page" action="list" method="GET">
                            <g:hiddenField name="q" value="${params?.q ?: ''}" />
                            <g:hiddenField name="max" value="${params?.max}" />
                            <g:hiddenField name="page" value="${params?.page}" />
                            <g:hiddenField name="site" value="${params?.site}" />
                            <g:hiddenField name="lang" value="${params?.lang}" />
                            <g:hiddenField name="segment" value="${params?.segment}" />
                            <g:hiddenField name="category" value="${params?.category}" />
                            <g:hiddenField name="categoryType" value="${params?.categoryType}" />
                            <!-- Start Widget -->
                            <div class="widget" id="sort" data-toggle=".toggle-sort">
                                <a class="close" href="#"><span class="fa fa-times"></span></a>
                                <h6 class="title"><g:message code="public.article.list.sortBy.label" default="Sort By" /></h6>
                                <select name="sort" class="form-control select input-sm margin-bottom20 margin-top20 js-auto-submit">
                                    <g:each in="${canon.Page.ES_SORTABLE_FIELDS}" var="sortableField">
                                        <option value="${sortableField.key}" ${(params?.sort && params?.sort == sortableField.key || !params?.sort && 'az' == sortableField.key) ? 'selected' : '' }>
                                            <g:message code="public.article.list.sort.${sortableField.key}.label" default="${sortableField.key}" />
                                        </option>
                                    </g:each>
                                </select>
                            </div>
                            <!-- End Widget -->
                            <!-- Start Widget -->
                            <div class="widget" id="filter" data-toggle=".toggle-filter">
                                <a class="close" href="#"><span class="fa fa-times"></span></a>
                                <h6 class="title"><g:message code="public.article.list.filterBy.label" default="Filter by" /></h6>
                                <p class="sub-title"><g:message code="public.article.list.subject.label" default="Topic" /></p>
                                <ul class="clear-list" id="range">
                                    <g:if test="${resultAggs && resultAggs.get('subjects')}">
                                        <g:each in="${resultAggs.get('subjects')}" var="aggs">
                                            <li>
                                                <div class="checkbox">
                                                    <input id="checkbox-${aggs.key}" type="checkbox" value="${aggs.key}" name="subject"
                                                           class="js-auto-submit" ${params?.list('subject')?.contains(aggs.key?.toString()) ? 'checked' : ''}>
                                                    <label for="checkbox-${aggs.key}">
                                                        ${aggs.value?.name}
                                                    </label>
                                                </div>
                                            </li>
                                        </g:each>
                                    </g:if>
                                </ul>
                            </div>
                            <!-- End Widget -->
                        </g:form>
                    </div>
                </div>
                <div class="col-md-9 main">
                    <div class="content">
                        <!-- Start Btn Toggle -->
                        <div class="btn-filter">
                            <a href="#" class="btn btn-default btn-sm toggle-filter">
                                <g:message code="public.article.list.filterBy.label" default="Filter by" />
                            </a>
                            <a href="#" class="btn btn-default btn-sm toggle-view">
                                <g:message code="public.article.list.sortBy.label" default="Sort By" />
                            </a>
                        </div>
                        <!-- End Btn Toggle -->

                        <g:if test="${resultList?.size() > 0}">
                            <!-- Start Row -->
                            <div class="row">
                                <g:each in="${resultList}" var="content">
                                    <div class="col-md-4 col-sm-6 col-xs-12">
                                        <!-- Start Post Item -->
                                        <div class="post-item match-height border">
                                            <img src="${content.get('banner')?.url ?: ''}" alt="" />
                                            <div class="content">
                                                <div class="equal-height">
                                                    <h5 class="title">
                                                        <g:link mapping="${ content.get('contentType')?.name()?.toLowerCase()+'Details' }" id="${content.get('slug')}"
                                                                params="[site: params?.site, segment: params?.segment, category: params?.category]">
                                                            ${content.get("title")}
                                                        </g:link>
                                                    </h5>
                                                    <p class="margin-bottom0">
                                                        <g:if test="${content.get('publishedDate')}">
                                                            <strong><g:formatDate date="${content.get('publishedDate')}" format="dd MMMM yyyy" /></strong><br/>
                                                        </g:if>
                                                        ${content.get("shortDescription")}
                                                    </p>
                                                </div>
                                                <hr />
                                                <div class="row social-link">
                                                    <g:set var="shareUrl" value="${createLink(absolute: Boolean.TRUE, mapping: content.get('contentType')?.name()?.toLowerCase()+'Details', id: content.get('slug'),
                                                            params: [site: params?.site, segment: params?.segment, category: params?.category])}" />
                                                    <div class="col-xs-4 text-left"><a href="${shareUrl}"><sapn class="fa fa-facebook"></sapn></a></div>
                                                    <div class="col-xs-4"><a href="${shareUrl}"><sapn class="fa fa-twitter"></sapn></a></div>
                                                    <div class="col-xs-4 text-right"><a href="${shareUrl}"><sapn class="fa fa-google-plus"></sapn></a></div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- End Post Item -->
                                    </div>
                                </g:each>
                            </div>
                            <!-- End Row -->
                        </g:if>
                    </div>
                    <g:if test="${resultCount  > params?.int('max')}">
                        <!-- Start Pagination -->
                        <div class="post-pagination">
                            <ul>
                                <li class="nav"><a href="#"><span class="fa fa-angle-left"></span></a></li>
                                <li class="page"><input type="number" value="1"></li>
                                <li>of</li>
                                <li class="count">${resultCount}</li>
                                <li class="nav"><a href="#"><span class="fa fa-angle-right"></span></a></li>
                            </ul>
                        </div>
                        <!-- End Pagination -->
                    </g:if>
                </div>
            </div>
        </div>
    </div>
    <!-- End Content -->

</div>
<!-- End Wrap Component -->

<div class="clearfix"></div>
</body>
</html>