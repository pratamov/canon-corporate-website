<%--
  Created by IntelliJ IDEA.
  User: marjan
  Date: 11/12/17
  Time: 18.29
--%>

<%@ page import="canon.enums.Status" contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title><g:message code="admin.dashboard.title" default="Admin Dashboard"/></title>
    <meta name="layout" content="admin"/>
</head>

<body>

<!-- Start Page Heading -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Product Details (Singapore — EN)</h2>
        <ol class="breadcrumb">
            <li>
                <g:link controller="adminProduct" action="index">Product</g:link>
            </li>
            <li>
                <g:link controller="adminProduct" action="index">Product List</g:link>
            </li>
            <li>
                <g:link controller="adminProduct" action="show" id="${product?.id}">Location & Language</g:link>
            </li>
            <li class="active">
                <strong>${product?.name}</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2 text-right ">
        <h4 class="info">Version: <span>${product?.version}</span></h4>
        <g:link controller="adminProduct" action="delete" id="${product?.id}" class="btn btn-danger">Delete</g:link>
    </div>
</div>
<!-- End Page Heading -->

<!-- Start Content -->
<div class="wrapper wrapper-content">
    <!-- Start box -->
    <div class="ibox-title">
        <h3 class="title">Product Overview</h3>
    </div>
    <div class="ibox-content">
        <div class="form-group margin-bottom20">
            <label>Select Workflow</label>
            <input type="text" class="form-control" placeholder="Product Workflow" />
        </div>
        <div class="row">
            <div class="col-md-3">
                <div class="label-arrow">Requestor</div>
                <div class="form-group margin-bottom20">
                    <label>Assign to</label>
                    <input type="text" class="form-control" placeholder="Theodore Twombly" />
                </div>
            </div>
            <div class="col-md-3">
                <div class="label-arrow">Preparer</div>
                <div class="form-group margin-bottom20">
                    <label>Assign to</label>
                    <select class="select form-control" data-placeholder="Product Preparer">
                        <option></option>
                        <option value="Bahamas">Bahamas</option>
                        <option value="Bahrain">Bahrain</option>
                        <option value="Bangladesh">Bangladesh</option>
                        <option value="Barbados">Barbados</option>
                        <option value="Belarus">Belarus</option>
                        <option value="Belgium">Belgium</option>
                        <option value="Belize">Belize</option>
                        <option value="Benin">Benin</option>
                    </select>
                </div>
                <div class="form-group">
                    <label>Deadline</label>
                    <div class="input-group date">
                        <input type="text" class="form-control" value="02-10-2018">
                        <span class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="label-arrow">Confirmation</div>
                <div class="form-group margin-bottom20">
                    <label>Assign to</label>
                    <select class="select form-control" data-placeholder="Product Confirmation">
                        <option value="Bahamas">Bahamas</option>
                        <option value="Bahrain">Bahrain</option>
                        <option value="Bangladesh">Bangladesh</option>
                        <option value="Barbados">Barbados</option>
                        <option value="Belarus">Belarus</option>
                        <option value="Belgium">Belgium</option>
                        <option value="Belize">Belize</option>
                        <option value="Benin">Benin</option>
                    </select>
                </div>
                <div class="form-group">
                    <label>Deadline</label>
                    <div class="input-group date">
                        <input type="text" class="form-control" value="08-10-2014">
                        <span class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="label-arrow">Publishing</div>
                <div class="form-group margin-bottom20">
                    <label>Assign to</label>
                    <select class="select form-control" data-placeholder="Product Publisher">
                        <option></option>
                        <option value="Bahamas">Bahamas</option>
                        <option value="Bahrain">Bahrain</option>
                        <option value="Bangladesh">Bangladesh</option>
                        <option value="Barbados">Barbados</option>
                        <option value="Belarus">Belarus</option>
                        <option value="Belgium">Belgium</option>
                        <option value="Belize">Belize</option>
                        <option value="Benin">Benin</option>
                    </select>
                </div>
                <div class="form-group">
                    <label>Deadline</label>
                    <div class="input-group date">
                        <input type="text" class="form-control" value="16-10-2014">
                        <span class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Ibox -->
    <!-- Start Ibox -->
    <div class="ibox-content padding-bottom0">
        <p><em>Changes will be applied to other languages of the same site</em></p>
        <div class="row">
            <div class="col-md-3">
                <div class="form-group margin-bottom20">
                    <label>Product Category</label>
                    <select class="select form-control" data-placeholder="Select Product Category">
                        <option></option>
                        <option value="Lorem Ispum">Lorem Ispum</option>
                        <option value="Lorem Ispum">Lorem Ispum</option>
                        <option value="Lorem Ispum">Lorem Ispum</option>
                        <option value="Lorem Ispum">Lorem Ispum</option>
                        <option value="Digital DSLR (EOS)" selected>Digital DSLR (EOS)</option>
                        <option value="Lorem Ispum">Lorem Ispum</option>
                        <option value="Lorem Ispum">Lorem Ispum</option>
                        <option value="Lorem Ispum">Lorem Ispum</option>
                    </select>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group margin-bottom20">
                    <label>Product Price </label> (For Asia only)
                    <input type="text" class="form-control" value="$59.90" />
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group margin-bottom20">
                    <label>Product Availability Date</label>
                    <input type="text" class="form-control" value="9 Nov 2018"  disabled/>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group margin-bottom20">
                    <label>Product Discontinued  Date</label>
                    <input type="text" class="form-control" value="9 Nov 2018" disabled/>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group margin-bottom20">
                    <label>Publish Date</label>
                    <div class="input-group date">
                        <input type="text" class="form-control" value="16-10-2014">
                        <span class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group margin-bottom20">
                    <label>Archive Date</label>
                    <div class="input-group date">
                        <input type="text" class="form-control" value="16-10-2014">
                        <span class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Ibox -->
    <!-- Start Ibox -->
    <div class="ibox-content margin-bottom30 padding-bottom0">
        <div class="form-group margin-bottom20">
            <label>Product Name</label>
            <input type="text" class="form-control" value="${product?.name}" />
        </div>
        <div class="row">
            <div class="col-md-3 margin-bottom20">
                <div class="form-group margin-bottom20">
                    <label class="margin-bottom15">Product Language</label>
                    <input type="text" class="form-control" value="English" disabled/>
                </div>
            </div>
            <div class="col-md-3 margin-bottom20">
                <p><label>Full Product Specification</label></p>
                <a href="#" class="btn btn-primary">Input Specification</a>
            </div>
            <div class="col-md-3 margin-bottom20">
                <p><label>Related Products</label></p>
                <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#modalrelation">Input Relation</a>
            </div>
            <div class="col-md-3 margin-bottom20">
                <p><label>Consumables</label></p>
                <a href="#" class="btn btn-primary">Input Consumables</a>
            </div>
        </div>
    </div>
    <!-- End Ibox -->

    <!-- Start Modal -->
    <div class="modal inmodal" id="modalrelation" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content animated fadeIn">
                <div class="modal-header">
                    <div class="row">
                        <div class="col-sm-8">
                            <h3 class="modal-title">
                                Product Relation<br/>
                                <small><em>Recommeded: 3 – 8 Products</em></small>
                            </h3>
                        </div>
                        <div class="col-sm-4 text-right">
                            <a href="#" class="btn btn-default btn-title clearCheck">Clear All</a>
                        </div>
                    </div>
                </div>
                <div class="modal-body">
                    <p>Version 4 is approved.</p>
                    <p></p>To make edits, a new version has to be created.</p>
                    <div class="text-right margin-top30">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                        <button type="button" class="btn btn-primary">Confirm</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Modal -->

    <div class="row margin-bottom30">
        <div class="col-md-8">
            <!-- Start Ibox -->
            <div class="ibox-title">
                <button class="btn btn-primary pull-right  margin-left10">Preview</button>
                <button class="btn btn-default pull-right margin-left10">Inline Editor</button>
                <button class="btn btn-default pull-right">Template</button>
                <h3 class="title">Preview</h3>
            </div>
            <div class="preview-page margin-bottom30">
                <div class="content">our preview will appear here. Click on the Edit Content button below to start creating your product.</div>
            </div>
            <div class="ibox-content">
                <!-- Start Panel -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <strong>Tag</strong>
                    </div>
                    <div class="panel-body">
                        <p>Select up to 3 tags to associate with the page:</p>
                        <input class="tagsinput form-control" type="text" placeholder="Type Something Here" value="Amsterdam,Washington,Sydney,Beijing,Cairo"/>
                    </div>
                </div>
                <!-- End Panel -->

                <!-- Start Panel -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <strong>Social Share</strong> (optional)
                    </div>
                    <div class="panel-body">
                        <div class="form-horizontal" action="/action_page.php">
                            <div class="form-group">
                                <label class="control-label col-sm-2 text-left">Title</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control">
                                    <small><em>If the field is blank, the value will be pulled from product title.</em></small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2 text-left">Description</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control">
                                    <small><em>If the field is blank, the value will be pulled from text below hero image (synopsis)</em></small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2 text-left">Image</label>
                                <div class="col-sm-10">
                                    <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                        <div class="form-control" data-trigger="fileinput">
                                            <i class="glyphicon glyphicon-file fileinput-exists"></i>
                                            <span class="fileinput-filename"></span>
                                        </div>
                                        <span class="input-group-addon btn btn-default btn-file">
                                            <span class="fileinput-new">Select file</span>
                                            <span class="fileinput-exists">Change</span>
                                            <input type="file" name="..."/>
                                        </span>
                                        <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                    </div>
                                    <small><em>If the field is blank, the hero image will be used.</em></small>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Panel -->
            </div>
            <!-- End Ibox -->
        </div>
        <div class="col-md-4">
            <!-- Start Ibox -->
            <div class="ibox-title">
                <h3 class="title">Comments </h3>
            </div>
            <div class="ibox-content">
                <div class="form-group">
                    <textarea class="form-control" placeholder="Add a comment" rows="7"></textarea>
                </div>
                <div class="text-right">
                    <button class="btn btn-primary">Submit</button>
                </div>
            </div>
            <div class="ibox-content" style="height:715px;overflow-y:scroll">
                <ul class="list-comment">
                    <li>
                        <h5 class="title">
                            Jason Huang
                            <em>28/10/2017, 05:30pm, GMT+8</em>
                        </h5>
                        <p>@Jack Toh As Johnny Mcclean is current on medical leave, I have submitted the article for your editing.</p>
                        <div class="action"><a href="#" class="link">Edit</a> | <a href="#" class="link">Delete</a></div>
                    </li>
                    <li>
                        <h5 class="title">
                            Elson Loh
                            <em>25/10/2017, 9:00a.m, GMT+8</em>
                        </h5>
                        <p>@Jonny Mcclean This is the second draft. Edits made: Video segment has been updated. Images have been updated with the latest images from Alice Keys.</p>
                        <div class="action"><a href="#" class="link">Edit</a> | <a href="#" class="link">Delete</a></div>
                    </li>
                    <li>
                        <h5 class="title">
                            Johnny Mcclean
                            <em>24/10/2017, 04:07pm, GMT+8</em>
                        </h5>
                        <p>@Elson Loh Please see the comments below for changes required:</p>
                        <p>In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus.</p>
                        <p>Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus</p>
                        <div class="action"><a href="#" class="link">Edit</a> | <a href="#" class="link">Delete</a></div>
                    </li>
                </ul>
            </div>
            <!-- End Ibox -->
        </div>
    </div>

    <div class="row margin-bottom70">
        <div class="col-md-8">
            <!-- Start Ibox -->
            <div class="ibox-title">
                <h3 class="title">Verison History</h3>
            </div>
            <div class="ibox-content" style="height:560px; overflow-y:scroll">
                <!-- Start Panel -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <strong>Master Version 3</strong>
                    </div>
                    <div class="panel-body padding-bottom0">
                        <table class="table table-version">
                            <tr>
                                <td class="action-live"><button class="btn btn-default">LIVE</button></td>
                                <td><a href="#" class="single-link">Verison 6</a></td>
                                <td>01 Nov 2018 0:01 (GMT+8)</td>
                                <td>Draft</td>
                            </tr>
                            <tr>
                                <td><button class="btn btn-default">LIVE</button></td>
                                <td><a href="#" class="single-link">Verison 5</a></td>
                                <td>01 Nov 2018 0:01 (GMT+8)</td>
                                <td>Approved</td>
                            </tr>
                            <tr>
                                <td><button class="btn btn-default">LIVE</button></td>
                                <td><a href="#" class="single-link">Verison 4</a></td>
                                <td>01 Nov 2018 0:01 (GMT+8)</td>
                                <td>Approved</td>
                            </tr>
                            <tr>
                                <td><button class="btn btn-default">LIVE</button></td>
                                <td><a href="#" class="single-link">Verison 3</a></td>
                                <td>01 Nov 2018 0:01 (GMT+8)</td>
                                <td>Approved</td>
                            </tr>
                            <tr>
                                <td><button class="btn btn-default">LIVE</button></td>
                                <td><a href="#" class="single-link">Verison 2</a></td>
                                <td>01 Nov 2018 0:01 (GMT+8)</td>
                                <td>Approved</td>
                            </tr>
                            <tr>
                                <td><button class="btn btn-default">LIVE</button></td>
                                <td><a href="#" class="single-link">Verison 1</a></td>
                                <td>01 Nov 2018 0:01 (GMT+8)</td>
                                <td>Approved</td>
                            </tr>
                        </table>
                    </div>
                </div>
                <!-- End Panel -->
                <!-- Start Panel -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <strong>Master Version 2</strong>
                    </div>
                    <div class="panel-body padding-bottom0">
                        <table class="table table-version">
                            <tr>
                                <td class="action-live"><button class="btn btn-default">LIVE</button></td>
                                <td><a href="#" class="single-link">Verison 6</a></td>
                                <td>01 Nov 2018 0:01 (GMT+8)</td>
                                <td>Draft</td>
                            </tr>
                            <tr>
                                <td><button class="btn btn-default">LIVE</button></td>
                                <td><a href="#" class="single-link">Verison 5</a></td>
                                <td>01 Nov 2018 0:01 (GMT+8)</td>
                                <td>Approved</td>
                            </tr>
                            <tr>
                                <td><button class="btn btn-default">LIVE</button></td>
                                <td><a href="#" class="single-link">Verison 4</a></td>
                                <td>01 Nov 2018 0:01 (GMT+8)</td>
                                <td>Approved</td>
                            </tr>
                            <tr>
                                <td><button class="btn btn-default">LIVE</button></td>
                                <td><a href="#" class="single-link">Verison 3</a></td>
                                <td>01 Nov 2018 0:01 (GMT+8)</td>
                                <td>Approved</td>
                            </tr>
                        </table>
                    </div>
                </div>
                <!-- End Panel -->
            </div>
            <!-- End Ibox -->
        </div>
        <div class="col-md-4">
            <!-- Start Ibox -->
            <div class="ibox-title">
                <h3 class="title">Audit Log</h3>
            </div>
            <div class="ibox-content log" style="height:479px; overflow-y:scroll">
                <ul class="list-comment">
                    <li>
                        <em>Updated on 10 Nov 2017, 10:55pm, EDT</em><br/>
                        <p><a href="#" class="single-link"><strong>Jason Huang</strong></a> submitted the product for Editing to <a href="#" class="single-link"><strong>Jack Toh</strong></a>.</p>
                    </li>
                    <li>
                        <em>Updated on 11 Nov 2017, 11:00am, GMT+8</em><br/>
                        <p><a href="#" class="single-link"><strong>Johnny Mcclean</strong></a> returned the product to <a href="#" class="single-link"><strong>Elson Loh</strong></a>.</p>
                    </li>
                    <li>
                        <em>Updated on 12 Nov 2017, 11:00am, GMT+8</em><br/>
                        <p><a href="#" class="single-link"><strong>Elson Loh</strong></a> submitted the product for Proofreading to <a href="#" class="single-link"><strong>Johnny Mcclean</strong></a>.</p>
                    </li>
                </ul>
            </div>
            <div class="ibox-content ibok-action">
                <a href="#" class="btn btn-default">View Full Log</a>
            </div>
            <!-- End Ibox -->
        </div>
    </div>

</div>
<!-- End Content -->

<content tag="footer">
    <div class="pull-right">
        <button class="btn btn-default margin-left10">Cancel</button>
        <button class="btn btn-primary margin-left10" data-toggle="modal" data-target="#modalConfirm">Submit</button>
    </div>
</content>

<!-- Start Footer -->
<div class="footer">
    <div>
        <div class="pull-right">
            <button class="btn btn-default margin-left10">Cancel</button>
            <button class="btn btn-primary margin-left10" data-toggle="modal" data-target="#modalConfirm">Submit</button>
        </div>
        <span class="copy"><strong>Copyright</strong> &copy; 2017 Canon. All rights reserved.</span>
    </div>
</div>
<!-- End Footer -->

<!-- Start Modal -->
<div class="modal inmodal" id="modalConfirm" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated fadeIn">
            <div class="modal-header">
                <h3 class="modal-title">New Verison</h3>
            </div>
            <div class="modal-body">
                <p>Version 4 is approved.</p>
                <p></p>To make edits, a new version has to be created.</p>
                <div class="text-right margin-top30">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary">Confirm</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Modal -->
</body>
</html>