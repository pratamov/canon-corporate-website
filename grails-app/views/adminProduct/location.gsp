<%--
  Created by IntelliJ IDEA.
  User: marjan
  Date: 11/12/17
  Time: 18.29
--%>

<%@ page import="canon.enums.Status" contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title><g:message code="admin.dashboard.title" default="Admin Dashboard"/></title>
    <meta name="layout" content="admin"/>
</head>

<body>

<!-- Start Page Heading -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-12">
        <h2>Location & Language Selection</h2>
        <ol class="breadcrumb">
            <li>
                <g:link controller="adminProduct" action="index">Product</g:link>
            </li>
            <li>
                <g:link controller="adminProduct" action="index">Product List</g:link>
            </li>
            <li class="active">
                <strong>Location & Language</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2"></div>
</div>
<!-- End Page Heading -->

<!-- Start Content -->
<div class="wrapper wrapper-content">
    <!-- Start Ibox -->
    <div class="ibox-content margin-bottom60">
        <div class="row">
            <div class="col-sm-8">
                <form class="form-inline">
                    <div class="form-group margin-bottom20">
                        <label>Filter by</label>
                        <select class="form-control w100">
                            <option>All Locations</option>
                            <option>Option 1</option>
                            <option>Option 2</option>
                            <option>Option 3</option>
                            <option>Option 4</option>
                        </select>
                    </div>
                    <div class="form-group margin-bottom20">
                        <label>Date range</label>
                        <select class="form-control">
                            <option>14 Sept 2017</option>
                            <option>13 Sept 2017</option>
                            <option>12 Sept 2017</option>
                            <option>11 Sept 2017</option>
                            <option>10 Sept 2017</option>
                            <option>9 Sept 2017</option>
                        </select>
                    </div>
                    <div class="form-group margin-bottom20">
                        <label>to</label>
                        <select class="form-control">
                            <option>14 Sept 2017</option>
                            <option>13 Sept 2017</option>
                            <option>12 Sept 2017</option>
                            <option>11 Sept 2017</option>
                            <option>10 Sept 2017</option>
                            <option>9 Sept 2017</option>
                        </select>
                    </div>
                </form>
            </div>
            <div class="col-sm-4">
                <div class="input-group"><input type="text" placeholder="Search keyword" class="input-sm form-control"> <span class="input-group-btn">
                    <button type="button" class="btn btn-sm btn-primary">Search</button> </span></div>
            </div>
        </div>
        <div class="row margin-top30">
            <div class="col-sm-6">
                <div data-toggle="buttons" class="btn-group">
                    <label class="btn btn-sm btn-white"> <input type="radio" id="option1" name="options"> Published (120)</label>
                    <label class="btn btn-sm btn-white active"> <input type="radio" id="option2" name="options"> Draft (120)</label>
                    <label class="btn btn-sm btn-white"> <input type="radio" id="option3" name="options"> Archive (120)</label>
                    <label class="btn btn-sm btn-white"> <input type="radio" id="option4" name="options"> Trash (0)</label>
                </div>
            </div>
            <div class="col-sm-6 text-right">
                <div data-toggle="buttons" class="btn-group">
                    <label class="btn btn-sm btn-white"> <input type="radio" id="option5" name="options"> Duplicate</label>
                    <label class="btn btn-sm btn-white"> <input type="radio" id="option6" name="options"> Delete</label>
                    <label class="btn btn-sm btn-white"> <input type="radio" id="option7" name="options"> Move to Archive</label>
                </div>
            </div>
        </div>
        <div class="table-responsive margin-top50">
            <table class="table table-striped sortable">
                <thead>
                <tr>

                    <th class="input"><input type="checkbox" id="checkall" class="i-checks" name="input[]"></th>
                    <th data-sort="string" class="sort">Location — Language</th>
                    <th data-sort="string" class="sort">Category</th>
                    <th data-sort="string" class="sort">Publish Date</th>
                    <th data-sort="string" class="sort">Archive Date</th>
                    <th data-sort="string" class="sort">Created By</th>
                    <th data-sort="string" class="sort">Workflow Status</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td class="input"><input type="checkbox" class="i-checks" name="input[]"></td>
                    <td><a href="#" class="single-link">Master Copy — EN</a></td>
                    <td>Digital SLR (EOS)</td>
                    <td>09 Nov 2018 0:01 (GMT +8)</td>
                    <td>09 Nov 2019 0:01 (GMT +8)</td>
                    <td><a href="#">Theodore Twombly</a></td>
                    <td>Preparer</td>
                </tr>
                <tr>
                    <td class="input"><input type="checkbox" class="i-checks" name="input[]"></td>
                    <td><a href="#" class="single-link">Singapore — EN</a></td>
                    <td>Digital SLR (EOS)</td>
                    <td>09 Nov 2018 0:01 (GMT +8)</td>
                    <td>09 Nov 2019 0:01 (GMT +8)</td>
                    <td><a href="#">Theodore Twombly</a></td>
                    <td>Preparer</td>
                </tr>
                <tr>
                    <td class="input"><input type="checkbox" class="i-checks" name="input[]"></td>
                    <td>
                        <i class="fa fa-lock pull-right"></i>
                        Hongkong — EN
                    </td>
                    <td>Digital SLR (EOS)</td>
                    <td>09 Nov 2018 0:01 (GMT +8)</td>
                    <td>09 Nov 2019 0:01 (GMT +8)</td>
                    <td><a href="#">Theodore Twombly</a></td>
                    <td>Pending</td>
                </tr>
                <tr>
                    <td class="input"><input type="checkbox" class="i-checks" name="input[]"></td>
                    <td>
                        <i class="fa fa-lock pull-right"></i>
                        Hongkong — CH
                    </td>
                    <td>Digital SLR (EOS)</td>
                    <td>09 Nov 2018 0:01 (GMT +8)</td>
                    <td>09 Nov 2019 0:01 (GMT +8)</td>
                    <td><a href="#">Theodore Twombly</a></td>
                    <td>Pending</td>
                </tr>
                <tr>
                    <td class="input"><input type="checkbox" class="i-checks" name="input[]"></td>
                    <td>
                        <i class="fa fa-lock pull-right"></i>
                        India — EN
                    </td>
                    <td>DSLR</td>
                    <td>09 Nov 2018 0:01 (GMT +8)</td>
                    <td>09 Nov 2019 0:01 (GMT +8)</td>
                    <td><a href="#">Theodore Twombly</a></td>
                    <td>Pending</td>
                </tr>
                <tr>
                    <td class="input"><input type="checkbox" class="i-checks" name="input[]"></td>
                    <td>
                        <i class="fa fa-lock pull-right"></i>
                        India — HI
                    </td>
                    <td>DSLR</td>
                    <td>09 Nov 2018 0:01 (GMT +8)</td>
                    <td>09 Nov 2019 0:01 (GMT +8)</td>
                    <td><a href="#">Theodore Twombly</a></td>
                    <td>Pending</td>
                </tr>
                <tr>
                    <td class="input"><input type="checkbox" class="i-checks" name="input[]"></td>
                    <td>
                        <i class="fa fa-lock pull-right"></i>
                        Indonesia — EN
                    </td>
                    <td>DSLR</td>
                    <td>09 Nov 2018 0:01 (GMT +8)</td>
                    <td>09 Nov 2019 0:01 (GMT +8)</td>
                    <td><a href="#">Theodore Twombly</a></td>
                    <td>Pending</td>
                </tr>
                <tr>
                    <td class="input"><input type="checkbox" class="i-checks" name="input[]"></td>
                    <td>
                        <i class="fa fa-lock pull-right"></i>
                        Indonesia — BAHASA
                    </td>
                    <td>DSLR</td>
                    <td>09 Nov 2018 0:01 (GMT +8)</td>
                    <td>09 Nov 2019 0:01 (GMT +8)</td>
                    <td><a href="#">Theodore Twombly</a></td>
                    <td>Pending</td>
                </tr>
                <tr>
                    <td class="input"><input type="checkbox" class="i-checks" name="input[]"></td>
                    <td>
                        <i class="fa fa-lock pull-right"></i>
                        Malaysia  — EN
                    </td>
                    <td>DSLR</td>
                    <td>09 Nov 2018 0:01 (GMT +8)</td>
                    <td>09 Nov 2019 0:01 (GMT +8)</td>
                    <td><a href="#">Theodore Twombly</a></td>
                    <td>Pending</td>
                </tr>
                <tr>
                    <td class="input"><input type="checkbox" class="i-checks" name="input[]"></td>
                    <td>
                        <i class="fa fa-lock pull-right"></i>
                        Malaysia  — Malay
                    </td>
                    <td>DSLR</td>
                    <td>09 Nov 2018 0:01 (GMT +8)</td>
                    <td>09 Nov 2019 0:01 (GMT +8)</td>
                    <td><a href="#">Theodore Twombly</a></td>
                    <td>Pending</td>
                </tr>
                </tbody>
            </table>
        </div>

        <!-- Start Pagination -->
        <div class="post-pagination">
            <ul>
                <li class="nav"><a href="#"><span class="fa fa-angle-left"></span></a></li>
                <li class="page">1</li>
                <li>of</li>
                <li class="count">24</li>
                <li class="nav"><a href="#"><span class="fa fa-angle-right"></span></a></li>
            </ul>
        </div>
        <!-- End Pagination -->

    </div>
    <!-- End Ibox -->
</div>
<!-- End Content -->
</body>
</html>