<%--
  Created by IntelliJ IDEA.
  User: muyalif
  Date: 16/12/17
  Time: 03.35
--%>

<%@ page import="canon.enums.Status" contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title><g:message code="admin.dashboard.title" default="Admin Dashboard"/></title>
    <meta name="layout" content="admin"/>
</head>

<body>
<!-- Start Page Heading -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Create a new product</h2>
        <ol class="breadcrumb">
            <li>
                <g:link controller="adminProduct" action="index">Product</g:link>
            </li>
            <li>
                <g:link controller="adminProduct" action="index">Product List</g:link>
            </li>
            <li class="active">
                <strong>Add New Product</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2"></div>
</div>
<!-- End Page Heading -->
<g:hasErrors bean="${this.product}">
    <ul class="errors" role="alert">
        <g:eachError bean="${this.product}" var="error">
            <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
        </g:eachError>
    </ul>
</g:hasErrors>
<g:form controller="adminProduct" action="update" id="${product?.id}" method="PUT">
    <g:render template="form_content"
              model="[product: product, languageList: languageList, siteList: siteList, consumerList: consumerList, businessList: businessList, productRelationMap: productRelationMap]" />
</g:form>
<content tag="footer">
    <div class="pull-right">
        <button class="btn btn-default margin-left10">Cancel</button>
        <button class="btn btn-primary margin-left10 js-submit-form" data-flow="NO_CHANGES">Save Changes</button>
        <button class="btn btn-primary margin-left10 js-submit-form" data-flow="NEXT">Submit</button>
    </div>
</content>
</body>
</html>