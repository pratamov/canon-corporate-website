<script id="selling-point-template" type="x-tmpl-mustache">
    <div class="input-group crud margin-bottom10 {{timestamp}} ">
        <input type="text" name="sellingPoints[]" class="form-control" style="background-color:white;" readonly value="{{content}}" >
        <div class="input-group-btn">
            <button class="btn btn-default selling-point-remove" type="button" temp="{{timestamp}}"><span class="fa fa-trash"></span></button>
        </div>
    </div>
</script>