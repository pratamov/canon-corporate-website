<%--
  Created by IntelliJ IDEA.
  User: rifai
  Date: 10/12/17
  Time: 6:18 PM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title><g:message code="public.login.title" default="Login - Canon"/></title>
    <meta name="layout" content="canon"/>
</head>

<body>
<!-- Start Wrap Component -->
<div class="wrap-component">
    <!-- Start Content -->
    <div class="inner-page breadcrumb-line">
        <div class="container">
            <!-- Breadcrumbs -->
            <ol class="breadcrumb pull-left err">
                <li><a href="#">Shopping Cart</a></li>
                <li class="sub active">Login</li>
            </ol>
        </div>
    </div>
    <!-- End Content -->

    <div class="clearfix"></div>

    <!-- Start Content -->
    <div class="section section-login">
        <div class="container">
            <h1 class="title-section text-center">How would you like to check out today?</h1>
            <hr  class="first-line" />

            <div class="row">
                <div class="col-md-12 col-md-offset-0 col-sm-6 col-sm-offset-3">

                    <div class="row">
                        <div class="col-md-4">
                            <!-- Start Box -->
                            <div class="box-panel match-height text-left">
                                <h3 class="title">Sign In</h3>
                                <div class="content-login">
                                    <div class="form-group">
                                        <input type="email" class="form-control" placeholder="Email Address">
                                    </div>
                                    <div class="form-group margin-bottom20">
                                        <input type="password" class="form-control" placeholder="Password">
                                    </div>
                                    <div class="checkbox">
                                        <input id="checkbox5" type="checkbox">
                                        <label for="checkbox5">
                                            Keep me logged in
                                        </label>
                                    </div>
                                    <p>
                                        <a href="#" class="single-link">Forgot Password</a>
                                    </p>
                                    <div class="content-btn">
                                        <button class="btn btn-primary">Member Checkout</button>
                                    </div>
                                </div>
                            </div>
                            <!-- End Box -->
                        </div>
                        <div class="col-md-4">
                            <!-- Start Box -->
                            <div class="box-panel match-height text-left">
                                <h3 class="title">Guest</h3>
                                <div class="content-login">
                                    <p>Join Canon newsletter you will get</p>
                                    <ul class="list-normal margin-bottom30">
                                        <li>Stay up-to-date with Canon new products and events.</li>
                                        <li>Get notified of the latest promotions.</li>
                                    </ul>

                                    <div class="form-group margin-bottom20">
                                        <input type="email" class="form-control" placeholder="Email Address">
                                    </div>
                                    <div class="checkbox">
                                        <input id="checkbox6" type="checkbox">
                                        <label for="checkbox6">
                                            I agree that Canon my collect, use, disclose and/or transfer my personal data in accordance with Canon latest <a href="#" class="single-link">Privacy policy</a>
                                        </label>
                                    </div>
                                    <div class="content-btn">
                                        <button class="btn btn-primary">Guest Checkout</button>
                                    </div>
                                </div>
                            </div>
                            <!-- End Box -->
                        </div>
                        <div class="col-md-4">
                            <!-- Start Box -->
                            <div class="box-panel match-height text-left padding-bottom0">
                                <h3 class="title">Other Methods</h3>
                                <div class="content-login">
                                    <ul class="list-method">
                                        <asset:stylesheet src="style.css"/>
                                        <li><asset:image src="brand/img2.jpg" alt="Paypal"/></li>
                                        <li><asset:image src="brand/img1.jpg" alt="Paypal"/></li>
                                        <li><asset:image src="brand/img3.jpg" alt="Paypal"/></li>
                                        <li><asset:image src="brand/img4.jpg" alt="Paypal"/></li>
                                    </ul>
                                </div>
                            </div>
                            <!-- End Box -->
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <!-- End Content -->
</div>
<!-- End Wrap Component -->
</body>
</html>