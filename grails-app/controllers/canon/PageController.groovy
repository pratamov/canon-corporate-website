package canon

import canon.enums.CategoryType
import canon.enums.ComponentType
import canon.enums.Segment
import canon.enums.Status

class PageController {

    def pageService

    def index() {
        CategoryType categoryType = CategoryType.ARTICLE
        Segment segment = Segment.CONSUMER
        try {
            String categoryTypeName = params.categoryType?.trim()
            if (params.categoryType) categoryType = CategoryType.valueOf(categoryTypeName?.toUpperCase())
            if (params.segment) segment = Segment.getBySlug(params.segment as String)
        } catch (Exception e) {
            log.warn(e)
        }

        List<Category> categories = Category.createCriteria().list {
            and {
                eq("categoryType", categoryType)
                eq("segment", segment)
            }
            order("categoryOrder", "asc")
        }

        if(categoryType.equals(CategoryType.SOLUTION)){
            businessSolution(categoryType, categories)
        }

        List<canon.model.Component> components = new ArrayList<>()
        Map<String, Object> featuredConf = new HashMap<>()
        featuredConf["hideTitle"] = Boolean.TRUE
        featuredConf["contentType"] = categoryType.name() // Use CategoryType as ContentType
        featuredConf["featuredOnly"] = Boolean.TRUE
        featuredConf["hideViewAll"] = Boolean.TRUE
        canon.model.Component featured = new canon.model.Component(ComponentType.FEATURED_ARTICLES, featuredConf, 0)
        components.add(featured)

        categories?.each {
            Map<String, Object> cmpConf = new HashMap<>()
            cmpConf["contentType"] = categoryType.name() // Use CategoryType as ContentType
            cmpConf["categoryId"] = it.id
            canon.model.Component cmp = new canon.model.Component(ComponentType.FEATURED_ARTICLES, cmpConf, 0)
            components.add(cmp)
        }

        model: [
                categoryType: categoryType,
                categories: categories,
                components: components
        ]
    }

    def businessSolution(CategoryType categoryType, List<Category> categories){
        List<canon.model.Component> components = new ArrayList<>()
        categories?.each {
            Map<String, Object> cmpConf = new HashMap<>()
            cmpConf["contentType"] = categoryType.name() // Use CategoryType as ContentType
            cmpConf["categoryId"] = it.id
            cmpConf["hideViewAll"] = Boolean.TRUE
            canon.model.Component cmp = new canon.model.Component(ComponentType.FEATURED_ARTICLES_SOLUTION, cmpConf, 0)
            components.add(cmp)
        }

        render view: "businessSolution", model: [
                categoryType: categoryType,
                categories: categories,
                components: components
        ]
    }

    def list() {
        params.lang = params?.lang ?: "en"
        CategoryType categoryType = CategoryType.ARTICLE
        Segment segment = Segment.CONSUMER
        try {
            String categoryTypeName = params.categoryType?.trim()
            if (params.categoryType) categoryType = CategoryType.valueOf(categoryTypeName?.toUpperCase())
            if (params.segment) segment = Segment.getBySlug(params.segment as String)
        } catch (Exception e) {
            log.warn(e)
        }

        def paramsOverride = [:]
        def searchResult = pageService.searchArticles(params, paramsOverride)

        List<Category> categories = Category.createCriteria().list {
            and {
                eq("categoryType", categoryType)
                eq("segment", segment)
            }
            order("categoryOrder", "asc")
        }

        model: [
                categoryType: categoryType,
                resultList: searchResult?.resultList,
                resultAggs: searchResult?.resultAggs,
                resultCount: searchResult?.resultCount,
                categories: categories
        ]
    }

    def show() {
        Date date = new Date()
        Site site = Site.findByCode(params.get("site") as String)
        CategoryType categoryType = CategoryType.ARTICLE
        Segment segment = Segment.CONSUMER
        try {
            String categoryTypeName = params.categoryType?.trim()
            if (params.categoryType) categoryType = CategoryType.valueOf(categoryTypeName?.toUpperCase())
            if (params.segment) segment = Segment.getBySlug(params.segment as String)
        } catch (Exception e) {
            log.warn(e)
        }

        Page page = Page.createCriteria().get {
            createAlias("pageDetails", "ps")
            and {
                eq("slug", params?.id)
                eq("ps.site", site)
                eq("status", Status.PUBLISHED)
                le("publishedDate", date)
            }
            order("publishedDate", "desc")
            maxResults(1)
        }

        def pageComponents = PageComponent.createCriteria().list {
            and{
                eq("page", page)
                eq("status", Status.PUBLISHED)
            }
            order("component.compOrder", "asc")
        }

        List<Category> categories = PageCategory.createCriteria().list {
            createAlias("category", "c")
            projections {
                property("category")
            }
            and {
                eq("c.categoryType", categoryType)
                eq("c.segment", segment)
            }
            order("c.categoryOrder", "asc")
        }

        def suggestedArticles = Page.createCriteria().list {
            createAlias("pageDetails", "ps")
            createAlias("pageCategories", "pc")
            and {
                ne("id", page?.id)
                eq("ps.site", site)
                eq("status", Status.PUBLISHED)
                le("publishedDate", date)
                if(categories) {
                    'in'("pc.category", categories)
                }
            }
            order("publishedDate", "desc")
            maxResults(3)
        }

        def relatedProducts = PageProduct.createCriteria().list {
            createAlias("product", "p")
            and {
                eq("page", page)
                eq("p.status", Status.PUBLISHED)
            }
        }

        model: [
                page : page,
                pageComponents : pageComponents,
                relatedProducts : relatedProducts,
                suggestedArticles: suggestedArticles
        ]
    }
}
