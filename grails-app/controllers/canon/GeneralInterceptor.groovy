package canon

import canon.enums.Segment
import org.apache.commons.lang.StringUtils


class GeneralInterceptor {

    def generalService
    def paramsLookupService

    public GeneralInterceptor() {
        matchAll().excludes(controller: "admin")
                .excludes(controller: "admin(.*)")
                .excludes(controller: ~/(login|logout)/)
                .excludes(uri: "/robots.txt")
                .excludes(uri: "/manifest.json")
    }

    boolean before() {
        Site siteSelected = paramsLookupService.checkSite(params?.site as String)
        if (!siteSelected) {
            siteSelected = generalService.getSiteOfCountry(params?.country as String)
        }

        if (!siteSelected) siteSelected = Site.findByIsDefault(Boolean.TRUE)
        if (!params.site) {
            params.site = siteSelected?.code
        }

        Language languageSelected = paramsLookupService.getLanguageSelected(params?.lang as String)
        String lang = languageSelected?.id
        if (StringUtils.isBlank(lang)) {
            lang = siteSelected?.defaultLangCode ?: "en" // default language: en = English
        }

        if (!params.segment) params.segment = Segment.CONSUMER.slug()

        true
    }

    boolean after() {
        Site siteSelected = paramsLookupService.checkSite(params?.site as String)
        List<Language> siteLanguages = new ArrayList<>()
        if (siteSelected) {
            siteLanguages = generalService.getSiteLanguages(siteSelected.code)
        }
        Language languageSelected = paramsLookupService.getLanguageSelected(params?.lang as String)

        String lang = languageSelected?.id
        if (StringUtils.isBlank(lang)) {
            lang = siteSelected?.defaultLangCode ?: "en" // default language: en = English
        }

        Segment segment = Segment.getBySlug(params?.segment ?: 'consumer')

        if (model != null) {
            model.put("siteSelected", siteSelected?.name)
            model.put("languageSelected", lang)
            model.put("headerMenu", generalService.getHeaderMenu(siteSelected?.code, lang, segment.slug()))
            model.put("appSettings", generalService.getAppSettings(lang, siteSelected?.code))
        }

        true
    }

    void afterView() {
        // no-op
    }
}
