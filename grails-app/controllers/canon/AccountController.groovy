package canon

class AccountController {

    def login() {
        if (session["user"]) {
            redirect(uri: "/")
        }
        render(view: "/account/login", model: [reqForwardUri: params.reqForwardUri])
    }
}
