package canon

import canon.enums.Segment
import canon.constant.SettingCodeConstant
import canon.enums.Status
import canon.utils.JsonUtils
import org.apache.commons.lang.StringUtils
import org.json.JSONArray
import org.json.JSONObject

class HomeController {

    def index() {
        params?.lang = params?.lang ?: "en"
        params?.site = params?.site
        Date date = new Date()
        Site site = Site.findByCode(params.get("site") as String)
        Segment segment = Segment.CONSUMER
        try {
            if (StringUtils.isNotBlank(params?.segment)) {
                segment = Segment.getBySlug(params?.segment as String)
            }
        } catch (Exception e) {}

        Page homepage = Page.createCriteria().get {
            createAlias("pageDetails", "ps")
            and {
                eq("ps.site", site)
                eq("segment", segment)
                eq("isHomepage", Boolean.TRUE)
                eq("status", Status.PUBLISHED)
                le("publishedDate", date)
            }
            order("publishedDate", "desc")
            maxResults(1)
        }
        def pageComponents = PageComponent.createCriteria().list {
            and {
                eq("page", homepage)
                eq("status", Status.PUBLISHED)
            }
            order("component.compOrder", "asc")
        }

        render(view: "index", model: [
                homepage: homepage,
                pageComponents: pageComponents
        ])
    }

    def regionSelection() {
        Setting siteSetting = Setting.findBySettingCode(SettingCodeConstant.REGION_SELECTOR)

        JSONArray sites
        if(siteSetting && JsonUtils.isJSONValid(siteSetting?.settingValue)) {
            sites = new JSONArray(siteSetting?.settingValue)
        } else {
            sites = new JSONArray(SettingCodeConstant.REGION_SELECTOR_DEFAULT)
        }

        JSONObject asiaRegion = null
        List<JSONObject> otherRegion = new ArrayList<>()
        for(int i = 0; i < sites?.length(); i++) {
            JSONObject json = sites.get(i)
            if ("Asia".equals(json.get("name"))) {
                asiaRegion = json
            } else {
                otherRegion.add(json)
            }
        }

        render(view: "regionSelection", model: [
                "asiaRegion": asiaRegion,
                "otherRegions": otherRegion
        ])
    }
}
