package canon.admin

import canon.Flow
import grails.converters.JSON
import grails.transaction.Transactional

class AdminFlowController {

    def adminFlowService

    def index() { }

    @Transactional
    def getWorkflowByFlow(Long flowId) {
        Flow flow = Flow.findById(flowId)

        def result = adminFlowService.getWorkflowByFlow(flow)

        render result as JSON
    }
}
