package canon.admin

import java.text.SimpleDateFormat
import grails.converters.JSON
import grails.transaction.Transactional
import canon.Faq
import canon.FaqTranslation
import canon.FaqItem
import canon.FaqItemTranslation
import canon.FaqItemAnswer
import canon.Site
import canon.Language
import canon.User
import canon.AdminFaqService

class AdminFaqController {


    /*
    def index() {
    	redirect(action: "faqList")
    }

    def faqList(){

        def sites = Site.findAll()
        [
            sites: sites
        ]
        
    }

    def faqCategoryList(){

    	def id = params?.int("id", 1)
        def site = Site.findById(id)
        def categories = Faq.findAllBySite(site)

        [
            categories: categories
        ]

    }

    def faqCategoryDetail(){

    	def id = params?.int("id", 1)
        def categoryDetail = []

        def faqContents = []

        def category = Faq.findById(id)

        categoryDetail = [
            contentEN: category.name,
            contentZH: FaqTranslation.findByFaq(category)?.name
        ]

        def labels = FaqItem.withCriteria {
            eq("faq", category)
            projections {
                distinct("label")
            }
        }

        def questions = FaqItem.findAllByFaq(category)
        Language en = Language.findById("en")
        Language zh_CN = Language.findById("zh_CN")
        for (question in questions) {
            faqContents << [
                label: question.label,
                contentEN: question.content,
                contentZH: FaqItemTranslation.findByFaqItem(question)?.content,
                answerEN: FaqItemAnswer.findByFaqItemAndLanguage(question, en)?.content,
                answerZH: FaqItemAnswer.findByFaqItemAndLanguage(question, zh_CN)?.content
            ]
        }

        [
            categoryDetail: categoryDetail,
            faqContents: faqContents,
            labels: labels
        ]

    }


    def getSites(){

        def sites = Site.findAll()
        [
            sites: sites
        ]

    }

    def getFaqs(){

        def id = params?.int("id", 1)
        def site = Site.findById(id)
        def faqs = Faq.findAllBySite(site)

        [
            faqs: faqs
        ]

    }

    def getFaqItems(){

        def id = params?.int("id", 1)
        def faq = Faq.findById(id)
        def faqItems = FaqItem.findByFaq(faq)


    }



    @Transactional
    def updateFaq(){

        Faq faq = Faq.findById(params?.faq_id)
        faq = bindFaq(params, faq)
        faq.save flush: true, failOnError: true

        FaqTranslation faqTranslation = Faq.findByFaq(faq)
        faqTranslation = bindFaqTranslation(params, faqTranslation)
        faq.save flush: true, failOnError: true

        redirect action: "index"

    }

    @Transactional
    def updateFaqItem(){

        FaqItem faqItem = FaqItem.findById(params?.faqItem?.id)
        faqItem = bindFaqItem(faqItem)
        faqItem.save flush: true, failOnError: true

        FaqItemAnswer faqItemAnswer = FaqItemAnswer.findByFaqItem(faqItem)
        faqItemAnswer = bindFaqItemAnswer(faqItem)
        faqItemAnswer.save flush: true, failOnError: true

        FaqItemTranslation faqItemTranslation = FaqItemTranslation.findByFaqItem(faqItem)
        faqItemTranslation = bindFaqItemTranslation(faqItem)
        faqItemTranslation.save flush: true, failOnError: true

        redirect action: "index"
    }

    */

    static allowedMethods = [
        countryList: "GET", 
        categoryList: "GET", 
        detail: "GET", 
        addCategory: "POST", 
        addQuestion: "POST"
    ]

    AdminFaqService adminFaqService
    def springSecurityService

    def index() {
        redirect(action: "countryList")
    }

    def countryList() {
        def sites = adminFaqService.getSites()

        [
            sites: sites
        ]
    }

    def categoryList() {
        def siteId = params?.int("id", 1)
        def site = Site.findById(siteId)

        def archived = (params?.archived != null && (params?.archived == 1 || params?.archived == 'true'))
        def categories = adminFaqService.getFaqs(siteId, archived)

        def allCount = Faq.countBySite(site)
        def archivedCount = Faq.countBySiteAndArchivedDateLessThan(site, new Date())

        [
            site: site,
            categories: categories,
            archivedCount: archivedCount,
            unarchivedCount: allCount-archivedCount,
            archived: archived
        ]
    }

    def detail() {
        def faqId = params?.int("id", 1)
        def faq = Faq.findById(faqId)
        def category = adminFaqService.getFaq(faqId)
        def labels = adminFaqService.getFaqItemLabels(faqId)
        def contents = adminFaqService.getFaqItems(faqId)

        log.info labels.size()
        log.info contents.size()

        [
            faqId: faqId,
            faq: faq,
            category: category,
            labels: labels,
            contents: contents
        ]
    }

    @Transactional
    def addCategory(){
        
        User user = springSecurityService.getCurrentUser()

        Language zh_CN = Language.findById('zh_CN')
        Language en = Language.findById('en')

        def site_id = params?.site_id
        def faq_name = params?.faq_name
        def faqTranslation_name = params?.faqTranslation_name

        Site site = Site.findById(site_id)

        Faq faq = new Faq(
            site: site,
            name: faq_name,
            createdBy: user,
            modifiedBy: user,
            dateCreated: new Date(), 
            lastUpdated: new Date()
        )
        faq.save(failOnError: true)

        FaqTranslation faqTranslation = new FaqTranslation(
            faq: faq,
            language: zh_CN,
            name: faqTranslation_name,
            createdBy: user,
            modifiedBy: user,
            dateCreated: new Date(), 
            lastUpdated: new Date()
        )
        faqTranslation.save(failOnError: true)

        redirect(action: "detail", params: [id: faq.id])
    }

    @Transactional
    def addQuestion(){
        
        User user = springSecurityService.getCurrentUser()

        Language zh_CN = Language.findById('zh_CN')
        Language en = Language.findById('en')

        def faq_id = params?.faq_id
        def faqItem_publishedDate = params?.faqItem_publishedDate
        def faqItem_archivedDate = params?.faqItem_archivedDate
        def faqItem_label = params?.faqItem_label
        def faqItem_content = params?.faqItem_content
        def faqItemTranslation_content = params?.faqItemTranslation_content
        def faqItemAnswerEN_content = params?.faqItemAnswerEN_content
        def faqItemAnswerCH_content = params?.faqItemAnswerCH_content

        log.info "faqItem_publishedDate " + faqItem_publishedDate
        log.info "faqItem_archivedDate " + faqItem_archivedDate

        Faq faq = Faq.findById(faq_id)

        FaqItem faqItem = new FaqItem(
            faq: faq, 
            label: faqItem_label, 
            content: faqItem_content, 
            createdBy: user,
            modifiedBy: user,
            dateCreated: new Date(), 
            lastUpdated: new Date()
        )
        try{
            faqItem.publishedDate = new SimpleDateFormat("dd MMM YYYY").parse(faqItem_publishedDate)
        } catch (Exception e) {}
        try{
            faqItem.archivedDate = new SimpleDateFormat("dd MMM YYYY").parse(faqItem_archivedDate)
        } catch (Exception e) {}

        faqItem.save(failOnError: true)

        FaqItemTranslation faqItemTranslation = new FaqItemTranslation(
            faqItem: faqItem, 
            language: zh_CN, 
            label: faqItem_label, 
            content: faqItemTranslation_content,
            createdBy: user,
            modifiedBy: user,
            dateCreated: new Date(), 
            lastUpdated: new Date()
        )
        faqItemTranslation.save(failOnError: true)

        FaqItemAnswer faqItemAnswerEN = new FaqItemAnswer(
            faqItem: faqItem, 
            language: en, 
            content: faqItemAnswerEN_content,
            createdBy: user,
            modifiedBy: user,
            dateCreated: new Date(),
            lastUpdated: new Date()
        )
        faqItemAnswerEN.save(failOnError: true)

        FaqItemAnswer faqItemAnswerZH = new FaqItemAnswer(
            faqItem: faqItem, 
            language: zh_CN, 
            content: faqItemAnswerCH_content,
            createdBy: user,
            modifiedBy: user,
            dateCreated: new Date(),
            lastUpdated: new Date()
        )
        faqItemAnswerZH.save(failOnError: true)

        redirect(action: "detail", params: [id: faq_id])
    }

    @Transactional
    def updateCategory(){

        User user = springSecurityService.getCurrentUser()

        Language zh_CN = Language.findById('zh_CN')
        Language en = Language.findById('en')

        def faq_id = params?.faq_id
        def faq_name = params?.faq_name
        def faqTranslation_name = params?.faqTranslation_name

        Faq faq = Faq.findById(faq_id)
        faq.name = faq_name
        faq.save(failOnError: true)

        FaqTranslation faqTranslation = FaqTranslation.findByFaqAndLanguage(faq, zh_CN)
        faqTranslation.name = faqTranslation_name
        faqTranslation.save(failOnError: true)

        def labels = params?.labels


        /*
        labels.each{ before, after ->

            log.info "before " + before
            log.info "after " + after

            FaqItem faqItem = 
                FaqItem.executeUpdate("update FaqItem set label = " + after + " where label = " + before)

            FaqItemTranslation faqItemTranslation = 
                FaqItemTranslation.executeUpdate("update FaqItemTranslation set label = " + after + " where label = " + before)

        }

        */

        redirect(action: "detail", params: [id: faq.id])

    }

    @Transactional
    def updateQuestion(){
        User user = springSecurityService.getCurrentUser()

        Language zh_CN = Language.findById('zh_CN')
        Language en = Language.findById('en')

        def faqItem_id = params?.faqItem_id
        def faqItem_publishedDate = params?.faqItem_publishedDate
        def faqItem_archivedDate = params?.faqItem_archivedDate
        def faqItem_label = params?.faqItem_label
        def faqItem_content = params?.faqItem_content
        def faqItemTranslation_content = params?.faqItemTranslation_content
        def faqItemAnswerEN_content = params?.faqItemAnswerEN_content
        def faqItemAnswerCH_content = params?.faqItemAnswerCH_content

        log.info "faqItem_publishedDate " + faqItem_publishedDate
        log.info "faqItem_archivedDate " + faqItem_archivedDate

        FaqItem faqItem = FaqItem.findById(faqItem_id)
        try{
            faqItem.publishedDate = new SimpleDateFormat("dd MMM yyyy").parse(faqItem_publishedDate)
            log.info faqItem.publishedDate
        } catch (Exception e) {
            faqItem.publishedDate = null
        }
        try{
            faqItem.archivedDate = new SimpleDateFormat("dd MMM yyyy").parse(faqItem_archivedDate)
            log.info faqItem.archivedDate
        } catch (Exception e) {
            faqItem.archivedDate = null
        }
        faqItem.label = faqItem_label
        faqItem.content = faqItem_content
        faqItem.lastUpdated = new Date()
        faqItem.modifiedBy = user
        faqItem.save(failOnError: true)

        FaqItemTranslation faqItemTranslation = FaqItemTranslation.findByFaqItem(faqItem)
        faqItemTranslation.content = faqItemTranslation_content
        faqItemTranslation.lastUpdated = new Date()
        faqItemTranslation.modifiedBy = user
        faqItemTranslation.save(failOnError: true)


        FaqItemAnswer faqItemAnswerEN = FaqItemAnswer.findByFaqItemAndLanguage(faqItem, en);
        faqItemAnswerEN.content = faqItemAnswerEN_content
        faqItemAnswerEN.lastUpdated = new Date()
        faqItemAnswerEN.modifiedBy = user
        faqItemAnswerEN.save(failOnError: true)

        FaqItemAnswer faqItemAnswerCH = FaqItemAnswer.findByFaqItemAndLanguage(faqItem, zh_CN);
        faqItemAnswerCH.content = faqItemAnswerCH_content
        faqItemAnswerCH.lastUpdated = new Date()
        faqItemAnswerCH.modifiedBy = user
        faqItemAnswerCH.save(failOnError: true)

        redirect(action: "detail", params: [id: faqItem.faq.id])
    }

    @Transactional
    def archiveQuestion(){

        def faqItem_id = params?.faqItem_id
        FaqItem faqItem = FaqItem.findById(faqItem_id)

        //FaqItemTranslation.findByFaqItem(faqItem).delete()
        //FaqItemAnswer.findByFaqItem(faqItem).delete()
        
        //faqItem.delete()
        faqItem.archivedDate = new Date()
        faqItem.save(failOnError: true)

        redirect(action: "detail", params: [id: faqItem.faq.id])
    }

    @Transactional
    def archiveCategory(){
        User user = springSecurityService.getCurrentUser()

        def site_id = params?.site_id

        params?.faq_id_list.each {
            Faq faq = Faq.findById(it)
            faq.archivedDate = new Date()
            faq.modifiedBy = user
            faq.save()
        }

        redirect(action: "categoryList", params: [id: site_id])
    }

}
