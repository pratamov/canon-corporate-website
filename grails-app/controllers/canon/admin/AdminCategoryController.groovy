package canon.admin

import canon.Site
import canon.enums.CategoryType
import canon.model.DataResult
import grails.converters.JSON

class AdminCategoryController {

    def adminCategoryService

    def index() { }

    def getLowestLevelProductCategories(){
        String idStr = params?.siteId

        Long siteId = null
        Site site = null
        if(idStr && idStr.isLong()) {
            siteId = Long.valueOf(idStr)
            site = Site.findById(siteId)
        }

        def data = []
        if(site) {
            data = adminCategoryService.getLowestLevelCategories(site, CategoryType.PRODUCT)
        }

        DataResult result = new DataResult(Boolean.TRUE, data)

        render result.toMap() as JSON
    }
}
