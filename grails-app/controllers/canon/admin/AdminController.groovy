package canon.admin

import canon.IndexingJob

class AdminController {

    def index() { }

    def indexing() {
        Boolean articles = params?.boolean("articles") ?: Boolean.FALSE
        Boolean products = params?.boolean("products") ?: Boolean.FALSE
        Boolean publishedOnly = Boolean.TRUE // default to true
        if (params?.published && !params?.boolean("published")) {
            publishedOnly = Boolean.FALSE
        }

        Long siteId = params?.long("site")
        IndexingJob.triggerNow([articles: articles, products: products, published: publishedOnly, site: siteId])

        render "IndexingJob triggered."
    }
}
