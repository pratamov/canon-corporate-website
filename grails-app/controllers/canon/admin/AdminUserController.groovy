package canon.admin

import grails.converters.JSON
import grails.transaction.Transactional

class AdminUserController {

    def adminUserService

    def index() { }

    @Transactional
    def findAll() {
        render adminUserService.findAllUser() as JSON
    }
}
