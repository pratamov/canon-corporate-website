package canon.admin

import canon.Flow
import canon.Language
import canon.Product
import canon.ProductRelation
import canon.ProductSellingPoint
import canon.ProductDetail
import canon.Site
import canon.User
import canon.WorkFlow
import canon.enums.ComponentType
import canon.enums.RelationType
import canon.enums.Segment
import canon.enums.Status
import canon.model.Component
import canon.model.DataResult
import grails.converters.JSON
import grails.transaction.Transactional

@Transactional(readOnly = true)
class AdminProductController {

    static allowedMethods = [archiveProducts: "POST"]

    def adminProductService
    def adminCategoryService
    def springSecurityService

    def index() {
        if (!params.get("status")) params.status = Status.PUBLISHED.name()

        String dateFormat = message(code: 'admin.default.view.date.format', default: 'd MMM yyyy')
        Date dateFrom = params?.dateFrom ? params?.date('dateFrom', dateFormat) : null
        Date dateTo = params?.dateTo ? params?.date('dateTo', dateFormat) + 1 : null

        def paramsOverride = [:]
        paramsOverride.put("dateFrom", dateFrom)
        paramsOverride.put("dateTo", dateTo)

        def productList = adminProductService.getProductList(params, paramsOverride)
        def productCount = adminProductService.getProductCount(params, paramsOverride)

        def countByStatus = [:]
        def statusList = [Status.PUBLISHED, Status.DRAFT, Status.ARCHIVED]
        statusList.each {
            countByStatus.put(it.name(), adminProductService.getProductCount(params, [status: it.name(), dateFrom: dateFrom, dateTo: dateTo]))
        }

        [
                productList: productList,
                productCount: productCount,
                statusList: statusList,
                countByStatus: countByStatus
        ]
    }

    def create() {
        User user = springSecurityService.getCurrentUser()

        Site masterDataSite = Site.findByIsDefault(Boolean.TRUE)

        Product newProduct = new Product()
        newProduct.site = masterDataSite

        newProduct.addToProductDetails(new ProductDetail(site: masterDataSite))

        [
                product: newProduct,
                user: user,
                flows: Flow.list(sort: "name"),
                languageList: Language.list(sort: "name"),
                siteList: Site.list(sort: "name")
        ]
    }

    def location() {
        Product product = Product.findById(params.long("id"))
        User user = springSecurityService.getCurrentUser()
        def flow = Flow.findByName("Product Workflow")
        def workFlowList = WorkFlow.findAllByFlow(flow)
        def authorList = User.list(sort: "username")
        def currentWorkFlow = WorkFlow.findByFlowAndIsStart(flow, Boolean.FALSE)
        def languageList = Language.list(sort: "name")

        [
                product: product,
                user: user,
                flow: flow,
                workFlowList: workFlowList,
                authorList: authorList,
                currentWorkFlow: currentWorkFlow,
                languageList: languageList
        ]
    }

    def show() {
        Product product = Product.findById(params.long("id"))
        User user = springSecurityService.getCurrentUser()
        def flow = Flow.findByName("Product Workflow")
        def workFlowList = WorkFlow.findAllByFlow(flow)
        def authorList = User.list(sort: "username")
        def currentWorkFlow = WorkFlow.findByFlowAndIsStart(flow, Boolean.FALSE)
        def languageList = Language.list(sort: "name")

        [
                product: product,
                user: user,
                flow: flow,
                workFlowList: workFlowList,
                authorList: authorList,
                currentWorkFlow: currentWorkFlow,
                languageList: languageList
        ]
    }

    @Deprecated
    def delete() {

    }

    @Transactional
    def save(Product prd) {
        if (prd == null) {
            transactionStatus.setRollbackOnly()
            return
        }

        Product product = adminProductService.saveProduct(params, prd)

        if (product.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond product.errors, view:'create'
            return
        }

        if (params?.flow as String == 'NEXT'){
            redirect action: "index"
        }else {
            redirect(action: "edit", id: product?.id)
        }
    }

    def edit(Product product) {
        User user = springSecurityService.getCurrentUser()
        def flow = Flow.findByName("Product Workflow")
        def workFlowList = WorkFlow.findAllByFlow(flow)
        def authorList = User.list(sort: "username")
        def currentWorkFlow = WorkFlow.findByFlowAndIsStart(flow, Boolean.TRUE)
        def languageList = Language.list(sort: "name")

        List<Site> siteList = Site.list(sort: "name")

        def consumerList = adminCategoryService.getLowestLevelCategories(Segment.CONSUMER)
        def businessList = adminCategoryService.getLowestLevelCategories(Segment.BUSINESS)

        def relatedProducts =[]
        def relatedConsumable =[]
        def relatedAccessories =[]
        product?.productRelations?.each { ProductRelation pr ->
            if (pr.relationType == RelationType.PRODUCT) relatedProducts.add(pr)
            else if (pr.relationType == RelationType.CONSUMABLE) relatedConsumable.add(pr)
            else relatedAccessories.add(pr)
        }

        [
                product: product,
                user: user,
                flow: flow,
                workFlowList: workFlowList,
                authorList: authorList,
                currentWorkFlow: currentWorkFlow,
                languageList: languageList,
                siteList: siteList,
                consumerList: consumerList,
                businessList: businessList,
                productRelationMap: [
                        product: relatedProducts,
                        consumable: relatedConsumable,
                        accessories: relatedAccessories
                ]
        ]
    }

    @Transactional
    def update(Product prd) {
        if (prd == null) {
            transactionStatus.setRollbackOnly()
            return
        }

        Product product = adminProductService.updateProduct(params, prd)

        if (product.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond product.errors, view:'edit'
            return
        }

        if (params?.flow as String == 'NEXT'){
            redirect action: "index"
        }else {
            redirect(action: "edit", id: product?.id)
        }
    }

    @Transactional
    def archiveProducts() {
        adminProductService.archiveProducts(params?.list('ids'))
    }
	
	@Transactional
	def restoreProducts() {
		adminProductService.restoreProducts(params?.list('ids'))
	}

    def inline() {
        List<Component> componentList = new ArrayList<>()
        componentList.add(new Component(ComponentType.IMAGE,
                [title: "Photo 1: A vast poppy field against the evening sun",
                 caption: "EOS 5D Mark III/ EF24-105mm f/4L IS USM/ FL: 47mm/ Aperture-priority AE (f/16, 1/25 sec, EV+1.3)/ ISO 200/ WB: White fluorescent light<br>Date of shoot: Late May<br>Photo by Rika Takemoto",
                 copyright: "",
                 imageUrl: "/assets/article/img5.jpg",
                 mediaId: null], 0))
        componentList.add(new Component(ComponentType.TEXT, [content: "Step 1: Shoot downward from a high position<br />\n" +
                "                                        I aimed to create a dramatic image by framing the field of poppies together with the evening sun. The field extended all the way into the background, and in order to bring out that sense of scale, I initially shot at eye level, using a tripod. However, that made the elements in the frame look very scattered.\n" +
                "                                        I realized that I needed to create a point of interest in the frame by framing the image so that the poppies in front of me appeared larger and closer. Therefore, I found a location where there were some tall poppies directly in front of me, and also adjusted my tripod so that it was at waist level.\n" +
                "                                        I had a few different shooting positions in mind, but I eventually chose this one because it provided a good compositional balance between the height of the sun, the silhouettes of the mountains and the poppies in the foreground."], 1))
        componentList.add(new Component(ComponentType.IMAGE,
                [title: "Photo 2: Lilies illuminated by the glow of the evening sun",
                 caption: "EOS 6D/ EF16-35mm f/4L IS USM/ FL: 16mm/ Manual exposure (f/9, 1/200 sec, EV-1.0)/ ISO 100/ WB: Auto<br />Date of shoot: mid-Jun<br />Photo by Yoshiki Fujiwara",
                 copyright: "",
                 imageUrl: "/assets/article/img7.jpg",
                 mediaId: null], 2))
        componentList.add(new Component(ComponentType.TEXT_LEFT,
                [content: "<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. " +
                        "It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>",
                 imageUrl: "/assets/snippets/img/placeholder-600x400.png",
                 mediaId: null], 3))
        componentList.add(new Component(ComponentType.TEXT_RIGHT,
                [content: "<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. " +
                        "It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>",
                 imageUrl: "/assets/snippets/img/placeholder-600x400.png",
                 mediaId: null], 4))

        [componentList: componentList]
    }

    def getProductCategoryBySite(Integer siteId) {
        return adminCategoryService.findProductCategoriesBySite(siteId)
    }

    /*Selling Point*/
    def sellingPoint() {
        def sellingPoints = ProductSellingPoint.findAll()
        User user = springSecurityService.getCurrentUser()
        def flow = Flow.findByName("Product Workflow")
        def workFlowList = WorkFlow.findAllByFlow(flow)
        def authorList = User.list(sort: "username")
        def currentWorkFlow = WorkFlow.findByFlowAndIsStart(flow, Boolean.FALSE)
        def languageList = Language.list(sort: "name")

        [
                sellingPoints: sellingPoints,
                user: user,
                flow: flow,
                workFlowList: workFlowList,
                authorList: authorList,
                currentWorkFlow: currentWorkFlow,
                languageList: languageList
        ]
    }

    @Transactional
    def getProductList() {
        def data = adminProductService.findAll()
        DataResult dataResult = new DataResult(true, data);
        render dataResult.toMap() as JSON
    }

    @Transactional
    def duplicate(){
        adminProductService.duplicateProduct(params?.list('ids'))
    }

}
