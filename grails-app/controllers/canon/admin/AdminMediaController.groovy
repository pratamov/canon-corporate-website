package canon.admin

import canon.Media
import canon.enums.MediaSource
import canon.enums.MediaType
import canon.enums.Status
import canon.model.DataResult
import canon.utils.ImageUtils
import canon.utils.VideoUtils
import grails.converters.JSON
import grails.transaction.Transactional

import javax.servlet.http.HttpServletResponse

import static org.springframework.http.HttpStatus.NOT_FOUND

class AdminMediaController {

    static allowedMethods = [upload: "POST", getMediaDirectories: "GET"]

    def adminMediaService
    def springSecurityService

    def index() { }

    @Transactional
    def upload(Media media) {
        DataResult dataResult = new DataResult()

        def file = request.getFile('file')
        if (file.empty) {
            dataResult.status = "error"
            dataResult.message = "Please select file to upload."
            dataResult.success = false

            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            render dataResult.toMap() as JSON
            return
        }

        boolean isImage = false
        boolean isVideo = false
        if (ImageUtils.isImage(file.getInputStream())) {
            media.mediaType = MediaType.IMAGE
            isImage = true
        } else if (VideoUtils.isVideo(file.getInputStream())){
            isVideo = true
            media.mediaType = MediaType.VIDEO
        } else {
            media.mediaType = MediaType.DOCUMENT
            /*dataResult.status = "error"
            dataResult.message = "Invalid file type. File should be an image/video."
            dataResult.success = false

            response.setStatus(HttpServletResponse.SC_BAD_REQUEST)
            if (request.getHeader("X-Requested-With") == "XMLHttpRequest") {
                render dataResult.message
            } else {
                render dataResult.toMap() as JSON
            }
            return*/
        }

        def upload = adminMediaService.upload(file, media, isImage, isVideo)
        dataResult.errors = upload.errors
        dataResult.message = upload.message
        dataResult.status = upload.status
        dataResult.success = upload.success
        dataResult.data = upload.data

        if (!upload.success) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST)
        }
        render dataResult.toMap() as JSON
    }

    @Transactional
    def save() {
        DataResult dataResult = new DataResult()

        Media media = new Media()
        media.properties = params
        media.status = Status.DRAFT

        def result = adminMediaService.createFromLink(media)
        dataResult.errors = result.errors
        dataResult.message = result.message
        dataResult.status = result.status
        dataResult.success = result.success
        dataResult.data = result.data

        if (!dataResult.success) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST)
        }
        render dataResult.toMap() as JSON
    }

    def directories() {
        DataResult dataResult = new DataResult()
        def directories = adminMediaService.getMediaFolders()

        def result = [:]
        result.status = "success"
        result.data = directories
        result.success = true

        render dataResult.toMap() as JSON
    }

    def search() {
        DataResult dataResult = new DataResult()
        def medias = adminMediaService.search(params)

        dataResult.status = "success"
        dataResult.success = true
        dataResult.data = medias

        render dataResult.toMap() as JSON
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'media.label', default: 'Media'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
