package canon.admin

import canon.Flow
import canon.Language
import canon.Page
import canon.PageTag
import canon.Product
import canon.Site
import canon.Subject
import canon.Tag
import canon.User
import canon.WorkFlow
import canon.enums.Segment
import canon.enums.Status
import canon.enums.CategoryType
import canon.enums.ContentType
import grails.transaction.Transactional

@Transactional(readOnly = true)
class AdminArticleController {

    static allowedMethods = [save: "POST"]

    def springSecurityService
    def adminCategoryService
    def adminPageService
    def adminArticleService

    def index() { 
        if (!params.type) params.type = "ALL"
        if (!params.status) params.status = Status.PUBLISHED.name()

        def statusList = [Status.PUBLISHED, Status.DRAFT, Status.ARCHIVED]
        def typeList = [ContentType.ARTICLE, ContentType.PAGE, ContentType.EVENT]

        String dateFormat = message(code: 'admin.default.view.date.format', default: 'd MMM yyyy')
        Date dateFrom = params?.date('dateFrom', dateFormat)
        Date dateTo = params?.date('dateTo', dateFormat)

        def paramsOverride = [:]
        paramsOverride.put("dateFrom", dateFrom)
        paramsOverride.put("dateTo", dateTo)
        paramsOverride.put("statusList", statusList)
        paramsOverride.put("typeList", typeList)

        List<Page> pageList = adminArticleService.getProductList(params, paramsOverride)
        
        def statusCount = adminArticleService.countBy("status", params, paramsOverride)
        def typeCount = adminArticleService.countBy("contentType", params, paramsOverride)

        def countByStatus = [:]
        statusList.each {
            countByStatus.put(it.name(), statusCount.get(it, 0))
        }

        def countByType = [ALL: typeCount?.collect{it.value}.sum {it}?:0]
        typeList.each {
            countByType.put(it.name(), typeCount.get(it, 0))
        }

        [
            locationList: Site.all,
            statusList: statusList,
            countByStatus: countByStatus,
            typeList: typeList,
            countByType: countByType, 
            pageList: pageList
        ]
    }

    def create() {
        User user = springSecurityService.getCurrentUser()
        def flow = Flow.findByName("Page Workflow")
        def authorList = User.list(sort: "username")
        def workFlowList = WorkFlow.findAllByFlow(flow)
        def currentWorkFlow = WorkFlow.findByFlowAndIsStart(flow, Boolean.TRUE)
        def languageList = Language.list(sort: "name")
        def topics = Subject.list(sort: "name")
        def tags = Tag.list(sort: "name")
        List<Site> siteList = Site.list(sort: "name")

        List<CategoryType> categoryTypes = new ArrayList<CategoryType>()
        categoryTypes.add(CategoryType.ARTICLE)
        categoryTypes.add(CategoryType.EVENT)
        categoryTypes.add(CategoryType.COURSE)

        def consumerList = adminCategoryService.getLowestLevelCategories(Segment.CONSUMER,categoryTypes)
        def businessList = adminCategoryService.getLowestLevelCategories(Segment.BUSINESS,categoryTypes)


        [
                page: new Page(),
                user: user,
                authorList: authorList,
                siteList:siteList,
                flow: flow,
                workFlowList: workFlowList,
                currentWorkFlow:currentWorkFlow,
                languageList:languageList,
                consumerList:consumerList,
                businessList:businessList,
                topics:topics,
                tags:tags
        ]
    }

    @Transactional
    def save(Page page) {
        User user = springSecurityService.getCurrentUser()

        if (page == null) {
            transactionStatus.setRollbackOnly()
            page = new Page()
            page.errors = page.errors.allErrors.collect{g.message([error : it])}
            return
        }

        Site defaultSite = Site.findByCode("asia")

        if(!page?.contentType){
            page.contentType = ContentType.ARTICLE
        }

        if(!page?.site){
            page.site = defaultSite
        }

        page = adminPageService.savePage(params, page)

        if (page.hasErrors()) {
            transactionStatus.setRollbackOnly()

            def flow = Flow.findByName("Page Workflow")
            def authorList = User.list(sort: "username")
            def workFlowList = WorkFlow.findAllByFlow(flow)
            def currentWorkFlow = WorkFlow.findByFlowAndIsStart(flow, Boolean.TRUE)
            def languageList = Language.list(sort: "name")
            def topics = Subject.list(sort: "name")
            def tags = Tag.list(sort: "name")
            List<Site> siteList = Site.list(sort: "name")

            List<CategoryType> categoryTypes = new ArrayList<CategoryType>()
            categoryTypes.add(CategoryType.ARTICLE)
            categoryTypes.add(CategoryType.EVENT)
            categoryTypes.add(CategoryType.COURSE)

            def consumerList = adminCategoryService.getLowestLevelCategories(Segment.CONSUMER,categoryTypes)
            def businessList = adminCategoryService.getLowestLevelCategories(Segment.BUSINESS,categoryTypes)

            respond page.errors, view:'create',
                    model: [
                            page:page,
                            user: user,
                            authorList: authorList,
                            siteList:siteList,
                            flow: flow,
                            workFlowList: workFlowList,
                            currentWorkFlow:currentWorkFlow,
                            languageList:languageList,
                            consumerList:consumerList,
                            businessList:businessList,
                            topics:topics,
                            tags:tags
                    ]
            return
        }

        if (params?.flow as String == 'NEXT'){
            redirect action: "index"
        }else {
            redirect(action: "edit", id: page?.id)
        }
    }
}
