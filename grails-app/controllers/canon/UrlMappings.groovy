package canon

import canon.enums.CategoryType

class UrlMappings {

    static mappings = {
        "/$controller/$action?/$id?(.$format)?"{
            constraints {
                controller notEqual: "contactUs"
            }
        }

        group "/admin", {
            "/article/$action?/$id?(.$format)?"(controller: "adminArticle")
            "/media/$action?/$id?(.$format)?"(controller: "adminMedia")
            "/page/$action?/$id?(.$format)?"(controller: "adminPage")
            "/product/$action?/$id?(.$format)?"(controller: "adminProduct")
            "/tool/$action?/$id?(.$format)?"(controller: "adminTool")
            "/category/$action?/$id?(.$format)?"(controller: "adminCategory")
            "/faq/$action?/$id?(.$format)?"(controller: "adminFaq")
        }


        "/"(controller: "home")

        /**
         * Contact Us
         */
        "/$site/contact-us"(controller: "contactUs", action: "index")
        "/$site/contact-us/submit"(controller: "contactUs", action: "submit")
        "/contact-us"(controller: "contactUs", action: "index")
        "/contact-us/submit"(controller: "contactUs", action: "submit")


        "/comparing/specs/$idA?/$idB?/$idC?"(controller: "product", action: "comparing")

        "/$site/products"(controller: "product", action: "category")
        "/$site/products/search"(controller: "product", action: "list")
        "/$site/products/compare/$idA?/$idB?/$idC?"(controller: "product", action: "comparing")
        "/$site/$id/product"(controller: "product", action: "show")

        name articleMain: "/$site/articles"(controller: "page", action: "index")
        name articleList: "/$site/articles/search"(controller: "page", action: "list")
        name articleDetails: "/$site/$id/article"(controller: "page", action: "show")

        name courseMain: "/$site/course"(controller: "page", action: "index") { categoryType = CategoryType.COURSE.name() }
        name courseList: "/$site/course/search"(controller: "page", action: "list") { categoryType = CategoryType.COURSE.name() }
        name courseDetails: "/$site/$id/course"(controller: "page", action: "show")

        name solutionMain: "/$site/solutions"(controller: "page", action: "index") { categoryType = CategoryType.SOLUTION.name() }
        name solutionList: "/$site/solutions/search"(controller: "page", action: "list") { categoryType = CategoryType.SOLUTION.name() }
        name solutionDetails: "/$site/$id/solution"(controller: "page", action: "show")

        name eventMain: "/$site/events"(controller: "page", action: "index") { categoryType = CategoryType.EVENT.name() }
        name eventList: "/$site/events/search"(controller: "page", action: "list") { categoryType = CategoryType.EVENT.name() }
        name eventDetails: "/$site/$id/event"(controller: "page", action: "show")


        "500"(view:'/error')
        "404"(view:'/notFound')
    }
}
