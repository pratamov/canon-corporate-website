package canon

import canon.constant.SettingCodeConstant
import canon.model.contact.asia.ContactUsForm
import canon.utils.ServletUtils
import grails.converters.JSON
import grails.transaction.Transactional
import org.apache.commons.codec.binary.Base64
import org.apache.commons.lang.StringUtils
import org.apache.commons.lang.math.NumberUtils
import org.apache.commons.validator.routines.EmailValidator
import org.json.JSONArray
import org.json.JSONObject
import org.tempuri.CanonDataService
import org.tempuri.ICanonDataService

import javax.xml.namespace.QName

class ContactUsController {

    def contactService;

    private static final QName SERVICE_NAME = new QName("http://tempuri.org/", "CanonDataService");

    def index() {
        String siteParam = params?.site?: '';
        params?.site = params?.site ?: 'singapore'
        params?.lang = params?.lang ?: 'en'
        params?.segment = params?.segment ?: 'consumer'

        Site site = null;

        if (siteParam) {
            site = Site.findByCode(siteParam.toLowerCase());
        }

        println("siteParam: " + siteParam);
        println("site: " + site?.code);

        if (!site) {
            site = Site.findByIsDefault(true);
            redirect(controller: "contactUs", action: "index", params: [site: site.code]);
            return;
        }


        if ("asia".equals(site.code)) {
            Language language = Language.findById(params?.lang);
            Setting leftSideContent = Setting.findBySiteAndLanguageAndSettingCode(site, language, SettingCodeConstant.CONTACT_LEFT_CONTENT);
            Setting enquiryList = Setting.findBySiteAndLanguageAndSettingCode(site, language, SettingCodeConstant.CONTACT_TYPE_OF_ENQUIRY);
            Setting salutationList = Setting.findByLanguageAndSettingCode(language, SettingCodeConstant.CONTACT_SALUTATION);
            Setting countryList = Setting.findByLanguageAndSettingCode(language, SettingCodeConstant.CONTACT_COUNTRIES);

            JSONArray enquiryJsonArray = new JSONArray(enquiryList?.settingValue?:SettingCodeConstant.CONTACT_TYPE_OF_ENQUIRY_SELECTOR_DEFAULT);
            def enquiries = []
            for (int i = 0; i < enquiryJsonArray.length(); i++) {
                JSONObject enquiry = enquiryJsonArray.getJSONObject(i);
                def enquiryItem = [:];
                enquiryItem.key = enquiry.optString("key");
                enquiryItem.name = enquiry.optString("name");
                enquiries.add(enquiryItem);
            }

            JSONArray salutationJsonArray = new JSONArray(salutationList?.settingValue?:SettingCodeConstant.CONTACT_SALUTATION_DEFAULT);
            List<String> salutations = new ArrayList<>();
            for (int i = 0; i < salutationJsonArray.length(); i++) {
                salutations.add(salutationJsonArray.optString(i));
            }

            JSONArray countryJsonArray = new JSONArray(countryList?.settingValue?:SettingCodeConstant.CONTACT_COUNTRIES_DEFAULT);
            def countries = []
            for (int i = 0; i < countryJsonArray.length(); i++) {
                JSONObject enquiry = countryJsonArray.getJSONObject(i);
                def countryItem = [:];
                countryItem.key = enquiry.optString("key");
                countryItem.name = enquiry.optString("name");
                countryItem.url = enquiry.optString("url");
                countries.add(countryItem);
            }

            render(view: "/contactUs/asia/index", model: [
                leftSideContent: leftSideContent.settingValue,
                salutationList: salutations,
                enquiryList: enquiries,
                enquiryJson: (enquiryList?.settingValue?:SettingCodeConstant.CONTACT_TYPE_OF_ENQUIRY_SELECTOR_DEFAULT),
                countryList: countries
            ]);
            return;
        }

//        URL wsdlURL = new URL("http://goldcrm.convergys.com/GoldBulletWCF/CanonDataService.svc?wsdl")
//        CanonDataService ss = new CanonDataService(wsdlURL, SERVICE_NAME);
//        ICanonDataService port = ss.getBasicHttpBindingICanonDataService();
//
//        System.out.println("Invoking createCanonOnSiteServiceRequests...");
//        String _createCanonOnSiteServiceRequests_serialnumber = "ZX1HAHK32GW";
//        String _createCanonOnSiteServiceRequests_faultydescription = "Just testing wsdl only";
//        String _createCanonOnSiteServiceRequests_avoidlunchtime = "1";
//        String _createCanonOnSiteServiceRequests_salutation = "Mr";
//        String _createCanonOnSiteServiceRequests_contactperson = "Muhammad Rifai Syukri";
//        String _createCanonOnSiteServiceRequests_contactno = "81234567";
//        String _createCanonOnSiteServiceRequests_emailaddress = "ifhayz@gmail.com";
//        String _createCanonOnSiteServiceRequests_confirmemailaddress = "ifhayz@gmail.com";
//        String _createCanonOnSiteServiceRequests_companyname = "Fairtech Pte Ltd";
//        Integer _createCanonOnSiteServiceRequests__return = port.createCanonOnSiteServiceRequests(_createCanonOnSiteServiceRequests_serialnumber, _createCanonOnSiteServiceRequests_faultydescription, _createCanonOnSiteServiceRequests_avoidlunchtime, _createCanonOnSiteServiceRequests_salutation, _createCanonOnSiteServiceRequests_contactperson, _createCanonOnSiteServiceRequests_contactno, _createCanonOnSiteServiceRequests_emailaddress, _createCanonOnSiteServiceRequests_confirmemailaddress, _createCanonOnSiteServiceRequests_companyname);
//        System.out.println("createCanonOnSiteServiceRequests.result=" + _createCanonOnSiteServiceRequests__return);


        render(view: "/contactUs/index", model: [])
    }

    @Transactional
    def submit() {
        params?.site = params?.site ?: 'singapore'
        params?.lang = params?.lang ?: 'en'
        params?.segment = params?.segment ?: 'consumer'

        def jsonObject = request.JSON

        if (params?.site == 'asia') {
            return canonAsia(jsonObject);
        }


        def result = [:]
        result.status = "success"
        result.success = true

        render result as JSON
    }

    /****
     * For canon asia contact.
     * @param jsonObject
     * @return
     */
    private def canonAsia(jsonObject){

        Language language = Language.findById(params?.lang);
        Site site = Site.findByCode(params?.site);

        Setting enquiryList = Setting.findBySiteAndLanguageAndSettingCode(site, language, SettingCodeConstant.CONTACT_TYPE_OF_ENQUIRY);
        Setting salutationList = Setting.findByLanguageAndSettingCode(language, SettingCodeConstant.CONTACT_SALUTATION);
        Setting countryList = Setting.findByLanguageAndSettingCode(language, SettingCodeConstant.CONTACT_COUNTRIES);

        def contactUsForm = new ContactUsForm(jsonObject)

        contactUsForm.validate();

        String enquiryType = contactUsForm.typeOfEnquiry;
        String specificType = contactUsForm.specificEnquiry;
        List<String> specificTypes = new ArrayList<>();
        if (contactUsForm.errors.getFieldErrorCount("typeOfEnquiry") == 0) {
            JSONArray enquiryJsonArray = new JSONArray(enquiryList?.settingValue?:SettingCodeConstant.CONTACT_TYPE_OF_ENQUIRY_SELECTOR_DEFAULT);
            boolean isExist = false;
            for (int i = 0; i < enquiryJsonArray.length(); i++) {
                JSONObject enquiry = enquiryJsonArray.getJSONObject(i);
                enquiry.getJSONArray("types");
                for (int j = 0; j < enquiry.getJSONArray("types").length(); j++) {
                    specificTypes.add(enquiry.getJSONArray("types").getJSONObject(j).getString("key"));
                    if (enquiry.getJSONArray("types").getJSONObject(j).getString("key").equalsIgnoreCase(contactUsForm.specificEnquiry)){
                        specificType = enquiry.getJSONArray("types").getJSONObject(j).getString("name");
                    }
                }
                if (contactUsForm.typeOfEnquiry.equalsIgnoreCase(enquiry.optString("key"))) {
                    isExist = true;
                    enquiryType = enquiry.optString("name");
                }
            }
            if (!isExist) {
                contactUsForm.errors.rejectValue("typeOfEnquiry", "canon.model.contact.asia.ContactUsForm.typeOfEnquiry.invalid");
            }
        }

        if (contactUsForm.errors.getFieldErrorCount("specificEnquiry") == 0 && !specificTypes.contains(contactUsForm.specificEnquiry)) {
            contactUsForm.errors.rejectValue("specificEnquiry", "canon.model.contact.asia.ContactUsForm.specificEnquiry.invalid");
        }

        String
        if (contactUsForm.errors.getFieldErrorCount("salutation") == 0) {
            JSONArray salutationJsonArray = new JSONArray(salutationList?.settingValue?:SettingCodeConstant.CONTACT_SALUTATION_DEFAULT);
            boolean isExist = false;
            for (int i = 0; i < salutationJsonArray.length(); i++) {
                if (salutationJsonArray.optString(i).equalsIgnoreCase(contactUsForm.salutation)) {
                    isExist = true;
                }
            }

            if (!isExist) {
                contactUsForm.errors.rejectValue("salutation", "canon.model.contact.asia.ContactUsForm.salutation.invalid");
            }
        }

        String countryName = contactUsForm.countryOfResidence;
        if (contactUsForm.errors.getFieldErrorCount("countryOfResidence") == 0) {
            JSONArray countryJsonArray = new JSONArray(countryList?.settingValue?:SettingCodeConstant.CONTACT_COUNTRIES_DEFAULT);
            boolean isExist = false;
            for (int i = 0; i < countryJsonArray.length(); i++) {
                JSONObject enquiry = countryJsonArray.getJSONObject(i);

                String key = enquiry.optString("key");
                String name = enquiry.optString("name");
                String url = enquiry.optString("url");

                if (!StringUtils.isNotBlank(url) && key.equals(contactUsForm.countryOfResidence)) {
                    isExist = true;
                    countryName = name;
                }

            }
            if (!isExist) {
                contactUsForm.errors.rejectValue("countryOfResidence", "canon.model.contact.asia.ContactUsForm.countryOfResidence.invalid");
            }
        }

        if (contactUsForm.hasErrors()) {
            def result = [:]
            result.status = "error"
            result.message = "Please enter valid data before subscribing"
            result.success = false
            result.errors = contactUsForm.errors

            response.setStatus(400)
            render result as JSON
            return
        }


        String inputParams = "Email Sent: Country: "+countryName+" " +
                "| Type Of Enquiry:"+enquiryType+" " +
                "| Type of Specific Enquiry:"+specificType+" " +
                "| ENQUIRY: "+contactUsForm.message+" " +
                "| SALUTATION: "+ contactUsForm.salutation+" " +
                "| Contact Person: "+contactUsForm.name+" " +
                "| Contact No: "+contactUsForm.contactNo+" " +
                "| Email: "+contactUsForm.emailAddress+" " +
                "| Confirm Email Address: "+contactUsForm.retypeEmailAddress+" | File name: null | File Type: null | File Data: null";
        String ipAddress = ServletUtils.getIpAddress(request);

        B2BContact b2BContact = new B2BContact(
                fullUrl: request.getRequestURL().toString(),
                environment: grails.util.Environment.current.name,
                ipAddress: ipAddress,
                inputParams: inputParams,
                flag: 1
        );

        contactService.submitAsiaContact(contactUsForm, b2BContact);

        def result = [:]
        result.status = "success"
        result.success = true

        render result as JSON

    }
}
