package canon

import canon.enums.Segment

class CategoryController {

    def categoryService
    def categoryMediaService

    /**
     * Moved to {@link ProductController#category()}
     */
    @Deprecated
    def index() {
        Language lang = Language.findById("en")
        Site site = Site.findByCode("singapore")
        Segment segment = Segment.getBySlug("customer")

        List<Category> categoryList = categoryService.getMainProductCategory(site, lang, segment)

        Map<Long, CategoryMedia> categoryMediaMap = new HashMap<>()
        categoryList.each {Category category ->
            categoryMediaMap.put(category.id, categoryMediaService.getFirstByCategory(category))
        }

        render(view: "index", model: [
                categoryList: categoryList,
                categoryMediaMap: categoryMediaMap
        ])
    }

    /**
     * Moved to {@link ProductController#list()}
     */
    @Deprecated
    def show() {

        Category category = categoryService.getBySlug(params?.id)

        List<Category> subCategoryList = new ArrayList<>()
        if(category) {
            subCategoryList = categoryService.getSubProductCategory(category)
        }

        Category selectedSubCategory = null
        subCategoryList.each {Category pc ->
            if(pc?.slug == params?.subCategory) {
                selectedSubCategory = pc
            }
        }

        if (!selectedSubCategory && subCategoryList.size() > 0) {
            selectedSubCategory = subCategoryList?.get(0)
            params?.subCategory = subCategoryList?.get(0)?.slug
        }

        render(view: "show", model: [
                category : category,
                subCategoryList : subCategoryList,
                selectedSubCategory : selectedSubCategory
        ])
    }
}
