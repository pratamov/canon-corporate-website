package canon

import canon.enums.RelationType
import canon.enums.Segment
import canon.enums.Status
import grails.converters.JSON

class ProductController {

    def categoryService
    def productService

    def category() {
        params?.site = params?.site ?: 'singapore'
        params?.lang = params?.lang ?: 'en'
        params?.segment = params?.segment ?: 'consumer'

        Language lang = Language.findById(params?.lang)
        Site site = Site.findByCode(params?.site)
        Segment segment = Segment.getBySlug(params?.segment as String)

        List<Category> categoryList = categoryService.getMainProductCategory(site, lang, segment)

        render(view: "category", model: [
                categoryList: categoryList
        ])
    }

    def list() {

        params?.lang = params?.lang ?: "en"
        params?.site = params?.site ?: "singapore"
        params?.segment = params?.segment ?: 'consumer'

        Language lang = Language.findById(params?.lang)
        def paramsOverride = [:]

        Set<Category> subCategories = new ArrayList<>()
        Category category = Category.findBySlug(params.category as String)
        Category parent = category?.parent
        if (!parent) {
            parent = category
            subCategories = parent?.childrens
            category = subCategories?.getAt(0)
            paramsOverride.category = category?.slug
        } else {
            subCategories = parent?.childrens
        }

        List<Product> featuredProductList = productService.getFeaturedProducts(params?.site as String, category.slug, null)

        def result = productService.searchProducts(params, paramsOverride)

        render(view: "list", model: [
                parent : parent,
                category: category,
                subCategories : subCategories,
                featuredProducts: featuredProductList,
                resultList: result.productList,
                resultCount: result.productCount,
                aggs: result.aggs
        ])
    }

    def show() {
        params?.site = params?.site ?: 'singapore'
        params?.lang = params?.lang ?: 'en'
        params?.segment = params?.segment ?: 'consumer'

        Site site = Site.findByCodeAndStatus(params?.site, Status.ACTIVE)
        Product product = Product.findBySlug(params?.id)
        ProductDetail productLocation = ProductDetail.findByProductAndSite(product, site)

        List<ProductComponent> productComponentList = productService.getPublishedProductComponents(product)

        model: [
                product : product,
                productLocation: productLocation,
                productCategory: getProductCategory(product, params?.category),
                productComponentList: productComponentList
        ]
    }

    @Deprecated
    def overview() {
        params?.site = params?.site ?: 'singapore'
        params?.segment = params?.segment ?: 'consumer'

        Product product = Product.findBySlug(params?.id)

        Site site = Site.findByCodeAndStatus(params?.site, Status.ACTIVE)
        ProductDetail productLocation = ProductDetail.findByProductAndSite(product, site)

        def featureList = productService.getProductFeatures(product)
        def mainFeature = featureList.take(3)
        featureList = featureList.drop(3)

        def productRelations = productService.getProductRelations(product)

        render(view: "overview", model: [
                product : product,
                productLocation : productLocation,
                mainFeature : mainFeature,
                restFeature : featureList,
                relatedConsumable: productRelations.get(RelationType.CONSUMABLE),
                relatedProducts: productRelations.get(RelationType.PRODUCT)
        ])
    }

    def specification() {
        params?.site = params?.site ?: 'singapore'
        params?.segment = params?.segment ?: 'consumer'

        Product product = Product.findBySlug(params?.id)

        Site site = Site.findByCodeAndStatus(params?.site, Status.ACTIVE)
        ProductDetail productLocation = ProductDetail.findByProductAndSite(product, site)

        String mediaUrl = product?.getDefaultMedia()?.media?.url

        //define later
        def manualUrl = null

        //define later
        def brochureUrl = null

        def productSpecifications = productService.generateProductSpec(product?.productSpecifications)

        render(view: "specification", model: [
                product : product,
                productImage : mediaUrl,
                productManual : manualUrl,
                productBrochure : brochureUrl,
                productSpecifications : productSpecifications,
                productLocation : productLocation,
                productCategory: getProductCategory(product, params?.category)
        ])
    }

    def comparing(){
        Product productA = Product.findBySlug(params?.idA)
        Product productB = Product.findBySlug(params?.idB)
        Product productC = Product.findBySlug(params?.idC)

        Category productCategory = Category.findBySlug(params?.subCategory)


        def priceFormatb = null
        def priceFormatc = null

        if( productB?.currencyCode && productB?.price){
            priceFormatb = formatNumber(number: productB?.price, type: 'currency', currencyCode: productB?.currencyCode, locale: 'en_US')
            //priceFormatb = productB?.currencyCode + ' ' +productB?.price
        }
        if(productC?.currencyCode && productC?.price){
            priceFormatc = formatNumber(number: productC?.price, type: 'currency', currencyCode: productC?.currencyCode, locale: 'en_US')
            //priceFormatc = productC?.currencyCode + ' ' +productC?.price
        }

        def mediab = productB?.getDefaultMedia()?.media?.url
        def mediac = productC?.getDefaultMedia()?.media?.url

        def comparedProduct = [productA,productB,productC]

        def pSpecs = ProductSpecification.findAllByProductInList(comparedProduct)

        def groups = [:]
        def specsA = [:]
        def specsB = [:]
        def specsC = [:]

        productA?.productSpecifications?.each{ spec ->
            if(spec.specification?.comparable) {
                specsA.put(spec.specification.name, spec.specValue)
            }
        }
        productB?.productSpecifications?.each{ spec ->
            if(spec.specification?.comparable) {
                specsB.put(spec.specification.name, spec.specValue)
            }
        }
        productC?.productSpecifications?.each{ spec ->
            if(spec.specification?.comparable) {
                specsC.put(spec.specification.name, spec.specValue)
            }
        }

        pSpecs.each{ s ->
            def group = groups.get(s.specification.group.name)
            if(!group){
                group = []
                groups.put(s.specification.group.name,group)
            }

            def valuea
            def valueb
            def valuec
            def event

            if(productA){
                valuea = specsA.get(s.specification.name)
                if(!valuea){
                    valuea = '-'
                }
            }
            if(productB){
                valueb = specsB.get(s.specification.name)
                if(!valueb){
                    valueb = '-'
                }
                if(valuea!=valueb){
                    event = 'event'
                }
            }
            if(productC){
                valuec = specsC.get(s.specification.name)
                if(!valuec){
                    valuec = '-'
                }
                if(valuea!=valueb || valueb!=valuec || valuea!=valuec){
                    event = 'event'
                }
            }

            group.add([
                    name:s.specification.name,
                    event:event,
                    valuea:valuea,
                    valueb:valueb,
                    valuec:valuec
            ])
        }

        def finalGroups = []
        groups.each{ grp ->
            finalGroups.add([groupName:grp.key,specs:grp.value])
        }


        def result = [productb:[img:mediab,price:priceFormatb,name:productB?.name],
                      productc:[img:mediac,price:priceFormatc,name:productC?.name],
                      groups: finalGroups]
        render result as JSON
    }

    def compare() {
        params?.site = params?.site ?: 'singapore'
        params?.segment = params?.segment ?: 'consumer'

        Product productA = Product.findBySlug(params?.id)
        Product productB = Product.findBySlug(params?.pbId)
        Product productC = Product.findBySlug(params?.pcId)

        Category c = ProductCategory.findByProduct(productA)?.category

        Site site = Site.findByCodeAndStatus(params?.site, Status.ACTIVE)
        ProductDetail productLocation = ProductDetail.findByProductAndSite(productA, site)

        def comparedProduct = [productA,productB,productC]

        def pSpecs = ProductSpecification.findAllByProductInList(comparedProduct)

        def groups = [:]
        def specs = [:]

        productA?.productSpecifications?.each{ spec ->
            if(spec.specification?.comparable){
                specs.put(spec.specification.name,spec.specValue)
            }
        }

        pSpecs.each{ s ->
            def group = groups.get(s.specification.group.name)
            if(!group){
                group = []
                groups.put(s.specification.group.name,group)
            }
            group.add(s.specification)
        }

        def products = Product.findAllByTopic(productA.topic)
        //def products = ProductCategory.findAllByCategory(c)?.product


        render(view: "compare", model: [
                product : productA,
                productA : productA,
                productB : productB,
                productC : productC,
                products: products,
                productCategory: getProductCategory(productA, params?.category),
                productLocation: productLocation,
                price: productA?.price,
                groups: groups,
                specs: specs
        ])
    }

    def whereToBuy() {

    }

    private ProductCategory getProductCategory(Product product, String categorySlug) {
        ProductCategory result = null

        if (categorySlug) {
            Category category = Category.findBySlug(categorySlug)
            if (category) {
                result = ProductCategory.findByProductAndCategory(product, category)
                if (result) return result
            }
        }

        return ProductCategory.findByProduct(product)
    }
}
