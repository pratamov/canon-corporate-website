package canon

class IndexingJob {

    def elasticSearchService

    static triggers = {
      //simple repeatInterval: 5000l // execute job once in 5 seconds
    }

    def execute(context) {
        Boolean products = context.mergedJobDataMap.get('products') ?: Boolean.FALSE
        Boolean articles = context.mergedJobDataMap.get('articles') ?: Boolean.FALSE
        Boolean publishedOnly = context.mergedJobDataMap.get('published') ?: Boolean.FALSE
        if (products) elasticSearchService.indexingProducts(publishedOnly)
        if (articles) elasticSearchService.indexingArticles(publishedOnly)
    }
}
