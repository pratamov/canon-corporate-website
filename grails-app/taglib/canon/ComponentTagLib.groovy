package canon

import canon.enums.ComponentType
import canon.enums.Status
import canon.model.comp.*
import groovy.json.JsonOutput

class ComponentTagLib {

    def productService
    def pageService

    /**
     * Render component with template
     *
     * @attr component REQUIRED the field name
     * @attr value the field value
     */
    def renderComp = { attrs, body ->
        canon.model.Component component = attrs.component
        String site = attrs.site
        String segment = attrs.segment
        if (component) {
            def modelMap = [:]
            if (component?.compType?.name()?.startsWith("PROD_")) {
                Product product = null
                if (attrs.product) {
                    product = attrs.product
                } else {
                    Long id = component?.asJsonObject?.id ? Long.valueOf(component?.asJsonObject?.id) : null
                    product = getProduct(id)
                }
                modelMap.product = product
            }
            prepareContent(component, site, segment)
            modelMap.component = component
            modelMap.segment = segment
            out << render(template:"/templates/tagLibs/" + component?.compType?.name()?.toLowerCase(), model: modelMap)
        }
    }

    def getProduct(Long id) {
        if (!id) return null
        Product.findById(id)
    }

    private void prepareContent(canon.model.Component comp, String site, String segment) {
        switch (comp.compType) {
            case ComponentType.BANNER:
                BannerComp bannerComp = (BannerComp) comp.getAsComponent()
                prepareBanner(bannerComp)
                break
            case ComponentType.FEATURED_PRODUCTS:
                FeaturedProductsComp featuredComp = (FeaturedProductsComp) comp.getAsComponent()
                prepareFeaturedProduct(featuredComp, site, segment)
                break
            case ComponentType.FEATURED_ARTICLES:
                FeaturedArticlesComp featuredComp = (FeaturedArticlesComp) comp.getAsComponent()
                prepareFeaturedArticles(featuredComp, site, segment)
                break
            case ComponentType.FEATURED_ARTICLES_SOLUTION:
                FeaturedArticlesComp featuredComp = (FeaturedArticlesComp) comp.getAsComponent()
                prepareFeaturedArticles(featuredComp, site, segment)
                break
            case ComponentType.PROD_FEATURES:
                ProdFeaturesComp featuresComp = (ProdFeaturesComp) comp.getAsComponent()
                prepareProductFeatures(featuresComp)
                break
            case ComponentType.PROD_RELATION:
                ProdRelationComp relationComp = (ProdRelationComp) comp.getAsComponent()
                prepareProductRelation(relationComp)
                break
            default:
                def content = comp.getAsJsonObject()
                if(content instanceof Map) {
                    switch (comp.compType){
                        case ComponentType.PROD_RELATED_READS:
                            content.put("articles", prepareProductRelatedReadsContent(content))
                            break
                    }
                }
                comp.compContent = JsonOutput.toJson(content)
        }
    }

    private def prepareProductRelatedReadsContent(Map content) {
        def articles = []
        String prodIdStr = content.get("id")
        Long prodId = prodIdStr && prodIdStr.isLong() ? Long.valueOf(prodIdStr) : null
        if (prodId) {
            Product prod = Product.findById(prodId)
            if(prod) {
                def relatedArticles = productService.getProductRelatedReads(prod)
                relatedArticles?.each {Map map ->
                    articles.add([
                            id: map.get("id"),
                            slug: map.get("slug"),
                            tagId: map.get("tagId"),
                            tagName: map.get("tagName"),
                            title: map.get("title"),
                            bgId: map.get("bgId"),
                            bgUrl: map.get("bgUrl")
                    ])
                }
            }
        }

        return articles
    }

    private void prepareFeaturedProduct(FeaturedProductsComp featuredComp, String site, String segment) {
        try {
            List<Product> products = productService.getFeaturedProducts(site, null, segment)
            featuredComp.setFeaturedList(products)
        } catch (Exception e) {
            log.error(e?.message, e)
        }
    }

    private void prepareFeaturedArticles(FeaturedArticlesComp featuredComp, String site, String segment) {
        try {
            List<Page> articles = pageService.getFeaturedArticles(site, featuredComp.contentType?.name(), segment, featuredComp.categoryId)
            featuredComp.setFeaturedList(articles)
            if (featuredComp.categoryId) {
                featuredComp.category = Category.findById(featuredComp.categoryId)
            }
        } catch (Exception e) {
            log.error(e?.message, e)
        }
    }

    private void prepareBanner(BannerComp bannerComp) {
        try {
            Banner banner = Banner.findById(bannerComp.id)
            bannerComp.setBanner(banner)
        } catch (Exception e) {
            log.error(e?.message, e)
        }
    }

    private void prepareProductFeatures(ProdFeaturesComp featuresComp) {
        try {
            if (featuresComp?.getFeaturesId()?.size() > 0) {
                List<Feature> features = Feature.createCriteria().list {
                    and {
                        eq("status", Status.PUBLISHED)
                        'in'("id", featuresComp?.getFeaturesId())
                    }
                }
                featuresComp.setFeatureList(features)
            }
        } catch (Exception e) {
            log.error(e?.message, e)
        }
    }

    private void prepareProductRelation(ProdRelationComp comp) {
        if (comp?.id && comp?.relationType) {
            Product product = Product.findById(comp?.id)
            if (product) {
                List<Product> products = productService.getProductRelationsOverview(product, comp?.relationType)
                products?.each {Product p ->
                    ProductMedia productMedia = p.getDefaultMedia()
                    comp?.relatedProducts?.add([
                            id: p.id,
                            name: p.name,
                            slug: p.slug,
                            mediaId: productMedia?.media?.id,
                            mediaUrl: productMedia?.media?.url
                    ])
                }

                int countRelatedProducts = ProductRelation.countByProductAndRelationType(product, comp?.relationType)
                comp.countRelatedProducts = countRelatedProducts
            }
        }
    }
}
